How to install and user dbxmlServer

Versions of Python come bundled with the distributions to provide
turnkey installation.  Starting with Tethys 3.0 we no longer provide
installers but use stand-alone (run-in-place) distibution models as
some organizations security protocols did not allow even signed
installer packages to run.

In general, you should not need to install your own version of Python.
If you do, use the requirements.txt to pick up most of the dependencies.
There are not PyPi releases for some of the modules and others that were
installed locally where you will need to update the requirements.txt.

The requirements.txt was generated as follows using the pipreqs package:
# pip install pipreqs
# ../python -m  pipreqs.pipreqs --encoding utf-8  /path/to/project

Install the required software:
Python 2.7 or Python 3.9 for Tethys versions > 2.5

Berkeley DBXML:  http://www.oracle.com/database/berkeley-db/xml/index.html
	Berkeley DBXML has Python modules both for the underlying
	SleepyCat relational database and for DBXML.  Both must be
	installed to provide Python access to DBXML.
	
	Interfaces to Python must be compiled for specific versions of the language.
	For Python, we need to use the same version of the compiler to compile
	the glue code.  The stock Python 2.7 from python.org was compiled with
	Visual Studio (VS) 2008.  We created a binary distribution as
	follows:   

	        Open up shell window with Visual Studio Tools on path,
		note that VS 2008 has different command prompts for 32
		and 64 bit builds.  Both are typically on the start menu:
		Visual Studio 2008\Visual Studio Tools\
			\Visual Studio Command Prompt
			\Visual Studio x64 Win64 Command Prompt

		Let Root be a variable containing the root directory
		of the dbxml distribution

		1) Build Berkeley DB distribution:
		cd %Root%\dbxml\src\python\bsddb3-4.8.1
		python setup.dbxml.py bdist --format=wininst 
		OR
		python setup.dbxml.py bdist --format=msi

		
		2) 
		cd %Root%\dbxml\src\python
		python setup.py bdist --format=wininst
		OR
		python setup.py bdist --format=msi

		These commands will create install packages in the
		dist subdirectories of each directory where the
		prompt is created.  These must be run to

	May 2018 - Building for Sean Herbert's no admin installer
	(uses Microsfot installer packages with TARGETDIR).
	Libraries are not being installed to the site-packages/bsddb3
	directories.  It looks like setting TARGETDIR to the Python
	root cause package data to be copied there.  Modified the
	bsdb3's setup.dbxml.py to set a data directory:

                # Kludge to support run anywhere misexec script with TARGETDIR
                # set.  Reset to "" for other types of builds
                datadir = "lib/site-packages/bsddb3"  

                if debug:
                    # BDB XML -- extra info, and build dynamic
                    bindir = '../../../../bin/debug'
                    libname = ['libdb%sd' % ver]     # Debug, dynamic
                    data = [(datadir, ["../../../../bin/debug/libdb%sd.dll" %ver])]
                    #libname = ['libdb%ssd' % ver]     # Debug, static
                else:
                    bindir = '../../../../bin'
                    libname = ['libdb%s' % ver]      # Release, dynamic
                    data = [(datadir,["../../../../bin/libdb%s.dll" %ver])]
                    #libname = ['libdb%ss' % ver]      # Release, static
                    # end BDB XML
	

	
        # trying new things 5/2018, msi installer:
        python setup.dbxml.py bdist --format=msi

        SIMILAR CHANGES ARE REQUIRED FOR dbxml's setup.py:

  # Place libraries in site packages
  datadir = "lib/site-packages"

  if debug:
    LIBDIRS =   [os.path.join(dbxml_home,"../lib"),
                 os.path.join(dbxml_home,"build_windows/Debug"),
                 os.path.join(db_home, "build_windows/Debug"),
                 os.path.join(xqilla_home, "lib"),
                 os.path.join(xerces_home, "Build/Win32/VC7")]
  
    LIBS =      ["libdbxml25D",
                 "libdb48D", "xqilla22d",
                 "xerces-c_3D"]

    DATAFILES = [(datadir, [os.path.join(dbxml_home,"../bin/debug/libdbxml25D.dll"),
                       os.path.join(dbxml_home,"../bin/debug/libdb48D.dll"),
                       os.path.join(dbxml_home,"../bin/debug/xqilla22d.dll"),
                       os.path.join(dbxml_home,"../bin/debug/xerces-c_3_0D.dll"),
                       os.path.join(dbxml_home,"build_windows/zlib1.dll"),
                       os.path.join(dbxml_home,"build_windows/zlibwapi.dll")])]
  else:
    LIBDIRS =   [os.path.join(dbxml_home,"../lib"),
                 os.path.join(dbxml_home,"build_windows/Release"),
                 os.path.join(db_home, "build_windows/Release"),
                 os.path.join(xqilla_home, "lib"),
                 os.path.join(xerces_home, "Build/Win32/VC7")]
  
    LIBS =      ["libdbxml25",
                 "libdb48", "xqilla22",
                 "xerces-c_3"]

    DATAFILES = [(datadir, [os.path.join(dbxml_home,"../bin/libdbxml25.dll"),
                       os.path.join(dbxml_home,"../bin/libdb48.dll"),
                       os.path.join(dbxml_home,"../bin/xqilla22.dll"),
                       os.path.join(dbxml_home,"../bin/xerces-c_3_0.dll"),
                       os.path.join(dbxml_home,"build_windows/zlib1.dll"),
                       os.path.join(dbxml_home,"build_windows/zlibwapi.dll")])]



		
Building debug versions of dbxml Python libraries:
		THIS IS NOT NEEDED UNLESS YOU PLAN ON DOING DEVELOPMENT OF TETHYS
		AND NEED TO DEBUG PROBLEMS IN BERKELEY DB XML OR ONE OF THE LIBRARIES
		THAT IT USES (e.g. the Apache Xerces library for XML parsing, or the
		Xqilla library that implements the XQuery and XPath query XML query
		languages).

		In the Python installation, modify include/pyconfig.h such that it no longer
		defines symbol Py_DEBUG when the _DEBUG preprocessor symbol is set.  This
		will prevent linking against the debug libraries that python.org does not
		distribute.  If you build Python from scratch, you can build a debug version
		and forget about this.  
		
		1) Build Berkeley DB distribution:
		cd %Root%\dbxml\src\python\bsddb3-4.8.1
		python setup.dbxml.py build --debug

		2) 
		cd %Root%\dbxml\src\python
		python setup.py build --debug
		
		This will create debug versions of the libraries that must be installed manually.
		In both cases, they will have an _d in the name for the debug library.  Remove
		the _d and copy these libraries into the Python libraries distribution:
			[python]/Lib/site-packages/_dbxml.pyd and
			[python]/Lib/site-packages/bsddb3/_pybsddb.pyd .
		As these will have been linked against debug versions of the Berkeley
		db xml libraries (which you will need to have built), copy the debug
		libraries into the site-packages and bsddb3 directories, or the linker
		will not be able to find them.  The libraries can be found relative to the 
		dbxml installation root:
		
		for 64 bit Windows:
		
		This library must be copied to site-packages and site-package/bsddb3
			[dbxml root]/build_windows/x64/Debug/libdb61d.dll
		These libaries need only be copied to site-package:
			[dbxml root]/build_windows/x64/Debug/libdbxml60d.dll
			xqilla23d.dll
			[dbxml_root]/xerces-c-src/Build/Win64/VC8/Debug/xerces-c_3_1d.dll
			[dbxml_root]\xqilla\build\windows\VC8\x64\Debug/xqilla23d.dll
			
		Note that version numbers of these libraries may change.  The tool dependency walker
		can be used to open _dbxml.pyd and _bsddb.pyd to determine which libraries are
		needed:  http://www.dependencywalker.com/
			
		In addition, the Visual Studio debug runtime assembly:  Microsoft.VC90.DebugCRT
		is required for Visual Studio 2008.  The files located in:
		[VisualStudio_root]/VC/redist/Debug_NonRedist/amd64/Microsoft.VC90.DebugCRT
		must be copied to Python's site packages.  This may not be true with compilers
		later than Visual Studio 2008.

		If any of these libraries cannot be loaded, cryptic error messages such as an indicating that
		its side-by-side configuration is incorrct.  Side-by-side errors can be debugged by looking
		at the Windows event log for application SideBySide errors (event 33) and using Microsoft's
		side by side (sxstrace) tool.  
		
		Details on sxsstrace and sxs events can currently be found at:
		http://blogs.msdn.com/b/junfeng/archive/2006/04/14/576314.aspx
		
		

Egenix mxBase:  http://www.egenix.com/products/python
    Free Python modules for ISO time and other classes.  We mainly
    use the time functionality.  Note that these must correspond to
    the Python version.
    Egenix does not support Python 3 and is not used after Tethys 2.5

Cherrypy:  http://www.cherrypy.org/
	Cherrpy is a web framework for Python
	Downlaod the zip file and install as follows after extracting:
	cd into cherrypy directory where setup.py exists
	python setup.py install
    
PyODBC - http://code.google.com/p/pyodbc/
	Module permitting platform independent access to databases
	Comes with an installer, run it.
	
Running the server

	More detailed explanations can be found in the documentation.
	Short version:  
		edit tethys.bat for correct directory, then run
	
	dbXMLserver.py:  starts server with default SSL and transactions
	For now, we expect the directory testbed to exist, and the database
	is kept there.
	
	You may wish to set up the database as a service that automatically starts
	with Windows.  Create a .bat file which sets the current working directory
	appropriately (such that you can access the database) and starts the server.
	
	Here is our startup, tethys.bat:
	cd c:\Users\mroch\documents\eclipse\dbxmlServer\src
	dbXMLserver.py --secure-socket-layer false
	
	Then, from an account with administrative privileges, you can create
	a service, which is a program that is run automatically when Windows
	starts.  If you are using Windows Vista or later, you will need to start
	a command prompt with the Run as Administrator prompt that occurs when you
	right click on the command prompt in the start menu.
	
	You will need to use a wrapper program which makes the database look like
	a service.  We use the "non-sucking service manager" developed by
	Iain Patterson:   http://iain.cx/src/nssm/
	
	With nssm, use:
		nssm install service_name tethys.bat
		
	Tell Windows when to start Tethys:
	sc config service_name start= auto
	(For Windows Vista and later, delayed-auto is a better choice.  The service
	is not started until after Windows has initialized everything, making for
	a smoother startup.)
	
	To start manually:  sc start service_name	
	To remove the service permanently:  nssm remove service_name	

	Populating the database:
		tethys-reset.bat - Clear out existing database
		Run the following from a bash shell (cygwin)
		assumes source spreadsheets in source-docs:
			build_db.sh  # adds detection information
			cmd /c python client.py  # will add deployment information
			
		To do this on your own machine, add --server localhost
	

JAXB XML Java bindings
	To build Java bindings for XML schema with JAXB, make sure that 
	the Jax binding compiler (xjc) is on the system path.  This is
	part of the standard Java development kit. 
	
	Example where $JavaClientSrc maps to the source directory in the
	dbxmlJavaClient project and $Schema maps to the schema directory:
	
	example:
	JavaClientSrc=c:/Users/mroch/Documents/eclipse/dbxmlJavaClient/src
	Schema=c:/Users/mroch/Documents/eclipse/dbxmlServer/Tethys/metadata/lib/schema
	cd $Schema
	xjc -d $JavaClientSrc *.xsd 
	
	It looks like a jaxb.index file is required as well.  It can be
	generated as follows:
	cd $Schema
	[not sure this is right yet, it generates the class list, but not
	 necessarily in the right place]
	ls *.xsd | sed 's/\..*//' >$JavaClientSrc/jaxb.index
	
NOTE FOR Eclipse IDE Users
	We've used pydev for developing this project (http://pydev.org).  It's a
	very nice Eclipse perspective for Python.  We've occasionally seen when 
	moving from one machine to another that pydev marks many of the imports
	as errors when they are not.  If this happens, one solution is to right
	click on the project, go to the PyDev menu and remove the PyDev project
	configuration.  Afterwards, you can repeat the process, this time adding
	a PyDev project configuration.  This seems to clean things up.
	
	
	
	
