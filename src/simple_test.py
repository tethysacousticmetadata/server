'''
Created on Sep 22, 2009

@author: mroch
'''

import bsddb3.db
import dbxml
import socket
import sys
import pdb

xml_db = 'testbed'
if __name__ == '__main__':
    # Berkeley DB XML requires an environment which describes how a set of
    # one or more XML databases are to be accessed.
    environment = bsddb3.db.DBEnv()
    # open the environment
    environment.open(xml_db, 
                     # create if not already there
                     dbxml.DB_CREATE | 
                     # use locking to manage concurrency
                     dbxml.DB_INIT_LOCK | 
                     # start logging system for crash recovery
                     dbxml.DB_INIT_LOG | 
                     # provide cache for multi-threaded systems
                     dbxml.DB_INIT_MPOOL |
                     # use transactions 
                     dbxml.DB_INIT_TXN | 
                     # recover from crash if needed using transaction log 
                     bsddb3.db.DB_RECOVER,  
                     0)
    
    # databases.  
    manager = dbxml.XmlManager(environment, 
                               # tethys can query w/o explicitly opening/closing
                               # containers.
                               dbxml.DBXML_ALLOW_AUTO_OPEN |
                               # tethys can query external sources (e.g. URLs, files)
                               dbxml.DBXML_ALLOW_EXTERNAL_ACCESS)
    containerName = "testContainer.dbxml"
    container = None
    try:    
        container = manager.openContainer(containerName)
        print("opened %s"%(containerName))
    except (dbxml.XmlContainerExists, dbxml.XmlContainerNotFound) as e:
        print("Unable to find container or container exists: ")
        print(e)
    except Exception as e:
        print("other exception opening container")
        print(e)

    if container is None:
        try:
            print("creating container %s"%(containerName))
            container = manager.createContainer(containerName)
            print("created container %s"%(containerName))
        except Exception as e:
            print("unable to create container")
            print(e)
            sys.exit()
    

    # Run a query, fails first time when database empty, should
    # succeed on subsequent executions
    q_context = manager.createQueryContext()
    try:
        # Run the query
        queryRslt = manager.query("collection('%s')"%(containerName), q_context)
        # Convert result to a string
        result = "".join([item.asString() for item in queryRslt])
        print("Results: <%s>\n"%(result))
        del queryRslt
        del q_context
                
    except Exception as e:
        print("Query failed:")
        print(e)
    
    print("putting document")
    upd_context = manager.createUpdateContext()
    transaction = environment.txn_begin()
    xmltransaction = manager.createTransaction(transaction)
    try:
        container.putDocument(
            xmltransaction,
            "plants",         
            """
            <plants>
                <flowering> rose chrysanthemum daisy </flowering>
                <bromeliads> who-knows </bromeliads>
            </plants>
            """,
            upd_context,
            0)
        transaction.commit(0)
        # del transaction
    except Exception as e:
        print("Put failed:")
        print(e)
    del upd_context       
    
    # Repeat the query - should always succeed
    print("retrieve document")
    q_context = manager.createQueryContext()
    try:
        # Run the query
        queryRslt = manager.query("collection('%s')"%(containerName), q_context)
        # Convert result to a string
        result = "".join([item.asString() for item in queryRslt])
        print(result)
        del queryRslt
                
    except Exception as e:
        print("Query failed:")
        print(e)
    del q_context
        
    # all done, clean up
    del container
    del manager
    environment.close(0)
    
    
 
    
