# Python standard library modules
import datetime
import time
import math
import re

# Add-on modules from PyPI
import maya
import pendulum
import pandas as pd

class TimeError(Exception):
    def __init__(self, reason):
        self.message = reason
        
# For modifying maya times, see documentation on snap relative times
# from the splunks project
# https://docs.splunk.com/Documentation/Splunk/latest/SearchReference/SearchTimeModifiers#How_to_specify_relative_time_modifiers
# In general, for a MayaDT, use snap with
# @ target time element, e.g. h for hours
# +- offset
# For example,
# snap("@h+12h") returns a time 12 h later



class DateTime:
    """
    DateTime - Date and time representation
    Accepts dates, timestamps, and time offsets in a variety of formats
    including ISO 8601 (supported by packages maya and pendulum).

    Representation format is in ISO 8601 for date times.
    Supports Sum class method when all, or all but one of the DateTime instances
    are times or intervals.
    """

    # a second, used when constructing string representations
    one_s = datetime.timedelta(seconds=1)
    
    # Default date used to detect bad parses
    # bad day for Marie Antoinette of France...
    baddate = datetime.date(month=10, day=16, year=1793)

    @classmethod
    def combine(cls, dates):
        """
        Given an ISO date or datetime and a Time, combine into a DateTime
        :param dates:  List with one Date or DateTime and possibly time
        :return: DateTime
        """

        # Get the underlying representation if an instance of DateTime, otherwise
        # assume a class that we know how to work with.
        get_underlying = lambda t : t.timestamp if isinstance(t, DateTime) else t

        if len(dates) == 1:
            result = dates[0]
        elif len(dates) == 2:
            # Assume second instance is time or datetime where we
            # ignore the date (Excel represents times as hours past
            # the end of 1899
            converted = False
            try:
                if isinstance(dates[1], pd.Timestamp):
                    hhmm = dates[1]
                elif isinstance(dates[1], DateTime):
                    hhmm = pd.Timestamp(dates[1].timestamp)
                else:
                    hhmm = pd.Timestamp(dates[1])
                converted = True
            except TypeError as e:
                pass # try next...

            if converted:
                hhmm = hhmm - hhmm.floor("d")  # interval past midnight
                if isinstance(dates[0], DateTime):
                    result = dates[0].timestamp + hhmm
                else:
                    result = dates[0] + hhmm
            else:
                # Could not convert to an hhmm Timestamp, might be a time
                result = datetime.datetime.combine(dates[0], dates[1])
        else:
            raise ValueError("Cannot Combine >2 iso8601.DateTime")
        return result

    def __init__(self, src=None):
        """Initialize a date from one of the following sources:
            Current date and time (use no arguments)
            Microsoft Win32 API COM date
            datetime
            string
        """
        if src is not None:
            self.timestamp = self.to_date(src)
        else:
            self.timestamp = datetime.datetime.now()

    def __repr__(self):
        return '<Date object for "%s" at %x>' % (str(self.timestamp),id(self))
    
    def __add__(self, other):
        """
        Combine a date and a time
        :param other: date or time
        :return: DateTime
        """
        return DateTime.combine([self, other])

    def _to_int(self, src):
        "_to_int - Convert string to integer "
        if src is None:
            i = 0
        else:
            i = int(src)
        return i
    
    def _to_float(self, src):
        "_to_float - Convert string to floating point"
        if src is None:
            f = 0
        else:
            f = float(src)
        return f
        
    def to_date(self, src):
        if isinstance(src, datetime.datetime) or isinstance(src, datetime.date):
            timestamp = src
        elif isinstance(src, float):
            # Assume this is an Excel COM date relative to 1/1/1900
            # Note that Excel can also use a 1/1/1904 date which comes from
            # Excel on older MacOS systems that used that date as an epoch.
            # While Excel can still support that date, we do not expect to
            # see current workbooks using it.  For details, see:
            # https://docs.microsoft.com/en-us/office/troubleshoot/excel/1900-and-1904-date-system
            epoch = datetime.datetime(1899, 12, 31, 0, 0, 0)
            # 1900 was not a leap year, but Excel counts it as such
            # https://docs.microsoft.com/en-US/office/troubleshoot/excel/wrongly-assumes-1900-is-leap-year
            offset = -1 if src > 60 else 0
            # Must subtract 2 days from epoch
            timestamp = epoch + datetime.timedelta(days=src+offset)
        elif isinstance(src, str):
            # Try to determine to a day.  If relative to a day,
            # convert to a complete timestamp whether user passed in a
            # a full blown timestamp (date & time of day) or
            # if it is relative date
            timestamp = pendulum.parse(src, exact=True)
            # Convert to a naïve UTC date
            utc = pendulum.timezone('UTC')
            timestamp = utc.convert(timestamp).naive()
        else:
            raise TimeError(f"Unable to parse date: {src}")
        return timestamp

    def __str__(self):
        # format time in ISO 8601 format
        # We wish to preserve milliseconds, but need to account for
        # times when the selected precision rounds up to the next
        # minute.
        value = self.timestamp
        if isinstance(value, datetime.date) or isinstance(value, datetime.datetime):
            value = maya.parse(value)
        if isinstance(value, pendulum.Time):
            value_str = value.isoformat()
        else:
            value_str = value.iso8601()

    
        return value_str


if __name__ == "__main__": 
    # Debug tests     
    t = DateTime('2013-01-13')
    print(t)
    t2 = DateTime('12:24:00')
    print(t2)
    t3 = t + t2
    print("Should be sum of two timestamps")
    print(t3)
    print(DateTime.combine([t, t2]))


