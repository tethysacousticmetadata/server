
# This program will generate a schema for the integrated taxonomic
# information system (ITIS)
# It assumes a namespace and that a tsngroup has been defined in
# CommonElements.xsd

import sys

# Ranks are from:
# ITIS Data Model
# Entity Relationships and Element Definitions
# Revision 2.3, April 23, 2019
# We restrict ourselves to animals here
ranks = """
Kingdom
Subkingdom
Infrakingdom
Superphylum
Phylum
Subphylum
Infraphylum
Superclass
Class
Subclass
Infraclass
Superorder
Order
Suborder
Infraorder
Section
Subsection
Superfamily
Family
Subfamily
Tribe
Subtribe
Genus
Subgenus
Species
Subspecies
Variety
Form
Race
Strip
Morph
Aberration
"""
rank_list = ranks.split()

file_h = open("ITIS.xsd", 'w')

print(f"""<?xml version="1.0" encoding="UTF-8"?>
<xs:schema targetNamespace="http://tethys.sdsu.edu/schema/1.0"
  xmlns="http://tethys.sdsu.edu/schema/1.0" xmlns:xs="http://www.w3.org/2001/XMLSchema"
  elementFormDefault="unqualified">

  <xs:include schemaLocation="CommonElements.xsd"/>
 
  <xs:element name="ITIS">
    <xs:complexType>
      <xs:sequence>
        <xs:element name="{rank_list[0]}" type="{rank_list[0]}Type" maxOccurs="unbounded"/>
      </xs:sequence>
    </xs:complexType>
  </xs:element>
  
""", file=file_h)

# Produce the type hierarchy
for idx in range(len(rank_list)):
    typename = f'{rank_list[idx]}Type'
    print(f'<xs:complexType name="{typename}">', file=file_h)
    print("  <xs:sequence>", file=file_h)

    print(f'    <xs:group ref="tsngroup"/>', file=file_h)

    # subtypes of this rank
    print(f'    <xs:choice maxOccurs="unbounded" minOccurs="0">', file=file_h)
    for sub_idx in range(idx+1,len(rank_list)):
        subtypename = f'{rank_list[sub_idx]}Type'
        print(f'      <xs:element name="{rank_list[sub_idx]}" type="{subtypename}"/>', file=file_h)
    print(f'    </xs:choice>', file=file_h)

    print("  </xs:sequence>", file=file_h)
    print(f'</xs:complexType>', file=file_h)

print('</xs:schema>', file=file_h)

print('done')