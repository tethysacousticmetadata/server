import datetime
import os
import logging

# Add-on modules
import pandas as pd

# local modules
import resources


class Timer(object):
    def __init__(self):
        self.start = datetime.datetime.now()

    def get_start(self):
        "get_start() - Return start time"
        return self.start

    def elapsed(self):
        "elapsed() - Return timedelta since instantiation"
        return datetime.datetime.now() - self.start

    def elapsed_s(self):
        "elapsed() - Return seconds since instantiation"
        return self.elapsed().total_seconds()

    def elapsed_min(self):
        "elapsed() - Return minutes since instantiation"
        s_per_min = 60.0
        return self.elapsed().total_seconds() / s_per_min

    def __str__(self):
        "__str__() - Return start and elapsed time"
        s = self.elapsed().total_seconds()
        return f"started at: {self.start} elapsed {pd.Timedelta(seconds=s)}"

    def __repr__(self):
        "__repr__() - Return representation of timer"
        return f"timer {self.__str__()}"



class PerformanceMonitor(object):
    def __init__(self):
        
        logfmt = logging.Formatter("%(message)s")
        
        #timer vars
        self._end = None
        self._elapsed = None
        self._start = None
        
 
        #logger
        self.logger = logging.getLogger('benchmarker')
        self.logger.setLevel(logging.DEBUG)
        
        #send to sys.stderr
        console = logging.StreamHandler()
        console.setLevel(logging.DEBUG)
        console.setFormatter(logfmt)
        logging.getLogger('benchmarker').addHandler(console)


        # Construct rotating performance log
        maxBytes = 10000000
        maxCount = 5
        bench_path = os.path.join(resources.names['logs'],'performance')
        if not os.path.exists(bench_path):
            os.mkdir(bench_path)
        perfLogHandler = logging.handlers.RotatingFileHandler(
                os.path.join(bench_path, "performance.txt"), 'a',
                maxBytes, maxCount)
        perfLogHandler.setLevel(logging.DEBUG)
        perfLogHandler.setFormatter(logfmt)
        self.logger.addHandler(perfLogHandler)
        
     
    def start(self):
        """
        start() - Starts a timer
        :return: Timer, must be passed to stop (allows multiple nested timers)
        """
        return Timer()
        
    def stop(self, timer, operation="timed_operation", msg = None):
        """
        stop - Log the end of an event
        :param timer:  Timer instance returned by start()
        :param operation:  log name for operation, some names
                           perform special formatting for msg
        :param msg: Any msg printed into log
        :return: None
        """
        elapsed_s = timer.elapsed_s()
        s_per_min = 60
        perf = f"{operation} completed, {timer}"

        if operation == 'query':
            self.log_performance(f"{perf}\n____ query\n{msg}\n____\n")
        else:
            self.log_performance(perf)

    
    def __str__(self):
        return 'Enabled'
    def log_performance(self,msg):
        self.logger.debug(msg)
    
    @staticmethod
    def clear_logs():
        bench_path = os.path.join(resources.names['logs'],'performance')
        filelist = [ f for f in os.listdir(bench_path) if f.endswith(".txt") ]
        for f in filelist:
            os.remove(os.path.join(bench_path,f))
        return 'cleared'
    
        
        
def test():
    b = PerformanceMonitor()
    b.log_performance('hello')


if __name__ == '__main__':
    test()
        
        