import tethys

if __name__ == '__main__':
    print("dbxmlServer.py has been renamed to tethys.py, please update")
    print("any batch scripts that start Tethys.  dbxmlServer.py will be")
    print("removed in a future version of Tethys.")
    tethys.main()