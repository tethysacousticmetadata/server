'''
Created on Feb 6, 2010

Implemented argparse over deprecated optparse module
by sherbert 4/14/2016

@author: mroch
'''

import argparse
import os
import re
import pdb

import xml.dom.minidom
from xml.dom.minidom import Node

# user modules
import socket
import urllib.request, urllib.parse, urllib.error

# We need to determine the IP
# Use a web-based script to get the address if we are behind a router
checkip = "http://roch.sdsu.edu/report_client_ip.shtml?nocache=1"
    
localhost_ip = "127.0.0.1"

# The following are default addresses that can be overridden by
# the client or by populating a default_server.xml file which has
# the following format:
#
# <Server>
#  <Name>yourserver.yourdomain</Name>
#  <Port>9779</Port>
#  (: NAT_IP is only necessary when the server is connected to a switch
#     providing network address translation.  When this is true, the server
#     will have an IP in the private network range:  192.168.x.x or 10.x.x.x
#   :)
#  <NAT_IP>192.168.0.101</NAT_IP>
# </Server>

defaults = {
            "Name": "localhost",
            "NATIP": "192.168.0.101",
            "Port": 9779, 
            "SSL": False,
            "Transactional": True, # Never should be anything else
            "ServerType": "REST",
            "Resources": "C:\\Users\Tethys\metadata",
            "Recovery": True,
            "QueryCache": True,
            }

re_true = re.compile("[yYtT].*")
re_false = re.compile("[nNfF].*")

# callbacks for option parsing
def set_port(value):
    value = int(value)
    # Valid port number?
    if value >=1024 and value <= 65535:
        return value
    else:
        raise argparse.ArgumentError("Port must be between 1024-65535")
 
def true_false(value):
    # Determine if user passed in valid true/false argument
    boolean = None
    if re_true.match(value):
        boolean = True
    elif re_false.match(value):
        boolean = False
    else:
        # apparently they did not...
        raise argparse.ArgumentError(
            "option '%s' must be a boolean value (e.g. true/false"%(value))
    return boolean  # store option value
    
    
def get_url(server_addr, port, ssl=True):
    "Construct URL for communicating with server"
    
    if server_addr == "localhost":
        server_addr = localhost_ip

    # Construct http connection as plaintext or encrypted
    result = None
    if ssl == True:
        result = ("https://%s:%s"%(server_addr, port), "secure socket layer")
    else:
        result = ("http://%s:%s"%(server_addr, port), "plain text (UNSECURED)")
    return result

    
def __add_common_opts(parser):

    # Parse XML file if it exists to find defaults
    
    parentdir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    grandparentdir = os.path.dirname(parentdir)
    paramfile = os.path.join(grandparentdir, "ServerDefault.xml")

    xmldoc = None
    if os.path.exists(paramfile):
        try:
            xmldoc = xml.dom.minidom.parse(paramfile)
        except Exception:
            print(f"Unable to parse parameter file {paramfile}")
    else:
        print(f"No server configuration file at {paramfile} "
              "(not an error), using internal defaults")


    if xmldoc is not None:
        for n in ["Name", "IP", "NATIP", "Port"]:
            try:
                node = xmldoc.getElementsByTagName(n)[-1]
                value = "".join([t.data for t in node.childNodes 
                    if t.nodeType == xml.dom.minidom.Node.TEXT_NODE])
                defaults[n] = value
            except Exception:
                pass        
    
    parser.add_argument(
        "-s", "--secure-socket-layer", type=true_false,
        dest="secure_socket_layer", default=defaults["SSL"],
        help="Use encrypted communication (true/false)? "
        "encrypted-->https:// unencrypted-->http:// (default %(default)s)")
    parser.add_argument(
        "--port", type=set_port, default=defaults["Port"],
        help="port to run on (default=%(default)s)")
    
def add_server_opts(parser):
    __add_common_opts(parser)
    parser.add_argument(
        "-t", "--transactional", type=true_false,
        dest="transactional", default=defaults["Transactional"],
        help="Use transaction processing (default = %(default)s)?")
    parser.add_argument(
        "-d", "--database", type=str,
        action="store", default=None,
        help="Directory (folder) name where the XML database "
        "will be stored (must exist). Most users wishing to "
        "specify -d should probably use the -r switch instead.")
    parser.add_argument(
        "-r", "--resourcedir", type=str,action="store", default=defaults["Resources"],
        help="Set Tethys's resource directory (folder).  This is the parent "
             "directory for all data used by Tethys including the "
             "XML database.  ")
    parser.add_argument(
        "--recovery", type=true_false, dest="recovery",
        default=defaults["Recovery"],
        help="Open database temporarily in recovery mode.  "
             "If successful, recovers, closes, and reopens normally")
    parser.add_argument(
        "--querycache", type=true_false, dest="querycache",
        default=defaults["QueryCache"],
        help="Enable (true) or disable (False) cacheing "
             "of XML queries, may be changed once started")


def add_client_opts(parser):
    __add_common_opts(parser)

    # Determine where the server is running.  The default address
    # depends on whether our client is on the same NAT network
    # as the server or a different one.
    server_ip = None  # put in scope
    my_wan_ip = None
    
    try:
        server_ip = socket.gethostbyname(defaults["Name"])
    except Exception:
        print("Unable to resolve %s (default server) to IP address"%(defaults["Name"]))
    try:
        my_wan_ip = urllib.request.urlopen(checkip).read()
    except Exception:
        my_wan_ip = ""
        print("Unable to determine client wide area network IP")
        
    if server_ip == my_wan_ip:
        # On the same NAT network or same machine
        my_local_ip = socket.gethostbyname_ex(socket.gethostname())[2][0]
        if my_local_ip != my_wan_ip or my_local_ip == defaults["NATIP"]:
            # directly plugged into the Internet, or we are running on
            # the machine we expect the server to be on. 
            default_ip = (localhost_ip, "%s: local host"%(localhost_ip))
        else:
            default_ip = (defaults["NATIP"], 
                          "%s on local NAT network"%(defaults["NATIP"]))
    else:
        default_ip = (server_ip, "%s -> %s"%(defaults["Name"], server_ip))
        
    parser.add_argument(
        "--server", default=default_ip[0],
        help="Server name (default= %(default)s)")

