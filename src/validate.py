# verify
# Post creation tests for schema instances that cannot be verified
# via schema.

import bisect
import collections
import glob
import itertools
import os
import os.path
import re
import sys
import traceback
from typing import List
import urllib
import zipfile

# Berkeley dbxml modules
import dbxml

# add-on modules
import lxml.etree as ET
import pandas as pd

# local stuff
from XML.transform import map2elem
import dbxmlInterface.error
import resources


#Dictionary containing the root elements of each container
container_roots = {
    "Detections": "Detections",
    "Deployments": "Deployment",
    "Events": "Event",
    "Localizations": "Localize",
    "Ensembles": "Ensemble",
    "SourceMaps": "Mapping",
    "SpeciesAbbreviations": "Abbreviations",
    "ITIS": "ITIS",
    "ITIS_ranks": "ranks",
    "Calibrations": "Calibration",
}

def is_uri(uri: str) -> bool:
    """
    Given a potential URI determine if it is well-formed
    :param uri:
    :return: True if well-formed, else False
    """
    try:
        # We don't validate whether or not the URI functions, but we
        # do check to see if it can be parsed and has a valid protocol
        # (scheme) and network address (netloc).
        parsed = urllib.parse.urlparse(uri)
        valid = parsed.scheme != "" and parsed.netloc != ""
    except ValueError:
        valid = False

    return valid

def identify_malformed_uris(uri_list: List) -> List:
    """
    Filter a list of URIs to remove all well-formed URIs
    :param uri_list:
    :return:  list of malformed uris
    """

    # iterate over list, adding bad URIs
    malformed = []
    for uri in uri_list:
        if not is_uri(uri):
            malformed.append(uri)

    return malformed

def to_xqlist(iterable):
    """
    Convert an iterable to an XQuery list
    :param iterable: list, tuple
    :return: string (x1, x2, ..., xn)
    """

    if len(iterable) > 1:
        val = str(tuple(iterable))
    elif len(iterable) == 1:
        if isinstance(iterable, str):
            val = f'("{iterable[0]}")'
        else:
            val = f'({iterable[0]})'
    else:
        val = '()'
    return val

def binary_search(array, value, low=0, high=None):
    """
    Search for an item in a sorted array.
    array
    :param array: array to search
    :param value: search value
    :param low:  low bound
    :param high:  high bound
    :return: index such that array[index]==value if present, else -1
    """
    if high is None:
        high = len(array)
    idx = bisect.bisect_left(array, value, low, high)
    found = idx != high and array[idx] == value
    return idx if found else -1

class MediaValidator:
    def __init__(self, srcdir, basename, validator: 'Instance'):
        """

        :param srcdir:
        :param basename:
        :param validator:  validator Instance using this media validator
        """

        # Note where things are
        self.srcdir = srcdir
        self.basepath = os.path.join(srcdir, basename)
        self.doc_validator = validator

        # See if there are archives associated with this.
        self.types = ['Bespoke', 'Image', 'Audio', 'attach']
        self.archive_files = collections.defaultdict(dict)
        for t in self.types:
            # Data are either in a direcory or an archive
            directory = f"{self.basepath}-{t}"
            archive = f"{self.basepath}-{t}.zip"
            if os.path.isfile(archive):
                # Open the archive and parse the files contained within
                # attach.zip should have type/filename
                z = zipfile.ZipFile(archive)
                file_list = z.namelist()
                for f in file_list:
                    parts = os.path.split(f)
                    if len(parts) != 2:
                        self.doc_validator.note_problem(
                            self.doc_validator.warnings,
                            "Attachment",
                            f"Unexpected path in archive {archive}: {f}, " +
                            "should have type/filename")
                    else:
                        self.archive_files[f[0]][f[1]] = True
            elif os.path.isdir(directory):
                # Add in all the files associated with this type
                file_list = glob.glob(f"{directory}/*")
                for f in file_list:
                    self.archive_files[t][f] = True

    def exists(self, data_type, data_name):
        """
        Is media file data_type/data_name accessible
        :param data_type:
        :param data_name:
        :return:
        """
        return data_type in self.archive_files and \
                data_name in self.archive_files[data_type]


class Instance:
    """Generic in-memory XML document that can be queried"""
    
    
    def __init__(self, manager):
        """Create an in-memory instance that we can query/operate on
        manager - XmlManager instance
        xmldoc - XML document as a string
        source - Where the material came from
        """

        self.modified = False  # Have we modified the document?

        self.manager = manager
        # Access the low-level SWIG interface
        self.manager_proxy = manager.manager

        # Create an XmlDocument with the text passed in by the user
        self.doc = self.manager_proxy.createDocument()
        
        # Query engine requires an XmlValue and QueryContext
        self.value = dbxml.XmlValue(self.doc)
        self.qcontext = self.manager_proxy.createQueryContext()
        
        self.errors = {}
        self.warnings = {}
        self.media = {}
        
        # Used in all queries needing a namespace
        self.tethys_namespace = "http://tethys.sdsu.edu/schema/1.0"
        # Bind ty: to tethys namespace
        self.ns_ty = 'import schema namespace ty="' + \
            self.tethys_namespace + '" at "tethys.xsd";\n'
        # Set tethys namespace as default namespace
        self.ns_def = 'declare default element namespace "' + \
            self.tethys_namespace + '";\n'

        # function definitions from www.xqueryfunctions.com
        # used for checking OnEffort spIDs vs. Kind spIDs
        self.functx = """
        declare namespace functx = "http://www.functx.com";
         
         declare function functx:is-value-in-sequence
          ( $value as xs:anyAtomicType? ,
            $seq as xs:anyAtomicType* )  as xs:boolean {
        
           $value = $seq
         } ;
         """
        
        # Dictionaries that indicate whether or not a specific
        # instance document has had an error or warning as tracked
        # by the source name given to the document.
        self.source_errorP = {}
        self.source_warningP = {}
        
        self.source = None

        self.non_validated_collections = ['SourceMaps']
        self.valid_ns_attr = '{http://www.w3.org/2001/XMLSchema-instance}schemaLocation="http://tethys.sdsu.edu/schema/1.0 tethys.xsd"'

    def get_doc(self):
        """
        :return:  Return the document content
        """
        return self.doc.getContent()

    def get_nsdecl(self, container: str):
        """
        Return a default namespace declaration for a specific container
        :param container:
        :return:
        """
        if container not in container_roots or \
                container in ["SourceMaps", "Events"]:
            # unknown container or special container w/o namespace
            decl = ""
        else:
            decl = self.ns_def

        return decl

    def is_modified(self):
        """
        :return:  Was the dcoument modified during validation?
          If so, use get_doc() to retrieve the modified document
        """
        return self.modified

    def setId_ifneeded(self, id_value):
        """
        If Id is not the first child of the root element, add an Id node
        :param id_value: string which must be unique to the collection
        :return: None
        """

        #todo Set the /Id if not present
        pass
    def __del__(self):
        "Release any resources as we go out of scope"
        del self.qcontext
        del self.doc
        
    def query(self, query):
        """
        Query an in-memory XmlDocument, use . as root of paths
        Returns an XmlResults that the user is responsible for deleting

        :param query:  XQuery
        :return: XmlResults - CAVEAT:  invoke del on result when done
        """

        # compile the query
        q_expression = self.manager.manager.prepare(query, self.qcontext)
        
        results = q_expression.execute(self.value, self.qcontext)

        return results

    def query_str(self, query):
        """
        Query an in-memory XmlDocument, use . as root of paths
        Returns result as a string
        :param query:
        :return: str
        """

        results = self.query(query)  # execute query
        result_str = "".join([r.asString() for r in results])  # build result str
        del results  # release XmlResults

        return result_str

    def get_problems(self, clear=False, xml=None):
        """get_problems - Report current problems and warnings 
           clear - clear problem list for next report
           xml - Send report to specified XMLGenerator rather 
                 than returning string"""
        
        
        if xml:
            result = None # Nothing to return
            
            # Does the xml object support newline() ?
            supports_nl = callable(getattr(xml, 'newline', None))
            # Construct function nl() that calls newline if xml supports it,
            # otherwise does nothing
            nl = lambda : xml.newline() if supports_nl else None
            
            # Process warnings and errors
            for (problemcat, problems) in [("Warnings", self.warnings),  
                                     ("Errors", self.errors),
                                     ("Media", self.media)]:
                if len(problems) > 0:
                    if problemcat == "Media" and len(self.errors) > 0:
                        # Only provide media warnings if there were no errors
                        # As the client does not know what is contained in the
                        # detections, a two pass strategy is used.  In the first
                        # pass, we throw media errors which the client passes
                        # and then uploads.  If there were other errors, these
                        # will have occurred in the first pass and providing
                        # the media errors here will be misleading as the user
                        # does not typically see those errors if they are
                        # resolved in the second submission.
                        continue
                    nl()
                    xml.startElement(problemcat, {})
                    nl()
                    dbxmlInterface.error.dict2msgXML(problems, xml)
                    xml.endElement(problemcat)
                    nl()
        else:
            msgs = []
            # Process warnings and errors
            for (problemcat, problems) in [("Warnings", self.warnings),  
                                     ("Errors", self.errors),
                                     ("Media", self.media)]:
                if len(problems) > 0:
                    msgs.append("%s %s ----\n%s"%(
                        problemcat, self.source, 
                        dbxmlInterface.error.dict2msg(problems, 2)))
            
            if len(msgs) > 0:
                result = "Problems occurred parsing %s\n%s"%(self.source, "\n".join(msgs))
            else:
                result = ""
            
        return result  

    def has_errors(self):
        "has_errors - Have we encountered any errors?"
        return len(self.errors) != 0
    
    def has_warnings(self):
        "has_warnings - Have we encountered any warnings?"
        return len(self.warnings) != 0

    def source_has_errors(self, source):
        "has_errors(source) - Has this source encountered any errors?"
        return self.source_errorP[source]
    
    def source_has_warnings(self, source):
        "has_errors(source) - Has this source encountered any errors?"
        return self.source_warningP[source]
    
    def source_missing_media(self, source):
        "source_missing_media - Any missing media attachments?"
        # todo:  We do not have any way of currently checking for
        #        per document missing media.  May not be a problem
        #        as we are currently clearing the validator after
        #        processing each document.
        try:
            missing = len(self.media["Missing"]) > 0
        except KeyError:
            missing = False
        return missing
            
    def note_problem(self, problemdict, *args):
        # Report a problem
        # problemdict - appropriate dictionary: self.warnings, self.errors
        # *args - arguments describing error, see dbxmlInterface.error.addErrMsg.

        if problemdict is self.errors:
            self.source_errorP[self.source] = True
        elif problemdict is self.warnings:
            self.source_warningP[self.source] = True
        
        dbxmlInterface.error.addErrMsg(problemdict, *args)
    
    def merge_problems(self, problemdict, updatewithdict):
        """merge_problems(problemdict, updatewithdict
            Update problemdict with the entries from a second dictionary,
            updatewithdict.  It is *assumed* that no keys will collide. """
        
        if len(updatewithdict) > 0:
            if problemdict is self.errors :
                self.source_errorP[self.source] = True
            elif problemdict is self.warnings:
                self.source_warningP[self.source] = True
                
            problemdict.update(updatewithdict)
    
    def setRebuild(self,rebuild=True):
        """Sets a flag notifying whether or not this validation is for
        a container rebuild or a submission"""
        self.rebuild = rebuild

    def validate(self, xmldoc, source, container_name):
        """Validate document named source with content xmldoc
        Subclasses are expected to call this method, then implement specifics
        container is container name""" 

        # Real work is done by subclasses, here we set things
        # up and assume that the subclass will call this method
        self.doc.setContent(xmldoc)  # make current document
        self.source = source
        # No errors or warnings until we learn otherwise
        self.source_warningP[source] = False
        self.source_errorP[source] = False

        if container_name in container_roots:
            expected_root = container_roots[container_name]

            try:
                # First query to document will be slower as XML processor
                # must parse the document, even if the query is trivial
                exists = self.query_str(
                    self.get_nsdecl(container_name) +
                    f"exists(/{expected_root})")
                exists = exists.lower() == "true"
            except dbxml.XmlException as e:
                raise dbxmlInterface.error.QueryError(xmldoc, str(e))

            if not exists:
                self.note_problem(
                    self.errors,
                    f"Collection {container_name}",
                    f"Document root is not {expected_root}")

                # make sure there is xmlns:xsi or xsi:schemaLocation
                if container_name not in self.non_validated_collections:
                    # get namespace attributes from root element
                    doc_ns_attr = [r.asString() for r in self.query(
                        self.ns_def + "\n" + self.ns_ty + "\n ./" + container_name + "/@*")]
                    # if no namespace attribute or they are not accurate, throw error
                    if len(doc_ns_attr) == 0 or doc_ns_attr[
                        0] != self.valid_ns_attr:
                        self.note_problem(
                            self.errors,
                            "Collection",
                            "Namespace schema instance and/or location " +
                            f"was not provided, expected '{self.valid_ns_attr}'")

        else:
            self.note_problem(
                self.errors,
                "Collection",
                f"{container_name} not a known collection to be validated")


class Detections(Instance):
    def __init__(self, manager):
        "In-memory instance of a Detections instance - see Instance for arguments"
        super().__init__(manager)
        self.rebuild = False

    def validate(self, xmldoc, source, container_name, ignoreMedia=False):
        """
        Valdiation a detections document
        :param xmldoc: detection document
        :param source: source information for detections
        :param container_name: container to which document will be added
        :param ignoreMedia: Validate or ignore that additional media
          specified in the document exists
        :return:
        """

        Instance.validate(self, xmldoc, source, container_name)

        missing = "Unable to find %s element, verify that it exists " + \
                  f"in namespace {self.tethys_namespace}"

        # Verify referenced DataSource exists
        # Check DeploymentId
        depid = self.query_str(
            self.ns_def + "/Detections/DataSource/DeploymentId/text()")
        if depid != "":
            r = self.manager.query(
                self.ns_def +
                f'exists(collection("Deployments")/Deployment[Id="{depid}"])')
            if not bool(r):
                self.note_problem(self.errors, "DeploymentId",
                                  f'Deployment Id="{depid} does not exist')
        else:
            # No DeploymentId, check EnsembleId
            ensid = self.query_str(
                self.ns_def + "/Detections/DataSource/EnsembleId/text()")
            if ensid != "":
                r = self.manager.query(
                    self.ns_def +
                    f'exists(collection("Ensembles")/Ensemble[Id="{ensid}"])')
                if not bool(r):
                    self.note_problem(self.errors, "EnsembleId",
                                      f'Ensemble Id="{ensid} does not exist')
            else:
                self.note_problem(self.errors, "DataSource",
                                  "Must specify a DeploymentId or EnsembleId")

        # Verify effort is present
        result = self.query(self.ns_def + "./Detections/Effort/Start/text()")
        start = [r.asString() for r in result]
        del result
        
        if len(start) > 0:
            start = start[0]
        else:
            self.note_problem(self.errors, "Time",
                              "EffortStart",
                              missing % "EffortStart" +
                              " and contains a properly formatted datetime")
        result = self.query(self.ns_ty + """
        ./ty:Detections/ty:Effort/ty:End/text()""")
        end = [r.asString() for r in result]
        del result

        # Verify that binned detections have a BinSize_min attribute
        result = self.query(self.ns_ty + """
        ./ty:Detections/ty:Effort/ty:Kind[ty:Granularity = "binned" 
            and not(exists(ty:Granularity/@BinSize_min))]""")
        result_str = [r.asString() for r in result]
        del result
        if len(result_str) > 0:
            self.note_problem(
                self.errors, "Granularity",
                "Effort/Kind/Granularity has been specified without " 
                "specifying BinSize_min attribute")

        if len(end) > 0:
            end = end[0]
        else:
            self.note_problem(self.errors, "Time",
                              "EffortEnd",
                              missing % "EffortEnd" +
                              " and contains a properly formatted datetime")

        # Verify detections in /OnEffort/ have TSNs that
        # are listed in Effort/Kind
        xq = self.ns_def + self.functx + """
            let $onEff := distinct-values(./Detections/OnEffort/Detection/SpeciesId)
            let $kinds:= distinct-values(./Detections/Effort/Kind/SpeciesId)
            
            (:loop through distinct oneffort spp :)
            
            let $inKind := 
                for $sp in $onEff 
                    return  if (not(functx:is-value-in-sequence($sp,$kinds)))
                            then data($sp)
                            else() (:good, don't care :)
                
            return distinct-values($inKind)
            """
        result = self.query(xq)
        missing_tsn = [int(r.asNumber()) for r in result]
        del result
        
        # To do, look up TSN and provide Latin name or name in species map
        # if available.
        if len(missing_tsn) > 0:
            xq = self.ns_def + """
                let $kinds:= ./Detections/Effort/Kind/SpeciesId
                return distinct-values($kinds)
            """
            result = self.query(xq)
            avail_tsn = [int(r.asNumber()) for r in result]
            del result

            # Pull in the Latin names for the species in question.  We
            # add a dummy field to the results as pd.read_xml which is
            # used to extract the data does not process nodes without
            # siblings correctly.
            result_str = self.manager.query(self.ns_ty + f"""
               let $detections := {to_xqlist(missing_tsn)}
               let $kinds := {to_xqlist(avail_tsn)}""" + """
               return
                  <EffortViolation> {
                    for $k in $kinds return
                       <effort> 
                         <completename>
                         {collection("ITIS_ranks")/ty:ranks/ty:rank[ty:tsn=$k]/ty:completename/text()}
                         </completename>
                       </effort>
                    } {
                    for $d in $detections return
                        <detected>
                           <completename>
                           {collection("ITIS_ranks")/ty:ranks/ty:rank[ty:tsn=$d]/ty:completename/text()}
                           </completename>
                        </detected>
                    }
                   </EffortViolation>
            """)
            result_tree = ET.fromstring(result_str)
            effort_names = result_tree.xpath("effort/completename/text()")
            detection_names = result_tree.xpath("detected/completename/text()")
            self.note_problem(
                self.errors, "EffortViolation",
                'OnEffort detections for: " '
                ", ".join(detection_names) +
                f'. Specified effort: {", ".join(effort_names)}. '
                '(Species shown in Latin regardless of original coding.)')

        # Do the same for the values of the Group attribute
        xq = self.ns_def + self.functx + """
            let $kinds:= distinct-values(./Detections/Effort/Kind/SpeciesId/@Group)
            let $onEffgrp := distinct-values(./Detections/OnEffort/Detection/SpeciesId/@Group)
        
            (:loop through distinct oneffort spp :)
            
            let $inKind := 
                for $grp in $onEffgrp 
                    return  if (not(functx:is-value-in-sequence($grp,$kinds)))
                            then data($grp)
                            else() (:good, don't care :)
                
            return distinct-values($inKind)
        """
        result = self.query(xq)
        missing_group = [r.asString() for r in result]
        del result
        
        if len(missing_group) > 0:
            self.note_problem(
                self.errors, "EffortViolation",
                "Groups with detections that were not specified in effort " +
                f"{missing_group}")

        # Verify that detections that should have End times do indeed have them
        result = self.query(self.ns_def + """
         for $k at $ecount in ./Detections/Effort/Kind
             (: only binned detections may have an empty End time :)
         where $k/Granularity = "encounter"
         return
          for $d at $count in ./Detections/OnEffort/Detection[
           not(exists(End)) and SpeciesId = $k/SpeciesId and 
             Call = $k/Call and 
             (not(exists($k/Parameters/Subtype)) or
              Parameters/Subtype = $k/Parameters/Subtype)]
          return
            $count""")
        missingend = [int(r.asNumber()) for r in result]
        del result
        
        if len(missingend) > 0:
            self.note_problem(self.errors, "Time", "EndMissing", 
                     "OnEffort", "SequenceNumbers", missingend)
        
        # OffEffort End times must be after start times
        result = self.query(self.ns_def + """
        for $d at $count in ./Detections/OffEffort/Detection[Start > End]
            return ($count, $d/Start)
        """)
        indexes = []
        starts = []
        for r in result:
            try:
                starts.append(r.getFirstChild().asString())
            except:
                indexes.append(int(r.asNumber()))
        endbeforestart = indexes
        del result
        
        if len(endbeforestart) > 0:
            self.note_problem(self.errors, "Time", "StartAfterEnd", 
                          "OffEffort", "SequenceNumbers", endbeforestart, "Start_Time", starts)
        
        # Detections must be within specified effort

        # todo:  Sort OnEffort/OffEfort detections
        # See code in XmlMethods iter_docs that is currently turned off


        queryStr = self.ns_def + """
        let $effstart := ./Detections/Effort/Start
        let $effend := ./Detections/Effort/End
        for $d in ./Detections/OnEffort/Detection[Start < $effstart or End > $effend or Start > End]
            return if ($d/Start > $d/End)
            then 
               <DetectionTimeViolation>
                 {concat ($d/Start/text(), " after ", $d/End/text())}
               </DetectionTimeViolation>
            else 
               <EffortViolation>
                 {concat (Start, " or ", End, " outside effort")}
               </EffortViolation> 
        """
        time_check = self.query_str(queryStr)
        if time_check != "":
            self.note_problem(
                self.errors,
                "Time",
                "OnEffort detection Start after End or outside of Effort\n" +
                time_check
            )

        queryStr = self.ns_def + """
        for $d in ./Detections/OffEffort/Detection[Start > End]
            return 
              <DetectionTimeViolation>
                {concat ($d/Start/text(), " after ", $d/End/text())}
              </DetectionTimeViolation>
        """
        time_check = self.query_str(queryStr)
        if time_check != "":
            self.note_problem(
                self.errors,
                "Time",
                "OffEffort detection Start after End\n" + time_check
            )


        if not ignoreMedia and not self.rebuild:
            # The validation step does not validate additional media,
            # but it does mark needed media as missing and it is up to
            # caller to verify that these have been satisfied in the
            # document upload.

            # A special case occurs when a user is rebuilding the database.
            # In this situation, the media documents are expected to already
            # be present in the file system, and we do not verify that they
            # are intact.
            if self.rebuild:
                srcdoc_path = os.path.join(
                    resources.names['source-docs'], "Detections")
                media_val = MediaValidator(srcdoc_path, source, self)
            else:
                media_val = None

            # Handle bespoke data URIs, invalid URIs expected to be attachments
            bespoke_uri_query = self.ns_def + """
            for $uri in ./Detections/BespokeData/Data/Uri
              return $uri/text()
            """
            bespoke = self.query_str(bespoke_uri_query)
            expected_files = identify_malformed_uris(bespoke.split())
            self.process_required_attachments(
                "Bespoke", expected_files, media_val)


            # Note the name of all media files.  They must be uploaded
            image = self.query(self.ns_def + """
            for $eff in (./Detections/OnEffort, ./Detections/OffEffort), 
               $d in $eff/Detection/Image
                  return $d/text()
            """)

            # Gather the image files,
            none_re = re.compile("none", re.IGNORECASE)
            imagefiles = [r.asString() for r in image
                          if not none_re.match(r.asString())]
            del image
            self.process_required_attachments("Image", imagefiles, media_val)

            audio = self.query(self.ns_def + """
            for $eff in (./Detections/OnEffort, ./Detections/OffEffort),
              $d in $eff/Detection/Audio
                return $d/text()
            """)
            audiofiles = [r.asString() for r in audio
                          if not none_re.match(r.asString())]
            del audio
            self.process_required_attachments("Audio", audiofiles, media_val)

    def process_required_attachments(self, atype:  str, attachments: List[str], media_val: MediaValidator=None):
        """
        Process a list of additional media of a specific type (e.g. Audio)
        that is required by this XML document.  We note each item that is
        required as a Missing file (it may be satisfied later) and note
        any duplicate entries as warnings.
        :param atype: Type of attachment
        :param attachments:  List of attachment names
        :param media_val:  Optional MediaValidator.  This is only used
           when processing attachments for a document that has already
           been submitted and we are simply verifying that the documents
           that are supposed to be on disk actually are.  This is typically
           done when rebuilding the database from submitted documents.
        :return: None
        """

        if len(attachments) > 0:
            # Use the Counter class to count # of each attachment
            itemcounts = collections.Counter(attachments)
            # filter for items in itemcounts that occur more than once
            duplicates = [item for item, count in list(itemcounts.items()) 
                          if count > 1]
            required = list(itemcounts.keys())

            self.note_problem(self.media, "Missing", atype, required)
            if len(duplicates) > 0:
                self.note_problem(self.media, "Warnings", "DuplicateReference",
                                  atype, "File", duplicates)

            if media_val is not None:
                # Check if required items are on disk
                if 'Missing' in self.media and atype in self.media['Missing']:
                    # iterate over copy of list to modify the original
                    for fname in list(self.media['Missing'][atype]):
                        if media_val.exists(atype, fname):
                            # Present, remove from missing
                            self.media['Missing'][atype].remove(fname)
                    # Remove the attachment type from Missing if
                    # there is nothing left
                    if len(self.media["Missing"][atype]) == 0:
                            del self.media["Missing"][atype]

class Deployment(Instance):
    def __init__(self,manager):
        "In-memory instance of a Deployment instance - see Instance for arguments"
        Instance.__init__(self, manager)
        # Used in all queries needing a namespace
        self.prolog = 'import schema namespace ty="http://tethys.sdsu.edu/schema/1.0" at "tethys.xsd";'
        self.rebuild = False
        
    def validate(self, xmldoc, source):
        """
        Given a Deployment document
        :param xmldoc: XML deployment document
        :param source: description of source
        :return: None
        """
        
        Instance.validate(self, xmldoc, source)
        
        # Get some Deployment Information
        result = self.query(self.ns_def + """
        let $project := ./Deployment/Project/text()
        let $id := ./Deployment/DeploymentId/text()
        let $site := ./Deployment/Site/text()
        
        let $result :=($project,$id,$site)
        return $result
        """)
        info = [r.asString() for r in result]
        del result
        
        dep_info = "Deployment {0[0]}{0[1]}{0[2]}".format(info)
        
        # Verify sensors
        result = self.query(self.ns_def + """
        let $chsensors := distinct-values(./Deployment/SamplingDetails/Channel/SensorNumber)
        let $sensors := distinct-values(./Deployment/Sensors/Audio/Number)
        return
            for $s at $count in distinct-values($chsensors[not(.=$sensors)])
                return 
                if ($count > 0)
                  then $s
                  else $count
                
        """)
        unmatched = [int(r.asNumber()) for r in result]
        del result
        
        if len(unmatched) > 0:
            self.note_problem(self.errors, dep_info, "Channel",
                              "MissingAudioSensor",
                              "SensorNumber",
                              unmatched)
            
            
        #verify sampling regimen time order
        result = self.query(self.ns_def + """
        let $channels := ./Deployment/SamplingDetails/Channel
        return
            for $channel at $chcount in $channels
                let $times := $channel/Sampling/Regimen/TimeStamp
                let $timecount := count($times)
                return
                    for $stamp at $count in (1 to ($timecount - 1))
                    where $times[($stamp + 1)] < $times[$stamp]
                    return $chcount
                    (: return (<Chnl>{$chcount}</Chnl>,<Stamp>{$count}</Stamp>) :)
        """)
        outoforder = [int(r.asNumber()) for r in result]
        del result
        
        if len(outoforder)>0:
            channel_list = [(ch,count) for ch,count in list(collections.Counter(outoforder).items())]
            for pair in channel_list:
                self.note_problem(self.errors, dep_info,
                                  "SamplingDetails", "UnsortedRegimenTimes",
                                  "Channel "+str(pair[0]), "Count",
                                  str(pair[1]))

class SourceMap(Instance):

    def __init__(self, manager):
        """
        In-memory instance of a SourceMap instance - see Instance for arguments
        :param manager:
        """
        super().__init__(manager)
        self.rebuild = False

    def validate(self, xmldoc, docname, container_name):
        """
        Simple validation tests on a SourceMap
        :param xmldoc:  XML specification of <SourceMap> document
        :param docname:  Name that will be associated with the document
        :param container_name:  Name of source container (collection)
        :return:
        """
        Instance.validate(self, xmldoc, docname, container_name)

        # Retrieve information about document
        # Note that sourcemaps are not in the Tethys namespace, so we
        # do not prepend the default namespace declaration.
        result = self.query("""
                let $name := ./Mapping/Name/text()
                let $directives := exists(./Mapping/Directives)

                let $result :=($name,$directives)
                return $result
                """)
        info = [r.asString() for r in result]
        del result

        srcname = info[0]
        if len(info) < 2 or info[1] != "true":
            self.note_problem(self.errors, "Directives",
                              "No directives were provided.")

        if srcname == "":
            self.note_problem(self.errors, "Name",
                              "No source map Name provided.")
        else:
            # See if map name already exists
            # Request the basename (submitted name of each document with
            # the Mapping/Name that is being submitted.
            result = self.query(f"""
              for $name in collection("SourceMaps")/Mapping/Name[.="{srcname}"]
              return base-uri($name)
            """)

            info = [r.asString() for r in result]
            basenames = [i.split("/")[-1] for i in info]
            del result
            if len(basenames) > 0:
                if len(basenames) > 1:
                    self.note_problem(
                        self.errors, "Mapping",
                        "More thane one document with Mapping/Name exists: "
                        + basenames.join(", ") +
                        ". Delete all but one of these documents.")
                elif basenames[0] != docname:
                    self.note_problem(
                        self.errors, "Mapping",
                        f"Document {basenames[0]} already has Mapping/Name="
                        f'"{srcname}".  Rename {docname}->{basenames[0]} '
                        f"and overwrite or remove {basenames[0]}."
                    )


class Attachments:
    def __init__(self):
        """Create a new attachments object"""
        self.types = dict()
        self.verified = dict()
        self.errors = dict()
        self.warnings = dict()

    def add(self, attachments):
        """
        Add attachments to be verified.
        :param attachments: list of named tuples.  Expects the following names:
           filename - Name of file
           content_type - media type (aka multipurpose Internet mail extension, or MIME type)
               content_type must have a .value
        :return:  None
        """

        # Access the category, create if needed
        if attachments is not None and not isinstance(attachments, list):
            attachments = [attachments]
        try:
            for a in attachments:
    
                try:
                    fname = a.filename
                except AttributeError:
                    self.note_problem(self.errors, "Attach", "FilenameMissing")
                    continue
                
                try:
                    mimetype = a.content_type.value  # Get mime type
                except AttributeError:
                    self.note_problem(self.errors, "Attach", "MimeTypeMissing", [fname])
                    continue
                
                mimetypes = mimetype.split("/")
                category = mimetypes[0].capitalize()
                
                # Get/create file dictionary for this mimetype
                if not category in self.types:
                    self.types[category] = dict()
                catdict = self.types[category]

                if fname in catdict:
                    self.note_problem(self.warnings, "Attachment",
                        "Duplicate", "File", [fname])
                    continue
                else:
                    # Add the attachment to the dict for this type
                    catdict[fname] = a
        except Exception as e:
            tb = sys.exc_info()[2]
            traceback.print_tb(tb)
            del tb
            pass
        # Attachments haven't been verified yet, but set 
        # up the keys that we'll be using.
        for k in list(self.types.keys()):
            self.verified[k] = dict()
        
    def note_problem(self, problem, *args):
        """
        Record that a problem occurred
        :param problem: dict that will record the issuer
        :param args: list of arguments describing the problem
        :return: None
        """

        dbxmlInterface.error.addErrMsg(problem, *args)
            
    def get_problems(self, clear=False, xml=None):
        """get_problems - Report current problems and warnings 
           clear - clear problem list for next report
           xml - Send report to specified XMLGenerator rather 
                 than returning string"""
        
        
        if xml:
            result = None # Nothing to return
            
            # Does the xml object support newline() ?
            supports_nl = callable(getattr(xml, 'newline', None))
            # Construct function nl() that calls newline if xml supports it,
            # otherwise does nothing
            nl = lambda : xml.newline() if supports_nl else None
            
            # Process warnings and errors
            for (problemcat, problems) in [("Warnings", self.warnings),  
                                     ("Errors", self.errors)
                                     ]:
                if len(problems) > 0:
                    xml.startElement(problemcat, {})
                    nl()
                    dbxmlInterface.error.dict2msgXML(problems, xml)
                    xml.endElement(problemcat)
                    nl()
        else:
            msgs = []
            # Process warnings and errors
            for (problemcat, problems) in [("Warnings", self.warnings),  
                                     ("Errors", self.errors)]:
                if len(problems) > 0:
                    msgs.append("%s %s ----\n%s"%(
                        problemcat, self.source, 
                        dbxmlInterface.error.dict2msg(problems, 2)))
            
            if len(msgs) > 0:
                result = "Problems occurred parsing %s\n%s"%(self.source, "\n".join(msgs))
            else:
                result = ""
            
        return result  

    
    def has_problems(self):
        "has_problems - Any problems at all?"    
        return self.has_errors() or self.has_warnings()
    
    def has_errors(self):
        "has_problems - Have we encountered any "
        return len(self.errors) > 0
    
    def has_warnings(self):
        "has_warnings - Have we encountered any warnings?"
        return len(self.warnings) > 0

    def validate_attachments(self, valobj):
        """
        Given a validation object with a set of required MIME objects,
        make sure they were all attached.
        :param valobj:  validation object.
        :return:
        """
        """"""
           
        # Verify everything that was expected is present.
        # The media attribute of the validation object should contain a
        # dictionary.  If Missing is a key, we will examine the attachments
        # to see if they satisfy the files
        if 'Missing' in valobj.media:
            # Look for attachments that we expected

            # Check to see if a zip archive has been attached.  If so,
            # we expect one or more archives that start with the document
            # name and end with -attach.zip
            if 'Application' in self.types:
                appl_files = self.types['Application'].keys()
                archive_re = re.compile(
                    valobj.source + r"(-(?P<type>.*))?-attach.zip",
                    re.IGNORECASE)

                zip_archives = [f for f in appl_files if archive_re.match(f)]
                if len(zip_archives):
                    for z in zip_archives:
                        # Retrieve the contents of the zip file. Strip off
                        # any path elements and sort for efficient search
                        tmp = zipfile.ZipFile(self.types['Application'][z].file)
                        arc_files = tmp.namelist()
                        arc_basenames = [os.path.basename(f) for f in arc_files]
                        arc_basenames.sort()
                        archive_used = False

                        # Check each type of media within this archive
                        for media_type in valobj.media['Missing'].keys():
                            # Get list of files missing for this media type
                            filelist = valobj.media['Missing'][media_type]
                            # Create a new list that contains the missing
                            # files not satisfied by this archive
                            not_found = []
                            for f in filelist:
                                basename = os.path.basename(f)
                                if binary_search(arc_basenames, basename) == -1:
                                    not_found.append(f)
                                else:
                                    archive_used = True  # something matched
                            valobj.media['Missing'][media_type] = not_found
                        if archive_used:
                            self.verified['Application'][z] = self.types['Application'][z]

            for key in ('Image', 'Audio'):
                if key in self.types:
                    files = self.types[key]
                    if len(files):
                        self.verified[key] = dict()
                        file_set = set([files[key].filename for key in files.keys()])
                        miss_set = set(valobj.media['Missing'][key])
                        remaining = set(valobj.media['Missing'][key]) - file_set
                        present_set = file_set.union(miss_set)
                        for item in present_set:
                            self.verified[key][item] = \
                                self.types[key][item]
                        valobj.media['Missing'][key] = list(remaining)

            # Remove Missing lists that have been satisfied
            for media_type in list(valobj.media['Missing'].keys()):
                if len(valobj.media['Missing'][media_type]) == 0:
                    del valobj.media['Missing'][media_type]
            if len(valobj.media['Missing']) == 0:
                del valobj.media['Missing']

            # If there were any warnings, copy them to the validator
            # object.  We assume that there will be not key collisions
            # perhaps not... valobj.warnings.update(self.warnings)
            valobj.merge_problems(valobj.errors, self.errors)
            valobj.merge_problems(valobj.warnings, self.warnings)

        return
 
class SpeciesAbbreviations(Instance):
    def __init__(self, manager):
        "In-memory instance of a Deployment instance - see Instance for arguments"
        super().__init__(manager)
        # Used in all queries needing a namespace
        self.prolog = 'import schema namespace ty="http://tethys.sdsu.edu/schema/1.0" at "tethys.xsd";'

    def validate(self, xmldoc, source, container_name):
        """
        Validate a species abbreviation document
        :param xmldoc: species abbreviation XML specification
        :param source: document source
        :param container_name: XML document collection name
        :return:  None.  Has side effect of potentially setting errors/warnings
        """
        super().validate(xmldoc, source, container_name)

        # Get some Deployment Information
        result = self.query(self.ns_def + """
        let $latin := ./Abbreviations/Map/completename/text()
        return $latin
        """)
        # List of Latin species names
        latin = [r.asString() for r in result]
        del result

        # Make sure that there are TSNs for all of them
        # Query the database instead of the document
        latin_set = set(latin)
        latin = list(latin_set)
        if len(latin) > 1:
            names = f'{tuple(latin)}'
        else:
            names = latin

        # Run query on database
        result = self.manager.query(self.ns_def + f"""
<result xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
{{
  for $r in collection("ITIS_ranks")/ranks/rank[completename={names}]
  return
    <rank>
      {{$r/tsn}},
      {{$r/completename}}
    </rank>
}}
</result>
""", True)
        try:
            mappings = pd.read_xml(result, xpath="/result/rank")
        except ValueError:
            self.note_problem(
                self.errors, "Unable to retrieve TSNs based on the " +
                "Latin species names in the abbreviations document. ")
            return

        # Did we find all Latin names?
        missing = latin_set.difference(mappings.completename)
        if len(missing):
            self.note_problem(
                self.errors, "Unknown Latin name",
                "The following Latin names are not in the ITIS collection:  \n" +
                "\n".join([f'"{m}"' for m in missing]))
        else:
            # Update internal document
            # Note {{ escapes { in Python format string (f-string)
            update = self.query(f"""
            {self.ns_def}
            let $tsns := {result}
            return
               for $map in ./Abbreviations/Map
               let $rank := $tsns/rank[completename = $map/completename]
               return
 
                insert nodes
                   element 
                     {{QName("http://tethys.sdsu.edu/schema/1.0", "tsn")}}
                     {{$rank/tsn/text()}}
                   
                   after $map/completename
            """)
            self.modified = True










        

        
