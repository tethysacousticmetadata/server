""" This modules provides a lightweight API to access Excel data.
    There are many ways to read Excel data, including ODBC. This module
    uses ADODB and has the advantage of only requiring a file name and a
    sheet name (no setup required).
    
    from ActiveState Recipes by Nicolas Lehuen
    Recipe 440661: Read tabular data from Excel spreadsheets the fast 
    and easy way 
    
    Note on dates:  
    Excel serial dates are stored in s relative to Jan 1 1900.
    Matlab serial dates are stored in s relative to Jan 0, 0.
    According to Mathworks, one can convert from Excel serial dates
    to Matlab serial dates with the Matlab command:
        datenum('30 Dec 1899') + ExcelDates
    
    
    Contains local modifications by Sean Herbert & Marie Roch
    to address 32/64 bit issues.
"""

import os
import win32com.client
import pythoncom
import platform
import pdb



class ExcelDocument(object):
    """ Represents an opened Excel document.
    """

    def __init__(self,filename):
        # Determine Excel version
        (name, ext) = os.path.splitext(filename)
        
        if ext == ".xls":
            version = "Excel 8.0"
        elif ext == ".xlsx":
            version = "Excel 12.0 Xml"
        elif ext == ".xlsm":
            version = "Excel 12.0 Macro"
        else:
            raise ValueError("Unknown Microsoft Excel version")
                
        self.connection = win32com.client.Dispatch('ADODB.Connection')
        
        providers = ['Microsoft.ACE.OLEDB.12.0', 'Microsoft.Jet.OLEDB.4.0']
        connected = False
        idx = 0
        connectionStrings = []
        while not connected and idx < len(providers):
            connectionStrings.append("".join([
                "PROVIDER=%s;"%(providers[idx]),
                "DATA SOURCE='%s';"%(filename),
                'Extended Properties="%s;HDR=1;IMEX=1"'%(version)]))
            try:
                # Attempt to open a connection to our most recent source
                self.connection.Open(connectionStrings[-1])
                connected = True  
            except pythoncom.com_error as e:
                idx = idx + 1
                
        if not connected:
            arch = platform.architecture()
            raise RuntimeError(
                "\n".join(["Unable to connect to Excel.", 
                    "Is the Excel driver installed for this %s system?"%(arch[0]),
                    "Tried:  \n\t",
                    " and\n\t".join(connectionStrings)]))
        
            
                

    def sheets(self):
        """ Returns a list of the name of the sheets found in the document.
        """
        result = []
        recordset = self.connection.OpenSchema(20)
        while not recordset.EOF:
            result.append(recordset.Fields[2].Value)
            recordset.MoveNext()
        recordset.Close()
        del recordset
        return result

    def sheet(self,name,encoding=None,order_by=None):
        """ Returns a sheet object by name. Use sheets() to obtain a list of
            valid names. encoding is a character encoding name which is used
            to encode the unicode strings returned by Excel, so that you get
            plain Python strings.
        """
        return ExcelSheet(self,name,encoding,order_by)
            
    def __del__(self):
        self.close()

    def close(self):
        """ Closes the Excel document. It is automatically called when
            the object is deleted.
        """
        try:
            self.connection.Close()
            del self.connection
        except:
            pass

def strip(value):
    """ Strip the input value if it is a string and returns None
        if it had only whitespaces """
    if isinstance(value,str):
        value = value.strip()
        if len(value)==0:
            return None
    return value

class ExcelSheet(object):
    """ Represents an Excel sheet from a document, gives methods to obtain
        column names and iterate on its content.
    """
    def __init__(self,document,name,encoding,order_by):
        self.document = document
        self.name = name
        self.order_by = order_by
        if encoding:
            def encoder(value):
                if isinstance(value,str):
                    value = value.strip()
                    if len(value)==0:
                        return None
                    else:
                        return value.encode(encoding)
                elif isinstance(value,str):
                    value = value.strip()
                    if len(value)==0:
                        return None
                    else:
                        return value 
                else:
                    return value
            self.encoding = encoder
        else:
            self.encoding = strip
                
    def __iter__(self):
        """ Returns a paged iterator by default. See paged().
        """
        return ExcelPagedQuery(self.name, self.order_by, self.document.connection)

class ExcelPagedQuery:
    def __init__(self, sheet_name, order_by, connection, pagesize=128):
        """

        :param sheet_name:
        :param order_by:
        :param connection:
        :param pagesize:  Record count to fetch each time.
            Fetching multiple records each time lowers overhead significantly
        """

        self.recordset = win32com.client.Dispatch('ADODB.Recordset')
        if order_by is not None:
            query = f'SELECT * FROM [{sheet_name}] ORDER BY {order_by}'
        else:
            query = f'SELECT * FROM [{sheet_name}]'

        # Run the query
        self.recordset.Open(query, connection, 0, 1)

        self.index = 0
        self.rows = []
        self.fields = []
        self.pagesize = pagesize  # Fetch N rows at a time
        if self.more():
            self.read_batch()  # priming read, first rows of data
            # retain the fields
            self.fields = [f.Name for f in self.recordset.Fields]

    def read_batch(self):
        """
        Read in next batch for row data
        :return:
        """

        self.index = 0  # Index into processed rows
        # Thanks to Rogier Steehouder for the transposing tip
        self.rows = list(zip(*self.recordset.GetRows(self.pagesize)))

    def more(self):
        """
        Is the data cursor positioned to read more data?
        :return: True | False
        """
        return self.recordset.EOF is False

    def __del__(self):
        """
        Release resoures/locks
        :return: None
        """
        # Close recordset if open
        if self.recordset.state != 0:
            self.recordset.Close()

    def columns(self):
        """
        :return: a list of column names for the sheet/query.
        """
        return self.fields

    def __next__(self):
        if self.index < len(self.rows):
            # Set up next result, skipping over any empty rows which
            # frequently occur at the end of a spreadsheet.
            result = dict(list(zip(self.fields, self.rows[self.index])))
            self.index += 1
            if all([v is None for v in result]):
                result = self.__next__(self)
        else:
            if self.more():
                self.read_batch()
                result = self.__next__()
            else:
                raise StopIteration  # No more

        return result



