'''
Created on Sep 28, 2013

@author: mroch
'''

class connector(object):
    
    @classmethod
    def MySQL(cls, Provider=None, Driver="{MySQL ODBC 5.1 Driver}",
              Server="localhost",
              Database=None,
              Port=None,
              User=None,
              Password=None,
              Option=None
              ):       
        # Works with MySQL 4.1 and greater
        # Use Provider MSDASQL for 64 bit according to www.connectionstrings.com
    
        connectionStr = cls.__BuildConnectionStr__([
            "Provider", "Driver", "Server", "Database", "Port",
             "User", "Password", "Option"], locals())
        if Database is None:
            Database = "MySQL database"
        return connector(connectionStr, Database) 
    
    @classmethod
    def MicrosoftExcel(cls,
            Driver="{Microsoft Excel Driver (*.xls, *.xlsx, *.xlsm, *.xlsb)}",
            DBQ=None,
            Extended_Properties='"Excel 12.0;HDR=Yes;IMEX=1"'):
            "Open a Microsoft Excel spreadsheet, Data_Source is the spreadsheet filename"

            if DBQ is None:
                raise ValueError("Path to Excel file (Data_Source) must be specified")
            connectionStr = cls.__BuildConnectionStr__(
               ["Driver", "DBQ", "Extended_Properties"],
               locals(),
               Underscore2Space=True)
            
            return connector(connectionStr, DBQ, additionalParams={'autocommit':True})
        
    @classmethod
    def MicrosoftAccess(cls, Driver="{Microsoft Access Driver (*.mdb, *.accdb)}", DBQ=None):
            "Open a Microsoft Access database, DBQ is the database filename"
            connectionStr = cls.__BuildConnectionStr__(
               ["Driver", "DBQ"], locals())
            if DBQ is None:
                raise ValueError("Path to Access database (DBQ) must be specified")
            return connector(connectionStr, DBQ)
    
    @classmethod
    def ConnectionFromMimeType(cls, filename, mimetype):
        if mimetype == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet':
            return cls.MicrosoftExcel(DBQ=filename)
        elif mimetype == "application/vnd.ms-excel":
            return cls.MicrosoftExcel(DBQ=filename)
        elif mimetype in ["application/msaccess", "application/x-msaccess"]:
            return cls.MicrosoftAccess(DBQ=filename)
         
         
    @classmethod
    def ConnectionString(cls, String=None):
        if String is None:
            raise ValueError("Connection string must be specified")
        else:
            return connector(String, "User specified connection string")        
        
    @classmethod
    def __BuildConnectionStr__(cls, paramlist, valuedict,
                               Underscore2Space=False,
                               additionalParams=dict()):
        """Internal method for constructing a list of connection
        items.  Expects ordered names connection parameters to build
        into a connection string based on a dictionary of values.
        """
        
        connections = []
        for name in paramlist:
            try:
                value = valuedict[name]
                if name in ["Database", "DBQ", "Data_Source"]:
                    value.replace('\\', '\\\\')  # Escape backslash if present
                if Underscore2Space:
                    name = name.replace('_', ' ')
                connections.append("%s=%s;" % (name, value))
            except KeyError:
                pass  # Shouldn't get here
        
        return "".join(connections) 
        
    def __init__(self, connection_str, name="ODBC Connector",
                 additionalParams=dict()):
        """
        build a connection string object
        additionalParams are additional keyword/value parameters
        that will be passed on connection.
        """
        self.connection_str = connection_str
        self.source = name,
        self.params = additionalParams
        
    def get_connector(self):
        "Return the connection string"
        return self.connection_str
    
    def get_source(self):
        return self.source