# -*- coding: utf-8 -*-

import re
from io import StringIO

from xml.sax.saxutils import XMLGenerator
from xml.sax.xmlreader import AttributesNSImpl

import xml.dom.minidom
from xml.dom.minidom import Node
import numbers

import lxml.etree as ET

# site installed packages
import dbxml

# local packages
import pendulum

# Our modules
import iso8601
import mediators.ODBC
from . import environment
import dbxmlInterface.error
from XML.transform import map2elem
import DataImport
from .data_parser import ItisCodeMap
from .timestamp import timestamp_parse
from .Error import MissingField


class Translator:
    """Translate from foreign data source to XML"""

    # List of child elements of <Directives>
    # that have special significance.
    directives = DataImport.directives

    # Namespaces and modules that queries may need    
    tethys_module = \
        'import module namespace lib="http://tethys.sdsu.edu/XQueryFns" at "Tethys.xq";\n'
    tethys_namespace = \
        'import schema namespace ty="http://tethys.sdsu.edu/schema/1.0" at "tethys.xsd";\n'
    tethys_modnamespace = tethys_module + tethys_namespace

    """
    For some detection data sources, we wish to specify different parameters
    for different types of effort.  Specifying effort with additional columns
    Parameter1, Parameter2, ..., ParameterN allow one to specify this.
    
    Example:  We wish to collect Start_Hz and End_Hz for certain sei whale
    calls, but different parameters for killer whale calls.  
    
    Common Name	  Species Code	Call	      Parameter 1	Parameter 2
    Sei Whale	  Bb	        Downsweep	  Start_Hz	    End_Hz
    Sei Whale	  Bb	        LF Downsweep  Start_Hz	    End_Hz
    Sei Whale	  Bb	        All		
    Sei Whale	  Bb	        Unspecified	  Start_Hz	    End_Hz
    ...			
    Killer Whale  Oo	        HFM	          High_Hz	    Low_Hz

    When specifying the effort, we would specify that the source was 
    *Parameters, and when processing Detections, we would interpret Parameter 1
    as either Start_Hz or High_Hz depending on the species and call type.
    
    More details may be found in the Tethys manual's Source Maps section: 
    Handling parameters.
    """

    # in the xml map, this will be where the parameters will be placed,
    # the marker to start parameterized processing
    parse_params_source_val = "[*Parameters]"
    ordered_params_list = ['Subtype', 'Score', 'Confidence',
                           'QualityAssurance', 'ReceivedLevel_dB',
                           'MinFreq_Hz', 'MaxFreq_Hz',
                           'PeakFreq_Hz', 'Peaks_Hz',
                           'Duration_s', 'Sideband_Hz']

    # empty attribute
    empty_attrib = AttributesNSImpl({}, {})
    
    # parsing for namespaces
    schema_loc_re = re.compile(r'"?(?P<namespace>\S+)\s+(?P<location>\S+)"?')
    schema_abbr_re = re.compile(r"xmlns:(?P<abbr>\S+)")
    
    # Entries consist of field names in brackets with fixed text before
    # or after, e.g.:  [Hours]:[Minutes]:[Seconds]
    schema_columns_re = re.compile(r'([^\[\]]*)\[([^\[\]]*)]([^\[\]]*)')
    
    # longitude/latitude 
    longlat_re = re.compile(r"""
   ^(?P<deg>\d+)    # degrees
    (  # minutes
     ([^\d]+   
      (?P<m>\d+(\.\d*)?)
     )
     ( # seconds
      [^\d]+?
      (?P<s>\d+(\.\d*)?)
     )?
    )?
   (  
    ([^\d]*\s+)?  # possible text followed by a direction
    (?P<direction>[NSEW])
    \s*$
   )?
   """, re.VERBOSE | re.IGNORECASE)

    # dbxml XQuery parsing
    user_err_re = re.compile(
        r"Error: User-requested error \[(?P<errtype>[^]]+)")

    # Functions for modifying string case
    case_modification = {
        'upper': str.upper,  # all caps
        'lower': str.lower,  # all lower case
        'capitalized': str.capitalize,  # first letter of first word capitalized
        'title': str.title  # first letter each word capitalized
    }

    # TODO
    def close(self):
        pass
    
    def __init__(self, methods, sources, translator=None, speciesabbr=None,
                 default_docid=None, debug=False):

        """Process tuple oriented data sources.  
        
        :param methods:  XmlMethods instance
        :param sources:  dictionary of data sources (e.g., created by
                         DataImport.File.Sources)
        :param translator: Name of the XML source map that will provide
            directives to translate from input fields to XML data
        :param speciesabbr:  Which set of species abbreviations should be used?
        :param default_docid: When provided, this value will be bound
           to the name "DefaultDocumentIdentifier".  When
           DefaultDocumentIdentifier is used as a source name in a document
           source translation (import) map, the current value will be assigned.
           In general, this should be the same as the document identifier and
           unique to the collection.  For schema that support elements that
           uniquely identify a document (e.g. ty:Deployments/Id,
           ty:Detections/Id, ...), this can be used to automatically provide
           a unique value
        :param debug: show debug information

        Subclasses of the translator may choose to look in specific
        data sources to retrieve the translation or species abbreviation
        maps if needed.         
        """

        self.type = 'general'  # translation type
        
        self.debug = debug 
        self.methods = methods

        self.warnings = dict()  # provide feedback, but let the user proceed
        self.errors = dict()  # errors that allow processing to continue
        
        self.translator = translator
        
        # Resources stores the XML Results of queries for translation maps,
        # attributes, etc
        self.resources = dict()
        
        # Documents that contain species information frequently have inputs
        # that are based on a specific coding scheme.
        self.species_abbr = speciesabbr
        self.species_encoder = ItisCodeMap(self.methods, speciesabbr)

        # Some of the elements expected as children of the Directives
        # elements are reserved words.  Create a list of these
        # List of special words in a translation tree
        self.reserved_words = []
        categories = set(self.directives.keys()).difference(["text", "keys"])
        for c in categories:
            words = self.directives[c]
            if isinstance(words, list):
                self.reserved_words.extend(self.directives[c])
            else:
                self.reserved_words.append(self.directives[c])
            
        # saving source connections to use in queries
        self.sources = sources
        # changes with each _query
        self.current_source = None 
        self.current_type = None
        self.default_docid = default_docid

        # Environment maintains the list of current tuples that are
        # active from all of the data sources that appear as we
        # process the document.
        #
        # Example:  If we query a deployments document and each row 
        # represents a deployment, values for the current row
        # would be in self.deployment.  If we produced a second query for
        # a trackline, a new environment would be pushed onto the existing one,
        # binding the names of the first row of the trackline query result
        # onto the current environment.
        self.environment = None

        self.produced_count = 0  # Number documents produced
        self.attempts_count = 0  # Number of documents we tried to produce

        # Special parameter parsing enabled?  (defaults to disabled)
        # See Tethys Manual for details
        self.parse_params = False
        self.declare_params = False

        # These will be set up later
        self.root_data = None  # First node to process
        self.root_translation = None  # Root of source map document
        self.doc_type = None  # produce single or multiple documents
        self.doc_attributes = None  # document attributes
        self.namespace = None  # document namespace
        self.param_types = {}  # parameters
        self.active = [None, None, None]
        self.param_values = {}
                 
    def prepare(self):
        """prepare - Perform initial parsing
        Not done in constructor to give subclasses an opportunity to
        change some attributes."""
        # might move all of this into init later if not necessary...

        # Check that the translation source map is specified
        if self.translator is None or len(self.translator) == 0:
            self.errors["Translator"] = "No Source Map specified " + \
                "to translate between user data and Tethys"
            raise ValueError(self.errors["Translator"])
        try:
            xmldoc = self.get_translator(self.translator)
        except Exception as e:
            self.errors['Translator'] = str(e)  # problems accessing
            raise ValueError(self.errors["Translator"])

        # Find the attributes and directives for the translation
        try:
            self.resources['attributes'] = self.methods.query_doc(
                "./Mapping/DocumentAttributes", xmldoc)
        except dbxml.XmlQueryEvaluationError:
            self.warnings['Translator'] = \
               f"{self.translator} has no attributes specified, " + \
               "schema validation will not be possible"
        try:
            self.resources['directives'] = self.methods.query_doc(
                "./Mapping/Directives", xmldoc)
        except dbxml.XmlQueryEvaluationError:
            self.errors['Translator'] = \
                f'No Directives element in {self.translator}'
            raise ValueError(self.errors["Translator"])
        
        # Translation document can specify attributes that will be
        # included as attributes of the generated document.
        # Example:
        #
        # <DocumentAttributes>
        #  <Attribute> 
        #     <Name>xmlns:ty</Name>
        #    <Value>http://tethys.sdsu.edu/schema/1.0</Value>
        #  </Attribute>
        # </DocumentAttributes>
        #
        # would add the attribute xmlns:ty=http://tethys.sdsu.edu/schema/1.0
        # to the root element.  Additional attributes may be specified
        try:
            self.resources['attributes'] = self.methods.query_doc(
                "./Mapping/DocumentAttributes", xmldoc)
        except dbxml.XmlQueryEvaluationError:
            self.warnings['Translator'] = \
               f"{self.translator} has no attributes specified, " + \
               "schema validation will not be possible"
        # Parse out the attributes and determine the namespace if any
        self.doc_attributes, self.namespace = self.parse_attributes(
            self.resources['attributes'].__next__().asString())

        # Access the <Mapping/Directives> element which specifies how
        # translation is to be handled.        
        try:
            self.resources['directives'] = self.methods.query_doc(
                "./Mapping/Directives", xmldoc)
        except dbxml.XmlQueryEvaluationError:
            self.errors["Directives"] = f'No Directives element in {Translator}'
            raise ValueError(self.errors["Directives"])
        
        # If user has specified a species abbreviation map (coding),
        # verify that it exists
        # As these are only used for detections(localizations?) we want to
        # move this section elsewhere, now in FileImport.Sources()
        # Defaults to "TSN" if omitted, but "TSN" here is not found

        # Some data sources use items from the same column in different
        # roles.  They can do so by specifying the attribute
        # <Table ... parameters="define"> 
        # to specify that the names will be Param1, Param2, ...
        # and they will be mapped to specific names based upon
        # information gathered elsewhere.  The location of the 
        # translation is dependent upon ????????
        # No parameter parsing until someone uses the parameters="define"
        # attribute on a data source which indicates that a mapping
        # will be established (currently with a per species/call key)
        # between the number parameters and the names to which they will
        # be translated.  See Tethys manual for details on how to use this.
        self.parse_params = False
        self.declare_params = False

        # Parse the directives into a document object model (DOM) tree
        # This is somewhat inefficient as we are serializing and unserializing
        # the document, but the code was originally written using the minidom
        # parser and it's a bit of work to change it.  Given that the document
        # is typically small, this should not be much of an issue.
        
        directives = self.resources['directives'].__next__().asString()
        node = xml.dom.minidom.parseString(directives)
        # get Directives node
        elements = [n for n in node.childNodes
                    if n.nodeType == Node.ELEMENT_NODE]
        # get children of Directives
        elements = [n for n in elements[0].childNodes
                    if n.nodeType == Node.ELEMENT_NODE]
        if len(elements) != 1:
            raise ValueError("Directives must have a single child element " +
                             "which will be the XML document or a data source")

        # If child element of <Directives> is a data source 
        # then one document will be generated for each row of that
        # data source.  If the element name is not a "data" directive,
        # then a single XML document will be returned.
        root = elements[0]
        if root.nodeName in self.directives['data']:
            self.doc_type = "multiple"  # >= 1 document
            self.root_data = root
            elements = [n for n in root.childNodes 
                        if n.nodeType == Node.ELEMENT_NODE]
            if len(elements) != 1:
                raise ValueError("First Directive is a data source, " +
                                 "requires single document element as child")
            self.root_translation = elements[0]
        else:
            self.doc_type = "single"  # only one document
            # Store the root of the translation tree
            self.root_translation = elements[0]
            
        # Verify that the root of the translation tree is
        # acceptable as an enclosing document name
        if self.root_translation.nodeName in self.reserved_words:
            raise ValueError(
                "The document root, {self.root_translation.nodeName}, " +
                "is a processing directive.")

        self.param_types = {}
        
        # User can iterate over the documents to be generated.
        return

    def __iter__(self):
        if self.doc_type == "multiple":
            # Root node is a data source, query it so that
            # self.environment is initialized to iterate over
            # multiple documents
            self.get_source(self.root_data, debug=self.debug)
        self.attempts_count = 0
        self.produced_count = 0
        return self

    def __next__(self):
        """
        Generate the next XML document
        :return: XML text
        """

        self.errors = {}
        self.warnings = {}
        if self.doc_type == "multiple":
            # update self.environment to the next record
            _env = next(self.environment)

            # each time we're inside this loop, we should clear
            # child environments to prevent nested environments
            # being retained between documents

            # reset errors and warnings for new document
            # todo:  keep XML for errors between documents for reports
            # (probably just output to xml report generator)
            self.attempts_count += 1
            self.produce_document(self.root_translation)
            self.produced_count += 1
            return self.get_xml()
        else:
            if self.attempts_count > 0:
                raise StopIteration  # Only one document

            self.attempts_count += 1
            self.produce_document(self.root_translation)
            self.produced_count += 1
            return self.get_xml()

    def produce_document(self, docroot):
        """produce_document - Generate a single XML document
           docroot - root node of document
        """
        # Produce one document using the current environment
        docname = self.namespace + docroot.nodeName
        self.__document_start(docname)
        self.parse(docroot)
        self.__document_end(docname)
         
    def __document_start(self, root_element):
        """
        Create opening part of an XML document
        :param root_element: root element of the document
        :return:  None
        """

        self.xmlstr = StringIO()
        self.xml = XMLGenerator(self.xmlstr, encoding='utf-8')
        self.xml.startDocument()
        self.xml.startElement(root_element, self.doc_attributes)
        
    def __document_end(self, root_element):
        """
        Close off a document that is in progress
        :param root_element:  End name
        :return:  None
        """
        self.xml.endElement(root_element)
        self.xml.endDocument()

    def parse(self, node, ignore=False):
        """
        Parse children of a specified node
        :param node:  node to parse
        :param ignore: if True, do nothing
        :return: None
        """

        # These may be populated with Species, Call and Subtype
        # if the fields are present.  They define the active 
        # record and are used for processing parameters
        # whose names change
        self.active = [None, None, None]
        self.param_values = {}  # Reset parameter dictionary for this row
            
        if ignore:
            return
        
        # Process all children of node

        for cn in node.childNodes:
            if cn.nodeName == self.directives['condition']:
                # Evaluate conditional statement and determine
                # if we should process its children
                self.__condition(cn)
                # If the condition was met, we will have recursively processed
                # the condition node children.
                continue
            elif cn.nodeName == self.directives['predicate']:
                # we want to skip this
                continue
                
            elif cn.nodeName == self.directives['entry']:
                # Translate a user entry
                (dest_tag, value) = self.__entry(cn)
                source_val = value[0]
                if not source_val:
                    continue
                
                attr_dict = value[1]
                for an in self.get_children_by_name(
                        cn, self.directives['attribute']):
                    (a_dest_tag, a_source_val) = self.__entry(an)
                    if a_source_val[0] is not None:
                        attr_dict[a_dest_tag] = a_source_val[0]
                    
                attr = AttributesNSImpl(attr_dict, {})
    
                # XML requires a string
                if source_val and not isinstance(source_val, str):
                    source_val = str(source_val)
                  
                if source_val and len(source_val.strip()) > 0:
                    try:
                        self.xml.startElement(dest_tag, attr)
                    except AttributeError:
                        (dest_tag, value) = self.__entry(cn)
                    self.xml.characters(source_val)
                    self.xml.endElement(dest_tag)
            
            elif cn.nodeName in self.directives["data"]:
                # User is accessing a data source
                # Is the user defining parameters in this data source?
                # If so, we will be associating parameter names with the 
                # positional parameter columns Parmeter 1, Parameter 2, ...
                # on a per species and call type basis.
                if cn.getAttribute("parameters").lower() == "define":
                    # From this point out, the parameter expansion keyword
                    # is valid
                    self.parse_params = True
                    self.declare_params = True
                else:
                    self.declare_params = False
                self.__query(cn)

            elif cn.nodeName not in \
                    [self.directives["text"], self.directives["comment"]]:
                # User has specified an element that is to be part of the 
                # XML document.  Add it to the current document and process
                # its children.
                if self.debug:
                    print(f'Copying element <{cn.nodeName}>')
                self.xml.startElement(cn.nodeName, AttributesNSImpl({}, {}))
                self.parse(cn)
                self.xml.endElement(cn.nodeName)
                
            elif cn.nodeName == self.directives["text"]:
                # Text node.  Strip space and copy to XML
                value = cn.nodeValue.strip()
                if value != "":
                    self.xml.characters(value)
                    
    def declare_parameters(self, env, debug):
        """Note parameters associated with this species/call
           Assumes the species and call have already been processed"""
           
        # Add mappings from named parameter to column name
        # for each non-empty parameter entry.
        mapping = dict()
        for name in list(env.frame.keys()):
            if name.startswith('Parameter') and env[name] is not None:
                field = map2elem(env[name])
                mapping[name] = field
        # self.declare_params = False #reset
        self.param_types[tuple(self.active)] = mapping
        
    def row_parameter_values(self, params_tag, row, debug=False):
        """Process parameters from a data row_num
           Has the side-effect of modifying self.param_values"""
        
        # Retrieve the parameter names associated with this entry
        # Note that some parameters may have been set elsewhere
        # through special processing.
        names = self.param_types.get(tuple(self.active), None)
        if names is not None and len(names) > 0:
            # Populate parameters
            for param_numbered, param_named in names.items():
                try:
                    value = row[param_numbered]
                    if value is not None:
                        self.param_values[param_named] = value
                except KeyError:
                    pass

        # Output parameters
        if len(self.param_values) > 0:
            self.xml.startElement(params_tag, self.empty_attrib)
            
            # Process ordered parameters
            for p in self.ordered_params_list:
                if p in list(self.param_values.keys()):
                    self.xml.startElement(p, self.empty_attrib)
                    self.xml.characters(str(self.param_values[p]))
                    self.xml.endElement(p)
                    del self.param_values[p]
            
            # Anything remaining is UserDefined
            if len(self.param_values) > 0:
                self.xml.startElement('UserDefined', self.empty_attrib)
                # Output remaining in a sorted list
                key_list = list(self.param_values.keys())
                key_list.sort()
                
                for k in key_list:
                    self.xml.startElement(k, self.empty_attrib)
                    self.xml.characters(str(self.param_values[k]))
                    self.xml.endElement(k)
                self.xml.endElement('UserDefined')
            
            self.xml.endElement(params_tag)  
        
        return

    def __query(self, node):
        """Process a data source query
        Query can be Table or Sheet as follows:
        <Table query="some SQL statement" 
               source="data source name"
               label="nameRepresentingQuery">

        The attributes have the following semantics:
        query:  any valid SQL query for the specified source
        source:  name of data source, may be omitted if there is only one
            source
        label:  Optional name that can be used to reference
            values that are hidden by nested queries
            Example:  Suppose a query used label="Deployment" and had
            a value lat that represented the deployment latitude.  A later
            query might retrieve a trackline that also has a value of lat.
            We could use the notation "[Deployment:lat]" in a translation
            or condition node to denote the lat from Deployment instead of
            the one from trackline.  Note that labels should not be integers
            and integers representing the nesting depth can be used as well:
            [0:lat] for the inner-most lat, [1:lat] for the next level up.


        <Sheet source="data source name"
               name="sheet name"
               label="nameRepresentingQuery">
        
        Attributes source and label have the same semantics as above.
        Attribute name is the name of a database table or workbook sheet,
        all rows are processed.        
        """

        # identify common attributes
        source = node.getAttribute("source")

        if(type(source) is str):
            source = "";

        if source == "":
            # Only allowed if there is only one source
            # Verify number of sources
            sources = list(self.sources.items())
            if len(sources) > 1:
                # for lack of a better error, after all, it is an xml attr...
                raise AttributeError(
                    "source attribute required when >1 source is provided")
            source = sources[0][0]
            # pull out value - odbc connection
            connection = sources[0][1]['connection']
        else:
            # Find source
            # attribute should reference the <name> element of the source, which
            # should be stored as key in dictionary
            try:
                connection = self.sources[source]['connection']
            except KeyError as k:
                raise ValueError(f'Cannot find Source named: "{k.args[0]}"')
        self.current_source = source    
        
        label = node.getAttribute("label")
        label = None if label == "" else label
        
        # Format a string giving source and label for any error messages
        src_id = f"source={source}{label if label else ''}"

        query = None  # Add to scope, resolve as we go along
        if node.nodeName == "Table":
            query = node.getAttribute("query")
            if query == "":
                self.note_problem(
                    self.errors, "Table", f"{src_id} - no query")
                return  # Can't process query, skip children
        
            # Replace any query variables with their values
            # Query variables are of the form {name}
            # e.g. select * from longlat where time <= {enddeployment}
            # enddeployment would be a variable that is active from 
            # some other query.
            # CAVEAT:
            # Note that we could get into trouble here if the value of the
            # variable matches one of the other variables that we are 
            # replacing (including the { }).  The likelihood of this seems
            # very low, so we just produce an error if this happens.
            variables = re.findall("{.*?}", query)
            uniquevariables = set(variables)
            missing = []
            for v in uniquevariables:
                variable = v[1:-1]  # Strip off { }
                # lets assume no more than 99 levels
                match = re.match("[0-9]:|[1-9][0-9]:", variable)
                if match:
                    level = match.group()
                    # variable name without the level prefix
                    variable = variable.replace(level, "")
                try:
                    value = self.environment[variable]
                    # Check for problem noted above
                    if value in uniquevariables:
                        self.note_problem(
                            self.errors, "Table",
                            f"{src_id} - variable {v}'s value ({value})" +
                            "matches another query variable, " +
                            "change name of other variable")
                        return  # skip rest of table
                    # update query
                    if isinstance(value, str):
                        # Quote the string value, might get into problems
                        # here with dates...
                        query = query.replace(v, f'"{value}"')
                    else:
                        # coerce into str for query
                        query = query.replace(v, str(value))
                except KeyError:
                    missing.append(v)
                    
            # Were we able to find all variables?
            if len(missing):
                missing_str = ",".join(missing)
                self.note_problem(
                    self.errors, "Table",
                    f"{query} unable to substitute values for: {missing_str}")
                return  # Can't process query, skip children
            
            # Guard against SQL code injections that would modify
            # the database.  Note that this is not vary sophisticated, we
            # don't parse and would fail on values that matched, 
            # e.g.  ... where tupleattr === "insert" 
            # would cause a problem.  Can be expanded later if needed.
            lquery = query.lower()
            for kw in ["insert", "delete"]:
                if kw in lquery:
                    self.note_problem(
                        self.errors, f"{kw} not permitted in query {src_id}")
                    return  # Can't process query, skip children
            
        elif node.nodeName == "Sheet":
            name = node.getAttribute("name")
            if name is None:
                self.note_problem(
                    self.errors, node.nodeName,
                    f"{src_id} missing name attribute to specify source sheet")
                return  # skip table
            
            # SQL query appends $ to name.  We'll only do this if the
            # sheetname does not end in $ already
            if name[-1] != "$":
                name = f"[{name}$]"
            query = f"select * from {name}"
            
        self.current_type = node.nodeName
        try:
            # Get an iterable containing table rows from SQL query
            rows = connection.sql(query)
        except Exception as e:
            if node.getAttribute("optional").lower() == "true":
                # Query failed, but was marked as optional, skip
                return
            raise ValueError(
                f"Unable to execute data query:  {query}" + "\n" +
                f"SQL error:  {str(e)}" +
                "\nVerify table/sheet and selected fields exist in data source.")

        if self.environment is None:

            # First encounter with a data source, set up the environment
            if self.default_docid:
                # Caller provided a default document identifier, create a
                # base environment with the name bound to their value
                static_bindings = {
                    "DefaultDocumentIdentifier": self.default_docid}
            else:
                static_bindings = dict()

            self.environment = environment.IterEnvironment(
                rows, static_values=static_bindings, name=query)

            if self.doc_type == 'single':
                # process row for all items in environment
                for _env in self.environment:
                    self.parse(node)

            if self.declare_params:
                self.declare_parameters(self.environment, self.debug)
        else:
            # environment exists, push new frame onto it
            self.environment = self.environment.push(rows, name=query)
            for _env in self.environment:
                # instead of looping through children,
                # loop through node; since parse() acts on children
                # we skip a child otherwise
                
                # KLUDGE for old effort naming
                try:
                    sc = _env['Species Code']
                    if sc is not None and 'effort' in sc.lower():
                        continue
                except (KeyError, TypeError, AttributeError):
                    pass  # no need to do anything
                self.parse(node)
                if self.declare_params:
                    self.declare_parameters(_env, self.debug)

            # nest has finished, pop it off
            self.environment = self.environment.get_parent()
        
    def __condition(self, node):
        """Process a conditional directive"""
        
        # Black Mesa operations...
        operations = {
                      "empty": lambda val: val is None,
                      "iftrue": lambda retain: bool(retain),
                      "iffalse": lambda retain: not bool(retain),
                      "matches": lambda val, expr: val == expr,
                      "matchesregexp":
                          lambda val, regex: bool(re.match(regex, val))
                      }
        conditions = dict()
        nodes = self.get_children_by_name(node, self.directives['predicate'])
        prednode = nodes[0]
        exp = None  # will be set in loop
        for c in ['operand', 'operation']:
            try:
                ops = self.get_children_by_name(prednode, self.directives[c])
            except KeyError:
                self.note_problem(
                    self.errors, f"Unable to retrieve condition/{c}")
                pass
            for op in ops:
                try:
                    if c == 'operation':                   
                        for o in ('op', 'retain'):
                            try:
                                conditions[o] = op.getAttribute(o)
                                if conditions[o] in ('matches', 'matchesregexp'):
                                    exp = op.firstChild.nodeValue
                                conditions[o] = operations[conditions[o]]
                            except KeyError:
                                pass
                    if c == 'operand':
                        conditions[c] = op.firstChild
                        conditions[c] = conditions[c].nodeValue.strip()
                        
                except IndexError:
                    pass
                except KeyError:
                    pass
       
        # conditions and ops are all set up in the dict, see if it passes...
        if conditions['operand'] is not None:
            # there is a dependent field, lets find it
            matches = self.schema_columns_re.findall(conditions['operand'])
        if len(matches) >= 1:

            value = []
            # Expand out variables.  If a variable does not exist, use None
            for m in matches:
                try:
                    v = self.environment[m[1]]
                except KeyError:
                    v = None
                value.append(v)

            # For comparison, we merge strings then check
            # If any fields are not strings, we use the first one.
            if all([isinstance(v, str) for v in value]):
                value = "".join(value)
                if len(value.strip()) == 0:
                    value = None
            elif isinstance(value[0], (int, float, complex)):
                # Consider zero as absent (todo: build better comparison check)
                value = value[0] if value[0] != 0 else None
            else:
                value = value[0]
            
            # have the values, now test the conditions
            if conditions['op'] == operations['empty']:
                opresult = conditions['op'](value)
                result = conditions['retain'](opresult)
            elif conditions['op'] == operations['matches']:
                opresult = conditions['op'](value, exp)
                result = conditions['retain'](opresult)
            elif conditions['op'] == operations['matchesregexp']:
                opresult = conditions['op'](value, exp)
                result = conditions['retain'](opresult)
        
            if result:
                # condition passed, parse
                self.parse(node)

    def get_element_path(self, node, element):
        """
        Given a Dest element in a sourcemap and its parent node, return
        the path to the element in the document that will be generated.

        :param node: node that contains Dest element
        :param element: name of Dest child of node
        :return:  String with XPath to node in document
        """

        paths = [element]  # Dest entry
        # Chase parents up the tree, skipping over reserved words
        while node is not None:
            name = node.nodeName
            if name == "Directives":
                break  # Last entry
            if name not in self.reserved_words:
                paths.append(name)
            node = node.parentNode
        # ancestors --> descendants
        paths.reverse()
        return "/".join(paths)
    
    def __entry(self, node):
        """__entry - Process a DOM element or attribute
        """
        "Find values for a node, returns output tag, [value, attributes]"

        # data, kind, default optional although we really expect source or kind
        transform = dict()  # dictionary of nodes of various types
        access_issues = []
        case_transform = None
        for d in ['dest', 'source', 'kind', 'default']:
            nodes = self.get_children_by_name(node, self.directives[d])
            try:
                # Value should be the first child of the node.
                # In the case of duplicate names, use the last one
                transform[d] = nodes[-1].firstChild
                transform[d] = transform[d].nodeValue.strip()
                if d == 'kind':
                    transform[d] = transform[d].lower()
                if d == 'source':
                    if 'case' in nodes[0].attributes:
                        action = nodes[0].attributes['case'].nodeValue
                        if action in self.case_modification:
                            case_transform = self.case_modification[action]
                        else:
                            # Bad value
                            valid = ", ".join(self.case_modification.keys())
                            raise ValueError(
                                f'Source case="{action} item {transform[d]};' 
                                f'case not in: {valid}'
                            )
            except IndexError:
                transform[d] = None
            except AttributeError:
                access_issues.append(self.directives[d])  # node, but no nodeValue

        if len(access_issues) > 0:
            msg = [
                f"Unable to access Entry directive(s): {', '.join(access_issues)}." \
                "\nWe processed the following Entry directives successfully: " ]
            for k, v in transform.items():
                if v is not None:
                    msg.append(f"{self.directives[k]}={v}")
            msg.append("\nA likely cause is a bad Entry directive value in "
                       "the source map.")
            raise ValueError(" ".join(msg))

        path = self.get_element_path(node.parentNode, transform['dest'])

        if self.parse_params and \
                transform['source'] == self.parse_params_source_val:
            # User has specified special parameter processing
            # Multiple values are output as a side effect and
            # we return None to indicate that there is nothing
            # to output
            # TODO!  **********************************************
            self.row_parameter_values(transform['dest'], self.environment)
            # Nothing to output - special case
            return None, [None, {}]

        # Find each named field along with any preceding or following text
        # Each match is a triple (preceding text, field name, following text)
        if transform['source'] is not None:
            # Multiple fields may be permitted, 
            # e.g. <Source> [YYMMDD]T[HHMMSS]Z </Source>
            # These are grouped into a list of tuples where index 1 is the 
            # field and indices 0 and 2 show the extra text.  
            # For our example:  [(' ', 'YYMMDD', 'T'), ('', 'HHMMSS', 'Z ')
            matches = self.schema_columns_re.findall(transform['source'])
        else:
            matches = []

        # Assume output is a string if we have more than one entry or
        # nonempty text to be prepended or appended.
        source_val = None
            
        if len(matches) >= 1:
            match_fields = [m[1] for m in matches]
            if self.environment is None:
                raise ValueError(
                    "Attempted to map source field(s) "
                    f"{', '.join(match_fields)} to {transform['dest']} "
                    "without an active data source in the SourceMap "
                    "(e.g., Table)"
                )
            try:
                # Substitute the source fields with their values from the row_num

                # Example
                # <Entry> <Source> [YYMMDD]T[HHMMSS] </Source>
                #         <Dest> Start </Dest>

                # Get values associated with each field name
                # e.g. ['2012-09-23', '08:32:22']
                values = [self.environment[m] for m in match_fields]
                
                # perform special processing for dates
                if transform['kind'] == 'datetime':
                    t = timestamp_parse(values, matches)
                    if t is None:
                        raise MissingField(transform['source'][0])
                    source_val = (t, {})
                else:
                    missing = []
                    for idx in range(len(values)):
                        if values[idx] is None:
                            missing.append(transform['source'][idx])
                    if len(missing) > 0:
                        raise MissingField(" ".join(missing))
                    # Pair it with the matches list from above, e.g.
                    # [['2012-09-23', (' ', 'YYMMDD', 'T')], 
                    #  ['08:32:22', ('', 'HHMMSS', 'Z ')]
                    # ]
                    # and format the correct string using pieces of each list
                    attr = dict()
                    strvals = []
                    for m in range(len(values)):
                        [v, d] = self.process_source_node(
                            values[m], transform['kind'])
                        if v is not None:
                            if isinstance(v, float) and int(v) == v:
                                # Avoid decimal points in whole numbers
                                v = int(v)
                            strvals.append(f"{matches[m][0]}{v}{matches[m][2]}")
                            for key in list(d.keys()):
                                attr[key] = d[key]
                        
                    if len(strvals) > 0:    
                        source_val = ("".join(strvals), attr)
                    else:
                        source_val = (None, attr)

            except KeyError:
                # Source document did not supply a value
                if transform['default'] is not None:
                    # Source map has a <Default> for this entry
                    attr = dict()
                    default = transform['default']
                    
                    # See if source map is using a special default value
                    if default == "_Id_":
                        default = self.current_source
                    
                    # Process the default entry as if from data source
                    [v, d] = self.process_source_node(default, 
                                                      transform['kind'])
                    if v is not None:
                        for key in list(d.keys()):
                            attr[key] = d[key]
                    source_val = (v, attr)
                else:
                    source_val = None
            except pendulum.parsing.exceptions.ParserError as t:
                self.note_problem(
                    self.errors, "TimeError", f"Unable to parse datetime: {path}",
                    str(t))
            except ValueError as v:
                source_val = None
                if hasattr(v, 'message') and v.message:
                    self.note_problem(
                        self.errors, "ValueError"
                        f"Unable to parse entry for {path}: {v.message}")
                else:
                    self.note_problem(
                        self.errors, "ValueError", f"Unable to parse entry for {path}: {v}")
            except iso8601.TimeError as t:
                source_val = None
                self.note_problem(
                    self.errors, "TimeError",
                    f"Unable to parse entry for {path}: {t.message}")
            except MissingField as m:
                # one or more values were missing, use default if present
                if transform['default']:
                    attr = dict()
                    [v, d] = self.process_source_node(
                        transform['default'], transform['kind'])
                    if v is not None:
                        for key in list(d.keys()):
                            attr[key] = d[key]
                    source_val = (v, attr)
                else:
                    source_val = None
                                        
        elif transform['default']:
            source_val = (transform['default'], {})
    
        if source_val is None:
            source_val = (None, {})

        if case_transform and isinstance(source_val[0], str):
            # User requested case transformation for this string
            source_val = (case_transform(source_val[0]), source_val[1])

        if self.debug:
            print("Source(%s)=%s" % (transform['kind'], str(source_val)))
                         
        return transform['dest'], source_val
              
    def parse_attributes(self, attr_str):
        """
        Process any attributes that are to be assigned to the instance document"
        :param attr_str:  attribute string
        :return:
        """
        # Pull out all of the attribute/value pairs
        attributes = {}
        node = xml.dom.minidom.parseString(attr_str)  # document root
        node = node.childNodes[0]  # DocumentAttributes tag
        for attrnode in self.get_children_by_name(node, 'Attribute'):
            tags = ['Name', 'Value']
            values = [None, None]
            for idx in range(len(tags)):
                # Get first child with specified tag and pull its value
                child = self.get_children_by_name(attrnode, tags[idx])
                if len(child):
                    values[idx] = self.get_node_text(child[0])
                    # strip quotes if needed
                    if len(values[idx]) >= 2 and values[idx][0] == '"'\
                            and values[idx][-1] == '"':
                        values[idx] = values[idx][1:-1]
                     
            # Did we get a name/value pair?
            if all([v is not None for v in values]):
                attributes[values[0]] = values[1]
                
        # If user specified a namespace, store the abbreviation
        # so that we can output it for the top level element
        abbr = ""
        try:
            ns = attributes['xsi:schemaLocation']  # Get namespace
        except Exception:
            ns = None
        
        if ns:
            match = self.schema_loc_re.match(ns)
            if match:
                ns = match.group("namespace")
                # find namespace in list of values
                for (k, v) in list(attributes.items()):
                    if v == ns:
                        # key probably has xmlns:SomeAbbrev
                        # pull out the abbreviation
                        match = self.schema_abbr_re.match(k)
                        if match:                        
                            abbr = match.group("abbr") + ':'
                            break
                    
        return attributes, abbr
         
    def get_translator(self, translator):
        """
        Query the SourceMaps collection to pull out the parse specification
        :param translator: Name of SourceMap.  We look for
           collection("SourceMaps")/Mapping[Name=translator]
        :return: document matching translator specification
        """

        # Pull the translation map
        mapQuery = f'collection("SourceMaps")/Mapping[Name="{translator}"]'
        result = self.methods._query(mapQuery)
        # Responsible for deleting later...
        self.resources['translator'] = result
        if result.size() < 1:
            raise ValueError(f"Unable to find SourceMap mapping {translator}")
        elif result.size() > 1:
            # Shouldn't ever get here...
            raise ValueError(f"Mapping {translator} is not unique")
        sole_result = next(result)
        return sole_result.asDocument()

    def note_problem(self, probdict, category, message, detail=None, line=True):
        """note_problem - Record a problem with the specified problem dictionary

        :param probdict:  Dictionary to which problem will be added
        :param category:  General category, should be an XML element name
        :param message:  Human readable information about this type of error
        :param detail:  Detailed information, for example a bad value that we parsed
        :param line:  If True, the current line number of the innermost source is given.
           This is not always the right one.
        :return:

        *args - First argument must be the problem dictionary to which 
        the problem will be appended.
        Remaining elements of *args are string arguments that will be 
        translated to element tags.  The last string is either treated as an 
        element or a value (see below)
        
        **kwargs - Set of keyword arguments, all of which are optional:         
           line=True (default)|False - Note line number.  If false,
               the last string of *args is treated as a value.
           element=None (default)|string - Target element being parsed.

               
        """

        # Find out where we are in the parse
        if line:
            [_datacollection, _datasource, line] = self.get_position()
            if detail is None:
                detail = f"row: {line}"
            else:
                detail = f"{detail} row: {line}"

        dbxmlInterface.error.addErrMsg(probdict, category, message, detail)

    def has_errors(self):
        """has_errors - Have we encountered any errors?"""
        return len(self.errors) != 0
    
    def has_warnings(self):
        """has_warnings - Have we encountered any warnings?"""
        return len(self.warnings) != 0
    
    def cannot_overwrite(self):
        """cannot_overwrite - Document exists in the container to which 
           it will be added and overwrite permission has not been granted"""
        
        self.note_problem(
            self.errors, "Document Exists",
            "Document exists and cannot be overwritten without permission",
            line=False)
        
    def get_position(self):
        """Return information about the current location:
        (name of current source, datatype, data index)
        """

        idx = self.environment.idx if self.environment else 0
        return self.current_source, self.current_type, idx

    def get_problems(self, clear=False, xml=None):
        """get_problems - Report current problems and warnings 
           clear - clear problem list for next report
           xml - Send report to specified XMLGenerator rather 
                 than returning string"""

        if xml:
            result = None  # Nothing to return
            
            # Does the xml object support newline() ?
            supports_nl = callable(getattr(xml, 'newline', None))

            # Construct function nl() that calls newline if xml supports it,
            # otherwise does nothing
            def nl():
                if supports_nl:
                    xml.newline()

            # Process warnings and errors
            for (problemcat, problems) in [("Warnings", self.warnings),  
                                     ("Errors", self.errors)]:
                if len(problems) > 0:
                    nl()
                    xml.startElement(problemcat, {})
                    nl()
                    dbxmlInterface.error.dict2msgXML(problems, xml)
                    xml.endElement(problemcat)
                    nl()
        else:
            msgs = []
            # Process warnings and errors
            for (problemcat, problems) in (
                    ("Warnings", self.warnings), ("Errors", self.errors)):
                if len(problems) > 0:
                    msgs.append("%s %s ----\n%s"%(
                        problemcat, self.source, 
                        dbxmlInterface.error.dict2msg(problems, 2)))
            
            if len(msgs) > 0:
                result = "Problems occurred parsing %s\n%s" % (
                    self.source, "\n".join(msgs))
            else:
                result = ""
            
        return result
    
    def get_source(self, tablenode, debug=False):
        return self.__query(tablenode)
        
    def get_xml(self):
        """
        Produce XML from parsed source
        :return:  XML document
        """

        # Send the XML through element tree so that we ensure that
        # elements are fully qualified.  As ET.fromstring expects a byte
        # we need to re-encode as unicode
        uni = self.xmlstr.getvalue().encode('utf-8')

        tree = ET.fromstring(uni)
        xml = ET.tostring(tree, pretty_print=True)

        return xml

    def get_current_source_doc(self, close=False):
        """
        Returns the filename of the source material for the current document.
        If there is no file or the file is used to generate multiple documents,
        None is returned.
        :param close: Closes the file (if one exists)
        :return:  (type, mimetype, data) where
          type is string:
             unavailable - data not available
             filename - value contains a file path
             mimetype - media type
             data - value contains data
        """

        filename = None
        datatype = "unavailable"
        mimetype = "unavailable"
        data = None
        if self.doc_type == 'single':
            if self.current_source in self.sources:
                src = self.sources[self.current_source]
                mimetype = src['mimetype']
                if mimetype in ("text/xml", "application/xml"):
                    filename = src['file']
                    datatype = "data"
                    data = self.sourcedoc
                else:
                    filename = src['tempfile']
                    datatype = "file"
                    if close:
                        # Close off the ODBC connection, releasing the file
                        self.sources[self.current_source]['connection'].close()
        return datatype, filename, mimetype, data

    def process_source_node(self, source_val, kind):
        """Perform type based processing user's data"""
        
        if source_val is None:
            # if source_val = None, then return an empty string
            source_val = ['', {}]
        elif not kind:
            return [source_val, {}]
        else:
            # Define <Kind>'s here
            if kind == 'datetime':
                # todo:  Need to think about this case.  Mostly handled in
                # __entry() except when we have a default value.  Should
                # unifty this.
                try:
                    source_val = [iso8601.DateTime(source_val), {}]
                except Exception:
                    self.note_problem(self.errors, 'Unable to parse date %s' % (str(source_val)))
            elif kind == 'string':
                try:
                    source_val = [source_val, {}]
                except TypeError:
                    self.note_problem(self.errors, 'Cannot convert to String')
            elif kind == 'integer':
                try:
                    source_val = [int(float(source_val)), {}]
                except TypeError:
                    self.note_problem(self.errors, 'Cannot convert to Integer')
            elif kind == 'number':
                try:
                    source_val = [float(source_val), {}]
                except TypeError:
                    self.note_problem(self.errors, 'Cannot convert to Number')

            elif kind == 'speciescode':
                tsngroup = self.species_encoder[source_val]
                self.active[0] = source_val  # Track active species
                source_val = tsngroup

            elif kind == 'latlong':
                # longitude/latitude
                source_val = [self.parse_longlat(source_val), {}]
                    
            elif kind == 'calltype':
                # escaping seems to be automatically done by the xml creator
                # import cgi
                # source_val = cgi.escape(source_val) # escapes & > and <
                i = source_val.find('/')
                if i != -1:                       
                    # split into call & subtype
                    subtype = source_val[i + 1:]
                    source_val = source_val[0:i]         
                                       
                    # Store for parameters if needed
                    self.param_values['Subtype'] = subtype
                else:
                    subtype = None
                
                # Track active call & subytpe, used in parameter parsing
                self.active[1:3] = [source_val, subtype]
                
                # Put in standard form, value, attributes                        
                source_val = [source_val, {}]
                
            elif kind == 'callsubtype':
                # Pull out the subtype
                i = source_val.find('/')
                if i != -1:
                    source_val = [source_val[i + 1:], {}]
                else:
                    source_val = [None, {}]    
            else:
                raise ValueError(f'unknown Kind {kind} specified')

        # any other Kind values?
        return source_val

    def parse_longlat(self, value):
        """
        parse_longlat - Convert a longitude/latitude to the proper format

        :param value:  string or number.  Strings should be up to a triplet
          of numbers (degrees, minutes, seconds) separated by non-numbers and
          followed by a cardinal direction [NESW].  Examples:
             32 54 73 W or 32˚ 54' 23''
        :return:  decimal value in degrees east or degrees north
        """

        # special handling for longitudes and latitudes
        if not isinstance(value, numbers.Number):
            m = self.longlat_re.match(value)
            if m:
                groups = ["deg", "m", "s"]
                dms = []
                for g in groups:
                    try:
                        v = float(m.group(g))
                    except TypeError:
                        v = 0.0
                    dms.append(v)
                    
                degrees = dms[0] + dms[1] / 60.0 + dms[2] / 3600.0
                
            # All longitude and latitudes are specified with
            # 360 degrees (no negative values)
                direction = m.group("direction")
                if direction is not None:
                    direction = direction.upper()
                    if direction == 'W':
                        degrees = 360 - degrees
                    elif direction == 'S':
                        degrees = -degrees
                value = f"{degrees}"
            return value
        else:
            if value < 0:
                value = 360 + value
            return value

    def get_children_by_name(self, node, name):
        """
        Similar to DOM getElementsByTagName, but only retrieves first
        generation descendants
        :param node:  DOM node
        :param name: Name to return
        :return: list of nodes whose nodeName matches name
        """

        return [cn for cn in node.childNodes if cn.nodeName == name]

    def get_node_text(self, node, strip=True):
        """
        Extract the first text child of a DOM node, returns None when no
        text is found
        :param node:  DOM node
        :param strip: strip whitespace
        :return:  text from first text child
        """
        found = None
        for child in node.childNodes:
            if child.nodeType == Node.TEXT_NODE:
                found = child.data
                if strip:
                    found = found.strip()
                break  # bail out of loop
            
        return found
    
    def multiple_document_check(self):
        """
        Can this translator return more than one document?
        :return: True - Iteration might produce more than one document
                 False - Iteration will only produce a single document
        """
        return self.doc_type == "multiple"

class XmlTranslator(Translator):
    """XmlTranslator - Class for parsing XML, XML documents are produced
    without translation
    """

    def __init__(self, methods, sources=None, basename=None,
                 stream=None, **kwargs):
        """
        xml_translator - XML parsing instance
        :param methods: XmlMethods object
        :param sources:  dictionary of XML sources. Currently only supports
                         a single entry.  Entry is keyed by document
                         identifier with a value that is another dictionary
                         containing information about the type, filename,
                         a stream source, and potential other unused keys.
        :param basename: basename of document to be added
        :param stream: File stream to associate with basename, only use this
           if sources is None

        Additional keyword arguments inherited from parent
        :param debug:  print information if True
        """

        # should only have one, not both
        if sources and stream:
            raise ValueError("Must provide sources or stream, but not both")
        elif sources is None and stream is None:
            raise ValueError("Must provide sources or stream")

        # We either have source or stream
        # Construct a source dictionary if given a stream.
        if stream:
            # build sources dictionary from stream
            sources = {basename: {'stream': stream, 'sql': False}}

        super().__init__(methods, sources, **kwargs)

        source_key = list(sources.keys())[0]
        self.sources = sources
        self.current_source = source_key

        # Verify that we only have a single document
        if len(self.sources) > 1:
            raise ValueError(f"{self.__class__} only supports a single document source")

        try:
            self.sourcedoc = sources[source_key]['stream'].read()
        except KeyError:
            raise KeyError("XML file names must include .xml extension")

        # Decode the byte stream
        try:
            tree = ET.fromstring(self.sourcedoc)
        except ET.ParseError as e:
            line, col = e.position
            # We might get the encoding wrong here, the whole point of using
            # element tree is to process the encoding properly, but the
            # document was mal-formed.
            src = str(self.sourcedoc, "UTF-8")
            lines = dbxmlInterface.error.show_context(
                src, line, col, highlight=True)
            lines.insert(0, f"Unable to parse XML: {e.msg}")
            e.msg = "\n".join(lines)
            raise

        self.sourcedoc = ET.tostring(tree, encoding="unicode")
        if isinstance(self.sourcedoc, bytes):
            # Unclear why this is happening for XML soem files.
            # They should be decoded after a round trip
            # through element tree.  The files in question are
            # marked as a UTF-8 encoding and were generated by
            # Nilus, maybe its the wrong encoding?
            self.sourcedoc = self.sourcedoc.decode()

        self.doc_type = 'single'
        self.type = "XML"
        self.current_type = "XML"

        self.produced = False
    
    def __iter__(self):
        """
        iter() produce an iterator for documents.  This is not thread safe
        :return: xml_translator
        """
        self.produced = False
        return self
    
    def __next__(self):
        if self.produced:
            raise StopIteration
        else:
            self.produced = True
            return self.sourcedoc
