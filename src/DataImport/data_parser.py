'''
Created on Mar 29, 2012

@author: Daniel Hwang/Marie A. Roch/Sean Herbert
'''

import csv
import os
import re
from io import StringIO

from xml.sax.saxutils import XMLGenerator
from xml.sax.xmlreader import AttributesNSImpl

import xml.dom.minidom
from xml.dom.minidom import Node

import lxml.etree as ET

import numbers

# Windows packages
import pythoncom

# site installed packages
import dbxml
import pyodbc
import pandas as pd

# local modules
import dbxmlInterface.error
import excel
import iso8601
from XML.transform import map2elem
from mediators.ODBC import RowData, RowDataFetchMany
import DataImport
from .timestamp import timestamp_parse
from .Error import MissingField

# longitude/latitude
longlat_re = re.compile(r"""
   ^(?P<deg>\d+)    # degrees
    (  # minutes
     ([^\d]+   
      (?P<m>\d+(\.\d*)?)
     )
     ( # seconds
      [^\d]+?
      (?P<s>\d+(\.\d*)?)
     )?
    )?
   (  
    ([^\d]*\s+)?  # possible text followed by a direction
    (?P<direction>[NSEW])
    \s*$
   )?
   """, re.VERBOSE | re.IGNORECASE)


def parse_longlat(value, vtype):
    """
    Convert a longitude/latitude string to an appropriate value
    :param value: String or number
    :param vtype: Latitude or Longitude
    :return: decimal number in degees N [90, -90] or E [0, 360)
    """

    if not isinstance(value,numbers.Number):
        m = longlat_re.match(value)
        if m:
            groups = ["deg", "m", "s"]
            dms = []
            for g in groups:
                try:
                    v = float(m.group(g))
                except TypeError:
                    v = 0.0
                dms.append(v)
            
            degrees = dms[0] + dms[1] / 60.0 + dms[2] / 3600.0
        
        # All longitude and latitudes are specified with
        # 360 degrees (no negative values)
            direction = m.group("direction")
            if direction is not None:
                direction = direction.upper()
                if direction == 'W':
                    degrees = 360 - degrees
                elif direction == 'S':
                    degrees = -degrees
            value = "%f" % (degrees)
        return value
    else:
        if value < 0 and "lon" in vtype.lower():
            # Longitudes must be non-negative degrees East
            value = 360 + value
        return value


class data_parser:
    """
    Class for parsing a set of workbook_parser and translating them into XML
    """

    # Dictionary containing resource objects that need to be deleted
    resources = {}

    # Dictionary of child elements of SourceMap/Directives
    # that have special significance.
    # Keys represent a category and values thee nodes that can match them
    directives = DataImport.directives

    param_declarations_sheet = "Effort"
    # in the xml map, this will be where the parameters will be placed,
    # the marker to start parameterized processing
    parse_params_source_val = "[*Parameters]"
    ordered_params_list = ['Subtype', 'ReceivedLevel_dB', 'MinFreq_Hz', 
                           'MaxFreq_Hz', 'PeakFreq_Hz', 'Peaks_Hz', 
                           'Duration_s', 'Sideband_Hz']
    
    # Special sheet names
    detections_sheet = "Detections"
    metadata_sheet = "Metadata"

    # Namespaces and modules that queries may need
    tethys_module = \
        'import module namespace lib="http://tethys.sdsu.edu/XQueryFns" at "Tethys.xq";\n'
    tethys_namespace = \
        'import schema namespace ty="http://tethys.sdsu.edu/schema/1.0" at "tethys.xsd";\n'
    tethys_modnamespace = tethys_module + tethys_namespace

    translator = None

    # empty attribute
    empty_attrib = AttributesNSImpl({}, {})
    
    # parsing for namespaces
    schema_loc_re = re.compile('"?(?P<namespace>\S+)\s+(?P<location>\S+)"?')
    schema_abbr_re = re.compile("xmlns:(?P<abbr>\S+)")
    
    # dbxml XQuery parsing
    user_err_re = re.compile(r"Error: User-requested error \[(?P<errtype>[^\]]+)")

    # Functions for transforming case of strings
    case_transforms = {
        "lower":lambda x: x.lower(),
        "upper":lambda x: x.upper(),
        "capitalize":lambda x: x.capitalize(),
        "title":lambda x: x.title()
    }
                    
    def __init__(self, methods, translator=None, speciesabbr=None, debug=False):
        """Process a tuple oriented data source.  This is an abstract class and
        should not be called directly.  Subclasses are used to implement
        how data is read from tuple oriented data sources such as relational
        databases or spreadsheets.
        
        given a column-header to xml tag mapping translator
        methods - XmlMethods instance
        translator - XML to provide translation from input fields to output
        speciesabbr - Which set of species abbreviations should be used?        
        """
        
        # Translation between species encodings and integrated taxonimic
        # information system (ITIS; http://www.itis.gov/)
        self.code2tsn = dict()
        
        self.debug = debug 
        self.methods = methods

        self.warnings = dict()  # provide feedback, but let the user proceed
        self.errors = dict()  # errors that allow procesing to continue 

        self.code2tsn = ItisCodeMap(self.methods, speciesabbr)

        # Retrieve the translation map
        try:
            xmldoc = self.get_translator(translator)
        except Exception:
            self.close()  # Release data source
            raise  # propagate the error to caller
        
        # Access various parts of the document
        try:
            self.resources['attributes'] = self.methods.query_doc("./Mapping/DocumentAttributes", xmldoc)
        except dbxml.XmlQueryEvaluationError:
            self.warnings['Translator'] = \
               "%s has no attributes specified, " % (translator) + \
               "schema validation will not be possible"
        try:
            self.resources['directives'] = self.methods.query_doc("./Mapping/Directives", xmldoc)
            
        except dbxml.XmlQueryEvaluationError:
            raise ValueError('No Directives element in %s' % (translator))
        
        # Find attributes to be included in output document
        self.doc_attributes, self.namespace = self.parse_attributes(
            self.resources['attributes'].__next__().asString())
        
        self.species_abbr = speciesabbr
        self.missing_abbrevs = dict()

        if self.species_abbr is not None:
            q_str = self.tethys_module + \
                    f' lib:AbbreviationMapExists("{self.species_abbr}")'
            queryRslt = self.methods._query(q_str)
            valid = queryRslt.__next__().asBoolean()
            if not valid:
                self.close()  # Release data source
                raise ValueError("Unknown SpeciesAbbreviation map name %s" % (
                        self.species_abbr))
        
        # No parameter parsing until someone uses the parameters="define"
        # attribute on a data source 
        self.parse_params = False
        
        # List of special words in a translation tree
        self.reserved_words = []
        categories = set(self.directives.keys()).difference(["text", "keys"])
        for c in categories:
            if isinstance(self.directives[c], list):
                self.reserved_words.extend(self.directives[c])
            else:
                self.reserved_words.append(self.directives[c])
            
        # Parse the directives into a document object model (DOM) tree
        # This is somewhat inefficient as we are serializing and unserializing
        # the document, but the code was originally written using the minidom
        # parser and it's a bit of work to change it.  Given that the document
        # is typically small, this should not be much of an issue.
        
        directives = self.resources['directives'].__next__().asString()
        node = xml.dom.minidom.parseString(directives)
        # get Directives node
        elements = [n for n in node.childNodes if n.nodeType == Node.ELEMENT_NODE]
        # get children of Directives
        elements = [n for n in elements[0].childNodes if n.nodeType == Node.ELEMENT_NODE]
        if len(elements) != 1:
            raise ValueError("Directives must have a single child element "
                             "which will be the XML document")

        root = elements[0]
        if root.nodeName in self.directives['data']:
            self.root_data = root
            elements = [n for n in root.childNodes 
                        if n.nodeType == Node.ELEMENT_NODE]
            if len(elements) != 1:
                raise ValueError(
                    "First Directive is a data source, "
                    "requires single document element as child")
            self.root_translation = elements[0]
        else:
            self.root_data = None  # Single document; first child is a node
            # Store the root of the translation tree
            self.root_translation = elements[0]
            
        # Verify that the root of the translation tree is
        # acceptable as an enclosing document name
        if self.root_translation.nodeName in self.reserved_words:
            raise ValueError(
                f"The document root, <{self.root_translation.nodeName}>, "
                "is a processing directive.")

        self.param_types = {}
        self.default_values = {}

    def close(self):
        """
        close - Release any open resources
        """
        pass  # To be implemented by subclass if needed
   
    def __iter__(self):
        """iter()
        Return an iterator that will yield one or more documents
        when called with next.  After the last document is called,
        a StopIteration exception is generated.
        """

        # If the data source (e.g. source map Table directive)
        # comes before the first node of the source map, then we
        # produce multiple documents
        self.documents_processed = 0
        if self.root_data is not None:
            # Retrieve the rows that will be used to create
            # multiple documents
            self.datarows = self.get_source(self.root_data, debug=self.debug)
            self.datarow_it = iter(self.datarows)
        else:
            self.datarows = None

        return self
     
    def __document_start__(self, docname):
        """Start an XML document named docname"""
        self.xmlstr = StringIO()
        self.xml = XMLGenerator(self.xmlstr)
        # Start the new document
        self.xml.startDocument()
        self.xml.startElement(docname, self.doc_attributes)
        
    def __document_end__(self, docname):
        """Close off an existing XML document named docname"""
        self.xml.endElement(docname)         
        self.xml.endDocument()

    def multiple_document_check(self):
        """
        Is this data source capable of returning multiple documents?
        :return: True|False
        """
        return self.root_data is not None

    def __next__(self):
        """Generates the next document that can be accessed via get_xml()"""

        docname = self.namespace + self.root_translation.nodeName
        self.errors = {}
        self.warnings = {}

        if self.datarows is not None:
            # Get row that will be used for the current document
            r = next(self.datarow_it)
            # Reset document information
            self.row_num = self.datarow_it.row_idx()
            # Generate document
            self.documents_processed += 1
            self.__document_start__(docname)
            self.process_row(self.root_translation, r, self.debug)
            self.__document_end__(docname)
        else:
            # Only one document
            if self.documents_processed > 0:
                raise StopIteration
            else:
                self.documents_processed += 1
                # Generate document
                self.__document_start__(docname)
                # Process each child of the root
                for c in self.root_translation.childNodes:
                    self.translate(c, self.debug)
                self.__document_end__(docname)

        xml = self.get_xml()
        return xml

    def __del__(self):
        if self.translator:
            del self.translator
    
    def get_position(self):
        """Return information about the current location:
        (name of file/database, table/sheet, row_num)
        """
    
        return self.source, self.current_stream, self.row_num
    
    def get_translator(self, translator):
        """
        Query the SourceMaps collection to pull out the parse specification
        """
        
        # Pull the translation map
        result = self.methods._query(
            f'collection("SourceMaps")/Mapping[Name="{translator}"]')
        # Responsible for deleting later...
        self.resources['translator'] = result
        if result.size() < 1:
            raise ValueError(f"Unable to find SourceMap named {translator}")
        elif result.size() > 1:
            # Shouldn't ever get here...
            raise ValueError("Mapping %s is not unique" % (translator))
        sole_result = next(result)
        return sole_result.asDocument()          

    def get_current_source_doc(self, close=False):
        """
        In derived classes will return the source document if available.
        In this class, returns None

        :param close: close the document if open
        :return: None
        """
        return None

    def get_node_text(self, node, strip=True):
        """
        Extract the first text child of a DOM node
        :param node: DOM node
        :param strip: if True, strip whitespace
        :return: First child's text, or None if nothing found
        """
        found = None
        for child in node.childNodes:
            if child.nodeType == Node.TEXT_NODE:
                found = child.data
                if strip:
                    found = found.strip()
                break  # bail out of loop
            
        return found

    def set_default_value(self, name, value):
        """
        Set a name and value pair that can be used in a sourcemap.
        If the field appears in the current row of the spreadshet, we use
        the row value.  Otherwise, we check the default_value dictionary
        to see if a value can be substituted.
        :param name:  name of value
        :param value:  value itself
        :return: None
        """
        self.default_values[name] = value

    def parse_attributes(self, attr_str):
        "Process any attributes that are to be assigned to the instance document"
        
        # Pull out all of the attribute/value pairs
        attributes = {}
        node = xml.dom.minidom.parseString(attr_str)  # document root
        node = node.childNodes[0]  # DocumentAttributes tag
        for attrnode in self.getChildrenByTagName(node, 'Attribute'):
            tags = ['Name', 'Value']
            values = [None, None]
            for idx in range(len(tags)):
                # Get first child with specified tag and pull its value
                child = self.getChildrenByTagName(attrnode, tags[idx])
                if len(child):
                    values[idx] = self.get_node_text(child[0])
                    # strip quotes if needed
                    if len(values[idx]) >= 2 and \
                        values[idx][0] == '"' and values[idx][-1] == '"':
                        values[idx] = values[idx][1:-1]
                     
            # Did we get a name/value pair?
            if all([v is not None for v in values]):
                attributes[values[0]] = values[1]
                
        # If user specified a namespace, store the abbreviation
        # so that we can output it for the top level element
        abbr = ""
        try:
            ns = attributes['xsi:schemaLocation']  # Get namespace
        except Exception:
            ns = None
        
        if ns:
            match = self.schema_loc_re.match(ns)
            if match:
                ns = match.group("namespace")
                # find namespace in list of values
                for (k, v) in list(attributes.items()):
                    if v == ns:
                        # key probably has xmlns:SomeAbbrev
                        # pull out the abbreviation
                        match = self.schema_abbr_re.match(k)
                        if match:                        
                            abbr = match.group("abbr") + ':'
                            break
                    
        return (attributes, abbr)  

        
    def translate(self, doc, debug=False):
        name = doc.nodeName      
        if debug:
            print("Translate %s" % (name))

        if name in self.directives['data']:
            # User has specified a new data source
            rows = self.get_source(doc, debug)
            
            # Is the user defining parameters in this data source?
            # If so, we will be associating parameter names with the 
            # positional parameter columns Parameter 1, Parameter 2, ...
            # on a per species and call type basis.
            declare_params = doc.getAttribute("parameters").lower() == "define"
            if declare_params:
                # From this point out, the parameter expansion keyword
                # is valid
                self.parse_params = declare_params
                
            # Process each row_num in the data source
            for r in rows:

                # KLUDGE:  At SIO SWAL, some of our early detection workbooks
                # denoted effort as a detection entry in the [Species Code]
                # field.  Skip any such rows
                try:
                    sc = r['Species Code']
                    if sc is not None and 'effort' in sc.lower():
                        continue
                except (KeyError, TypeError, AttributeError):
                    pass  # no need to do anything
                
                # These may be populated with Species, Call and Subtype
                # if the fields are present.  They define the active 
                # record and are used for processing parameters
                # whose names change
                self.active = [None, None, None]
                self.param_values = {}  # Reset parameter dictionary for this row
                
                # Generate XML for the row_num
                self.row_num = rows.row_idx()
                self.process_row(doc, r, self.debug)

                if declare_params == True:
                    self.declare_parameters(r, self.debug)

            
        else:
            # Output node unless it is one of the following types
            output = not(doc.nodeType in [Node.TEXT_NODE, Node.COMMENT_NODE])
            if output:
                self.xml.startElement(doc.nodeName, doc.attributes)
            for child in doc.childNodes:
                self.translate(child, debug)
                
            if output:
                self.xml.endElement(doc.nodeName)    
    
    def getChildrenByTagName(self, node, name):
        "Similar to DOM getElementsByTagName, but only retrieves first generation descendants"
        
        return [cn for cn in node.childNodes if cn.nodeName == name]
            
    def get_problems(self, clear=False, xml=None):
        """get_problems - Report current problems and warnings 
           clear - clear problem list for next report
           xml - Send report to specified XMLGenerator rather 
                 than returning string"""
        
        
        if xml:
            result = None # Nothing to return
            
            # Does the xml object support newline() ?
            supports_nl = callable(getattr(xml, 'newline', None))
            # Construct function nl() that calls newline if xml supports it,
            # otherwise does nothing
            nl = lambda : xml.newline() if supports_nl else None
            
            # Process warnings and errors
            for (problemcat, problems) in [("Warnings", self.warnings),  
                                     ("Errors", self.errors)]:
                if len(problems) > 0:
                    xml.startElement(problemcat, {})
                    nl()
                    dbxmlInterface.error.dict2msgXML(problems, xml)
                    xml.endElement(problemcat)
                    nl()
        else:
            msgs = []
            # Process warnings and errors
            for (problemcat, problems) in [("Warnings", self.warnings),  
                                     ("Errors", self.errors)]:
                if len(problems) > 0:
                    msgs.append("%s %s ----\n%s"%(
                        problemcat, self.source, 
                        dbxmlInterface.error.dict2msg(problems, 2)))
            
            if len(msgs) > 0:
                result = "Problems occurred parsing %s\n%s"%(self.source, "\n".join(msgs))
            else:
                result = ""
            
        return result    
                    
    def has_errors(self):
        "has_errors - Have we encountered any errors?"
        return len(self.errors) != 0
    
    def has_warnings(self):
        "has_warnings - Have we encountered any warnings?"
        return len(self.warnings) != 0
        
    def declare_parameters(self, row, debug):
        """Note parameters associated with this species/call
           Assumes the species and call have already been processed"""
           
        # Add mappings from named parameter to column name
        # for each non-empty parameter entry.
        mapping = dict()
        for name in row:
            if name.startswith('Parameter') and row[name] is not None:
                field = map2elem(row[name])
                mapping[field] = name
            
        self.param_types[tuple(self.active)] = mapping
                
    def process_row(self, node, row, debug=False):
        """
        Perform translation for one row_num of a table
        :param node: DOM node to process
        :param row:  dictionary row[col] = data
        :param debug:  True|False output debugging information
        :return: None
        """

        # Iterate over all children of the translation map looking 
        # for "Entry" nodes to process
        # If other types of nodes are found, will be copied into the output


        for cn in node.childNodes:
            if self.debug:
                print(f"Processing {cn.nodeName}")

            if cn.nodeName == self.directives['condition']:
                # this element's creation is conditional
                self.process_condition(cn, row)
                
            elif cn.nodeName == self.directives['predicate']:
                continue
                
            elif cn.nodeName == self.directives['entry']:
                # This node of the map indicates that we should process
                # a value from the current row
                (dest_tag, value) = self.process_row_value(cn, row)
                source_val = value[0]
                if self.debug:
                    print(f"<{dest_tag}> -> '{source_val}' ")
                if source_val is None:
                    continue
                
                attr_dict = value[1]
                for an in self.getChildrenByTagName(cn, self.directives['attribute']):
                    (a_dest_tag, a_source_val) = self.process_row_value(an, row)
                    if a_source_val[0] is not None:
                        attr_dict[a_dest_tag] = a_source_val[0]
                    
                attr = AttributesNSImpl(attr_dict, {})
    
                # XML requires a string
                if source_val and not isinstance(source_val, str):
                    source_val = str(source_val)
                  
                if source_val and len(source_val.strip()) > 0:
                    try:
                        self.xml.startElement(dest_tag, attr)
                    except AttributeError:
                        (dest_tag, value) = self.process_row_value(cn, row)
                    self.xml.characters(source_val)
                    self.xml.endElement(dest_tag)

            elif cn.nodeName in self.directives["data"]:
                self.translate(node)

            elif cn.nodeName not in [self.directives["text"], self.directives["comment"]]:
                if debug:
                    print(f'Element: {cn.nodeName} found')
                self.xml.startElement(cn.nodeName, AttributesNSImpl({}, {}))
                self.process_row(cn, row, debug)
                self.xml.endElement(cn.nodeName)
            else:
                if cn.nodeName == self.directives["text"]:
                    value = cn.nodeValue.strip()
                    if value != "":
                        self.xml.characters(value)
                        if debug:
                            print(f"Output text:  {value}")
    
    # Entries consist of field names in brackets with fixed text before
    # or after, e.g.:  [Hours]:[Minutes]:[Seconds]
    schema_columns_re = re.compile('([^\[\]]*)\[([^\[\]]*)]([^\[\]]*)')
    
    def process_condition(self, node, row,debug=False):
        """Process a conditional directive"""
        
        #Black Mesa operations...
        operations = {
                      "empty":          lambda val: val is None,
                      "iftrue":         lambda retain: bool(retain),
                      "iffalse":        lambda retain:not bool(retain),
                      "matches":        lambda val,exp: val == exp,
                      "matchesregexp":  None, 
                      }
        conditions = dict()
        nodes = self.getChildrenByTagName(node, self.directives['predicate'])
        prednode = nodes[0]      
        for c in ['operand', 'operation']:
            try:
                ops = self.getChildrenByTagName(prednode, self.directives[c])
            except KeyError:
                #need sourcemap error here
                pass
            for op in ops:
                try:
                    if c == 'operation':                   
                        for o in ['op','retain']:
                            try:
                                conditions[o] = op.getAttribute(o)
                                if conditions[o]=='matches':
                                    exp = op.firstChild.nodeValue#.encode('UTF-8')
                                conditions[o] = operations[conditions[o]]
                            except KeyError:
                                pass
                    if c == 'operand':
                        conditions[c] = op.firstChild
                        conditions[c] = conditions[c].nodeValue.strip()
                        
                except IndexError:
                    pass
                except KeyError:
                    pass
       
        #conditions and ops are all set up in the dict, lets see if it passes...
        if conditions['operand'] is not None:
            #there is a dependent field, lets find it
            matches = self.schema_columns_re.findall(conditions['operand'])
        if len(matches) >= 1:
            value = [row[m[1]] for m in matches]
            
        #have the values, now test the conditions
        if conditions['op'] == operations['empty']:
            opresult = conditions['op'](value[0])
            result = conditions['retain'](opresult)
        elif conditions['op'] == operations['matches']:
            opresult = conditions['op'](exp,value[0])
            result = conditions['retain'](opresult)
        elif conditions['op'] == operations['matchesregexp']:
            pass
        
        if result:
            # condition passed, process the row
            self.process_row(node, row, debug)
            

    def process_row_value(self, node, row, debug=False):
        """
        Perform data mapping for current node
        :param node: DOM node
        :param row: current row
        :param debug: Output debug inforamtion
        :return: output_element, [value, attributes]
        """

        # data, kind, default optional although we really expect source or kind
        transform = dict()  # dictionary of nodes of various types 
        for d in ['dest', 'source', 'kind', 'default']:
            nodes = self.getChildrenByTagName(node, self.directives[d])
            try:
                # Value should be the first child of the node.
                # In the case of duplicate names, use the last one

                transform[d] = nodes[-1].firstChild
                transform[d] = transform[d].nodeValue.strip()
                if d == 'source':
                    attr = nodes[-1].attributes
                    if 'case' in attr.keys():
                        case = attr['case'].value
                        if case not in self.case_transforms:
                            raise ValueError(
                                "Source element has a bad case value <Source case=("
                                f'{",".join(self.case_transforms.keys())})>')
                        case_transform = self.case_transforms[case]
                    else:
                        case_transform = None
                if d == 'kind':
                    transform[d] = transform[d].lower()
            except IndexError:
                transform[d] = None
        
        if self.parse_params and \
            transform['source'] == self.parse_params_source_val:
            # User has specified special parameter processing
            # Multiple values are output as a side effect and
            # we return None to indicate that there is nothing
            # to output
            self.row_parameter_values(transform['dest'], row, debug)
            # Nothing to output - special case
            return (None, [None, {}])

        # Find each named field along with any preceding or following text
        # Each match is a triple (preceding text, field name, following text)
        if transform['source'] is not None:
            # Multiple fields may be permitted, 
            # e.g. <Source> [YYMMDD]T[HHMMSS]Z </Source>
            # These are grouped into a list of tuples where index 1 is the 
            # field and indices 0 and 2 show the extra text.  
            # For our example:  [(' ', 'YYMMDD', 'T'), ('', 'HHMMSS', 'Z ')
            matches = self.schema_columns_re.findall(transform['source'])
        else:
            matches = []

        # Assume output is a string if we have more than one entry or nonempty text
        # to be prepended or appended.
        source_val = None
            
        if len(matches) >= 1:
            # Substitute the source fields with their values from the row_num
            try:                
                # Example
                # <Entry> <Source> [YYMMDD]T[HHMMSS] </Source>
                #         <Dest> Start </Dest>
                
                # Get values associated with each field name
                # e.g. ['2012-09-23', '08:32:22']
                values = [row[m[1]] if m[1] in row else self.default_values[m[1]] for m in matches]
                # Put row values in context
                value_str = [m[0] + str(v) + m[2] for m, v in zip(matches, values)]
                value_str = "".join(value_str)

                if transform['kind'] == 'datetime':
                    t = timestamp_parse(values, matches)
                    if t is None:
                        raise MissingField(transform['source'][0])
                    source_val = [t, {}]
                else:
                    missing = []
                    for idx in range(len(values)):
                        if values[idx] is None:
                            missing.append(transform['source'][idx])
                    if len(missing) > 0:
                        raise MissingField(" ".join(missing))
                    # Pair it with the matches list from above, e.g.
                    # [['2012-09-23', (' ', 'YYMMDD', 'T')], 
                    #  ['08:32:22', ('', 'HHMMSS', 'Z ')]
                    # ]
                    # and format the correct string using pieces of each list
                    attr = dict()
                    strvals = []
                    for m in range(len(values)):
                        [v, d] = self.process_source_node(values[m], transform)
                        if v is not None:
                            if isinstance(v, float) and int(v) == v:
                                # Avoid decimal points in whole numbers
                                v = int(v)
                            strvals.append(f"{matches[m][0]}{v}{matches[m][2]}")
                            for key in list(d.keys()):
                                attr[key] = d[key]
                        
                    if len(strvals) > 0:
                        strval = "".join(strvals)
                        if case_transform:
                            strval = case_transform(strval)
                        source_val = [strval, attr]
                    else:
                        source_val = [None, attr]

            except KeyError:
                if transform['default']:
                    attr = dict()
                    strvals = []
                    [v, d] = self.process_source_node(
                        transform['default'], transform)
                    if v is not None:
                        for key in list(d.keys()):
                            attr[key] = d[key]
                    source_val = [v, attr]
                else:
                    source_val = None
            except ValueError as v:
                source_val = None
                if hasattr(v, 'message'):
                    self.note_problem(self.errors, v.message)
                else:
                    self.note_problem(
                        self.errors, "ValueError",
                        f'importing {transform["source"]}, ' +
                        f'expanded to: "{value_str}": ' + str(v))
            except MissingField as m:
                # one or more values were missing, use default if present
                if transform['default']:
                    attr = dict()
                    strvals = []
                    [v, d] = self.process_source_node(
                        transform['default'], transform)
                    if v is not None:
                        for key in list(d.keys()):
                            attr[key] = d[key]
                    source_val = [v, attr]
                else:
                    source_val = None
            
                                        
        elif transform['default']:
            source_val = [transform['default'], {}]
    
        if source_val is None:
            source_val = [None, {}]
            
        if debug:
            print(f"{transform['source']}->{transform['dest']}="
                  f"{source_val} {transform['kind']}")

        return (transform['dest'], source_val)
                        
    def row_parameter_values(self, params_tag, row, debug=False):
        """Process parameters from a data row_num
           Has the side-effect of modifying self.param_values"""
        
        # Retrieve the parameter names associated with this entry
        # Note that some parameters may have been set elsewhere
        # through special processing.
        names = self.param_types.get(tuple(self.active), None)
        if names is not None and len(names) > 0:
            # Populate parameters
            for param_named, param_numbered in names.items():
                try:
                    value = row[param_numbered]
                    if value is not None:
                        self.param_values[param_named] = value
                except KeyError:
                    pass

        # Output parameters
        if len(self.param_values) > 0:
            self.xml.startElement(params_tag, self.empty_attrib)
            
            # Process ordered parameters
            for p in self.ordered_params_list:
                if p in list(self.param_values.keys()):
                    self.xml.startElement(p, self.empty_attrib)
                    self.xml.characters(str(self.param_values[p]))
                    self.xml.endElement(p)
                    del self.param_values[p]
            
            # Anything remaining is UserDefined
            if len(self.param_values) > 0:
                self.xml.startElement('UserDefined', self.empty_attrib)
                # Output remaining in a sorted list
                key_list = list(self.param_values.keys())
                key_list.sort()
                
                for k in key_list:
                    self.xml.startElement(k, self.empty_attrib)
                    self.xml.characters(str(self.param_values[k]))
                    self.xml.endElement(k)
                self.xml.endElement('UserDefined')
            
            self.xml.endElement(params_tag)  
        
        return
            
    def process_source_node(self, source_val, transform):
        """Perform type based processing user's data"""

        if transform is not None:
            kind = transform['kind']
            if source_val is None:
                # if source_val = None, then return an empty string
                source_val = ['', {}]
            elif not kind:
                return [source_val, {}]
            elif kind == 'string':
                return [source_val, {}]
            else:
                # Process specific types of data
                if kind == 'datetime':
                    try:
                        source_val = [iso8601.DateTime(source_val), {}]
                    except Exception:
                        self.note_problem(self.errors, 'Unable to parse date %s' % (str(source_val)))
                elif kind == 'integer':
                    try:
                        source_val = [int(float(source_val)), {}]
                    except TypeError:

                        self.note_problem(self.errors, 'Cannot convert to Integer')
                elif kind == 'number':
                    try:
                        source_val = [float(source_val), {}]
                    except TypeError:
                        self.note_problem(self.errors, 'Cannot convert to Number')
                elif kind == 'speciescode':

                    try:
                        tsngroup = self.code2tsn[source_val]
                    except ValueError as e:
                        self.note_problem(self.errors, str(e), line=False)
                        # Allow processing to continue with dummy value to
                        # avoid flame and die processing
                        tsngroup = ["unknown species", {}]

                    self.active[0] = source_val  # Track active species
                    source_val = tsngroup

                elif kind == 'latlong':
                    # longitude/latitude
                    source_val = [parse_longlat(source_val, transform['dest']), {}]

                elif kind == 'calltype':
                    # escaping seems to be automatically done by the xml creator
                    # import cgi
                    # source_val = cgi.escape(source_val) # escapes & > and <
                    i = source_val.find('/')
                    if i != -1:
                        # split into call & subtype
                        subtype = source_val[i + 1:]
                        source_val = source_val[0:i]

                        # Store for parameters if needed
                        self.param_values['Subtype'] = subtype
                    else:
                        subtype = None

                    # Track active call & subytpe, used in parameter parsing
                    self.active[1:2] = [source_val, subtype]

                    # Put in standard form, value, attributes
                    source_val = [source_val, {}]

                elif kind == 'callsubtype':
                    # Pull out the subtype
                    i = source_val.find('/')
                    if i != -1:
                        source_val = [source_val[i + 1:], {}]
                    else:
                        source_val = [None, {}]
                else:
                    raise ValueError('unknown Kind %s specified' % (kind))
        else:
            # untranslated value with no attributes
            source_val = [source_val, {}]

        return source_val
    
    def get_xml(self):
        "Produce XML from parsed workbook"
        return self.xmlstr.getvalue()
    
    def validate_effort(self):
        ''' maybe do this during doc insertion? '''
        raise 'not currently implemented'
    
        
    def note_problem(self, *args, **kwargs):
        """note_problem - Record a problem with the specified problem dictionary
        
        *args - First argument must be the problem dictionary to which 
        the problem will be appended.
        Remaining elements of *args are string arguments that will be 
        translated to element tags.  The last string is either treated as an 
        element or a value (see below)
        
        **kwargs - Set of keyword arguments, all of which are optional:         
           line=True (default)|False - Note line number.  If false,
               the last string of *args is treated as a value.
           datastream=True (default)|False - note stream from which data came
               (e.g. sheet from workbook)
               
        """
        # Find out where we are in the parse
        [datacollection, datasource, line] = self.get_position()
        
        recordstream = True
        try:
            recordstream = kwargs["datastream"]
        except KeyError:
            pass  # use default
        
        args = list(args)

        details = f"{datasource}\n" if recordstream else ""

        recordline = kwargs.get("line", True)
        if recordline:
            details = f"line {line} " + details

            
        dbxmlInterface.error.addErrMsg(*args, details)
            
    def cannot_overwrite(self):
        """cannot_overwrite - Document exists in the container to which 
           it will be added and overwrite permission has not been granted"""
        
        self.note_problem(self.errors, "Document Exists",
                "Document exists and cannot be overwritten without permission",
                datastream=False)

class workbook_parser(data_parser):
    "Parse an Excel workbook"

    data_source = "Sheet"  # Name of data source keyword
        
    def __init__(self, methods, spreadsheet, translator=None, speciesabbr=None, debug=False):
        """Create a workbook parsers
        methods - instance of XmlMethods
        spreadsheet - Name of spreadsheet file to parse
        translator - XML document to translate from spreadsheet fields to 
           schema.  Will look on sheet Metadata field Parser if not specified.
        speciesabbr - Species abbreviation map name to translate from local
           names to ITIS codes.  Will look on sheet Metadata field 
           SpeciesAbbreviation if not present
        debug - Output debug msgs
        """

        self.source = spreadsheet  # Remember who you are...
        workbook = os.path.basename(spreadsheet)  # user-friendly name

        # open the detection file
        try:
            self.xls = excel.ExcelDocument(spreadsheet)
        except pythoncom.com_error as e:  # @UndefinedVariable (generated at runtime)
            self.xls = None
            raise ValueError(
                f"Unable to open {workbook}.  Server COM error message:\n"
                f"{e.excepinfo[1]}\n{e.excepinfo[2]}")

        self.sheets = self.xls.sheets()

        # If the source has a metadata sheet and they
        # have not specified a translator or speciesabbr,
        # we may be able to recover these from the workbook metadata
        if translator is None or speciesabbr is None:
            row = None  # no data yet
            meta = self.metadata_sheet + '$'  # ODBC appends $
            # See if metadata sheet present (case independent)
            if meta.lower() in map(str.lower, self.sheets):
                metadata = self.xls.sheet(meta)
                try:
                    data = iter(metadata)
                    row = next(data)  # only expecting one row_num
                except Exception as e:
                    self.close()

            if row is not None:
                # Workbook provided a metadata sheet, extract translator
                # and species abbreviations if needed and present
                parser_fld = 'Parser'
                if translator is None and parser_fld in row:
                    translator = row[parser_fld]

                species_fld = 'SpeciesAbbreviation'
                if speciesabbr is None and species_fld in row:
                    speciesabbr = row[species_fld]

            if debug:
                print(f'parser map:  {translator} abbreviation map {speciesabbr}')

        if translator is None:
            self.close()
            raise ValueError(
                "Import source mapping must be specified by user "
                f"when not specified by {parser_fld} field of "
                f"sheet {self.metadata_sheet} in workbook {workbook}")

        super().__init__(methods, translator, speciesabbr, debug)

        # Set up values for DefaultDocumentIdentifier
        docname, _ = os.path.splitext(workbook)
        self.default_values["DefaultDocumentIdentifier"] = docname


    def close(self):
        if self.xls:
            self.xls.close()
            self.xls = None
            
    def __del__(self):
        if self.xls:
            self.xls.close()
        data_parser.__del__(self)

    def get_source(self, sheetnode, debug=False):
        
        if sheetnode.nodeName != "Sheet":
            raise ValueError("Expecting SourceMap directive Sheet, found %s"%(
                sheetnode.nodeName))
        sheetname = sheetnode.getAttribute("name")
        optional = sheetnode.getAttribute("optional").lower() == "true"
        
        if sheetname + '$' in self.sheets:
            self.current_stream = sheetname
            if debug:
                print("Parsing sheet: %s" % (sheetname))
            xls_sheet = self.xls.sheet(sheetname + '$')
            sheet_iterator = iter(xls_sheet)

            return RowData(f"select * from {sheetname}", sheet_iterator,
                           sheet_iterator.columns(), firstrow=2)

        else:
            if optional:
                return []
            else:
                raise ValueError("Sheet %s not found" % (sheetname))
    
class connector(object):
    
    @classmethod
    def MySQL(cls, Provider=None, Driver="{MySQL ODBC 5.1 Driver}",
              Server="localhost",
              Database=None,
              Port=None,
              User=None,
              Password=None,
              Option=None
              ):       
        # Works with MySQL 4.1 and greater
        # Use Provider MSDASQL for 64 bit according to www.connectionstrings.com
    
        connectionStr = cls.__BuildConnectionStr__([
            "Provider", "Driver", "Server", "Database", "Port",
             "User", "Password", "Option"], locals())
        if Database is None:
            Database = "MySQL database"
        return connector(connectionStr, Database) 
    
    @classmethod
    def MicrosoftExcel(cls,
            Driver="{Microsoft Excel Driver (*.xls, *.xlsx, *.xlsm, *.xlsb)}",
            DBQ=None,
            Extended_Properties='"Excel 12.0;HDR=Yes;IMEX=1"'):
            "Open a Microsoft Excel spreadsheet, Data_Source is the spreadsheet filename"

            if DBQ is None:
                raise ValueError("Path to Excel file (Data_Source) must be specified")
            connectionStr = cls.__BuildConnectionStr__(
               ["Driver", "DBQ", "Extended_Properties"],
               locals(),
               Underscore2Space=True)
            
            return connector(connectionStr, DBQ, additionalParams={'autocommit':True})
        
    @classmethod
    def MicrosoftAccess(cls, Driver="{Microsoft Access Driver (*.mdb, *.accdb)}", DBQ=None):
            "Open a Microsoft Access database, DBQ is the database filename"
            connectionStr = cls.__BuildConnectionStr__(
               ["Driver", "DBQ"], locals())
            if DBQ is None:
                raise ValueError("Path to Access database (DBQ) must be specified")
            return connector(connectionStr, DBQ)
        
    @classmethod
    def ConnectionString(cls, String=None):
        if String is None:
            raise ValueError("Connection string must be specified")
        else:
            return connector(String, "User specified connection string")        
        
    @classmethod
    def __BuildConnectionStr__(cls, paramlist, valuedict,
                               Underscore2Space=False,
                               additionalParams=dict()):
        """Internal method for constructing a list of connection
        items.  Expects ordered names connection parameters to build
        into a connection string based on a dictionary of values.
        """
        
        connections = []
        for name in paramlist:
            try:
                value = valuedict[name]
                if name in ["Database", "DBQ", "Data_Source"]:
                    value.replace('\\', '\\\\')  # Escape backslash if present
                if Underscore2Space:
                    name = name.replace('_', ' ')
                connections.append("%s=%s;" % (name, value))
            except KeyError:
                pass  # Shouldn't get here
        
        return "".join(connections) 
        
    def __init__(self, connection_str, name="ODBC Connector",
                 additionalParams=dict()):
        """
        build a connection string object
        additionalParams are additional keyword/value parameters
        that will be passed on connection.
        """
        self.connection_str = connection_str
        self.source = name,
        self.params = additionalParams

        
    def get_connector(self):
        "Return the connection string"
        return self.connection_str
    
    def get_source(self):
        return self.source
        
class csv_parser(data_parser):
    
    data_source = "Table"  # Keyword to indicate a data source
            
    def __init__(self, methods, fname, translator=None, speciesabbr=None,
                 debug=False):

        """Creates a data_parser specialized for ODBC connections.
        methods - an XMLMethods instance
        connector - an connector object that specifies how the 
            connection should be made
        translator - Use the specified translator from collection('SourceMaps')
           to parse a data source consisting of tuples.  Source can
           be anything that ODBC can query.
        speciesabbr - How species names are encoded
        """
        
        # Remember who you are...
        self.source = fname
        self.current_stream = "CSV"
        # data_parser.__init__(self, methods, translator, speciesabbr, debug)
        
    def get_source(self):
        """Return an iterator that provides a dictionary for each row
         with field names as keys"""
        self.fileh = open(self.source, 'rt')
        reader = csv.DictReader(self.fileh)        
        return reader
    
    def close(self):
        self.fileh.close()  
        
    def __del__(self):
        self.close()  
        
        
class odbc_parser(data_parser):
    """Parse data from a SQL database.  
       Default assumes MySQL 5.1 but other data sources may be used"""
    
    data_source = "Table"  # Keyword to indicate an ODBC data source
    
    password_re = re.compile("(<?Pre>.*Password=)[^;](<?Post>;.*)", re.I)

    def __init__(self, methods, connector,
                 translator=None, speciesabbr=None,
                 debug=False):
        """Creates a data_parser specialized for ODBC connections.
        methods - an XMLMethods instance
        connector - an connector object that specifies how the 
            connection should be made
        translator - Use the specified translator from collection('SourceMaps')
           to parse a data source consisting of tuples.  Source can
           be anything that ODBC can query.
        speciesabbr - How species names are encoded
        """
        
        # Remember who you are...
        self.source = connector.get_source()
        self.db = None
        try:
            self.db = pyodbc.connect(connector.get_connector(), **connector.params)
        except Exception as e:
            # Remove password prompt if present and print the connection string
            conn = connector.get_connector()
            m = self.password_re.match(conn)
            if m is not None:
                conn = m.group("Pre") + "<PasswordOmitted>" + m.group("Post")
            print("Connection string:  %s" % (conn))            
            
            known_sources = []
            for k, v in list(pyodbc.dataSources().items()):
                known_sources.append("%25s\t%s" % (k, v))
            known_sources = "\n".join(known_sources)
            
            raise

        super().__init__(methods, translator, speciesabbr, debug)

    def __del__(self):
        self.close()

    def close(self):
        if self.db is not None:
            self.db.close()
            self.db = None
                    
    def get_source(self, tablenode, debug=False):
        
        if tablenode.nodeName == "Table":
            query = tablenode.getAttribute("query")
            if query == "":
                raise ValueError('The query attribute is mandatory for Table sources')
        else:
            # User is using the sheet syntax, translate to query
            sheet = tablenode.getAttribute("name")
            if sheet == "":
                raise ValueError('The name attribute is mandatory for Sheet sources')
            query = "select * from [%s$]" % (sheet)
        if query is None:
            # dead code?
            raise ValueError("query attribute is mandatory for <%s>" % (odbc_parser.data_source))
            
        if "insert" in query.lower():
            raise ValueError('insert is not permitted in table queries')
        if "delete" in query.lower():
            raise ValueError('delete is not permitted in table queries')
        
        try:
            table = self.db.execute(query)
        except Exception as e:
            raise ValueError("Unable to execute data query:  %s\nreturned: %s"%(
                query, e))
        
        self.current_stream = query  # Note query in case of errors
        return RowDataFetchMany(query, table)

class xml_parser(data_parser):
    """xml_parser - Class for parsing XML.  Currently
       no parsing is done, it simply reads an XML document
       and returns an iterator that serves the one document
       when get_source is called.
       
       Unlike the other data_parsers, no translation is done"""

    def __init__(self, methods, name=None, sourcedoc=None, debug=False):
        """xml_parser
               methods - XmlMethods object
               name - XML document source filename OR name of XML document
               sourcedoc - XML document as a string (None if filename given)
               debug - True/False
               
               When the XML document is passed as a string via sourcedoc, the
               name is interpreted as the document name.  Otherwise, name is a
               filename and the document name is derived from it.
               """
        
        self.current_stream = "XML"
        self.row_num = 0      
        self.sourcedoc = sourcedoc
        if sourcedoc is None:
            # Use basename of file without extension as source
            self.source = os.path.splitext(os.path.basename(name))[0]
            with open(name, "rb") as xml_h:
                self.sourcedoc = xml_h.read()
        else:
            # name serves as the document name
            self.source = name

        if isinstance(self.sourcedoc, bytes):
            # Let element tree figure out the encoding
            # Costs us a round trip (decode -> encode), but worth it.
            tree = ET.fromstring(self.sourcedoc)
            self.sourcedoc = ET.tostring(tree, encoding="unicode")

        self.debug = debug
        self.errors = {}
        self.warnings = {}
        self.root_data = None  # Incdicates we only produce 1 document
    
    def __iter__(self):
        self.iterated = False
        return self
    
    def __next__(self):
        if self.iterated:
            raise StopIteration
        else:
            self.iterated = True
            return self.sourcedoc

class ItisCodeMap:

    bad_tsn = -999999  # Unused taxonomic serial number

    def __init__(self, methods, speciesabbr = None):
        """
        ItisCodeMap - Abbreviation/Latin to integrated taxonomic serial numbers
        It is also okay to pass in TSNs directly.

        :param methods: query handler
        :param speciesabbr: name of species abbreviation map
        """

        self.methods = methods  # query handler

        self.missing = {}  # names that we could not map to TSNs
        self.to_tsn = {}   # map names to TSNs

        if speciesabbr and speciesabbr.lower() in ('latin', 'tsn'):
            self.speciesabbr = None
        else:
            self.speciesabbr = speciesabbr  # abbreviation map

        if self.speciesabbr is not None:
            xquery = f"""
import schema namespace ty="http://tethys.sdsu.edu/schema/1.0" at "tethys.xsd";
collection("SpeciesAbbreviations")/ty:Abbreviations[ty:Name="{self.speciesabbr}"]
"""
            map = self.methods.query(xquery)
            if not 'tsn' in map:
                raise ValueError(
                    'Unable to retrieve species abbreviations for '
                    f'abbreviation map "{self.speciesabbr}"')

            # Parse the returned XML
            ns = {"ty": dbxmlInterface.namespaces['ty']}  #namespace
            frame = pd.read_xml(map, xpath="/ty:Abbreviations/ty:Map",
                                namespaces=ns)
            try:
                groups = pd.read_xml(
                    map,
                    xpath="/ty:Abbreviations/ty:Map/ty:completename",
                    namespaces=ns)
                frame = pd.concat([frame, groups], axis=1, join="inner")
            except ValueError:
                # No Group attributes in species map
                frame.assign(Group=None)

            # Build dictionary
            # coding key --> (TSN, @Group attribute | None)
            for row in frame.itertuples(index=False):
                group = {"Group":row.Group} if row.Group else {}
                self.to_tsn[row.coding] = (row.tsn, group)

    def __getitem__(self, key):
        """
        Map to an ITIS taxonomic serial number and group
        Most groups will be None, but groups are sometimes used to denote
        distinguishing chracteristics within a taxon, e.g., we know it is type
        of beaked whale, and there is something distinct about it, but
        we do not know species of beaked whale
        :param key:  abbreviation, Latin name, or TSN
        :return: (TSN, group)  TSN is ITIS taxonomic serial number, group
           is a dictionary that is empty or has a group key with a string value
        """

        if isinstance(key, int):
            retvav = (key, None)  # user passed in a TSN
        elif key in self.to_tsn:
            retval = self.to_tsn[key]
        else:
            # See if this is a Latin species name
            try:
                tsn = self.methods._query(
                    'import schema namespace ty="http://tethys.sdsu.edu/schema/1.0" at "tethys.xsd";',
                    f'data(collection("ITIS_ranks")/ty:ranks/ty:rank[ty:completename = "{key}"]/tsn))')
                # Groups can only be specified in abbreviation maps, so we
                # return an emtpy group dictionary
                retval = (int(tsn), {})
            except Exception as e:
                if key not in self.missing:
                    self.missing[key] = True
                # Return bogus TSN so that we can continue processing.
                # User of this class should call get_missing() at end of
                # processing
                retval = (self.bad_tsn, {})

            self.to_tsn[key] = retval  # Store this new name

        return retval

    def get_missing(self):
        if len(self.missing) > 0:
            missing = [f'species codes: {", ".join(self.missing.keys())} not in ']
            if self.speciesabbr:
                missing.append(f'SpeciesAbbreviation map {self.speciesabbr}, ')
            missing.append('Latin species names, or TSNs.  Likely cause is an')
            missing.append("incorrect species coding or abbreviation map")
            retval = " ".join(missing)
        else:
            retval = None
        return retval
