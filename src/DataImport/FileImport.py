'''
Created on Sep 28, 2013

Used when a file of some type is to be imported

@author: mroch
'''

import os
import mimetypes
import re
import tempfile
import gzip

# Tethys modules
from . import translator
from . import data_parser
from . import Connection
from mediators import File
from benchmark import Timer

from .Process import AddDocuments
from dbxmlInterface.error import DocumentFailure
from mediators.File import stream
from mediators.File import StreamToFile, StreamToTempFile, RemoveTempFile
import mediators.ODBC
import REST.util



def getMimeType(ext, mimetype=None):
    """getMimeType - Extract mimetype from file extension if not specified
    Returns mimetype if specified, otherwise guesses from extension that
    must contain the period (.ext)
    """
    csv = '.csv'

    if not mimetype:
        mimetype = mimetypes.guess_type('x'+ext)
    
    if ext == csv:
        mimetype = str('text/csv')
    return mimetype


def Sources(methods, collection, xmlsink, spec, form, attachments=None):
    """
    Sources - Import data from specified sources to collection
    :param methods:  instance of XmlMethods
    :param collection:  instance of collection
    :param xmlsink:  instance of XmlSink or derived class
    :param spec:  POST specification of data sources and other parameters
    :param form:  POST form variables
    :param attachments:  Any attachments for documents
    :return:
    """

    timer = Timer()   # stopwatch

    tethys_module = \
        'import module namespace lib="http://tethys.sdsu.edu/XQueryFns" at "Tethys.xq";\n'
    tethys_namespace = \
        'import schema namespace ty="http://tethys.sdsu.edu/schema/1.0" at "tethys.xsd";\n'
    tethys_modnamespace = tethys_module + tethys_namespace
    containerName = collection.container_name
    

    src_element = spec['sources']
    docname = spec['docname']
    if docname:
        # Remove automatically generated temporary file portions of name
        temp_re = re.compile("(?P<name>.*?)(\.\w+)?-temp-.*")
        m = temp_re.match(docname)
        if m:
            docname = m.group('name')

    overwrite = spec['overwrite']

    error = False

    # changed to a list so that can access it in the function below
    tempdir = [None]
    # Define closure on this function's environment to access/manipulate
    # tempdir.  
    def getTempDir():
        "Return the temporary directory, creating it if it does not exist"
        # now accesses the list rather than the variable
        if not tempdir[0]:
            tempdir[0] = tempfile.mkdtemp()
        return tempdir[0]
    
    # Create ODBC connectors for each source
    sources = dict()
    
    # connectors for CSVs are special, the Microsoft text driver
    # connects to a directory and can read any of the files therein.
    # When text files are used, we create a special text directory
    # and copy files there using the data source name and writing a
    # special file that describes the formatting of each.
    # The same connector is used for multiple text CSVs
    
    csv = None
    xml = False  # switch later if using XML source
    failures = []
    for s in src_element:
        try:
            name = s['name']
        except KeyError as e:
            name = 'source'
                
        s['sql'] = True  # Assume SQL support until we find out otherwise
        if s['type'] == "file":
            fname = s['file']
            if not fname:
                xmlsink.elementText("Source",
                                    "filename is mandatory for type file")
                failures.append('Missing filename specification')
                error = True
                continue
            else:
                try:
                    (filename, mimetype, fileH) = form[fname][0]
                except KeyError:
                    xmlsink.elementText("Source",
                                        f"Data file {fname} not present")
                    error = True
                    failures.append(fname)
                    continue
                except TypeError:
                    xmlsink.elementText("Source",
                                        f"Unable to access Data file {fname}")
                    error = True
                    failures.append(fname)
                    continue

            # mimetype is needed for two reasons:
            # 1 - Determine appropriate handling for files.
            # 2 - Derive connector string if user did not specify.
            #     If user specifies connection string, they should
            #     specify <*filename*> in place of the filename
            #     so that we can replace the string with the appropriate
            #     filename.
            [_, ext] = os.path.splitext(filename)
            mimetype = getMimeType(ext, mimetype)  # extension -> mimetype
            
            s["mimetype"] = mimetype

            # XML sources
            if mimetype in ["application/xml", "text/xml"]:
                xml = True  # no sourcemap/abbreviation processing
                s["stream"] = stream(fileobj=fileH)
                s["sql"] = False    # Cannot use SQL on XML
            elif mimetype in ['application/octet-stream', 'application/x-gzip'] \
                    and ext in ('.gzip', '.gz'):
                zfile = gzip.GzipFile(mode='rb', fileobj=fileH)
                xml = True  # no sourcemap/abbreviation processing
                s["stream"] = zfile
                s["sql"] = False  # Cannot use SQL on XML
            elif mimetype in ["application/vnd.ms-excel",
                              # Long mimetype split into two lines (no comma!)
                              "application/vnd.openxmlformats-officedocument"
                              ".spreadsheetml.sheet"]:
                # Copy to tempfile
                tmp = getTempDir()
                s["tempfile"] = StreamToFile(fileH,
                                        os.path.join(tmp, filename))
                # Set up ODBC
                c = mediators.ODBC.Connector.MicrosoftExcel(DBQ=s["tempfile"])
                s['connectionstring'] = c.get_connector()
                s['connection'] = mediators.ODBC.source(c)
                
            elif mimetype in ["text/csv", "text/comma-separated-value"]:
                # Have we processed a comma separated value source
                # already?
                tmp = getTempDir()
                if not csv:
                    # No, set it up.  All csv files will use the
                    # same source
                    csv = dict()
                    # Create path to directory 
                    csv["dir"] = os.path.join(tmp, "text")
                    os.mkdir(csv["dir"])
                    # Create connector with trailing separator: a/b/c/
                    # Needed as filenames will be appended

                    # For entries describing text files
                    csv["schema"] = open(
                        os.path.join(csv["dir"], "Schema.ini"), "w")

                # Write out file as source name with txt extension
                # All SQL queries will need to reference this new name
                # with the txt extension
                
                newname = "%s.csv"%(name)
                s["tempfile"] = StreamToFile(fileH, 
                                        os.path.join(csv["dir"], newname))

                # Write schema entry
                # Currently on support comma and tab delimited,
                # assume .tab for tab delimited, anything else is
                # comma delimited
                # It would not be very difficult to write something
                # to guess the delimiter, the importer can recognize
                # anything except double quotes.  Example:
                # Format=Delimited(;)
                # would use semi-colons to delimit fields
                csv["schema"].write("[%s]\nFormat=%s\n"%(
                        newname, 
                        "TabDelimited" if ext == "tab" else 
                                          "CSVDelimited"))
                # todo - Test, this might be problematic.  We haven't
                # created a source for now, but if we do when we
                # process the first ODBC, it might try to read
                # schema.ini, but it won't be written yet.  May
                # need to process all sources, then open csv if there
                # were any and store the sources into the sourcelist.
                #fileH.close()
                s["connection"] = mediators.ODBC.Connector.TextDriver(
                        DBQ=os.path.join(csv["dir"],""))

                # TODO:  At end of loop.  close off schema.ini, open
                # the odbc connection and set sources.
                
            elif mimetype in ["application/msaccess", 
                              "application/x-msaccess",
                              "application/mdb"
                              "application/x-mdb"]:
                
                tmp = getTempDir()
                # Write file
                s["tempfile"] = StreamToFile(fileH, 
                                        os.path.join(tmp, filename))
                                        #os.path.join(csv["dir"], filename))
                if s['odbc']:
                    c = mediators.ODBC.GetConnector(s['odbc'], s["tempfile"], s['odbc']['driverName'])
                    pass # build connector based on dict
                elif s['connectionstring']:
                    c = mediators.ODBC.Connector.ConnectionString(s['connectionstring'])
                else:
                    c = mediators.ODBC.Connector.MicrosoftAccess(DBQ=s["tempfile"])
                    s['connectionstring'] = c.get_connector()
                s['connection'] = mediators.ODBC.source(c)

        else:
            # network resource
            if 'odbc' in s:
                c = mediators.ODBC.Connector.GetConnector(s['odbc'], None)
            elif 'connectionstring' in s:
                c = mediators.ODBC.Connector.ConnectionString(
                    s['connectionstring'])
            s['connection'] = mediators.ODBC.source(c)

        sources[name] = s
        
    if error:
        return [], failures
   
    if csv:
        # close off schema.ini for csv
        csv['schema'].close()
        # create sources
        for k, v in list(sources.items()):
            # assume ODBC.connectors were reserved for csv files
            # create sources for them
            if isinstance(v['connection'], mediators.ODBC.Connector):
                sources[k]['connection'] = mediators.ODBC.source(v['connection'])
        
    if not xml:
        # Determine which source map (data translation specification)
        # and which species map (name to taxonomic serial number specification)
        # The source map is always mandatory.  If it is not present we will
        # look for a table in the data source called MetaData and attempt to
        # retrieve these from the Parser (source map) and SpeciesAbbreviation
        # (species map) fields.

        source_map, species_map = determine_maps(spec, containerName, sources)
        trans = translator.Translator(methods, sources, translator=source_map,
                                      default_docid=docname,
                                      speciesabbr=species_map)
        trans.prepare()  # prepare for import
    else:
        # XML document
        trans = translator.XmlTranslator(methods, sources, docname)

    print(f"{__name__}.{__file__} - Parsed input in: {timer.elapsed()},"
          "adding...")

    [successes, failures] = \
        AddDocuments(methods, collection, docname, trans, xmlsink,
                     auxInfo=attachments, replace=overwrite)

    print(f"{__name__}.{__file__} - "
          f"Documents parsed/validated/added: {timer.elapsed()}")

    return [successes, failures]


def File(methods, collection, xmlsink, importmap, abbreviations, docId, filename,
         mimetype=None, overwrite=False, auxInfo=None):
    """File - Import data from a local file"""
    
    # Determine media type
    if mimetype is None:
        [basename, ext] = os.path.splitext(filename)
        mimetype = getMimeType(ext, mimetype)
        
    parser = None
    if mimetype in ["application/xml", "text/xml"]:
        fileH = open(filename, 'rt')
        parser = parseXML(methods, xmlsink, docId, fileH)
    elif mimetype in ["application/vnd.ms-excel",
          "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"]:
        try:
            parser = data_parser.workbook_parser(methods, filename, 
                importmap, abbreviations)
        except ValueError as v:
            DocumentFailure(xmlsink, basename, "Error", v.message)
            raise ValueError(xmlsink.finalize())
    else:
        DocumentFailure(xmlsink, basename, "Error", "Unknown data type for %s"%(filename))
        raise ValueError(xmlsink.finalize())
    
    [successes, failures] = AddDocuments(methods, collection, basename, parser, xmlsink, auxInfo=auxInfo, replace=overwrite)
    return [successes, failures]
        
    
def Stream(methods, collection, xmlsink, importmap, abbreviations,
           fileH, filename, mimetype=None, overwrite=False, auxInfo=None):
    """Stream - import data from an open stream (file handle)"""
    
    [basename, ext] = os.path.splitext(filename)
    if mimetype is not None:
        mimetype = getMimeType(ext, mimetype)

    tmpfname = None
    if mimetype in ["application/xml", "text/xml"]:
        parser = parseXML(methods, xmlsink, basename, fileH)
    
    elif mimetype in ["application/vnd.ms-excel",
          "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"]:
        # Need to write out workbook as Excel processor will need to open it
        tmpfname = StreamToTempFile(fileH, ext)    
        try:
            parser = data_parser.workbook_parser(methods, tmpfname, 
                importmap, abbreviations)
        except ValueError as v:
            DocumentFailure(xmlsink, basename, "Error", v.message)
            # Failed, remove temporary file
            os.remove(tmpfname)
            raise ValueError(xmlsink.finalize())
    else:
        DocumentFailure(xmlsink, basename, "Error", "Unknown data type for %s"%(filename))
        raise ValueError(xmlsink.finalize())
    
    [successes, failures] = AddDocuments(methods, collection, basename, parser, xmlsink, auxInfo=auxInfo, replace=overwrite)    
    
    RemoveTempFile(tmpfname, basename, xmlsink) 


    return [successes, failures]

    
def ODBC(methods, collection, xmlsink, importmap, abbreviations, 
        fileH, filename, mimetype=None, overwrite=False, auxInfo=None):
    """
    Import data from open database connectivity source
    As a side effect, the xmlsink object may be modified.

    :param methods:  XmlMethods instance (database interface)
    :param collection:  Documents stored in this collection
    :param xmlsink:  XMLSink instance for adding messages
    :param importmap: SourceMap instance
    :param abbreviations:  SpeciesAbbreviations
    :param fileH:  Data to be processed, e.g. spreadsheet data
    :param filename:  Original filename for fileH, used to determine
       type of data
    :param mimetype:  Application data type, will try to infer if absent
    :param overwrite: Should existing documents be overwritten?
    :param auxInfo:  object that can verify higher level information
        not contained in the XML document (e.g. additional media files,
        etc.)  Used by AddDocuments which is called inside this function
    :return:  List containing two entries.  First is the list of documents
        that were successfully added, the second is the list of failures
    """

    "ODBC - Import data from an ODBC file stream"
    
    [basename, ext] = os.path.splitext(filename)
    if mimetype is not None:
        mimetype = getMimeType(ext, mimetype)
    tmpfname = None     # temporary file if needed
    
    if mimetype == "application/xml":
        parser = parseXML(methods, xmlsink, basename, fileH)
    else:
        tmpfname = StreamToTempFile(fileH, ext)  # parser expects on disk
        # Find out how to connect to the data source and instantiate a parser
        # for it.
        connector = Connection.connector.ConnectionFromMimeType(tmpfname, mimetype)
        if connector is None:
            DocumentFailure(xmlsink, basename, "Error", "Unknown data type for %s"%(filename))
            raise ValueError(xmlsink.finalize())
        parser = data_parser.odbc_parser(methods, connector, importmap, abbreviations)
        
    [successes, failures] = AddDocuments(methods, collection, basename, parser,
                                         xmlsink, replace=overwrite,
                                         auxInfo=auxInfo)
    RemoveTempFile(tmpfname, basename, xmlsink)    
    
    return [successes, failures]
    

def parseXML(methods, xmlsink, basename, fileH):
    
    try:
        parser = translator.XmlTranslator(methods, basename, stream=fileH)
    except ValueError as v:
        DocumentFailure(xmlsink, basename, "Error", str(v))
        raise ValueError(xmlsink.finalize())
    return parser


def determine_maps(spec, collection, sources):
    """
    Determine which source data translation map will be used and if
    the user is submitting species data using something other than TSNs

    :param spec: submitted data specification dictionary
    :param collection: collection to which we will be adding the data
       (not all collections use species maps)
    :param sources:  data sources, needed if we have to query the data
    :return: source_map and species_map
    """
    # Attempt to find Parser and SpeciesAbbreviations fields in the source
    # specification, then change "" to None to standardize the missing value
    # case
    maps = dict()
    maps['source_map'] = spec.get('source_map', None)
    maps['species_map'] = spec.get('species_map', None)
    retrieve = []
    for m, v in maps.items():
        if v is None or len(v) == 0:
            retrieve.append(m)
            if v is not None:
                maps[m] = None   # standardize error case

    # If the submission did not include a source and species map, see if we
    # can retrieve it from the data.  If there is a table/sheet called MetaData,
    # we will look for the fields Parser and SpeciesAbbreviations.
    if len(retrieve) > 0:
        # map so we know what variables to set
        var2field = {'source_map': 'Parser',
                     'species_map': 'SpeciesAbbreviation'}
        # We only look in the first source
        # this may not work well when people are submitting CSV which may
        # consist of multiple sources.
        source_list = list(sources.items())
        source_info = source_list[0][1]

        # Retrieve list of tables and see if there is a metadata table
        # As Excel appends a $ sign, we accept variants of the word metadata
        connection = source_info['connection']
        tables = connection.get_tables()
        #ability to bind multiple fields to same column in schema (see parameter *)
        metadata_indices = [idx for idx, tbl in enumerate(tables)
                            if "metadata" in tbl.lower()]

        if len(metadata_indices) > 0:
            for tbl_idx in metadata_indices:
                query = f"select * from [{tables[tbl_idx]}]"
                rowdata = connection.sql(query)
                data = next(iter(rowdata)) # Only check the first row
                remaining = []
                for var in retrieve:
                    field = var2field[var]
                    if field in data:
                       maps[var] = data[field]
                    else:
                        remaining.append(var)
                metadata_indices = remaining
                if len(metadata_indices) == 0:
                    break  # found everything, stop looking

    return maps['source_map'], maps['species_map']

    
    
    
    
    
    
    
    
      
    