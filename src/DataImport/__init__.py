# data import

""" 

Notes on overall structure of data import:

data_parser is the main class for doing data import.
Once constructed on a source, it is designed to be iterated over.
Each iteration will produce an XML document and may result in one or more
queries to the data source for each document produced.

FileImport - Used to start an import from various sources:
    files, streams, open database connectivity (ODBC) sources, XML
    
Process - Used for adding documents

TODO:  There is some duplicate functionality that needs to be removed:
    Connection/data_parser . odbc_connector
    NetworkImport/FileImport ODBC
    
"""

# Dictionary of child elements of SourceMap/Directives
# that have special significance.
# Keys represent a category and values thee nodes that can match them
directives = {
    "attribute": "Attribute",
    "data": ["Sheet", "Table"],
    "entry": "Entry",
    "default": "Default",
    "dest": "Dest",
    "kind": "Kind",
    "text": "#text",
    "comment": "#comment",
    "source": "Source",
    "condition": "Condition",
    "predicate": "Predicate",
    "operand": "Operand",
    "operation": "Operation",
}
