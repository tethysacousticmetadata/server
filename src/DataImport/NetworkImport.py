
from . import Connection
from . import data_parser
from .Process import AddDocuments


def ODBC(methods, collection, docname, xmlsink, importmap, abbreviations,overwrite, 
        connect_str):
    
    connector = Connection.connector.ConnectionString(connect_str)
    parser = data_parser.odbc_parser(methods, connector, importmap, abbreviations)
        
    committed = AddDocuments(methods, collection, 
                             docname, parser, xmlsink,
                             replace=overwrite, auxInfo=None)
    
    return xmlsink.finalize()