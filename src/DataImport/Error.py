class MissingField(Exception):
    """MissingField - Exception thrown when a field is not present"""
    def __init__(self, message):
        self.message = message