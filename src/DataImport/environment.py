
# environment class
"""Example
# Create an environment for binding names to values
e = environment.environment()
e["alpha"] = "hi there"
f = e.push()  # Create a new environment capable of overriding names
f["alpha"] = "bye"

e["alpha"]
'hi there'

f["alpha"]
'bye'

f.vars()
['alpha']

print f.stack()
level 1:
alpha=hi there
level 0:
alpha=bye

"""


class Environment:
    """
    Environment frames
    Provides dynamic scoping of user variables
    """

    def __init__(self, parent=None, name=None):
        """Create an environment for bindings between names and variables
        parent - Parent environment, will look here if name not found
            in this environment.
        name - Optional name of environment."""
        self.frame = dict()
        self.parent = parent
        self.name = name
        
    def __getitem__(self, item):
        if item in self.frame:
            v = self.frame[item]  # Grab from the current frame
        else:
            # In parent environment?
            if self.parent:
                v = self.parent[item]  # recurse into next stack frame
            else:
                # no more stack frames
                ke = KeyError(f"Import variable '{item}' does not exist")
                ke.missing_var = item
                raise ke

        # v is guaranteed to be set if we reach here
        return v    
    
    def __setitem__(self, item, value):        
        self.frame[item] = value
        
    def depth(self):
        """
        Get the depth of the current environment.  Depth of zero indicates
        that the environment is not nested.
        :return: nesting level of current environment
        """

        d = 0
        current = self.get_parent()
        while current is not None:
            d = d + 1
            current = current.get_parent()
        return d
    
    def push(self):
        return Environment(parent=self)
    
        
    def get_parent(self):
        "Return parent environment. Renamed to get_parent due to TypeErrors"
        return self.parent
    
    def get_root(self):
        """
        Retrieve the environment's root
        :return:  oldest ancestor
        """
        depth = self.depth()
        if depth == 0:  # no parents
            return self
        
        parent = self.get_parent()
        if parent.depth() == 0:
            return parent
        else:
            while depth > 0:
                ancestor = parent.get_parent()
                depth = ancestor.depth()
                parent = ancestor
            return ancestor
    
    def vars(self):
        """
        Get variables in frame
        :return: list of visible variable names"
        """
        variables = list(self.frame.keys())
        if self.parent:
            variables.extend(self.parent.vars()) # merge with parents
            # Remove duplicates by building a set & convert back to list
            variables = list(set(variables))
            
        return variables
    
    def stack(self, level=0, names_only=False):
        """
        Return environment stack
        :param level: Stack depth (used internally)
        :param names_only:  If True, return stack frame names or numbers only
        :return:  String with stack frames and variable bindings for each frame
        """

        if self.name is not None:
            name = self.name
        else:
            name = f"level {level}"
        
        if self.parent is not None:
            ancestors = self.parent.stack(level-1, names_only)
        else:
            ancestors = ""

        content = f"{ancestors}{name}" + "\n"
        if not names_only:
            content += \
                ", ".join([f"{k}={v}" for k, v in self.frame.items()]) + "\n"

        return content
    
    def __str__(self):
        return self.stack(self.depth())

    def frames(self):
        """
        :return: String of stack frames without values
        """
        return self.stack(self.depth(), names_only=True)


class IterEnvironment(Environment):
    """
    IterEnvironment
    An environment that binds names to values like Environment, except
    that it takes a binding iterator that produces name/values
    bindings in the form of a dictionary.

    Each time we call next on the IterEnvironment, the name/value
    bindings are updated based on the next value of the iterator of
    the IterEnvironment, we update the bindings based on the next
    value of the binding iterator.
    """

    def __init__(self, binding_iterable, parent=None, name=None,
                 static_values=None):
        """
        IterEnvironment
        :param binding_iterable:  Object that can be iterated over
          to produce dictionaries that bind names and values.
        :param parent:  parent environment
        :param name: name of this environment
        :param static_values: dictionary that provide a set of values
           that remain static across all iterations.  They are overridden
           by any static assignments.
        """

        self.iterable = iter(binding_iterable)
        # start at negative one, so that the first will start at 0
        self.idx = -1
        self.static_values = static_values

        super().__init__(parent, name)

    def __iter__(self):
        return self

    def push(self, binding_iteratable, name=None):
        """
        Create a new record on the stack.
        :param binding_iteratable: Our table or other iterable which will
          produce dictionaries of variable names and values.
        :param name:  Name of new environment
        :return:  Returns a new IterEnvironment whose parent is the current
          one.
        """
        return IterEnvironment(binding_iteratable, parent=self, name=name)

    def __next__(self):
        """
        next - Get next environment
        Updates the bindings to the next row of the SQL query.
        Raises StopIteration when there are no more rows
        :return: self
        """
        nextdic = next(self.iterable)
        self.idx = self.idx + 1
        if not isinstance(nextdic, dict):
            raise ValueError(
                "binding iterator does not return a dictionary")
        self.frame = nextdic
        return self

    def __getitem__(self, item):
        if item in self.frame:
            v = self.frame[item]  # Grab from the current frame
        elif self.static_values and item in self.static_values:
            # Values that do not change between iterations
            v = self.static_values[item]
        else:
            # In parent environment?
            if self.parent:
                v = self.parent[item]  # recurse into next stack frame
            else:
                # no more stack frames
                ke = KeyError(f"Import variable '{item}' does not exist")
                ke.missing_var = item
                raise ke

        # v is guaranteed to be set if we reach here
        return v
    def get_index(self):
        "get_index - Return index of iterator"
        return self.idx


            