
# Python modules
import datetime
import numbers

# Add-on modules
import pendulum
import pandas as pd

def tz_aware(t):
    """
    Is time t timezone aware?
    :param t: datetime
    :return: True if timzene aware, else False
    """

    # Timezone aware if tzinfo field exists and utcoffset() does not return None
    return t.tzinfo is not None and t.tzinfo.utcoffset(t) is not None

def timestamp_parse(values, contexts):
    """

    :param values:  List of values representing time, strings, datetime, etc.
    :param contexts: List of contexts for each value.  Each context is of the
       form [precontext, value, postcontext] where precontext and postcontext
       are strings that can be used to help form a valid timestamp.  For
       example, if values = ["2022-01-01", "13:24:56"] context
       might be
       ['', '2022-01-1', ''], ['T', '13:24:56', 'Z']]
       which would enable us to combine the two values into:
       2022-01-01T13:24:56Z
       Note that contexts are ignored when the list of values contains
       time and date objects.

    :return: ISO8601 UTC string or None if we cannot parse the time
    """

    # Convert dates.  We expect values to contain:
    # 1.  [datetime.datetime]
    # 2   [datetime.date, datetime.time]
    # 3.  A set of strings.  These are assembled along with
    #     any additional information provided in the <Source>
    #     field (e.g. a time separator T) and parsed with
    #     the pendulum package
    if len(values) > 0:
        if isinstance(values[0], datetime.datetime):
            # Case 1, 2, or 3 above
            if isinstance(values[-1], str):
                # Last value is a UTC offset, e.g. -02:00
                t = pd.Timestamp(values[0], tz=values[-1])
            else:
                if tz_aware(values[0]):
                    t = pd.Timestamp(values[0]).tz_convert(tz='UTC')
                else:
                    t = pd.Timestamp(values[0], tz='UTC')
            # See if we have HH:MM
            if len(values) > 1 and \
                    isinstance(values[1], (datetime.datetime, datetime.date, datetime.time)):
                # Assume HH:MM:SS
                h = pd.Timestamp(values[1])
                h = h - h.floor('d')  # Convert to interval
                t = t + h
        elif isinstance(values[0], datetime.date):
            if len(values) > 1 and isinstance(values[1], datetime.time):
                t = pd.Timestamp.combine(values[0], values[1]).tz_localize(tz='UTC')
            else:
                t = pd.Timestamp(values[0]).tz_localize(tz='UTC')
        else:
            # Assume strings
            time_str = []
            for context, value in zip(contexts, values):
                time_str.append(context[0])
                numeric = isinstance(value, numbers.Number)
                if numeric is True:
                    # Treat as Excel datetime
                    value = pd.to_datetime(value, unit='d', origin='1899-12-30')
                time_str.append(str(value))
                time_str.append(context[2])
            time_str = "".join(time_str)
            # Parse and put in UTC
            if len(time_str) > 0:
                d = pendulum.parse(time_str, strict=False)
                t = pd.Timestamp(d).tz_convert('UTC')
            else:
                t = None  # nothing to parse
    else:
        t = None

    if t:
        # Convert to ISO8601, use Z instead of +00:00
        t = t.isoformat().replace("+00:00", "Z")
    return t
