# our modules
from exception_handling import get_traceback_str
import dbxmlInterface.error


def AddDocuments(methods, collection, basename, parser, 
    sink, replace=False, txn=None, auxInfo=None):
    """
    Add one or more documents to a collection
    :param methods:  XmlMethods instance
    :param collection:  container object representing document collection
    :param basename: string representing document(s) to be added
    :param parser: data parser object responsible for transforming user
       data into a Tethys compliant XML document
    :param sink: XML generator for reporting messages related to the document(s)
    :param replace:  if True, existing documents will be overwritten
    :param txn:  Add using transaction (transactions will be created
         automatically if needed)
    :param auxInfo: object that can verify higher level information
               not contained in the XML document (e.g. additional media files,
               etc.)  auxInfo will be called on the validator object which
               is created for this container type.  If the auxInfo callback
               detects any errors it should log an error.
    :return:  Two lists of document names:  successful, failures
              indicating the result of each document that we attempted to
              add. Additional details about these will be logged to the
              sink parameter.
    """

    committed = False
         
    try:
        # Add XML documents.  
        # failures contains information on documents that could not be
        # added, committed is a boolean indicating whether or not we added
        # anything to the database
        successes, failures = methods._iter_docs(
            collection, basename, parser, sink, replace=replace,
            txn=txn, auxInfo=auxInfo)
    except ValueError as v:
        raise v  # bubble up exception
    except Exception as e:
        err_str = list()
        err_str.append(get_traceback_str(e))
        if hasattr(e, 'what'):
            err_str.append(e.what)
        if hasattr(e, 'message'):
            err_str.append(e.message)
        err_str.append(str(e))
        dbxmlInterface.error.DocumentFailure(
            sink, basename, "Error", "\n".join(err_str))
        v = ValueError(sink.finalize())
        raise v
    finally:
        parser.close()  # Release parser resources
        
    return successes, failures
