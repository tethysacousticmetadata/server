from collections import OrderedDict, namedtuple

class PathDict(OrderedDict):
    """
    PathDict - An ordered dictionary that supports accessing of nested
    dictionaries via a path separator.

    If p contains 3 levels of PathDict objects, one can access the third level
    as follows:
       p['key0/key1/key2']

    By default, paths are separated by /, but the separator character may
    be changed by the set_separator method
    """

    def __init__(self, other=(), /, **kwargs):
        """
        Create a new PathDict.  Arguments are identical to dict
        :param other:
        :param kwargs:
        """
        self.separator = "/"
        self.parent = None
        self.parent_name = ""

        super().__init__(other, **kwargs)

    def set_separator(self, separator):
        """
        Set the character used to separate path elements in the nested
        dictionary.
        :param separator: separation character
        :return: None
        """
        if not isinstance(separator, str) or len(separator) != 1:
            raise ValueError("separator must be a string of length 1")

        self.separator = separator

    def get_separator(self):
        """
        :return: current path separation character
        """
        return self.separator

    def get(self, path, default=None):
        """
        Allow access via paths.  Parameters object p,
        p.get('data/labels/dirs')

        :param path: Separated path to navigate nested dictionaries
        :param default:  Returns this value if path does not exist
        :return:  Value if entry exists, otherwise default
        """

        exists, value = self.__access__(path)
        if not exists:
            value = default

        return value

    def get_path(self, string=True):
        """
        Return parent path to this node.  Useful when one has access to
        a sub PathDict and the parent path needs to be recovered.
        :string:  Boolean controlling how the path is returned.
           True (default) - path as a string
           False - path as a list
        :return:  Return path from root PathDict to current instance
        """

        def get_path_aux(obj):
            if obj.parent_name == "":
                l = list()  # base case
            else:
                l = get_path_aux(obj.parent)
                l.append(obj.parent_name)

            return l

        path = get_path_aux(self)
        if string:
            path = self.separator.join(path)

        return path

    def __getitem__(self, path):
        """
        Provide access with [ ] operator.
        Like get, but does not permit a default return value
        :param path: / (or other separator) separated path
        :return:  Value at path or KeyError

        Caveat:  There is no way to distinguish between a None value
        and the path not existing.
        p['experiment/type'] if p is a Parameters object
        """

        exists, value = self.__access__(path)
        if not exists:
            raise KeyError(f"Path {path} does not exist")
        return value

    def __setitem__(self, path, value):
        """
        Assign to Parameters, e.g.
        p['foo/bar'] = True
        If foo or bar do not exist, they will be added.
        if the path exists, it will be replaced
        :param path:  Path to item
        :param value:  New/replacement value
        :return: None
        """

        path_list = path.split(self.separator)
        current = self
        for key in path_list[:-1]:
            if key in current:
                current = current[key]
            else:
                # does not exist, create a new one
                current[key] = PathDict()
                # Note parent PathDict and the name that was used
                # to get to the new child
                current[key].parent = current
                current[key].parent_name = key
                current = current[key]

        # We have identified the correct OrderedDict, set value
        super(PathDict, current).__setitem__(path_list[-1], value)


    def item_paths(self):
        """
        Return breadth first ordered list of paths to leaves
        (shallowest paths first).

        :return:  List of paths to leaves present in the dictionary.
        """

        paths = []  # List of paths to leaves
        queue = list(super().items())  # Initial queue

        while len(queue) > 0:
            path, value = queue.pop()
            if isinstance(value, PathDict):
                # subdict, add keys and dictionaries to queue
                for k, v in super(value, PathDict).items():
                    queue.append(self.separator.join((path, k)))
            else:
                paths.append(path)  # leaf, note path

        return paths

    def __contains__(self, path):
        """
        Implementation of in operator:
           if path in parameter_object

        :param path:  Forward slash separated path to navigate HJSON structure
        :return: True if path is present in the structure object, else False
        """

        if self.separator in path:
            exists, value = self.__access__(path)  # recursively check
        else:
            exists = super().__contains__(path)  # key in this dict
        return exists

    def keys(self):
        return super().keys()

    def __access__(self, path):
        """
        Function for doing parsing.  Given / separated path, determines
        if the path exists and if so, its value

        :param path:  / separated path
        :return:  True|False, value
        """

        path_list = path.split(self.separator)  # path to list
        next = self

        found = True  # Assume true unless we learn otherwise
        for key in path_list[:-1]:
            if super(PathDict, next).__contains__(key):
                next = super(PathDict, next).__getitem__(key)
                if not isinstance(next, dict):
                    found = False
                    break
            else:
                found = False
                break

        value = None
        if found:
            # Made our way down to a base dictionary
            found = super(PathDict, next).__contains__(path_list[-1])
            if found:
                value = super(PathDict, next).__getitem__(path_list[-1])

        return found, value


class PathValueDict(PathDict):
    """
    PathValueDict
    Ordered dictionary for parsing paths.  Similar to PathDict except
    that interior nodes contain a value in addition to a dictionary
    that allows subsequent levels.

    The __get_item__ and get methods return values at a given level.
    """

    node = namedtuple("NodeType", ("value", "subdict"))

    def __init__(self, other=(), /, **kwargs):
        super().__init__(other, **kwargs)
        self.default_interior_value_fn = lambda: None

    def set_default_interior_value_fn(self, fn):
        """
        When adding paths, it is possible to have interior nodes that have
        an undefined value.  By default, these are set to None, but in some
        cases it is desirable to have these default to a different value.

        This function should only be called on empty dictionaries, it is
        not possible to change the default function once the dictionary is
        populated

        :param fn:  function handle that will construct the default item
        :return:  None

        Examples:

        # Create a list for all interior nodes
        d = DefaultValueDict()
        d.set_default_interior_value_fn(list)
        d['foo/bar'] = 7
        The value of d['foo'] will be []

        # Assign a counter variable
        def counter_factory():
           x = 0
           def counter():
               nonlocal x
               x = x + 1
               return x
           return counter
        d = DefaultValueDict()
        d.set_default_interior_value_fn(counter_factory())
        d['foo/bar'] = 7  # value of d['foo'] is 0
        d['la/petite/sirene'] = 'hi'  # d['la']=1 d['la/petite']=2
        """

        if len(self.keys()) > 0:
            raise ValueError("Cannot set default function after values are added")

        self.default_interior_value_fn = fn

    def __getitem__(self, path):
        """
        Provide access with [ ] operator.
        Like get, but does not permit a default return value
        :param path: / (or other separator) separated path
        :return:  Value at path or KeyError

        Caveat:  There is no way to distinguish between a None value
        and the path not existing.
        p['experiment/type'] if p is a Parameters object
        """

        exists, node = self.__access__(path)
        if not exists:
            raise KeyError(f"Path {path} does not exist")
        return node.value

    def get(self, path, default=None, node=False):
        """
        Allow access via paths.  Parameters object p,
        p.get('data/labels/dirs')

        :param path: Separated path to navigate nested dictionaries
        :param default:  Returns this value if path does not exist
        :param node: If True, return the NodeType named tuple associated
           with the path as opposed to NodeType.value.  Has fields:
              value - value associated with entry
              subdict - None or PathValueDict containing next path elements
           Note that syntax such as v, s = p.get(path, node=True) can be
           used to retrieve the named tuple fields directly
        :return:  Value or node if entry exists, otherwise default
        """

        exists, a_node = self.__access__(path)
        if not exists:
            value = default
        else:
            value = a_node if node else a_node.value

        return value

    def __setitem__(self, path, value):
        """
        Assign to Parameters, e.g.
        p['foo/bar'] = True
        If foo or bar do not exist, they will be added.
        if the path exists, it will be replaced
        :param path:  Path to item
        :param value:  New/replacement value
        :return: None
        """

        path_list = path.split(self.separator)
        current = self

        # Note that we use the parent of the PathDict for gettting/setting
        # so that we can use OrderedDict's methods to get/put instances
        # of NodeType

        for key in path_list[:-1]:
            if key in current:
                entry = super(PathDict, current).__getitem__(key)
            else:
                # key unknown, create new entry w/ default value
                # Note that we use the class constructor to ensure that
                # this continues to work if executed from a subclass.
                newdict = self.__class__()
                newdict.parent = current
                newdict.parent_name = key
                # Copy the handle to the current default value function
                newdict.default_interior_value_fn = \
                    current.default_interior_value_fn
                default_value = current.default_interior_value_fn()
                entry = self.node(default_value, newdict)
                super(PathDict, current).__setitem__(key, entry)
            current = entry.subdict  # Get PathValueDict containing child nodes

        # We have identified the correct OrderedDict, set value and carry
        # existing dictionary which may have child values
        key = path_list[-1]
        if key in current:
            # Key exists, update the value and use the existing dict
            existing_node = super(PathDict, current).__getitem__(key)
            entry = self.node(value, existing_node.subdict)
        else:
            # Key is brand new, create value with new dict
            new_dict = self.__class__()
            new_dict.default_interior_value_fn = \
                current.default_interior_value_fn
            new_dict.parent = current
            new_dict.parent_name = key
            entry = self.node(value, new_dict)
        super(PathDict, current).__setitem__(key, entry)

    def __access__(self, path):
        """
        Function for doing parsing.  Given / separated path, determines
        if the path exists and if so, its value

        :param path:  / separated path
        :return:  True|False (key presence), node
        """

        path_list = path.split(self.separator)  # path to list

        parent = self
        current = self

        # Make sure that keys in path leading to the last one exist
        parent_path = True
        for idx, key in enumerate(path_list[:-1]):
            if super(PathDict, current).__contains__(key):
                # Access the next level of the dictionary.
                node = super(PathDict, current).__getitem__(key)
                current = node.subdict
                if not isinstance(current, dict):
                    parent_path = False
                    break
            else:
                parent_path = False
                break

        if parent_path:
            # Handle final key
            key = path_list[-1]
            found = super(PathDict, current).__contains__(key)
            if found:
                entry = super(PathDict, current).__getitem__(key)
            else:
                entry = None
        else:
            found = False
            entry = None

        return found, entry

    def item_pairs(self, filter=lambda x: x is not None, parent=""):
        """
        Return list of tuples of (path, value) listed in a breadth first order
        (shallowest paths first).

        :param filter:  Function that is applied to the value of each
          path.  The list of paths will only contain dictionary paths whose
          value cause filter to be True
        :param parent:  Not settable by caller, used in recursion

        :return:  List of tuples. For each tuple
           tuple[0] - Path to a for loop control point
           tuple[1] - Entry associated with this control point
        """

        # We could probably recode this as an items() iterator

        paths = []

        if len(parent) > 0:
            parent = parent + self.separator  # Append separator

        # Handle keys, then children
        children = []
        for key in self.keys():
            node = super(PathDict, self).__getitem__(key)
            # save key & associated PathValueDict
            children.append((key, node.subdict))
            # Determine if we should retain this path
            if filter(node.value):
                paths.append((parent + key, node.value))

        # Recurse over children
        for child in children:
            new_paths = child[1].item_pairs(filter, parent + child[0])
            paths.extend(new_paths)

        return paths

    def item_paths(self, filter=lambda x: x is not None):
        """
        Return list of paths in a breadth first manner.  This is the
        same functionality as item_pairs except that only paths are returned

        :param filter:  Function that is applied to the value of each
          path.  The list of paths will only contain dictionary paths whose
          value cause filter to be True
        :return:  List of paths
        """

        pairs = self.item_pairs(filter)
        paths = [pair[0] for pair in pairs]
        return paths


if __name__ == "__main__":

    # Demonstration with enumerated default values for inner nodes

    def counter_factory():
        """
        Build a fn that returns one more each time it is called
        :return:  counter function with current value in closure
        """
        x = 0  # closure value
        def counter():
            nonlocal x  # Access closure instead of new local
            x = x + 1
            return x

        return counter


    pvd = PathValueDict()
    pvd.set_default_interior_value_fn(counter_factory())
    pvd['a/b/c'] = 5
    # Access the c sub PathValueDict
    c_val, c_dic = pvd.get('a/b/c', node=True)
    print("c's parent path: ")
    print(c_dic.get_path())

    print("Values along paths to c")
    for k in ('a', 'a/b', 'a/b/c'):
        print(f'pvd[{k}] = {pvd[k]}')

    pvd['a'] = 6  # override default key
    pvd['a/b/f'] = 8
    for k in ('a', 'a/b', 'a/b/c', 'a/b/f'):
        print(f'pvd[{k}] = {pvd[k]}')

    print('done')

