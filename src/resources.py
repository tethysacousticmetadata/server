# resources.py
# Filepaths and other resources

import os
import glob
import sys

basedir = ""
names = {}

def setbasedir(basedir):
    # Set module level variables:
    #    basedir - base directory
    #    names - dictionary of filenames & other resources
    setattr(sys.modules[__name__], 'basedir', basedir)
    setattr(sys.modules[__name__], 'names', {
         "database": os.path.join(basedir, "db"),
         "diskcache": os.path.join(basedir, "diskcache"),
         # Location for libraries: modules, schema, etc
         "library": os.path.join(basedir, "lib"),
         # Schemata and modules locations
         "schemata": os.path.join(basedir, "lib", "schema"),
         "modules": os.path.join(basedir, "lib", "modules"),
         # Location for storing original data sources for rebuilding as
         # well as media that is not in the XML database
         "source-docs": os.path.join(basedir, "source-docs"),
         # Location of deleted documents, only the most recent deleted version
         # is retained
         "deleted-docs": os.path.join(basedir, "DeletedArchive"),
         "logs": os.path.join(basedir, "logs"),  # log directory
         # temporary submissions directory (see XMLMethods::submitJob)
         "temporary": os.path.join(basedir, "TemporaryFiles"),
         "ResultXML": os.path.join(basedir,"ResultXML"),
         "ImportConfig": os.path.join(basedir, "lib", "import-map-specifications")

    })

def createdirsIfNeeded():
    for k, v in names.items():
        if v.startswith(basedir):
            # Check if directory exists
            if not os.path.isdir(v):
                # Don't create anything with an extension, it's a file
                (fname, ext) = os.path.splitext(v)
                if ext == '':
                    try:
                        os.mkdir(v)
                    except OSError as what:
                        print("Unable to create directory %s"%(v))
                        print("Is the resource directory %s specified correctly?"%(basedir))
                        raise what

def get_schemata():
    "get_schemata - Return dictionary of schemata names and paths"

    results = dict()    
    files = glob.glob(os.path.join(names["library"], "schema", "*.xsd"))
    for f in files:
        _, name = os.path.split(f)            
        results[name] = f
        
    return results
    
    
     
setbasedir("c:/Users/Tethys")  # default base directory