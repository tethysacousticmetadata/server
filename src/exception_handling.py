import traceback

# Generic methods for exception handling

def get_traceback_str(e):
    """
    Given an exception e, format the traceback string
    :param e: exception
    :return: stack backtrace string
    """

    components = traceback.TracebackException.from_exception(e).format()
    return "".join(components)