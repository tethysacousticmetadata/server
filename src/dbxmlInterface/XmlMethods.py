# XmlMethods.py
# Methods supported by XML server

# Standard Python modules
from dataclasses import dataclass
from collections import defaultdict
import glob
import os
import re
import shutil
import string
import sys
import threading
import gzip

# 3rd party modules
# Berkeley dbxml modules
import dbxml    
import maya
import diskcache
import pandas as pd

# user modules ---------------------------------------------------------

from .XmlAbstractions import XmlContainer
import DataImport.data_parser
import DataImport.translator
import DataImport.FileImport
import DataImport.NetworkImport
import resources
import xml.dom.minidom
import validate
from benchmark import PerformanceMonitor, Timer

import XML.sink
import XML.Report
from XML.transform import escape, strip_namsepaces

# mediators for foreign databases/services
import mediators.manager

from .error import DocumentReport, DocumentFailure, BerkeleyDbErr
from .error import XmlQueryEvaluationErrorExtended, QueryError
from .error import ImportReport, DBInternalError
from dbxml import XmlDocumentNotFound


stripq_re = re.compile("(^\".*\"$|^'.*'$)")


def stripquotes(s):
    """Remove matching single/double quotes at beginning & end of string"""
    match = stripq_re.match(s)
    if match:
        s = s[1:len(s) - 1]
    return s


def namevalues(s, stripQuotes=True, separator="/"):
    """"Parse strings of form name1=val1/name2=val2 into lists.
    List of lists returned:  [[name1, val1], [name2, val2]]
    If stripQuotes is True, matching quotes from the first & last position
    will be removed from both names and values.
    The character separator indicates the symbol expected to separate list
    items."""
    
    items = s.split(separator)  # List [a=b, c=d, ...]
    pairs = [item.split("=") for item in items]  # List [[a,b] [c,d], ...]
    if stripQuotes:
        # Strip quotes from each item in [[a,b], [c,d], ...]
        pairs = [[stripquotes(subitem) for subitem in pair] for pair in pairs]
    return pairs

def copyfile(src, dest, rmsrc=False, bufsz=64*1024):
    """
    Copy src file to dest file.
    :param src:  Source file
    :param dest:  Destination file
    :param rmsrc:  Remove source file after copy
    :param bufsz:  Copy file in chunks of N bytes
    :return:  None
    """

    with open(src, 'rb') as src_h:
        with open(dest, 'wb') as dest_h:
            buf = src_h.read(bufsz)
            while buf:
                dest_h.write(buf)
                buf = src_h.read(bufsz)

    if rmsrc:
        try:
            os.remove(src)
        except Exception as e:
            # Shouldn't hit this unless someone has an open handle
            # on the file
            print(f"Unable to remove {src}.  Cause: {e}")

class XmlMethods(object):
    """Supported methods for server"""
    title = 'XML Database Interface via XML Remote Procedure Calls'
    name = 'Spatial-temporal-database'

    tmp_container_lock = threading.Semaphore()  # ensure atomic access
    tmp_container = "temp_external_trans"  # define scratch work container
    # Temporary container document id
    tmp_container_doc_id = 0

    # Query cache parameters
    gigabyte = int(2 ** 30)
    diskcache_limit = 25 * gigabyte
    
    # benchmark instance written to log
    performance_monitor = None

    # The following contains are created on startup if they do not exist
    # Switched TransferFunctions for Calibrations
    default_containers = ['Detections', 'Deployments', 'Ensembles',
                          'Localizations', 'SpeciesAbbreviations',
                          'SourceMaps', 'ITIS', 'ITIS_ranks',
                          'Events', 'Calibrations',
                          tmp_container,
                          mediators.manager.Manager.mediator_cache]

    # Mapping between root elements and the container in which they reside
    root2containers = {
        "Calibration": "Calibrations",
        "Detections": "Detections",
        "Deployment": "Deployments",
        "Ensemble": "Ensembles",
        "Localize": "Localizations",
        "Abbreviations": "SpeciesAbbreviations",
        "Mapping": "SourceMaps",
        "ITIS": "ITIS",
        "ranks": "ITIS_ranks"
    }

    # The following containers are always opened with validation enabled
    # Note that XQuery Update *does not* enforce validation.
    validated_containers = ['Detections', 'Deployments', 'Ensembles',
                          'Localizations', 'SpeciesAbbreviations', 'Calibrations',
                          'ITIS', "ITIS_ranks", "SourceMaps"]
     
    # When problematic documents are encountered (invalid XML, bad schema, etc.)
    # should they be saved to the logs/doc directory?
    saveProblemDocs = True
    

    
    # base64 encoded file string
    # Expect:  file<fname>:base64-encoded-content
    base64_re = re.compile("^file<(?P<fname>[^>]+)>:(?P<content>.*)$")
    
    # Map file regular expression
    # Used to store information about which maps were used when
    # importing a document
    maps_re = re.compile(r"""
        <Maps>
            <SourceMap>
                (?P<SourceMap>[^<]*)
            </SourceMap>
            (<SpeciesAbbreviations>
                (?P<SpeciesAbbreviations>[^<]*)
             </SpeciesAbbreviations>)?             
        </Maps>""", re.X)

    server = None

    def __init__(self, manager, transactions=False, debug=False, enablecache=True):
        self.debug = debug
        self.python_string = string
        self.manager = manager
        self.containers = {}
        self.transactions = transactions
        self.container_flags = 0

        if self.transactions:
            print("Starting DB XML in transactional mode")
            self.container_flags = self.container_flags | \
            dbxml.DBXML_TRANSACTIONAL
        else:
            print("Starting DB XML")


        # Post schema validation routines for collection types
        # that cannot be fully enforced through schema 
        self.validation = {
            "Detections": validate.Detections,
            "SpeciesAbbreviations": validate.SpeciesAbbreviations,
            "SourceMaps": validate.SourceMap,
            #"Deployments" : validate.Deployment,
            }

        # Query cache
        xquery_gb = 128
        self.xquery_cache_active = enablecache  # Cache enabled
        self.xquery_cache = diskcache.Cache(
            directory=resources.names['diskcache'],
            eviction_policy="least-recently-used",
            size_limit=int(xquery_gb * self.gigabyte),
            statistics=True,
        )
        print(f"Query cache: {xquery_gb} GB at {resources.names['diskcache']}, enabled={enablecache}")

        # Mediator management
        self.mediator_mgr = mediators.manager.Manager(self)

        # Automatically enable performance monitor if debug enabled
        # Otherwise enabled by RESTful PUT:
        #  PUT server_url:port/Tethys/performance_monitor/on
        if self.debug:
            self.set_perfmon(True)
        
        
    def access_container(self, name, create=False):
        """
        Return the handle to a dbxml container collection
        :param name: Container name
        :param create: True|False - If True, creates if it does not exist
        :return: container handle
        """

        # TODO:  Parts of this should perhaps be protected by a semaphore...
        if name in self.containers:
            container = self.containers[name]
        else:
            # not in our cache, 
            # let's see if it is exists and has not been opened
            config = dbxml.XmlContainerConfig()
            config.setTransactional(self.get_transactional())
            #print "get_transactional in _access_container ",self.get_transactional()
            config.setThreaded(True)
            if name in self.validated_containers:
                config.setAllowValidation(True)

            # Determine appropriate transaction (if any) & if we should commit
            try:
                container = XmlContainer.open(self, name, config)
                self.containers[name] = container  # cache handle for later
            except dbxml.XmlContainerNotFound as e:
                # todo:  decide whether or not to set transactional stuff,
                # perhaps we only want this for temporary entities we don't
                # care about...  Also questions of page size, etc.
                if create:
                    if name == self.tmp_container:
                        # No validation or indexing
                        self.container_add(name, False, False)
                    else:
                        self.container_add(name) 
                    # Container will have been added
                    container = self.containers[name]
                else:
                    raise e
            except Exception as e:
                raise e

        return container

    def release(self):
        # close containers and release their resources
        for container in list(self.containers.values()):
            container.close()
            del container

    def __get_tempdoc_id(self):
        """__get_tempdoc_id() - Atomically acquire a temporary name
        Wraps at 1e6 documents, assume that we won't have more than that
        many simultaneously open tables"""
    
        XmlMethods.tmp_container_lock.acquire()
        docname = "temp_%d" % (XmlMethods.tmp_container_doc_id)
        XmlMethods.tmp_container_doc_id = (XmlMethods.tmp_container_doc_id + 1) % 1000000 
        XmlMethods.tmp_container_lock.release()
        return docname   
 
    def register_server(self, server):
        """Set server handle"""
        self.server = server
        
    def set_perfmon(self, active=True):
        """
        Set performance monitor state.  Ensures that performance
        monitor is in selected state.

        :param active: True -> ensure active, False -> ensure deactivated
        :return:  None

        Note:  Use perfmon_active() to check state
        """

        if self.performance_monitor is not None:
            # performance monitor is running
            if not active:
                # user requested deactivation
                del self.performance_monitor
                self.performance_monitor = None
        else:
            # no performance monitor active
            if active:
                # Start the performance monitor
                self.performance_monitor = PerformanceMonitor()

    def perfmon_active(self):
        """perfmon_active()
        Indicate whether or not a performance monitor is running.
        :return: True if running, else False
        """
        return self.performance_monitor is not None

    def log(self, message):
        """
        write text to server log
        :param message:
        :return:
        """
        if (not self.server is None):
            self.server.log_error(message)
        else:
            raise RuntimeError("Unable to log, server not registered")
        
    def _savexmldoctolog(self, xmlfname, xmldoc, formatxml=True):
        """_savexmldoc2log - Write xmldoc to log/xmldocs/xmlfname
        formatxml will pretty print the XML document (using faster minidom method)
        """
        
        (_, ext) = os.path.splitext(xmlfname)
        if ext != ".xml":
            xmlfname = xmlfname + ".xml"
        
        basedir = resources.names['logs']  # Get the log directory
        # Verify log/xmldocs exists, create if needed
        directory = os.path.join(basedir, 'xmldocs')
        if not os.path.exists(os.path.join(basedir, 'xmldocs')):
            try:
                os.mkdir(directory)
            except OSError as e:
                self.log("Unable to create log directory %s:  %s" % (
                        directory, e.what))
        # Write out the file
        try:
            fh = open(os.path.join(directory, xmlfname), 'w')
            if formatxml:
                xml_dom = xml.dom.minidom.parseString(xmldoc)
                xmldoc = xml_dom.toprettyxml()
            fh.write(xmldoc)
        except (OSError, IOError) as e:
            self.log("Unable to save XML document %s to log directory" % (xmlfname))
            
    def test(self):
        return "<ServerStatus> alive </ServerStatus>"
    
    def checkPrivileges(self, credentials, operation, container, obj=None):
        """checkPrivileges - Credentials sufficient for operation?
        Checks to see if user has sufficient privileges
        """
        
        # todo: implement
        return True
 
    def container_add(self, name, validation=True, defaultIndex=True):
        """
        Create a new container.
        :param name: Container name
        :param validation: Should container be validated?
        :param defaultIndex: Use container's default index?  If True,
           sets index nodes to on.  The actual indices used for the container
           are container specific and defined for a limited number of containers
           in XmlAbstractions.Indices
        :return:
        """

        config = dbxml.XmlContainerConfig()  # default
        config.setAllowValidation(validation)
        config.setThreaded(True)
         
        if defaultIndex:    
            config.setIndexNodes(dbxml.XmlContainerConfig.On)
        else:
            config.setIndexNodes(dbxml.XmlContainerConfig.Off)
        pageSize = 32 * 1024
        if pageSize is not None:
            try:
                config.setPageSize(pageSize)
            except Exception as e:
                print(e)
                raise e

        container = XmlContainer.create(self, name, config)

        print(f"Created container {name}: pageSize {container.getPageSize()},"
              f" validation {validation} node-indices {defaultIndex}")

        self.containers[name] = container  # note for future references
        return True
        
    def container_exists(self, container_name):
        """Does the specified container exist?"""

        # See if its has already been opened
        exists = container_name in self.containers
        if not exists:
            # Check if it exists but has not been opened
            exists = self.manager.existsContainer(container_name) > 0
            
        return exists
        
    def container_remove(self, name, txn=None):
        """
        Removes specified container and all of the documents contained within
        Use with caution!!
        :param name:  container name
        :param txn: transaction handle
        :return: None
        """
        # todo:  Add security

        # Close the container and remove from cache if open
        try:
            container = self.containers[name]
            del container
            del self.containers[name]
        except KeyError:
            pass  # container is not in container cache, assume not open
        
        # Delete the container from the backing store
        [commit, txn] = self.get_transaction(txn)
        if txn is not None:
            self.manager.removeContainer(txn, name)
            if commit:
                txn.commit()
        else:
            self.manager.removeContainer(name)
    
    def container_clear(self, name):
        """container_clear - DANGEROUS:  Remove all documents from the container
        name is the name of the container
        """
        
        # todo:  Add security
        
        container = self.access_container(name)
        cleared = container.clear()
        
        if cleared:
            # Move the source documents (XML, Excel, etc.) to a special
            # area for deleted documents
            self.remove_collection(name)
        return cleared
    
    def container_reindex(self, name, txn=None):
        """
        container_reindex - Reindex the container.  Performs in-place
        modifiction of the container.  DANGEROUS:  Backup before using
        :param name:  container to be reindexed
        :param txn:  Make reindexing part of specified transaction.  If None
           a transaction will be created/committed if the container uses
           transactions.
        :return:  None
        """

        # todo:  Add security
        if self.performance_monitor:
            timer = self.performance_monitor.start()

        # Create objects needed for reindexing
        update_context = self.manager.createUpdateContext()
        # create if needed and note that we will need to commit
        commit, txn = self.get_transaction(txn)

        if txn is None:
            self.manager.reindexContainer(name, update_context)
        else:
            try:
                self.manager.reindexContainer(txn, name, update_context)
            except Exception as e:
                # Something went wrong, abort the transaction if we created it
                if commit:
                    txn.abort()
                raise  # propagate error up the chain

        if commit:
            txn.commit()  # only commit method created the transaction

        if self.performance_monitor:
            self.performance_monitor.stop(timer, 'reindexed', name)

    def doc_exists(self, container_name, doc_name, txn=None):
        """Does the specified document in container exist?
        When the container does not exist, we return False as opposed
        to throwing an exception"""
        
        exists = True  # until we learn otherwise
        container = None  # make sure remains in scope
        
        # Get handle to container
        try:
            container = self.access_container(container_name)
        except dbxml.XmlContainerNotFound:
            exists = False

        # Use a transaction? If so, commit it?
        [commit, txn] = self.get_transaction(txn)
        try:
            if exists:
                container.getDocument(doc_name, txn)
                #container.getDocByNameElement(doc_name, txn)
        except dbxml.XmlDocumentNotFound:
            exists = False
        except dbxml.XmlDatabaseError:
            exists = False

        if commit:
            txn.commit()
        return exists

    def get_abbreviations_map(self, map_id):
        """
        Retrieve a species abbreviation map as a Pandas data frame
        :param map_id: name of SpeciesAbbreviations map
        :return:  DataFrame with TSN, completename, coding, Group
        """

        q = f"""
        declare default element namespace "http://tethys.sdsu.edu/schema/1.0";
        collection("SpeciesAbbreviations")/Abbreviations[Name="{map_id}"]
        """
        map_xml = self.query(q, clean_results=True)
        if map_xml == "":
            raise ValueError(
                f'There is no SpeciesAbbreviation map named "{map_id}"')
        map_df = pd.read_xml(map_xml, xpath="./Map")
        map_df = map_df.set_index('coding')

        # pd.read_xml (v1.4.3 at time of writing) does not appear to read
        # attributes when they do not appear on the first line.  To update
        # map_df with the appropriate group, we need to get the groups first
        # and associate them with the proper coding.
        try:
            # Retrieve the group names
            groups = pd.read_xml(
                map_xml, xpath="./Map/completename[@Group]", attrs_only=True)
        except ValueError:
            groups = None  # species abbreviations has not Group attributes

        if groups is not None:
            # Get the coding for these groups, then combine with groups
            group_coding_df = pd.read_xml(
                map_xml, xpath="./Map[completename/@Group]")
            coding_df = pd.concat([group_coding_df.coding, groups], axis=1)

            # Join on coding axis
            coding_df = coding_df.set_index('coding')
            map_df = pd.concat([map_df, coding_df], axis=1)
        else:
            # abbreviation map does not have any Groups, add default so
            # this is not a special case
            map_df["Group"] = float('nan')

        return map_df

    def get_document(self, container_name, doc_name, txn=None):
        """Get document by name from container"""

        # Get handle to container
        container = self.access_container(container_name)
        
        doc_str = None
        [commit, txn] = self.get_transaction(txn)
        try:            
            xml_doc = container.getDocument(doc_name, txn)
            doc_str = xml_doc.getContent()
        except XmlDocumentNotFound:
            doc_str = "notfound"
            if commit:
                txn.abort()
                commit = False
        except Exception as e:
            doc_str = str(e)
            if commit:
                txn.abort()
                commit = False
                
        if commit:
            txn.commit()
        return doc_str
    
    def get_transactional(self):
        """Are transactions desired?"""
        return self.container_flags & dbxml.DBXML_TRANSACTIONAL > 0
    
    def get_transaction(self, txn=None):
        """Transaction analysis
        Returns a pair [commit, transaction]
        commit - If txn was not None, we assume that it contains a
                 transaction record in which this operation will take
                 part.  Consequently, we would expect the initiater
                 of the transaction to perform the commit after all
                 operations in the transaction are complete. The caller
                 can check the commit part of the tuple to see if they
                 should commit or leave it to a higher level.
        transaction - Contains either a transaction to be used or None
                      meaning no transaction is to be used.  None is
                      returned when txn is None and the database
                      was initialized without transaction support.
                      (See self.get_transactional() which reports this.)
        """

        useTxn = txn is not None or self.get_transactional()  # Transactional?

        if txn is None and useTxn:
            # User did not specify a transaction but wants
            # transactional processing
            commit = True
            txn = self.manager.createTransaction()
        else:
            commit = False
        
        return [commit, txn]

    def get_manager(self):
        """Return the dbxml.XmlManager instance"""
        return self.manager

    def get_mediator_manager(self):
        """
        Access the mediator manager
        :return: instance of mediators.manager.Manager
        """
        return self.mediator_mgr

    def import_data(self, collectionName, spec, form):
        """Import data into collection using data specification
        
        collectionName - name of collection to which we will import docs
        spec - Dictionary detailing import specification
        form - Form variables associated with the request including data
            files and attachments
            
        It is assumed that this method is invoked by a method associated
        with the cherrypy framework and that form is populated appropriately
        for a RESTful POST request.
        """

        # Generate XML error reports
        sink = XML.sink.Sink(True)  # We rely on indentation, do not change
        report = ImportReport(sink)

        collection = self.access_container(collectionName, False)
        if collection is None:
            sink.elementText("Error", f'No such resource: "{collectionName}"')
            raise ValueError(sink.finalize())

        attachments = None
        if "docname" in spec:
            print(f'Processing {spec["docname"]}')
            # Attachments might be present when a document name is specified
            attachments = self.__process_attachments(
                form, sink, spec["docname"])
        else:
            print(f'Processing untitled document(s)')
            spec["docname"] = None

        committed = DataImport.FileImport.Sources(
            self, collection, sink, spec, form, attachments)
        
        if 'committed' in locals() and committed[0]:
            if attachments is not None:
                verified = attachments.verified
            else:
                verified = None
            if spec['sources'][0]['mimetype'] != 'application/xml':
                # Save import options for sources that might be using them
                self._save_meta(collectionName, spec["docname"],
                                spec['source_map'] if 'source_map' in spec else None,
                                spec['species_map'] if 'species_map' in spec else None,
                                verified)
        
        """
        
        if 'committed' in locals() and committed[0]:
            # We committed one or more documents, save a copy
            # of the file in the source-docs directory
            fileH.seek(0)   # Rewind
            self._save_source(collectionName, fileH, basename + ext, overwrite)
            fileH.close()
            if not attachments is None:
                verified = attachments.verified
            else:
                verified = None
            self._save_meta(collectionName, basename, importmap, 
                                abbreviations, verified)                   
                #if the report is empty .close() gives errors.
                #quick and dirty test to see if there are elements
                #not even sure report.close is necessary, with sink.finalize()
            if len(report.sink.xml._element_stack)>0:
                report.close()
        """
        return sink.finalize()


    def post_data(self, collectionName, form, op):
        """
        Deprecated in favor of self.import_data.
        This method will be removed in a future release.

        post_data - POST event - add to a collection
        specify collection.  form is a dictionary containing the form data
        and any ancillary data that is not stored directly in the XML
        
        Expected form variables:
        collectionName - Name of XML collection to which data is to be added
        form - Dictionary of information guiding what should be added
            Key    Semantics
            data   Data to be added, e.g. spreadsheet or XML
                   Information is a list of tuples, exactly one of which is
                   expected: (filename, mimetype, open file handle)
            Attachment
                    Ancillary information about the data to be added
                    When this is used, it is assumed that a single data file
                    is to be added.  
                    Information is a list of files similar to data.
            import_map - Optional, indicates import map to use
            species_abbr - Optional, indicates species abbreviations to use
        op - How is data to be processed?  "file" or "odbcfile"
        Assumes called from CherryPy framework 
        """
        
        collection = self.access_container(collectionName, False)
        if collection is None:
            raise ValueError(f"No such resource {collectionName}")

        # Retrieve mappings for data translation (importmap) and
        # species names to ITIS.
        importmap = form.get('import_map', None)
        abbreviations = form.get('species_abbr', None)

        # Determine filename and data type for import
        (filename, mimetype, fileH) = form['data'][0]
        [basename, ext] = os.path.splitext(filename)

        # Generate XML error reports
        sink = XML.sink.Sink(True)  # We rely indentation, do not change
        report = ImportReport(sink)

        attachments = self.__process_attachments(form, sink, filename)
                
        # Overwrite documents if they exist?
        overwrite = form['overwrite']

        committed = None
        if op == "file":
            committed = DataImport.FileImport.Stream(
                self, collection, sink, importmap, abbreviations,
                fileH, filename, mimetype, overwrite, attachments)
        elif op == "odbcfile":
            committed = DataImport.FileImport.ODBC(
                self, collection, sink, importmap, abbreviations,
                fileH, filename, mimetype, overwrite, attachments)
        
        if 'committed' in locals() and committed[0]:
            # We committed one or more documents, save a copy
            # of the file in the source-docs directory
            self._save_source(collectionName, fileH, basename + ext, overwrite)
            if attachments is not None:
                verified = attachments.verified
            else:
                verified = None
                
            # don't need to save meta if .xml
            self._save_meta(collectionName, basename, importmap, 
                            abbreviations, verified)
            # if the report is empty .close() gives errors.
            # quick and dirty test to see if there are elements
            # not even sure report.close is necessary, with sink.finalize()
            if report.sink.xml.depth() > 0:
                report.close()
                
        return sink.finalize()
    
    def post_data_ConnectionString(self, collectionName, form):
        """post_data_ConnectionString - post new data taken from an
        Open Database Connectivity (ODBC) source that was not transmitted
        with the request (e.g. data is on server's machine or a separate
        server is used.
        """

        collection = self.access_container(collectionName, False)
        if collection is None:
            raise ValueError(f"No such resource {collectionName}")
        
        # Retrieve mappings for data translation (importmap) and
        # species names to ITIS.
        importmap = form.get('import_map', None)
        abbreviations = form.get('species_abbr', None)
        docname = form.get('DocId', 'harp')
        overwrite = form.get('overwrite', None)

        # Retrieve the connection string
        connect_str = form.get('ConnectionString', None)
        
        # Generate XML error reports
        # we rely on Sink being indented, do not change
        sink = XML.sink.Sink(True)
        report = ImportReport(sink)
        
        if 'Attachment' in form:
            DocumentFailure(sink,
                            "Attachments are not allowed for ODBC sources that"
                            " are expected to produce more than one result.")
            raise ValueError(sink.finalize())
            
        committed = DataImport.NetworkImport.ODBC(
            self, collection, docname, sink, importmap,
            abbreviations, overwrite, connect_str)
        
        if not committed:
            raise ValueError(sink.finalize())
        
        return sink.finalize()

    def __process_attachments(self, form, xmlsink, filename):
        """__process_attachments - internal method
        Used for pulling attachments from a CherryPy form"""
        
        # Build an object containing the attachment files that are to
        # be referenced by this document.  This will be passed to the
        # parser which will validate that it matches the attachments
        # referenced in the document.
        
        if 'Attachment' in form:
            attachments = validate.Attachments()
            attachments.add(form['Attachment'])
            if attachments.has_errors():
                doc = DocumentReport(filename, xmlsink)
                doc.report_errors(attachments)
                if attachments.has_warnings():
                    doc.report_warnings(attachments)
                
                attachments.get_problems(True, xml=xmlsink.xml)
                del doc  # Close off DocumentReport
                raise ValueError(xmlsink.finalize())
        else:
            attachments = None
        return attachments

    def _save_meta(self, collection, basename, sourcemap=None,
                   abbreviations=None, attachments=None):
        """
        _save_meta - Write information needed should we ever have to
        import the data again
        """
        docs = resources.names['source-docs']
        tgtdir = os.path.join(docs, collection)
        
        map_ext = '.sourcemap'
        
        if not os.path.exists(tgtdir):
            os.mkdir(tgtdir)
        if sourcemap or abbreviations:
            fname = os.path.join(docs, tgtdir, "%s%s" % (basename, map_ext))
            with open(fname, 'w') as fh:
                fh.write("<Maps>")
                if sourcemap:
                    fh.write(f"<SourceMap>{sourcemap}</SourceMap>")
                if abbreviations:
                    fh.write(f"<SpeciesAbbreviations>{abbreviations}" 
                             "</SpeciesAbbreviations>")
                fh.write("</Maps>")
                
        if attachments is not None:
            for itemtype in attachments.keys():
                keys = attachments[itemtype].keys()
                if itemtype == "Application":
                    for key in keys:
                        body = attachments[itemtype][key]
                        mime = body.content_type.value
                        if mime.endswith("zip"):
                            if basename not in body.filename:
                                raise ValueError(f"Submitted attachment {body.filename} that does not contain document identifier {basename}")
                            filename = os.path.join(tgtdir, body.filename)
                            with open(filename, 'wb') as outH:
                                body.file.seek(0)
                                attachbytes = body.file.read()
                                outH.write(attachbytes)
                else:
                    # Ensure there is a -attach/image or -attach/audio dir
                    attachdir = os.path.join(
                        tgtdir, basename + "-attach", itemtype)
                    if not os.path.exists(attachdir):
                        os.makedirs(attachdir)
                    for key in keys:
                        body = attachments[itemtype][key]
                        filename = os.path.join(attachdir, body.filename)
                        with open(filename, 'wb') as outH:
                            body.file.seek(0)
                            attachbytes = body.file.read()
                            outH.write(attachbytes)
        
    def _save_source(self, collection, tmpfile, dest, overwrite=False):
        """_save_source - Move the file from its temporary location to 
        the source file repository.
        tmpfile may be either a file handle or file name (string)
        """

        # Assumes collection has been validated, it is a valid container
        # name.
        isfname = isinstance(tmpfile, str)
        
        docs = resources.names['source-docs']
        tgtdir = os.path.join(docs, collection)
        if not os.path.exists(tgtdir):
            os.makedirs(tgtdir)
        # See if the source exists
        tgtdoc = os.path.join(tgtdir, dest)
        
        # Find basename of file (w/o extension)
        destbase, destext = os.path.splitext(dest)

        # Attachments may be in destbase + each attachment suffix
        if os.path.exists(tgtdoc) and overwrite:
            self.remove_source_docs(collection, destbase)
        
        if isfname:
            # Assume this is a filename
            try:
                os.rename(tmpfile, tgtdoc)
            except OSError as e:
                # If the temp directory and the tgtdir are on different
                # filesystems, rename will fail.
                # Win:  WinError 17 ...cannot move file to different disk drive
                # UNIX:  Errno 18 Invalid cross-device link
                # good cross-platform way to do this.
                try:
                    copyfile(tmpfile, tgtdoc, rmsrc=True)
                except Exception as e:
                    raise ValueError(
                        f"Unable to move temporary file to {tgtdoc}: {e}")
        else:
            tmpfile.seek(0) # Rewind
            # Assume a file handle and copy to the destination file
            fileH = open(tgtdoc, "wb")
            fileH.write(tmpfile.read())
            fileH.close()
            tmpfile.close()
            
    def _save_xml_source(self, collection, xmldoc, basename, overwrite=False):
        """
        Similar to _save_source, but for an XML string
        :param collection:  collection associated with xml document
        :param xmldoc:  text of xml document
        :param basename: filename (w/ or w/o .xml)
        :param overwrite:  overwrite if exists?
        :return:
        """

        docs = resources.names['source-docs']
        tgtdir = os.path.join(docs, collection)
        if not os.path.exists(tgtdir):
            os.makedirs(tgtdir)

        # See if the source exists
        tgtdoc = os.path.join(tgtdir, basename)
        if not tgtdoc.lower().endswith(".xml"):
            tgtdoc = tgtdoc + ".xml.gz"
        else:
            tgtdoc = tgtdoc + ".gz"
        
        # Attachments may be in basename + each attachment suffix
        if os.path.exists(tgtdoc):
            if overwrite:
                self.remove_source_docs(collection, basename)
            else:
                return   # We do not overwrite the existing document
        
        if isinstance(xmldoc, str):
            # should always be true            
            with gzip.open(tgtdoc, 'wt') as gzip_source_xml:
                gzip_source_xml.write(xmldoc)
        else:
            raise TypeError("generated xmldoc is not a string")            
        

    def checkpointmsg(self, result):
        curr_date = maya.now()
        if result:
            print(f"Tethys checkpointed at {curr_date.iso8601()}")
        else:
            print(f"FAILED Tethys checkpoint attempt at {curr_date.iso8601()}")

    def exploreResult(self, result):
        """exploreResult - Given an XmlResult, print out the structure"""

        for item in result:
            # Recursively exploe the XmlValues
            self.aux_exploreResult(item)

    def aux_exploreResult(self, xmlval, depth=0):
        """
        Print out an XmlValue tree, displaying information about the tree
        :param xmlval:  XmlValue (item in XmlResult)
        :param depth: maximum depth to which information is displayed
        :return: None
        """

        while not xmlval.isNull():
            # Information about the current node
            print(("  "*depth), end=' ')  # indent
            print(f"Name {xml.getNodeName()}, type {xmlval.getNodeType()} "
                  f"value {xmlval.GetNodeValue()} str {xmlval.isString()} "
                  f"num {xmlval.isNumber()}"
                  )

            # Process first child
            child = xmlval.getFirstChild()
            if child is not None:
                self.aux_exploreResult(child, depth+1)

            # Grab next sibling and update loop condition
            xmlval = xmlval.getNextSibling()

    def query_plan(self, queryStr):
        """Return XML showing how the query woudl be executed.
        Does not support queries to external collections
        """

        # Create a query context and prepare the query, but don't execute it
        q_context = self.manager.createQueryContext()

        # We'll do this without transactions as we will not actually run
        # the query (might be a mistake)
        plan = self.manager.prepare(queryStr, q_context)
        result = plan.getQueryPlan()
        del q_context
        return result

    # Recognize collection("name") or collection('name')
    # Used by query_cache_clear to only remove cached results related
    # to specific collections.
    quoted_collection_re = re.compile(
        r"""collection\(\s*(['"])(?P<collection>[^\1]+?)\1\s*\)""")

    def query_cache_clear(self, collections=[]):
        """
        Clear out the XQuery cache
        :param collections:  If [], all entries are cleared.  If a list
          of collection names, any query that references any of the names
          in the list (e.g., ["Deployments", "ext:horizons"] will removed
          from the cache.  A string may be used to clear a single collection.
        :return:  None
        """

        if not isinstance(collections, list):
            collections = [collections]  # ensure a list

        if len(collections) == 0:
            self.xquery_cache.clear()
            self.log("XQuery cache cleared")
        else:
            # Ensure case-independence by converting everything to lower case
            collections = [c.lower() for c in collections]
            # For each document in cache, see if it needs to be purged
            for key in self.xquery_cache.iterkeys():
                referenced = self.quoted_collection_re.findall(key)
                if len(referenced):
                    for r in referenced:
                        # Extract out collection name
                        r = r[1].lower()
                        if r in collections:
                            del self.xquery_cache[key]
                            break
            self.log(f"XQuery cache cleared documents using: {collections}")

    def query_cache_status(self, value=None):
        """

        :param value:
          If not None, activates (True) or deactivates (False) cache
        :return:
          Returns cache status, True if active
        """

        if value is not None:
            self.xquery_cache_active = value is True
        return self.xquery_cache_active

    def query_cache(self, queryStr, clean_results):
        """
        See if XQuery is in cache
        :param queryStr:  XQuery
        :param clean_results: Remove namespaces & pretty print?
        :return: serialized result of XQuery (string) or None if not in cache
        """
        key = f"clean:{clean_results}" + "\n" + queryStr
        if key in self.xquery_cache:
            result = self.xquery_cache[key]
        else:
            result = None
        return result

    # Match updating query.  Matches all cases we currently use in XQuery 1.0
    # (https://www.w3.org/TR/xquery-update-10).  It does not handle
    # upd: primitives.
    query_update_re = re.compile(
        r"""(
        replace\s+?(node|value) |
        insert\s+?node|
        rename\s+?node |
        delete\s+?node
        )""", re.VERBOSE)

    def query(self, queryStr, clean_results=False):
        """
        Execute an XQuery
        :param queryStr:  XQuery
        :param clean_results:  Remove namespaces and pretty print
        :return:  serialized result of XQuery (string)
        """

        # Determine if this an updating query
        update = len(self.query_update_re.findall(queryStr)) > 0

        result = None
        if not update:
            if self.performance_monitor:
                timer = self.performance_monitor.start()

            # See if query is in cache
            result = self.query_cache(queryStr, clean_results) \
                if self.xquery_cache_active else None

            if result is not None and self.performance_monitor:
                self.performance_monitor.stop(
                    timer, "Retrieved query from cache")

        if result is None:
            # We have not run this query recently, execute it.
            queryRslt = self._query(queryStr)

            if self.performance_monitor:
                timer = self.performance_monitor.start()

            result = "\n".join([item.asString() for item in queryRslt])

            if self.performance_monitor:
                self.performance_monitor.stop(timer, 'construct_xml_string')

            del queryRslt

            resultN = len(result)
            if resultN > 0:
                # This is a bit simple-minded for now, clean up once we
                # have better external query interface
                # Check for errors by looking for error/exception string
                resultStart = result[0:min(resultN, 100)].lower()
                if "error" in resultStart or "exception" in resultStart:
                    raise QueryError(queryStr, result)

                if clean_results:
                    result = strip_namsepaces(result)

            if update:
                # Determine which collections this update referenced
                # and remove any cached queries as they may no longer
                # be accurate
                referenced = self.quoted_collection_re.findall(queryStr)
                # Pull out collection names as lowercase
                collections = [r[1].lower() for r in referenced]
                self.query_cache_clear(collections)  # remove stale entries
            elif self.xquery_cache_active and resultN > 0:
                # Add result to not recently used cache for later usage
                key = f"clean:{clean_results}" + "\n" + queryStr
                self.xquery_cache[key] = result

        return result

    def _query(self, queryStr, q_context=None):
        """
        Execute an XQuery/XPath query, returns
        :param queryStr: XQuery string
        :param q_context: QueryContext (will create if not given)
        :return: XmlResults object which user must delete

        Note:  The internal _query never uses cached queries, use query
        if you wish to take advantage of the cache
        """
            
        # Query external data if needed
        queryStr = self.mediator_mgr.xquery(queryStr)
        # context needed to process
        if q_context:
            deleteContext = False
        else:
            q_context = self.manager.createQueryContext()
            deleteContext = True
        # doesn't work yet...
        # q_context.setBaseURI(resources.names["tethys-modules"]

        if self.debug:
            print(f"Running query\n{queryStr}\n")
        try:
            # Run the query
            if self.performance_monitor:
                timer = self.performance_monitor.start()
            queryRslt = self.manager.query(queryStr, q_context)
            if self.performance_monitor:
                self.performance_monitor.stop(
                    timer, 'query_execution', queryStr)
        except dbxml.XmlQueryParserError as e:
            e_str = str(e)
            new_e = QueryError(queryStr, e_str)
            raise new_e
        except dbxml.XmlQueryEvaluationError as e:
            raise XmlQueryEvaluationErrorExtended(e, queryStr)
        except dbxml.XmlDatabaseError as e:
            # Problem with underlying Berkeley DB
            raise BerkeleyDbErr(e, queryStr)

        except Exception as e:
            raise e  # execute finally block to clean up and bail

        if deleteContext:
            del q_context
        
        if self.debug and self.container_exists(XmlMethods.tmp_container):
            print("external container exists")
        return queryRslt
    
    def query_doc(self, query, doc):
        """Query an in-memory XmlDocument, use . as root of paths
        Returns an XmlResults that the user is responsible for deleting"""
        
        q_context = self.manager.createQueryContext()
        # compile the query
        q_expression = self.manager.prepare(query, q_context)
        
        value = dbxml.XmlValue(doc)
        results = q_expression.execute(value, q_context)
        
        del q_context
        return results
    
    def _sort_detections(self, doc_str):
        """
        Runs an XQuery on XML documents to sort <On/OffEffort>
        sections of Detections documents. Returns the sorted document

        :param doc_str: xml as a string
        :return:
        """

        prolog = 'import schema namespace ty="http://tethys.sdsu.edu/schema/1.0" at "tethys.xsd";'
        query = prolog + \
        """declare function local:sort-detections
          ( $dets as item()* )  as item()* {
           for $d in $dets/Detection
           order by xs:dateTime($d/Start)
           return ($d,"&#10;", "&#x9;")
         };
        
        (: return a deep copy of  the element and all sub elements :)
        declare function local:copy-and-sort($element as element()) as element()* {
           element {node-name($element)}
              {$element/@*,
                  for $child in $element/node()
                      return    (
                       if(string(node-name($child)) = "OnEffort") then (
                        <OnEffort>&#10;&#x9;{local:sort-detections($child)}</OnEffort>
                       )
                        else if (string(node-name($child)) = "OffEffort") then (
                        <OffEffort>&#10;&#x9;{local:sort-detections($child)}</OffEffort>
                        )
                        else if ($child instance of element()) then (
                          local:copy-and-sort($child)
                          )
                            else $child
                            )
              }
        };
        
        let $hdr := '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>'
        let $doc := ./ty:Detections
        return ($hdr,local:copy-and-sort($doc))
        """
        memory_doc = self.manager.createDocument()
        memory_doc.setContent(doc_str)
        result = self.query_doc(query, memory_doc)
        newdoc = [r.asString() for r in result]
        sorted_doc = "\n".join(newdoc)
        return sorted_doc
    """
    def sort_via_lxml(self,doc_str):
        xslt_root = etree.XML('''
        <xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
            <xsl:template match="node()|@*">
                <xsl:copy>
                    <xsl:apply-templates select="node()|@*"/>
                </xsl:copy>
            </xsl:template>
             
            <xsl:template match="OnEffort">
                <xsl:copy>
                    <xsl:apply-templates select="Detection">
                      <!--ISO8601 Expected!-->
                      <xsl:sort select="Start"/> 
                    </xsl:apply-templates>
                </xsl:copy>
            </xsl:template>
            <xsl:template match="OffEffort">
                <xsl:copy>
                    <xsl:apply-templates select="Detection">
                      <!--ISO8601 Expected!-->
                      <xsl:sort select="Start"/> 
                    </xsl:apply-templates>
                </xsl:copy>
            </xsl:template>
        </xsl:stylesheet>''')
        transform = etree.XSLT(xslt_root)
        doc = etree.fromstring(doc_str)
        result_tree = transform(doc)
        return str(result_tree).encode('UTF-8')
        """
    
    def remove_document(self, container_name, title,
                        txn=None, clear_cache=True):
        """
        Remove document by document title from specified container
        :param container_name: container/collection name
        :param title: document title
        :param txn: Transaction with which this operatin is asociated
           (will be created if None)
        :param clear_cache: Clear references from query cache.
           In general, this should be True, but in cases where
        :return:
        """

        # obtain handle to container
        container = self.access_container(container_name)
        # Get transaction if not passed in and needed and 
        # indicator as to whether or not we need to commit
        [commit, txn] = self.get_transaction(txn)
        container.rmDocument(title, txn)
        if commit:
            txn.commit()
            # Remove stale cache entries
            self.query_cache_clear(container_name)
        self.remove_source_docs(container_name, title)
        
        return True
    
    def remove_source_docs(self, container_name, title):
        """Checks deleted archive for anything related to 'title'
        and removes them. Then moves source-docs related to 'title'
        to deleted archives
        
        Updated to no longer clear deleted-docs if there is nothing to replace
        """
        
        # Process source docs, they should be moved to the deleted-docs
        # directory.
        src = os.path.join(resources.names['source-docs'], container_name)
        dest = os.path.join(resources.names['deleted-docs'], container_name)
        
        if os.path.isdir(dest):
            # Remove any previously deleted documents
            old = glob.glob(os.path.join(dest, f'{title}*'))
            for f in old:
                if os.path.isdir(f):
                    # Remove attachment directories
                    for d in ["-attach", "-image", "-audio"]:
                        if f.lower().endswith(d):
                            shutil.rmtree(f, True)
                elif os.path.isfile(f):
                    _unused_head, tail = os.path.split(f)
                    name, _unused_ext = os.path.splitext(tail)
                    if name == title:
                        # Document related to this XML document
                        os.remove(f)
        else:
            # Create container folder if not there
            os.mkdir(dest)    
        
        # deleted-docs should be empty now, move documents
        # associated with the current file to deleted-docs
        todelete = glob.glob(os.path.join(src, f'{title}*'))
        for f in todelete:
            if os.path.isdir(f):
                # Move attachment directories
                for d in ["-attach", "-image", "-audio"]:
                    if f.lower().endswith(d):
                        destpath = os.path.join(dest, title+d)
                        os.rename(f, destpath)
            elif os.path.isfile(f):
                # Move associated files
                _unused_head, tail = os.path.split(f)
                name, _unused_ext = os.path.splitext(tail)
                if name == title:
                    # Document related to this XML document
                    destpath = os.path.join(dest, tail)
                    os.rename(f, destpath)
                    
    def remove_collection(self, container_name):
        """Checks deleted archive for the collection, removes it.
        Moves the source-doc collection to deleted archive"""
        # Process source docs, they should be moved to the deleted-docs
        # directory.
        src = os.path.join(resources.names['source-docs'], container_name)
        dest = os.path.join(resources.names['deleted-docs'], container_name)

        if os.path.isdir(src):
            if os.path.isdir(dest):
                # Remove previously deleted collection
                shutil.rmtree(dest, True)
            # Move current src-doc folder to deleted archive
            os.rename(src, dest)

    def remove_document_path(self, docpath, txn=None):
        """
        Remove document via dbxml path
        :param docpath:  dbxml path, e.g.:  dbxml:///collection/docId"
        :param txn: transaction (will generate one if needed and None)
        :return:
        """

        # Parse out collection and document title
        elements = docpath.split("/")
        if len(elements) != 5:
            raise ValueError(f'Bad document path:  "{docpath}" '
                             'expected form dbxml:///collection/docId')
        
        self.remove_document(elements[-2], elements[-1], txn)

    def _iter_docs(self, container, name, data_source, sink,
                   summary=True, replace=False, txn=None, auxInfo=None,
                   rebuild=False):
        """Iterate over a data_source, adding one or more XML documents.
           container - container to which documents will be added
           name - documents are given this name.  If more than one
               document is added, numbers are appended
           data_source - parser that will be iterated over
           sink - XML sink to which log entries will be written
           replace - Replace existing documents
           txn - Tie everything to a current open transaction.  Note that
               transactions that become too long can cause problems.
           auxInfo - object that can verify higher level information
               not contained in the XML document (e.g. additional media files,
               etc.)  auxInfo will be called on the validator object which
               is created for this container type.  If the auxInfo callback 
               detects any errors it should log an error.
           summary - Generate a summary report in the XML stream of which
               documents were added and which failed.
           rebuild - flag to indicate container rebuilding
           Returns two lists of documents:
               successfully added, failed
           """

        [commit, txn] = self.get_transaction(txn)
        
        count = 0

        # Get a validator for this container type
        container_name = container.getName()
        if container_name in self.validation:
            validatorfactory = self.validation[container_name]
        else:
            # no validator for this type, use generic
            validatorfactory = validate.Instance
        validator = validatorfactory(self)
        errors = False  # No errors yet
        docs_added = []  # List of document ids added to the database
        docs_all = []
        
        # Iterate through the documents the user is interested in adding
        # The errors associated with data_source are document specific.
        # The data_source.has_errors() function reflects the errors with
        # the most recently processed document. 
        
        report = None
        dociterator = iter(data_source)
        done = False
        xmldoc = None
        while not done:
            if report is not None:
                del report  # close off previous report
            if name is not None:
                # User has specified a name for the document.
                docname = name if count == 0 else f"{name}_{count}"
            count = count + 1

            try:
                xmldoc = next(dociterator)
                # We produced the document, replace the name with the Id
                # if it has one
                if isinstance(xmldoc, bytes):
                    # Unclear why this is happening for XML files.
                    # They should be decoded after a round trip
                    # through element tree
                    # an encoding string, maybe it's the wrong one?
                    xmldoc = xmldoc.decode()
                if data_source.multiple_document_check():
                    # If the iterator can return multiple documents, use
                    # the Id as the document name.  This can get us in trouble
                    # if the Id's are not unique, which is only a problem for
                    # Calibrations
                    m = re.search("<Id>(?P<id>.*)</Id>", xmldoc)
                    if m:
                        docname = m.group('id')
            except ValueError as v:
                errors = True
                DocumentFailure(sink, docname, "UnableToParse", str(v))                
                continue
            except StopIteration:
                done = True
                continue
            except Exception as e:
                raise e
            finally:
                if not done:
                    docs_all.append(docname)  # track documents processed
                    # Check if document contained species coding errors
                    # (these do not throw errors when being processed)
                    # They could be checked in the validator, but it would
                    # require a second pass.
                    if hasattr(data_source, 'species_encoder'):
                        unknown_tsn = data_source.species_encoder.get_missing()
                        if unknown_tsn is not None:
                            errors = True
                            DocumentFailure(sink, docname,
                                            "UnrecognizedSpecies", unknown_tsn)
                            continue


            #nameOfMapping = pd.read_xml(xmldoc)['Name'][0]

            # See if the document exists and if we need overwrite it
            #exists = self.doc_exists(container.getName(), doc_name, txn)
            exists = self.doc_exists(container.getName(), docname, txn)
            existsNoOverwrite = exists and not replace
            if existsNoOverwrite:
                # must explicitly allow overrwriting, note error
                data_source.cannot_overwrite()

            if self.saveProblemDocs is True and not existsNoOverwrite:
                saveFn = lambda: self._savexmldoctolog(docname, xmldoc)
            else:
                saveFn = None
           
            # Report writing object.                
            # If the document had problems and saveProblemDocs is true,
            # a copy of the XML document will be written to a log directory         
            report = DocumentReport(docname, sink, saveFn)
            
            if data_source.has_errors():
                report.report_errors(data_source)              
                errors = True
                continue  # No further processing of this xmldoc
            elif data_source.has_warnings():
                report.report_warnings(data_source)

            # We have generated successful XML, validate anything that cannot
            # be validated via schema. If detections document, sort the
            # detections
            
            if container_name == "Detections":
                
                # use later....
                if False:
                    # start_time = timeit.default_timer()
                    xmldoc = self.sort_via_lxml(xmldoc)
                    # elapsed = timeit.default_timer() - start_time
                    # print(elapsed)
                if False:
                    # start_time = timeit.default_timer()
                    xmldoc = self._sort_detections(xmldoc)
                    # elapsed = timeit.default_timer() - start_time
                    # print(elapsed)

            if validator is not None:
                if rebuild is True:
                    validator.setRebuild()
                validator.validate(xmldoc, docname, container.container_name)
                if auxInfo is not None:
                    # auxiliary may have additional information that
                    # can add errors or remove potential ones
                    auxInfo.validate_attachments(validator)
                                        
                if (validator.source_has_errors(docname) or
                        validator.source_missing_media(docname)):
                    errors = True
                    # Note problems with this source and clear out for
                    # next validation
                    report.report_errors(validator)
                    
                    if self.saveProblemDocs is True:
                        self._savexmldoctolog(docname, xmldoc, True)
                    continue  # No further processing of this xmldoc
                
                elif validator.source_has_warnings(docname):
                    report.report_warnings(validator)

                if validator.is_modified():
                    # Update the document based on validation modifications
                    xmldoc = validator.get_doc()

            # If we made it this far, the document has been validated with
            # additional rules from the validation
            # object. Schema validation occurs on addString
            try:
                if exists:
                    container.updateString(docname, xmldoc, txn=txn)
                else:
                    container.addString(docname, xmldoc, txn=txn)
                report._added()
                docs_added.append(docname)
            except dbxml.XmlException as e:
                report.report_errstr(str(e))
                self.log(str(e))
                errors = True
                continue
            
        if 'report' in locals() and report is not None:
            del report  # close off previous report
            
        if commit:
            if len(docs_added) >= 1:
                txn.commit()
                
                # do not save sources if this is an update operation.
                if not rebuild:
                    # save source document
                    datatype, fname, mimetype, data = \
                        data_source.get_current_source_doc(close=True)
                    if datatype == "file":
                        # Copy the file to the appropriate container
                        _, tgt_file = os.path.split(fname)
                        self._save_source(
                            container.container_name, fname,
                            tgt_file, overwrite=replace)
                    elif datatype == "data" and mimetype == 'application/xml':
                        self._save_xml_source(
                            container.container_name, data, fname, replace)
                    else:
                        # Source document is not available.  This may be
                        # due to a source generating multiple documents
                        # or not being suitable for saving, e.g. a query
                        # to a database.
                        pass

                # Invalidate XQuery cache entries that use this container
                self.query_cache_clear([container_name])
            else:
                txn.abort()  # Didn't commit anything
        
        docs_failed = list(set(docs_all) - set(docs_added))
        if summary is True:
            XML.Report.DocumentSummary(sink, docs_added, docs_failed)
        
        if errors:
            # Raise an error letting the caller know if anything was added as
            # they might want to retain source documents.
            v = ValueError(sink.finalize())
            v.documents_added = docs_added
            v.documents_failed = docs_failed
            raise v
        
        return docs_added, docs_failed
        
    def update_documents(self, containerName, directory, replace=True, clear=False, sink=None):
        """Build a collection from documents located in a directory
           relative to the resources source documents directory
           Generates a BatchLog for the operation when updating Detections,
           localizations, and ITIS
           containerName - container to which documents should be added
           directory - source directory
           replace - Update existing documents
           clear - Empty the container before updating"""
        
        container = self.access_container(containerName, True)

        # Generate XML error reports
        # These lines may be obsolete as UpdateWorker will supply either:
        # a LogSink (if detections/localizations/itis) else a Sink
        if not sink:
            # Create an indented sink (do not remove indentation, we rely
            # on this)
            sink = XML.sink.Sink(True)
        
        if clear:
            container.clear()  # Remove all documents

        sink.xml.startElement("UpdateDocuments", {})
        sink.xml.newline()
        sink.xml.startElement("Collection", {})
        sink.xml.characters(containerName)
        sink.xml.endElement("Collection")
        sink.xml.newline()
        sink.xml.startElement("ClearBeforeUpdate", {})
        sink.xml.characters(str(clear))
        sink.xml.endElement("ClearBeforeUpdate")
        sink.xml.newline()
        sink.xml.startElement("ReplaceExistingDocuments", {})
        sink.xml.characters(str(replace))
        sink.xml.endElement("ReplaceExistingDocuments")
        sink.xml.newline()

        # Find all source documents and add them
        unknown_ext = set()
        sourcedir = os.path.join(resources.names['source-docs'], directory)
        successes = []
        failures = []
        files = []
        try:
            files = os.listdir(sourcedir)
        except Exception as e:
            pass
        
        # The following extensions may occur in a SourceDocs directory, but
        # these represent byproducts of imports or other files that should
        # be skipped.
        map_ext = '.sourcemap'
        attachment_ext = '.zip'
        xslt_ext = '.xslt'  # Extensible stylesheet language transformation
        extensions_to_skip = [map_ext, attachment_ext, xslt_ext]
        
        source_docs = defaultdict(list)
        
        for f in files:
            basename, ext = os.path.splitext(f)
            fullpath = os.path.join(sourcedir, f)
            if ext in extensions_to_skip or os.path.isdir(fullpath):
                # skip sourcemaps, attach zips, directories
                continue
            source_docs[basename].append(f)

        for basename in list(source_docs.keys()):
            print(f"Processing {basename}: {source_docs[basename]}")
            # get related files beginning with this name
            # can make this better. once a file is completed, should remove
            # it from the file list so won't have to search through the
            # whole thing for subsequent files
            
            source_files = source_docs[basename]

            # find sourcefile with the most recent modified time
            times = [os.path.getmtime(os.path.join(sourcedir, source))
                     for source in source_files]
            
            target_source = source_files[times.index(max(times))]
            
            fname = os.path.join(sourcedir, target_source)
            # /a/b/SOCAL32.xls to: root = /a/b/SOCAL32, ext=.xls
            root, ext = os.path.splitext(fname)
            
            sourcemap = None
            abbreviations = None

            try:
                # Retrieve translator etc.
                map_filename = os.path.join(sourcedir, root + map_ext)
                mapping = open(map_filename, "rt").read()
                m = self.maps_re.match(mapping)
                if m:
                    sourcemap = m.group("SourceMap")
                    abbreviations = m.group("SpeciesAbbreviations")
            except IOError:
                if ext != ".xml":
                    print(f"No sourcemap for {map_filename} (might be expected)")
            
            try:
                if ext in [".xls", ".xlsx"]:
                    # Spreadsheets are either parsed with ODBC or
                    # the workbook_parser.  We can tell from the sourcemap
                    # which one we need to use (table->ODBC,sheet->workbook)
                    # If no sourcemap is provided, we assume that the
                    # workbook processor is used.
                    workbook = True  # Assume workbook
                    if sourcemap is not None:
                        # Check SourceMap to see if a Table directive is used
                        queryStr = f'count(collection("SourceMaps")/Mapping[Name="{sourcemap}"]/Directives//Table)>0'
                        try:
                            table_present = self.query(queryStr)
                            workbook = table_present.lower() == "false"
                        except Exception:
                            raise ValueError(
                                f"Unable to access SourceMap {sourcemap}")
                            
                    if workbook:
                        parser = DataImport.data_parser.workbook_parser(
                            self, fname, sourcemap, abbreviations)
                    else:
                        connector = DataImport.data_parser.connector.MicrosoftExcel(DBQ=fname)
                        connection = mediators.ODBC.source(connector)
                        docid = os.path.basename(root)
                        sources = {
                            docid: {
                                'type': 'file',
                                'file': os.path.basename(fname),
                                'name': docid,
                                'connector': connector.get_connector(),
                                'connection': connection,
                                'sql': True,
                                'mimetype': DataImport.FileImport.getMimeType(ext),
                                'tempfile': fname
                                }
                        }
                        parser = DataImport.translator.Translator(
                            self, sources, translator=sourcemap,
                            default_docid=docid,
                            speciesabbr=abbreviations)
                        parser.prepare()
                        #parser = DataImport.data_parser.odbc_parser(
                        #    self, connector, sourcemap, abbreviations)
                elif ext in [".csv"]:
                    parser = DataImport.data_parser.CSVparser(
                        self, fname, sourcemap, abbreviations)
                elif ext in [".xml"]:
                    parser = DataImport.data_parser.xml_parser(self, fname)
                elif ext in [".gz"]:
                    # extract the file
                    with gzip.open(fname, 'rb') as source_xml_gz:
                        xml = source_xml_gz.read()
                    parser = DataImport.data_parser.xml_parser(
                        self, basename, xml)
                else:
                    if ext != "":
                        unknown_ext.add(ext)
                    continue
            except ValueError as v:
                # Missing source map errors end up here
                DocumentFailure(sink, basename, str(v))
                failures.append(basename)
                continue

            # debugging issues with document parsing can be difficult when
            # everything is wrapped up in a try block.  Set this variable
            # to false to bail out when there is an error.
            try_block = True
            if try_block:
                try:
                    s, f = self._iter_docs(
                        container, basename, parser, sink,
                        False, replace, rebuild=True)
                except Exception as e:
                    if hasattr(e, 'documents_added'):
                        s = e.documents_added
                        f = e.documents_failed
                    else:
                        s =[]
                        f = [basename]
                    # Continue to next basename, finally will be executed first
                    continue
                finally:
                    parser.close()
                    successes.extend(s)
                    failures.extend(f)
            else:
                [s, f] = self._iter_docs(container, basename, parser,
                                         sink, False, replace, rebuild=True)
                parser.close()
                successes.extend(s)
                failures.extend(f)

        XML.Report.DocumentSummary(sink, successes, failures)
        return sink.finalize()

    def addString(self, container_name, title, content, txn=None, flags=0):
        """
        add a document to a container
        :param container_name: container to which we should add
        :param title: document title, must be unqiue
        :param content: string with XML that conforms to the container type
            (no validation is done)
        :param txn: If running in transaction mode, transaction to which
            this add should be associated.  If txn is None, we create and
            commit a transaction if appropriate.
        :param flags: container add flags
        :return: True if successful, otherwise raises error
        """

        container = None  # put in scope

        # get a handle to the container
        try:
            container = self.access_container(container_name)
        except Exception:
            raise ValueError(f"Unable to access container '{container_name}'")

        # Add the new XML document
        [commit, txn] = self.get_transaction(txn)
        
        try:
            container.addString(title, content, flags, txn=txn)
        except Exception:
            # Something went wrong, abort the transaction if we created it
            if commit:
                txn.abort()
            raise  # propagate the error up the chain
        
        if commit:
            txn.commit()
            
        return True

    def setString(self, container_name, title, xml_doc, txn=None):
        """
        Given an existing document
        :param container_name: container/collection name
        :param title: document title
        :param xml_doc: new xml string
        :param txn: transaction to associate this operation with
           (will generate if None and the database is in transactional mode)
        :return:  None
        """
        # get a handle to the container
        try:
            container = self.access_container(container_name)
        except Exception:
            raise ValueError(f"Unable to access container '{container_name}'")

        # Add the new XML document
        [commit, txn] = self.get_transaction(txn)

        try:
            container.updateString(title, xml_doc, txn=txn)
        except Exception:
            # Something went wrong, abort the transaction if we created it
            if commit:
                txn.abort()
            raise  # propagate the error up the chain

        if commit:
            txn.commit()

    def addURL(self, container_name, title, url):
        """
        Add specified URL to a container
        :param container_name:  target container (string)
        :param title: document title
        :param url: document content at url
        :return: Empty string "" on success, diagnostic string on failure
        """
        if self.debug:
            print("Adding URL")

        container = None  # put in scope
        try:
            # get a handle to the container, creating it if needed
            container = self.access_container(container_name)
        except Exception as e:
            print(f"Unable to access container {container_name}")
            return str(e)

        try:
            container.addURL(title, url)
        except Exception as e:
            return str(e)
        
        return ""
        
    def shutdown(self):
        """Exit the server"""
        
        # todo:  check privileges
        sys.exit()
             
        

    
    
         
