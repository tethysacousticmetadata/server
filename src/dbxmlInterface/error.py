'''
Created on Jul 20, 2013

@author: mroch
'''

# Standard Python library modules
import math
import re
import io
from xml.sax.xmlreader import AttributesNSImpl, AttributesImpl

# local imports
import dbxml

from XML.transform import map2elem
from XML.indent import XMLIndentGenerator


class DBInternalError(Exception):
    """ Error for server internal problems
        Attributes:
            serv - server-side error
            user - user-side error
    """
    
    def __init__(self, serv=None, user=None):
        self.serv = serv
        self.user = user
    
    def __str__(self):
        if self.serv is None:
            msg = ""
        else:
            msg = ": %s" % (self.serv)
        return 'DBInternalError: %s' % (msg)
        
    def userstr(self):
        if self.user:
            return 'Error: %s\n' % (self.user)
        else:
            return 'Error: Database Server Internal Error, please contact system administrator with details\n'

user_err_re = re.compile('Error: User-requested error \[(?P<code>[^\]]*)\]')
error_type_re = re.compile('(?P<errcode>.*) ?Error: (?P<Msg>.*)')
lineno_re = re.compile('.*:(?P<line>\d+):(?P<col>\d+)\s*($|, line.*$)')
linechar_re = re.compile('.*at line (?P<line>\d+)(, char (?P<char>\d+))?.*')
    
    
def construct_newline_fn(xmlSink):
    """ Does the xmlSink object support newline() ?
    xmlSink must be an XMLGenerator or something derived from it.
    (XMLGenerator does not have a newline() method, but some
     classes derived from it might.)
     
    Sample usage:
        nl = construct_newline_fn(xmlSink)
        
        xmlSink.startElement("MagicWord")
        nl()
        xmlSink.characters("xyzzy")
        nl()
        xmlSink.endElement("MagicWord")
        nl()
        
    """
    supports_nl = callable(getattr(xmlSink, 'newline', None))
    # Construct function that calls newline if xmlSink supports it,
    # otherwise does nothing
    return lambda : xmlSink.newline() if supports_nl else None
    

def get_errcode(errstr, xmlElement=False):
    """Get the error code from an XQuery result
    If xmlElement is True, the returned value will be a valid XML element name
    """
    
    details = None
    m = user_err_re.match(errstr)
    if m:
        val = m.group('code')
    else:
        m = error_type_re.match(errstr)
        if m:
            val = m.group('errcode').strip()
            details = m.group("Msg").strip()
        else:
            val = errstr
            
    if xmlElement:
        val = map2elem(val)        
        
    return (val, details)

def get_lineno(errstr):
    "Get the line and column number if they are present"
    
    m = lineno_re.match(errstr)
    val = dict()
    if m:
        val = m.groudict()
    else:
        m = linechar_re.match(errstr)
        if m:
            val = m.groupdict()
    
    return val  

def err2xml(errstr, xmlSink):
    "Write a dbxml error message to an XMLGenerator"
    
    (err, details) = get_errcode(errstr, True)
    attribs = get_lineno(errstr)
      
    nl = construct_newline_fn(xmlSink)
    if details:
        nl() #adding this here since removing from start <Document> element
        xmlSink.startElement(err, attribs)
        xmlSink.characters(details)
        xmlSink.endElement(err)
        nl()
    else:
        xmlSink.emptyElement(err, attribs)
        nl()
    
def dict2msg(d, indent=0):
    """Dictionaries are sometimes used to store a set of errors.
    Convert the dictionary to a message. 
    
    Error dictionaries are multilevel dictionaries with
    the following format:  d[L1][L2][L3]
    L1 - Specifies the problem type
    L2 - Either a string with details, or a dictionary whose keys are
         the datacollection with which the problem occurred
         (e.g a query or sheet in a workbook)
    L3 - List of rows in the datacollection where the problem occurs
         (L3 is not used if L2 is a string)
    """
        
    lines = []
    pad = " " * indent
    for problem in list(d.keys()):
        for collection in list(d[problem].keys()):
            if isinstance(d[problem], str):
                line = "%s%s %s" % (pad, problem, collection)
                lines.append(line)
            else:
                # Assume we have another level of dictionary
                for source in list(d[problem][collection].keys()):
                    someproblem = d[problem][collection][source]
                    if isinstance(someproblem, str):
                        line = "%s%s %s %s: %s" % (
                            pad, problem, collection, source, someproblem)
                    else:
                        line = "%s%s %s %s on rows: %s" % (
                            pad, problem, collection, source,
                             " ".join(['%d' % (r) 
                                       for r in someproblem]))
                    lines.append(line)
    return "\n".join(lines)

def addErrMsg(problemdict, category, message, detail=None):
    """
    Add a problem description to a dictionary.  The dictionary will be used to produce
    problem reports.
    :param problemdict:  target dictionary to add to
    :param category:  problem category, should be a valid element tag
    :param message:  Human readable description of problem
    :param detail: Optional text that provides additional information

    Example: addErrMsg(errors,
                       "Unable to parse timestamp",
                       'line: 392 value: "2025-01-01T00:32:68Z"')

    :return:  None, modifies dictionary
    """

    # Construct category dictionary if needed
    if category not in problemdict:
        problemdict[category] = dict()

    if message not in problemdict[category]:
        problemdict[category][message] = list()

    if detail:
        if isinstance(detail, list):
            problemdict[category][message].extend(detail)
        else:
            problemdict[category][message].append(detail)

    return
            

def dict2msgXML(problemdict, xml):
    """
    Dictionaries are sometimes used to store a set of errors.
    Convert the dictionary to an XML message on an existing XML
    stream.  XML stream must support the SAX XMLGenerator class.
    If the class supports a newline method, it will be used to
    provide formatted XML.

    Problem dictionaries are multilevel dictionaries with
    the following format:  problemdict[L1][L2]...[LN]

    Each level will be rendered as an element and the final value
    will be converted to a string.

    :param problemdict: multi-level dictionary with the following
     characteristics
       L1 - category of error
       L2 - Human readable message
           L2 keys have values that are list.  Each list item is reported as
           a detail of the L1/L2 error
    :param xml:  Stream supporting the SAX XMLGenerator class
    :return:
    """
    
    nl = construct_newline_fn(xml)
    no_attr = {}
    for k in problemdict:
        subdict = problemdict[k]
        element = map2elem(k)  # Ensure legal element name

        nl()
        xml.startElement(element, no_attr)

        # Note issue
        for issue in subdict.keys():
            nl()
            xml.startElement("Issue", no_attr)
            nl()
            xml.startElement("Description", no_attr)
            xml.characters(issue)
            xml.endElement("Description")
            # Note details about issue
            for v in subdict[issue]:
                nl()
                xml.startElement("Detail")
                xml.characters(v)
                xml.endElement("Detail")

            xml.endElement("Issue")

        xml.endElement(element)


class ImportReport(object):
    """Generates surrounding <Import></Import> tags for an xml sink.
    Closing tag generated when del called or instance goes out of scope"""
    
    def __init__(self, sink):
        self.sink = sink
        self.sink.xml.startDocument()
        self.sink.xml.newline()
        self.sink.xml.startElement("Import", {})
        self.sink.xml.newline()
        
    def close(self):
        self.sink.xml.endElement("Import")
        self.sink.xml.newline()                
        self.sink.xml.endDocument()
    
        
    
def DocumentFailure(sink, docname, *args):
    """Similar to a DocumentReport, this is used for a single catastrophic
    error that prevents us from parsing anything else.  args is a list of 
    XML structuring elements.
    e.g. DocumentFailure(sink, "SOCAL32GLowFreq", "ImportFailure",
                        "Document source requires Sheet import, Table specified"))
    """
    sink.xml.startElement('Document', {'name':docname})
    N = len(args)
    if N > 0:
        for idx in range(N-1):
            sink.xml.newline()
            sink.xml.startElement(args[idx], {})
            
        sink.xml.characters(args[N-1])
            
        for idx in range(N-2, -1, -1):
            sink.xml.endElement(args[idx])
            sink.xml.newline()

    sink.xml.endElement('Document')
    sink.xml.newline()
        
    
class DocumentReport(object):
    "Generates an XML report for a document import"
    def __init__(self, docname, sink, saveFn=None):
        """
        Track progress on parsing of a document.
        :param docname: document name
        :param sink: an instance of an XML sink object that can be written to
            The report will be in the sink object when the report goes out of
            scope or is deleted (del)
        :param saveFn:  Handle to a function that will save out the XML sink.
           This function should be a closure with the needed data and is
           invoked without any parameters.
        """
        self.errors = False
        self.warnings = False
        self.doc = docname
        self.sink = sink
        
        self.sink.xml.startElement('Document', {'name':self.doc})
        #self.sink.xml.newline()
            #commented this out, don't think its necessary to begin on a new line. maybe parse errors?
        
        self.saveFn = saveFn
        
    def report_errors(self, errors):
        self.__problem_prolog()        
        self.errors = True
        self.__problem_report(errors)
        
    def _added(self,text="added"):
        #self.sink.xml.emptyElement(text)
        #self.sink.xml.newline()
        self.sink.xml.characters(text)
        
    def report_warnings(self, warnings):
        self.__problem_prolog()
        self.warnings = True
        self.__problem_report(warnings)
        
    def __problem_report(self, problems):
        problems.get_problems(clear=True, xml=self.sink.xml)
        
    def __problem_prolog(self):
        if not self.has_problems():
#            self.sink.xml.startElement("Errors", {})
#            self.sink.xml.newline()
            pass
            
    def has_problems(self):
        return self.errors or self.warnings
    
    def report_errstr(self, errstr):
        self.__problem_prolog()
        self.errors = True

        self.sink.xml.startElement('Error')
        self.sink.xml.characters('\n')
        self.sink.xml.characters(errstr)
        self.sink.xml.characters('\n')
        self.sink.xml.endElement('Error')
        
    def __del__(self):
        """
        Complete this document report.  Writes out final element to
        the XML sink that was created with the document
        :return:
        """
        self.sink.xml.endElementTo("Document")
        
        if self.has_problems() and self.saveFn is not None:
            self.saveFn()   # Save the document someplace

# regular expression for parsing errors in queries
# Could create one bad-ass query with alternates, but hard enough to read
# as is:
error_re_list = [
    re.compile(r"(?P<errclass>\w+ [eE]rror): *(?P<msg>[^\[]+)\[(?P<errcode>[^\]]*).*line (?P<line>\d+), column (?P<col>\d+).*"),
    re.compile(r"(?P<errclass>[eE]rror): *(?P<msg>[^\[]+)\[(?P<errcode>[^\]]*).*:(?P<line>\d+):(?P<col>\d+).*"),
    re.compile(r"(?P<errclass>([A-Za-z]*Exception \d+, *)?[eE]rror): *(?P<errcode>.*)at line *(?P<line>\d+),? *char *(?P<col>\d+)\.? *(?P<msg>.*)")
]
class QueryError(ValueError):
    """Exception for errors from queries.
    Attributes:
        query - entire query that produced error
        result - result string
        context - troublesome portion of query (if identifiable)"""

    def __init__(self, query, result, context_lines=2):
        self.query = query

        # Try to parse the error
        for regexp in error_re_list:
            m = regexp.match(result)
            if m:
                break  # found it

        if m:
            # Save the matched error
            self.text = m.string[m.start():m.end()]
            # Able to find descriptor for line/column
            line = int(m.group("line")) - 1
            col = int(m.group("col")) - 1

            # save context of error in query
            context = []
            if line >= 0:
                context.append("Context:")
                query_lines = query.split("\n")
                for r in range(max(0, line - context_lines),
                               min(len(query_lines), line + context_lines)):
                    if r == line:
                        if col < len(query_lines[r]):
                            begin = query_lines[r][0:col]
                            end = query_lines[r][col:]
                            context.append(
                                f"{r+1:3d}:  {begin}__ERROR-HERE__{end}")
                        else:
                            context.append(
                                f"{r+1:3d}:  {query_lines[r]}__ERROR-HERE__")
                    else:
                        context.append(f"{r+1:3d}:  {query_lines[r]}")

            if len(context) > 0:
                self.context = "\n".join(context)
            else:
                self.context = query
        else:
            self.text = \
                f"Error in query, unable to parse error message: {result}"
            self.context = query

    def __str__(self):
        return "\n".join([
            f'Error: {self.text}', self.context])

class XmlQueryEvaluationErrorExtended(dbxml.XmlQueryEvaluationError):
    "XmlQueryErrorExtended - XmlQueryError that stores the failing query"
    def __init__(self, XmlQueryError, Query):
        # Copy attributes from XmlQueryError
        for name in list(XmlQueryError.__dict__.keys()):
            if name == 'what':
                if 'UnknownAbbreviation' in XmlQueryError.__dict__[name]:
                    setattr(self, name, getattr(XmlQueryError, name).replace('UnknownAbbreviation', 'Unknown Species Abbreviation'))
                else:
                    setattr(self, name, getattr(XmlQueryError, name))
            else:
                setattr(self, name, getattr(XmlQueryError, name))
        self.Query = Query

    def error_caret(self):
        lines = self.Query.split("\n")
        out = []
        for n in range(len(lines)):
            if n + 1 == self.getqueryLine():
                # print up to column, show error, and rest
                line = lines[n].rstrip()
                if self.getqueryColumn() > len(line):
                    out.append(line + "<Error>")
                else:
                    out.append(line[0:self.getqueryColumn()] +
                               "<Error>" + line[self.getqueryColumn():])
            else:
                out.append(lines[n])
        return "\n".join(out)

    def __str__(self):
        return "XmlQueryEvaluationError %s, line %d, column %d:\n%s" % (
            self.what, self.queryLine, self.queryColumn, self.error_caret())

class BerkeleyDbErr(RuntimeError):
    def __init__(self, e, operation=None):
        """BerkeleyDbErr (exception, operation)
        Problem with the underlying call to Berkeley db
        e is the Berkeley db exception
        operation, if present is a string description of what we were trying to do.
        """
        self.DbErrno = e.getDbErrno()
        self.exceptionCode = e.getexceptionCode()
        self.what = e.what
        self.operation = operation


    def __str__(self):
        "string representation of exception"
        msg = "Error in underlying Berkeley DB.  error number %d exception code %d"%(
            self.DbErrno, self.exceptionCode) + \
            "\nDescription %s"%(self.what)
        if not self.operation is None:
            msg = msg + "\nCalling operation: %s"%(self.operation)

        return msg

def show_context(text, line=10, column=None, context=10, highlight=True):
    """

    :param text:  Text document, e.g. XQuery, XML
    :param line: Line on which problem occurred or we wish to show context
        This is the line number starting from 1 which is what most error
        parsers will report.  Python will start lines from 0.
    :param column: Include to show a message at the column
    :param highlight:  Draw attention to line (and column) in output
    :return: List of annotated lines, can be joined with "\n".join(.)
    """

    if line < 0:
        return ["Error is not associated with a specific line, no context to show."]
    center = line - 1
    lines = text.split(sep='\n')
    start = max(0, center - context)
    stop = min(center + context, len(lines))

    # Determine how many digits we need for line number
    digits = math.ceil(math.log10(stop))
    if highlight:
        digits = digits+1  # Add column for marker

    annotated_lines = []
    max_cols = 0

    for l_idx in range(start, stop):
        l_num = l_idx+1
        if highlight and l_idx == center:
            # Draw attention to line with marker
            if column is None:
                text = f'*{l_num:{digits-1}d}: {lines[l_idx]}'
            else:
                if column < len(lines[l_idx]):
                    begin = lines[l_idx][0:column]
                    end = lines[l_idx][column:]
                    text = f"*{l_num:{digits-1}d}:  {begin} **ERROR**{end}"
                else:
                    text = f"*{l_num:{digits-1}d}:  {lines[l_idx]}**ERROR**"
        else:
            text = f'{l_num:{digits}d}: {lines[l_idx]}'
        # Keep track of maximum columns
        max_cols = max(max_cols, len(lines[l_idx]))
        annotated_lines.append(text)
    # Generate column guideline for top/bottom of XML segment
    guideline = []
    for idx in range(1, max_cols + 1):
        rem = idx % 5
        if rem == 0:
            if idx % 10 == 0:
                guideline.append(f"{int(idx / 10 % 10):d}")
            else:
                guideline.append('+')
        else:
            guideline.append('-')
    guideline = "N"*digits + "".join(guideline)
    annotated_lines.insert(0, guideline)
    annotated_lines.append(guideline)

    return annotated_lines
