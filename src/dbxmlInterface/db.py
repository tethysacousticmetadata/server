'''
Created on Apr 14, 2013

@author: mroch
'''

import math

import os
import errno

# Berkeley db XML 
import bsddb3.db
import bsddb3.dbobj
import dbxml

# local imports
from dbxmlInterface.XmlResolverTethys import Resolver
from dbxmlInterface.XmlMethods import XmlMethods
from benchmark import Timer


class Server(object):
    
    def __init__(self, xml_db, transaction=True, recovery=False, querycache=True):
        """
        Open Berkeley db key/value database store
        :param xml_db: database directory in file system, directory must exist
        :param transaction: If True, database operations are conducted
          in transactional mode.
        :param recovery: If True, opens database in recovery mode.  Concurrency
          is not supported in recovery mode.
        """
        self.transactional = transaction
        self.name = xml_db
        self.verbose = True

        """
        Information on open flags
            bsddb3.db.DB_CREATE       create if not already existing
            bsddb3.db.DB_INIT_LOCK    use locking to manage concurrency 
            bsddb3.db.DB_INIT_LOG     start logging system for crash recovery_result
            bsddb3.db.DB_INIT_MPOOL   provide cache for multi-threaded systems
            bsddb3.db.DB_THREAD       allow FREE THREADING
        """
        flags = \
            bsddb3.db.DB_CREATE \
            | bsddb3.db.DB_INIT_LOCK \
            | bsddb3.db.DB_INIT_LOG \
            | bsddb3.db.DB_INIT_MPOOL \
            | bsddb3.db.DB_THREAD 
        self.log_file_scrub_error = ''
        if transaction:
            
            # dbxml.DB_INIT_TXN use transactions
            flags = flags | bsddb3.db.DB_INIT_TXN 

            if recovery:
                # Run recovery_result in case it is needed
                # bsddb3.db.DB_RECOVER causes transaction logs to read and verified
                # Note that we cannot have multiple users access the database in recovery
                # mode which is why we open, recover, and exit


                print(f"\nExamining transaction logs in {xml_db} to verify "
                      "database integrity.\nThis may require a long time "
                      "(e.g., 1 h) when the Tethys has not been restarted "
                      "in a while or very large changes have been made to "
                      "the database.")
                timer = Timer()
                print(f"Transaction log processing started at {timer.get_start()}")
                tmp_env = self.make_dbEnv()
                recovery_result = tmp_env.open(xml_db, flags |
                                                 bsddb3.db.DB_RECOVER)
                if recovery_result is None:
                    print(f"Transition log processing complete. {str(timer)}")
                    print(f"Checkpointing database {self.name}... ", end=' ')
                    cp_result = tmp_env.txn_checkpoint(0, 0)  # Checkpoint if any activity occurred
                    if cp_result is None or cp_result == 0:
                        print("checkpoint complete")
                    else:
                        raise ValueError("checkpoint failed - invalid flag value or parameter")

                    log_files = tmp_env.log_archive()
                    # Convert from bytes to utf-8
                    log_files = [log_file.decode('utf8') for log_file in log_files]
                    # Recovered, close it up, we don't want to operating in recovery_result mode
                    tmp_env.close()

                    filenames = os.listdir(xml_db)
                    log_files_old = [f for f in filenames if f.startswith("log.")]
                    log_diff = set(log_files_old[0:-1]).difference(set(log_files))
                    if len(log_diff) > 0:
                        msg = \
                            "Note that the following log entries on disk " + \
                            "were not listed in the log archive: " + \
                            f"{log_diff}\nA file lock may have prevented these " + \
                            "from being removed and this is not " + \
                            "necessarily an issue.  These files will not be " + \
                            "removed automatically, but can be removed " + \
                            f"manually from {xml_db}.  "
                        print(msg)
                        self.log_file_scrub_error = msg

                    old_logs = os.path.join(xml_db, "verified_logs")
                    if len(log_files) > 0:
                        try:
                            os.mkdir(old_logs)
                        except OSError as e:
                            # Ignore if error is due to directory existing,
                            # otherwise propagate error
                            if e.errno != errno.EEXIST:
                                raise e
                        # Any existing log files have now been validated.
                        # Move all except the last file to the old logs home
                        # where they are left to convalesce having served a
                        # useful and productive life.  We leave the last log
                        # intact so that we continue to increment the log
                        # number
                        print(("Logs intact, moving logs to %s"%(old_logs)))
                        for f in log_files:  # used to be log_files[0:-1]
                            print("moving ",f)
                            # We use replace instead of move just in case the
                            # user has built a new environment and the log
                            # numbers are duplicated.
                            os.replace(os.path.join(xml_db, f),
                                       os.path.join(old_logs, f))
                else:
                    raise ValueError(
                        f"Unable to open {xml_db} in recovery_result mode")
            
        # open the environment 
        self.environment = self.make_dbEnv()
        result = self.environment.open(xml_db, flags)
        if result is None:
            print("BSDDB environment initialized")
        else:
            raise ValueError("Unable to open BSDDB")

        # create a manager for the environment
        # XmlManager expects underlying DBEnv object, not the Python wrapper
        # We violate the interface by accessing this directly
        self.manager = dbxml.XmlManager(self.environment._cobj,
            # tethys can query w/o explicitly opening/closing containers.
           dbxml.DBXML_ALLOW_AUTO_OPEN |
           # tethys can query external sources (e.g. URLs, files)
           dbxml.DBXML_ALLOW_EXTERNAL_ACCESS)
        
        # Add a custom resolver
        self.resolver = Resolver()
        self.manager.registerResolver(self.resolver)

        if self.verbose:
            # TODO: enable logging
            # Requires DBEnv->set_msgfile(DBEnv *env, FILE *outfile)
            # or DBENV->set_msgcall to be accessed.
            # These are not setup in the Python bindings and we would
            # need to create the bindings.
            # Below are the first steps for creating an FILE * handle
            # to a log file.

            # import ctypes
            # libc = ctypes.cdll.msvcrt  # C library
            # fileH = libc.fopen("msg_log.txt", "w")

            # self.environment._cobj contains the underlying pointer
            # to a DBEnv.  Unclear how to dereference to access the
            # set_msgfile function and appropriate parameters, will
            # need investigation.
            # self.environment._cobj.set_msgfile(fileH)

            # Select categories to log
            self.manager.setLogCategory(dbxml.CATEGORY_QUERY |
                                        dbxml.CATEGORY_OPTIMIZER |
                                        dbxml.CATEGORY_INDEXER, True)


        # Interface to dbxml
        self.methods = XmlMethods(self.manager, transaction, enablecache=querycache)


        # Generate the initial containers if they do not exist
        for c in self.methods.default_containers:
            if not self.methods.container_exists(c):
                self.methods.container_add(c)

    def make_dbEnv(self):
        "make_dbEnv() - Create a Berkeley DB environment"
        
        # Berkeley DB XML requires an environment which describes how a set of
        # one or more XML databases are to be accessed.
        #environment = bsddb3.db.DBEnv()
        environment = bsddb3.dbobj.DBEnv()
        
        # increase the defaults
        env_cache_GB = 1
        GB = 1024*1024*1024
        # Split into gigabytes and bytes    
        env_cache_b = int((env_cache_GB - math.floor(env_cache_GB))*GB)
        env_cache_g = int(env_cache_GB)
    
        # Cache tweaks for environments that don't handle 
        # 1GB contiguous memory regions.
        environment.set_cachesize(env_cache_g, env_cache_b, 2)
        print("Cache size set to %.2f GB"%(env_cache_GB))
        
        environment.set_lg_bsize(500000) #perhaps unnecessary
        
        # Locks are used on nodes of XML documents to lock access during
        # navigation (I believe...)
        default_locks = 50000   # default no. locks (1000 is default)
        maximum_locks = default_locks * 100
        #print "Setting # locks to %d"%(maximum_locks)
        environment.set_lk_max_lockers(default_locks)   # Max # concurrent lockers
        environment.set_lk_max_locks(maximum_locks)    # Maximum # locks
        environment.set_lk_max_objects(maximum_locks)  # Max # locked objects
        
        return environment
            
    def checkpoint(self, m=120):
        """checkpoint(m) - Checkpoint if more than m minutes has past since
           the last checkpoint"""
           
        if self.transactional:
            # txn_checkpoint(kb, min, flags)
            # kb - Checkpoint if >= N KB changed
            # min - Checkpointing in N minutes have passed since the last
            #       checkpoint
            # flags - DB_FORCE:  force even if no activity
            result = self.environment.txn_checkpoint(0, m, 0)
        else:
            result = None # no transactions
        return result is None
    
    def getEnvironment(self):
        "Access underlying Berkeley DB environment on which DBXML is built"
        return self.environment

    def getManager(self):
        "Access underlying Berkeley DBXML manager"
        return self.manager
    
    def getMethods(self):
        "Access the methods object for manipulating the database"
        return self.methods
    
    def isTransactional(self):
        "Is the database using transactions?"
        return self.transactional
    
    def release(self):
        "Shut down Berkeley DBXML"
        self.methods.release()      # close any open collections
        del self.manager
        self.environment.close()
