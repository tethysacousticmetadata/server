'''
Created on Jul 28, 2012

@author: mroch
'''

# standard Python modules
import pdb
from urllib.parse import urlparse
import os.path


# Berkeley db XML 
import bsddb3.db
from dbxml import XmlResolver, XmlInputStream, XmlException, XmlDocumentNotFound

import dbxml

# site packages
import resources


class Resolver(dbxml.XmlResolver):
    # X Query modules live in this directory
    librarydir = "../XQueryModules"
    debug = False
    
    def __init__(self):
        XmlResolver.__init__(self)  # initialize parent class
        if self.debug:
            print("Initializing resolver")
        if self.debug:
            print("Resolver initialized")
    
    def __relative_path__(self, path):
        """Resolve a relative path, 
            making certain that the user does not attempt to arbitrarily
            access the file system"""
            
        relpath = os.path.normpath(path)
        if relpath.startswith(".."):
            # User is trying to escape the library dir
            raise XmlException('Path at %s cannot '%(path) + 
             'reference parent directories.  ' +
             'Too many ".." references')
        
        return relpath
        
    def resolveSchema(self, txn, mgr, schemaLoc, nameSpace):
        if self.debug:
            print("Resolving schmea entry")
        url = urlparse(schemaLoc)
        if url.scheme == "":
            path = self.__relative_path__(schemaLoc)
            fullpath = os.path.join(resources.names['library'], 'schema', path)
            if os.path.exists(fullpath): 
                istream = mgr.createLocalFileInputStream(fullpath)
            else:
                # Not really the right exception, but the closest one
                # that's defined.
                raise XmlDocumentNotFound("Unable to resolve schema %s from lib/schema."%(path))
            
        elif url.scheme in ["ftp", "http", "https"]:

            if self.debug:
                print("Resolving schema URL using parent class")
            istream = XmlResolver.resolveSchema(self, txn, mgr, schemaLoc, nameSpace)

        if self.debug:
            print("Resolving schema %s to %s valid istream:  %s"%(
                nameSpace, schemaLoc, istream is not None))
        return istream
    
    def resolveModuleLocation(self, txn, mgr, nameSpace, result):
        if self.debug:
            print("resolveModuleLocation")
        istream = XmlResolver.resolveModuleLocation(self, txn, mgr, nameSpace, result)
        return istream
        
    def resolveModule(self, txn, mgr, location, nameSpace):
        if self.debug:
            print("Resolving module %s"%(location))

        istream = None
        #Determine if URL
        url = urlparse(location)
        if url.scheme in ["ftp", "http", "https"]:
            if self.debug:
                print("resolving web address in parent")
            istream = XmlResolver.resolveModule(self, txn, mgr, location, nameSpace)
        elif url.scheme == "":
            # File path relative to library module directory
            
            # Ensure that user does not use relative paths
            # to gain arbitrary access to the file system
            path = self.__relative_path__(location)
            istream = mgr.createLocalFileInputStream(
                        os.path.join(resources.names['library'], 'modules', path))
        elif url.scheme == "file":
            raise XmlException("file:// URLs are not supported for modules\n" +
                "Use module names relative to server's " +
                "%s directory"%(os.path.basename(self.librarydir)))
        else:
            raise XmlException("Unknown URL scheme %s in module location %s"%(url.scheme, location))
        return istream
    
        
    
