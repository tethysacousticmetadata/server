import io
import lxml
import typing
import re
import math

# Add on modules
import bsddb3.db
import dbxml
import cherrypy
import urllib.parse

# our modules
import collections
import pandas as pd
from REST.util import get_public_url

if typing.TYPE_CHECKING:
    from .XmlMethods import XmlMethods

DocumentExists = 19     # tried to add a document already in container


class DocumentError(dbxml.XmlException):
    """
    Thrown when a document cannot be validated.
    """

    # Regular expressions for parsing errors
    parser_re = re.compile(
        ".*at line (?P<line>\d+)(, *char *(?P<char>\d+))?.*" +
        "Parser message: (?P<message>.*)"
    )
    element_re = re.compile(
        "'(?P<element>[^']+)[^']*(?P<expected>[^']*)'" +
        ".*Document: (?P<document>[^)]*)\)"
    )

    # Number of XML context lines to show
    context = 7

    def __init__(self, e, title, xml, collection):
        """
        :param e:  Exception
        :param title:  Document name
        :param xml:  xml text that caused error
        :param collection: collection type
        """
        self.exceptionCode = e.getexceptionCode()
        if self.exceptionCode == dbxml.XML_PARSER_ERROR:
            # Unable to parse the document
            # Format the error message to be easier to understand
            lines = xml.split(sep='\n')
            m = self.parser_re.match(e.what)
            if m is not None:
                self.line = int(m.group('line'))
                self.char = int(m.group('char'))
                start = max(0, self.line-self.context)
                stop = min(self.line+self.context, len(lines))
                annotated_lines = []
                max_cols = 0
                for lnum in range(start, stop):
                    line = f'{lnum+1:6d}: {lines[lnum]}'
                    max_cols = max(max_cols, len(lines[lnum]))
                    annotated_lines.append(line)
                # Generate column guideline for top/bottom of XML segment
                guideline = []
                for idx in range(1, max_cols+1):
                    rem = idx % 5
                    if rem == 0:
                        if idx % 10 == 0:
                            guideline.append(f"{int(idx / 10 % 10):d}")
                        else:
                            guideline.append('+')
                    else:
                        guideline.append('-')
                guideline = "        " + "".join(guideline)
                annotated_lines.insert(0, guideline)
                annotated_lines.append(guideline)

                annotated = "\n".join(annotated_lines)

                # Find Web Client advanced query URL
                url = get_public_url()
                client_url = url._replace(path="/Client")
                log_url = url._replace(path=f"/ParseError/{title}",
                                       query="format=HTML")
                msglines = [
                    f"Unable to parse XML representation of submitted data:  {m.group('message')}",
                    f"Parser reports error line {self.line}, character {self.char}",
                    "",
                    "This represents the point at which the parser was unable to understand the input.",
                    "The actual error may be above this and the reported line number is commonly ",
                    "at the end of the element name that caused the error.  In many cases, looking at",
                    "the expected document format can help you to understand the issue.",
                    "",
                    "Messages such as element 'z' is not allowed for content model ('w','x?','y','z')",
                    "mean that the XML document should have had three or four elements with element x",
                    "being optional.  When the parser encountered z, it was expecting one of the earlier",
                    "elements.  For example, <w> some data </w> <z> some more data </z> would produce this error",
                    "as element y was expected.",
                    "",
                    "Examining the web client's advanced query editor may help you to understand",
                    f"what was expected:  {client_url.geturl()} (select Advanced Queries and ",
                    "the collection (type of data) to which you are submitting data.",
                    "",
                    "The following lines show the context of the error. "
                    "Note that the XML has been formatted to be more readable and while the ",
                    "error message will reflect the context of the XML that was submitted, the ",
                    "line numbers in the original document may be different than what is shown here.",
                    "A complete copy of the document that was submitted may retrieved by visiting:",
                    log_url.geturl(),
                    "Contact your local Tethys administrator or the Tethys maintainers if you need assistance.",
                    "",
                    ]
                msglines.append(annotated)
                self.what = "\n".join(msglines)
            else:
                # could not parse
                self.what = e.what
        else:
            self.what = e.what

        
        
    
class XmlContainer:
    "Interface to a collection of XML documents"
    
    @staticmethod
    # line below changed
    def create(methods, name,
               config: dbxml.XmlContainerConfig):
        """
        Return a handle to a newly created XmlContainer
        :param methods: XmlMethods interface manager
        :param name: name of container to open/create
        :param config: container configuration
        :return:  XmlContainer object
        """

        manager = methods.get_manager()
        # Determine whether or not to use transactions & if we should commit        
        [commit, txn] = methods.get_transaction(None)

        update_context = manager.createUpdateContext()
        if txn is None:
            low_level_container = manager.createContainer(name, config)
        else:
            low_level_container = manager.createContainer(txn, name, config)
            if commit:
                txn.commit()

        high_level_container = XmlContainer(methods, low_level_container)
        # Set up default indexing if any
        if name in Indices.default_indices:
            indices = Indices(high_level_container, methods)
            indices.add_defaults()  # Set up indices for this type of container
            indices.apply()  # Enable indexing

        return high_level_container
    
    @staticmethod
    def open(methods, name, config):
        manager = methods.get_manager()
        # Determine whether or not to use transactions & if we should commit        
        [commit, txn] = methods.get_transaction(None)
        "open an existing container & return a high level representation to it"
        if txn:
            low_level_container = manager.openContainer(txn, name, config)
        else:
            low_level_container = manager.openContainer(name, config)
        high_level_container = XmlContainer(methods, low_level_container)
        # Note the configuration used to open the container.
        # We will only need this if we have to reopen it later on
        high_level_container.opened_with_config = config
        if commit:
            txn.commit()
        return high_level_container
    
    def __init__(self, methods, container: dbxml.XmlContainer):
        """
        XmlContainer - Wrapper class for an opened Berkeley dbXML
        container.  In most cases, this constructor will not be called
        directly.  Use the static method XmlContainer.open to open containers
        instead of using this constructor which is called after the container
        has already been opened.
        :param methods:  XmlMethods instance, interface to dbXML
        :param container:  dbxml.XmlContainer instance
        """

        self.methods = methods
        self.manager = container.getManager()
        self.container_name = container.getName()
        self.container = container
        self.verbose = True
   
    def __del__(self):
        """del - Delete this container instance.  
        Closes container"""
        
        self.container.close()            

    def getConfig(self):
        """
        Retrieve the configuration of this container
        :return: XmlContainerConfig instance
        """
        return self.container.getContainerConfig()

    def getIndexSpecification(self):
        """
        Construct a new Index instance for this container
        :return:
        """
        return Indices(self, self.methods)

    def __addDocument__(self, title, body, flags=0, txn=None, meta=None):
        """__addDocument__ adds an XML document to the container
        Body must be a string or input stream on a URL or file.
        Title is the document title which if set to None will cause
        a title to be generated.
        Flags are passed directly to the the database.
        This method should not be called directly by the user.
        Use addString, addFile, or addURL instead.
        meta specifies metadata for the document via a dictionary.
            Each key specifies a namespace.
            Each value is a subdictionary:
                subkey - attribute name in associated namespace
                subvalue - attribute value.  Attribute values must either
                    be strings or XML data types (e.g. DateTime)
        
        example metadata: {
            'http://tethys.sdsu.edu/demo', {
                "author" : "marie",
                "added" : dbxml.XmlValue(dbxml.XmlValue.DATE_TIME, 
                            "2013-12-13T13:13:13Z")}
        }
        
        """
        # tell Berkeley DBXML to generate a name if one was not given
        if not title:
            title = ""
            flags = flags | dbxml.DBXML_GEN_NAME
        else:
            if title.find('*') > -1:
                raise ValueError('Document title "%s" contains *'%(title))
            
        MB = 1024.0*1024.0  # 1 MB
        # number of pages in documents
        pages = math.ceil(len(body)/self.container.getPageSize())

        upd_context = self.manager.createUpdateContext()

        [commit, txn] = self.methods.get_transaction(txn)
        docname = None

        MB = 1024.0 * 1024.0  # 1 MB
        self.methods.log(f"add document {title} to collection "
                         f"{self.container_name} ({len(body)/MB:.3f} MB, "
                         f"{pages} pages)")


        # Format the body so that we can provide intelligent feedback
        indented_body = self.pretty_print(body)
        try:
            if not txn is None:
                docname = self.container.putDocument(txn, title, indented_body, upd_context, flags)
            else:
                docname = self.container.putDocument(title, indented_body, upd_context, flags)
            self.methods.log(f"add document {title}: complete")
        except Exception as e:
            self.methods.log(f"put document {title}: FAILED")
            if commit:
                txn.abort()
                del txn
            error = DocumentError(e, title, indented_body, self.container_name)
            raise error

        # If the user specified metadata, we need to add it to the document 
        if meta is not None:
            document = None
            self.methods.log("Retrieve document in order to update metada")
            if not txn is None:
                document = self.container.getDocument(txn, docname)
            else:
                document = self.container.getDocument(docname)
            self.methods.log("Done retrieving Document")

            for ns, nsmeta in meta.items():
                for name, value in nsmeta.items():
                    if isinstance(value, str):
                        value = dbxml.XmlValue(dbxml.XmlValue.STRING, value) 
                    document.setMetaData(ns, name, value)
            self.methods.log("Attempting to update Document")
            if not txn is None:
                self.container.updateDocument(txn, document, upd_context)
            else:
                self.container.updateDocument(document, upd_context)
            self.methods.log("Done updating Document")

        if not txn is None and commit:
            txn.commit()
            self.methods.log("COMMITED")
        if self.verbose:
            print("done")
            
        del upd_context
        return docname
    
    def __updateDocument__(self, title, body, txn=None):
        
        [commit, txn] = self.methods.get_transaction(txn)
        
        # Retrieve the document and create an update context 
        document = self.getDocument(title, txn)
        document.setContent(body)
        upd_context = self.manager.createUpdateContext()

        # Format the body so that we can provide intelligent feedback
        indented_body = self.pretty_print(body)
        try:
            if not txn is None:
                self.container.updateDocument(txn, document, upd_context)
                if commit:
                    txn.commt()
            else:
                self.container.updateDocument(document, upd_context)
        except Exception as e:
            if commit:
                txn.abort()
                del txn
            error = DocumentError(e, title, indented_body, self.container_name)
            raise error
        finally:
            del upd_context
                        
    def updateString(self, title, body, txn=None):
        """updateString - replace the body (string) of an
        exsiting document called title"""
        
        return self.__updateDocument__(title, body, txn)
        
    def addString(self, title, body, flags = 0, txn=None, meta=None):
        """addString - document is in body as a string
        See __addDocument__ for details"""

        return self.__addDocument__(title, body, flags, txn, meta)

    def addFile(self, title, body_fname, flags = 0, txn=None, meta=None):
        """addFile - document is stored in file specified
        by body_fname.  
        See __addDocument__ for details"""

        f_stream = self.manager.createLocalFileInputStream(body_fname)
        return self.__addDocument__(title, f_stream, flags, meta)      

    def addURL(self, title, body_url, flags = 0, txn=None, meta=None):
        """addURL - document is stored at the URL specified
        by body_url.  
        See __addDocument__ for details"""

        url_stream = self.manager.createURLInputStream("", body_url, "")
        return self.__addDocument__(title, url_stream, flags, meta)
    
    def clear(self):
        """Remove all documents from the container
        if num of docs exceeds threshold, then do it incrementally"""
        
        threshold = 10
        
        if self.container.getNumDocuments() > threshold:
            #large container, delegating to incremental
            self.clear_incrementally()
        elif self.container.getNumDocuments()>0:
            #the usual
            
            # Allocate a transaction if needed, commit is a flag
            # that indicates whether or not we need to commit
            # in this subroutine or if a parent will take care of it
            [commit, txn] = self.methods.get_transaction(None)
            
            # Retrieve all documents
            if not txn is None:
                values = self.container.getAllDocuments(txn, dbxml.DBXML_LAZY_DOCS | dbxml.DB_READ_COMMITTED)             
            else:
                values = self.container.getAllDocuments(dbxml.DBXML_LAZY_DOCS)          
            
            # Iterate through the documents, deleting each one
            #commented out debug print lines for janitor worker
            updcontext = self.manager.createUpdateContext()
            #doc_count = 1
            while values.hasNext():      
                value = next(values)     # get the XmlValue
                doc = value.asDocument()  # convert it to an XmlDocument
                #print str(doc_count)+": Deleting %s"%(doc.getName())
                if not txn is None:
                    self.container.deleteDocument(txn, doc, updcontext)
                else:
                    self.container.deleteDocument(doc, updcontext)
                #doc_count=doc_count+1
                del value
            del values
                  
            if commit:
                #print "Comitting"
                txn.commit()
                del txn
            del updcontext
        return True
    
    def clear_incrementally(self):
        """Remove all documents from the container, $interval at a time
        If less than $threshold docs in the container, then will do it normally"""
        
        threshold = 10
        values = None #Store all docs here.
        result = False#default return value, so as not to remove source docs unless totally clear.
        if self.container.getNumDocuments() > threshold:
            # Allocate a transaction for first interval, commit is a flag
            # that indicates whether or not we need to commit
            # in this subroutine or if a parent will take care of it 
            [commit, txn] = self.methods.get_transaction(None)
            values = self.container.getAllDocuments(txn, dbxml.DBXML_LAZY_DOCS | dbxml.DB_READ_COMMITTED)          
            
            updcontext = self.manager.createUpdateContext()
            interval = 10 #how often to commit
            doc_count = 0 #counter for document
            comp_count = 0
            while values.hasNext():
                doc_count += 1
                value = next(values)     # get the XmlValue
                doc = value.asDocument()  # convert it to an XmlDocument
                
                #delete the document
                if not txn is None:
                    print(("Del attempt %d: %s" % (doc_count, doc.getName())))
                    self.container.deleteDocument(txn, doc, updcontext)
                else: #this should never execute..
                    self.container.deleteDocument(doc, updcontext)
                del value, doc #free up memory? make sure this doesn't crash stuff
                
                #check if its time to commit
                if doc_count % interval == 0 and commit:
                    del values #error if this isnt executed
                    comp_count += 1
                    print(("Commit attempt %d..." % comp_count))
                    txn.commit()
                    del txn
                    [commit, txn] = self.methods.get_transaction(None)
                    values = self.container.getAllDocuments(txn, dbxml.DBXML_LAZY_DOCS | dbxml.DB_READ_COMMITTED)
            #out of loop, reached end of XmlResults
            #check if any commits remaining
            del values
            if doc_count % interval > 0:
                txn.commit()
            del txn
            del updcontext
            result = True
            
        else:
            #if there are less than 100 documents, clear normally
            result = self.clear()
        return result

    def close(self):
        "Close the container"
        self.container.close()
        return True
        
    def getDocument(self, doc_id, txn=None):
        [commit, txn] = self.methods.get_transaction(txn)
        if txn is None:
            x = self.container.getDocument(doc_id)
        else:
            x = self.container.getDocument(txn, doc_id)
            if commit:
                txn.commit()
        return x
    def getDocByNameElement(self, doc_name, txn=None):
        [commit, txn] = self.methods.get_transaction(txn)
        if txn is None:
            x = self.container.getDocument(doc_name)
        else:
            all_docs = self.container.getContainerConfig()
            for doc in all_docs:
                name = pd.read_xml(doc)['Name'][0]
                if(name.lower() == doc_name.lower()):
                    x = doc
                    if commit:
                        txn.commit()
        return x;

        #     x = self.container.getDocument(txn, doc_id)
        #     if commit:
        #         txn.commit()
        # return x
    
    def getName(self):
        "Return container name"
        return self.container_name
    
    def getPageSize(self):
        "Show container's page size"
        return self.container.getPageSize()
        
    def getTransactional(self):
        return self.container.getFlags() & dbxml.DBXML_TRANSACTIONAL == \
            dbxml.DBXML_TRANSACTIONAL

    def pretty_print(self, body):
        """
        pretty_print - Given an XML document, format it nicely
        :param body: XML string
        :return: formatted XML string
        """
        lxml_parser = lxml.etree.XMLParser(remove_blank_text=False)
        body_h = io.BytesIO(bytes(body, encoding='utf8'))
        lxml_tree = lxml.etree.parse(body_h, lxml_parser)
        indented_body = lxml.etree.tounicode(lxml_tree, pretty_print=True)
        return indented_body

    def rmDocument(self, doc_id, txn=None):
        "Given a document id, remove it from the container"
        
        upd_context = self.manager.createUpdateContext()

        [commit, txn] = self.methods.get_transaction(txn)
        if not txn is None:
            self.container.deleteDocument(txn, doc_id, upd_context)
            if commit:
                txn.commit()
        else:
            self.container.deleteDocument(doc_id, upd_context)
            
        del upd_context
    

class Indices:
    """
    Support for custom indexing of containers
    
    General usage:
    Read the current index for an open container
    This stores the index associated with the container 
    indices = Indices(XmlContainer)
    
    Getters can return the underlying dbxml.XmlIndexSpecification
    or a string representation of the index
    
    clear_indices remove all index entries from the specification
    except ones that are mandated by the underlying database
    
    Indices can be modified by calling add_indices or default_indices.
    (See indices.default_indices to see what indices will be applied
    to specific containers.)  add_indices and default_indices
    only set a copy of the IndexSpecification, and do not update
    the container.

    To update the container's index specification with the current index
    set, call apply().  To reindex the container, call reindex().  It is
    generally assumed that containers will be reindexed after a new index
    specification is applied.

    Note that reindexing is slow and will close the underlying container
    during reindexing. We highly recommend making backups of the database
    prior to applying an index or reindexing a container.

    """

    tethys_ns = "http://tethys.sdsu.edu/schema/1.0"

    """
    Specifications for indices of documents.
    If document structure changes, we must change and reindex
    the container.
    
    Our indices are specified as a tuple IndexSpec(URI, [NodeIndex])
    The URI is the namespace for the index.
    Each NodeIndex is of the form:  NodeIndex(Name, index-specification)
    When Name starts with a @, it is considered to be an attribute.  The
    index is created without the @, but the @ lets us know that we should
    not be placing the index in the specified URI as attributes do not
    have namespaces.

    See Chapter 7, "Using BDB XML Indices"
    for details.

    Brief summary:
    Each element index is specified with a 4 or 5 part hyphenated string.
    [unique]-{path type}-{node type}-{key type}-{syntax type}

    unique - if present, values must be unique in the container
    path - node or edge (edge uses parent for context & is usually preferred)
    node - element: standard element
           attribute: element attribute
           metadata:  document metadata
    key - equality
          presence - Checks for node existing irregardless of value
          substring - search for parts of string (slows adding documents)
    """
    NodeIndex = collections.namedtuple("NodeIdx", ("node", "index"))
    IndexSpec = collections.namedtuple("IndexSpec", ("uri", "indices"))
    default_indices = {
        'Calibrations': IndexSpec(tethys_ns, [
            NodeIndex("Id", "edge-element-equality-string"),
            NodeIndex("Timestamp", "edge-element-equality-dateTime"),
            NodeIndex("Type", "edge-element-equality-string"),
            NodeIndex("Software", "edge-element-equality-string"),
            NodeIndex("Version", "edge-element-equality-string"),
            NodeIndex("Quality", "edge-element-equality-string")
        ]),

        'Detections': IndexSpec(tethys_ns, [
            NodeIndex("Id", "unique-edge-element-equality-string"),
            NodeIndex("EnsembleId", "edge-element-equality-string"),
            NodeIndex("DeploymentId", "edge-element-equality-string"),
            NodeIndex("UserId", "edge-element-equality-string"),
            NodeIndex("Detection", "edge-element-presence"),
            NodeIndex("Start", "edge-element-equality-dateTime"),
            NodeIndex("End", "edge-element-equality-dateTime"),
            NodeIndex("Event", "edge-element-equality-string"),
            NodeIndex("SpeciesId", "edge-element-equality-double"),
            NodeIndex("@Group", "edge-element-equality-string"),
            NodeIndex("Call", "edge-element-equality-string"),
            NodeIndex("Granularity", "edge-element-equality-string"),
            NodeIndex("@BinSize_m", "edge-attribute-equality-double"),
            NodeIndex("Subtype", "edge-element-equality-string"),
            NodeIndex("Score", "edge-element-equality-double"),
            NodeIndex("Confidence", "edge-element-equality-double"),
            NodeIndex("QualityAssurance", "edge-element-equality-string"),
            NodeIndex("Comment", "edge-element-equality-string")
        ]),

        'Deployments': IndexSpec(tethys_ns, [
            NodeIndex("Id", "unique-edge-element-equality-string"),
            NodeIndex("Project", "edge-element-equality-string"),
            NodeIndex("DeploymentNumber", "edge-element-equality-double"),
            NodeIndex("Site", "edge-element-equality-string"),
            NodeIndex("Cruise", "edge-element-equality-string"),
            NodeIndex("Platform", "edge-element-equality-string"),
            NodeIndex("Region", "edge-element-equality-string"),
            NodeIndex("Start", "edge-element-equality-dateTime"),
            NodeIndex("End", "edge-element-equality-dateTime"),
            NodeIndex("TimeStamp", "edge-element-equality-dateTime"),
            NodeIndex("SampleRate", "edge-element-equality-double"),
            NodeIndex("SampleBits", "edge-element-equality-double"),
            NodeIndex("RecordingDuration_m", "edge-element-equality-double"),
            NodeIndex("RecordingInterval_m", "edge-element-equality-double"),
            NodeIndex("Longitude", "edge-element-equality-double"),
            NodeIndex("Latitude", "edge-element-equality-double"),
            NodeIndex("DepthInstrument_m", "edge-element-equality-double"),
            NodeIndex("DepthBottom_m", "edge-element-equality-double"),
            NodeIndex("AudioTimeStamp", "edge-element-equality-dateTime"),
            NodeIndex("SensorId", "edge-element-equality-string"),
            NodeIndex("HydrophoneId", "edge-element-equality-string"),
            NodeIndex("PreampId", "edge-element-equality-string"),
        ]),

        'Ensembles': IndexSpec(tethys_ns, [
            NodeIndex("Id", "unique-edge-element-equality-string"),
            NodeIndex("UnitId", "edge-element-equality-double"),
            NodeIndex("DeploymentId", "edge-element-equality-string"),
        ]),

        'Localizations': IndexSpec(tethys_ns, [
            NodeIndex("Id", "unique-edge-element-equality-string"),
            NodeIndex("DeploymentId", "edge-element-equality-string"),
            NodeIndex("EnsembleId", "edge-element-equality-string"),
            NodeIndex("Software", "edge-element-equality-string"),
            NodeIndex("Version", "edge-element-equality-string"),
            NodeIndex("Time", "edge-element-equality-dateTime"),
            NodeIndex("Event", "edge-element-equality-string"),
        ]),

        'SpeciesAbbreviations': IndexSpec(tethys_ns, [
            # Name of abbreviation map
            NodeIndex("name", "edge-element-equality-string"),
            # Abbreviation used by user
            NodeIndex("coding", "edge-element-equality-string"),
            # Latin name
            NodeIndex("completename", "edge-element-equality-string"),
        ]),

        'ITIS': IndexSpec(tethys_ns, [
            NodeIndex("tsn", "unique-node-element-equality-double"),
            NodeIndex("completename", "unique-node-element-equality-string"),
            NodeIndex("vernacular", "node-element-substring-string")
        ]),

        'ITIS_ranks': IndexSpec(tethys_ns, [
            NodeIndex("tsn", "unique-node-element-equality-double"),
            NodeIndex("completename", "node-element-substring-string"),
            NodeIndex("name", "node-element-substring-string")
        ]),

        # Removed this index.  Wanted to enforce unique Mapping/Name
        # on SourceMaps.  Unfortunately, Attribute/Name is also used
        # and edge indexing does not support specifying only certain
        # parents.
        # 'SourceMaps': IndexSpec('', [
        #     NodeIndex("Name", "unique-edge-element-equality-string")
        #])
    }

    def __init__(self, container: XmlContainer, methods, debug=True):
        """
        :param container: XmlContainer
        :param methods: XmlMethods
        """

        self.container = container
        # We need the dbxml.XmlManager instance to perform reindexing.
        # There is a reference to this in the container, but in order to
        # reindex the container must be closed and this will release the
        # instance of the manager stored in the container.
        self.methods = methods
        self.manager = methods.manager
        self.debug = debug

        # get a transaction if we need one and retrieve the current index
        [commit, txn] = self.methods.get_transaction(None)
        if txn is None:
            self.index = container.container.getIndexSpecification()
        else:
            self.index = container.container.getIndexSpecification(txn, 0)
            if commit:
                txn.commit()

    @classmethod
    def create_default_index(cls, container_name) -> dbxml.XmlIndexSpecification:
        """
        Generate a specification instance suitable for use in container
        creation.  If a default specification is available, it will be used.
        Otherwise, automatic indexing will be turned on.
        :param container_name:
        :return: dbmxl.XmlIndexSpecification
        """

        index = dbxml.XmlIndexSpecification()
        if container_name in cls.default_indices:
            index.setAutoIndexing(False)
            defaults = cls.default_indices[container_name]
            cls.add_specification_to_index(index, defaults)
        else:
            index.setAutoIndexing(True)

        if True:
            print(index.get_indices_str())
        return index

    def get_indices(self, container: XmlContainer) -> dbxml.XmlIndexSpecification:
        """
        Return the set of indices associated with the container
        :param container:  XmlContainer abstraction
        :return: dbxml.XmlIndexSpecification:
        """
        return self.index

    def get_indices_str(self) -> str:
        """
        Return a string representation of an XML index specification
        :return:  string with index specifications
        """

        config = self.container.getConfig()
        name = self.container.getName()
        msg = []
        indexed = config.getIndexNodes() == dbxml.XmlContainerConfig.On
        msg.append(f"Container:  {name}")
        msg.append("Indices: XmlContainerConfig::" +
                   "On" if indexed else "Off")
        if indexed:
            msg.append(f"AutoIndexing: {self.index.getAutoIndexing()}")
            msg.append("Node, Index Specification, namespace")
            # Reset index specification iterator and list all indices
            self.index.reset()
            for spec in self.index:
                msg.append(
                    f"{spec.get_name()}, {spec.get_index()}, {spec.get_uri()}")

        msg = "\n".join(msg) + "\n"
        return msg

    def clear_indices(self, autoindex=False):
        """
        Remove all indices from the index specification except mandatory
        ones for the metadata
        :param autoindex:  If False, shuts off automatic indexing
        :return: None
        """

        # Don't understand this.  Running this block only deletes the string
        # indices (we don't even see the double indices in the print
        # statement.  If we reset the iterator and run it again we
        # get what we want.  Strangely, printing out the indices
        # shows everything...  kludge fix:  run multiple times
        maxN = 10
        changed = True
        idx = 0
        while idx < maxN and changed:
            changed = False
            self.index.reset()
            for old in self.index:
                if old.get_name() == 'name' and old.get_uri() == 'http://www.sleepycat.com/2002/dbxml':
                    continue  # built in index, cannot be removed
                # print(f"Removing {old.get_uri()}:{old.get_name()} {old.get_index()}")
                self.index.deleteIndex(old.get_uri(), old.get_name(),
                                    old.get_index())
                changed = True
            idx += 1

        if self.debug:
            print(f"Indices.clear_indices() required {idx+1} passes to remove all")
        self.index.setAutoIndexing(autoindex)

        return

    def add_defaults(self):
        """
        add_defaults - If the container associated with this index has
        defaults as defined in the Indices.default_indices dictionary,
        add them to the index.

        :return:  None

        CAVEATS:
        1. This method adds indices, call clear_indices first if
           you do not want the existing indices to remain
        2. Indices are not applied until container_update is called.
        3. This method does not actually change the indices on the container,
           merely the specification.  Call Indices.apply to apply the index set
           to the container with which the Index was created.
        """

        name = self.container.getName()
        if name in self.default_indices:
            specification = self.default_indices[name]
            # Do not use autoindexing if an index has been implemented
            self.index.setAutoIndexing(False)
            self.add_indices(specification)

    def add_indices(self, idxSet: IndexSpec):
        """
        Add the existing set of indices to an index specification

        :param idxSet: Indices.IndexSpec indicating set to be used.
           This is a named tuple that provides information on the
           namespace that will be used and the set of indices to
           be applied.
        :return: None

        CAVEATS:
        1. This method adds indices, call clear_indices first if
           you do not want the existing indices to remain
        2. Indices are not applied until container_update is called.
        3. This method does not actually change the indices on the container,
           merely the specification.  Call Indices.apply to apply the index set
           to the container with which the Index was created.
        """

        # Class the class method using the current dbxml.XmlIndexSpecification
        # stored in self.index
        self.add_specification_to_index(self.index, idxSet)

    @classmethod
    def add_specification_to_index(cls, index: dbxml.XmlIndexSpecification, idxSet: IndexSpec):

        # Iterate over specification set, adding indices
        uri = idxSet.uri
        specifications = idxSet.indices
        for s in specifications:
            # Attributes are not in namespaces
            if s.node.startswith("@"):
                attribute = s.node[1:]
                index.addIndex("", attribute, s.index)
            else:
                index.addIndex(uri, s.node, s.index)

    def report_indices(self):
        """
        Print current indices
        :return:
        """

        config = self.container.getConfig()
        name = self.container.getName()
        msg = []
        indexed = config.getIndexNodes() == dbxml.XmlContainerConfig.On
        msg.append(f"Container:  {name}")
        msg.append("Indices: XmlContainerConfig::" +
                   "On" if indexed else "Off")
        if indexed:
            # Report indices
            msg.append(self.get_indices_str())

        msg = "\n".join(msg)
        print(msg)

    def apply(self, txn=None):
        """
        Updates the associated container's index specification to the current
        one.  It is suggested to call reindex after applying.
        :return: None
        """

        [commit, txn] = self.methods.get_transaction(txn)
        update_context = self.manager.createUpdateContext()
        if txn is None:
            self.container.container.setIndexSpecification(self.index, update_context)
        else:
            self.container.container.setIndexSpecification(txn, self.index, update_context)
            if commit:
                txn.commit()

    def reindex(self):
        """
        reindex() - Reindex the container.
        Performs in-place modifiction of the container.
        DANGEROUS:  Backup before using

        :return:  None

        Concern:  It is unclear how the logging mechanism will deal with
        closing the container which does not require a transaction.  Containers
        must be closed before they are reindexed.  All the more reason to backup
        before reindexing...  The underlying dbxml Container class does not
        support transactions for its destructors.
        """

        # todo:  Add security
        if self.methods.perfmon_active():
            timer = self.methods.performance_monitor.start()

        # Create a default container configuration
        config = dbxml.XmlContainerConfig()

        # Create objects needed for reindexing
        update_context = self.manager.createUpdateContext()
        # create if needed and note that we will need to commit
        commit, txn = self.methods.get_transaction(None)

        # Container must be closed before we can reindex
        name = self.container.getName()  # get name so we can reopen
        # this line causes the manager to no longer be valid.
        del self.container.container  # close underlying container

        # reindex
        if txn is None:
            self.manager.reindexContainer(name, update_context, config)
        else:
            try:
                self.manager.reindexContainer(txn, name, update_context, config)
            except Exception as e:
                # Something went wrong, abort the transaction if we created it
                if commit:
                    txn.abort()
                raise  # propagate error up the chain

        if commit:
            # Commit the reindexing, and get another transaction to reopen
            # the container
            txn.commit()
            commit, txn = self.methods.get_transaction(None)

        # We've completed reindexing, reopen the container
        config = self.container.opened_with_config
        if txn is None:
            low_level_container = self.manager.openContainer(name, config)
        else:
            low_level_container = self.manager.openContainer(txn, name, config)
        self.container.container = low_level_container
        if commit:
            txn.commit()  # only commit method created the transaction

        if self.methods.perfmon_active():
            self.methods.performance_monitor.stop(timer, 'reindexed', name)

        return
