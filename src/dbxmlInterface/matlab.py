from xml.etree import cElementTree as ET
from collections import defaultdict
from scipy import io
import numpy as np
from pprint import pprint
import datetime
import maya
import re

def etree_to_dict(t):
    d = {t.tag: {} if t.attrib else None}
    children = list(t)
    if children:
        #dd = defaultdict(lambda: np.empty(shape=[0,0],dtype=np.object))
        dd = defaultdict(list)
        for dc in map(etree_to_dict, children):
            for k, v in list(dc.items()):
                #dd[k] = np.append(dd[k],np.array([v], dtype=np.object))
                dd[k].append(v)
        d = {t.tag: {k:v[0] if len(v) == 1 else v for k, v in list(dd.items())}}

    if t.attrib:
        d[t.tag].update(('@' + k, v) for k, v in list(t.attrib.items()))
    if t.text:
        text = t.text.strip()
        if children or t.attrib:
            if text:
              d[t.tag]['#text'] = text
        else:
            #print text
            #d[t.tag] = text
            try:
                d[t.tag] = np.array([float(text)])
            except:
                try:
                    #dt = datetime.datetime.strptime(text, "%Y-%m-%dT%H:%M:%SZ")
                    regex = r'^(-?(?:[1-9][0-9]*)?[0-9]{4})-(1[0-2]|0[1-9])-(3[01]|0[1-9]|[12][0-9])T(2[0-3]|[01][0-9]):([0-5][0-9]):([0-5][0-9])(\.[0-9]+)?(Z|[+-](?:2[0-3]|[01][0-9]):[0-5][0-9])?$'
                    match_iso8601 = re.compile(regex).match
                    if match_iso8601(text) is None:
                        raise
                    else:
                        dt = maya.when(text).datetime(naive=True)
                        mdn = dt + datetime.timedelta(days=366)
                        frac_seconds = (dt - datetime.datetime(dt.year, dt.month, dt.day, 0, 0, 0)).seconds / (24.0 * 60.0 * 60.0)
                        frac_microseconds = dt.microsecond / (24.0 * 60.0 * 60.0 * 1000000.0)
                        newTime = mdn.toordinal() + frac_seconds + frac_microseconds
                        d[t.tag] = np.array([newTime], dtype=np.object)
                except:
                    d[t.tag] = np.array([text], dtype=np.object)
    return d




if __name__ == "__main__":

    queryXML = """<ty:Result xmlns:ty="http://tethys.sdsu.edu/schema/1.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
    <out>
    <DeploymentDetails>
      <Longitude>239.200767</Longitude>
      <Latitude>34.318717</Latitude>
      <DepthInstrument_m>749</DepthInstrument_m>
      <TimeStamp>2008-02-13T00:00:00Z</TimeStamp>
      <AudioTimeStamp>2008-02-13T00:00:00Z</AudioTimeStamp>
      <ResponsibleParty>
        <organizationName>Scripps Whale Acoustics Lab</organizationName>
      </ResponsibleParty>
    </DeploymentDetails>
    <RecoveryDetails>
      <Longitude>239.200767</Longitude>
      <Latitude>34.318717</Latitude>
      <TimeStamp>2008-04-03T21:00:00Z</TimeStamp>
      <AudioTimeStamp>2008-04-03T21:00:00Z</AudioTimeStamp>
      <ResponsibleParty>
        <organizationName>Scripps Whale Acoustics Lab</organizationName>
      </ResponsibleParty>
    </RecoveryDetails>
    <Site>C</Site>
    <Site>extra</Site>
    <SpeciesId>Bm</SpeciesId>
  </out>
  <out>
    <DeploymentDetails>
      <Longitude>239.200767</Longitude>
      <Latitude>34.318717</Latitude>
      <DepthInstrument_m>749</DepthInstrument_m>
      <TimeStamp>2008-02-13T00:00:00Z</TimeStamp>
      <AudioTimeStamp>2008-02-13T00:00:00Z</AudioTimeStamp>
      <ResponsibleParty>
        <organizationName>Scripps Whale Acoustics Lab</organizationName>
      </ResponsibleParty>
    </DeploymentDetails>
    <RecoveryDetails>
      <Longitude>239.200767</Longitude>
      <Latitude>34.318717</Latitude>
      <TimeStamp>2008-04-03T21:00:00Z</TimeStamp>
      <AudioTimeStamp>2008-04-03T21:00:00Z</AudioTimeStamp>
      <ResponsibleParty>
        <organizationName>Scripps Whale Acoustics Lab</organizationName>
      </ResponsibleParty>
    </RecoveryDetails>
    <Site>C</Site>
    <SpeciesId>Bm</SpeciesId>
  </out>
  </ty:Result>"""

    myTree = ET.XML(queryXML)
    myDict = etree_to_dict(myTree)
    myDict["Result"] = myDict["{http://tethys.sdsu.edu/schema/1.0}Result"]
    del myDict["{http://tethys.sdsu.edu/schema/1.0}Result"]
    print()
    print()
    pprint(myDict)
    io.savemat("C:\\Users\\jcavner\\Documents\\foo4.mat",myDict)
    #mat = io.loadmat("C:\\Users\\jcavner\\Documents\\foo4.mat")
    #print
    #pprint(mat)

    #rec3 = np.array([(np.array((34), dtype='f8'), np.array((32), dtype='f8'), np.array(('M'), dtype='U1'),np.array(('SOCAL2'), dtype='U5'))],
    #                dtype=[('long', 'f8'), ('latitude', 'f8'), ('site', 'U1'), ('project', 'U5')])

    #rec2 = np.array([(np.array((389), dtype='f8'), np.array((312), dtype='f8'), np.array(('MM'), dtype='U1'),
    #                  np.array(None, dtype='U5'))],
    #                dtype=[('long', 'f8'), ('latitude', 'f8'), ('site', 'U1'), ('project', 'U5')])

    #io.savemat("C:\\Users\\jcavner\\Documents\\foobar.mat", {'records': np.array([rec2,rec3])})
    #datetime.datetime(2019, 1, 1, 0, 0)
    #dmatlab = d+datetime.timedelta(days=366)
    #dmatlab.toordinal()