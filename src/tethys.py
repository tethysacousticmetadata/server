# system modules
import argparse
import os
import sys
import pdb
import datetime
import site

import platform
if platform.system() == 'Windows':    
    import pythoncom

# user modules
import client_server
import resources
import dbxmlInterface.db 
import REST.Serve
import REST.Monitors
from version import get_version
       
def main():
    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawDescriptionHelpFormatter,
        description="""%prog - Tethys Acoustic Metadata Server
    Default values for choices are marked by an *""")
    
    timestr = '['+datetime.datetime.now().strftime("%d/%b/%Y:%H:%M:%S")+']'
    print(
        f"{timestr} Welcome to Tethys version {get_version()}. "
        f"Process id {os.getpid()} - Initializing...")
    client_server.add_server_opts(parser)
    options = parser.parse_args()
    
    if options.resourcedir:
        if not (os.path.isdir(options.resourcedir) and \
                os.access(options.resourcedir, os.W_OK | os.R_OK)):
            parser.exit(
                "Resource directory %s does not exist or is not accessible"%(
                options.resourcedir))
        resources.setbasedir(options.resourcedir)

    if options.database:
        dir = os.path.dirname(options.database)
        if dir != "":
            # User specified a directory, use it as specified
            resources.names['database'] = options.database
        else:
            # Filename only, use relative to resourcedir
            resources.names['database'] = \
                os.path.join(resources.basedir, options.database)

    if not os.path.isdir(resources.names['database']):
        raise ValueError(f"Resource directory {resources.basedir} does not "
                         f"have a database directory {resources.database}")
    # Some directories may not be present (e.g., the diskcache), create if needed
    resources.createdirsIfNeeded()

    # open the XML database
    xmlServer = dbxmlInterface.db.Server(resources.names["database"],
                                  options.transactional, options.recovery,
                                  options.querycache)

    ssl = {}
    if options.secure_socket_layer:
        ssl['Certificate'] = os.path.join(resources.basedir, "host.crt")
        ssl['PrivateKey'] = os.path.join(resources.basedir, "host.key")
        errors = []
        for k,n,v in [('Certificate', 'Certificate file', 'host.crt'),
                    ('PrivateKey', 'Private/public key file', 'host.key')]:
            ssl[k] = os.path.join(resources.basedir, v)            
            if not os.path.exists(ssl[k]):
                errors.append("%s %s must exist."%(n, ssl[k]))
        if len(errors) > 0:
            errors.append("See Using Secure Socket Layer with Tethys manual") 
            errors.append("\tfor creating certificates and keys.")
            raise RuntimeError("Unable to start secure socket layer:\n\t%s"%(
                "\n\t".join(errors)))

    startup_message = xmlServer.log_file_scrub_error
    if startup_message != '':
        startup_message += "\n"
    startup_message += f"Tethys {get_version()}: Server starting"
    REST.Serve.Server(xmlServer, options.port, ssl, startup_message)

if __name__ == '__main__':
    main()
    
