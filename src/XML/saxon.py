import saxonche  # Saxonica XML parser & tools

# The Python Saxon processor has been reported to have issues when
# more than one processor is defined.  Define it once here
saxon_proc = saxonche.PySaxonProcessor(license=False)

class XMLParserError(saxonche.PySaxonApiError):
    def __init__(self, msg):
        pass

class XSLT:
    def __init__(self):
        """
        Saxon XSLT transformer.
        Must invoked set_xml and set_stylesheet prior to using transform
        The xml and stylesheet operations are implemented as separate methods
        to allow reuse of a compiled stylesheet as well as fine-grained error
        handling
        """
        self.xslt_proc = saxon_proc.new_xslt30_processor()
        self.xml_doc = None
        self.compiled_xslt = None

    def set_xml(self, text):
        """
        Set the XML document to be transformed
        :param text:  String with XML
        :return: None
        """
        self.xml_doc = saxon_proc.parse_xml(xml_text=text)

    def set_stylesheet(self, text):
        """
        Set the stylesheet that will transform the XML
        :param text:  Valid XSLT stylesheet
        :return: None
        """
        self.compiled_xslt = self.xslt_proc.compile_stylesheet(stylesheet_text=text)

    def transform(self):
        """
        Apply the current stylesheet to the current XML document.
        Throws errors if either have not been set or if there are errors
        in the stylesheet.
        :return: transformed XML
        """
        if self.xml_doc is None:
            raise RuntimeError("XSLT: No valid document has been specified"
                               " to transform")
        if self.compiled_xslt is None:
            raise RuntimeError("XSLT: No valid transformation style sheet"
                               " has been specified")

        # Perform transformation
        xml = self.compiled_xslt.transform_to_string(xdm_node=self.xml_doc)
        return xml




