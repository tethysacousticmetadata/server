'''
transform.py
Created on Jul 21, 2013

Module for translating to valid XML
@author: mroch
'''

# Standard Python modules
import re
import string
from xml.sax.saxutils import escape  # do not remove, provided for other modules
import io
# Extensions
import lxml.etree

# bad XML tag characters
tag_replacechar_re = re.compile("[ ]")
tag_dropchar_re = re.compile("[%s]" % (string.punctuation.replace("_", "")))

def map2elem(name):
    "Translate a name to a legal XML element"
    elem = tag_replacechar_re.sub("_", name)
    elem = tag_dropchar_re.sub("", elem)
    return elem

#
# Extensible stylesheet language transformation to strip namespaces
# Source:  https://stackoverflow.com/questions/5268182/how-to-remove-namespaces-from-xml-using-xslt?rq=1
xslt_strip_ns = """<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:output indent="yes" method="xml" encoding="utf-8" omit-xml-declaration="yes"/>

    <!-- Stylesheet to remove all namespaces from a document -->
    <!-- NOTE: this will lead to attribute name clash, if an element contains
        two attributes with same local name but different namespace prefix -->
    <!-- Nodes that cannot have a namespace are copied as such -->

    <!-- template to copy elements -->
    <xsl:template match="*">
        <xsl:element name="{local-name()}">
            <xsl:apply-templates select="@* | node()"/>
        </xsl:element>
    </xsl:template>

    <!-- template to copy attributes -->
    <xsl:template match="@*">
        <xsl:attribute name="{local-name()}">
            <xsl:value-of select="."/>
        </xsl:attribute>
    </xsl:template>

    <!-- template to copy the rest of the nodes -->
    <xsl:template match="comment() | text() | processing-instruction()">
        <xsl:copy/>
    </xsl:template>

</xsl:stylesheet>
"""
def strip_namsepaces(doc, format=True):
    """
    Remove identical namespaces from a query
    :param doc:  XML document
    :param format:  Indent the document by nesting level if True
    :return: transformed document
    """
    doc_h = io.StringIO(doc)
    xslt_h = io.StringIO(xslt_strip_ns)

    # create document object models
    doc_dom = lxml.etree.parse(doc_h)
    xslt_dom = lxml.etree.parse(xslt_h)

    # create and apply XLST transform
    transform = lxml.etree.XSLT(xslt_dom)
    doc_new_dom = transform(doc_dom)

    xml = lxml.etree.tostring(doc_new_dom, pretty_print=format)
    xml = str(xml, 'utf-8')  # decode binary string
    return xml


# Strip off encoding string <?xml version="..." encoding="..."?>
# if present as already decoded.  Leaves string untouched if
# there is not an encoding string


def strip_xml_encoding_declaration(xml_doc):
    """
    Remove declaration for XML encoding from a document that has already been
    translated to unicode.
    :param xml_doc:
    :return:
    """

    # pattern for matching XML encoding strings.  Etree will not
    # parse strings that carry encoding information.
    preamble = r'(<\?xml version="[^"]+" +)encoding="[^"]+"([^>]*\?>)\n?'
    no_decl = re.sub(preamble, r'\1\2', xml_doc, count=1)

    return no_decl






