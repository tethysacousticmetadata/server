'''
Created on Aug 7, 2013

@author: mroch
'''

import io
import os
from xml.sax.saxutils import XMLGenerator, quoteattr, escape

# Local modules
from .indent import XMLIndentGenerator

class Sink:
    def __init__(self, indented=True):
        """XML generator with StringIO backing.
        Note that not all XML.Sink objects support the introduction
        of newline.  Currently, newline only works when indented is True.
        When newline is not supported (see supports_newline()), calling
        newline() on the sink's XML object is still allowed, but has no
        effect.
        Usage:
            s = Sink(indented=True)
            s.xml.startDocument()
            null_attrib = todo null attribute list
            s.xml.startElement("a", null_attrib)
            s.xml.newline()  # new line (no-op if unsupported - see above)
            s.xml.startElement("b", null_attrib)
            s.xml.characters("some text")
            s.xml.endElement("b")
            s.xml.newline()
            s.xml.endElement("a")
            
            # Add an element with text <c>oink</c>
            s.xml.elementText("c", "oink")
            
            s.xml.endDocument()
            print s.getstr() 
            
        Note:  If an interface requires an XMLGenerator, one can
        pass s.xml to it.           
        """
         
        self.string = io.StringIO()
        self.indented = indented
        if indented:
            self.xml = XMLIndentGenerator(self.string)
        else:
            self.xml = XMLGenerator(self.string)
            
        self.nlsupport = callable(getattr(self.xml, 'newline', None))
        if self.nlsupport == False:
            # add a dummy newline function
            self.xml.newline = lambda : None            
            
    def supports_newline(self):
        "Does this sink support newline() capability?" 
        return self.nlsupport
            
    def getstr(self):
        return self.string.getvalue()
    
    def elementText(self, name, string):
        "simple name:  add <name>string</name> to sink"
        self.xml.startElement(name)
        self.xml.characters(string)
        self.xml.endElement(name)
        
    def finalize(self):
        "Closes any open elements and returns a string."
        if not self.indented:
            # elements only tracked when indented
            raise RuntimeError("Unable to close off non-indented XML")
        else:
            self.xml.endElementAll()
        return self.string.getvalue()
        
            
    
class LogSink(Sink):
    
    def __init__(self, logfile):
        Sink.__init__(self, True) #indented
        self.string = logfile
        self.xml = XMLIndentGenerator(self.string)
                
    def getstr(self):
        self.string.seek(0)
        retval = self.string.read()
        # We should be at the end of the file already, but on Windows
        # a seek must occur before switching modes.
        self.string.seek(0, os.SEEK_END)
        return retval
    
    def finalize(self):
        "Closes any open elements and returns a string."
        if not self.indented:
            # elements only tracked when indented
            raise RuntimeError("Unable to close off non-indented XML")
        else:
            self.xml.endElementAll()
        return self.getstr()
        