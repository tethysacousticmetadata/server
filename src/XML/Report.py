import os

def DocumentSummary(sink, docs_added, docs_failed):
	"""DocumentSummary - Given a list of documents added and failed, write
	a summary to the specified XML sink which must support newline.
	"""
	
	sink.xml.startElement("SummaryReport", {})
	sink.xml.newline()
	if len(docs_added) > 0:
	    sink.xml.startElement("Success", {})
	    sink.xml.newline()
	    for d in docs_added:
	        sink.xml.startElement("Document", {})
	        sink.xml.characters(d)
	        sink.xml.endElement("Document")
	        sink.xml.newline()
	    sink.xml.endElement("Success")
	    sink.xml.newline()
	
	if len(docs_failed) > 0:
	    sink.xml.startElement("Failure", {})
	    sink.xml.newline()
	    for d in docs_failed:
	        sink.xml.startElement("Document", {})
	        sink.xml.characters(d)
	        sink.xml.endElement("Document")
	        sink.xml.newline()
	    sink.xml.endElement("Failure")
	    sink.xml.newline()
	sink.xml.endElement("SummaryReport")
	sink.xml.newline()


def log_report(sink, finished, working):
	sink.xml.startElement('BatchLogs', {})
	sink.xml.newline()
	sink.xml.startElement('InProgress', {})
	if len(working) > 0:
		sink.xml.newline()
		for log in working:
			sink.xml.startElement('Log', {})
			sink.xml.characters(log)
			sink.xml.endElement('Log')
			sink.xml.newline()
	else:
		sink.xml.characters('None')
	sink.xml.endElement('InProgress')
	sink.xml.newline()
	if len(finished)>0:
		sink.xml.startElement('Complete', {})
		sink.xml.newline()
		for log in finished:
			sink.xml.startElement('Log', {})
			sink.xml.characters(log)
			sink.xml.endElement('Log')
			sink.xml.newline()
	sink.xml.endElement('Complete')
	sink.xml.newline()
	
def rebuild_report(sink,logfile,op="Generic", 
				serverURL='http://SERVER:PORT/'):
	operation = op
	
	[root, ext] = os.path.splitext(logfile.name)
	basename = os.path.basename(root)
	fname = basename + ext
	batchlogs = serverURL + "BatchLogs"
	html = "?html=true"  # flag to produce HTML instead of XML
	sink.xml.comment('%s operation in Progress.\n\t'%(operation) + 
		"Operation status at ProgressURL\n\t" + 
		"Once complete, report at ReportURL\n\t" +
		"Use URLsForHumans to produce web browser display\n\t")
	sink.xml.newline(True)
	sink.xml.startElement('UpdateLog', {})
	sink.xml.newline()
	sink.xml.startElement('ProgressURL', {})
	sink.xml.characters(batchlogs)
	sink.xml.endElement('ProgressURL')
	sink.xml.newline()
	sink.xml.startElement('ReportURL', {})
	sink.xml.characters(batchlogs + '/' + fname)
	sink.xml.endElement('ReportURL')
	sink.xml.newline()
	sink.xml.startElement('URLsForHumans', {})
	sink.xml.newline()
	sink.xml.startElement('ProgressURL', {})
	sink.xml.characters(batchlogs + html)
	sink.xml.endElement('ProgressURL')
	sink.xml.newline()
	sink.xml.startElement('ReportURL', {})
	sink.xml.characters(batchlogs + '/' + fname + html)
	sink.xml.endElement('ReportURL')
	sink.xml.newline()	
	sink.xml.endElement('URLsForHumans')
	sink.finalize()
	
	