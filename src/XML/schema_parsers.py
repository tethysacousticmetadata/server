'''
Created on Jul 28, 2018

@author: mroch
'''
import csv
import io
import json
import xmljson
import xml
import math
import os
import re
import xml.etree.ElementTree as ET
import xml.dom.minidom
from collections import namedtuple, OrderedDict

from copy import deepcopy
# local modules
import resources
from util.pathdict import PathDict, PathValueDict


re_namespace_etree = re.compile("{(?P<ns>[^}]*)}(?P<name>.*)")
re_namespace_xml = re.compile("((?P<ns>[^:]+):)?(?P<name>.*)")

class DecoratedSchemaDict(PathValueDict):
    """
    DecoratedSchemaDict
    Ordered dictionary for parsing paths.  Similar to a PathValueDict with
    values of type ElementInfo and a few extra methods

    To access attributes, either get the element and look in the .attribute
    field, or place an @ in front of the attribute name.

    Examples to access the BinSize_m attribute of element Granularity:
        values = d['Detections/Effort/Kind/Granularity]
        attr = values.attribute["BinSize_m"]
    or
        attr = d['Detections/Effort/Kind/Granularity/@BinSize_m']

    """

    any_elem = "any"  # Special name for Schema extensions

    def __init__(self, other=(), /, **kwargs):
        super().__init__(other, **kwargs)

    def get_item_and_ancestors(self, path):
        """
        Returns element along path like __getitem__
        Also returns a list of paths that contain nodes that can appear more
        than once.  Most recent ancestor is last.

        :param path:
        :return: ElementInfo, path_list

        """

        found, node, ancestors = self.__access__(path, ancestors=True)
        if node is None:
            raise KeyError(f'Path "{path}" does not exist')
        else:
            return node.value, ancestors

    def __access__(self, path, ancestors=False):
        """
        Function for doing parsing.  Given / separated path, determines
        if the path exists and if so, its value

        :param path:  / separated path
        :param ancestors:  If True, return list of ancestor elements that can
            appear more than once.  Most recent ancestor is last
        :return:  True|False (key presence), node
        """

        # Create a list if we need to catch the ancestors
        ancestors = [] if ancestors else None

        path_list = path.split(self.separator)  # path to list
        attribute = path_list[-1].startswith('@')  # looking for an attribute?
        last_key_idx = len(path_list) - 1
        current = self

        found = True  # Assume true unless we learn otherwise
        for idx, key in enumerate(path_list):
            if idx == last_key_idx:
                # Made our way down to a base dictionary
                if attribute is True:
                    if idx == 0:
                        raise ValueError(
                            f"Path {path} specified an attribute without" +
                            "a parent element")
                    attr_name = key[1:]  # Strip @
                    node = node.value.attributes.get(attr_name, None, node=True)
                    if node is None:
                        found = False
                else:
                    # Normal element termination, get the associated value
                    node = super(PathDict, current).get(key, None)
                    if node:
                        value = node.value
                        if value is not None:
                            if ancestors is not None and value.max > 1:
                                # node can appear multiple times
                                ancestors.append(path)
                        else:
                            found = False
                    else:
                        found = False  # None
            else:
                if super(PathDict, current).__contains__(key):
                    # Access the next level of the dictionary
                    node = super(PathDict, current).__getitem__(key)
                    current = node.subdict
                    found = current is not None
                    if not found:
                        break  # Terminate loop if not found
                    if ancestors is not None and node.value.max > 1:
                        ancestors.append(self.separator.join(path_list[:idx+1]))
                    # Special handling for ##any elements
                    if len(current) == 1 and self.any_elem in current:
                        node = super(PathDict, current).__getitem__(
                            self.any_elem)
                        found = True
                        break
                else:
                    found = False
                    break

        if not found:
            node = None

        if ancestors is not None:
            return found, node, ancestors
        else:
            return found, node

    def item_pairs(self, filter=lambda x: x is not None, parent=""):
        """
        Return list of tuples of (path, value) listed in a breadth first order
        (shallowest paths first).

        :param filter:  Function that is applied to the value of each
          path.  The list of paths will only contain dictionary paths whose
          value cause filter to be True.  Any attributes associated with
          the path will also be returned
        :param parent:  Not settable by caller, used in recursion

        :return:  List of tuples. For each tuple
           tuple[0] - Path to a for loop control point
           tuple[1] - Entry associated with this control point
        """

        paths = []

        if len(parent) > 0:
            parent = parent + self.separator  # Append separator

        # Handle keys, then children
        children = []
        for key in self.keys():
            node = super(PathDict, self).__getitem__(key)
            # save key & associated PathValueDict
            children.append((key, node.subdict))
            # Determine if we should retain this path
            if filter(node.value):
                path = parent + key
                paths.append((path, node.value))
                # Append any attributes
                if node.value.attributes is not None:
                    for attr_name in node.value.attributes.keys():
                        attr_path = path + self.separator + f"@{attr_name}"
                        paths.append(
                            (attr_path, node.value.attributes[attr_name]))

        # Recurse over children
        for child in children:
            new_paths = child[1].item_pairs(filter, parent + child[0])
            paths.extend(new_paths)

        return paths
class DecoratedSchema:
    """
    DecoratedSchema
    Representation of schema containing information about type, whether elements
    can repeat, etc.

    This does not cover all cases for XML schema descriptions (XSD), but covers
    most common ones and those used in the Tethys metadata system
    """

    # Structures for representing the tree
    # ElementInfo provides a representation of an element in an OrderedDict
    # The name is the key, the value describes the number of times the element
    # can be used (min/max), its type, an OrderedDict of attributes, and a
    # documentation string if provided.  Children contains an OrderedDict of
    # the direct descendants of the node.
    element_info = namedtuple("ElementInfo",
                              ("min", "max", "type", "attributes", "doc"))

    # AttributeInfo are the values in the ElementInfo.attributes OrderedDict.
    # They describe the type and any documentation that was present.
    attr_info = namedtuple("AttributeInfo", ("type", "doc"))

    # TypeInfo
    # Description of types.  Usually type contains None (no values expected)
    # or a type such as xs:double, xs:dateTime etc.
    # When type is:
    #   list, info describes the type of the list items
    #   For enumerated types, type contains a list of the possible values
    type_info = namedtuple("TypeInfo", ("type", "info"))

    # Wildcard for extensions to Schema
    element_any = "any"
    namespace_specifier = "##"  # Used to specify namespaces on xs:any

    def __init__(self, schema_rep, debug=False):
        """
        :param schema_rep:  Badgerfish dictionary representation of a schema
        """

        self.schema = DecoratedSchemaDict()

        schema_node = schema_rep['schema']
        # Some schema only provide type definitions and no elements, we
        # do not process these other than to return an empty schema
        if 'element' in schema_node:
            min_occurs = 1  # root element must occur
            # Although the root element cannot be repeated, we mark the
            # maximum number of occurrences as infinite to represent that
            # multiple instance of the document can occur within an XML
            # document collection
            max_occurs = math.inf
            root_elem = schema_node['element']
            root_type = self.get_node_type(root_elem)
            root_attr = self.get_node_attributes(root_elem)
            root_doc = self.get_node_doc(root_elem)
            root_info = self.element_info(
                min_occurs, max_occurs,
                root_type, root_attr, root_doc)
            key = root_elem['@name']
            self.schema[key] = root_info

            # We build the DecoratedSchemaDict recursively and need
            # the node associated with the root.  By passing nodes
            # we do not need to track the paths as we descend.  They
            # are stored in the higher level nodes as we keep expanding
            # node.subdict.  node.value contains the ElementInfo.
            node = self.schema.get(key, node=True)
            self.process_children(root_elem, node)

            if debug:
                self.print_tree(self.schema)

    def print_tree(self, d, indent=0, indent_step=2):
        """
        Recursively display the DecoratedTree dictionary d in a textual
        representation

        :param d: dict containing tree, keys are element names, values are
           named tuples of type ElementInfo
        :param indent: Level of indent
        :param indent_step:  Indent tabs are indent_step characters each
        :return:
        """
        docN = 20 # Limit doc string to first N characters
        tab = ' ' * indent * indent_step  # number of spaces of indent
        for k, node in d.items():
            v = node.value
            # Limit doc string to first N characters
            doc_str = v.doc[0:docN] + "..." if (v.doc and len(v.doc) > docN) else v.doc

            print(f"{tab} {k}: min={v.min}, max={v.max} type={v.type} attrib={v.attributes} doc={doc_str}")
            if len(node.subdict) > 0:
                self.print_tree(node.subdict, indent+1)

    def process_children(self, elem, info):
        """
        Analyze children of a schema node

        :param elem:  Processed schema node (children have not yet been processed)
        :param info:  ElementInfo describing the node.  Contains information
           about node type, attributes, documentation, etc.  The field children
           should contain an instnace of a DecoratedSchema dict.  Any children
           of this node will be recursively analyzed and their information will
           be added to the dict.

        :return:  None
        """

        key_list = elem.keys()
        # We iterate over keys as opposed to hashing to the OrderedDict elem
        # to ensure that order is preserved
        for key in key_list:
            if key in ('complexType', 'complexContent', 'extension'):
                # complex is handled recursively
                self.process_children(elem[key], info)
            elif key in ('choice', 'sequence'):
                # Process list-like entities.  We treat choices as if
                # there were sequences.  Not actually correct, but in terms
                # of query design this is similar to a sequence.

                if isinstance(elem[key], list):
                    # In some cases, elem[key] is a list
                    for l in elem[key]:
                        self.process_sequence(l, elem, info)
                else:
                    self.process_sequence(elem[key], elem, info)

    def process_sequence(self, seqdict, parent, info):
        seq = []
        # Sequences are usually followed by an element or any
        # However, it is possible to nest a sequence within sequence,
        # this usually occurs when we are composing a group with other
        # elements.
        for key in seqdict.keys():
            if key in ("sequence", "choice"):
                # Add nested sequence to same info
                # On rare occasions, seqdict[key] will be a list.
                # For example, if we have a choice followed by multiple
                # sequences, these sequences will be put into a list.
                next_level = seqdict[key]
                if isinstance(next_level, list):
                    for item in next_level:
                        self.process_sequence(item, parent, info)
                else:
                    self.process_sequence(seqdict[key], parent, info)
            elif key == self.element_any:
                self.process_node(seqdict[self.element_any], parent, info)
            elif key == "element":
                elements = seqdict['element']  # We expect one or more nodes
                if not isinstance(elements, list):
                    elements = [elements]
                for element in elements:
                    self.process_node(element, parent, info)

    def process_node(self, s, elem, info):
        if "@name" in s:
            child_name = s.get('@name', None)
            if child_name is None:
                print("Missing @name unexpected")
        elif s.get('@namespace', "").startswith(self.namespace_specifier):
            child_name = self.element_any

        if "@maxOccurs" in s:
            # The maxOccurs attribute is present, verify it is a value
            # that can result in multiple copies of the element
            if s['@maxOccurs'] == "unbounded":
                max_occurs = math.inf
            else:
                max_occurs = int(s['@maxOccurs'])
        else:
            max_occurs = 1

        if "@minOccurs" in s:
            min_occurs = int(s["@minOccurs"])
        else:
            min_occurs = 1

        typename = self.get_node_type(s)
        # Mark special elements for downstream processing
        if child_name == "SpeciesId":
            if typename.info is None:
                # No dict was present, add one
                typename = self.type_info(typename.type, dict())
            # This is an ITIS node, it is stored as an integer, but
            # users may provide string encodings such as Latin species
            # names
            typename.info['special'] = 'ITIS'

        attributes = self.get_node_attributes(s)
        # Pull out the doc string
        doc = self.get_node_doc(s)

        info.subdict[child_name] = self.element_info(
            min_occurs, max_occurs, typename, attributes, doc)

        # Recursively process any children of nodes if needed
        if child_name != self.element_any:
            child_info = info.subdict.get(child_name, node=True)
            self.process_children(s, child_info)

    def get_node_doc(self, n):
        """
        Get documentation for node.  Looks for annotation/documentation/$
        Does not search any subnodes, e.g. this will not find a doc string
        buried in a type declaration
        :param n:  document node
        :return:
        """
        doc = n.get("annotation", None)
        if doc:
            if isinstance(doc, list):
                doc = doc[0]  # just take first one
            doc = doc.get("documentation", None)
            if doc:
                doc = doc.get("$", None)
        return doc

    def __getitem__(self, item):
        """
        Access a particular element of the schema.
        :param item:  Path
        :return: node, multiple_element_list
          node - ElementInfo for the element in question
          multiple_element_list  - Set of named paths to each ancestor
            (including self) where more than one element can occur.  Nearest
            ancestor appears last.
        """
        return self.schema[item]

    def get_node_attributes(self, n):
        """
        get_node_attributes - Recursively search for attributes.
        For now, we only handle attributes that are in the node, or embedded
        within simpleContent and extension
        :param n: node
        :return:
        """
        if "attribute" in n:
            # Process attributes.  Parse attributes from a list, pulling
            # out name, type, and documentation string (if any).
            # These are stored in a PathValueDict.  Attributes are not
            # recursive, so an OrderedDict would have sufficed, but this
            # eliminates some of the special handling for attributes as
            # they will be similar to the PathValueDict's used for processing
            # nodes
            attributes = PathValueDict()
            attr_list = n["attribute"]
            if not isinstance(attr_list, list):
                # Convert single dict to list to simplify processing
                attr_list = [attr_list]
            for attr in attr_list:
                if "@name" in attr:
                    attr_name = attr.get("@name", None)
                    attr_type = self.get_node_type(attr)
                    doc = attr.get("annotation/documentation/$", None)
                    if attr_name:
                        attributes[attr_name] = self.attr_info(attr_type, doc)
        else:
            for t in ("simpleContent", "complexType", "extension"):
                # These types can have attributes embedded further down...
                if t in n:
                    attributes = self.get_node_attributes(n[t])
                    if attributes is not None:
                        break  # found it
            else:
                attributes = None

        return attributes

    def get_node_type(self, n):
        """
        Given a node, determine its type
        :param n:
        :return: node_type information
        """

        if "enumeration" in n:
            # List of dictionaries with value restrictions
            values = n["enumeration"]
            enumeration = list()
            for vdict in values:
                value = vdict.get("value", None)
                if value:
                    enumeration.append(value)
        else:
            enumeration = None

        if "@type" in n:
            typename = self.type_info(n["@type"], None)
        elif 'restriction' in n:
            # Get base type of restricted values
            # We ignore any restrictions, those will be handled
            # by a real schema parser (xerxes)
            typename = self.get_node_type(n['restriction'])
        elif 'complexType' in n:
            typename = self.get_node_type(n['complexType'])
        elif 'simpleType' in n:
            typename = self.get_node_type(n['simpleType'])
        elif 'simpleContent' in n:
            typename = self.get_node_type(n['simpleContent'])
        elif 'extension' in n:
            typename = self.get_node_type(n['extension'])
        elif 'list' in n:
            # Get type of list items
            item_type = n['list']
            typename = self.type_info('list', item_type["@itemType"])
        elif '@itemType' in n:
            typename = self.type_info(n['@itemType'], None)
        elif '@base' in n:
            typename = self.type_info(n['@base'], None)
        elif n.get("@namespace", "").startswith(self.namespace_specifier):
            # xs:any won't have a name, but its namesapce will be specified
            typename = self.type_info(self.element_any, None)  # any valid XML
        else:
            typename = None

        # We will fall on our tushes if we have a list of enumerated values,
        # but we don't have this now and will avoid over-engineering.
        # Only store enumerated values when there is no other type helper
        # information
        if typename and not typename.info and enumeration:
            typename = self.type_info(typename.type, enumeration)

        return typename


class Schemata:

    # not all collections have a root element with the same name as
    # the schema, this lets us handle those
    collection2root_special_cases = {
        "Calibrations": "Calibration",
        "Deployments": "Deployment",
        "Events": "Event",
        "ITIS_ranks": "ranks",
        "Localization": "Localize",
        "SpeciesAbbreviations": "Abbreviations"}
    # invert the dictionary to enable finding collection names
    root2collection_special_cases = {
        v: k for k, v in collection2root_special_cases.items()}

    def __init__(self, strip_namespaces=True, debug_level=0):
        """Schemata
        Process the system's schemata
        Provides mechanisms for introspection and conversion to other
        formats (e.g. JSON)
        
        strip_namespaces - Boolean, if True (default) removes any namespace 
        labels which can clutter things up.  If multiple namespaces are 
        involved, this could be tricky in the case of collisions. 
        
        CAVEATS:  Multiple levels of includes are not handled
        """
        
        # Maps that will be constructed
        #
        # root2schemaname - Maps basename to expected top element of document
        # schema2file - Maps basename of schema file to full path
        # schemaname2root- Maps top element of schema to schema basename
        # schemata - Maps basename to ElementTree representation of schema
        #
        # Examples:
        # self.schemaname2root["Detections"] returns the name of the 
        #     file containing the detections schema without a full path
        # self.schema2file["Deployments.xsd"] returns the full path
        #    to the deployments schema in the currently running database
        # self.root2schema["Deployments.xsd"] returns the ElementTree
        #    representing the contents of the Deployments schema file
        #    and any schemata it includes.  All user-defined types and 
        #    groups will have been expanded.
         
        self.debug = debug_level # 0 none, 1 messages, 2 verbose
        self.strip_namespaces = strip_namespaces
        
        # Output converter
        self.to_badgerfish = xmljson.BadgerFish(xml_fromstring=False)
        
        # build dict of schema names and file names
        self.schema2file = resources.get_schemata()
        # These schema have issues that we are not handling yet
        # ITIS - circular types result in an infinite loop
        # tethys - multiple inlcude levels (could write code to handle, but no need)
        # jaxb - Used by JAXB schema compiler
        # We make  these comparisons in a case-independent manner,
        # some of the schemata should not be parsed and we ensure that
        # they are not parsed because somebody changed the case of a filename.
        schemata = [k for k in self.schema2file.keys()]
        schemata_lc = [k.lower() for k in schemata]
        for name in ['ITIS.xsd', 'tethys.xsd', 'jaxb.xsd']:
            lc_name = name.lower()
            try:
                idx = schemata_lc.index(lc_name)
                del self.schema2file[schemata[idx]]
            except ValueError:
                # Not in there, might have been removed on a previous call
                pass

        # root elements
        self.root2schemaname = dict()
        schemata_names = list(self.schema2file.keys())
        for k in schemata_names:
            # Remove extension, Handling  multiple . if present
            [basename, ext] = os.path.splitext(k)
            while ext != "":
                [basename, ext] = os.path.splitext(basename)
            # Handle root elements named differently than the filename
            if basename in self.collection2root_special_cases:
                basename = self.collection2root_special_cases[basename]
            self.root2schemaname[basename] = k
        # invert for map to root elements
        self.schemaname2root = dict(
            [[v,k] for k, v in list(self.root2schemaname.items())])

        # Create dictionary of schema names and definitions
        self.schemata = dict()
        for name, file in self.schema2file.items():
            # parse out the XSD, stripping namespaces
            iter = ET.iterparse(file)
            for _, el in iter:
                if self.strip_namespaces:
                    # Strip all namespaces
                    m = re_namespace_etree.match(el.tag)
                    if m.group("ns") != "xs":
                        el.tag = m.group("name")

            self.schemata[name] = iter.root     # store parsed XSD
        
        # expand includes       
        for name, xsd in self.schemata.items():
            if self.debug > 0:
                print(f"Parsing includes:  {name}")
            self._parse_includes(name, xsd)
        
        # expand type/group definitions
        for name, xsd in self.schemata.items():
            if self.debug > 0:
                print(f"Parsing types:  {name}")
            self._parse_types(name, xsd)
            
            # expansion of types/groups can cause nested sequences, fix
            self._collapse_nested_sequences(name, xsd)

        # Other representations of the schemata
        # JSON schema and decorated parse structure tree
        self.json = dict()
        self.tree = dict()
        for schema_file, xsd in self.schemata.items():
            # Schema files that are included in other schema might not have
            # a root element
            try:
                schema_key = self.schemaname2root[schema_file]
                root = self._get_schema_root_element(schema_key)
            except ValueError as e:
                if debug_level > 0:
                    print(f"{schema_file} does not have a root element"
                          " (not an error)")
                continue

            self.json[schema_key] = self._to_json(root)  # JSON dict
            self.tree[schema_key] = DecoratedSchema(self._to_json(xsd))

        # XML - less used, populate a dict cache as needed
        self.xml = dict()

    def schema_to_str(self, name, format="XML",
                      decorated=False,
                      typed_only=False):
        """
        Convert a schema to a string using the specified encoding format.

        :param name:  Root element of schema
        :param format: JSON/XML/text
           XML - Return XML specification of schema document
           JSON - Javascript object notation of schema document.
              By default, this is the Badgerfish convention conversion.
              See below for behavior when decorated is True
           CSV - Returns CSV list of path names and types.  Types are
              only provided for names in the schema that are bound to types
           HTML - HTML formatted list of path names that can be bound to values
        :param typed_only: If True (default False), only return elements
           that can be set to value.
           Only works when format in ("text", "html")
        :param decorated:  if True, return the decorated parse tree
           instead of a raw version of the format.  This representation
           is simpler to use and removes control structures that are not
           relevant to understanding the schema format.
           Only works when format == "JSON"
        :return:
        """

        name = name.capitalize()  # normalize the schema name
        format = format.lower()

        # Verify that we can find the collection and root element
        # Users are supposed to put in the root element, but we take
        # either the root or collection name as people make mistakes.
        if name not in self.root2schemaname.keys():
            if name in self.collection2root_special_cases.keys():
                collection = name
                name = self.collection2root_special_cases[name]
            else:
                raise ValueError(f"{name} is not a root element for a schema")
        else:
            if name in self.root2collection_special_cases:
                collection = self.root2collection_special_cases[name]
            else:
                collection = name  # collection has same name as root

        if format == "xml":
            if name in self.xml:
                result = self.xml[name]  # in cache
            else:
                # create and cache
                el = self._get_schema_root_element(name)
                result = xml.dom.minidom.parseString(
                    ET.tostring(el)).toprettyxml(indent="  ")
                self.xml[name] = result
        elif format == "json":
            # JSON formats are cached at object creation as they
            # tend to be used more often.
            if decorated:
                result = self.get_parse_tree(name, True)
            else:
                result = json.dumps(self.json[name], indent=1)
        else:
            if typed_only:
                filter = lambda x: x.type is not None
            else:
                filter = lambda x: x is not None

            if format in ("csv", "html"):
                # Retrieve paths that can be bound to values
                paths = self.tree[name].schema.item_paths(filter)
                rows = []
                for p in paths:
                    info = self.tree[name][p]
                    typename = info.type.type if info.type else ""
                    if typename == "list":
                        typename = f"{typename}({info.type.info})"
                    doc = info.doc if info.doc else ""
                    if isinstance(info, DecoratedSchema.attr_info):
                        min = 0
                        max = 1
                    else:
                        min = info.min
                        max = info.max
                    row = [p, min, max, typename, doc]
                    rows.append(row)

                result_io = io.StringIO()
                if format == "csv":
                    # Add header row
                    rows.insert(0, ['path', 'min occurs', 'max occurs',
                                    'type', 'description'])
                    csv_writer = csv.writer(result_io)
                    for row in rows:
                        csv_writer.writerow(row)
                elif format == "html":
                    result_io = io.StringIO()
                    result_io.write("<html>\n")
                    # CSS styling, color every other row
                    result_io.write(
"""
<head>
<style>
table {
  border-collapse: collapse;
  width: 100%;
}

th, td {
  text-align: left;
  padding: 8px;
}

tr:nth-child(even) {
  background-color: #D6EEEE;
}
</style>
</head>            
"""
                    )
                    result_io.write("<body>\n")
                    result_io.write('<h1> collection'
                        f'("{collection}") has root {name}</h1>\n')
                    result_io.write("<table>\n<tr>")
                    result_io.write(" <th> path </th>\n")
                    result_io.write(" <th> min occurs </th>\n")
                    result_io.write(" <th> max occurs </th>\n")
                    result_io.write(" <th>type</th>\n")
                    result_io.write(" <th>description</th>\n")
                    result_io.write(" </tr>\n")

                    # Generate a description for each item
                    for row in rows:
                        result_io.write(" <tr>\n")
                        for item in row:
                            result_io.write(f" <td>{item}</td>" + "\n")
                        result_io.write(" </tr>\n")
                    result_io.write("</table>\n")
                    result_io.write("</body></html>\n")

                else:
                    raise ValueError("Bad format")
                # Retrieve the HTML
                result = result_io.getvalue()

        return result


    def get_parse_tree(self, name, serialize=False):
        """
        Given a root element name, retrieve a decorated tree that indicates
        information about the schema such as valid children, type information,
        documentation strings and the number of times elements can be repeated.

        :param name: Name is the name of the schema
        :param serialize:  Convert to a string
        :return:
        """

        if name in self.tree:
            retval = self.tree[name]
        else:
            raise ValueError(f'"{name}" is not a top level schema element. '
                             f'Select from {", ".join(self.tree.keys())}')

        if serialize:
            # JSON module does not treat named tuples as dict, so no field
            # names appear on named tuples.  This is certainly fixable, but
            # not worth doing now.
            retval = json.dumps(retval.schema, indent=1)

        return retval

    def _get_schema_root_element(self, name):
        """
        Given the root element of a schema, return its
        ElementTree representation
        :param name:  Root element, e.g. Detections
        :return: DOM model of schema

        Raises a ValueError is the name does not exist
        """

        try:
            schemakey = self.root2schemaname[name]
        except KeyError:
            raise ValueError("Name %s is not a root element of a known schema"%(name))
        
        xsd = self.schemata[schemakey]    # Get the schema definition
        
        # Grab root element
        el = xsd.find('./element[@name="%s"]'%(name))
        if el is None:
            raise ValueError("Unable to find schema associated with " +
                               "root %s in schema %s"%(name, schemakey))
            
        return el
        
            
    def _to_json(self, xsd):
        "_to_json(xsd) - Convert element tree to JSON"
        
        # Use the badgerfish XML->JSON convention
        # https://badgerfish.ning.com/
        json = self.to_badgerfish.data(xsd)

        return json
        
    def _parse_includes(self, name, xsd):
        "Look for any includes and handle them"
        
        # Build map to access parents
        # Includes are usually near the top level, we only parse two levels
        parent_map = {}
        for p in xsd:
            parent_map[p] = xsd
            for c in p:
                parent_map[c] = p

        includes = xsd.findall("./include")  # Find top-level includes
    
        for i in includes:
            target = i.attrib['schemaLocation']  # name of schema to include
            
            target_xsd = self.schemata[target]  # Get the appropriate schema
            if target_xsd is None:
                raise ValueError("Bad target")

            # Replace the include with contents
            self._replace_item(i, parent_map[i], [n for n in target_xsd])
            
        
    
        pass
    
    def _replace_item(self, item, parent, replacement_list):
        """_replace_item.(item, parent, replacement_list)
        Given an Element and it's parent, replace the element
        with the items in replacement_list.                
        """
        
        idx = list(parent).index(item)    # Find item's position
        
        parent.remove(item)  # modify parent
        
        # Add replacements in reverse order
        for r in reversed(replacement_list):
            parent.insert(idx, r)
    
    @staticmethod
    def _ns_split(element):
        """_ns_split - Split an element into a namespace and name
        Returns list (ns, name) where ns is the namespace and name
        is the element name stripped of its namespace.
        This should not fail on well-formed element names.
        When no namespace is present, ns is None
        """
        
        # Parse out namespace, shouldn't fail if schema document is well formed                    
        m = re_namespace_xml.match(element)
        ns = m.group("ns")
        name = m.group("name")
        return (ns, name)

    @staticmethod        
    def tree_to_string(xsd):
        "tree_to_string(xsd) - pretty printed XML from xsd element tree"
        
        # conver to string representation of XML
        s = xml.dom.minidom.parseString(ET.tostring(xsd)).toprettyxml(indent = "  ")
        text = os.linesep.join([l for l in s.splitlines() if l.lstrip()])
        return text
        
    def _parse_types_helper(self, node, parent, name, xsd, processed):
        """_parse_types_helper(node, parent, name, xsd)
        Given a node in a schema (xsd) and its parent node,
        look for any user defined types that need to be expanded
        and replace them with their definitions
        
        name is the schema name and is only required for error processing
        processed is a dictionary containing keys that are types that
            have already been encountered.  These do not need to be processed
            subsequent times.
        """
        
        elem_name = node.tag   # What kind of element
        # Find the name of the element we are looking for and  
        # set the appropriate xpath expression to find it
        if elem_name == "extension":
            elem_type_ns = node.attrib.get("base")
            xpath = './%s[@name="%s"]'
        elif elem_name == "group":
            elem_type_ns = None
            # This is either a references to a group (ref) or a definition (name)
            if node.attrib.get("name") is not None:
                # This is a group node definition.  Process children
                for child in node:
                    self._parse_types_helper(child, parent, name, xsd, processed)
                return
            else:
                # group does not have a name, we are referencing a group defn
                elem_type_ns = node.attrib.get("ref")
                xpath = './%s[@name="%s"]'
        elif "type" in node.attrib:
            # Has a type been specified?            
            elem_type_ns = node.attrib.get("type")
            xpath = './%s[@name="%s"]'
        else:
            elem_type_ns = None  # no type associated with element

        # Ensure that we know the namespace and type
        if elem_type_ns is None:
            type_name = None
            type_ns = None
        else:
            # If we reached here, we are processing a type
            (type_ns, type_name) = self._ns_split(elem_type_ns) 
                                        
        # User-defined type that we need to process?
        # 
        # Might need to look at other types than xs namespace later
        user_type = not (type_name is None) and (type_ns is None or not type_ns in ["xs"])

        if user_type:
            # Choose the type name we'll search for (w/ or w/o namespace)
            searchfor = type_name if self.strip_namespaces else elem_type_ns
            
            # Locate the type to be resolved
            for record_type in ['complexType', 'simpleType', 'group']:
                target = xsd.find(xpath%(record_type, searchfor))
                if not target is None:
                    break  # found definition

            if target is None:
                print(("Unable to resolve type %s"%(searchfor)))
            else:
                # target may contain user-defined types
                # If we've never seen it before, process it
                if searchfor not in processed:
                    # This is the first time that we've seen this type definition
                    # As it may reference other types, we should process it 
                    # to expand any embedded types
                    processed[searchfor] = True
                    self._parse_types_helper(target, None, name, xsd, processed)
                    
                # update the node with the information from target
                if elem_name == "element":
                    # simply add the children of the target element to node
                    del node.attrib['type']  # type info is expanded
                    for c in target:
                        node.append(c)
                elif elem_name == "extension":
                    del node.attrib['base']  # type info is expanded
                    for c in target:
                        node.insert(0, c)
                elif elem_name == "group":
                    # Replace group node with the target's child
                    # We assume that the group only has a single child that
                    # that is an element, sequence, or choice, and we process
                    # the first one of those that we find
                    for c in target:
                        if c.tag in ['element', 'sequence', 'choice']:
                            # Copy the child into the group node and change 
                            # its type
                            node.clear()  # Get rid of children, attributes...
                            node.attrib = c.attrib
                            node.tag = c.tag
                            node.tail = c.tail
                            for subchild in c:
                                node.append(subchild)
                            pass
                            break
                    
                
        else:
            # no type or builtin, process children
            for c in node:
                self._parse_types_helper(c, node, name, xsd, processed)
                
        return            

    
    def _parse_types(self, name, xsd):
        """_parse_types(name, xsd)
        Replace any user-defined types/groups with their definitions
        """
        
        #root = xsd.find(self.schemaname2root(name))
        # Handle any group, complexType, simpleType
        self._parse_types_helper(xsd, None, name, xsd, {})
        
        if self.debug > 1:
            el = xsd.find('./%s[@name="%s"]'%(
                "element", self.schemaname2root[name]))
            print("^------------------------------------------------------")
            if el is None:
                print(("No root found for %s"%(name)))
            else:
                xml = self.tree_to_string(el)
                print(xml)
            print("v------------------------------------------------------")

            
            
            
        pass
    
    def _collapse_nested_sequences(self, schema_name, node, parentseq=None):
        """_collapse_nested_sequences(schema_name, node, parentseq)
        Expand any child of a sequence that is also a sequence that cannot
        be repeated multiple times. 
        <sequence> <a> <sequence> <b> <c> </sequence> <d> </sequence>
        to:  <sequence> <a> <b> <c> <d> </sequence>
        
        When node is not the root, parentseq should be set to the parent
        node if and only if the parent is a sequence.
        """

        children = [c for c in node]
        
        # Process the children in reverse order.
        # We may be expanding some of them so we want to ensure that
        # we have processed everything as we grow the list
        nodeisseq = node.tag == "sequence"
        nextparent = node if nodeisseq else None
        if children is not None:
            children.reverse()
            for c in children:
                self._collapse_nested_sequences(schema_name, c, nextparent)

        # Handle a sequence within a sequence that occurs at most 1 time
        try:
            maxoccurs = node.attrib['maxOccurs'] == 1
        except KeyError:
            maxoccurs = 99

        if (nodeisseq and parentseq is not None) and maxoccurs == 1:

            # Add children of this sequence to the parent 
            # and remove the node from the parent
            
            # Find position of node in the parent.  We should be guaranteed
            # to find something
            nodeat = 0
            found = False
            siblings = list(parentseq)
            while not found and nodeat < len(siblings):
                if siblings[nodeat] == node:
                    found = True
                else:
                    nodeat += 1
            if not found:
                raise ValueError('Error processing list of lists %s: %s -> %s'%(
                    schema_name, parentseq.tag, node.tag))
                
            # We now know the position.  Start inserting children
            # in reverse order (children list was reversed above)
            if children is not None:
                for c in children:
                    parentseq.insert(nodeat, c)
            # Remove the sequence, no longer needed
            parentseq.remove(node)        
