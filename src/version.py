
# Version must be a number optionally followed by text where the text
# indicates minor releases e.g., 3.2a or beta status
__version__ = "3.2 beta 2"

def get_version():
    """
    Get current version of Tethys server
    :return: current version string
    """
    return __version__
