import pyodbc
import re
import getpass
from collections import namedtuple
from util.pathdict import PathValueDict


from xml.sax.saxutils import XMLGenerator, quoteattr, escape
from xml.sax.xmlreader import AttributesNSImpl, AttributesImpl

import pandas as pd

import xmlpp

tsn_re = re.compile("^.*-(?P<tsn>[0-9]+)$")

def tagged_data(xmlsink, tag, attributes, data):
    """Output simple tagged data <tag> data string </tag>"""
    
    xmlsink.startElement(tag, attributes)
    try:
        xmlsink.characters(escape(str(data)))
    except UnicodeEncodeError as e:
        print(e)
    xmlsink.endElement(tag)


class GenOrder:
    """Generate XML from the ITIS database for specified taxa"""

    RankType = namedtuple("Rank",
                          ("tsn", "rank_name", "completename", "vernacular"))

    def __init__(self, db):
        """
        Generate taxonomic trees from an ITIS database
        Writes ITIS.xml which is a nested (hierarchical) form of ITIS
        and ITIS_ranks.xml which is a flattened form of ITIS.

        When a Latin name (completename in ITIS terminology) appears more
        than once (e.g. at different taxonomic levels), an _# is appended
        to the Latin name so that each taxonomic rank has a distinct
        completename.  The number (#) starts with 1 and increments each time
        the name is seen.

        :param db:  pyodbc Connection object (database handle)
        """

        self.taxadescend = taxadescend if taxadescend else []
        self.taxaascend = taxaascend if taxaascend else []

        # namespace
        self.attrib = AttributesNSImpl({}, {})

        # Retrieve order
        self.query = db.cursor()  # handle for queries

        taxon_unit_types = self.query.execute(
            '''select * from `taxon_unit_types`;''')
        records = taxon_unit_types.fetchall()
        names = [c[0] for c in taxon_unit_types.description]
        df = pd.DataFrame.from_records(data=records, columns=names)
        self.taxon_units = df.set_index('rank_id')

        # Some Latin completenames occur multiple times in ITIS
        # We only add the first taxonomic serial number that we
        # encounter as the Tethys ITIS schema is indexed on unique
        # completenames
        self.completename = {}

        # Retrieve hierarchies and construct PathValueDict
        self.hierarchy = PathValueDict()
        self.hierarchy.set_separator('-')
        result = self.query.execute("select * from hierarchy;")
        hierarchies = []
        for h in result:
            hierarchies.append(h)

        N = len(hierarchies)
        idx = 0
        for h in hierarchies:
            idx = idx + 1
            # if idx % 10000 == 0:
            #     print(f"{idx/N*100:.1f} processed")

            # Query the completename and any vernacular
            # Retrieve information about the rank
            item = self.query.execute(f'''
                select * from taxonomic_units, longnames, taxon_unit_types 
                where 
                   taxonomic_units.tsn = "{h.TSN}" and 
                   taxonomic_units.tsn = longnames.tsn and 
                   taxonomic_units.kingdom_id = taxon_unit_types.kingdom_id and
                   taxonomic_units.rank_id = taxon_unit_types.rank_id;
                ''').fetchone()

            completename = item.completename
            if item.completename not in self.completename:
                self.completename[completename] = [item.tsn]
            else:
                # Increment number of times we have seen this
                self.completename[completename].append(item.tsn)
                # completenames must be unique in Tethys, append instance #
                completename = \
                    f"{completename}_{len(self.completename[completename])-1}"

            # Retrieve vernacular
            vern = self.query.execute(f'''
                select * from vernaculars where vernaculars.tsn = "{h.TSN}";
                ''')

            # Collect entries for each language so that languages are always
            # in the same order
            vernacular = dict()
            for v in vern:
                if v.language != "unspecified":
                    try:
                        vernacular[v.language].append(v.vernacular_name)
                    except KeyError:
                        vernacular[v.language] = [v.vernacular_name]

            self.hierarchy[h.hierarchy_string] = self.RankType(
                item.tsn, item.rank_name,
                completename, vernacular)

        # Remove everything except animalia
        keys = list(self.hierarchy.keys())
        for k in keys:
            if k != '202423':
                del self.hierarchy[k]

        self.itis_flat()
        self.itis_nested()

    def itis_flat(self):
        """
        Generate a set of flattened ITIS entries.
        We ensure that the Latin completename is unique within this set.
        When completenames are encountered more than once, only the first
        such entry is used.
        :return:
        """
        itis_h = open("ITIS_ranks.xml", "wb")
        xml = XMLGenerator(itis_h, encoding='UTF-8', short_empty_elements=True)

        # Write header
        xml.startDocument()
        xml.startElement(
            "ranks",
            AttributesImpl({
                'xmlns:xsi': "http://www.w3.org/2001/XMLSchema-instance",
                'xmlns': "http://tethys.sdsu.edu/schema/1.0",
                'xsi:schemaLocation':
                    "http://tethys.sdsu.edu/schema/1.0 tethys.xsd"
            })
        )

        # Add in other entries
        # Includes non animalia (negative TSNs) and anything where the species
        # classification is in debate.  Invalid ITIS entries are not included
        # as they are removed from the hierarchy.  We may wish to have those
        # identifiers to show historical effort or in cases where the
        # classification is in contention (e.g. the ongoing genetics work of
        # Jefferson and Archer at NOAA SWFSC on Dephinus delphis)
        # Note that animalia entries will only be in the flattened data, not
        # in the hierarchical data.

        additional = [
            (-10, "Other phenomena",
             dict(English="Other", French="Autre", Spanish="Otro")),
            (555654, "Delphinus capensis",
             dict(English="Long-beaked Common Dolphin")),
            (612597, "Balaenoptera brydei",
             dict(English="Bryde's whale"))
            ]
        enclosing_element = "rank"
        for a in additional:
            xml.startElement(enclosing_element, self.attrib)
            tagged_data(xml, "tsn", self.attrib, str(a[0]))
            tagged_data(xml, "completename", self.attrib, a[1])
            xml.startElement("vernacular", self.attrib)
            for lang, value in a[2].items():
                attrib = AttributesImpl({'language': lang})
                tagged_data(xml, "name", attrib, value)
            xml.endElement("vernacular")
            xml.endElement(enclosing_element)
            xml.characters("\n")

        self.process_flat(self.hierarchy, xml)

        xml.endElement("ranks")
        xml.endDocument()

    def itis_nested(self):
        
        # Set XML stream
        itis_h = open("ITIS.xml", "wb")
        xml = XMLGenerator(itis_h, encoding='UTF-8', short_empty_elements=True)

        noattrib = AttributesNSImpl({}, {})

        # Write header
        xml.startDocument()
        xml.startElement(
            f"ITIS",
            AttributesImpl({
            'xmlns:xsi': "http://www.w3.org/2001/XMLSchema-instance",
            'xmlns': "http://tethys.sdsu.edu/schema/1.0",
            'xsi:schemaLocation': "http://tethys.sdsu.edu/schema/1.0 tethys.xsd"
            })
        )
        xml.characters("\n")

        # Add category for Other, e.g. physical; Use -tsns
        enclosing_element = "Kingdom"
        xml.startElement(enclosing_element, noattrib)
        tagged_data(xml, "tsn", noattrib, "-10")
        tagged_data(xml, "completename", noattrib, "Other phenomena")
        xml.startElement("vernacular", noattrib)
        for lang, value in {"English": "Other",
                            "French": "Autre",
                            "Spanish": "Otro"}.items():
            attrib = AttributesImpl({'language': lang})
            tagged_data(xml, "name", attrib, value)
        xml.endElement("vernacular")
        xml.endElement(enclosing_element)
        xml.characters("\n")

        # Process species
        self.process_hierarchical(self.hierarchy, xml)

        xml.endElement("ITIS")
        xml.endDocument()

    def write_entry_flat(self, taxon, xml):
        """
        Write a taxonomic entry
        :param taxon: Named tuple containing taxon information
        :param xml: xml generator
        :return: None
        """

        xml.startElement("rank", self.attrib)

        tagged_data(xml, "tsn", self.attrib, str(taxon.tsn))
        tagged_data(xml, 'taxon_unit', self.attrib, taxon.rank_name)
        tagged_data(xml, "completename", self.attrib, taxon.completename)

        xml.startElement("vernacular", self.attrib)
        lang_keys = list(taxon.vernacular.keys())
        lang_keys.sort()
        for lang in lang_keys:
            attrib = AttributesImpl({'language': lang})
            names = sorted(taxon.vernacular[lang])
            for n in names:
                tagged_data(xml, "name", attrib, n)
        xml.endElement("vernacular")

        xml.endElement("rank")
        xml.characters("\n")

        return

    def process_flat(self, level, xml):
        """
        Generate ITIS representation in tabular form, hierarchy information
        is not preserved with the exception of the taxonomic unit
        :param level: Current level in ITIS tree
        :param xml: xml generator
        :return:
        """

        # Iterate over the rank entries at this level
        tsns = list(level.keys())
        for tsn in tsns:
            taxon, children = level.get(tsn, node=True)
            if taxon:
                self.write_entry_flat(taxon, xml)

            # Process ranks children
            self.process_flat(children, xml)

        return

    def process_hierarchical(self, level, xml):
        """
        Generate the hierarchical (nested) version of the ITIS database
        :param level: Dictionary of rank entries under a given taxonomic unit
        :param xml: XML generator
        :return:
        """

        # Iterate over the rank entries at this level
        tsn_list = list(level.keys())
        for tsn in tsn_list:
            taxon, children = level.get(tsn, node=True)

            xml.startElement(taxon.rank_name, self.attrib)

            tagged_data(xml, "tsn", self.attrib, str(taxon.tsn))
            tagged_data(xml, "completename", self.attrib, taxon.completename)

            xml.startElement("vernacular", self.attrib)
            lang_keys = list(taxon.vernacular.keys())
            lang_keys.sort()
            for lang in lang_keys:
                attrib = AttributesImpl({'language': lang})
                names = sorted(taxon.vernacular[lang])
                for n in names:
                    tagged_data(xml, "name", attrib, n)
            xml.endElement("vernacular")
            xml.characters("\n")

            # Nest children
            self.process_hierarchical(children, xml)

            xml.endElement(taxon.rank_name)

    def __iter__(self):
        """
        Iterate through the taxa
        :return:
        """

        # Use a stack to implement a depth-first walk
        self.stack = []
        self.stack.append((None, self.hierarchy.keys()))

        while len(self.stack):
            # Retrieve current taxon
            path, keys = self.stack.pop()
            for key in keys:
                # Hierarchical path to key
                kpath = f"{path}-{key}" if path else key
                # Retrieve the taxon and next PathValueDict
                record, node = self.hierarchy.get(kpath, node=True)
                # Push children onto stack so that they are processed
                # on the next iteration.  Children are not ordered, but
                # we'll reverse the list so that they are processed in the
                # same order as they are retrieved
                children = list(node.keys())
                children.reverse()
                self.stack.append((kpath, children))
                # Return current record
                yield record

        raise StopIteration

    
database = "itis"
host = 'localhost'
print(f'ITIS collections built from "{database}" on "{host}"')
default_user = "root"
user = input(f'user (default {default_user}): ')
if user is None:
    user = default_user
password = getpass.getpass()

Connection = "DRIVER={MySQL ODBC 8.0 Unicode Driver};" + \
             f"Server={host};Port=3306;Database={database};" + \
             f"User={user};Password={password}"
print(f"Connecting to {database} database on {host}")
db = pyodbc.connect(Connection)
order = GenOrder(db)
