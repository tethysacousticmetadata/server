<?xml version="1.0" encoding="UTF-8"?>
<!--
 XSLT for placing XML document in implicit namespace and
 stripping off top-level declared namespace
 -->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output omit-xml-declaration="yes" />
    <xsl:template match="node()|@*">
        <xsl:copy>
            <xsl:apply-templates select="node()|@*" />
        </xsl:copy>
    </xsl:template>


    <!-- Just change the match="/*" to match="*" ; if you want to add namespace in all elements -->
    <xsl:template match="*">
        <xsl:element name="{local-name()}" namespace="http://tethys.sdsu.edu/schema/1.0">
            <xsl:apply-templates select="node()|@*" />
        </xsl:element>
    </xsl:template>
</xsl:stylesheet>