
import schema namespace ty="http://tethys.sdsu.edu/schema/1.0" at "tethys.xsd";

<ty:ranks xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:te="http://tethys.sdsu.edu/schema/1.0"> {
for $doc in collection("SpeciesAbbreviations")/ty:Abbreviations[Name="%s"]
     let $latin := $doc/Map/completename
     let $ranks := collection("ITIS_ranks")/ty:ranks/rank[completename = $latin]
return
   for $rank in $ranks,
	     $map in $doc/Map[completename = $rank/completename]
     return
		 <rank> {
	      $rank/tsn,
				$rank/completename,
        $map/coding,
				<Group>{data($map/completename/@Group)}</Group>
		}
		</rank>
}
</ty:ranks>
