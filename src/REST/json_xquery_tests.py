"""
Unit tests for translating JSON to XQuery
Constructing XQuery from JSON is somewhat complex, and these tests
are designed to regression test the JSON to XQuery translation when
changes are made.

They are not comprehensive, and should be added to over time.
We assume that the server is on the default port of the machine
the test code is running on (localhost).
"""
import re
import unittest
from dataclasses import dataclass
import difflib
import json
import sys

# site-packages
import requests

# Tethys packages

@dataclass
class JSON_XQ:
   json : str
   xq : str


class JSON2Xquery(unittest.TestCase):
    protocol = "http"
    port = 9779
    site = "localhost"
    xquery_resource = f"{protocol}://{site}:{port}/XQuery"

    tests = [
        # ----------------------------------------------------
        JSON_XQ(
            """
{
    "enclose": 1,
	"enclose_join": "YogiBear",
    "namespaces": 0,
    "return": [
        "Detections/Id",
        "Detections/Effort/Start",
        "Detections/Effort/End",
        "Detections/DataSource",
        "Detections/Effort/Kind",
		"Detections/OnEffort/Detection/Start",
        "Deployment/Site"
    ],
    "select": [
        {
            "op": "=",
            "operands": [
                "Deployment/Site",
                [
                    "M",
                    "N"
                ]
            ],
            "optype": "binary"
        }
		
    ],
    "species": {
        "query": {
            "op": "lib:completename2tsn",
            "operands": "%s",
            "optype": "function"
        },
        "return": {
            "op": "lib:tsn2completename",
            "operands": "%s",
            "optype": "function"
        }
    }
}            
            """,
            """import module namespace lib="http://tethys.sdsu.edu/XQueryFns" at "Tethys.xq";
declare default element namespace "http://tethys.sdsu.edu/schema/1.0";

<Result xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"> {
for $Deployment0 in collection("Deployments")/Deployment[Site = ('M', 'N')],
    $Detections0 in collection("Detections")/Detections[DataSource[DeploymentId = $Deployment0/Id]]
return
<YogiBear> {
  (
  <Deployment>{
    $Deployment0/Site
  }</Deployment>,
  <Detections>{
    $Detections0/Id,
    $Detections0/Effort/Start,
    $Detections0/Effort/End,
    $Detections0/DataSource[DeploymentId = $Deployment0/Id],
    $Detections0/Effort/Kind,
    $Detections0/OnEffort/Detection/Start
  }</Detections>
  )
} </YogiBear>
} </Result>"""
        ),
    # ----------------------------------------------------
    JSON_XQ(
        """{
    "enclose": 1,
    "namespaces": 0,
    "return": [
        "Detections/OnEffort/Detection/Start",
        "Detections/OnEffort/Detection/End",
        "Detections/DataSource"
    ],
    "select": [
        {
            "op": "=",
            "operands": [
                "Detections/Effort/Kind/SpeciesId",
                "Oo"
            ],
            "optype": "binary"
        },
        {
            "op": "=",
            "operands": [
                "Detections/OnEffort/Detection/SpeciesId",
                "Oo"
            ],
            "optype": "binary"
        },
        {
            "op": "exists",
            "operands": ["Detections/OnEffort/Detection"],
            "optype": "function"
        },
        {
            "op": "=",
            "operands": [
                "Deployment/Project",
                "Aleut"
            ],
            "optype": "binary"
        },
        {
            "op": "=",
            "operands": [
                "Deployment/DeploymentNumber",
                2
            ],
            "optype": "binary"
        }
    ],
    "species": {
        "query": {
            "op": "lib:abbrev2tsn",
            "operands": ["%s","SIO.SWAL.v1"],
            "optype": "function"
        },
        "return": {
            "op": "lib:tsn2completename",
            "operands": "%s",
            "optype": "function"
        }
    }
}""",
    """import module namespace lib="http://tethys.sdsu.edu/XQueryFns" at "Tethys.xq";
declare default element namespace "http://tethys.sdsu.edu/schema/1.0";

<Result xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"> {
for $Deployment0 in collection("Deployments")/Deployment[Project = "Aleut" and DeploymentNumber = 2],
    $Detections0 in collection("Detections")/Detections[DataSource[DeploymentId = $Deployment0/Id] and Effort[Kind[SpeciesId = 180469]] and OnEffort[exists(Detection[SpeciesId = 180469])]]
return
<Record> {
  <Detections>{
    $Detections0/DataSource,
          for $Detections2 in $Detections0/OnEffort/Detection[SpeciesId = 180469]
          return
            <Detection>{
              $Detections2/Start,
              $Detections2/End
            }</Detection>
  }</Detections>
} </Record>
} </Result>""")

    ]
    def test_JSON2Xquery(self):
        passed = True
        session = requests.session()
        hdrs = {"Accept-Encoding": "gzip"}
        re_standard_ws = re.compile("\s+")
        for idx, t in enumerate(self.tests):
            # message parameters
            params = {"JSON": t.json,  # JSON payload
                      "plan": 2,  # JSON -> XQuery
                      }
            result = session.post(self.xquery_resource, params, headers=hdrs)

            query_passed = result.status_code == requests.codes.ok
            if not query_passed:
                print(f"Query {idx} failed:  {result.reason} ({result.status_code}")
                print(t.json)
            else:
                expected = [re_standard_ws.sub(l, ' ')
                            for l in t.xq.split("\n")]
                received = [re_standard_ws.sub(l, ' ')
                            for l in result.text.split("\n")]

                cmp = difflib.context_diff(
                    expected, received,
                    fromfile="expected", tofile="computed"
                )
                diff_lines = [c for c in cmp]
                if len(diff_lines) > 0:
                    passed = False
                    print("Failure processing JSON Query:")
                    print(t.json)
                    print("XQuery diff")
                    for l in diff_lines:
                        print(l, file=sys.stderr)
                    sys.stderr.writelines(diff_lines)


        return passed








if __name__ == '__main__':
    unittest.main()
