'''
Created on Jul 9, 2013

@author: mroch
'''

# Standard library
import socket
import os
import urllib

# Add-on modules
import cherrypy

def copy_stream(outS, inS, chunkSize=8192):
    """"copy_stream - Copies a CherryPy file stream (inS) to an open low
        level file descriptor.
        Optional chunkSize indicates how many bites to read at a time"""

    data = inS.file.read(chunkSize)
    while data:
        os.write(outS, data)
        data = inS.file.read(chunkSize)

    return


def get_public_url() -> urllib.parse.ParseResult:
    """
    Retrieve a url object that describes the root of the server with
    a fully-qualified domain name.

    :return: urlparse ParseResult object

    Example usage:
    url = get_public_url()  # Retrieve server address
    # Add a path argument and retrieve the URL
    print(f"Visit {url._replace(path="Client").geturl()} for more information"

    Useful fields to modify with _replace:
    path, params, query, fragment
    """
    # Get URL of Tethys server
    url = urllib.parse.urlparse(cherrypy.server.description)
    # split out server & port
    netloc = url.netloc.split(":")
    # replace server name with fully qualified domain name
    netloc[0] = socket.getfqdn()
    url = url._replace(netloc=":".join(netloc))

    return url