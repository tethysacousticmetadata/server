'''
Created on Dec 22, 2014

@author: mroch
'''

#HTTP response codes
codes = {
             # System has received request, processing (WebDAV)
             "Processing": 102,
             "OK": 200,  # Success
             # Request has been filled and new resource is created
             "Created": 201,
             # Request accepted but not completed (may not complete)
             "Accepted": 202,
             # Processed successfully, nothing to return (DELETE)
             "No Content": 204,
             "Reset Content": 205,
             "Bad Request": 400,
             "Unauthorized": 401,  # Authentication required
             "Payment Required": 402,  # RESERVED for future use
             "Forbidden": 403,
             "Not Found": 404,
             # Resource does not support request, eg, PUT on read only resource
             "Method Not Allowed": 405,
             "Internal Server Error": 500,
             "Not Implemented": 501,
             "Service Unavailable": 503,  # down for maintenance or other reason
             }
        