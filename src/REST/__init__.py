
# Default configuration for Cherry Py REST server
import os
import cherrypy

def getWebServicesDir():
    """
    :return:  Directory where static web content is located
    """

    # Get path to Tethys root
    # Assume structure:  root/src/module/__init__.py - this module
    tethys_root = __file__
    for idx in range(3):
        tethys_root = os.path.split(tethys_root)[0]
    # tethys_root/dirname - location of static web content
    assets_dir = os.path.join(tethys_root, "WebGUI")
    return assets_dir

# Cherrypy configuration variables
config = {
    'global': {
        'server.thread_pool': 30,  # Thread pool size (sys def = 10)
        'server.socket_host': '0.0.0.0', # Current host
        'server.socket_port': 8000,
        'server.max_request_body_size' : 400 * (1024 ** 2),  # N megabytes
        'engine.autoreload_on': False,  # Restart when source files changed?
        'tools.gzip.on': True,     # Support gzip compression
        'log.screen': True,
    },
    '/': {
        'request.dispatch': cherrypy.dispatch.MethodDispatcher(),
        # Let user see tracebacks.  Very useful for debugging, but 
        # not typically considered good form as it exposes information
        # to users that could be exploited 
        'request.show_tracebacks': True,
        'response.timeout': 1080,  # timeout duration (18 min)
        # enable session tracking, we won't make this persistent across
        # reboots for now, but it is possible with a filesession backend
        "tools.sessions.on": True,
    },
    # static serving not turned on as creating issues
    # '/static': {
    #     "tools.staticdir.on": True,
    #     "tools.staticdir.debug": True,
    #     "tools.staticdir.root": getWebServicesDir(),
    #     "tools.staticdir.dir": "",
    # }
}
