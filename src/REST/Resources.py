# -*- coding: utf-8 -*-

# System modules
from collections import namedtuple, OrderedDict
import mimetypes
import os
from io import BytesIO, StringIO
import platform
import re
import threading
import traceback
import uuid
import datetime
from enum import Enum
from functools import cache
import hashlib
import json
import math
import html
import zipfile
from queue import Queue
from threading import Thread
# from xml.etree import cElementTree as ET  # ended up using lxml

# Add-on modules
import cherrypy
import dbxml
from scipy import io
import lxml.etree as ET
import numpy as np
import pandas as pd
import maya
import pyodbc
import tempfile
import xmljson

import REST
import XML.schema_parsers

from urllib.parse import urlparse

# System specific module
if platform.system() == "Windows":
    import pythoncom

# Our modules
import XML.schema_parsers
import exception_handling
from collections import defaultdict
from .Response import codes
from . import util
import mediators.ODBC
import REST.simple_query_lang as smpllang
from XML import Report
from XML.sink import Sink, LogSink
from XML.schema_parsers import DecoratedSchemaDict
from XML.transform import strip_xml_encoding_declaration
from XML.saxon import XSLT
from util.pathdict import PathValueDict
from dbxmlInterface.error import QueryError, XmlQueryEvaluationErrorExtended
from mediators.r import RInterface
import dbxmlInterface.error
from dbxmlInterface.XmlAbstractions import Indices
from dbxmlInterface.error import DBInternalError
import resources
from benchmark import PerformanceMonitor, Timer
import version

pd.options.mode.chained_assignment = None

# pattern for pulling out server URL from a request
# might cause problems later if we start doing load balancing and a load
# balancer rewrites the URL, unfamiliar with load balancing web server
# architecture
server_re = re.compile("(?P<url>https?://[^/]+/)")
semaphores = {}


class Counter:
    """
    Counter - Returns integers each time next is called.
    Iterating over this will result in an infinite loop.
    Expected usage:
    counter = Counter()
    for some loop:
       next_val = next(counter)
    """
    def __init__(self):
        self.count = -1  # first value will be zero

    def __iter__(self):
        return self

    def __next__(self):
        """
        next()
        :return: integer that is incremented on each next()
        """
        self.count += 1
        return self.count


class XQueryJSON:
    """
    Generate XQuery from JSON specification.
    Method generate_Xquery is the main entry point for users wishing
    to construct XQueries from JSON specifications.  The strategy used
    in generate_Xquery is to first create a FLWOR data structure
    (generate_flwor) and then convert the FLWOR to XQuery.  We rely
    on knowing information about the schema to know where loops are required.

    Class used to construct XQueries from a JSON dictionary.  The user provides
    a JSON structure that specifies the following fields:
    
    select - A list of objects specifying how elements are selected.  Each
        list entry is a JSON object that describes a selection criteria.  It
        contains the following fields:
        op - An XQuery operator, e.g. "="
        operands - A list of operands to be used with the operator.  Currently,
           we assume that the first operand is a path to an element and use
           this element to determine the XQuery structure, e.g., when we
           need to construct loops.
        optype - Operator type, how the operators are joined.  Valid strings:
           "infix", "binary" - Both result in infix notation between the op and
               operands, e.g. a == b
           "function" - We assume the operator is a function and wrap it
               around the operands, separated by commas.
               e.g. function_name(a, b)
               
    return - A list of path values to be returned.  In the future, we may
       support operators, e.g. count, but these are not currently supported.
       
    species - single Java object that describes how species names are encoded
       in the selection criteria.  It has a format like the selection critieria.
       For example, to use a species abbreviation map, one would use:
           {
            "op": "lib:abbrev2tsn",
            "optype": "function",
            "args": ["%s", "SIO.SWAL.v1"]
           }

    nested - If True, loops produce results that are nested in their enclosing
       element.  Note that in cases where one returns the element on which
       one is looping, this will cause a double nesting and is not recommend.
       
    Several data structures are used in transforming JSON to XQuery.
    
    parse - A dictionary with one entry per collection type.  It is keyed
    by the root element name of the collection.  Each value is a 
    DecoratedSchemaDict, a path dictionary that allows one to look up
    information about schema elements such as their type, number of children,
    etc.
    
    query - A per collection dictionary keyed by the root element of the
    collection.  Each collection has a PathValueDict that allows whose entries
    are tuples of nodetype.
        selection - List of criteria for selecting the node based
            on criteria such value ranges, etc.
        projection - list of children that should be returned for this node
        ancestors - List of nodes leading from this node up to the document
            root where > 1 child is possible.  Each of these might have a loop
            variable associated with it.
        loop_idx - Variable number if the XQuery will loop over this path
            (-1 otherwise)
        loop_depth - Number of elements in the path.  Used to ensure proper
            ordering.
            
    The XQuery is constructed from a set of FLWORTYPE tuples that represent
    the XQuery.  FLWORTYPEs are named tuples with fields for For, Let,
    Order, and Return.  Subqueries can be nested in the Return field.

    We build up the FLOWRTYPE expression using information
    from the schemata (represented in the queries structure) and the
    JSON specificaition.
    """

    nodetype = namedtuple("NodeType",
                          ("selection",
                           "projection",
                           "ancestors",
                           "loop_idx",
                           "loop_depth"))
    # Representation of a FLOWR expression.  We capitalize
    # all fields to avoid collisions with Python reserved words
    flwortype = namedtuple("FlowrType",
                           ("For",
                            "Let",
                            "Where",
                            "Order",
                            "Return"))

    # EncloseType permits a set of Return values to be enclosed within an
    # element, e.g.
    #     return <Detection> {
    #         Start, End
    #     } </Detections>
    # would be represented as enclosetype("Detection", ["Start", "End"])
    enclosetype = namedtuple("EncloseType",
                             ("Element", "Return"))

    # ForType is used for specifying for values.  Each tuple consists
    # of a loop variable and a path over which we iterate.  The Path
    # is frequently constructed incrementally as a list.
    fortype = namedtuple("ForType", ('Variable', 'Path'))

    tab = 2   # Number of characters per indent level in XQuery

    # regular expression for matching variable names, e.g. $name3
    var_re = re.compile(r"\$(?P<var>[a-zA-Z]+)\d+")

    # regular expression for matching a path
    # Must start the path or be the first argument in a function specification
    # e.g. Detection/Start, fn(Detection/SpeciesId, xyzzy)
    path_re = re.compile(
        r"[A-Za-z][A-Za-z0-9]*\(([A-Za-z_0-9/]+)|([A-Za-z_0-9/]+)")

    def __init__(self, dbserver, debug=False):
        self.dbserver = dbserver
        self.schemata = XML.schema_parsers.Schemata()
        # Set up decorated parse trees of schemata
        self.parse = self.schemata.tree
        self.debug = debug

        # Specify what constrains should be added automatically when joining
        # from multiple collections.
        # The order of the key pairs is (inner var, outer var)
        self.join_constraints = dict()
        self.join_constraints[('Deployment', 'Detections')] = [dict(
            # XPath selection expression details
            op="=",
            optype="binary",
            operands=["Deployment/Id", "$Detections0/DataSource/DeploymentId"],
            type=XML.schema_parsers.DecoratedSchema.type_info(
                "variable", {})
            )]
        self.join_constraints[('Detections', 'Deployment')] = [dict(
            op="=",
            optype="binary",
            operands=["Detections/DataSource/DeploymentId", "$Deployment0/Id"],
            type=XML.schema_parsers.DecoratedSchema.type_info(
                "variable", {})
            )]
        self.join_constraints[("Detections", "Localize")] = [dict(
            op="=",
            optype="binary",
            operands=["Detections/Id",
                      "$Localize0/Effort/ReferencedDocuments/Document/Id"],
            type=XML.schema_parsers.DecoratedSchema.type_info(
                "variable", {})
            ), dict(
            op="=",
            optype="binary",
            operands=["Detections", "$Localize0/Effort/ReferencedDocuments"],
            type=XML.schema_parsers.DecoratedSchema.type_info(
                "variable", {})
            )]
        self.join_constraints[("Localize", "Deployment")] = [dict(
            op="=",
            optype="binary",
            operands=["Localizations/DataSource/DeploymentId",
                      "$Deployment0/Id"],
            type=XML.schema_parsers.DecoratedSchema.type_info(
                "variable", {})
            )]
        self.join_constraints[("Localize", "Ensemble")] = [dict(
            op="=",
            optype="binary",
            operands=["Localizations/DataSource/EnsembleName", "$Ensemble0/Id"],
            type=XML.schema_parsers.DecoratedSchema.type_info(
                "variable", {})
            )]
        self.join_constraints[("Ensemble", "Localize")] = [dict(
            op="=",
            optype="binary",
            operands=["Ensemble/Id", "$Localize0/DataSource/EnsembleName"],
            type=XML.schema_parsers.DecoratedSchema.type_info(
                "variable", {})
            )]
        # When a query results in joins between multiple collections,
        # we wrap each iteration in the following element (may be changed
        # in generate_Xquery)
        self.join_element_default = "Record"

    def generate_Xquery(self, jdict, enclose, debug=False):
        """
        generate_XQuery - Construct an XQuery from a JSON specification
        :param jdict:  JSON dictionary
        :param enclose: Boolean.  If true, generate enclosing elements for
          structures that repeat in the schema (e.g. that we loop over).
          For example, if the user is returning elements related to
          Detections/Effort, all efforts related to the same set of
          detections would have an enclosing element.  The class variable
          enclosetype determines the name of the enclosing element and as
          of this writing is Return.
        :param debug: debug information
        :return: XQuery
        """

        # Extract information used to build query
        # We expect selection and return to contain lists, make them
        # lists if they are not given as such
        species_translation = jdict.get('species', dict())
        selection = jdict.get('select', [])
        if not isinstance(selection, list):
            selection = [selection]
        # Verify selection operands are a list
        for s in selection:
            if "operands" in s and not isinstance(s['operands'], list):
                s['operands'] = [s['operands']]  # make a list
        # Verify projection operands are a list
        project = jdict.get('return', [])
        if not isinstance(project, list):
            project = [project]
        # Nest each item of for loop in element self.enclosetype?
        nested = jdict.get('nested', False)
        # operators are expected to have these key/values
        op_schema = ('operands', 'op', 'optype')
        if species_translation:
            for subkey in ("query", "return"):
                schema = species_translation.get(subkey, None)
                if schema:
                    # Verify expected keys are present
                    if set(op_schema) != set(schema.keys()):
                        # f-string use {{ for { literal
                        raise ValueError(
                            f"JSON species:{subkey} schema should have keys: " +
                            f"{{{', '.join(op_schema)}}}, but has " +
                            f"{{{', '.join(schema.keys())}}}"
                        )
                    # Ensure operands are a list
                    if not isinstance(schema['operands'], list):
                        schema['operands'] = [schema['operands']]
                else:
                    species_translation[subkey] = None  # no such key

        # See if users want their joins enclosed in a particular element
        # Defaults to Record if not present, and is not used if user
        # specifies omit.
        self.join_element = jdict.get("enclose_join", self.join_element_default)
        if self.join_element.lower() == "omit":
            self.join_element = None

        # Find all top-level elements so that we know what schema we need
        # Each top-level element will be associated with a specific collection
        # of documents

        # Build list of selection paths and determine top-level elements
        sel_paths = []
        if len(selection) > 0:
            for s in selection:
                sel_paths.append(s['operands'][0])
            top_level = [s.split("/")[0] for s in sel_paths]
        else:
            top_level = []
        # Add any top level elements that are projected (returned) from the XML
        if len(project) > 0:
            top_level.extend([p.split('/')[0] for p in project])
        # Create a list of unique top-level elements.  We use dict.fromkeys
        # which will give us uniqueness while preserving order
        # (Python 3.7+ required)
        top_level_dict = dict.fromkeys(top_level)
        top_level = list(top_level_dict.keys())

        if len(top_level) <= 0:
            raise ValueError(
                "Unable to identify root elements in JSON select/return")

        # Perform limited query optimization
        # For the most part, we rely on the underlying query processor to
        # optimize queries, but the order of loops can have large impacts
        # on joins that are not well-handled by the query optimizer.
        # The loop order is based on the order in which the select criteria
        # list appear.
        if "Deployment" in top_level:
            idx = top_level.index("Deployment")
            if idx > 0:
                # Move deployment to head of list if heuristic suggests
                # deployment criteria is tight
                if "Deployment/Site" in sel_paths or \
                        "Deployment/DeploymentNumber" in sel_paths:
                    top_level.insert(0, top_level.pop(idx))

        # Retrieve schemata parse trees used in select/return
        parse = dict()
        query = dict()

        # Dictionaries for assignment variable names
        counters = dict()
        counters['variables'] = Counter()

        # Values in query will be PathValueDict objects.  Any nodes that are
        # created implicitly due to path traversal should default to having
        # empty lists for the selection and projection criteria.
        def interior_node_factory():
            # Default: empty select, project, ancestor lists,
            # no loop variable, and leaf depth marker
            return self.nodetype(list(), list(), list(), -1, -1)

        for t in top_level:
            # Create a path dictionary that supports values for each node
            # and specify how to create default values for paths
            query[t] = PathValueDict()
            query[t].set_default_interior_value_fn(interior_node_factory)
            counters[t] = Counter()  # for creating unique variables
            # Add the top level node.  These nodes will result in loops
            # regardless of whether or not there are selection criteria as
            # the loop is needed to select documents within the document
            # collection
            query[t][t] = self.nodetype(
                [], list(),  # selection and projection criteria
                list(),  # ancestor list
                next(counters[t]),  # variable number
                0  # root element, always depth 0
            )

        # Use the parse trees to determine control points for looping through
        # the XML documents.  We construct a query dictionary for each
        # schema that we are using.  The nodes with values contain a list of
        # elements that are used as selection criteria and another list
        # for values that are to be returned associated with subtrees that
        # meet the selection criteria.

        # Populate the selection criteria
        # Any place with selection criteria will result in a loop, assign
        # integers to each one of these
        for criterion in selection:
            # First operand must be the path to the element of interest
            # (We could relax this and allow the path to be in multiple
            # positions or even have multiple paths, but this would
            # require adding something to mark which operands are paths)
            sel_path = criterion["operands"][0]
            nodes = sel_path.split("/")
            root = nodes[0]
            try:
                info, ancestors = self.parse[root].schema.get_item_and_ancestors(sel_path)
            except KeyError:
                raise ValueError(f"Selection path criterion {sel_path} does not exist")
            most_recent_ancestor = ancestors[-1]
            # Augment the selection criteria with type information
            criterion['type'] = info.type
            # Add the tuple to the selection entries for this ancestor
            if most_recent_ancestor in query[root]:
                # This selection path has been seen before.
                # Query the entry and add the new criterion
                entry = query[root][most_recent_ancestor]
                entry.selection.append(criterion)
                if len(entry.ancestors) == 0:
                    # This node has no ancestor information, it was generated
                    # implicitly by another path.  Implicit nodes do not
                    # have any ancestor information, provide it here
                    entry.ancestors.extend(ancestors)
                if entry.loop_idx == -1:
                    # We've encountered this node before, but not as a loop
                    # variable.  Create a mutated copy of the NodeType tuple
                    # with a loop index variable index and the depth
                    entry = self.nodetype(
                        *entry[:-2], next(counters[root]), len(ancestors)-1)
                    query[root][most_recent_ancestor] = entry
            else:
                # First time we see this path, generate a node that
                # records the selection criterion, that we have no projection
                # criteria, the ancestors where for loops might be created,
                # a variable counter and the depth of the loop with respect
                # to elements that can occur more than once.
                query[root][most_recent_ancestor] = \
                    self.nodetype([criterion], [], ancestors,
                                  next(counters[root]), len(ancestors)-1)
            # Selection criteria are also added to the parent nodes
            # This results in running tests multiple times, but ensures that
            # we do not traverse sections of documents (or documents themselves)
            # the do not meet the selection criteria.
            for ancestor in ancestors[:-1]:
                query[root][ancestor].selection.append(criterion)

        # Populate values to be returned (projected)
        for proj_path in project:
            nodes = proj_path.split('/')
            root = nodes[0]
            if root not in self.parse:
                raise ValueError(
                    f"Path {proj_path}, root element {root} is not a root "
                    "element for any of the schemata.\n"
                    f"Expected:  {', '.join(self.parse.keys())}")
            info, ancestors = \
                self.parse[root].schema.get_item_and_ancestors(proj_path)
            # Find the youngest ancestor (for loop control point)
            # that will be associated with a loop and add this value
            # to the list to be returned
            found = False
            while not found and len(ancestors) > 0:
                current = ancestors.pop()
                if current in query[root]:
                    entry = query[root][current]
                    if entry.loop_idx != -1:
                        # Make path relative to the loop node and store it
                        rel_path = proj_path.replace(current+"/", "")
                        entry.projection.append(rel_path)
                        found = True

            if not found:
                # We should not ever get here
                # Grab new copy of ancestors list as we destroyed it.
                _, ancestors = \
                    self.parse[root].schema.get_item_and_ancestors(proj_path)
                raise ValueError(
                    f"Unable to parse return value {proj_path}. ",
                    f"No match in ancestors: {ancestors}. "
                    "Contact maintainers to resolve this issue.")

        if debug:
            for t in top_level:
                info = query[t]
                pairs = info.item_pairs()
                for pair in pairs:
                    print(pair[0], pair[1])

        # Retrieve lists of control points and associated selection/projection
        # information
        if len(query) > 1:
            # Add in join constraints
            for outer in range(len(top_level)):
                for inner in range(outer+1, len(top_level)):
                    inner_outer = (top_level[inner], top_level[outer])
                    constraints = self.join_constraints[inner_outer]
                    for constraint in constraints:
                        # Get the schema tree for the inner loop, add constraint
                        root = top_level[inner]
                        schema_tree = query[root]
                        schema_tree[root].selection.insert(0, constraint)

        if 'query' in species_translation:
            # If user specified an abbreviation map, retrieve it and populate
            # the query structure so that we don't have to look up individual
            # species codes
            q = species_translation['query']
            if q is None or q.get('op', "") != "lib:abbrev2tsn":
                q = {'abbrev_map': None}
            else:
                # User planned on looking up abbreviations, grab the map
                operands = q.get("operands", [])
                if len(operands) < 2:
                    raise ValueError(f"Attempt to use {q.get('op')} without"
                                     "specifying species abbreviation map")
                name = operands[1]
                abbr_map = self.dbserver.methods.get_abbreviations_map(name)
                q['abbrev_map'] = abbr_map

        # Generate the query -----
        flwor = self.generate_flwor(
            query, top_level, species_translation, counters, nested, enclose)

        if self.debug:
            flwor_str = self.flwor_to_str(flwor)
            print(flwor_str)

        join_el = None
        if flwor.For is not None and len(flwor.For) > 1:
            join_el = self.join_element
        query = self.flwor_to_XQuery(flwor, indent=0, enclose_return=join_el)
        xquery = "\n".join([
            'import module namespace lib="http://tethys.sdsu.edu/XQueryFns" at "Tethys.xq";',
            'declare default element namespace "http://tethys.sdsu.edu/schema/1.0";',
            '',
            '<Result xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"> {',
            query,
            '} </Result>'
        ])
        return xquery

    def flwor_to_str(self, flwor, indent=0):
        """
        Given a FLWOR expression, convert it to an indented string.
        Useful for debugging
        :param flwor:  FLWORType instance
        :param indent: level of indentation
        :return:
        """
        values = []
        indent_str = " " * indent*self.tab
        for_values = flwor.For
        if not isinstance(for_values, list):
            for_values = [for_values]
        for f in for_values:
            if isinstance(f, self.flwortype):
                values.append(self.flwor_to_str(f))
            else:
                values.append(f"{indent_str}{f}")

        for field in ("Let", "Where", "Order"):
            field_val = getattr(flwor, field)
            if not (field_val is None or (isinstance(field_val, list) and len(field_val) == 0)):
                values.append(f"{indent_str}{field}: {field_val}")

        returns = flwor.Return
        if len(returns) > 0:
            values.append(f"{indent_str}Return")
            retindent = " "*(indent+1)*self.tab
            for r in returns:
                if isinstance(r, self.flwortype):
                    values.append(self.flwor_to_str(r, indent+1))
                elif isinstance(r, self.enclosetype):
                    values.append(f"{retindent}<{r.Element}>")
                    for value in r.Return:
                        if isinstance(value, self.flwortype):
                            values.append(self.flwor_to_str(value, indent+1))
                        else:
                            values.append(f"{retindent}{value}")
                    values.append(f"{retindent}</{r.Element}>")
                else:
                    values.append(f"{retindent}{r}")

        return "\n".join(values)

    def flwor_to_XQuery(self, flwor, indent=0, enclose_return=None):
        """
        flwor_to_XQuery
        Convert a FLWOR structure to an XQuery, may be called recursively
        :param flwor: for, let, where, order, return tuple
        :param indent:  indentation level
        :param enclose_return:  return clause should be embedded within
           the specified name
        :return:
        """

        indent_str = " "*indent*self.tab
        xquery = []
        join_str = ",\n    " + indent_str

        # We process the FLWOR returns first.  If there is nothing to return
        # we do not need the FLWOR
        returns = self.flwor_to_XQuery_process_returns(flwor.Return, indent + 1)
        returns = [r for r in returns if r]  # filter out any empty strings

        if len(returns) > 0:
            # Something to return, construct the FLWOR

            if len(flwor.For) > 0:
                joins = []
                syntax = "for "  # Start of for loops
                for f in flwor.For:
                    x_path = "".join(f.Path)
                    joins.append(
                        f"{indent_str}{syntax}{f.Variable} in {x_path}")
                    syntax = ""  # Subsequent clauses do not need for
                xquery.append(join_str.join(joins))
            if len(flwor.Let) > 0:
                xquery.append(indent_str + "let " +
                              f",\n{indent_str}    ".join(flwor.Let))
            if len(flwor.Order) > 0:
                raise ValueError("order not implemented")

            # Generate return statement if needed
            if len(flwor.For) > 0 or len(flwor.Let) > 0:
                xquery.append(f"{indent_str}return")

            if len(returns):
                ret_indent = indent_str + " "*self.tab
                if enclose_return:
                    xquery.append(f"{indent_str}<{enclose_return}> {{")
                if len(returns) > 1:  # Surround multiple items in sequences ( )
                    xquery.append(f"{ret_indent}(")
                    xquery.append(",\n".join(returns))
                    xquery.append(f"{ret_indent})")
                else:
                    xquery.extend(returns)
                if enclose_return is not None:
                        xquery.append(f"{indent_str}}} </{enclose_return}>")

            retval = "\n".join(xquery)
        else:
            retval = ""

        return retval

    def flwor_to_XQuery_process_returns(self, return_list, indent=0):
        """
        Process return values in the construction of an XQuery expression
        :param return_list: List of values to return
        :param indent:  indent level
        :return:
        """
        indent_str = " " * indent*self.tab

        returns = []
        for r in return_list:
            if isinstance(r, str):
                # Normal case, return a value represented by a path
                returns.append(f"{indent_str}{r}")
            elif isinstance(r, self.flwortype):
                # Nested query, a FLWOR is in the return value
                if r.For is not None and len(r.For) > 1:
                    enclose_return = self.join_element
                else:
                    enclose_return = None
                nesting = self.flwor_to_XQuery(r, indent + 1, enclose_return)
                if len(nesting):
                    returns.append(nesting)
            elif isinstance(r, self.enclosetype):
                # We enclose a set of return values in an element
                closure_returns = self.flwor_to_XQuery_process_returns(r.Return, indent + 1)
                if len(closure_returns) > 0:
                    closure = []
                    closure.append(f"{indent_str}<{r.Element}>" + "{")
                    closure.append(",\n".join(closure_returns))
                    closure.append(indent_str + "}" + f"</{r.Element}>")
                    returns.append("\n".join(closure))

        return returns

    def generate_flwor(
            self, query, ordering, species_translation, counters, nested, enclose):
        """

        Create an XQuery string from the specified selection and
        projection criteria

        :param query:  Tree representation of control points.  We use this
           if a control element requires us to access another element.
        :param ordering:  Order in which collections should be processed
        :param species_translation:  Information on how to translate Latin species
           names or abbreviations to taxonomic serial numbers (TSNs).  query
           key of this dictionary is None or provides function for translation
           to TSN.
        :param counters:  Dictionary of counters used for generating any new
           variables

        :param nested:  Should results from each document in a collection be
           nested within a document (True/False).  Only applies when processing
           highest level.
        :param enclose:  Enclose return values of subloops within an element?
           (True/False)

        :return:  Flwor node suitable for generating an XQuery
        """

        let_vars = OrderedDict()
        return_list = []
        # Empty flowr expression
        flwor = self.flwortype([], [], [], [], [])
        # Handle joins
        joins = []
        for root_el in ordering:  # Process collections in known order
            loop_construct = []
            ns = ''  # empty namespace, rely on default namespace declaration
            current = query[root_el]
            for nodepath in current:
                # Generate flwor for this collection
                c_flwor = self.generate_flwor_aux(
                    nodepath, current, ordering, species_translation,
                    counters, let_vars, enclose)
                flwor.For.extend(c_flwor.For)
                flwor.Let.extend(c_flwor.Let)
                flwor.Where.extend(c_flwor.Where)
                flwor.Return.extend(c_flwor.Return)

        # Some species ids should be handled as global let variables at
        # the top level of the expression to prevent repeated lookups
        if len(let_vars) > 0:
            # Values are (variable, assignment string)
            let_expr = [v[1] for v in let_vars.values()]
            parent_flwor = self.flwortype([], let_expr, [], [], [flwor])
            flwor = parent_flwor

        return flwor

    def schema_search(self, root_el, pattern):
        """
        Given a root element, see if the pattern can be found in the
        schema associated with the root.  Return a list of paths that
        match the pattern.
        :param root_el:
        :param pattern:
        :return:
        """

        @cache
        def get_schema(root):
            """
            Return a set of valid paths for specified schema root element
            :param root:
            :return:  List of paths

            Function is cached (memoized) to avoid performance hits as it
            can be called frequently.
            """
            # Retrieve CSV version of schema & extract paths.
            schema = self.schemata.schema_to_str(root, "CSV", False)
            # path is first argument
            paths = [ele.split(",")[0] for ele in schema.split("\n")]
            # Remove header row
            paths.pop(0)
            return paths

        # retrieve paths associated with this schema
        if root_el in self.schemata.root2schemaname:
            paths = get_schema(root_el)
        else:
            raise ValueError(f'Unknown schema root element: "{root_el}"')

        # See if pattern starts with a regex reserved char
        if pattern[0] in ".^$*+?{[":
            pattern_re = re.compile(pattern)
            fallback = None
        else:
            # Heuristic as to what the user might be looking for.
            # We'll assume that they have the most specific portion of the
            # path, e.g. the element name that they are interested in and
            # anchor it to the end of the string.

            # If they have a / in the pattern, we'll assume that they are
            # providing a partial path, otherwise we'll prepend a / to
            # so that we don't end up with matches to things that end the
            # same way (e.g., End --> /End so that we match a/End and not
            # a/TheEnd)
            if "/" in pattern:
                pattern_re = re.compile(f"{pattern}$")
                fallback = pattern
            else:
                # Most common pattern is one element name without a path, e.g.,
                # Start.  We try to see if we can match /Start and fallback to
                # Start if it fails.  This usually does what the user intends
                # and reduces the number of unintentional matches.  We pin the
                # search to end of the path as well to prevent matching paths
                # where the element has things after it
                pattern_re = re.compile(f"/{pattern}$")
                fallback = f"{pattern}$"

        matches = list(filter(pattern_re.search, paths))
        if len(matches) == 0 and fallback is not None:
            fallback_re = re.compile(fallback)
            matches = list(filter(fallback_re.match, paths))

        return matches

    def generate_flwor_aux(
            self, path, query, ordering, species_translation, counters,
            let_vars, enclose):
        """

        :param path:
        :param query:
        :param ordering:
        :param species_translation:
        :param counters:
        :param let_vars:
        :param enclose:
        :return:
        """

        path_list = path.split("/")

        # Retrieve the node associated with this path.
        # The node's value specifies our selection criteria as well as
        # values that we project back from the XML
        node, subdict = query.get(path, node=True)

        # Representation of FLWOR expression
        flwor = self.flwortype([], [], [], [], [])

        loop_exists = (len(node.selection) or len(node.projection)) \
                and node.loop_idx >= 0

        # Requires loop?
        if loop_exists:
            # We will be looping over this variable
            loop_var = f'${path_list[0]}{node.loop_idx}'
            # Determine what we are looping over
            if node.loop_idx == 0:
                # Special case for root element
                collection = self.dbserver.methods.root2containers[path_list[0]]
                for_path = f'collection("{collection}")/{path_list[0]}'
            else:
                # Search for the youngest ancestor that has a for loop
                # associated with it.  All references within this loop
                # will be relative to that ancestor.
                # We find the path (ancestor), and its associated node
                # (sel_ret_ancestor).
                # We do not consider the last ancestor as that is the node
                # itself
                ancestor = anc_info = None
                for ancestor in node.ancestors[-2::-1]:
                    anc_info, anc_dict = query.get(ancestor, node=True)
                    if anc_info.loop_idx >= 0:
                        break  # Found nearest loop variable
                # Sanity check for ancestor w/loop var
                # (should always be the case)
                if anc_info is None:
                    raise ValueError(
                        f"While building loop for nodes along path: {path}",
                        "\nwe were unable to find an enclosing element"
                        "that could be repeated multiple times."
                        "\nPlease report this query to the Tethys maintainers.")
                # Name of youngest ancestor's variable
                ancestor_var = f"${path_list[0]}{anc_info.loop_idx}"


                # Relative path from parent to current node
                rel_path = path.replace(anc_dict.get_path(), "")
                for_path = f'{ancestor_var}{rel_path}'

            for_construct = self.fortype(loop_var, [for_path])

            # Add selection criteria to for loop XPath
            sel_criteria = self.xpath_conditions(
                path, node, species_translation, counters, let_vars)
            selectionsN = len(sel_criteria)
            if selectionsN > 0:
                for_construct.Path.append(f'[{" and ".join(sel_criteria)}]')

            flwor.For.append(for_construct)

        # process elements to be returned
        if len(node.projection) > 0:
            projections = self.xpath_returns(
                path, node, loop_var,
                species_translation, counters, let_vars, enclose)
            if isinstance(projections, list) and len(projections) > 0:
                flwor.Return.extend(projections)
            elif isinstance(projections, self.enclosetype):
                flwor.Return.append(projections)

        # Find subflwors and then insert them into the proper return statements
        subflwors = []
        for k in subdict.keys():
            sub_flwor = self.generate_flwor_aux(
                f"{path}/{k}", query, ordering, species_translation,
                counters, let_vars, enclose)
            if max([len(t) for t in sub_flwor]) > 0:
                # Something in the flwor, keep it
                subflwors.append(sub_flwor)
        if len(subflwors) > 0:
            # child flwor has 1+ sub flwors in its return statement.
            # Add them to the return list.
            if len(flwor.Return) > 0 and \
                        isinstance(flwor.Return[-1], self.enclosetype):
                # flwor is enclosed, target enclosure return
                return_list = flwor.Return[-1].Return
            else:
                # Not enclosed, just add to flwor's return list
                return_list = flwor.Return
            return_list.extend(subflwors)

        return flwor

    def xpath_conditions(self, nodepath, node, species_translation, counters, let_vars):
        """
        Construct XPath selection criteria to be embedded within a node
        :param nodepath:  path to an element
        :param node:  NodeType named tuple, node.selection contains
           information about selections
        :param species_translation: Information for converting species labels to
           taxonomic serial numbers (TSNs)
        :param counters:  Dictionary of counter objects for collections
        :param let_vars:  Dictionary of variables assigned outside of loop
           constructs
        :return:  list of strings containing conditions
        """

        if len(node.selection):
            # Construct a PathValueDict that creates a hierarchy of paths.
            # This lets us process paths in order, nesting criteria within
            # XPaths. For example, if we have criteria:
            #    Effort/Kind/SpeciesId = 180530 and
            #    Effort/Kind/Granularity = "binned",
            # we want to generate an XPath:
            #     Effort/Kind[SpeciesId = 180530 and Granularity="binned"]
            hierarchy = PathValueDict()
            hierarchy.set_default_interior_value_fn(list)
            for operation in node.selection:
                o = operation.copy()
                var = o['operands'][0]
                if var in hierarchy:
                    hierarchy[var].append(o)
                else:
                    hierarchy[var] = [o]

            # Traverse the hierarchical dictionary, building up expressions as we go
            conditions = self.xpath_conditions_aux(
                nodepath, hierarchy, species_translation, counters, let_vars)
        else:
            conditions = []

        return conditions

    def xpath_conditions_aux(self, nodepath, hierarchy, species_translation,
                             counters, let_vars):
        """
        Auxilary function for parsing conditions associated with an xpath
        :param nodepath:  path to current node
        :param hierarchy:
        :param species_translation: information on how to handle TSNs
        :param counters:  Used for automatic variable generation
        :param let_vars:  Variables used in let statements
        :return:
        """

        conditions = []

        node = hierarchy.get(nodepath, node=True)
        if node is None:
            return conditions  # No conditions associated with path

        current = node.value
        level_hierarchy = node.subdict

        keys = level_hierarchy.keys()
        parent = level_hierarchy.parent_name  # Last element of path
        for key in keys:
            # Retrieve the conditions and sub-hierarchy associated
            # with the current key
            op_list, sub_hierarchy = level_hierarchy.get(key, node=True)

            if op_list is None:
                # This node in the path has no conditions associated with it
                continue

            # Handle conditions that are deeper in the tree
            if len(sub_hierarchy) > 0:
                # Process sub_keys with new path
                sub_path = key
                sub_conditions = self.xpath_conditions_aux(
                    sub_path, level_hierarchy, species_translation, counters,
                    let_vars)
                if self.debug and len(sub_conditions) > 0:
                    print(f"{nodepath}/{key} sub_conditions {sub_conditions}")
            else:
                sub_conditions = []

            # Keys with no value are either checked for existence or are
            # part of a path
            if len(op_list) == 0:
                # No conditions on this node, handle sub_conditions
                n = len(sub_conditions)
                if n > 0:
                    conditions.append(
                        f"{key}[{' and '.join(sub_conditions)}]")
            else:
                criteria = []
                # Path from the root to this node
                fullpath = level_hierarchy.get_path()
                first = True
                for operation in op_list:
                    # Get the variable we are referencing and strip out the
                    # path up to the loop variable

                    var = operation['operands'][0]  # operand 0 is loop variable
                    if var == fullpath:
                        # Special case, criteria are on the node itself
                        var = "."
                        attribute = False
                    else:
                        var = var.replace(fullpath + "/", "")
                        var_elements = var.split('/')
                        attribute = var_elements[-1].startswith("@")

                    if operation['optype'] in ('binary', 'infix'):
                        operands = operation['operands']
                        type_info = operation['type']

                        # SpeciesIds are handled a bit differently.  The
                        # possibility of Group attributes make this a bit
                        # more difficult. Consider someone querying a list of
                        # two codings that expand to different species and
                        # groups:  GAS1, GBS2
                        # The query must be
                        #   ((Group=A and Species=1) or (Group=B and Species=2))
                        # When groups are not involved, we can simply use
                        # Species=(1,2). This is only an issue when users are
                        # using a species abbreviation map
                        if type_info.info and \
                                type_info.info.get('special', "") == 'ITIS':
                            # Special handling for SpeciesId, modifies the
                            # criteria list and possibly counters, let_vars
                            self.xpath_conditions_species(
                                criteria, var, operation['op'], operands,
                                species_translation, counters, let_vars)
                            continue

                        elif type_info.type in ("xs:string", "xs:token"):
                            if isinstance(operands[1], list):
                                if len(operands[1]) > 1:
                                    # Convert to tuple: ("a", "b", ..., "z")
                                    rhs = f"{tuple([op for op in operands[1]])}"
                                else:
                                    rhs = f'"{operands[1][0]}"'
                            else:
                                rhs = f'"{operands[1]}"'
                        elif type_info.type in ('xs:int', 'xs:integer',
                                                'xs:double', 'xs:float'):
                            if isinstance(operands[1], list):
                                if len(operands[1]) > 1:
                                    # Convert to tuple: ("a", "b", ..., "z")
                                    rhs = f"{tuple([op for op in operands[1]])}"
                                else:
                                    rhs = f"{operands[1][0]}"

                            else:
                                rhs = f'{operands[1]}'

                        elif type_info.type == "xs:dateTime":
                            rhs = f'xs:dateTime("{operands[1]}")'

                        elif type_info.type == DecoratedSchemaDict.any_elem:
                            # Not in the schema -> treated as string
                            rhs = f'"{operands[1]}"'
                        else:
                            rhs = operands[1]
                        criteria.append(f"{var} {operation['op']} {rhs}")

                    elif operation['optype'] == "function":
                        # if operation['type']:
                        #     raise ValueError(
                        #     "JSON XQuery: Functions on typed operands not yet supported")
                        # else:
                        # Set up relative path
                        operands = operation['operands'].copy()
                        operands[0] = var  # Replace w/ relative path
                        # Any sub_conditions must be handled here as they
                        # must go inside the functor
                        if len(sub_conditions) > 0:
                            operands[0] = \
                                f"{operands[0]}[{' and '.join(sub_conditions)}]"
                            sub_conditions = []
                        # Ensure all operands are strings
                        str_operands = [f"{o}" for o in operands]

                        # Append the function
                        criteria.append(
                            f"{operation['op']}({','.join(str_operands)})")

                # merge criteria and sub_conditions associated with key
                # into conditions
                n = len(criteria)

                if n > 0:
                    if self.debug:
                        print(f"{nodepath}/{key} criteria {criteria}")
                    # First case will have sub_conditions incorporated into it
                    if len(sub_conditions) > 0:
                        found = False
                        idx = 0
                        while not found and idx < len(sub_conditions):
                            m = self.path_re.match(criteria[idx])
                            if m:
                                found = True
                                # Determine which group matched
                                # Add one as groups start at 1
                                grp = 1 + [i for i, v in enumerate(m.groups())
                                           if v is not None][0]
                                at = m.span(grp)
                                criteria[idx] = criteria[idx][0:at[1]] + \
                                    '[' + ' and '.join(sub_conditions) + ']' + \
                                    criteria[idx][at[1]:]
                        if not found:
                            raise ValueError(
                                "XQuery JSON generator:  "
                                f"Unable to inject {sub_conditions} "
                                f"into criteria {criteria}")

                    conditions.extend(criteria)

        return conditions

    def xpath_conditions_species(self, conditions, var, op, operands,
                                 species_translation, counters, let_vars):
        """
        :param conditions:
        :param var:
        :param op:  operator
        :param operands:  List expects path, species representation
            species representation may itself be a list
        :param species_translation:
        :param let_vars:
        :return:
        """

        lhs = operands[0]
        species_list = operands[1]
        if not isinstance(species_list, list):
            species_list = [species_list]


        qtsn = species_translation['query']
        if qtsn:
            operation = qtsn['op']
            if operation == 'lib:abbrev2tsn':
                # Caller is using an abbreviation map.
                # This can be tricky, we can have multiple species and
                # multiple groups

                # Start by gathering up the species codes and determine which
                # species and groups we are working with
                byspecies = defaultdict(set)
                abbrev_map = qtsn['abbrev_map']
                # for each species coding, find TSN and group
                for s in species_list:
                    try:
                        entry = abbrev_map.loc[s]
                    except KeyError as e:
                        if len(qtsn['operands']) > 1:
                            mapname = qtsn['operands'][1]
                        else:
                            mapname = ""  # map name not where we expected
                        raise ValueError(
                            f"Species {s} unrecognized in abbreviation map {mapname}")
                    groups = byspecies[entry.tsn]
                    byspecies[entry.tsn] = groups.union((entry.Group,))

                # We now know which species groups are associated with
                # each species, construct the criteria for the query
                simple = []   # Simple cases, tsn only
                complicated = []  # tsn and Group
                for k,v in byspecies.items():
                    v = list(v)
                    if any(pd.isna(v)):
                        # At least one item in the list does not have a group.
                        # The user is asking for the species irregardless of
                        # group.  This supersedes any group entries.
                        simple.append(k)
                    else:
                        complicated.append((k, v))

                or_list = []
                # These species we only need to check for the TSN
                if len(simple) > 0:
                    rhs = simple[0] if len(simple) == 1 else tuple(simple)
                    or_list.append(f"{var} {op} {rhs}")

                # These species we need to check for TSN and group
                if len(complicated) > 0:
                    for tsn, groups in complicated:
                        if len(groups) == 1:
                            group_str = f'"{groups[0]}"'
                        else:
                            group_str = '("' + '", "'.join(groups) + '")'
                        or_list.append(f'{var}[@Group = {group_str}] {op} {tsn}')
                if len(or_list) > 1:
                    conditions.append('(' + " or ".join(or_list) + ')')
                else:
                    conditions.extend(or_list)

            else:
                # Determine what ITIS expression to translate to TSN
                arg_list = '"' + '", "'.join(
                    map(lambda s: s % (operands[1]) if s.startswith('%')
                    else s, qtsn['operands'])) + '"'
                expression = f"{qtsn['op']}({arg_list})"
                if expression in let_vars:
                    # We have seen this one before, use existing variable
                    tsn_var, _ = let_vars[expression]
                else:
                    # Brand new assignment
                    tsn_var = f"$tsn{next(counters['variables'])}"
                    assignment = f"{tsn_var} := {expression}"
                    let_vars[expression] = (tsn_var, assignment)
                rhs = tsn_var  # right hand side of operand
                conditions.append(f"{var} {op} {rhs}")
        else:
            # simple case, raw TSNs
            if isinstance(operands[1], list):
                if operands[1] > 1:
                    rhs = f"{tuple(operands[1])}"
                else:
                    rhs = f"{operands[1]}"
            else:
                rhs = f"{operands[1]}"
            conditions.append(f"{lhs} {op} {rhs}")

        return conditions

    def xpath_returns(self, nodepath, node, variable,
                      species_translation, counters, let_vars,
                      enclose):
        """
        :param nodepath:  Return values are relative to this node
        :param node:  NodeType information about variables to be returned
        :param variable:  Return paths will be relative to this loop variable
        :param enclose:  If True, enclose variables in an element with the
           same name as the last element of nodepath.  Otherwise, elements
           are returned as a flat sequence
        :param counters:  Dictionary of counter objects for collections
        :param let_vars:  Dictionary of variables assigned outside of loop
           constructs
        :return:  List of elements to be returned
        """

        # List of elements that we will project from the current node
        proj_list = node.projection

        # We will construct return paths:
        proj_elements = []  # relative to current loop variable

        # The projections are either relative to a variable, or may be the
        # variable itself.
        # For example, when looping over Detections/Effort/Kind with var $x,
        # we might project Detection/Effort/Kind which would return $x
        # or we might project Detection/Effort/Kind/SpeciesId which would
        # require a return of $x/SpeciesId.
        # It should be rare to return both the loop element and a subelement
        # as this would result in redundant information, but it would not be
        # incorrect.
        for proj in proj_list:
            if nodepath == proj:
                proj_elements.append(variable)
            else:
                fullpath = f"{nodepath}/{proj}"  # path without $variables
                # Get projection path relative to loop variable
                relpath = proj.replace(nodepath + "/", "")
                varpath = f"{variable}/{relpath}"  # path from loop var
                # Conditions returns a list; this is a throwback from an
                # earlier time when we were constructing where clauses.
                # Now its just an XPath and there should only be one [0]
                condition_list = self.xpath_conditions(
                    fullpath, node, species_translation,
                    counters, let_vars)
                if len(condition_list):
                    conditions = f"[{' and '.join(condition_list)}]"
                else:
                    conditions = ""
                proj_elements.append(f"{varpath}{conditions}")  # from loop var

        # If user requested, enclose within the element.  Check for special case
        # of a user returning the enclosing element and avoid double wrapping.
        if enclose:
            enclosing_element = nodepath.split("/")[-1]  # last element
            if len(proj_elements) == 0 or "/" in proj_elements[0]:
                # len 0 --> enclosing element for children (nothing here) or
                # Multi-component path (/) implies not returning a loop variable
                proj_elements = self.enclosetype(enclosing_element,
                                                 proj_elements)

        return proj_elements


class XQuery:

    use_post_error =  "\n".join([
        "Queries must be submitted via POST with a multipart form. ",
        "Use body form XQuery to execute an XQuery. ",
        "Use body form JSON to to generate and execute XQuery queries automatically.",
        "See the Tethys RESTful web interface manual for details."])
    # XQuery for mapping taxonomic serial numbers to human-readable names
    tsn_query = "\n".join([
        'import module namespace lib="http://tethys.sdsu.edu/XQueryFns" at "Tethys.xq";',
        'declare default element namespace "http://tethys.sdsu.edu/schema/1.0";',
        '',
        '<Result>',
        '{',
        '  let $ids := %s  (: TSN list :)',
        '  return',
        '    for $rank in collection("ITIS_ranks")/ranks/rank[tsn = $ids]',
        '    return',
	    '      <entry>',
        '        {$rank/tsn}',
        '           <Latin>{$rank/completename/text()}</Latin>',
        '           <English>{$rank/vernacular/name[@language="English"][1]/text()}</English>',
        '           <Spanish>{$rank/vernacular/name[@language="Spanish"][1]/text()}</Spanish>',
        '           <French>{$rank/vernacular/name[@language="French"][1]/text()}</French>',
        '       </entry>',
        '}',
        '</Result>'])

    # Regular expression for detecting taxonomic serial numbers (species)
    # with possible Group attribute.  Group attribute will not accept
    # escaped quotes, e.g. Group='Caldwell\'s Group',
    # use Group="Calldwell's Group" in the XML instead or expand the regexp.
    speciesid_re = re.compile(r"""
	<(\w+:)?SpeciesId\s*   # Match opening SpeciesId with optional namespace
	  (?:                  # Match optional group
	    Group\s*=
	    \s*(["'])     # Remember opening quote
	      (?P<group>(?:[^\2])*?)   # Note non-greedy consumption to end quote
	    \2  # Match end quote
	  )?>
	    (?P<id>-?\d+)\s*</\1?SpeciesId\s*>  # Match the TSN
	""", re.VERBOSE | re.MULTILINE)

    def __init__(self, dbserver,
                 restserver,
                 xml2other=None,
                 verbosity=1):
        """
        XQuery processing class
        :param dbserver: dbxmlInterface.db.Server interface
        :param restserver: Restful web interface
        :param xml2other:  XML translator object
        :param verbosity:   control print statement verbosity (0=none)
        """
        self.dbserver = dbserver
        self.restserver = restserver
        self.xml2other = xml2other


        # Bind several methods of the XMLMethods object to local variables
        # This will make our queries more readable, but otherwise serves
        # no purpose
        methods = dbserver.getMethods()
        self.xquery = methods.query
        self.xqueryplan = methods.query_plan

        self.queue = Queue(maxsize=0)
        self.makeWorkers(1)

        # JSON to XQuery generator
        self.jsonXQuery = XQueryJSON(self.dbserver)
        self.verbosity = verbosity

    exposed = True

    def makeWorkers(self,numberThreads):

        for i in range(numberThreads):
            worker = Thread(target=self.writeXMLToFile, args=(self.queue,))
            worker.setDaemon(True)
            worker.start()

    def writeXMLToFile(self, queue):
        while True:
            #print "result from worker function ",queue.get()
            item = queue.get()
            path = os.path.join(resources.names['ResultXML'],
                                item['uuid'] + '.xml')
            try:
                fileHandle = open(path,'w')
                fileHandle.write(item['string'])
                fileHandle.close()
            except Exception:
                raise cherrypy.HTTPError(
                    codes["Not Found"],
                    "Could not write to directory %s" % path)
            semaphores[item['uuid']].release()
            queue.task_done()

    def GET(self, *args, **kwargs):
        raise cherrypy.HTTPError(codes["Forbidden"], self.use_post_error)

    def PUT(self, *args, **kwargs):
        raise cherrypy.HTTPError(codes["Forbidden"], self.use_post_error)

    def get_int_from(self, dictionary, name, default):
        value = dictionary.get(name, default)
        try:
            value = int(value)
        except ValueError:
            cherrypy.response.status = codes['Bad Request']
            raise(f"Path variable {name}={value} is not an integer")

        return value

    def POST(self, *args, **kwargs):
        """
        Generate/Run an XQuery
        :param args: None expected, we will ignore any passed in
        :param kwargs: Expected arguments from path and body variables:

          keyword Xquery or JSON must be present.

          XQuery - Value is XQuery text to be executed

          JSON - Value contains data that will be translated to an XQuery
          and processed

          The following JSON keys are supported:

            "species" - Optional schema on how to translate specifes identifiers
               May contain subkeys:
               "query" - Specification to translate SpeciesId to ITIS TSNs.
                  Must contain furhter subkeys to specify function, see
                  Tethys Web Services Manual for details and examples.
               "return" - Specification for how to translate SpeciesIds in
                  query results from ITIS TSNs to human readable Latin
                  names or custom abbreviation sets.  See
                  Tethys Web Services Manual for details and examples.
            "namespaces" - true|false   Useful for clients that have issues
               with namespaces.  If false, namespaces are stripped from
               the results.

            Two types of JSON queries are supported:
            1.  A list of selection criteria and return arguments, see
                the Tethys Restful Web services documentation for more
                details and examples.  Keys:

                "select" - selection arguments.
                "return" - return arguments
                "enclose" - Generate enclosing elements?
                   When fields are to be returned that are nested within an
                   enclosing element, a value of 1 will cause the enclosing
                   element to be generated.  A value of 0, or omitting enclose
                   will result in the enclosing elements being omitted.
                   See Tethys Web Services Interface manual for further
                   details.

            2.  A text-based query.  Keys:

                 "query" - Text based query, must conform to a custom
                     mini-language
                 "root" - The root element of the schema we are most interested
                     in.  In the query, users can write full paths, e.g.,
                     Deployment/SamplingDetails/Channel/Sampling/Regimen/SampleRate_kHz,
                     but if root is set to "Deployment", using a substring
                     of the name, SampleRate_kHz, or Regimen/SampleRate_kHz
                     will automatically find the full path.  When the path
                     matches more than one entry, an error is usually generated,
                     but in certain cases we can infer the correct thing to do.
                     Example:  In Detections, SpeciesId occurs in Effort/Kind,
                     OnEffort/Detection, and OffEffort/Detection.  If the
                     use specifies SpeciesId, we will use the "effort" key
                     describe below to determine if this should be
                     Effort/Kind/SpeciesId or OnEffort/Detection/SpeciesId.
                 "effort" - True|False (defaults to False if missing).  If True,
                     we assume that user is interested in effort as opposed to
                     results (not applicable to all collections).  The set of
                     default fields to be returned will be adjusted based upon
                     the value of effort.

          'plan' - Return information about what will be executed.  Requires
             an integer value [0-2]
             0 - query is executed and plan is not shown (default)
             1 - query is not executed and dbXML query plan is shown
             2 - Only valid for JSON/SimpleQuery queries, query is not executed and
                 the XQuery generated from the JSON specification is shown

          'namespaces' - 0 | 1 (default) If 0, removes namespaces from returned
               XML and pretty prints the result.  If using a JSON query, has
               precedence over the JSON specification.  Ignored when plan > 0
          'species' - Latin | English | French | Spanish or a JSON species
             specification.

             JSON queries should use create a "species" entry in the data
             passed to the JSON form variable.  This form variable will
             be ignored for JSON queries with a "species" entry.

             Currently, this entry only affects output translation.
             When the value is a language string (e.g, Latin, English)
             taxa will be listed using the Latin taxonomic name or in
             the vernacular of the specified language.  When entries are not
             present in the vernacular, the Latin name is used.

             When the value is a JSON string, it is expected to contain
             a return entry as described for the JSON species key.

             Note for people using XQuery
             Translation of input species names, vernacular, abbreviations etc.
             is only be done in the JSON specification as it would require
             more parsing to identify where TSNs are used in XQuery.
             We suggest using the Tethys XQuery library calls to perform
             translation:
                lib:completename2tsn, lib:abbrev2tsn, and lib:vernacular2tsn
             between TSNs in the query and
             to translate between human-readable representations of taxa and
             the taxonomic serial numbers.  It is recommended to place the
             translations in a let statement and then run the query with
             the resultant translation to avoid having the translation
             function executed multiple times.
                 XQuery users, write something like this:
             multiple times.

          .  If present, output
               TSNs will be mapped to the specified language.  Finer grained
                control is possible with the JSON species specification.
          'XSLT' - If query is executed successfully, the XSLT stylesheet
               will be applied to the result.
          'dataType' - What type of data will be returned.  Supported types:
               XML
               JSON


        :return: query result/plan/etc.
        """

        # handle arguments (some processing done at later stages)
        namespaces = self.get_int_from(kwargs, "namespaces", 1) == 1  # retain namespace?
        post_process = None  # Assume no post-processing for now
        dataType = kwargs.get('dataType', 'XML')
        plan = self.get_int_from(kwargs, "plan", 0)

        if 'XQuery' in kwargs:
            query = kwargs['XQuery']
            jdict = None  # No JSON structure

        elif 'JSON' in kwargs:
            jdict = self._parseJSON(kwargs["JSON"], "JSON")

            if "namespaces" not in kwargs:
                # Use JSON specification for whether or not to strip the
                # namespace if not specified in the post.
                namespaces = jdict.get("namespaces", 1) == 1

            enclose = jdict["enclose"] == 1

            if "query" in jdict:
                # simple query language produces select and return
                # elements suitable for jsonXQuery
                # jdict will be updated and ready

                xq_pred = XQueryPredicate(
                    self.dbserver, self.jsonXQuery, jdict)

            if "select" in jdict or "return" in jdict:
                # select/return list to XQuery
                query = self.jsonXQuery.generate_Xquery(jdict, enclose)
            else:
                raise cherrypy.HTTPError(
                    codes["Bad Request"],
                    "XQuery JSON specification must contain query or select/return"
                )

        else:
            raise cherrypy.HTTPError(
                codes["Bad Request"],
                "XQuery resource requires XQuery or JSON specification")

        if plan > 0:
            # Return the XQuery plan (result of XQuery parser) or the
            # Xquery constructed from the JSON specification.
            # We ignore post-processing and namespace directives for this
            # type of query that returns information about queries rather
            # than data.
            if jdict is not None and plan == 2:
                result = query  # XQuery text
            else:
                # get query plan
                try:
                    result = self.xqueryplan(query)
                except Exception as e:
                    cherrypy.response.status = codes["Bad Request"]
                    response = str(e)
                    return response

            return result

        executed = False
        try:
            result = self.xquery(query, not namespaces)
            executed = True

        except dbxmlInterface.error.XmlQueryEvaluationErrorExtended as e:
            cherrypy.response.status = codes["Bad Request"]
            if "Unknown Species Abbreviation" in e.what:
                what = "A species abbreviation code was specified that " + \
                       "is not in the specified abbreviation map.  Check " + \
                       "the abbreviation in the species map."
            elif "NoMatchingLatinCompletename" in e.what:
                what = "A Latin species name was specified that is not " + \
                       "in the database.  Check spelling of SpeciesId."
            else:
                what = e.what
            if query != e.Query:
                result = "\n".join([
                    what, "-- Query was transformed, ",
                    "this is the original query --", query,
                    "-- Query --", e.Query])
            else:
                result = "\n".join([what,"-- Query --", e.Query])

        except (dbxml.XmlDocumentNotFound,
                dbxml.XmlContainerNotFound,
                dbxml.XmlDatabaseError,
                dbxml.XmlQueryEvaluationError,
                dbxml.XmlQueryParserError,
                dbxml.XmlException,
                DBInternalError) as e:

            cherrypy.response.status = codes["Bad Request"]
            result = e.what

        except (QueryError, XmlQueryEvaluationErrorExtended) as e:
            cherrypy.response.status = codes["Bad Request"]
            result = str(e)

        except UnicodeDecodeError as e:
            #todo:  When sending in poor query strings to ERRDAP, this is the error thrown, e.g.
            #q.Query('collection("ext:erddap")/jplCcmp35aWindPentad?wspd[(2008-10-15T00:00:00.0.436500)]!')
            cherrypy.response.status = codes["Bad Request"]
            result = "\n<Error>\n Malformed query string\n</Error>"

        except (TypeError, ValueError, EnvironmentError) as e:
            cherrypy.response.status = codes["Forbidden"]
            result = "\n<Error>\n %s \n</Error>\n" % (str(e)) + \
                "<traceback>\n%s\n</traceback>" % (traceback.format_exc())

        if 'responseType' in kwargs:
            # User wants results in specified format
            cherrypy.response.headers['responseType'] = kwargs['responseType']
            # for case-independent tests
            responseType = kwargs['responseType'].lower()
        else:
            responseType = "application/xml"

        if executed:
            # Check to see if there are post-processing directives
            # These may appear either in the JSON specification
            # or as http form variables.  If they appear in both places,
            # precedence is given to the JSON specification.

            species_output_translation = None
            if jdict is not None:
                if "species" in jdict and "return" in jdict['species']:
                    species_output_translation = jdict["species"]["return"]
            if species_output_translation is None and \
                    "species" in kwargs:
                species = kwargs['species']
                try:
                    # Process JSON species output.  Bad JSON spec or
                    # simple language specification processed in error leg
                    speciesdict = \
                        self._parseJSON(kwargs["species"], "species")
                    if "return" in speciesdict:
                        species_output_translation = speciesdict['return']
                except ValueError as v:
                    # If the user wants to specify output in Latin or
                    # vernacular, we make it easy for them, just include
                    # one of these strings:
                    species = species.capitalize()  # case-indep comparison
                    # Languages available in ITIS (not complete for vernacular)
                    languages = ["Latin", "English", "French", "Spanish"]
                    if species in languages:
                        if species == "Latin":  # ITIS completename
                            species_output_translation = dict(
                                op="lib:tsn2completename",
                                operands="%s",
                                optype="function"
                            )
                        else:
                            # ITIS vernacular
                            species_output_translation = dict(
                                op="lib:tsn2vernacular",
                                operands=["%s", species],
                                optype="function"
                            )
                    else:
                        raise ValueError(
                            v,
                            "Alternatively, one may specify one of the "
                            "following strings\n"
                            f"({', '.join(languages)}.")

            stylesheet = kwargs.get("XSLT", None)

            result = self.postprocess(result,
                                      tsn_encoding=species_output_translation,
                                      xslt_stylesheet=stylesheet,
                                      response_type=responseType)

            if dataType == "save":
                docId = str(uuid.uuid4())
                semaphores[docId] = threading.Semaphore(0)
                self.queue.put({'uuid': docId,'string': result})  # why not put the semaphore in this dictionary?
                currentAddress = cherrypy.url()
                parseResult = urlparse(currentAddress)
                url = parseResult.scheme+"://"+parseResult.netloc+"/Convert/"+docId
                cherrypy.response.headers['Content-Location'] = url

        if responseType == 'application/json':
            cherrypy.response.headers['Content-Type'] = 'application/json'
            result = self.xml2other.convertToJSON(result)
        elif responseType == 'application/xml':
            # No need to convert anything
            pass
        elif responseType == "application/r":
            result = self.xml2other.convertToR(result)
            cherrypy.response.headers['Content-Type'] = 'application/octet-stream'
        elif responseType == "application/matlab":
            result = self.xml2other.convertToMatlab(result)
            cherrypy.response.headers['Content-Type'] = 'application/octet-stream'

        if self.verbosity > 0:
            if isinstance(result, bytes):
                hash = hashlib.md5(result).hexdigest()
            else:
                hash = hashlib.md5(result.encode('utf-8')).hexdigest()
            print(f"md5 hex digest checksum: {hash}")

        return result

    def _parseJSON(self, json_str, formvar="JSON"):
        """
        Parse JSON string into a dict
        :param self:
        :param json_str:  Java script object notation string
        :param formvar:  form variable from which this came (only
           needed for formatting the error message)
        :return: dict
        """
        try:
            jdict = json.loads(json_str)
        except json.decoder.JSONDecodeError as e:
            cherrypy.response.status = codes["Bad Request"]
            result = [
            f"Error parsing JSON from form variable '{formvar}': {e}",
            "This query passes information through a JSON object. "
            "The JSON decoder was unable to parse the JSON object, which "
            "indicates that the client module that produced the query "
            "has a programming error.  While client modules (e.g., Java, "
            "MATLAB, R, Python, etc. interfaces for Tethys) are not responsible "
            "for generating valid queries, they must pass information about the "
            "query in a properly formatted manner."
            ]

            for idx, line in enumerate(json_str.split("\n")):
                result.append(f'{idx+1:3d}: {line}')
            result = "\n".join(result)
            raise ValueError(result)

        # Set defaults for optional parameters that are common
        if "enclose" not in jdict:
            jdict["enclose"] = 0

        return jdict

    def postprocess(self, xml, tsn_encoding=None,
                    xslt_stylesheet=None,
                    response_type="application/xml"):
        """
        Postprocess a serialized XML result
        :param xml: XML string
        :param tsn_encoding: JSON TSN output encoding
        :param xslt_stylesheet: XSLT stylesheet specification.  XSLT will
           be executed using specified instructions after any encoding of
           TSNs.
        :param response_type:  Format of data returned
        :return:  resultant XML
        """

        if tsn_encoding is not None:
            # Get a list of TSNs
            matches = self.speciesid_re.findall(xml)
            if matches:

                # Matches are pairs (attributes/empty, TSN), pull out TSNs
                group_tsns = list(set([(m[2],m[3]) for m in matches]))
                tsns = list(set([m[1] for m in group_tsns]))  # get unique TSNs

                # Query to get Latin and some vernacular names
                # If using abbreviations, we only use these if something is
                # not in the abbreviation list.
                tsn_list = f'({",".join([str(t) for t in tsns])})'
                tsn_result = self.xquery(self.tsn_query % (tsn_list),
                                         clean_results=True)
                if "entry" not in tsn_result:
                    raise ValueError(f"Unable to resolve TSNs: {tsns}")

                tsn_map = pd.read_xml(
                    tsn_result, xpath="/Result/entry")

                if len(tsns) > 0:
                    # Determine what kind of names we will be needing
                    op = tsn_encoding['op']
                    if op == 'lib:tsn2abbrev':
                        if len(tsn_encoding['operands']) < 2:
                            raise ValueError("No abbreviation map specified " +
                                             "for result SpeciesId translation")
                        # Retrieve the abbreviation map
                        abbr_map = self.dbserver.methods.get_abbreviations_map(
                            tsn_encoding['operands'][1])

                        def replace_abbr(match):
                            """
                            Substitution subroutine run each time there is
                            a match on a species id.  If a TSN is present
                            that is not in the abbreviation map, we substitute
                            the Latin name
                            :param match:  re match object on a SpeciesId
                               element and its contents
                            :return: SpeciesId element with a human-readable
                                taxonomic string
                            """
                            groups = match.groups()
                            # Retrieve the TSN and the Group attribute
                            tsn = int(groups[-1])
                            tsn_group = groups[-2]  # usually not present
                            # Preserve explicit SpeciesId namespace if present
                            ns = groups[0] if groups[0] else ""

                            # Find rows (0+) in abbreviation map that match the
                            # TSN being replaced.
                            # 0 - no match, use Latin name
                            # 1 - exact match
                            # >1 - Only possible if some entries have @Group
                            rows = abbr_map.loc[abbr_map.tsn == tsn]
                            species_name = None
                            repl = None
                            if rows.size:
                                # TSN is in abbreviation map
                                if tsn_group:
                                    # Get coding with Group attribute
                                    grouprows = rows.loc[rows.Group == tsn_group]
                                    if grouprows.size > 0:
                                        species_name = grouprows.index[0]

                                    repl = f'<{ns}SpeciesId Group="{tsn_group}"' \
                                           f'>{species_name}</{ns}SpeciesId>'
                                else:
                                    # No group, pull out encoding if available
                                    row = rows[rows.Group.isnull()]
                                    if row.size:
                                        species_name = row.index[0]
                                    repl = f'<{ns}SpeciesId>{species_name}' \
                                           f'</{ns}SpeciesId>'
                            if species_name is None:
                                # No abbreviation, use Latin
                                species_name = tsn_map[tsn_map.tsn == tsn]
                                if species_name.size:
                                    # Default to Latin name, we may override
                                    itis=""
                                    species_name = species_name.iloc[0].Latin
                                else:
                                    # TSN is not in the ITIS collection
                                    # (Bad TSN, shouldn't happen)
                                    itis=' itis="TSN not in local ITIS database"'
                                    species_name = f"{tsn}"

                                repl = f'<{ns}SpeciesId{itis}>{species_name}' \
                                       f'</{ns}SpeciesId>'

                            return repl

                        xml = self.speciesid_re.sub(replace_abbr, xml)

                    elif op in ('lib:tsn2completename', 'lib:tsn2vernacular'):

                        if isinstance(tsn_encoding, dict):
                            # See if we can avoid running a second query
                            if op == 'lib:tsn2completename':
                                translate = "Latin"
                            elif op == 'lib:tsn2vernacular':
                                if len(tsn_encoding['operands']) != 2:
                                    raise ValueError(
                                        'JSON species:return: lib:tsn2vernacular expected ("%s", "language")')
                                if tsn_encoding['operands'][1] in ('English', 'French', 'Spanish'):
                                    translate = tsn_encoding['operands'][1]

                        if isinstance(translate, str):
                            valid_langs = (
                            'Latin', 'English', 'French', 'Spanish')
                            if translate not in valid_langs:
                                cherrypy.response.status = codes["Bad request"]
                                raise f"species={translate} not in {valid_langs}"
                            # Easy case, we've already obtained translations
                            # from the itisranks collection
                            for idx, row in tsn_map.iterrows():
                                source = str(row.tsn)
                                replacement = row[translate]
                                if pd.isna(replacement):
                                    replacement = row['Latin']  # NaN, use Latin
                                xml = re.sub(f">\s*{source}\s*</SpeciesId>",
                                             f">{replacement}</SpeciesId>",
                                             xml)

                    else:
                        raise ValueError(
                            f"Unknown result SpeciesId translation operator {op}")

        # simple stylesheet to indent output nicely (used if none specified)
        xslt_default = '''
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema" version="2.0">
<!-- Identity transform -->
<xsl:template match="@* | node()">
<xsl:copy>
    <xsl:apply-templates select="@* | node()"/>
</xsl:copy>
</xsl:template>
<xsl:output method="xml" version="1.0" encoding="UTF-8" omit-xml-declaration="yes" indent="yes"/>
</xsl:stylesheet>'''

        # We style the result if the user has specified an XSLT style sheet
        # or with a default style sheet if the result_type is XML
        process = response_type == "application/xml" or \
                  xslt_stylesheet is not None
        if process:
            xslt = XSLT()
            try:
                xslt.set_xml(xml)
            except RuntimeError:
                process = False  # Cannot continue transformation
                if xslt_stylesheet is not None:
                    raise RuntimeError(
                        "XSLT stylesheet provided, but query result is not a "
                        "valid XML document.  Rerun the query without XSLT "
                        "processing to see document structure and revise "
                        "the query to produce a valid XML document."
                    )
                else:
                    self.restserver.log_error(
                        "Legal XQuery resulted in invalid XML.  " +
                        "Use POST header responseType:text/plain " +
                        "to avoid this warning.")
            if process:
                transform = xslt_stylesheet or xslt_default
                xslt.set_stylesheet(transform)
                xml = xslt.transform()

        return xml


class DriverType(Enum):
    Excel = "Microsoft Excel"
    Access = "Microsoft Access Driver"
    AccessDBase = "Microsoft Access dBASE Driver"
    SqlServer = "SQL Server"
    MySql = "MySQL"
    Text = "Microsoft Access Text Driver"
    Postgres = "PostgreSQL"

class dbJSON:
    """
    Connections to ODBC objects, primarily used in
    web client for designing import schema.
    """
    exposed = True

    def __init__(self,dbserver):
        self.dbserver = dbserver
        self.cursors = {}

    def GET(self, *args, **kwargs):
        print("No GET")

    def getForeignKeyRelationships(self, cursor, dbName, driver, sourceName):
        """
        Retrieve information about foreign keys when present
        :param cursor: database cursor
        :param dbName: database name
        :param driver: database driver - Used to determine how to retrieve
           foreign keys which can vary depending on the database technology
           When adding new database ODBC drivers, this may need to be updated
        :param sourceName: source identifier
        :return:  list of JSON objects.  Each JSON object has:
           TABLE_NAME - Table with foreign key
           COLUMN_NAME - Table attribute containing foreign key
           REFERENCED_TABLE_NAME - Foreign key indexes into this table
           REFERENCED_COLUMN_NAME - Attribute to be used in join
           POSITION_IN_UNIQUE_CONSTRAINT - Column index of REFERENCED_COLUMN_NAME
        """

        constraintQuery = ''
        if driver.value == DriverType.SqlServer.value:
            constraintQuery =  \
                "SELECT OBJECT_NAME(f.parent_object_id) TableName, " \
                "COL_NAME(fc.parent_object_id,fc.parent_column_id) ColName " \
                "FROM sys.foreign_keys AS f " \
                "INNER JOIN sys.foreign_key_columns AS fc "\
                    "ON f.OBJECT_ID = fc.constraint_object_id " \
                "INNER JOIN sys.tables t ON t.OBJECT_ID = fc.referenced_object_id"

        elif driver.value == DriverType.MySql.value:
            constraintQuery = \
                "SELECT TABLE_NAME,COLUMN_NAME, REFERENCED_TABLE_NAME, " \
                "REFERENCED_COLUMN_NAME, POSITION_IN_UNIQUE_CONSTRAINT " \
                "FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE " \
                f"WHERE TABLE_SCHEMA = '{dbName}' " \
                  "AND "\
                    "ceil(POSITION_IN_UNIQUE_CONSTRAINT) = POSITION_IN_UNIQUE_CONSTRAINT " \
                "GROUP BY TABLE_NAME,COLUMN_NAME, REFERENCED_TABLE_NAME, " \
                    "REFERENCED_COLUMN_NAME, POSITION_IN_UNIQUE_CONSTRAINT;"

        constraints = cursor.execute(constraintQuery)
        lc = list(constraints)  # list of tuples
        field_names = ['TABLE_NAME', 'COLUMN_NAME', 'REFERENCED_TABLE_NAME',
                       'REFERENCED_COLUMN_NAME',
                       'POSITION_IN_UNIQUE_CONSTRAINT']
        list_for_json = []
        for row in lc:
            row_dict = {k: v for k, v in zip(field_names,row)}
            list_for_json.append(row_dict)

        return list_for_json

    def dbInfo(self,dbName):

        infoQuery = "SELECT TABLE_NAME.COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = '%s' ORDER BY TABLE_NAME, ORDINAL_POSITION;" % (dbName)

        table = self.db.execute(infoQuery)
        lt = list(table)
        tableNames = set([x[0] for x in lt])
        cdDict = {}
        for name in tableNames:
            cdDict[name] = [x[1] for x in lt if x[0] == name]  # add the column names to key
        return cdDict

    # Patterns for ODBC to Python data types.
    # ODBC will handle the conversions, but this lets us report metadata to others
    # Be sure to check for datetime before date or time
    odbc_str_re = re.compile(r"(char|text)", re.IGNORECASE)
    odbc_int_re = re.compile(r"(int)", re.IGNORECASE)
    odbc_float_re = re.compile(r"(double)", re.IGNORECASE)
    odbc_timestamp_re = re.compile(r"(datetime)", re.IGNORECASE)
    odbc_date_re = re.compile(r"(date)", re.IGNORECASE)
    odbc_time_re = re.compile(r"(time)", re.IGNORECASE)

    def POST(self, **formvars):
        """
        :param formvars:  information to connect to ODBC
        """
        driverName = formvars.get('driverName', None)
        dbName = formvars.get('dbName', None)
        rowNum = int(formvars.get("rowDisplayed", None))

        driver = [enum for enum in DriverType if enum.value in driverName][0]

        errorDict = mediators.ODBC.Connector.ValidateOdbcInputs(driver, formvars)

        if errorDict['errors']:
            return json.dumps(errorDict);

        fileInfo = formvars.get("fileInfo", None)
        sourceName = formvars.get("sourceName", None)

        resourceName = None
        if fileInfo:
            dbFileName = formvars.get("dbFileName", None)
            basename, ext = os.path.splitext(dbFileName)

            # In the case of text files, destination is temp directory with
            # files of the same name.
            if driver.value == "Microsoft Access Text Driver":
                tmpdir = tempfile.TemporaryDirectory()
                resourceName = tmpdir.name
                f_handle = open(os.path.join(resourceName, dbFileName), 'wb')
                f = f_handle.fileno()  # We use a low-level copy routine
            else:
                (f, resourceName) = tempfile.mkstemp(ext)
            util.copy_stream(f, fileInfo)  # Low-level copy to target file
            os.close(f)


        connector = mediators.ODBC.Connector.GetConnector(formvars, resourceName)
        try:
            odbc_source = mediators.ODBC.source(connector)
        except Exception as e:
            #errorMessage = re.sub("[\(\[].*?[\)\]]", "", e.args[1])
            #errorDict['errors'] = [errorMessage.replace('FATAL: ', "")];
            errorDict['errors'] = [e.args[1]]
            return json.dumps(errorDict);

        cursor = odbc_source.db.cursor()
        tableColumnDict = odbc_source.get_tableColumnDict(cursor, driver)

        finalDict = {}

        # Is driver for a database management system (dbms)?
        dbms = not driver in (DriverType.Text, DriverType.Excel)
        if dbms:
            # Learn about any referential constraints that the dbms may have
            constraints = self.getForeignKeyRelationships(cursor, dbName, driver, sourceName)
            finalDict['Constraints'] = constraints  # right now this is a list of row dicts

        finalDict['Tables'] = {}
        ODBC_attrib = 3
        ODBC_type = 5
        for table in tableColumnDict:
            queryTable = f'`{table}`' if (' ' in table) or ("$" in table) else table

            q = f"SELECT * FROM {queryTable}"

            try:
                if rowNum != -1:
                    data = cursor.execute(q).fetchmany(rowNum)
                else:
                    data = cursor.execute(q)
            except Exception:
                continue

            # Examine the cursor for type information
            attrCol = 0
            typeCol = 1
            typeInfo = {}
            for d in cursor.description:
                attr, typ = d[attrCol], d[typeCol].__name__
                typeInfo[attr] = typ

            # Create a list of data rows as dicts keyed by column
            newTableList = []
            if len(data) == 0: #there is no data so we just report the column names
                d = {f: '' for f in tableColumnDict[table]}
                newTableList.append(d)
            else:
                for tup in data:
                    non_none_vals = [x for x in tup if x is not None]
                    if len(non_none_vals) > 0:
                        dataColDict = list(zip(tableColumnDict[table], tup))
                        d = {f: self.catch(v) for f, v in dataColDict}  # str(v).encode('utf-8').strip()
                        newTableList.append(d)

            # Clean up the table name if needed
            table_cln = table.replace("'", '')
            # Note type information and data about this table
            finalDict['Tables'][table_cln] = dict(types=typeInfo, data=newTableList)

        return json.dumps(finalDict)

    def catch(self, value):
        try:
            return str(value)
        except:
            return value.encode('utf-8').strip()

    def getManyTables(self,cTDict, constraints):
        # put something in here, start of overarching list?
        finalDict = {}
        tablesDict = {}
        for name in cTDict.keys():  # one of these for each table
            #tableDict = {}
            #if " " in name:
            #    name = name.replace(" ","\ ")
            q= f"SELECT * FROM `{name}`;"

            print(f"Q: {q}")
            table = self.db.execute(q)
            ll = list(table)  # [(220, u'Programming 1', 1, 8),
            if len(ll) > 0:

                 # for each table we want a [{"User ID":"Asimonis","Project":"CINMS","Deployment":"5","Site":"C","Effort Start":"39785","Effort End":"39872.18403","Parser":"SIO.SWAL.Detections.AutomaticClicks.v1","SpeciesAbbreviation":"NOAA.NMFS.v1","Method":"Teager Energy Detector","Software":"SDSU/SIO Click Detector","Version":"1","Details":"STS Threshold:10,STS Min Freq:10000,STS Max Freq:100000,STS Min Sat:10000,STS Max Sat:90000, HRC Click Select:click+resonances,HRC Gr Sep:0.5,HRC Gr Ln:2, HRC Frame Len:1200, HRC Tech:cepstra, HRC Norm:none"}]
                newTableList = []
                for tup in ll:
                    fieldsInOrder = cTDict[name]
                    d = {f:self.catch(v) for f,v in zip(fieldsInOrder,tup)} #str(v).encode('utf-8').strip()
                    newTableList.append(d)
                tablesDict[name] = newTableList
                #finalDict[name] = tableDict

        finalDict['Tables'] = tablesDict
        finalDict['Constraints'] = constraints  # right now this is a list of row dicts
        return json.dumps(finalDict)

class XmlToRepresentation:
    """
    Resource for converting XML to other representations
    """

    # We expect ISO 8061 datetime strings to be well behaved as
    # they should have been standardized on XML ingest.
    iso8601_re = re.compile(r'\d+-\d+-\d+T\d+:\d+:\d+(\.\d+)?Z')

    def __init__(self, db_server, r_interface:RInterface):
        """

        :param db_server:
        :param r_interface:  Object to communicate with R process
        """
        self.dbserver = db_server
        self.r_interface = r_interface
        # XML -> JSON transforms
        self.to_json = xmljson.BadgerFish(xml_fromstring=True)

    def _convertToET(self, xml_str, preserve_namespaces=True):
        """
        Convert a valid XML document to element tree
        :param xml_str: document to be converted
        :param preserve_namespaces:  keep/discard namespaces
        :return: element tree
        """

        et = ET.XML(xml_str)
        if not preserve_namespaces:
            # ccpizza's suggestion
            # https://stackoverflow.com/questions/18159221/
            #   remove-namespace-and-prefix-from-xml-in-python-using-lxml
            # process entire tree
            for elem in et.getiterator():
                # Only process elements that might have namespaces
                if not isinstance(
                        elem, (ET._Comment, ET._ProcessingInstruction)):
                    # Update tag with localname
                    elem.tag = ET.QName(elem).localname
            # Remove unused namespaces
            ET.cleanup_namespaces(et)

        return et

    def convertToJSON(self, xml_str):
        """
        Return a byte stream representing the XML data as JSON
        :param xml_str:  XML to be converted
        :return:
        """
        et = self._convertToET(xml_str, preserve_namespaces=False)
        json_dict = self.to_json.data(et)
        json_str = json.dumps(json_dict, indent=2)
        # convert to byte string and return
        return json_str.encode('utf-8')

    def convertToR(self, xml_str):
        """
        Return a byte stream representing the XML data as an R data.tree
        :param xml_str:
        :return:
        """

        if self.r_interface is None:
            raise ValueError("R is not available on server")

        result = self.r_interface.execute("data.tree", xml_str)
        return result

    def convertToMatlab(self, xml_str):
        # converts an xml string to matlab format
        et = ET.XML(xml_str)
        data = defaultdict(list)
        self.etreeToDict(et, data)
        self.etreeCleanDict(data)  # map singleton lists:  [item] --> item
        with BytesIO() as f:
            io.savemat(f, data)
            return f.getvalue()

    def buildNestedTuple(self, d):

        if isinstance(d, dict):
            out = ()  # each record is a tuple, tuples are nested if a value is itself another struc array
            for val in d.values():
                if isinstance(val,np.ndarray):
                    out += (val.item(0),)
                elif isinstance(val, list): # if there is a list it is a series of records in a struc array, e.g. kinds
                    for innerDict in val:  # dictionaries are always the items of a list
                        deeper = self.buildNestedTuple(innerDict)
                        out += (deeper)
        return out

    def recurseDict(self,d):
        # this is supposed to build a nested complex datatype for inclusion in the construction of a  numpy structured array
        ll = []
        print(f"recurse D {d}")
        if isinstance(d, list):
            for i in [d[0]]:  # BECAUSE WE ONLY NEED TO THIS ONCE
                if isinstance(i, dict):
                    for k, v in i.items():
                        if isinstance(v, np.ndarray):
                            ll.append((k, v.dtype))
                        else:  # couldn't this be either another dict or a list,
                            ll.append((k, self.recurseDict(v)))
                # what if there are numpy arrays in the list?
        elif isinstance(d, dict):
            for k, v in d.items():
                if isinstance(v, np.ndarray):
                    ll.append((k, v.dtype))
                else:  # couldn't this be either another dict or a list,
                    ll.append((k, self.recurseDict(v)))
        return ll

    def etreeToDict(self, ele, d):
        """
        :param ele:  current element
        :return: dict
        """

        lname = ET.QName(ele).localname  # strip namespace

        self.etreeAttrib(ele, lname, d)  # process attributes (if any)

        # While XML nodes can have children and text,
        # Tethys schemata never mix these.
        # We either have child nodes or values
        is_interior = len(list(ele)) > 0
        if is_interior:
            self.etreeInteriorNode(ele, lname, d)
        else:
            self.etreeLeafNode(ele, lname, d)  # insert leaf values into dict

        return

    def etreeInteriorNode(self, ele, lname, d):
        children = list(ele)
        subdict = defaultdict(list)
        for child in children:
            is_interior = len(list(child)) > 0
            if is_interior:
                self.etreeToDict(child, subdict)
            else:
                lchild = ET.QName(child).localname
                self.etreeAttrib(child, lchild, subdict)
                self.etreeLeafNode(child, lchild, subdict)

        d[lname].append(subdict)

    def etreeLeafNode(self, ele, lname, d):
        if ele.text:
            text = ele.text
            text_ary = text.split()  # make array at whitespace
            parsed = False

            if len(text_ary) > 0:
                # Try to coerce the list to numbers or datetimes
                try:
                    float_ary = list(map(float, text_ary))  # Number?
                    d[lname].append(np.array(float_ary))
                    parsed = True
                except ValueError:
                    pass

                try:
                    time_ary = list(map(self.toMatlabSerialDate, text_ary))
                    d[lname].append(np.array(time_ary))
                    parsed = True
                except Exception:
                    pass

            if not parsed:
                    # use original text as string
                    d[lname].append(np.array([text], dtype=object))

    def etreeAttrib(self, ele, lname, d):
        if ele.attrib:
            attr_key = f"{lname}_attr"
            if not attr_key in d:
                d[attr_key] = defaultdict(list)

            for k, v in list(ele.attrib.items()):
                lattr = ET.QName(k).localname  # strip namespace
                # Append attr as float/text
                try:
                    val = float(v)
                    d[attr_key][lattr].append(val)
                except ValueError:
                    d[attr_key][lattr].append(v)

    def etreeCleanDict(self, d):
        for k, v in d.items():
            if isinstance(v, list):
                # filter children
                for idx in range(len(v)):
                    if isinstance(v[idx], dict):
                        self.etreeCleanDict(v[idx])

                if len(v) == 1:
                    d[k] = v[0]  # Remove list for singletons
            elif isinstance(v, dict):
                self.etreeCleanDict(v)

    h_per_day = 24.0
    s_per_day = h_per_day * 3600.0
    us_per_s = 1000000.0  # microseconds per s
    us_per_day = us_per_s * s_per_day
    def toMatlabSerialDate(self, date_str):
        """
        Given an ISO-8601 datetime, convert to a Matlab serial date
        :param date_str: ISO 8601 date and time string
        :return: Matlab floating point serial datetime
        Raises ValueError when date_str not ISO 8601
        """
        # Quick check to avoid overhead associated with Maya parsing when
        # it is not needed
        if not isinstance(date_str, str):
            raise ValueError("Not a datetime")
        m = self.iso8601_re.match(date_str)
        if m is None:
            raise ValueError("Not a datetime")
        # Probably a datetime, give it a go...
        dt = maya.when(date_str).datetime(naive=True)
        mdn = dt + datetime.timedelta(days=366)
        # Matlab counts days past epoch, one per day.  Compute fractional
        # day in seconds (s) and microseconds (us)
        s_past_midnight = \
            (dt - datetime.datetime(dt.year, dt.month, dt.day, 0, 0, 0)).seconds
        frac_s = s_past_midnight / self.s_per_day
        frac_us = dt.microsecond / self.us_per_day
        newTime = mdn.toordinal() + frac_s + frac_us

        return newTime

class Convert:
    exposed = True

    def __init__(self, db_server, r_interface:RInterface):
        """

        :param db_server:
        :param r_interface:  Object to communicate with R process
        """
        self.dbserver = db_server
        self.xml2other = XmlToRepresentation(db_server, r_interface)

    def get_xml_converter(self):
        """
        :return:  Expose underlying conversion object
        """
        return self.xml2other

    def GET(self, *args, **kwargs):
        """
        :param args: /docID/format  Valid formats: Matlab/R/XML/JSON
        :param kwargs:
        :return: returns converted file
        """
        # check len of args, kwargs doesn't get used in this request
        if len(args) != 2:
            raise ValueError("Expected ../documentID/{format to convert to}")

        docId = args[0]
        convertTo = args[1].lower()
        try:
            # doc = self.dbserver.getMethods().get_document('mediator_cache', docId)
            semaphores[docId].acquire()
            filename = os.path.join(resources.names['ResultXML'],
                                    docId + ".xml")
            fileHandle = open(filename, 'rt')
            doc = fileHandle.read()
            semaphores[docId].release()
        except:
            raise cherrypy.HTTPError(codes["Not Found"], "No such resource")
        # convert
        try:
            if convertTo == 'matlab':
                file = self.xml2other.convertToMatlab(doc)
            elif convertTo == "r":
                file = self.xml2other.convertToR(doc)
            elif convertTo == "xml":
                file = doc  # No conversion needed
            elif convertTo == "json":
                file = self.xml2other.convertToJSON(doc),
        except Exception as e:
            file = str(e)  # just a test
        return file

class Attachment:
    """
    Class for handling retrieval of attachments associated with documents
    Attachments must be referenced by document name, the attachment type
    and the attachment name.
    """

    def __init__(self):
        pass

    def get_attachment(self, collection, docid, type, name):
        """
        :param collection:  document collection
        :param docid: document identifier, this is the last part of the
           URI for the document.  It is frequently the same as the Id
           although not necessarily so.
        :param type: attachment type:  Image, Audio, Bespoke
        :param name: name of attachment as specified in document
        :return: Returns image, audio, or bespoke data

        Note:  Prior to the introduction of bespoke data which can be
        of any type, type was meaningful.  Now this parameter is ignored
        and we assume that all attachments have unique names regardless of
        their data type.  We still look in type-specific directories as some
        historical data may be stored with the hierarchical system.
        """

        sourcedir = os.path.join(resources.names["source-docs"], collection)

        """
        We assume that all attachments have unique names within a document 
        or that if there are duplicates, they refer to the same file.
        
        Attachments can be stored in a number of places in the filesystem
        (This is due to how we thought about this over time.)

        Everything is based on the document Id or for older attachments
        possibly the document identifier which is not necessarily the same
        as the Id.  We will refer to this as the id.
        
        We search in the following places in source-docs:
        directories collection/id-attach/type collection/id-type
        zip files:  collection/id-attach.zip collection/id-type-attach.zip
        
        For each of these, we look for the specified resource.
        Within a zip file, we look for the resource regardless of where
        in the file hierarchy it is.
        
        As we do not expect to mix the different types of storage that
        have been used over the years, once we find one type of storage
        we stop looking.
        """

        data = None
        mimetype = None

        # Look for files in directories
        for fname in [os.path.join(sourcedir, docid + "-attach", type, name),
            os.path.join(sourcedir, docid + "-" + type, name)]:
            if os.path.exists(fname):
                mimetype, _ = mimetypes.guess_type(fname)
                attach = open(fname, 'rb')
                data = attach.read()
                break
                
        if data is None:
            # See if in an archive
            # Archives to search and expected subdirectory within each
            archives = [os.path.join(sourcedir, f"{docid}-attach.zip"),
                    os.path.join(sourcedir, f"{docid}-{type}-attach.zip")]
            archive_idx = 0
            archive_dirs = [type, ""]  # subdirectory within archive
            while data is None and archive_idx < len(archives):
                # open the archive
                try:
                    archive = zipfile.ZipFile(archives[archive_idx])
                except FileNotFoundError:
                    archive_idx = archive_idx + 1
                    continue

                # search for the file within the archive
                dir_idx = 0
                while data is None and dir_idx < len(archive_dirs):
                    if archive_dirs[dir_idx] != "":
                        # don't use format string as may contain special chars
                        fname = archive_dirs[dir_idx] + "/" + name
                    else:
                        fname = name

                    try:
                        data = archive.read(fname)
                        mimetype, _ = mimetypes.guess_type(fname)
                    except KeyError:
                        dir_idx = dir_idx + 1

                archive_idx = archive_idx + 1

        return mimetype, data

    exposed = True

    def GET(self, *args, **kwargs):
        """GET Attach - Expecting argument of form:  Collection/DocName/Type?Item
        Collection must be a valid collection
        DocName is the base of the document URI.  Sample URL for retrieving
        an image from Detections document ALEUT02BD_MF_MFAOrca_ajc using a 
        locally hosted server running on port 9779:
        http://localhost:9779/Attach/Detections/ALEUT02BD_MF_MFAOrca_ajc?Image=Other-ALEUT2BD-20101214T172847.jpg
        (Note that the document and attachment must exist)
        """

        if len(args) != 2:
            raise ValueError("Expected collection/documentID")

        if len(kwargs) == 0:
            cherrypy.HTTPError(codes["Forbidden"],
                "Expected collection/documentID?Image=ImageFileName," +
                "Audio=AudioFileName,...")
        else:
            if len(kwargs) > 1:
                cherrypy.HTTPError(
                    codes["Forbidden"], "Multiple attachment not supported")
            else:
                for atype, name in list(kwargs.items()):
                    mimetype, value = \
                        self.get_attachment(args[0], args[1], atype, name)
                    if value is None:
                        raise cherrypy.HTTPError(
                            codes["Bad Request"],
                            f"No such attachment: {args[1]}/{atype}/{name}")
                cherrypy.response.headers['Content-Type'] = mimetype
                return value

    def PUT(self, *args, **kwargs):
        raise cherrypy.HTTPError(codes["Forbidden"],
            "Attachment data can only be submitted with a document")

    def POST(self, *args, **kwargs):
        raise cherrypy.HTTPError(codes["Forbidden"],
            "Attachment data can only be submitted with a document")



class Collection:
    # Match quoted string
    inquotes_re = re.compile('^"(?P<string>.*)"$')
    exposed = True

    def __init__(self, dbserver, collection):
        self.dbserver = dbserver
        self.collection = collection
        self.xquery = dbserver.getMethods().query

    def DELETE(self, *path, **kwargs):
        """DELETE - Remove one or more documents from collection.
        DocId is the last part of the URI for the document.  If the
        name is unknown, the URI can be accessed by using the base-uri()
        function in an XQuery and removing the prefix.  One or more DocIds
        can be specified in a ; separated list.  If the DocId is the special
        value, "<*clear*>", all XML documents will be removed from the
        collection.
        """
        rmall = "<*clear*>"
        #if kwargs.has_key("DocId"):
        #     docname = kwargs["DocId"].encode('utf-8')

        print(f"{kwargs} {path}")
        doclist = kwargs.get("DocId", None)
        if doclist:
            doclist = doclist.split(';')
            if rmall in doclist:
                # clearing container
                op = 0  # specifier for ContainerWorker, 0 is clear
                log = Sink()
                methods = self.dbserver.getMethods()
                retval = None
                worker = ContainerWorker(methods, self.collection, op,
                                         sink=log)
                try:
                    # Perhaps need to add some logging here
                    # logfile = worker.getLogFile()
                    worker.start()
                    if worker.logging:  # implies large container: det,local,itis
                        # Report.rebuild_report(log, logfile, "Clearing")
                        retval = "\n".join([
                            "<DELETE>",
                            "  <ITEM> Clearing in Progress. "
                            f'Query count(collection("{self.collection}") to '
                            "verify completion </ITEM>",
                            "</DELETE>"])
                    else:
                        # small container, wait for worker to join us
                        worker.join()
                        retval = "\n".join([
                            "<DELETE>",
                            "  <ITEM> All cleared </ITEM>",
                            "</DELETE>"])
                except Exception as e:
                    retval = str(e)
                    # We do not raise a cherrypy.HTTPError(400, result)
                    # as this will escape our XML
                    cherrypy.response.status = codes["Bad Request"]

                return retval

            else:
                # copy this for logging
                successes = []
                failures = []
                for doc in doclist:
                    try:
                        # Remove the document.  Don't bother clearing the
                        # query cache until all documents are processed
                        self.dbserver.methods.remove_document(
                            self.collection, doc, clear_cache=False)
                        successes.append(doc)
                    except Exception as e:
                        failures.append(e.what)

                # Only raise an error if nothing could be deleted
                raiseerror = len(successes) == 0 and len(failures) > 0

                retval = ""
                if len(successes) > 0:
                    # Clear out stale cache entries
                    self.dbserver.methods.query_cache_clear(self.collection)

                    succstr = "\n".join(["  <ITEM> %s </ITEM>"%(successes)])
                    retval = "\n".join(["<DELETE>", succstr, "</DELETE>"])

                if len(failures) > 0:
                    failstr = "\n".join(["  <ITEM> %s </ITEM>"%(failures)])
                    retval = retval + "\n".join(["<FAILED>", failstr, "<FAILED>"])

                if raiseerror:
                    raise cherrypy.HTTPError(410, retval)

            return retval
        else:
            raise cherrypy.HTTPError(codes["Bad Request"],
                                     "DocId parameter is mandatory")


    def GET(self, *path, **kwargs):
        """GET - Retrieve documents
           Path arguments are treated as an XPath expression and may be
           used to query the database.

           In the XPath expression:
           The Tethys libraries are bound to namespace abbreviation lib.
           The Tethys schema are bound to namespace abbreviation ty.

           Caveats:  XPath uses /, it will need to be encoded (%2F)
           Example:
           http:localhost:9779//Detections/Detections[DataSource%2FProject = "SOCAL" and DataSource%2FSite = "M" and DataSource%2FDeployment= 34]

           Keyword arguments are passed in the URL with ?Var, or ?Var=value patterns.
           Supported keywords:
           DocId - Return a specific document based on the document's
              metadata name.  This is usually the name of the file that was
              submitted without the extension, although it need not be.
              Example:  http:localhost:9779//Detections/?DocId=SOCAL33M_LF_Sean
              When DocId is used, any XPath specifications are ignored and the
              whole document is returned.

           Indices - Report indices used on the container

           format={XML|HTML}
               Specified representation format for document docId request:
               XML - Return XML document (default)
               HTML - Return a version of the document suitable for display
                  in a web page.  Document will be formatted.
               Note:  Only applies when DocId is specified.

           In the absence of path and keyword arguments, HTML showing the names
           of each document and a hyperlink to retrieve web pages of the
           documents is returned.
        """

        if "DocId" in kwargs:
            docname = kwargs["DocId"]
            result = self.dbserver.methods.get_document(self.collection, docname)
            if result == "notfound":
                cherrypy.response.status = codes["Not Found"]
                result = f"Unable to find document {docname}"
            else:
                format = kwargs.get('format', 'XML')
                if format == 'XML':
                    cherrypy.response.headers["ContentType"] = "text/xml"
                if format == 'HTML':
                    cherrypy.response.headers["ContentType"] = "text/html"
                    io_str = StringIO()
                    headers = [
                        '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" ' +
                           '"http://www.w3.org/TR/html4/strict.dtd">',
                    '<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">',
                    f'  <HEAD><TITLE>Document {docname} in collection {self.collection}</TITLE></HEAD>',
                    "   <BODY>",
                    f"    <H1>Document {docname} in {self.collection}</H1>",
                    "The XML below may not correspond to its original form.",
                    "It has been formatted to be easier to read.",
                    "     <H1>Document Content</H1>"
                    ]
                    io_str.write("\n".join(headers))

                    # Strip XML declaration if present (remove encoding string)
                    result = strip_xml_encoding_declaration(result)
                    # In theory this should not fail as we should only
                    # accept valid XML into the database
                    try:
                        tree = ET.fromstring(result)
                    except ET.ParseError as e:
                        line, col = e.position
                        # We might get the encoding wrong here, the whole point of using
                        # element tree is to process the encoding properly, but the
                        # document was mal-formed.
                        src = str(self.sourcedoc, "UTF-8")
                        lines = dbxmlInterface.error.show_context(src, line,
                                                                  col,
                                                                  highlight=True)
                        lines.insert(0, f"Unable to parse XML document {docname} in collection {self.collection}: {e.msg}")
                        e.msg = "\n".join(lines)
                        raise

                    # pretty print and reconstruct
                    ET.indent(tree, space="  ", level=0)
                    formatted = ET.tostring(tree, encoding="unicode")

                    io_str.write('<pre lang="XML">\n')
                    io_str.write(html.escape(formatted))
                    io_str.write('\n</pre>')
                    io_str.write("   </BODY>\n</html>\n")
                    result = io_str.getvalue()

        elif "Indices" in kwargs:
            result = self.indices(None)  # Report indices
        else:
            namespaces = "\n".join([
                "import schema namespace ty='http://tethys.sdsu.edu/schema/1.0' at 'tethys.xsd';",
                "import module namespace lib='http://tethys.sdsu.edu/XQueryFns' at 'Tethys.xq';",
                ""])
            if len(path) > 0:
                # Assume user specified an XPath query, construct XQuery
                # from XPath specification and run
                querystr = namespaces + \
                   f"collection('{self.collection}')/{'/'.join(path)}"
                result = self.xquery(querystr)
            else:
                # No arguments, list all document names
                href = cherrypy.url()
                qryStr = f"""
                let $n := count(collection("{self.collection}"))  
	            return 
	            <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
                  <title>{self.collection} document list</title>
                  <body>
                    <H1>Collection {self.collection} has {{$n}} documents</H1>
                    <ol>
                    {{
                      if ($n > 0) then (
                        for $doc in collection("{self.collection}")
                        let $docname := replace(base-uri($doc), 'dbxml:///{self.collection}/', '')
                        order by $docname
                        return 
                          <li>  
                            <a target='_blank' href="{href}?DocId={{$docname}}&amp;format=HTML">{{$docname}}</a>
                            or raw <a target='_blank' href="{href}?DocId={{$docname}}&amp;format=XML">XML</a>
                          </li> 
                      ) else ()
                    }}
                    </ol>
                  </body>
                </html>"""
                result = self.xquery(qryStr)

            if not result:
                cherrypy.response.status=codes["Bad Request"]
                result = "\n".join(["Unable to interpret request",
                    "Example detection document retrieval format:\n",
                    "http://SERVER:PORT//Detections/?DocId=Explosion counts SOCAL49H\n",
                    "Example detection data retrieval format:\n(%2reF is encoded '/' character)",
                    "http://SERVER:PORT//Detections/Detections[DataSource%2FPDeploymentId = ""SOCAL38-M""]"])

        return result

    def PUT(self, *path, **kwargs):
        raise cherrypy.HTTPError(codes["Forbidden"], "Documents are added as attachments to POST")

    def POST(self, *args, **formvars):
        """
        POST - Add or update documents to/in a collection.
        There are three types of additions/updates that are defined by
        the path argument and form variables that are passed.
        
        Note that we do not use the traditional PUT for adding a new document
        as multipart bodies are not allowed.

        The following methods are still supported (for now), but deprecated.
        They are likely to be removed in a future release.

        *** These two methods are DEPRECATED, use /import instead ***
        /add - Add or update an item
        /ODBC - Add or update from a database connection
        *************************************************************

        /rebuild
        Rebuild a collection from source data
        This is to be used for rebuilding the database from the source
        documents stored on the server.
        Note that we do not verify attachments during rebuilds as they should
        have already been verified.
            Variable    Semantics
            update      '1'- Update documents even if they already exist
                        '0' (default) - Only add documents not in a collection
            clear       '1' - Remove all documents from collection before
                            rebuilding.
                        '0' (default) - Don't clear documents before rebuilding.                        

        /reindex - Set index to default indices and rebuild the index
            We recommend backup of the database before reindexing.

        /import
        Add items from one or more ODBC data sources.  ODBC drivers are required,
        see the Tethys manual for details.  To see a list of ODBC drivers
        currently available on a Tethys server, perform a GET on:
            server:port/ODBC
        This can also be typed into a browser window.

        Import expects the following body parts:

        specification - File containing XML that specifies the how
           data will be processed.  XML specification:
        <?xml version="1.0" encoding="UTF-8"?>
        <import>
            <!-- Name that will be used for the associated XML document.
            If more than one document is generated, sequential numbers
            will be assigned, e.g. name1, name2, ...
            If absent, basename of first source will be used, e.g.
            if source is SOCAL32M-HF-SBP.xlsx, use SOCAL32M-HF-SBP.
            -->
            <docname>name</docname> 
            <!-- Replace an existing document? -->
            <overwrite>true|false</overwrite>
            <!-- For documents that refer to species, how are species
                declared:
                    TSN - ITIS taxonomic serial number (default is absent)
                    Latin - scientific name of taxon
                    abbreviation map name - Name of mapping in collection
                        SpeciesAbbreviations
            -->
            <species_map>TSN</species_map>
            <!-- Name of translation map to use, must already by on server -->
            <import_map>MAPNAME<import_map>
            <sources>
                <source>
                    <!-- What is being sent?
                    file - A file in the multipart body attachment.  It is
                    assumed that the file type can be derived from the file
                    extension and an appropriate ODBC connection string will
                    be generated by the server.
                    resource - A resource accessible by ODBC on the remote
                    server.  
                    -->
                    <type>file|resource</type>
                    <!-- filenames must be included to reference the actual file of interest -->
                    <file>meta.xls</file>
                    <!-- a nickname for this source, to be used in the @source attribute in the SourceMap-->
                    <name>metadata</name>
                    <!-- ODBC connection string.
                    Optional for type file.  If specified, remote system
                        will not attempt to guess the connection string.
                    Required for type resource.
                    -->
                    <connectionstring></connectionstring>
                </source>
                <!-- Repeat source as needed.
                    Name elements must have unique values -->
            </sources>
        </import>

        SourceFiles. - One attachment for each file source using the
             names specified in the <sources/source/name>.
             Mimetype must be specified.
             Example:  <name>boobear.xls</name> would expect a 
             boobear.xls bodypart.
        Attachment - File that is to be associated with this document,
             the mimetype should be specified.  Attachments may only be 
             specified when a single document is being added.  Each 
             attachment is expected to be referenced by name within the data.  
             Multiple attachments may be handled in one of two ways:
          1. Repeat Attachment label for mutliple attachments.  Some http clients
             do not allow repeated label names.  If so, adding a number:
             Attachment1, Attachment2, ... is also acceptable.
          2. Create a zip archive of file attachments and attach it.
             Filepaths must match the references.

        Currently, attachments are only available for detection documents, 
        and are typically used to show an image (e.g. spectrogram) or a short 
        clip of audio data.  Resources that expect attachments will verify 
        submissions and report any missing attachments.  This can be used 
        as a strategy to avoid having to parse the document being submitted 
        on the client side.  In our document submission client, we submit 
        a document, check to see if missing attachments are the only 
        problem, and if they are prepare a new submission with the 
        needed files. This requires the document to be transmitted 
        twice, but saves the additional burden of writing parsing tools 
        for anything but the XML that is returned from the server.

        Important notes for disaster recovery.  When only simple files 
        (comma separated values, spreadsheets, xml) are sent, the source 
        documents will be saved on the server, and the server's rebuild
        commands will be able to reconstruct the database if it is lost 
        or corrupted.  When more complicated resources are used 
        (e.g. a database), source documents are not stored as multiple 
        snapshots of a database are neither practical nor desirable. 


        """

        ops = {
               # Build list of methods to invoke for each operation
               # that is specified immediately after the resource
               # name by args[0]
               "add": self.__add__,
               "rebuild": self.__rebuild__,
               "ODBC": self.__ODBC__,
               "import": self.__import_sources__,
               "reindex": self.__reindex__
               }

        # Convert booleans appropriately
        boolmap = {'0': False, '1': True}
        for k in ("overwrite",):
            v = formvars.get(k, None)
            if v:
                try:
                    formvars[k] = boolmap[v]
                except KeyError:
                    raise ValueError("%s must contain 0 or 1"%(k))

        # If the server is threaded, we need to initialize Win32 com
        # We don't really need to do this for every method, but we
        # will take the performance hit and not have to worry about 
        # which methods require it or not.  It is only needed when
        # using Windows Com models
        if platform.system() == 'Windows':
            if threading.currentThread().getName() != 'MainThread':
                pythoncom.CoInitialize()   # @UndefinedVariable (generated at runtime)

        # Files are either passed in the formvars or in the request's
        # body parts (unclear what triggers processing either way).  In
        # both cases, the take the cherrypy._cpereqbody.Part object and
        # pull out the filename, content type and file handles.

        # Expand any MIME attachments so that the formvars contains usable info
        # Anything starting with Attachment receives special treatment.
        # We do not process the _cpereqbody.Part for attachments
        keys = list(formvars.keys())
        attachment_keys = [k for k in keys if k.startswith("Attachment")]
        if len(attachment_keys) > 0:
            if "Attachment" in attachment_keys:
                # Ensure Attachment is a list
                if not isinstance(formvars["Attachment"], list):
                    formvars["Attachment"] = [formvars["Attachment"]]
            else:
                formvars["Attachment"] = []  # create

            for k in attachment_keys:
                if k == "Attachment":
                    continue  # Already processed
                else:
                    v = formvars[k]
                    if isinstance(v, list):
                        v = list(v)
                    formvars[k].extend(v)
                    del formvars[k]  # no longer needed, merged.
        keys = list(formvars.keys())
        for k in keys:
            if k == "Attachment":
                continue
            try:
                self.__process_body(formvars, formvars[k])
            except AttributeError:
                pass

        # Handle body parts, these are merged into formvars
        for part in cherrypy.request.body.parts:
            try:
                self.__process_body(formvars, part)
            except AttributeError:
                continue    # Not a file attachment

        # Invoke the proper insertion routine
        if len(args) != 1 or args[0] not in list(ops.keys()):
            raise cherrypy.HTTPError(codes["Forbidden"],
                "POST must specify an operation:  %s"%(",".join(list(ops.keys()))))
        else:
            result = ops[args[0]](formvars)
        return result

    def __process_body(self, formvars, part):
        """Process a body in a multipart message. A file_tuple
        will be added to a list in formvars:
        (filename, mimetype, filehandle)
        The key of formvars is decided by name or the name
        field in the body part.  If formvars does not already
        contain a list, the contents will be replaced with
        a list containing the file_tuple.   
        
        formvars - dictionary of form variables
        part - bodypart, will throw an attribute error
            if part is not a body.
        name - Name to use when storing to """

        fileH = part.file
        filename = part.filename
        mimetype = part.content_type.value

        name = part.name
        if name is None:
            # No name specified, search content-disposition
            # for a name variable

            # Build content-disposition dictionary
            # List of form "attribute"=value ; "attr2"=val2 ; ...
            contentlist = part.headers['Content-Disposition'].split("; ")
            contentpairs = [item.split("=") for item in contentlist]
            contentdict = dict()

            for pair in contentpairs:
                if len(pair) == 2:
                    for idx in range(len(pair)):
                        m = self.inquotes_re.match(pair[idx])
                        if m is not None:
                            pair[idx] = m.group('string')
                    contentdict[pair[0]] = pair[1]

            name = contentdict['name']

        # Update formvars
        file_tuple = (filename, mimetype, fileH)
        try:
            formvars[name].append(file_tuple)
        except (KeyError, AttributeError):
            # Either not a list or name does not exist
            formvars[name] = [file_tuple]


    def __add__(self, formvars):
        """
        add
        This method is deprecated, do not use it for new code.

        :param formvars:  Form variables that control the input.
           See the Tethys RESTful import specification for details
           or below.
        :return:  Success/Failure message

        Form variables:
            Variable    Semantics
            data
                    Data to be added, e.g. spreadsheet or XML
                    Information is a list of tuples, exactly one of which is expected:
                    (filename, mimetype, open file handle).  The mimetype is
                    used to determine the type of document that is to be
                    processed.  We can process Excel spreadsheets, CSV, and
                    XML via this mechanism.
                    The document name is derived from the
                    filename without extension, and this must be unqiue
                    to a collection.
            overwrite
                    '0' (default) - do not overwrite existing documents
                    '1' - overwrite existing documents
            Attachment
                    Ancillary information about the data to be added
                    When this is used, it is assumed that a single document
                    is to be added (the data field does not represent
                    multiple documents) and all attachments are associated
                    with this data source.
                    Information is a list of files similar to data.
                    Some clients may not support multiple instances of the same
                    keyword.  In such cases, they may use AttachmentN where N
                    is a unique integer.
            import_map
                    Indicates import translation map to use.
                    Required if a map is needed and one is not specified
                    in the document to be added.  XML documents do not need
                    an import map, and workbooks (Excel) may specify an
                    import map via the Parser field on the MetaData sheet.
            species_abbr
                    Indicates species translation map for translating species
                    names to ITIS codes.
                    Not required for collections that do not have species
                    names or when workbooks specify this in their body
                    via a SpeciesAbbreviation field on a MetaData sheet.

                    *** XML documents are expected to have the ITIS codes and this
                    form variable has no effect on them. ***
        """

        formvars.setdefault('overwrite', False)  # no overwrite if not set

        # Verify the user passed in the proper data
        try:
            data = formvars["data"]
            if not isinstance(data, list):
                data = [data]
                formvars["data"] = data
        except KeyError:
            raise cherrypy.HTTPError(codes["Forbidden"], 'POST multipart message missing "data" element')

        if len(data) != 1:
            raise cherrypy.HTTPError(codes["Forbidden"], "POST has more than one 'data' form element")

        methods = self.dbserver.getMethods()

        result = None
        try:
            result = methods.post_data(self.collection, formvars, 'file')
        except MemoryError as m:
            result = "MemoryError:"+str(m)
        except Exception as e:
            result = str(e)
            # We do not raise a cherrypy.HTTPError(400, result)
            # as this will escape our XML
            cherrypy.response.status = codes["Bad Request"]
        return result

    def __rebuild__(self, formvars):
        """
        Reconstruct a collection from source documents
        :param formvars:  dictionary controlling behavior
           update -  0 don't update existing, 1 update
           clear - 0 don't remove documents before rebuild, 1 remove
        :return:  report of actions
        """

        # default XML sink.
        # Will get replaced by a LogSink if rebuilding Detections/Localizations
        sink = Sink()
        update_existing = formvars.get('update', '0') == '1'
        clear_existing = formvars.get('clear', '0') == '1'
        op = 1 #specifier for ContainerWorker, 1 is rebuild

        methods = self.dbserver.getMethods()
        result = None

        # Get the URL used by the requester so that we can tell them how to 
        # access the logs
        match = server_re.match(cherrypy.url())
        if match:
            schemehostport = match.group('url')
        else:
            schemehostport = "http://unknown_server:unknown_port/"

        worker = ContainerWorker(methods, self.collection,op, update_existing,
                                 clear_existing, sink, schemehostport)
        try:
            logfile = worker.getLogFile()
            worker.start()
            if worker.logging:
                Report.rebuild_report(sink, logfile, "Rebuild",
                                      serverURL=schemehostport)
            if self.collection not in ['Detections','Localizations']:
                #must be a quick rebuild , wait for the worker to join us
                worker.join()
            result = sink.finalize()
        except Exception as e:
            result = str(e)
            # We do not raise a cherrypy.HTTPError(400, result)
            # as this will escape our XML
            cherrypy.response.status = codes["Bad Request"]
        return result

    def indices(self, formvars):
        """
        indices - Report the indices used on this container
        :param formvars:
        :return:
        """

        container = self.dbserver.getMethods().access_container(self.collection)
        indices = Indices(container, self.dbserver.getMethods())

        indices_str = indices.get_indices_str()
        return f"<Indices>\n{indices_str}</Indices>"

    def __reindex__(self, formvars):
        """
        reindex - Set container variables to defaults and reindex
        :param formvars:  Form variables, this is part of the generic
           interface to collections and is not used for reindex
        :return:
        """

        timer = Timer()
        container = self.dbserver.getMethods().access_container(self.collection)
        indices = Indices(container, self.dbserver.getMethods())
        print(f"Indices of {self.collection} prior to reindexing")
        indices.report_indices()
        print(f"\nResetting indices of collection {self.collection} to defaults")
        indices.clear_indices()
        print(f"\nIndices of collection {self.collection} after reset")
        indices.report_indices()
        print("\nAdding in defaults")
        indices.add_defaults()
        print("\nCurrent index set")
        indices.report_indices()
        # indices.report_indices()
        indices.apply()  # Apply these indices to the container
        indices.reindex()
        return f"Collection {self.collection} has been reindexed in {timer.elapsed_min():,.2f} min"

    def __import_sources__(self, formvars):
        methods = self.dbserver.getMethods()
        result = None
        ##TODO source names
        # Anonymous function for creating an element
        elem = lambda tag, s, indent=0: " "*indent + "<%s>%s</%s>\n"%(tag, s, tag)

        errors = Sink(indented=True)
        errors.xml.startDocument()
        errors.xml.startElement("Errors")

        spec = dict()

        # specification form variable tells us which sources
        # are to be used, how they are to be imported, the
        # import map to be used and other items
        # We won't go far if we don't have this.
        if "specification" in formvars:
            specxml = formvars["specification"]
            # We already have this in unicode, so no need for an <?xml encoding="" string>
            specxml = strip_xml_encoding_declaration(specxml)
        else:
            errors.elementText("specification",
                               "No import specification present")
            cherrypy.response.status = codes["Bad Request"]
            return errors.finalize()

        # Parse the import specification
        try:
            import_opts = pd.read_xml(specxml, xpath="/import")
            if 'sources' in import_opts.columns:
                sources = pd.read_xml(specxml, xpath="/import/sources/source")
        except Exception as e:
            errors.elementText("specification", "Unable to parse import specification")
            errors.elementText("code", specxml)
            cherrypy.response.status = codes["Bad Request"]
            return errors.finalize()

        # overwrite existing?  Translation maps for species & source -> xml?
        fields = ['overwrite', 'species_map', 'source_map']
        for fld in fields:
            if fld in import_opts.columns:
                value = import_opts[fld][0]
                if isinstance(value, str):
                    value = value.strip() # strip whitespace from strings
                spec[fld] = value

        # Process sources
        source_list = []
        sourcesN = len(sources.index)
        if sourcesN == 0:
            errors.elementText("sources", "No source specified")
            cherrypy.response.status = codes["Bad Request"]
            return errors.finalize()
        else:
            if sourcesN > 1 and sources.name.isnull().values.any():
                errors.elementText(
                    "sources",
                    "Each source must have a name when "
                    "multiple sources are specified")
                cherrypy.response.status = codes["Bad Request"]
                return errors.finalize()
            for s_idx in range(sourcesN):
                source = dict()
                for field in ["type", "file", "name", "connectionstring"]:
                    if field in sources.columns:
                        value = sources[field][s_idx]
                        if isinstance(value, str):
                            value.strip()
                        source[field] = value

                try:
                    odbc = pd.read_xml(specxml, xpath=f"/import/sources/source[{s_idx+1}]/odbc")
                except ValueError:
                    odbc = None
                if odbc is not None and len(odbc) > 0:
                    source["odbc"] = dict()
                    for fld in odbc.columns:
                        source['odbc'][fld] = odbc[fld][0]
                        if isinstance(odbc[fld], str):
                            source['odbc'][fld] = odbc[fld].strip()

                source_list.append(source)

            spec['sources'] = source_list  # add sources to dict

        # The document name is important for file resources as it is used
        # to construct the document name associated with the XML document
        # and must be unique as we save the document to enable rebuilding
        # the database in case the database fails.
        if 'docname' in import_opts:
            spec['docname'] = import_opts['docname'][0].strip()
        elif sourcesN > 0:
            # If file resource, try to derive
            file_sources = sources[sources.type == 'file']
            if len(file_sources) and 'file' in file_sources.columns:
                # Use the name of the first file (w/o extension) that we find
                files = file_sources.file[file_sources.file.notnull()]
                if len(files):
                    spec['docname'] = os.path.splitext(files[0])[0]
                else:
                    errors.xml.newline()
                    errors.elementText("ImportSpecification",
                        "docname must be specified when the first source is not"
                        " a unique file to be submitted to Tethys")
                    errors.xml.newline()
                    cherrypy.response.status = codes["Bad Request"]
                    return errors.finalize()

        try:
            result = methods.import_data(self.collection, spec, formvars)
        except MemoryError as m:
            result = "MemoryError:"+str(m)
        except ValueError as e:
            # Something was wrong with the import, the exception
            # should carry information on the problem.
            result = str(e)
            cherrypy.response.status = codes["Bad Request"]
            return result
        except Exception as e:
            # This is an unexpected exception, get a stack
            # backtrace to make it easier to understand
            result = exception_handling.get_traceback_str(e)
            # We do not raise a cherrypy.HTTPError(400, result)
            # as this will escape our XML
            cherrypy.response.status = codes["Bad Request"]

        return result


    def __ODBC__(self, formvars):
        """
        Import via open database connectivity (ODBC)
        Deprecated, superseded by __import__ method.

        :param formvars:  Dictionary of form variables, see below
        :return:  String indicating success/failure

            Variable        Semantics

            data
                    When present, must contain a document that will be
                    processed by the server such as an Excel spreadsheet,
                    XML document, comma separated value file, Access
                    database, etc.  Caller is expected to set the mime
                    type for the file.

                    Omitted for network resources.
            overwrite
                    '0' (default) - do not overwrite existing documents
                    '1' - overwrite existing documents

                    Omitted for network resources.

            Attachment
                    Ancillary information about the data to be added.  May
                    be repeated multiple times.

                    When this is used, it is assumed that a single document
                    is to be added as it would be unclear with which document
                    the attachments should be associated.

            ConnectionString
                    ODBC connection string, see http://www.connectionstrings.com
                    for hints on constructing connection strings.

            import_map
                    Indicates import translation map to use.
                    Required if a map is needed and one is not specified
                    in the document to be added.
            species_abbr
                    Indicates species translation map for translating species
                    names to ITIS codes.
                    Not required for collections that do not have species
                    names
        """

        # might want to not allow overwrite for databases...
        formvars.setdefault('overwrite', '0')
        methods = self.dbserver.getMethods()
        result = None

        if "ConnectionString" in formvars:
            # Data is from a resource on the server machine or 
            # it is network available.    
            try:
                print("post_data_ConnectionString")
                result = methods.post_data_ConnectionString(
                        self.collection, formvars)
            except Exception as e:
                result = str(e)
                print(f"What about here??? {e}")
                # We do not raise a cherrypy.HTTPError(400, result)
                # as this will escape our XML
                cherrypy.response.status = codes["Bad Request"]

        else:
            # User provided data file
            try:
                data = formvars["data"]
                if not isinstance(data, list):
                    data = [data]
            except KeyError:
                raise cherrypy.HTTPError(codes["Forbidden"], 'POST multipart message missing "data" element')

            result = ""
            # Is the user passing a file or a network resource?
            if isinstance(data[0], str):
                # Network resource
                result = "Not implemented"
            else:
                try:
                    result = methods.post_data(self.collection, formvars, 'odbcfile')
                except Exception as e:
                    result = str(e)
                    # We do not raise a cherrypy.HTTPError(400, result)
                    # as this will escape our XML
                    cherrypy.response.status = codes["Bad Request"]

            return result


            """ THIS SECTION NEVER HITS """
            # validate the appropriate parameters were set
            problems = []

            connectors = ["MySQL", "MicrosoftAcces", "ConnectionString", "XML",
                          "csv", "MicrosoftExcel"]
            try:
                connect = formvars["connect"]
                if connect not in connectors:
                    problems.append("connect parameter not in [%s]"%(
                                ", ".join(connectors)))
            except KeyError:
                problems.append("connect parameter not set, must be one of [%s]"%(
                                ", ".join(connectors)))


            # Store the file to a temporary file if one was transmitted
            # as many of the tools won't work on a stream
            tmpfname = None
            docId = None  # document id
            if 'data' in formvars:
                if isinstance(formvars['data'], str):
                    # If string, resource is networked and we will need to connect
                    # to it.
                    datasource = formvars['data']
                elif isinstance(formvars['data'], list):
                    # Assume that we have a file
                    (filename, mimetype, fileH) = formvars['data'][0]
                    [basename, ext] = os.path.splitext(filename)
                    docId = os.path.basename(basename)
                    (f, tmpfname) = tempfile.mkstemp(ext)
                    util.copy_stream(f, fileH)
                    # to do - delete copy at end...
                    os.close(f)
                else:
                    raise cherrypy.HTTPError(codes["Forbidden"],
                        "data parameter must be a file or database name")

            connectionString = formvars.get('ConnectionString', None)
            source_map = formvars.get('import_map', None)
            species_map = formvars.get('species_map', None)

            methods = self.dbserver.getMethods()
            result = methods.import_data(self.collection, connect, connectionString,
                                         source_map, species_map)

            # todo:  Enclose in a finally block so we do this even on error
            if tmpfname:
                os.remove(tmpfname)

        return result

class Tethys(object):
    """
    Resource used for managing Tethys

    Use:
     GET Tethys/command to find status
     PUT Tethys/command/value to set status

     Valid commands:
     PUT Tethys/checkpoint - Create database checkpoint
     GET Tethys/performance_monitor - show if enabled
     PUT Tethys/performance_monitor/on|off|clear
        on - enable
        off - disable
        clear - reset statistics
    PUT Tethys/shutdown - Shutdown Tethys service
    PUT Tethys/cache/on|off|clear - Control XQuery cache
        on - enable (default)
        off - disable
        clear - clear cache
    GET Tethys/cache - show if XQuery cache is enabled
    """
    exposed = True
    cmds = [
        'checkpoint',
        'performance_monitor',
        'shutdown',
        'cache',
        'version',
        'erddap',
    ]


    def __init__(self, dbserver, r):
        """
        :param dbserver:  dbxmlInterface.db.Server instance for accessing
          Berkeley db database
        :param r:  Interface for accessing R (instance of
          mediators.r.RInterface or None if not available).
        """
        self.dbserver = dbserver
        self.methods = dbserver.getMethods()
        self.r = r  # R server (None if not serving)

    def __badcommand__(self, sink, element, attr={}):
            self.__error__(sink, element,
                '/command must be in [%s]'%(", ".join(self.cmds)), attr)

    def __error__(self, sink, element, msg, attr={}):
            sink.xml.startElement("Error", {})
            sink.xml.startElement(element, attr)
            sink.xml.characters(msg)
            sink.xml.endElment(element)
            sink.xml.endElement("Error")
            # Don't raise error as will escape XML
            cherrypy.response.status = codes["Bad Request"]

    def GET(self, *args, **kwargs):

        sink = Sink()  # output
        cmd = None
        try:
            cmd = args[0].lower()
        except IndexError:
            self.__badcommand__(sink, "No command specified")

        if cmd == "r":
            sink.xml.characters("false" if self.r is None else "true")

        elif cmd == "ping":
            print("pinged")
            sink.xml.startElement("Tethys", {})
            sink.xml.startElement("ping", {})
            sink.xml.characters("alive")
            sink.xml.endElement("ping")
            sink.xml.endElement("Tethys")

        elif cmd == "performance_monitor":
            sink.xml.startElement("Tethys", {})
            sink.xml.startElement("performance_monitor", {})
            status = "on" if self.methods.perfmon_active() else "off"
            sink.xml.characters(status)
            sink.xml.endElement("performance_monitor")
            sink.xml.endElement("Tethys")

        elif cmd == "cache":
            sink.xml.startElement("Tethys", {})
            sink.xml.startElement("cache", {})
            status = "on" if self.methods.query_cache_status() else "off"
            sink.xml.characters(status)
            sink.xml.endElement("cache")
            sink.xml.endElement("Tethys")

        elif cmd == "version":
            sink.xml.startElement("Tethys", {})
            sink.xml.startElement("version", {})
            sink.xml.characters(version.get_version())
            sink.xml.endElement("version")
            sink.xml.endElement("Tethys")

        elif cmd == "erddap":
            sink.xml.startElement("Tethys", {})
            sink.xml.startElement("ERDDAP", {})
            sink.xml.characters(mediators.default_erddap_server)
            sink.xml.endElement("ERDDAP")
            sink.xml.endElement("Tethys")

        else:
            # Unrecognized command
            sink.xml.startElement("Error")
            sink.xml.characters("Unrecognized resource command. ")
            sink.xml.characters(f"Valid commands:  {'/'.join(self.cmds)}")
            sink.xml.endElement("Error")

        return sink.finalize()


    def PUT(self, *args, **kwargs):
        """
        Set Tethys server property
        :param args:  property/value
            See class comments for valid combinations
        :param kwargs:
        :return:
        """

        sink = Sink()
        cmd = None
        task = None

        try:
            cmd = args[0].lower()
        except IndexError:
            self.__badcommand__(sink, "MissingElement")
        try:
            #was this a background task?
            task = args[1]
        except IndexError:
            pass

        if cmd == "checkpoint":
            sink.xml.startElement(args[0], {})
            if self.dbserver.transactional:
                result = self.dbserver.checkpoint()
                if result:
                    sink.xml.startElement('Success', {})
                    sink.xml.characters('checkpoint completed')
                    sink.xml.endElement('Success')
                else:
                    sink.xml.startElement('Failure', {})
                    sink.xml.characters("Unable to checkpoint")
                    sink.xml.endElement("Failure")

            else:
                self.__error__(sink, "NotTransactional",
                    "Checkpointing only possible for transactional databases")
            sink.xml.endElement(args[0])
            if task:
                return result
            else:
                return sink.finalize()
        elif cmd == "shutdown":
            # todo:  Security check to prevent unauthorized users from calling
            cherrypy.engine.exit()
            if self.r is not None:
                self.r.execute("stop", None)
            self.dbserver.release()
            return "<Tethys> exiting <" \
                   "/Tethys>"
        elif cmd =="performance_monitor":
            sink.xml.startElement("Tethys", {})
            sink.xml.startElement("performance_monitor", {})

            # Check and see if the performance monitor is currently active
            active = self.methods.perfmon_active()

            if len(args) > 1:
                opt = args[1].lower()
                sink.xml.characters(opt)
                if opt == "on":
                    if not active:
                        # only turn on if off
                        self.methods.set_perfmon()
                elif opt == "off":
                    if active:
                        self.methods.set_perfmon(active=False)
                elif opt == "clear":
                    PerformanceMonitor.clear_logs()
            else:
                sink.xml.characters("Path must terminate in /on, /off, or /clear, use GET to find status")

            sink.xml.endElement("performance_monitor")
            sink.xml.endElement("Tethys")
            return sink.finalize()

        elif cmd == "cache":
            sink.xml.startElement("Tethys", {})
            sink.xml.startElement("cache", {})

            if len(args) > 1:
                opt = args[1].lower()
                sink.xml.characters(opt)
                if opt == "on":
                    self.methods.query_cache_status(True)
                elif opt == "off":
                    self.methods.query_cache_status(False)
                elif opt == "clear":
                    self.methods.query_cache_clear()
            else:
                sink.xml.characters(
                    "Path must terminate in /on, /off, or /clear, use GET to find status")

            sink.xml.endElement("cache")
            sink.xml.endElement("Tethys")
            return sink.finalize()




class BatchLogs:
    exposed = True
    def __init__(self):
        pass

    def GET(self, *path, **kwargs):
        """GET - Retrieve documents  
            Retrieve a document batch log based on the path.
            Supports following URL parameters ?x=y:
            html=true|1|false|0 - Produce HTML log
              
            Specific log
            http:localhost:9779//BatchLogs/2013-01-13T02:45:00Z-ABC
              
            XML document with log names
            http:localhost:9779//BatchLogs
        """

        basedir = resources.names['logs']
        logdir = os.path.join(basedir, "BatchLogs")

        # return HTML?
        ret_html = "html" in kwargs and kwargs['html'].lower() in (1, 'true')

        # Check if directory exists, create if it doesn't
        if not os.path.exists(logdir):
            try:
                os.mkdir(logdir)
            except OSError as e:
                raise ValueError("Unable to create batchlog directory %s:  %s"%(
                        logdir, e.strerror))

        if len(path) > 1:
            raise ValueError("Path incorrect")

        # If there's no path element, list
        if len(path) == 0:
            result = ContainerWorker.GetReportLogs(ret_html)
        else:

            # otherwise pull in the specified one or return error not found
            fname = path[0]
            try:
                log = open(os.path.join(logdir,fname), 'rt')
                result = log.read()
            except IOError:
                raise cherrypy.HTTPError(codes["Forbidden"], "Log File not found")

            if ret_html:
                # Text in the log will be a combination of XML and escaped XML
                # that is text within XML fields.  For display purposes, we just
                # want to show human-readable characters.  We unescape the escaped
                # text, then escape everything.
                result = '<pre lang="xml">\n' + \
                         html.escape(html.unescape(result)) + "\n</pre>"
        return result


class ContainerWorker(threading.Thread):
    #renamed, since handling clear docs as well
    """Thread to handle update_documents and incremental of containers within XmlMethods.
        Creates a log for Detections, Localizations, and ITIS.
        Log is named with UTC time and appended with 
        eight (very probably) unique characters
    """
    active_threads = {}

    def __init__(self, methods, container, op=None, update=False, clear=False,
                 sink=None, accessURL="server:port"):
        """
        Start a container worker
        Usually used for operations that will take longer than the timeout
        
        methods - XMLMethods object
        container - dbxml container name on which we will be operating
        op - operation
        update - Update the container based on things that have already been
            submitted (e.g. rebuild)
        clear - clear the container
        sink - XML Sink where information will be recorded
        accessURL - URL used to access server, (used in constructing report
            directions for long operations)
        """

        threading.Thread.__init__(self)
        self.logfile = None
        self.logging = False #boolean for logging
        self.logsink = sink
        self.accessURL = accessURL
        if container in ['Detections', 'Localizations']:
            self.logging = True
            #Check for batchlog directory
            batchlog_resource = "BatchLogs"
            basedir = resources.names['logs']
            logdir = os.path.join(basedir, batchlog_resource)
            if not os.path.exists(logdir):
                try:
                    os.mkdir(logdir)
                except OSError as e:
                    self.logsink.elementText("OSError",
                        f"Unable to create log directory {logdir}: {e.strerror}")

            # Create unique BatchLog file, named by ISO8601 UTC datetime &
            # a (mostly) unique ID.  Milliseconds are shaved from the filename,
            # and colons replaced with periods
            logtime = (datetime.datetime.utcnow().isoformat()[:19]+'Z-').replace(':','.',2)
            # only take first 8 chars for convenience -- shouldn't cause issue
            logid = str(uuid.uuid4())[:8]
            self.logfname = logtime+logid + '.xml'
            self.logurl = self.accessURL + batchlog_resource + self.logfname

            try:
                logfile = open(os.path.join(logdir,self.logfname), 'w+')
                self.logfile = logfile
                self.logsink = LogSink(self.logfile)
                ContainerWorker.active_threads[self.logfname] = self
            except (OSError, IOError) as e:
                raise ValueError("Unable to access batchlog file")

        self.methods = methods
        self.container = container
        self.operation = op
        self.update = update
        self.clear = clear
        self.daemon = True


    def run(self):
        """ Executes update_documents in XML methods. Adds itself to
         the class dictionary: active_threads. 
        
        Calls CoInitialize for WIN32 Com issues
        """
        if platform.system() == 'Windows':
            if threading.currentThread().getName() != 'MainThread':
                pythoncom.CoInitialize()   # @UndefinedVariable (generated at runtime)

        # decide which method to execute...
        if self.operation == 1:
            # rebuild the container, updating all stored documents
            self.result = self.methods.update_documents(
                self.container, self.container, self.update,
                self.clear, self.logsink)
        elif self.operation == 0:
            # clear the container
            self.result = self.methods.container_clear(self.container)

        # Clear the cache as it has been modified.
        self.methods.query_cache_clear(collections=[self.container])

        if self.logging and not self.isActive(): #make sure this works...
            del ContainerWorker.active_threads[self.logfname]
        return

    #is this function necessary? perhaps not..
    def isActive(self):
        active = False
        try:
            active = self.active_threads[self.logfname].is_alive()
        except KeyError:
            pass
        return active

    def getLogFile(self):
        return self.logfile

    @classmethod
    def GetReportLogs(cls, genhtml=False):
        """Class method to handle returning list of all logs
        Will clear out leftover threads from the active_logs dictionary.
        Appends list of finished/inactive logs and active logs to their
        respective variables. Generates a report to display to user.

        :param genhtml:  generate HTML output if True
        """
        sink = Sink()

        basedir = resources.names['logs']
        logdir = os.path.join(basedir, "BatchLogs")
        files = os.listdir(logdir)
        finished = []
        working = []
        for log in files:
            inprogress = False
            try:
                instance = cls.active_threads[log]
                inprogress = instance.is_alive()
                if not inprogress:
                    #must have failed, didn't remove itself
                    del cls.active_threads[log]
                else:
                    working.append(log)
                    continue
            except KeyError:
                pass
            finished.append(log)
        Report.log_report(sink, finished, working)

        text = sink.finalize()

        if genhtml:
            io_str = StringIO()
            headers = [
                '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" ' +
                '"http://www.w3.org/TR/html4/strict.dtd">',
                '<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">',
                '  <HEAD><TITLE>Batch log report</TITLE></HEAD>',
                "   <BODY>",
                f"    <H1>Batch log report</H1>"
                "  <ul><code>"
            ]
            io_str.write("\n".join(headers))
            url = cherrypy.url()
            text = re.sub(r"<Log>(?P<entry>.+?)</Log>",
                          f'</code><li> <a href="{url}/\g<entry>?html=true">\g<entry></a><code>', text)
            t = re.sub(r"Log>(?P<entry>.+?)<?", "boo", text)
            io_str.write(t)
            io_str.write("</code></ul>\n")
            io_str.write("</BODY>\n</html>\n")
            text = io_str.getvalue()

        return text


class Schemata(object):
    """Schemata resource handles GET retrieval of schemata as XML or JSON
    """
    exposed = True

    def __init__(self):
        self.schemata = XML.schema_parsers.Schemata()

    def GET(self, *args, **kwargs):
        """GET(*args, **kwargs) - Retrieve a schema
        path is in args, valid path is schema name or noautocompletelist
        options are in kwargs    
        GET path:  Schemata/CollectionName
        GET options:
            These options are mutually exclusive.
            ?format={JSON|XML|CSV|HTML}
               Specified representation format for schema:
               XML - Return XML schema document (XSD)
               JSON - Return Badgerfish JSON representation of XSD
               CSV|HTML - Returns simplified path information.  Either produced
                 a CSV stream or a web page with information about paths in the
                 schema.  For each path, we indicate the name, the min and max
                 number of times it may appear.  If present, the type information
                 and documentation string are also returned.  The typed option
                 (below) may be used to select whether or not untyped paths
                 are returned (included by default)
            ?parsetree={true|false}, false is default
               Returns a decorated parse tree, only valid for JSON
               Decorated parse trees are a simplified representation of
               the schema and contain information in a format that makes it
               easy to determine an element's type (if any), children,
               number of times the element can be repeated, etc.
            ?typed={true|false}, false is the default
               For formats text and html, true restricts the list of returned
               elements to only those that contain typed values.

        If args[0] is
        noautocompletelist
                This is a special case for the WebClient.  The web client
                finds distinct values for elements to populate autocomplete
                lists.  In some cases, finding these values would require
                queries that would take a long time to compute.  This returns
                xpaths roots indicating the GUI should not attempt to build
                autocomplete lists for the path or its children.

        Examples:  Schemata/Detections?format=JSON
                   Schemata/noautocompletelist
        """

        # Check if string parameters (values following ? in URL) are reasonable
        valid_args = ('format', 'parsetree', 'typed')
        bad_kwargs = set(kwargs.keys()).difference(valid_args)
        if len(bad_kwargs) > 0:
            raise cherrypy.HTTPError(
                codes["Bad Request"],
                "Bad query string parameters "
                f"[{', '.join(bad_kwargs)}] in URL. "
                f"Legal options: {', '.join(valid_args)}. "
                "See Tethys Restful Web Interface Services for details. "
                "Legal example:  ?format=html"
            )

        if len(args) != 1:
            raise cherrypy.HTTPError(codes["Bad Request"],
                    "Path must contain a schema or noautocomplete")

        target = args[0]

        if target == "noautocompletelist":
            # Add any path you don't want being added to the Where Conditions
            # on html/AdvancedQueries.html to this list
            # Path's children will also be banned
            # (i.e. 'Detections/OnEffort/Detection/Channel' for
            # 'Detections/OnEffort')
            noautocomplete = " ".join([
                    'Detections/OnEffort',
                    'Detections/OffEffort'])
            cherrypy.response.headers['Content-Type'] = "text/plain"
            return noautocomplete.encode('utf8')

        valid_formats = ["JSON", "XML", "CSV", "HTML"]  # schema representations
        # default to XML if not specified
        format = kwargs.get('format', 'XML').upper()
        if format not in valid_formats:
            raise cherrypy.HTTPError(
                codes["Bad Request"],
                f'format={format}, must be in {", ".join(valid_formats)}')

        if "parsetree" in kwargs:
            parse_tree = kwargs["parsetree"].lower() == 'true'
        else:
            parse_tree = False
        if parse_tree and format != 'JSON':
            raise cherrypy.HTTPError(
                codes["Bad Request"],
                "?parsetree=true invalid without ?format=JSON")

        if 'typed' in kwargs:
            if format not in ('CSV', 'HTML'):
                raise cherrypy.HTTPError(
                    codes['Bad Request'],
                    "?typed=true|false invalid without ?format=html|text")
            typed = kwargs['typed'].lower() == "true"
        else:
            typed = False

        try:
            result = self.schemata.schema_to_str(target, format, parse_tree, typed)
        except ValueError as v:
            key_list = ", ".join(self.schemata.root2schemaname.keys())
            raise cherrypy.HTTPError(
                codes["Bad Request"],
                f"Schema root '{target}' does not exist.  Valid schema roots:  {key_list}")

        lc_format = format.lower()
        if lc_format == "html":
            cherrypy.response.headers['Content-Type'] = lc_format
        elif lc_format == "csv":
            cherrypy.response.headers['Content-Type'] = "text/csv"
        else:
            cherrypy.response.headers['Content-Type'] = \
                f"application/{lc_format}"

        return result.encode('utf8')  # return bytes


class Client:
    """Client - Handle JavaScript/HTML requests from web client
    """

    exposed = True # for public consumption
    basepath = REST.getWebServicesDir()

    def __init__(self):
        mimetypes.add_type("application/json", ".json")
        mimetypes.add_type("application/mat", ".mat")

    def GET(self, *args, **kwargs):
        "GET - Client wants a web page or JavaScript, etc."
        if len(args) == 0:
            #args = ("html", "query.html")
            currentAddress = cherrypy.url()
            new_url = currentAddress+"/html/query.html"
            raise cherrypy.HTTPRedirect(new_url)
        filename = os.path.join(self.basepath, *args)
        dir = os.getcwd()
        # be sure to sanitize to protect against serving arbitrary files
        try:
            # open up the file
            fileH = open(filename, 'rb')
        except IOError:
            raise cherrypy.HTTPError(
                codes["Not Found"],
                f'No such resource: {"/".join([str(a) for a in args])}')

        # read and return
        bytes = fileH.read()

        # Set the MIME type
        mime = mimetypes.guess_type(args[-1])
        print(f"{filename} -> {mime[0]}")
        cherrypy.response.headers['Content-Type'] = mime[0]

        return bytes


class ODBC:
    """
    Open database connectivity information
    """

    exposed = True

    def GET(self, *args, **kwargs):
        if len(args) == 0 or args[0] == "drivers":
            drivers = pyodbc.drivers()
            return "\n".join(drivers)
        else:
            raise cherrypy.HTTPError(codes["Bad Request"], "Bad arguments.")

class Upgrade:
    """
    Various forms of database management
    """

    exposed = True
    re_encoding = re.compile(r'<\?xml version="[^"]+" '
                          r'encoding="(?P<encoding>[^"]+)"\?>')

    def __init__(self, dbserver):
        self.dbserver = dbserver
        self.methods = dbserver.getMethods()
        self.xquery = self.methods.query


    def PUT(self, *args, **kwargs):
        """
        Modifications to existing collections
        :param args:  collection/action
            action - namespace
               Change top-level of document from a declared namespace to
               an implicit one, e.g.
                 <ty:Detections xmlns:ty="http://tethys.sdsu.edu/v1.0">
               to:
                 <Detections xmlns="http://tethys.sdsu.edu/v1.0">
               Most documents in Tethys prior to Tethys 3.0 simply wrapped
               the top-level element in a ty: namespace, leaving child elements
               outside of the namespace.

        :param kwargs:
        :return:
        """

        if args[0] == 'namespace':
            if len(args) < 2:
                raise ValueError("namespace requires a collection")


            # Create an XSLT transformer
            xslt = ET.parse("assets/implicit_ns.xsl")
            transformer = ET.XSLT(xslt)

            # Verify the collection exists
            collection = args[1]
            if not self.methods.container_exists(collection):
                raise ValueError(f'Collection "{collection}" does not exist')

            # Get all document names in collection
            qryStr = f"""
             for $doc in collection("{collection}")
               return replace(base-uri($doc), 'dbxml:///{collection}/', '')
             """
            result = self.xquery(qryStr)
            # Transform into list
            docs = result.split("\n") if result != '' else []
            # Transform each document
            kludge = True
            for d in docs:
                if kludge:
                    if d == "dbxml:/":
                        kludge = False
                    else:
                        print(f"Skipping document '{d}'")
                    continue
                xml_doc = self.methods.get_document(collection, d)


                print(f"Converting document '{d}'")
                # Strip off encoding string <?xml version="..." encoding="..."?>
                # if present as already decoded.  Leaves string untouched if
                # there is not an encoding string
                xml_doc = strip_xml_encoding_declaration(xml_doc)
                # Create document object model, transform namespace,
                # rebuild the document and save it
                dom = ET.fromstring(xml_doc)
                new_dom = transformer(dom)
                new_xml = ET.tostring(new_dom, pretty_print=True,
                                      encoding="unicode")
                print(d)
                self.methods.setString(collection, d, new_xml)


class ImportConfig:
    exposed = True

    def __init__(self, dbserver):
        self.dbserver = dbserver


    def POST(self, **formvars):

        # delete config name
        if "deleteConfigName" in formvars:
            basedir = resources.names['ImportConfig']
            fileName = formvars['deleteConfigName']
            jsonfie = os.path.join(basedir, fileName + '.json')
            os.remove(jsonfie)

        else: #save config name
            configInfo = json.loads(formvars['jsonString'])
            fileName = configInfo['configName']
            basedir = resources.names['ImportConfig']
            jsonfie = os.path.join(basedir, fileName + '.json')
            openedFile = open(jsonfie, "w", encoding="utf-8")
            openedFile.write(formvars['jsonString'])
            openedFile.close()

    def GET(self, **formvars):
        basedir = resources.names['ImportConfig']
        configInfoToReturn = {}

        # get all configuration file names for dropdown
        if "configName" not in formvars:
            files = os.listdir(basedir)
            idx = 0
            for f in files:
                if f.lower().endswith(".json"):
                    basename, ext = os.path.splitext(f)
                    configInfoToReturn[idx] = basename
                    idx = idx + 1
        else:
            # get specific config and load in data from file
            configName = formvars['configName']
            jsonfile = os.path.join(basedir, configName + '.json')
            with open(jsonfile, "r", encoding="utf-8") as file:
                fileInfo = json.load(file)
                configInfoToReturn[0] = fileInfo

        return json.dumps(configInfoToReturn)

class ParseError:
    """
    Serves failed imported XML documents for inspection
    """
    exposed=True

    def __init__(self):
        """
        ParseError constructor
        """
        # set path where we expect to find XML documents
        self.xml_dir = os.path.join(resources.names['logs'], "xmldocs")


    def GET(self, *path, **formvars):
        """

        :param *path:  path arguments
        :param formvars: form variables
        :return:

        form variables
        format= {XML (default) | HTML | HTML_Numbered}
               Specified representation format for document docId request:
               XML - Return XML document
               HTML - Return a version of the document suitable for display
                  in a web page.
               HTML_Numbered - Return a version of the document suitable for
                   display in a web page.  Document will be line numbered
               HTML formats will be pretty printed if the document has
               valid syntax.
        """

        if len(path) == 0:
            cherrypy.response.status = codes['Bad Request']
            raise ValueError("Bad document identifier")

        # Safeguard against retrieval exploits
        path = "/".join(path)
        if ".." in path or "/" in path:
            cherrypy.response.status = codes['Bad Request']
            raise ValueError(f'Document "{path}" contains .. or /')

        filename = os.path.join(self.xml_dir, path) + ".xml"
        if not os.path.exists(filename):
            cherrypy.response.status = codes['Bad Request']

        try:
            fh = open(filename, 'r')
            xmldoc = fh.read()
        except Exception as e:
            cherrypy.response.status = codes['Internal Server Error']
            raise RuntimeError("Document exists, but cannot be retrieved")

        format = formvars.get('format', 'XML')
        if format == 'XML':
            cherrypy.response.headers["ContentType"] = "text/xml"
        if format in ('HTML', 'HTML_Numbered'):
            # Create link messages to insert into web page
            server_url = util.get_public_url()
            url_str = "It can also be seen "
            if format == 'HTML':
                linnum_url = server_url._replace(
                    path=f"ParseError/{path}",
                    query="format=HTML_Numbered").geturl()
                url_str = url_str + \
                          f'with <a href="{linnum_url}">line numbers</a> '
            else:
                unnum_url = server_url._replace(
                    path=f"ParseError/{path}",
                    query="format=HTML").geturl()
                url_str = url_str + \
                          f'<a href="{unnum_url}"> without line numbers</a> '
            xml_url = server_url._replace(
                path=f"ParseError/{path}", query="format=XML").geturl()
            url_str = url_str + \
                      f'or <a href="{xml_url}" download="{path}.xml">' + \
                      'downloaded</a> in its raw form.'
            # Create web page text
            cherrypy.response.headers["ContentType"] = "text/html"
            io_str = StringIO()
            headers = [
                '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" ' +
                '"http://www.w3.org/TR/html4/strict.dtd">',
                '<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">',
                f'  <HEAD><TITLE>Failed Tethys import</TITLE></HEAD>',
                "   <BODY>",
                f"    <H1>This document failed to import correctly</H1>",
                "The XML document below may not correspond to its original form.",
                "It may have been formatted to be easier to read.",
                url_str,
                "     <H1>Document Content</H1>"
            ]
            io_str.write("\n".join(headers))

            try:
                # Attempt to parse XML and pretty print it.

                # Strip XML declaration if present as we've already decoded it
                result = strip_xml_encoding_declaration(xmldoc)
                tree = ET.fromstring(result)
                # pretty print and reconstruct
                ET.indent(tree, space="  ", level=0)
                xmldoc = ET.tostring(tree, encoding="unicode")
            except ET.ParseError as e:
                pass

            if format == 'HTML_Numbered':
                lines = xmldoc.split("\n")
                # Find out how many digits we need for line numbering
                n = len(lines)
                digits = math.ceil(math.log10(n))
                # Number the lines & reconstruct the document
                lines = [f"{idx+1:{digits}d}: {l}" for idx, l in enumerate(lines)]
                xmldoc = "\n".join(lines)

            io_str.write('<pre lang="XML">\n')
            io_str.write(html.escape(xmldoc))
            io_str.write('\n</pre>')
            io_str.write("   </BODY>\n</html>\n")
            xmldoc = io_str.getvalue()

        return xmldoc

class AmbiguousElement(ValueError):
    def __init__(self, xml_path : str, elements : list):
        """
        :param xml_path:  user input we tried to match
        :param elements:  list of possible matches to ambiguous element pattern
        """
        # ambiguous elements string
        ambiguous = "\n\t" + "\n\t".join(elements)
        msg = f"Unable to uniquely identify {xml_path} in the schema. " \
              f"Did you mean: {ambiguous}"
        self.elements = elements
        self.xmlpath = xml_path
        super().__init__(msg)


class XQueryPredicate:
    """
    QueryPredicate (simple query) language
    This class provides a mini query language that can be converted to XQuery
    The mini-language is parsed by a CongoCC-generated

    Generating XQueries is somewhat complicated, this class parses a string
    and generates a JSON object.  We leverage the XQueryJSON class to write
    the final XQuery.

    In the Tethys documentation, this is referred to as the simple query
    language as it consists of conjuctions of conditions (predicates) followed
    by an optional list of return elements
    """

    # When we are trying to resolve an element name that has been
    # partially specified, we search first in the schema associated
    # with the element specified in the json_spec root key.  If that
    # fails, we will look in other schemata in a manner that is
    # dependent on the root key.
    element_fallbacks = {
        "Detections": ["Deployment", "Ensemble"],
        "Localize": ["Deployment", "Ensemble", "Detections"],
    }

    # The return keyword is not mandatory.  If return is not specified,
    # the following elements will be returned.  The return list
    # depends on the collection being queried (specified by the root
    # element) and whether or not the query is for effort.
    default_returns = {
        # detections - effort False
        ("Detections", False): [
            "Detections/Id",
            "Detections/DataSource",
            "Detections/OnEffort/Detection/Start",
            "Detections/OnEffort/Detection/End",
            "Detections/OnEffort/Detection/SpeciesId"
        ],
        # detections - effort True
        ("Detections", True): [
            "Detections/Id",
            "Detections/DataSource/DeploymentId",
            "Detections/DataSource/EnsembleId",
            "Detections/Effort/Start",
            "Detections/Effort/End",
            "Detections/Effort/Kind"
        ],
        # localizations - effort False
        ("Localize", False): [
            "Localize/Id",
            "Localize/Localizations/Localization"
        ],
        # localizations - effort True
        ("Localize", True): [
            "Localize/Id",
            "Localize/Effort"
        ],
        # Calibrations & deployments - effort is not meaningful here
        # so the same values are returned in both cases.
        ("Calibration", False): ["Calibration"],
        ("Calibration", True): ["Calibration"],
        ("Deployment", False): ["Deployment"],
        ("Deployment", True): ["Deployment"]
    }

    def __init__(self, dbms, json_to_xquery, json_spec):
        """

        :param dbms:  handle to database manager
        :param json_to_xquery: instance of XQueryJSON
        :param json_spec: JSON specification for query
        """
        self.dbms = dbms
        self.json_to_xquery = json_to_xquery
        self.json = json_spec


        # Root element of principal target of query.  Lets us target
        # a specific schema for resolution of schema elements that are
        # not fully qualified
        self.root_element = self.json.get("root", None)
        # Should this query target Effort?  (Not valid for all schemata,
        # and is used to make inference decisions about ambiguous elements
        # and what to return
        self.effort = self.json.get("effort", False)
        if isinstance(self.effort, str):
            choices = {'0': False,
                       '1': True,
                       'false': False,
                       'true': True}
            eff_key = self.effort.lower()
            if eff_key in choices:
                self.effort = choices[eff_key]
            else:
                self.effort = False  # default (can't parse)

        if "query" in self.json:
            # Parse the user query
            query = self.json["query"]
            if not "return" in query:
                key = (self.root_element, self.effort)
                if key in self.default_returns:
                    query = query + "return " + \
                            ",".join(self.default_returns[key])
                else:
                    raise ValueError(
                        f"Query: {query}\n"
                        "does not contain return value and no appropriate "
                        "default return is available."
                    )

            parser = smpllang.parser.Parser(query)
            try:
                parser.parse_Query()
            except Exception as e:
                raise cherrypy.HTTPError(
                    codes["Bad Request"],
                    str(e)
                )

            # Parser returns lines & offsets, split input so that
            # we can reference these for error reporting
            self.query_lines = self.json["query"].split("\n")
            # Retrieve the parse tree
            self.parse_tree = parser.root_node
            # Build selection criteria
            json_spec["select"], json_spec["return"] = self.analyze_parse_tree()
            # We want to make sure that certain elements are enclosed
            # in their parent.  This makes repeated elements easier to pull
            # out into a table with XML processing tools
            wrap_elements = {
                # (RootName, Effort): element path
                ("Detections", True): "Detections/Effort/Kind",
                ("Detections", False): "Detections/OnEffort/Detection",
                ("Localize", False): "Localize/Localizations/Localization"
            }
            element = wrap_elements.get((self.root_element, self.effort), None)
            if element is not None:
                json_spec["select"].append({
                    "op": "exists",
                    "operands":  element,
                    "optype": "function"
                })
        else:
            raise ValueError("Expected query attribute in JSON specification")

    def analyze_parse_tree(self):
        """
        Examine the parse tree and construct a dictionary
        that represent the selection criteria and a list of paths
        to be returned
        :return:  selection_criteria_dict, return_list
        """

        # Get level 1 of parse tree
        # Will have (XmlExpr | XmlExprList) (return (XmlPath | RetValList))?
        children = self.parse_tree.children

        selections = []
        ret_list = []

        if len(children):
            criteria = children[0]  # potential selection criteria
            if isinstance(criteria, (smpllang.parser.XmlExpr,
                                     smpllang.parser.XmlExprList)):
                selections = self.parse_expression_list(criteria)
                rest = children[1:]
            else:
                rest = children

        # identify returns
        if len(rest):
            # last item contains return values
            ret_list = self.parse_returns(rest[-1])
        else:
            ret_list = []

        return selections, ret_list

    def normalize_path_or_literal(self, item):
        if isinstance(item, smpllang.parser.XmlPath) or \
                (isinstance(item, smpllang.parser.Token) and
                    item.token_type == smpllang.parser.XmlId):
            path = self.get_path(item)
            result = self.resolve_path(item)
        elif item.token_type == smpllang.tokens.TokenType.StringLiteral:
            if len(item.source) > 2:
                result = [item.source[1:-1]]  # strip quotes
            else:
                result = [""]  # empty string
        elif isinstance(item, smpllang.parser.OperandList):
            # Remove syntactic sugar that should have been deleted by
            # the parser
            sugar = (smpllang.TokenType.LParen, smpllang.TokenType.RParen,
                     smpllang.TokenType.Comma)
            remaining = [c for c in item.children if c.token_type not in sugar]
            result = []
            for r in remaining:
                result.extend(self.normalize_path_or_literal(r))
        else:
            result = [item.source]

        return result

    def parse_expression_list(self, expr_list, selections=None):
        """

        :param expr_list:  Node of type XmlExprList
        :return:
        """

        if selections is None:
            selections = list()

        # Expression lists consist of an expression followed by
        # an optional conjunction and new expression list.  Build up
        # recursively
        if isinstance(expr_list, smpllang.parser.XmlExprList):
            # tree should have [expr, and, expr_list]
            expr = expr_list[0]  # get first expression
            next_list = expr_list[2]
        else:
            expr = expr_list  # expression list is expression
            next_list = None

        # expression consists of LHS Relop RHS
        expr_items = expr.children
        if len(expr_items) == 3 and \
                expr_items[1].token_type == smpllang.tokens.TokenType.Relop:

            lhs = expr_items[0]
            self.annotate_node(lhs)
            op = expr_items[1]
            rhs = expr_items[2]
            self.annotate_node(rhs)

            # lhs may have more than one source, create an selection for each one
            left_annotations = lhs.annotation
            if not isinstance(left_annotations, list):
                left_annotations = (left_annotations,)
            for left_annotation in left_annotations:
                selection = {}
                selection["op"] = op
                selection["optype"] = "binary"
                selection["operands"] = [left_annotation, rhs.annotation]
                selections.append(selection)
        else:
            # Not yet supporting functions...
            subex = expr.input_source[expr.begin_offset:expr.end_offset]
            raise ValueError(
                f"Unable to parse sub-expression {subex} "
                f"from line {expr.begin_line} col {expr.begin_column} to "
                f"line {expr.end_line} {expr.end_column}")

        if next_list is not None:
            # Grammar will have forced conjunction [1] and XmlExprList [2]
            self.parse_expression_list(next_list, selections)

        return selections

    def get_path(self, ident):
        """
        Given a representation of an Xml identifier, construct a complete
        XPath for all elements it represents.
        :param ident:  identifier or path from parser
        :return:  string representation of the xpath
        """
        if isinstance(ident, smpllang.parser.XmlPath):
            # concatenate components of path
            path = "".join([n.source for n in ident.children])
        else:
            path = ident.source
        return path

    def annotate_operands(self, oplist):
        """
        Retrieve the operands from the parse tree
        :param oplist: list of tokens of form ( val, val, ... )
        :return: side effect adds .source to oplist
        """
        operands = []
        for operand in oplist.children:
            if operand.token_type in (smpllang.parser.LParen,
                                      smpllang.parser.RParen,
                                      smpllang.parser.Comma):
                continue  # discard lexical syntax
            self.annotate_node(operand)
            operands.append(operand.annotation)

        oplist.annotation = operands
        return


    def parse_returns(self, ret_val_list, root_element=None):
        """
        Given a list of return value tokens, create a list of fully
        resolved XML paths
        :param ret_val_list:  list of partially or fully resolved elements
        :param root_element:  Root element of the schema
        :return: Fully resolved paths
        """

        values = []

        # Find the XmlPath or RetValList and parse it

        # ReturnExpr : return (RetValList | XmlPath | XmlId)
        if isinstance(ret_val_list, smpllang.parser.ReturnExpr):
            ret_val_list = ret_val_list.children[-1]

        if isinstance(ret_val_list, smpllang.parser.RetValList):
            path_expr = ret_val_list[0]  # XmlPath | XmlId
            tail = ret_val_list[2]
        else:
            path_expr = ret_val_list # XmlPath | XmlId
            tail = None

        # Resolve path
        self.annotate_node(path_expr, must_be_unambiguous=True)  # Set XmlPath
        values.extend(path_expr.annotation)

        if tail is not None:
            values.extend(self.parse_returns(tail, root_element))

        return values

    def annotate_node(self, node, must_be_unambiguous=False):
        """
        Analyze a node to determine the data associated with it.
        Will have the side effect of updating a node's annotation field
        :param node:  parse tree node
        :param must_be_unambiguous:  If True and node is an XmlPath, the
           path may never resolve to more than one item (otherwise it is
           sometimes okay, see annotate_xmlpath)
        :return:  None, side effect on node.annotation
        """
        if isinstance(node, smpllang.parser.XmlPath) or \
           node.token_type == smpllang.tokens.TokenType.XmlId:
            # Set annotation to the complete path or in some cases path list
            self.annotate_xmlpath(node, must_be_unambiguous=must_be_unambiguous)
        elif isinstance(node, smpllang.parser.OperandList):
            # Set annotation to the operand or list of operands
            self.annotate_operands(node)
        elif node.token_type == smpllang.tokens.TokenType.Number:
            # Set annotation to value of number
            node.annotation = float(node.source)
        elif node.token_type == smpllang.tokens.TokenType.StringLiteral:
            # String literals should have their enclosing quotes
            if len(node.source) >= 2:
                node.annotation = node.source[1:-1]  # Strip quotes
            else:
                node.annotation = ""
        else:
            if not hasattr(node, "source"):
                print(f"Unable to annotate node {node} (no associated source)")
            else:
                # Set annotation to the text matched by the node
                node.annotation = node.source

    def annotate_xmlpath(self, node, root_element=None,
                     must_be_unambiguous=False):
        """
        Extract the XML path from a parser subtree rooted at node

        The path is resolved to a complete path within the schema by
        searching for an exact path from the root or a subpath and
        saved to node.annotation as a side effect.

        If there is no match, we may look for a fallback in another
        schema (based on schema that would be typically joined).

        If there is a match, in most cases, it must usually match
        exactly one path, with a notable exception for detections
        where we frequently desire matches with the same element that is
        a child of Effort and OnEffort.

        :param node:  Parser node, must be of type XmlPath
        :param must_be_unambiguous:  if True, multiple matches are never
           allowed
        :return:  None; side-effect to node.annotation
        """

        path = None
        if isinstance(node, smpllang.parser.XmlPath):
            # concatenate source of XmlId and separator children
            path = "".join([c.source for c in node.children])
        elif node.token_type == smpllang.tokens.TokenType.XmlId:
            path = node.source
        else:
            contextN = 20
            start = max(0, node.begin_offset - contextN)
            end = min(len(node.input_source), node.begin_offset + contextN)
            context = node.input_source[start:node.begin_offset] + \
                "↓" + node.input_source[node.begin_offset:end]

            raise ValueError(
                "Expected element or element path near ↓ (line " +
                f"{node.begin_line}, column {node.begin_column}): " +
                context
            )

        node.annotation = self.resolve_path(
            path, must_be_unambiguous=must_be_unambiguous)

        return

    def resolve_path(self, path_str, root_element=None,
                     must_be_unambiguous=False):
        """
        Given a potentially partial XML path in the schema, try to resolve it
        and return a list with one entry; the complete path from the root to
         the element in question.  When more than one match occurs, it is
        generally considered to be an error.

        A notable exception for this is when querying detections.  When
        specifying selection criteria, it is desirable to have something that
        occurs in a detection and the on-effort (e.g. a taxonomic identifier)
        be present in both the Effort and Detection portion of the query.
        In such cases, a list of matching paths is returned.

        This allows us to build a query that filters the selection criteria
        by the effort, greatly reducing query time as we do not have to
        search through every detection.  This should only be done while
        processing selection criteria as adding additional elements in a
        return will affect the results.  In contrast, when a user specifies
        an ambiguous return element in a non-effort query (as controlled by
        the effort parameter in the JSON query), e.g. Parameters/Subtype
        which appears in Effort, OnEffort/Detection, and OffEffort/Detection,
        the user probably wants the OnEffort/Detection.  We use the
        must_be_unambiguous flag to control whether multiple returns are
        allowed in this case.

        CAVEAT:  While the strategy of adding an ambiguous element to both
        the effort and detection query has tremendous performance benefits,
        it can affect results negatively if someone is querying for both
        OnEffort and OffEffort detections.  The Effort filter could prevent
        the investigation of OffEffort detections that do not meet the effort
        criteria.  As OffEffort detections by definition do not have
        systematic effort, this would be incorrect.  Avoid this by using
        complete paths if querying both OnEffort and OffEffort detections.

        :param path_str:  the partial or full xml path, e.g. Kind/SpeciesId
        :param root_element:  the root element of the schema, if not specified
           assumes the root element specified in the root key of the JSON
           specification.  This parameter allows us to override the search
           when a query relies on elements from another schema (e.g. a
           detection query that references deployment criteria)
        :param must_be_unambiguous:  If True, the xmlpath must match
           exactly one complete path.  Set to True to disable the exception
           where detections and effort are set.
        :return:  List of complete paths (usually of length 1)
        """

        if root_element is None:
            # Use the root element specified by user unless this is
            # a fallback case to another collection
            root_element = self.root_element

        paths = self.json_to_xquery.schema_search(root_element, path_str)

        if len(paths) == 0:
            # Couldn't find it.
            if root_element == self.root_element:
                # Try fallback schemata
                fallbacks = self.element_fallbacks[self.root_element]
                for f in fallbacks:
                    paths = self.resolve_path(
                        path_str, root_element=f,
                        must_be_unambiguous=must_be_unambiguous)
                    if len(paths) > 0:
                        break  # found match(es)
                if len(paths) == 0:
                    schemata = [self.root_element].extend(fallbacks)
                    raise ValueError(
                        f"Unable to match {path_str} to schemata: {schemata}")

        max_len = 1
        if len(paths) > max_len:
            # multiple matches, can we disambiguate?
            if self.effort:
                # User is requesting effort.  In such cases, we don't care
                # about many places where the ambiguous case does not contain
                # effort.  We only modify if we are able to match something.
                eff_paths = [p for p in paths if "/Effort/" in p]
                if len(eff_paths):
                    paths = eff_paths
            else:
                if root_element == "Detections":
                    if must_be_unambiguous:
                        # Assume user wants OnEffort detections when ambiguous
                        # We include both the OnEffort and the Effort as including
                        # Effort significantly speeds up queries
                        paths = list(
                            filter(
                                lambda path: "/OnEffort/" in path, paths))
                        max_len = 1  # Only OnEffort in path
                    else:
                        # Assume user wants OnEffort detections when ambiguous
                        # We include both the OnEffort and the Effort as including
                        # Effort significantly speeds up queries
                        paths = list(
                            filter(
                                lambda path:
                                "/OnEffort/" in path or "/Effort/" in path, paths))
                        max_len = 2  # Okay to have OnEffort & Effort in path

        # todo:  If we search on a secondary schema, we won't have set max_len > 1 if needed
        if len(paths) > max_len:
            raise AmbiguousElement(path_str, paths)
        else:
            return paths














