'''
Created on Apr 14, 2013
CherryPy RESTful server

@author: mroch
'''

import os.path
import logging.handlers
import ssl  # secure socket library
import socket

# Site modules
import cherrypy

# Local modules
import resources
import REST.Monitors
import REST.Resources
from REST.util import get_public_url
import mediators.r
import version

class Root:
    _cp_config = {
        # 'request.error_response' : err_handler,

        #potential place to start session
        }

    def __init__(self, dbxmlServer):
        pass


class Server:
    def __init__(self, db_server, port, cert={},
                 startup_msg=''):
        """
        dbxmlServer - dbxmlInterface.db.Server instance
        port - listens for traffice on port p
        cert - Dictionary, uses secure socket layer when the keys
            Certificate and PrivateKey are set to point to appropriate
            SSL certificate and private key files.
        """

        if port is not None:
            REST.config['global']['server.socket_port'] = port

        # Secure socket transport layer
        if len(cert) > 0:
            REST.config['global']['server.ssl_module'] = 'builtin'
            REST.config['global']['server.ssl_certificate'] = \
                cert['Certificate']
            REST.config['global']['server.ssl_private_key'] = cert['PrivateKey']
            
        root = Root(db_server)   # Create an instance of the root resource

        r_interface = None
        try_r = False  # No longer using R interface
        if try_r:
            try:
                r_interface = mediators.r.RInterface()
            except RuntimeError as e:
                print(e)

        root.Tethys = REST.Resources.Tethys(db_server, r_interface)
        root.Attach = REST.Resources.Attachment()
        root.BatchLogs = REST.Resources.BatchLogs()
        root.Schema = REST.Resources.Schemata()
        root.Client = REST.Resources.Client()
        # XML to other format converter
        root.Convert = REST.Resources.Convert(db_server, r_interface)
        # Query resource, also uses functionality of converter
        root.XQuery = REST.Resources.XQuery(db_server,
                                            self,
                                            root.Convert.get_xml_converter())
        root.ODBC = REST.Resources.ODBC()
        root.dbJSON = REST.Resources.dbJSON(db_server)
        root.Upgrade = REST.Resources.Upgrade(db_server)
        root.ImportConfig = REST.Resources.ImportConfig(db_server)
        root.ParseError = REST.Resources.ParseError()

        for resource in ['Detections', 'Localizations', 'Deployments',
                         'Ensembles', 'SourceMaps', 'ITIS', 'ITIS_ranks',
                         'SpeciesAbbreviations', 'Calibrations',
                         'mediator_cache']:
            setattr(root, resource,
                    REST.Resources.Collection(db_server, resource))

        # Serve static files from embedded web site,
        # assumes that Documentation directory is a peer of the server
        src_dir = os.path.split(version.__file__)[0]
        dist_dir = os.path.abspath(os.path.join(src_dir, '../..'))
        doc_dir = os.path.join(
            dist_dir, "Documentation", "quarto", "_site")

        cherrypy.tree.mount(object(),  # dummy object
                            "/Documentation",  # resource path
                            config={
                                "/": {
                                    "tools.staticdir.on": True,
                                    "tools.staticdir.dir": doc_dir,
                                    "tools.staticdir.index": "index.html",
                                }
                            })

        # Update global defaults
        cherrypy.config.update(REST.config)  # none at present
        
        # Authenticator
        # cherrypy.tools.my_basic_auth = cherrypy.Tool(
        #      'on_start_resource', Collection.my_basic_auth)

        # Create application to handle everything from server URL        
        app = cherrypy.tree.mount(root, '/', REST.config)
        self.app = app

        # Set up rotating log size and count parameters, use defaults
        # if they exist
        app.error_file = ""  # Remove any possible handlers
        app.access_file = ""

        maxBytes = getattr(app.log, "rot_maxBytes", 10000000)
        maxCount = getattr(app.log, "rot_backupCount", 20)
        # Construct rotating error(and everything else) log
        logHandler = logging.handlers.RotatingFileHandler(
                os.path.join(resources.names['logs'], "log.txt"), 'a',
                maxBytes, maxCount)
        logHandler.setLevel(logging.DEBUG)
        logHandler.setFormatter(cherrypy._cplogging.logfmt)
        app.log.error_log.addHandler(logHandler)
        # try setting a message string from db.py
        # Set up a log handler for HTTP access messages, in Apache/NCSA
        # Combined Log format
        appHandler = logging.handlers.RotatingFileHandler(
                os.path.join(resources.names['logs'], 'statistic.txt'), 'a',
                maxBytes, maxCount)
        appHandler.setLevel(logging.DEBUG)
        appHandler.setFormatter(cherrypy._cplogging.logfmt)
        app.log.access_log.addHandler(appHandler)

        if startup_msg != '':
            self.app.log.error(startup_msg)

        db_server.getMethods().register_server(self)
        
        cherrypy.engine.start()
        REST.Monitors.startMonitors(db_server, r_interface)

        # Access logging function
        logger = db_server.getMethods().log

        # Note service address of web client
        web_client = get_public_url()._replace(path="Client").geturl()
        logger(f"Web interface at {web_client} ")

        if try_r and r_interface is None:
            self.log_error("R programming language interface unavailable")

        cherrypy.engine.block()
        
    def log_error(self, msg):
        self.app.log.error(msg)

        

                
