
from cherrypy.process.plugins import BackgroundTask

from dbxmlInterface.XmlMethods import XmlMethods
import REST.Resources

# Units in seconds
minute = 60
hour = minute*60
day = 24*hour


def mediators_scrub_cache(methods : XmlMethods):
    """
    Remove old items from mediator cache

    :param methods: database methods object
    :return: None
    """
    mgr = methods.get_mediator_manager()
    mgr.cache_clean()

def checkpoint(tethys,methods):
    result = tethys.PUT("checkpoint",True)
    methods.checkpointmsg(result)
    
def startMonitors(dbxmlServer, r_interface=None):
    methods = dbxmlServer.methods
    mediator_task = BackgroundTask(12*hour, mediators_scrub_cache, [methods], {}) # 1*hour
    mediator_task.start()

    tethys = REST.Resources.Tethys(dbxmlServer, r_interface)
    checkpt_task = BackgroundTask(day, checkpoint, [tethys,methods],{}) #day

    checkpt_task.start()
    