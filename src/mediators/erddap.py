'''
erddap.py
Interface module for ERDDAP
Environmental Research Division Data Access Program
https://www.ncei.noaa.gov/erddap/index.html
Created on Feb 6, 2012

@author: mroch
'''

import os.path as path
import re
import json
import urllib.request, urllib.parse, urllib.error
import urllib.request, urllib.error, urllib.parse
import gzip
import requests

from datetime import datetime

from xml.sax.saxutils import XMLGenerator, quoteattr, escape
from xml.sax.xmlreader import AttributesNSImpl, AttributesImpl
from xml.sax.handler import ContentHandler

from io import StringIO

# local packages
from mediators import default_erddap_server

# default_server = "coastwatch.pfeg.noaa.gov"  # old server
# default_server = "upwell.pfeg.noaa.gov"  # claims to serve all NOAA data

def jtable(tbl, row, col):
    """Given a json table, access the specified row and column
    Note that this is inefficient for multiple uses as the column names
    are searched for each access.
    """
    
    # find the index of the desired column
    colN = tbl['table']['columnNames'].index(col)
    return tbl['table']['rows'][row][colN]  # return the data


class search(object):
    debug = False

    def __init__(self, type=None, args=None, metaargs=None):
        """Set up an ERDDAP search.
        Builds a URL that can be used to retrieve information about ERDDAP
        datasets in a variety of formats, e.g. JSON, html as specified by type.  
        Keyword arguments are any valid set of ERDDAP keywords.
        ERDDAP's web services discussion gives a couple of examples and contains
        a pointer to a GUI which will let people observe all settable parameters:
        http://coastwatch.pfeg.noaa.gov/erddap/rest.html
        
        :param type: type of information to return, e.g., html
        :param args: search arguments
        :param metaargs: arguments about the ERDDAP service as opposed to
           a specific request.  String with name=value;name=value;...
           Supported arguments:  server=URL for ERDDAP resource

        Search arguments as of this writing, see ERDDAP documentation:
        searchFor 
        protocol
        cdm_data_type
        institution
        ioos_category
        long_name
        standard_name
        minLat
        maxLat
        minLon
        MaxLon
        
        Parameters are joined with the & sign
        """
        
        # Store type of information returned by URL and construct it.
        valid_types = ("html", "json")
        if type is None:
            type = "html"
        if type not in valid_types:
            raise ValueError("URL search type must be in [%s]"%(", ".join(valid_types)))

        self.metaargs = argstr_to_dict(metaargs)

        self.type = type
        self.url = self.searchurl(args)
    
    def searchurl(self, searchArgs):
        """Construct URL for finding catalogs that meet search criteria
        Criteria consists of keyword value pairs conforming to ERDDAP 
        search parameters.  The type parameter is either json or html
        to return
        :param searchArgs: search arguments
        """

        url = f"{self.metaargs['server']}/search/advanced.{self.type}?{searchArgs}"
        return url

    def get_url(self):
        "Return the URL"
        return self.url
    
    def xml(self):
        "Return the URL encoded as XML"
        if self.debug:
            print(self.url)
        return "<url>%s</url>"%(escape(self.url))
    
    def xml_contents(self):
        "Return the search result as XML"
        raise NotImplementedError
    
    def get_contents(self):
        "Return the contents of the URL"

        try:
            data = urllib.request.urlopen(self.url)
        except urllib.error.HTTPError as e:
            e.msg = (f"\nUnable to read data from {self.url}." +
                  "\nEither ERDDAP is down or you have an improperly formatted request" +
                  "\nCheck data set identifier, argument order and bounds")
            raise  # reraise
        if self.type == "json":
            contents = json.load(data)
        elif self.type == "html":
            contents = data.readlines()
        
        if self.debug:
            print(self.url)
            print(contents)

        return contents

    
class data(object):
    
    dataset_re = re.compile("(?P<dataident>.+)\?(?P<Query>.*)")

    def __init__(self, args=None, metaargs=None, debug=True):
        """
        data - Retrieve ERDDAP data.
        datasetID must be a valid data set for the server
        See http://coastwatch.pfeg.noaa.gov/erddap/rest.html
        for a list of valid types of data that can be returned

        :param args:  data to retrieve
        :param metaargs: metadata, valid:
            server=https://server.name/erddap - path to erddap resource
            Uses default_erdap_server if not provided
        :param debug:
        """

        self.metaargs = argstr_to_dict(metaargs)
        self.debug = debug
        m = self.dataset_re.match(args)
        if m is None:
            raise ValueError("""ERDDAP:  Unable to parse dataset from %s.
Expected either a valid ERDDAP URL or dataset?param1=val1&param2=val2&..."""%(args))
        self.query = m.group("Query")
        
        # Supported data access protocols
        daptypes = ('griddap', 'tabledap')
        self.accesstype = None
        
        # See if the user passed in a whole URL or if they gave a dataset name
        target = m.group("dataident") 
        (pathto, basename) = path.split(target)
        (base, ext) = path.splitext(basename)

        # User should have specified jut the dataset id
        # Verify that this is what they did and clean up if necessary

        if pathto != "":
            raise ValueError("Must specify dataset id or complete URL.  "+
                "Example:  erdMBchla8day?param1[indices]... or " +
                "http://coastwatch.pfeg.noaa.gov/erddap/griddap/erdMBchla8day.json?param1=val1&")


        # Query the datatype to see what type of data interface it supports
        response = None
        headers = {'Accept-encoding': 'gzip'}
        info_url = self.metaargs["server"] + f"/info/{base}/index.csv"
        try:
            response = requests.get(info_url, headers=headers)
        except requests.exceptions.ConnectionError as e:
            errtxt = str(e)
            raise ValueError(errtxt)
        try:
            response.raise_for_status()
        except:
            errtxt = response.content
            raise ValueError(errtxt)

        info_rows = response.text.split("\n")
        # Identify table/grid based on cdm_data_type
        self.accesstype = None
        for row in info_rows:
            if "cdm_data_type" in row:
                cdm_data_type = row.split(",")[-1]
                if cdm_data_type.lower() == "grid":
                    self.accesstype = "griddap"
                else:
                    self.accesstype = "tabledap"
                break
        if self.accesstype is None:
            raise ValueError(
                f"Unable to determine if dataset {base} uses "
                "griddap or tabledap.  cdm_data_type was missing "
                "or contained an unexpected value in the metdata. "
                f"Metadata from:  {info_url}"
            )
        self.url = (self.metaargs["server"] +
                    f"/{self.accesstype}/{base}")

        if self.debug:
            print(f"ERDDAP URL: {self.url} arguments {args}")

        self.initargs = args

    def tagged_data(self, xmlsink, tag, attributes, data):
        "Output simple tagged data <tag> data string </tag>"
        
        xmlsink.startElement(tag, attributes)
        try:
            xmlsink.characters(escape(str(data)))
        except UnicodeError as e:
            raise ValueError(e.__repr__())
        xmlsink.endElement(tag)
    
    def _griddap_xml(self, jsondata, xmlsink):
        "_griddap - format griddap data"
        
        # Find number of axes.  Query will have a [ ] pair for each axis
        
        dim = len(re.findall('\[[^\]]*\]', self.query.split(',')[0]))
        (dims, axes) = self._griddap_dims(jsondata, dim)
        
        xmlsink.startElement("Grid", self.noattrib)
        
        
        # Write out information about the grid including
        # dimensions, labels and label unit type
        xmlsink.startElement("Dims", self.noattrib)
        xmlsink.characters(escape(" ".join([str(d) for d in dims])))
        xmlsink.endElement("Dims")
        
        xmlsink.startElement("Axes", self.noattrib)
        xmlsink.startElement("Names", self.noattrib)
        for ax in axes:
            self.tagged_data(xmlsink, "item", self.noattrib, ax[0])
        xmlsink.endElement("Names")
        xmlsink.startElement("Types", self.noattrib)
        for ax in axes:
            self.tagged_data(xmlsink, "item", self.noattrib, ax[1])
        xmlsink.endElement("Types")
        xmlsink.startElement("Units", self.noattrib)
        for ax in axes:
            self.tagged_data(xmlsink, "item", self.noattrib, ax[2])
        xmlsink.endElement("Units")
        
        for ax in axes:
            if ax[1] == 'String':
                xmlsink.startElement("Values", self.noattrib)
                for v in ax[3]:
                    self.tagged_data(xmlsink, "item", self.noattrib, v)
                xmlsink.endElement("Values")
            else:
                self.tagged_data(xmlsink, "Values", self.noattrib, " ".join([str(v) for v in ax[3]]))
        xmlsink.endElement("Axes")
        
        # Write out the data values
        datadim = len(jsondata['table']['rows'][0])
        units = jsondata['table']['columnUnits']
        types = jsondata['table']['columnTypes']
        xmlsink.startElement('Data', self.noattrib)
        xmlsink.startElement("Names", self.noattrib)
        for idx in range(dim, datadim):
            self.tagged_data(xmlsink, "item", self.noattrib,
                jsondata['table']['columnNames'][idx])
        xmlsink.endElement("Names")
        xmlsink.startElement("Types", self.noattrib)
        for idx in range(dim, datadim):
            self.tagged_data(xmlsink, "item", self.noattrib, types[idx])
        xmlsink.endElement("Types")

        xmlsink.startElement("Units", self.noattrib)
        for idx in range(dim, datadim):
            self.tagged_data(xmlsink, "item", self.noattrib, units[idx]) 
        xmlsink.endElement("Units")
        # Each row contains the axes labels followed by the data.
        # We've already extracted the axes labels so we simply 
        # pullt out the data.  dim contains the # of dimensions
        # that are stored in [0, dim-1], so we start at index dim
        for idx in range(dim, datadim):
            if units[idx] == 'String':
                xmlsink.startElement("Values", self.noattrib)
                for v in [r[idx] for r in jsondata['table']['rows']]:
                    self.tagged_data(xmlsink, "item", self.noattrib, v)
                xmlsink.endElement("Values")
            else:
                values = []
                for r in jsondata['table']['rows']:
                    if r[idx]:
                        values.append(str(r[idx]))
                    else:
                        values.append('NaN')
                self.tagged_data(xmlsink, "Values", self.noattrib,
                    ' '.join(values))
                    
        xmlsink.endElement('Data')
        xmlsink.endElement("Grid")

        
    def _griddap_dims(self, jsondata, dim):
        """_griddpa_dims - Data is in table format, extract the column
        axis data so that we don't have to transmit multiple copies
        Returns information about table axes names, type information
        units, and values"""
        
        step = 1
        labels = []
        dims = []
        rowsN = len(jsondata['table']['rows'])
        for idx in range(dim-1, -1, -1):
            # Find the first repeat in the current axis
            lookfor = jsondata['table']['rows'][0][idx]
            values = [lookfor]
            current = step
            found = False
            while current < rowsN and not found:
                value = jsondata['table']['rows'][current][idx]
                found = lookfor == value # Have we seen this one before
                if not found:
                    values.append(value)
                    current = current + step
            
            step = step * len(values)
            labels.append(
                (jsondata['table']['columnNames'][idx],
                 jsondata['table']['columnTypes'][idx],
                 jsondata['table']['columnUnits'][idx],
                 values))
            dims.append(len(values))
        
        # We find these in reverse order, but leave them that
        # way as it reflects the shape of the data.  ERDDAP
        # appears to have the rightmost column moving most
        # quickly.  As we take the data as a vector, having
        # the rightmost index move first works well for reshaping
        # data in a column oriented language like Matlab.   
        return (dims, labels)

    def xml(self, **args):
        "Return query jsondata as XML"
        if args is None:
            args = self.initargs
        query = f"{self.url}.json?{self.query}"
        if self.debug:
            print(f"Querying {query}")
        response = None
        headers = {'Accept-encoding':'gzip'}
        try:
            response = requests.get(query,headers=headers)
        except requests.exceptions.ConnectionError as e:
            errtxt = str(e)
            raise ValueError(errtxt)
        try:
            response.raise_for_status()
        except:
            errtxt = response.content
            raise ValueError(errtxt)

        try:
            jsondata = response.json()
        except ValueError as e:
            raise ValueError(str(e))

        xmlstr = StringIO()
        xmlsink = XMLGenerator(xmlstr)
        xmlsink.startDocument()

        # Most tags will not have attributes
        # Create an empty attribute to use in these cases
        self.noattrib = AttributesNSImpl({}, {})

        if self.accesstype == "griddap":
            self._griddap_xml(jsondata, xmlsink)
        elif self.accesstype == "tabledap":
            docElement = "Table"  # enclosing tag

            xmlsink.startElement(docElement, self.noattrib)
            cols = jsondata['table']['columnNames']
            types = jsondata['table']['columnTypes']
            units = jsondata['table']['columnUnits']
            rows = jsondata['table']['rows']

            attribs = []
            for idx in range(len(cols)):
                attr = {}
                if units[idx] is not None:
                    attr['units'] = units[idx]
                if types[idx] is not None:
                    attr['type'] = types[idx]
                attribs.append(AttributesImpl(attr))

            # todo:  Embed a DTD based on the type information
            # instead of type in attribute

            xmlsink.startElement("header", self.noattrib)
            for idx in range(len(cols)):
                xmlsink.startElement(cols[idx], attribs[idx])
                xmlsink.endElement(cols[idx])
            xmlsink.endElement("header")

            for row in rows:
                xmlsink.startElement('row', self.noattrib)

                for idx in range(len(cols)):
                    if row[idx] is None:
                        xmlsink.startElement(cols[idx], self.noattrib)
                        xmlsink.endElement(cols[idx])
                    else:
                        self.tagged_data(xmlsink, cols[idx], self.noattrib, row[idx])
                xmlsink.endElement('row')

            xmlsink.endElement(docElement)
        else:
            # Should not ever get here.
            raise ValueError("Unsupported jsondata access protocol %s" % (self.accesstype))

        if self.debug:
            print("returning jsondata %s" % (datetime.now()))

        # f = open("foobar.xml", "w")
        # f.write(xmlstr.getvalue())
        # f.close()
        return xmlstr.getvalue()
        
        
def argstr_to_dict(arg_spec, argument_sep=";", assign_sep="="):
    """
    Process argument string into dict with argument names and values
    Provides default for key server if not present

    :param arg_spec:  String with  k1=v1;k2=v2;..., or
       a dictionary.  If a dictionary is passed, we assume that
       the argument string has already been processed and return the
       dict untouched.
    :param argument_sep: Override argument separator from ;
    :param assign_sep: Override assignment separator from =
    :return: dict(k1=v1, k2=v2, ...)
    """

    if isinstance(arg_spec, dict):
        arguments = arg_spec  # already processed, return it
    else:
        arguments = dict()
        if arg_spec is not None:
            try:
                # "k1=v1;k2=v2;..." into dict keys and values
                kv_pairs = arg_spec.split(argument_sep)
                for item in kv_pairs:
                    k, v = item.split(assign_sep)
                    arguments[k] = v
            except Exception as e:
                raise ValueError(
                    f"Collection parameters '{arg_spec}' must be a "
                    "semicolon separated list of key=value pairs.")

        if "server" not in arguments:
            # didn't provide a server, use default
            arguments["server"] = default_erddap_server

    return arguments
