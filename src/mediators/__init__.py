# The mediators module is used to read data from
# various remote sources and make them look like XML

# US National Center for Environmental Information
default_erddap_server = "https://coastwatch.pfeg.noaa.gov/erddap"