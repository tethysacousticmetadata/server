"""
manager
Management of mediator interfaces, cached results, etc.
"""

from dataclasses import dataclass, field
from datetime import datetime, timedelta
import os
import re
from threading import Semaphore, Condition
import uuid

# Add-on modules
import dbxml
import maya
from XML.transform import escape

# Tethys modules
from dbxmlInterface import XmlMethods
import mediators.horizons
import mediators.sun
import mediators.erddap
import mediators.timezone
import mediators.erddap_metadata


# Mediator calls are cached.  We sometimes have multiple calls to a mediator
# with the same values.  When this occurs, we delay the subsequent calls
# until the first one completes and then return the result from the cache.
# This prevents using the mediator service multiple times.
# Active computations are keyed by the mediator name, path, and params
@dataclass
class MediatorStatus:
    """
    Information about active mediator queries
    """
    timeout: datetime
    completed: Condition = field(default_factory=Condition)


@dataclass
class MediatorInfo:
    mediator: str  # mediator collection
    path: str  # mediator arguments
    start: int  # start position in XQuery
    stop: int  # mediator query spans Xquery[start:stop]
    params: str  # Additional parameters
    check_cache: bool  # Check cache before performing computation
    tmpdoc: list[str] = field(default_factory=list[str])  # tmp document name


class Manager:

    # Cache for mediators
    mediator_cache = "mediator_cache"
    mediator_ns = "http://tethys.sdsu.edu/mediators"
    tethys_ns = "http://tethys.sdsu.edu/schema/1.0"
    default_ns = f'declare default element namespace "{tethys_ns}";'

    # if present in the mediator's metadata arguments
    # (a second colon in the collection name  ext:mediator:metadata_args)
    # then the mediator is executed without checking the mediator cache
    # Note that higher level caches may still apply
    skip_cache = "skip-mediator-cache"

    # Map of mediator names to classes
    # All classes must have constructors capable of parsing the path argument
    external_containers = {
        "horizons": mediators.horizons.ephemeris,
        "solar": mediators.sun.Ephemeris,
        "timezone": mediators.timezone.timezone,
        "erddap": mediators.erddap.data,
        "erddap_search": mediators.erddap.search,
        "erddap_metadata": mediators.erddap_metadata.erddap_metadata
    }

    # Wait no more than this amount of time for a duplicate query
    # to finish before abandoning all hope and running it again.
    max_wait_s = 90.0

    # External collection specification.  Matches strings of form
    # collection("ext:name"), collection('ext:name') or collections
    # with parameters:  collection("ext:name:parameters")
    #
    # Following the collection specification, a path argument indicates
    # directive to the mediator and must end with an exclamation point
    #
    # collection("ext:my_mediator)/do_this=72!
    #
    # If the mediator needs parameters in addition to the directives,
    # e.g., a server address, these can be passed in inside the collection
    # string with a second colon (:).  We assume that multiple parameters are
    # separated by semicolons (;).
    #
    # The special mediator parameter defined by this class's variable
    # skip_cache instructs the mediator manager to skip the step of
    # looking for previously computed results, and execute the mediator
    # regardless of the cache status.  See the skip_cache instance
    # variable to determine the string that results in this behavior.
    # At time of writing, bound to "skip-mediator-cache"
    #
    # Example:
    # collection("ext:my_mediator:server=https://server.org;skip-mediator-cache")/do_this=72!
    ext_collection_re = re.compile(r"""
    collection\(
          \s*(?P<quote>['"])
            ext:(?P<extcollection>[\w|\.]+)(:(?P<params>[\s\w\.=,;:/]*))?
          (?P=quote)   # Match quote type
          \s*\)""", re.VERBOSE)
    # Directives are passed to external collection in the same in a manner
    # similar to XPath, but must be terminated with and exclamation point.
    # e.g. collection('ext:ExternCollectionName')/longitude=32.98/latitude=99.1!
    ext_path_re = re.compile("(?P<path>/.*)!")

    # URI parser
    uri_re = re.compile(
        "(?P<scheme>[^:]+)://+(?P<hierarchy>[^#]+)(#(?P<fragment>.*))?")

    def __init__(self, db_methods: XmlMethods, debug: bool = True):
        """

        :param db_methods: Instance of an XML database interface
        :param debug: True|False - provide information messages
        """
        self.debug = debug
        self.db_methods = db_methods
        # Dictionary to manage pending mediator computations and
        # a semaphore for critical sections on this variable
        self.active_computations = {}
        self.active_sem = Semaphore(1)

    def xquery(self, query_str):
        """
        Parse FLWOR Xquery with references to mediator (external) collections.
        Mediator queries have collection names that start with ext:
            e.g., collection("ext:ExtCollName")/
        The collection function is followed by a path string terminated by a !
        These arguments are passed to the external collection, and the results
        are marshalled to XML.

        :param query_str:  XQuery string with reference to external collection
           (see below)

        :return:  query_str_modified
           Returns a modified version of the XQuery that references XML
           documents that have been added to the mediator cache.  These
           documents can be reused if the same mediator data are requested,
           and expire after about a week (as of this writing) to reduce
           stale mediator values which are not expected to change frequently.

        Two examples:

        1. A straight forward query that returns solar rise/set
        collection("ext:horizons")/target="sol"/latitude=32.8/longitude=243.8/
           start="2009-10-01T00:00-08:00"/stop="2009-10-04T00:00-08:00"/
           interval="5m tvh"!

        2.  This query is a bit more complicated and extracts portions of the
            XML that were returned by the mediator.
        for $entry in collection("ext:horizons")/target="moon"/latitude=52.723400/longitude=174.765400/start="2011-01-10T00:00:22"/stop="2011-02-27T15:00:22"/interval="10m"!/ephemeris/entry
           return $entry/moon

        In general, the mediator results are stored in a mediator_cache
        collection and the XQuery is rewritten to reference the document
        stored in the mediator cache.  Here's an example of how the second
        example above might be rewritten:

        for $entry in doc("dbxml:///mediator_cache/dbxml_3")/ephemeris/entry
        return $entry/moon

        CAVEAT:
        External queries can only contain static parameters.
        XQuery variables cannot be passed as part of the path specification
        following the collection("ext:mediator-name")/path!
        """

        match = self.ext_collection_re.search(query_str)
        externals = []  # information about each external collection query

        # Multiple external queries can be included in the same query.
        # External queries have the form:
        #   collection("ext:collection_name")/parm0/param1/.../paramN!)
        #
        # We process the first external query and then search the remainder
        # of the query for subsequent externals, setting up the next match
        # object at the bottom of the loop.
        #
        # At loop exit, we have constructed a list where each item is a list
        # containing the collection name, the argument string, and the
        # character indices of the start and end of the parameter path
        # in the query.

        while match is not None:
            start = match.start()
            stop = match.end()
            collection = match.group("extcollection")

            path = ""  # empty until we know better
            # try to match arguments after the external collection
            argmatch = self.ext_path_re.match(query_str, match.end())
            if argmatch is not None:
                stop = argmatch.end()  # update stopping point to include args
                path = argmatch.group("path")
                # Strip leading / if present
                if path[0] == '/':
                    path = path[1:]

            mediator_params = match.group("params")
            check_cache = True
            if mediator_params is None:
                mediator_params = ""
            else:
                # Check if special keyword to force cache update is present
                params_list = mediator_params.split(";")
                check_cache = not self.skip_cache in params_list
                if not check_cache:
                    # Reconstruct the list without the cache update
                    params_list.remove(self.skip_cache)
                    mediator_params = ";".join(mediator_params)

            # Note details so that we can rewrite the query
            # to access the appropriate document.
            externals.append(
                MediatorInfo(collection, path, start, stop,
                              mediator_params, check_cache))

            # Look for next external collection
            start = stop
            match = self.ext_collection_re.search(query_str, start)

        # Run queries, substitute in names for temporary documents
        if len(externals) > 0:
            for ext_q in externals:
                xmldocid = None

                # Check cache for result if appropriate
                if ext_q.check_cache:
                    try:
                        # Identify the document and construct an XQuery
                        # to retrieve it
                        xmlid = self.cache_get(ext_q.mediator, ext_q.path)
                    except KeyError as e:
                        pass  # Wasn't in the cache, proceed as normal

                if xmldocid is None:
                    # No cache entry, execute the mediator
                    xmlid, xmlbody = self.mediator_query(ext_q)

            # All temporary documents have been created, rewrite the query using
            # the temporary tables.
            new_query = []  # list to be joined
            start = 0

            for ext_q in externals:
                # Copy up to where the external access started
                if ext_q.start > 0:
                    new_query.append(query_str[start:ext_q.start])
                # Add in new query to temporary table
                new_query.append(xmlid)
                # update next start
                start = ext_q.stop

            # copy any remaining part of the query
            if start < len(query_str):
                new_query.append(query_str[start:])
            # join to create revised query
            query_str = "".join(new_query)

        return query_str  # return revised query

    def mediator_query(self, ext_q: MediatorInfo):
        """
        Invoke a mediator
        :param ext_q:  MediatorInfo specifies the mediator to use
           and the arguments for it.
        :return: xmlid, xmlbody
           xmlid - Identifier for document produced by mediator. It is
              suitable for use as the target for an XPath or XQuery.
              Example:  f'return {xmlid}/element1/element2/...'
           xmlbody -If the document was retrieved from the mediator cache,
              no body is returned (None).  If we had to produce the document,
              the XML is returned in xmlbody.
        """

        cache_result = True  # Do we need to cache the result?
        # We set completed to True once we have the data, mediator_executed
        # is set to True if we had to execute the mediator as opposed to using
        # a previously cached value.
        completed = False
        mediator_executed = False

        # Tuple identifies computation based on mediator type and
        # the path arguments passed to it.
        comp_id = (ext_q.mediator, ext_q.params, ext_q.path)

        xmlid = xmlbody = None

        other_active_req = None  # No other request for same data
        if cache_result and comp_id in self.active_computations:
            # Someone is currently executing the exact same query
            # and in general we are caching results.  Wait for them
            # to finish the computation and use the cached value

            # Note:  We considered removing the mediator caches as
            # the query cache created with Tethys 3.0 caches the final
            # results.  However, if someone uses a mediator as
            # part of a larger query, we still need a way to incorporate
            # the resultant XML document into the XQuery.

            if self.debug:
                print(f"Waiting for mediator {comp_id} to complete")
            with self.active_sem:
                # critical section
                other_active_req = self.active_computations[comp_id]
                # Ensure that we acquire the condition before leaving
                # the critical section to avoid a race condition
                # The other mediator should update the cache before
                # notifying us that it is safe to proceed
                other_active_req.completed.acquire()
            # wait must be performed while the condition variable is acquired
            data_acquired = other_active_req.completed.wait(self.max_wait_s)

        # Try to acquire the document from the cache if permitted
        if ext_q.check_cache:
            try:
                if other_active_req is not None:
                    print("check cache after waiting")
                xmlid = self.cache_get(ext_q.mediator, ext_q.path, ext_q.params)
                completed = True
                if self.debug:
                    print("Retrieved results from mediator cache")
            except KeyError as k:
                if other_active_req is not None:
                    # The same query was active in another request, we
                    # expected the cache entry to be present.  Something
                    # went wrong in the other request, but we don't know what.
                    raise KeyError(
                        f"Query to mediator: {ext_q.mediator} ("
                        f"params: {ext_q.params}, path: {ext_q.path}) "
                        "triggered an unknown error.  Query recently "
                        "failed and we expected to reuse the result, details "
                        "of error in first failure.  Try again later to see "
                        "error message")
                else:
                    pass  # Not in the cache, need to compute

        if not completed:
            # We need to run the query
            with self.active_sem:
                # Critical section
                self.active_computations[comp_id] = \
                    MediatorStatus(
                        datetime.now() + timedelta(seconds=self.max_wait_s))
            try:
                xmlbody = self.execute(ext_q)
                completed = True
                mediator_executed = True
            except Exception as e:
                with self.active_sem:
                    # Critical section
                    failed = self.active_computations[comp_id]
                    del self.active_computations[comp_id]  # de-list
                    # There may be duplicate queries waiting, notify them
                    with failed.completed:
                        failed.completed.notify_all()
                    raise e  # propagate error

        if mediator_executed:
            # We executed the mediator and produced a result.
            # As the design calls for us to return a document id, we will
            # always need to cache it as the XQuery will need to access it,
            # but if cache_result is False, we only cache for a short time.
            if cache_result:
                # Add results to cache
                xmlid = self.cache_put(
                    ext_q.mediator, ext_q.path, ext_q.params, xmlbody)
            else:
                # Cache with a short expiration date
                rnd_id = str(uuid.uuid4())  # Generate a random UUID
                xmlid = self.cache_put(
                    ext_q.mediator, ext_q.path + rnd_id, ext_q.params,
                    xmlbody, expiration_h=4)

        if other_active_req is None:
            # active indicates another thread was processing this request
            # This thread is the owner, so we are responsible for delisting
            # the computation and notifying any waiting threads.
            with self.active_sem:
                # Critical section
                # Get information about this computation and delist it
                other_active_req = self.active_computations[comp_id]
                del self.active_computations[comp_id]

                # Notify waiting threads to use the cached version
                if self.debug:
                    print("Notifying any waiting threads")
                with other_active_req.completed:
                    other_active_req.completed.notify_all()

        if xmlid is None:
            if ext_q.params is not None:
                extstr = f'("ext:{ext_q.mediator}:{ext_q.params}")/{ext_q.path}'
            else:
                extstr = f'("{ext_q.mediator}")/{ext_q.path}'
            raise ValueError(f"Failed external query collection{extstr}!")

        return xmlid, xmlbody

    def execute(self, ext_q: MediatorInfo):
        """
        Run a mediator and return its value
        :param ext_q: Information on mediator to be executed
        :return: XML string
        """

        try:
            factory = self.external_containers[ext_q.mediator]
        except KeyError as e:
            # delete any temporary documents that have been created.
            raise ValueError(
                f"mediator collection(ext:{ext_q.mediator}) "
                "does not exist")

        if self.debug:
            print(f"Running ext:{factory}({ext_q.path})...")
        try:
            if ext_q.params is not None and ext_q.params != "":
                instance = factory(args=ext_q.path, metaargs=ext_q.params)
            else:
                instance = factory(args=ext_q.path)
        except NameError as e:
            raise NameError("No such external collection")
        except Exception as e:
            raise e

        # Create result (some mediators don't do the work until this is invoked)
        xmlbody = instance.xml()

        if self.debug:
            print(f"{factory} complete")
        return xmlbody

    def cache_get(self, collection, path, params=None):
        """
        See if an external document exists already for the query.  If so,
        retrieve the document identifier, otherwise throw a KeyError
        exception

        :param collection:  Mediator "collection" name
        :param path: Mediator arguments
        :param params: Meta arguments to mediator
        :return: document id string in the form
              dbxml:///container/document_identifier
            To access the document in an XQuery, wrap it in doc():
            doc("dbxml:///container/document_identifier")
        """

        if params is None or params == "":
            params = "default"

        # Query looking for metadata matching the mediator query.
        result = self.db_methods.query(
            f"""
            declare default element namespace "{self.mediator_ns}";
            base-uri(collection('{self.mediator_cache}')/*[dbxml:metadata('collection') = '{escape(collection)}' 
            and dbxml:metadata('path') = '{escape(path)}' and 
            dbxml:metadata('params') = '{escape(params)}'])""")

        if result == "":
            raise KeyError("No cache entry")
        return f'doc("{result}")'

    def cache_put(self, collection, path, params, doc, expiration_h=168):
        """
        Add specified document from external collection to the mediator cache.
        path specifies the string handed to the external mediator and
        is part of the access key.  expiration_d indicates for how many days
        the cache entry will be valid.  

        :param collection:  mediator "collection" name
        :param path: arguments to mediator
        :param params: meta arguments to mediator
        :param doc: XML document
        :param expiration_h: Cache entry expires in N hours
        :return: XML document retrieval specification suitable for use in
           an XQuery
        """

        if params is None or params == "":
            params = "default"

        # Set expiration to expiration_h from now.
        curr_date = maya.now()
        exp_date = curr_date.snap(f'@h+{expiration_h}h')
        metadata = {
            self.mediator_ns: {
                "collection": escape(collection),
                "path": escape(path),
                "params": escape(params),
                "expiration": dbxml.XmlValue(
                    dbxml.XmlValue.DATE_TIME, exp_date.iso8601())
            }
        }
        collection = self.db_methods.access_container(self.mediator_cache, True)
        docId = collection.addString(None, doc, 0, meta=metadata)

        return f'doc("dbxml:///{self.mediator_cache}/{docId}")'

    def cache_clean(self):
        """
        Remove expired documents from mediator cache
        :return: None
        """

        curr_date = maya.now()
        print(f"Scrubbing mediator cache at {curr_date}")

        query = """
            declare namespace md = "http://tethys.sdsu.edu/mediators"; 
            for $c in collection("mediator_cache")/*[dbxml:metadata("md:expiration") < xs:dateTime("%s")] 
            return 
                base-uri($c)
            """ % (curr_date.iso8601())

        collection = self.db_methods.access_container(self.mediator_cache, False)
        queryRslt = self.db_methods._query(query)

        scrubbed = []
        for item in queryRslt:
            docId = item.asString()
            match = self.uri_re.match(docId)
            if match:
                path = match.group("hierarchy")
                doc = os.path.basename(path)
                collection.rmDocument(doc)
                scrubbed.append(doc)
        del queryRslt

        if len(scrubbed) > 0:
            self.db_methods.log("Mediator cache - Scrubbed %s" % (" ".join(scrubbed)))
