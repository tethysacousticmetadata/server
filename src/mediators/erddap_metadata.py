import json
import requests
import datetime

# Opendap reading package
from netCDF4 import Dataset

# Our packages
from XML.sink import Sink
import dbxmlInterface.XmlMethods

default_server = "upwell.pfeg.noaa.gov"  # claims to serve all NOAA data
class erddap_metadata:

    def __init__(self, args=None):
        self.server = default_server
        self.dataset = None
        self.output_type = None

        argpairs = dbxmlInterface.XmlMethods.namevalues(args)
        for arg in argpairs:
            if len(arg) != 2:
                raise ValueError(
                    "Bad ephemeris argument:  "
                    f"%{'='.join(arg)}, expected name=value")
            elif arg[0] in ['server', 'dataset', 'output_type']:
                setattr(self, arg[0], arg[1])
            else:
                raise ValueError(f"Unknown ephemeris argument {arg[0]}")

        if self.dataset is None:
            raise ValueError("Expected dataset specification")
        self.url = f'https://{self.server}/erddap/griddap/{self.dataset}.nc?pseudo_nitzschia%5B(2022-03-19T12:00:00Z):1:(2022-03-19T12:00:00Z)%5D%5B(31.3):1:(43.0)%5D%5B(232.5):1:(243.0)%5D,particulate_domoic%5B(2022-03-19T12:00:00Z):1:(2022-03-19T12:00:00Z)%5D%5B(31.3):1:(43.0)%5D%5B(232.5):1:(243.0)%5D,cellular_domoic%5B(2022-03-19T12:00:00Z):1:(2022-03-19T12:00:00Z)%5D%5B(31.3):1:(43.0)%5D%5B(232.5):1:(243.0)%5D,chla_filled%5B(2022-03-19T12:00:00Z):1:(2022-03-19T12:00:00Z)%5D%5B(31.3):1:(43.0)%5D%5B(232.5):1:(243.0)%5D,r555_filled%5B(2022-03-19T12:00:00Z):1:(2022-03-19T12:00:00Z)%5D%5B(31.3):1:(43.0)%5D%5B(232.5):1:(243.0)%5D,r488_filled%5B(2022-03-19T12:00:00Z):1:(2022-03-19T12:00:00Z)%5D%5B(31.3):1:(43.0)%5D%5B(232.5):1:(243.0)%5D'

    def xml(self):
        "Return query jsondata as XML"
        print("MY URL ", self.url)
        response = None
        headers = {'Accept-encoding': 'gzip'}
        try:
            response = requests.get(self.url, headers=headers)
        except requests.exceptions.ConnectionError as e:
            errtxt = str(e)
            raise ValueError(errtxt)
        try:
            response.raise_for_status()
        except:
            errtxt = response.content
            raise ValueError(errtxt)

        try:
            with open("response.nc", "wb") as f:
                f.write(response.content)
            with Dataset("response.nc") as rootgrp:
                metadata = rootgrp.__dict__
        except ValueError as e:
            raise ValueError(str(e))

        res = dict()
        for sub in metadata:
            if not isinstance(metadata[sub], str):
                res[sub] = str(metadata[sub])
            else:
                res[sub] = metadata[sub]

        sink = Sink()
        sink.xml.startElement("metadata")
        sink.xml.newline()

        if self.output_type == "xml":
            sink.xml.startElement("dict")
            sink.xml.characters(str(res))
            sink.xml.endElement("dict")
        elif self.output_type == "json":
            output = json.dumps(res)
            sink.xml.startElement("json")
            sink.xml.characters(output)
            sink.xml.endElement("json")
        else:
            # Should not ever get here.
            raise ValueError("Unsupported output type %s" % self.output_type)

        sink.xml.endElement("metadata")
        sink.xml.newline()

        return sink.getstr()