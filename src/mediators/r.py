"""
Interface to the R programming language
"""

# Standard library
import os
from multiprocessing import Process, Queue, Lock
import tempfile

successful_launch = "R started"
r_location = "R_HOME"  # Environment variable with path to R root folder

def process(mutex, task_q, result_q):
    if os.getenv(r_location) is None:
        result_q.put(f'Cannot start R, environment variable "{r_location}" is not set')
        return

    try:
        import rpy2.robjects
        from rpy2.robjects.packages import importr, isinstalled
        from rpy2.robjects.vectors import StrVector
        # Start R server and ensure the required packages
        # are installed.  We search for libraries in the R_LIBS directory
        # or R default library location if R_LIBS is not set.
        # Note that if we do not have write access to the library locations,
        # we may not be able to install new packages.
        utils = importr('utils')  # utils is a built-in
        # select a mirror for R packages
        utils.chooseCRANmirror(ind=1)  # select the first mirror in the list
        # to be checked for and installed if not already installed
        r_packages = ['stringi', 'R6', 'methods', 'xml2', 'data.tree']
        names_to_install = [x for x in r_packages if not isinstalled(x)]
        if len(names_to_install) > 0:
            # isinstalled(x,lib_loc=loc), this doesn't work for some reason
            utils.install_packages(StrVector(names_to_install))
        for package in r_packages:
            rpy2.robjects.r(f'library("{package}")')

        # Access packages we need to use
        xml2_pkg = importr('xml2')
        datatree_pkg = importr('data.tree')
    except Exception as e:
        result_q.put(f"R failed to start: {e}")
        return

    result_q.put(successful_launch)
    more = True

    while more:
        with mutex:
            # Ensure everything done in a critical section
            # to avoid problems with interleaving
            action, data = task_q.get()

            if action == "stop":
                more = False  # End process
                result_q.put(None)
                continue
            elif action == "data.tree":
                print("data.tree")
                doc = xml2_pkg.read_xml(data)  # parse XML
                xml_list = xml2_pkg.as_list(doc)  # convert to list
                # Convert to data.tree
                datatree = datatree_pkg.as_Node(xml_list)

                # Write out the data.tree representation to a temporary file
                print('saving')
                fd, tmpfile = tempfile.mkstemp(suffix=".Rdata")
                tmpfile = tmpfile.replace("\\", "/")
                # bind datatree representation to a variable
                rpy2.robjects.r.assign("tree", datatree)
                # save it to tmpfile
                rpy2.robjects.r("save(tree, file = '" + tmpfile + "')")

                # Read the data associated with the file, remove the file
                # and return the data.
                print('reading')
                with os.fdopen(fd, 'rb') as dt:
                    result = dt.read()
                os.remove(tmpfile)
                result_q.put(result)

            else:
                v = ValueError("Bad action")
                result_q.put(v)


class R(Process):
    def __init__(self, task_q, result_q):
        """
        R server
        Subclass of process for interfacing with R
        """
        super().__init__()

        self.task_q = task_q
        self.result_q = result_q
        self.mutex = Lock()  # Support for mutual exclusion

    def run(self):
        """
        Process queues.  Retrieves operation and data from task_q and enqueues
        the result into result_q.  A None is put into the result_q when the
        server receives a stop instruction

        Queue entries are tuples of (operation, data)
        Operations supported:
        data.tree, xml - String of XML data is converted to Rdata suitable
           for writing to a file
        stop, None - Shut down the process
        :return:  None
        """
        # Execute a subroutine that is not a member of the class (has problems if it is)
        # This routine loops, processing messages until it is told to shutdown.
        process(self.mutex, self.task_q, self.result_q)


class RInterface:
    def __init__(self):
        self.send_q = Queue()
        self.recv_q = Queue()
        self.r_process = R(self.send_q, self.recv_q)
        self.r_process.start()

        started = self.recv_q.get()
        if started != successful_launch:
            raise RuntimeError(started)

    def execute(self, action, data):
        """
        Execute an action on specified data.
        See class R for available action/data pairs
        :param action:
        :param data:
        :return: result of action on data
        """
        self.send_q.put((action, data))
        result = self.recv_q.get()

        return result


