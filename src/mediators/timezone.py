'''
Created on Oct 28, 2011

@author: mroch
'''

# Standard libraries
import urllib.request, urllib.parse, urllib.error
import math
import pdb

# Tethys libraries
import dbxmlInterface.XmlMethods



# timezone cache
timezones = dict()

class timezone(object):
    '''
    timezone - Deliver timezone offsets
    Relies on earthtools.org timezone delivery for localtime
    and uses a local cache for URL queries to prevent overloading servers
    '''

    debug = False;
     
    def __init__(self, args):
        """timezone(args) - timezone information
        
        Create new timezone instance
        args is a string with keyword/value pairs.  Each keyword and
        value is separated by an = sign and pairs are separated by /
        
        keywords:
        longitude - longitude east [0,360) or (-180,180]
        latitude - latitude [-90,90]
        tztype - 
            "nautical" (defualt if omitted) - Nautical timezone 
                15 degree gores centered on the prime meridian                
            "civil" - Attempts to find localtime following geopolitical
                variation of timezones.  Uses earthools.org for timezones,
                unverified data, use at your own risk.
        """
        
        names = ['latitude', 'longitude', 'tztype']
        tztype="nautical"
        longitude = None
        latitude = None
        
        argpairs = dbxmlInterface.XmlMethods.namevalues(args)

        for arg in argpairs:
            if len(arg) != 2:
                raise ValueError(
                    "Bad timezone argument:  %s, expected name=value.  "%("=".join(arg)))
            if arg[0] not in names:
                raise ValueError("Bad timezone argument %s, not in [%s]"%(
                    arg[0], ",".join(names)))
            else:
                if arg[0] in ['latitude', 'longitude']:
                    value = float(arg[1])
                else:
                    value = arg[1]
                setattr(self, arg[0], value)  # self.name = value

        if self.longitude is None or self.latitude is None:
            raise ValueError('Arguments latitude and longitude are mandatory')
        
        if self.longitude > 180:
            # Ensure East/West longitude is in range
            # [-180, 180]
            self.longitude = (self.longitude % 180) - 180
            
        self.longlat = (self.longitude, self.latitude)
        
        if tztype == "nautical":
            self.nautical_time()
        elif tztype == "civil":
            self.civil_time()
        else:
            raise ValueError("Bad timezone type")
        
        if timezone.debug:
            print(self.xml_timezone)

    def nautical_time(self):
        "Determine local nautical time zone"
        (longitude, latitude) = self.longlat

        # Size of arc that defines time zone
        arcwidth = 15.0   # Every N degrees
        arcwidth_half = arcwidth / 2.0  # Centered on prime meridian
        
        # Compute offset from prime meridian
        offset = int(math.copysign(1.0, longitude) * \
            math.floor((math.fabs(longitude)+arcwidth_half) / arcwidth))
     
        # Convert offset to an index into time zone suffix
        if offset < 0:
            suffix_idx = 12 - offset
        else:
            suffix_idx = offset
        suffix = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"[suffix_idx]

        self.xml_timezone = \
        """<?xml version="1.0" encoding="ISO-8859-1" ?>
<timezone xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
   xsi:noNamespaceSchemaLocation="http://www.earthtools.org/timezone-1.1.xsd">
  <version>1.1</version>
  <location>
    <latitude>%f</latitude>
    <longitude>%f</longitude>
  </location>
  <offset>%d</offset>
  <suffix>%s</suffix>
  <localtime></localtime>
  <isotime></isotime>
  <utctime></utctime>
  <dst>Unknown</dst>
</timezone>"""%(       latitude, longitude, offset, suffix)

    def local_time(self):
        (longitude, latitude) = self.longlat
        try:
            self.xml_timezone = timezones[self.longlat]
            # todo:  Check for cache staleness (14 days)
            if timezone.debug == True:
                print(("Returning cached value for long %f lat %f"%(
                    longitude, latitude)))
                
        except KeyError as e:
            # Wasn't in the Cache, query via URL
            url = "http://www.earthtools.org/timezone-1.1/%f/%f"%(longitude, latitude)
            try:
                self.xml_timezone = urllib.request.urlopen(url).read()
                timezones[self.longlat] = self.xml
                if timezone.debug == True:
                    print(("URL retrieved:  long %f lat %f"%(
                        longitude, latitude)))
            except Exception as e:
                print("Unable to access timezone")
                raise e

    def xml(self):
        "Return XML representation"
        return self.xml_timezone
        
        
        
        