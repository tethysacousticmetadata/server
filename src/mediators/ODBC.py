'''
Created on Dec 27, 2014

@author: mroch

Examples:
---------------------------------------------------------------------------
from mediators import ODBC

Excel =====================================================================
# Create connector
c = ODBC.connector.MicrosoftExcel(DBQ="C:/users/mroch/Desktop/tmp.xlsx")
# Create ODBC connection
x = ODBC.source(c)
rows = x.sql("select * from [Effort$]")
for r in rows:
    print r

Comma separated value file (csv) ==========================================
Note that the connector specifies a directory and the files are
treated as if they were tables in the directory.  More than one
file can be accessed from the same directory.
Example CSV file:
c = ODBC.connector.TextDriver(DBQ="C:/Users/mroch/Documents/")
x = ODBC.source(c, True)
rows = x.sql("select * from [jo.csv]")
for r in rows:
     print r

For other delimiters (e.g. tab), one can write a Schema.ini file in the
the directory. Sample for tab delimited file jo.tab
[jo.tab]
Format=TabDelimited

Other delimiters are possible, see the Microsoft documentation for Schema.ini

Access ====================================================================

MySQL =====================================================================
---------------------------------------------------------------------------
'''
from collections import defaultdict
import os
import re
import warnings

# site packages
import pyodbc

from REST.Resources import DriverType

class Drivers:
    def __init__(self):
        self.system_drivers = pyodbc.drivers()

    def __getitem__(self, item):
        """
        Access ODBC driver based on a portion of the name, see get() for rules
        :param item:  ODBC substring
        :return: {driver specification string}
        """
        return self.get(item, braces=True)

    def get(self, name, braces=False):
        """
        Given a portion of a driver name, retrieve the full ODBC driver name.
        When multiple matches are possible:
         Unicode drivers are given preference over ANSI
            (requires the system_driver name to have unicode/ansi in the string)
         A name that starts the system driver will be given preference.  e.g.,
           Given system drivers "SQL Server" and "MySQL ODBC ..."
           name = "SQL" will match SQL Server

        Raises a ValueError if there is no match or the name is ambiguous
        and matches more than one driver.

        :param name:
        :param braces:  If True, surrounds driver name with { }
        :return: odbc driver name
        """

        # name can contain regular expression characters, escape them
        # we are mainly using re to provide case independent search
        name = re.escape(name)
        search_re = re.compile(name, re.IGNORECASE)
        drivers = list(filter(search_re.search, self.system_drivers))

        if len(drivers) > 0:
            # Look for drivers that start with name
            leftmost_re = re.compile(r"^"+name, re.IGNORECASE)
            leftmost = list(filter(leftmost_re.search, drivers))
            if len(leftmost):
                drivers = leftmost  # found some

        if len(drivers) > 0:
            unicode_re = re.compile(r"unicode", re.IGNORECASE)
            unicode = list(filter(unicode_re.search, drivers))
            if len(unicode):
                drivers = unicode

        if len(drivers) > 1:
            raise ValueError(
                f'"{name}" matches multiple drivers: {", ".join(drivers)}')
        elif len(drivers) == 0:
            raise ValueError(
                f'"{name}" does not match drivers: " +'
                ", ".join(self.system_drivers))

        if braces:
            driver = "{" + drivers[0] + "}"
        else:
            driver = drivers[0]
        return driver


drivers = Drivers()  # singleton

properties = {
            "workbook" : '"Excel 12.0;HDR=Yes;IMEX=1"',
            "text" : "asc,csv,tab,txt"
            }
           
class Connector:
    
    # pyodbc and autocommit
    # From the pyodbc FAQ:  https://code.google.com/p/pyodbc/wiki/FAQs
    #
    # ODBC drivers always start with autocommit turned on, 
    # but the Python DB API specification requires it to be turned off. 
    # After a connection is made, pyodbc first attempts to turn off 
    # autocommit before returning the connection. 
    #
    # If the driver does not support transactions, such as the 
    # Microsoft Excel driver, you will see an error like the following:
    # 
    #     pyodbc.Error:('HYC00','[HYC00][Microsoft][ODBC Excel Driver]
    #    Optional feature not implemented (106) 
    #    (SQLSetConnectAttr(SQL_ATTR_AUTOCOMMIT))')
    #
    # This can be remedied by telling pyodbc not to change the connection
    # to turn it off:
    # cnxn = pyodbc.connect('DSN=test', autocommit=True)

    @classmethod
    def isNoneOrEmpty(cls, string=None):
        return string.strip() is None or string.strip() == ''

    @classmethod
    def ValidateOdbcInputs(cls, driver=None, formvars=None):
        errors = dict()

        if driver.value == DriverType.Excel.value:
            errors['errors'] = cls.MicrosoftExcelVal(formvars)
        elif driver.value == DriverType.MySql.value:
            errors['errors'] = cls.MySQLVal(formvars)
        elif driver.value == DriverType.SqlServer.value:
            errors['errors'] = cls.SqlServerVal(formvars)
        elif driver.value == DriverType.Access.value or driver.value == DriverType.AccessDBase.value:
            errors['errors'] = cls.MicrosoftAccessVal(formvars)
        elif driver.value == DriverType.Postgres.value:
            errors['errors'] = cls.PostgresVal(formvars)
        elif driver.value == DriverType.Text.value:
            errors['errors'] = cls.TextVal(formvars)
        else:
            errors['errors'] = ['Driver type not recognized']

        return errors

    @classmethod
    def MicrosoftExcelVal(cls, formvars=None):
        driver_name = formvars.get("driverName", None)
        db_path = formvars.get("dbFileName", None)
        source_name = formvars.get("sourceName", None)

        errors = []
        if cls.isNoneOrEmpty(driver_name):
            errors.append("Driver is required")
        if cls.isNoneOrEmpty(db_path):
            errors.append("Path to excel file is required")
        if cls.isNoneOrEmpty(source_name):
            errors.append("Source Name is required")

        return errors

    @classmethod
    def MicrosoftAccessVal(cls, formvars=None):
        driver_name = formvars.get("driverName", None)
        db_path = formvars.get("dbFileName", None)
        source_name = formvars.get("sourceName", None)

        errors = []
        if cls.isNoneOrEmpty(driver_name):
            errors.append("Driver is required")
        if cls.isNoneOrEmpty(db_path):
            errors.append("Path to database file is required")
        if cls.isNoneOrEmpty(source_name):
            errors.append("Source Name is required")

        return errors

    @classmethod
    def TextVal(self, formvars=None):
        driver_name = formvars.get("driverName", None)
        db_path = formvars.get("dbFileName", None)
        source_name = formvars.get("sourceName", None)

        errors = []
        if self.isNoneOrEmpty(driver_name):
            errors.append("Driver is required")
        if self.isNoneOrEmpty(db_path):
            errors.append("Path to txt file is required")
        if self.isNoneOrEmpty(source_name):
            errors.append("Source Name is required")

        return errors;

    @classmethod
    def MySQLVal(self, formvars=None):
        driver_name = formvars.get("driverName", None)
        server = formvars.get("server", None)
        database = formvars.get("dbName", None)
        port = formvars.get("port", None)
        uid = formvars.get("username", None)
        pwd = formvars.get("password", None)
        source_name = formvars.get("sourceName", None)

        errors = []
        if self.isNoneOrEmpty(driver_name):
            errors.append("Driver is required")
        if self.isNoneOrEmpty(server):
            errors.append("Server name is required")
        if self.isNoneOrEmpty(database):
            errors.append("Database name is required")
        if self.isNoneOrEmpty(port):
            errors.append("Port is required")
        if self.isNoneOrEmpty(uid):
            errors.append("Username is required")
        if self.isNoneOrEmpty(pwd):
            errors.append("Password is required")
        if self.isNoneOrEmpty(source_name):
            errors.append("Source Name is required")

        return errors;

    @classmethod
    def SqlServerVal(self, formvars=None):
        driver_name = formvars.get("driverName", None)
        server = formvars.get("server", None)
        database = formvars.get("dbName", None)
        uid = formvars.get("username", None)
        pwd = formvars.get("password", None)
        trusted_connection = formvars.get("trustedConnection", None)
        source_name = formvars.get("sourceName", None)

        errors = []
        if self.isNoneOrEmpty(driver_name):
            errors.append("Driver is required")
        if self.isNoneOrEmpty(server):
            errors.append("Server name is required")
        if self.isNoneOrEmpty(database):
            errors.append("Database name is required")
        if self.isNoneOrEmpty(uid):
            errors.append("Username is required")
        if self.isNoneOrEmpty(pwd):
            errors.append("Password is required")
        if self.isNoneOrEmpty(trusted_connection):
            errors.append("Choosing authentication type is required")
        if self.isNoneOrEmpty(source_name):
            errors.append("Source Name is required")

        return errors;

    @classmethod
    def PostgresVal(self, formvars=None):
        driver_name = formvars.get("driverName", None)
        server = formvars.get("server", None)
        database = formvars.get("dbName", None)
        port = formvars.get("port", None)
        uid = formvars.get("username", None)
        pwd = formvars.get("password", None)
        trusted_connection = formvars.get("trustedConnection", None)
        source_name = formvars.get("sourceName", None)

        errors = []
        if self.isNoneOrEmpty(driver_name):
            errors.append("Driver is required")
        if self.isNoneOrEmpty(server):
            errors.append("Server name is required")
        if self.isNoneOrEmpty(database):
            errors.append("Database name is required")
        if self.isNoneOrEmpty(port):
            errors.append("Port is required")
        if self.isNoneOrEmpty(uid):
            errors.append("Username is required")
        if self.isNoneOrEmpty(pwd):
            errors.append("Password is required")
        if self.isNoneOrEmpty(trusted_connection):
            errors.append("Choosing authentication type is required")
        if self.isNoneOrEmpty(source_name):
            errors.append("Source Name is required")

        return errors

    @classmethod
    def GetConnector (cls, formvars=None, tempFileLocation=None):
        """
        Given a dictionary with ODBC parameters, create a Connector instance
        :param formvars:
        :param tempFileLocation:
        :return:
        """
        driver_name = drivers[formvars.get("driverName", "")]
        server = formvars.get("server", None)
        database = formvars.get("dbName", None)
        port = formvars.get("port", None)
        uid = formvars.get("username", None)
        pwd = formvars.get("password", None)
        db_path = tempFileLocation
        trusted_connection = formvars.get("trustedConnection", None)

        driver_lc = driver_name.lower()  # for case independent comparison
        if "excel" in driver_lc:
            return cls.MicrosoftExcel(DBQ=db_path)
        elif "mysql" in driver_lc:
            return cls.MySQL(Driver=driver_name, Server=server, Database=database, Port=port, UID=uid, PWD=pwd)
        elif "sql server" in driver_lc:
            return cls.SQLServer(Driver=driver_name, Server=server, Database=database, UID=uid, PWD=pwd, Trusted_Connection=trusted_connection)
        elif "microsoft access text" in driver_lc:
            # Important to check for Microsoft Access Text Driver before
            # Microsoft Access Driver
            return cls.TextDriver(Driver=driver_name, DBQ=db_path)
        elif "microsoft access" in driver_lc:
            return cls.MicrosoftAccess(Driver=driver_name, DBQ=db_path)
        elif "postgres" in driver_lc:
            return cls.PostgreSQL(Driver=driver_name, Server=server, Database=database, Port=port, UID=uid, PWD=pwd)
        else:
            return cls.ConnectionString(formvars)

    @classmethod
    def MySQL(cls, Provider=None, Driver=None,
              Server=None,
              Database=None,
              Port=None,
              UID=None,
              PWD=None,
              Option=None
              ):
        # Works with MySQL 4.1 and greater
        # Use Provider MSDASQL for 64 bit according to www.connectionstrings.com
        if Driver is None:
            Driver = drivers['MySQL']

        connectionStr = cls.__BuildConnectionStr__([
            "Provider", "Driver", "Server", "Database", "Port",
            "UID", "PWD", "Option"], locals())
        if Database is None:
            Database = "MySQL database"

        return Connector(connectionStr, Database)

    @classmethod
    def MicrosoftExcel(cls, Driver=None, DBQ=None):
        """
        Open a Microsoft Excel workbook, DBQ is the spreadsheet filename
        :param driver:  ODBC driver string
            e.g., '{Microsoft Access Driver (*.mdb, *.accdb)}'
        :param DBQ:  Database filename
        :return: Connector object instance
        """
        if Driver is None:
            Driver = drivers['excel']

        if DBQ is None:
            raise ValueError("Path to Excel file (DBQ) must be specified")

        [_, ext] = os.path.splitext(DBQ)

        # HDR - header row?
        # IMEx - treat everything as text? 1 yes 0 no
        ext = ext.lower()
        if ext == ".xls":
            Extended_Properties = '"Excel 8.0;HDR=YES;IMEX=1"'
        elif ext == ".xlsm":
            Extended_Properties = '"Excel 12.0 Macro;HDR=YES;IMEX=1"'
        elif ext == ".xlsx":
            Extended_Properties = '"Excel 12.0;HDR=YES;IMEX=1"'
        else:
            # Don't know extension, hope it works...
            Extended_Properties = '"Excel 12.0;HDR=YES";IMEX=1'

        connection_str = cls.__BuildConnectionStr__(
            ["Driver", "DBQ", "Extended_Properties"], locals(),
            Underscore2Space=True)

        # Autocommit on as no transaction support
        return Connector(connection_str, DBQ, additionalParams={'autocommit': True})

    @classmethod
    def MicrosoftAccess(cls, Driver=None, DBQ=None):
        """
        Open a Microsoft Access database, DBQ is the database filename"
        :param Driver:  ODBC driver string
            e.g., '{Microsoft Access Driver (*.mdb, *.accdb)}'
        :param DBQ:  Database filename
        :return: Connector object instance

        """
        if Driver is None:
            Driver = drivers['access driver']

        connection_str = cls.__BuildConnectionStr__(
            ["Driver", "DBQ"], locals())

        return Connector(connection_str)

    @classmethod
    def TextDriver(cls, Driver=None, DBQ=None):
        "TextDriver - Open a tab or comma separated value list"

        if Driver is None:
            Driver = drivers["text"]

        Extensions = '"' + properties["text"] + '"'
        connectionStr = cls.__BuildConnectionStr__(
            ["Driver", "DBQ", "Extensions"], locals(),
            Underscore2Space=True)

        # if DBQ is None:
        #     raise ValueError("Path to csv file (DBQ) must be specified")
        # elif not os.path.isdir(DBQ):
        #     raise ValueError("Text directory file (DBQ) does not exist")

        # Autocommit on as no transaction support
        return Connector(connectionStr, DBQ, additionalParams={'autocommit': True})

    @classmethod
    def SQLServer(cls, Driver=None,
              Server=None,
              Database=None,
              UID=None,
              PWD=None,
              Trusted_Connection=None
              ):

        if Driver is None:
            Driver = drivers["SQL Server"]

        connectionStr = cls.__BuildConnectionStr__([
            "Driver", "Server", "Database",
            "UID", "PWD", "Trusted_Connection"], locals())

        return Connector(connectionStr)

    @classmethod
    def PostgreSQL(cls, Driver=None,
              Server=None,
              Database=None,
              Port=None,
              UID=None,
              PWD=None
              ):
        "Open a Postgres database, DBQ is the database filename"

        if Driver is None:
            Driver = drivers['Postgres']

        connectionStr = cls.__BuildConnectionStr__([
            "Driver", "Server", "Database", "Port",
            "UID", "PWD"], locals())

        return Connector(connectionStr)

            
    @classmethod
    def ConnectionString(cls, String=None, additionalParams=dict()):
        """ConnectionString - Connector with user specified connection string
        see www.connectionstring.com for examples of strings for different
            data sources
        """

        if String is None:
            raise ValueError("Connection string must be specified")
        else:
            return Connector(String, "User specified connection string", additionalParams)

    @classmethod
    def __BuildConnectionStr__(cls, paramlist, valuedict,
                               Underscore2Space=False,
                               additionalParams=dict()):
        """Internal method for constructing a list of connection
        items.  Expects ordered names connection parameters to build
        into a connection string based on a dictionary of values.
        """
        
        connections = []
        for name in paramlist:
            try:
                value = valuedict[name]
                if name in ["Database", "DBQ", "Data_Source"]:
                    value.replace('\\', '\\\\')  # Escape backslash if present
                if Underscore2Space:
                    name = name.replace('_', ' ')
                connections.append("%s=%s;" % (name, value))
            except KeyError:
                pass  # Shouldn't get here
        
        return "".join(connections) 
        
    def __init__(self, connection_str, name="ODBC Connector",
                 additionalParams=dict()):
        """
        build a connection string object
        additionalParams are additional keyword/value parameters
        that will be passed on connection.
        """
        self.connection_str = connection_str
        self.source = name,
        self.params = additionalParams

        
    def get_connector(self):
        "Return the connection string"
        return self.connection_str
    
    def get_source(self):
        return self.source

  
class source(object):
    """ODBC data source"""

    password_re = re.compile("(<?Pre>.*Password=)[^;](<?Post>;.*)", re.I)

    def __init__(self, connector, debug=False):
        """Creates an ODBC data source that can be queried with SQL.
        connector- a valid ODBC connector instance, 
            Note that drivers must be installed on the machine
        debug - Show debug messages?

        Example:
            connector = MicrosftExcel(DBQ="path/to/excel.xlsx")
            odbc_source = source(connector)
            table_list = odbc_source.get_tables()
            data = RowDataFetchMany("name for data source, e.g. excel.xlsx", odbc_source, ...?)
            for r in data:
                print(r)

        """
        
        # Remember who you are...
        self.source = connector.get_source()
        self.db = None
        self.debug = debug
        
        try:
            self.db = pyodbc.connect(connector.get_connector(), **connector.params)
        except Exception as e:
            # Remove password prompt if present and print the connection string
            conn = connector.get_connector()
            m = self.password_re.match(conn)

            if m is not None:
                conn = m.group("Pre") + "<PasswordOmitted>" + m.group("Post")
            print("Connection string:  %s"%(conn))            
            
            known_sources = []
            for k, v in list(pyodbc.dataSources().items()):
                known_sources.append("%25s\t%s" % (k, v))
            known_sources = "\n".join(known_sources)
            
            raise

    def sql(self, query):
        "sql - Run sql query"
        
        # Make sure user isn't trying to modify database
        queryl = query.lower()  # lower case
        if "insert" in queryl:
            raise ValueError('insert is not permitted in table queries')
        if "delete" in queryl:
            raise ValueError('delete is not permitted in table queries')

        try:
            table = self.db.execute(query)
        except Exception as e:
            if self.debug:
                print(e)
            raise ValueError(
                "Unable to execute data query:\n" 
                f"{query}"
                "\n\nServer responded:"
                f"{e}"
                "\nVerify SQL syntax and that table/sheet and "
                "selected fields exist")
        
        self.current_stream = query  # Note query in case of errors
        return RowDataFetchMany(query, table)

    def get_tableColumnDict(self, cursor, driver):
        if driver in (DriverType.Text, DriverType.Excel):
            # These ODBC sources are not relational databases and do not require
            # any restriction on the table query
            return self.get_tablesColumns(cursor)
        else:
            # Restrict query to tables, ignoring views and other types of
            # objects
            return self.get_tablesColumns(cursor, True)

    def get_excel_tables(self, cursor):
        """
        get_tables() - Return a list of tables in the current database
        :return:
        """
        name_idx = 2  # table names returned in this entry of tuples
        table_names = [tup[name_idx] for tup in cursor.tables()]

        return table_names


    def get_tables(self):
        """
        get_tables() - Return a list of tables in the current database
        :return:
        """
        cursor = self.db.cursor()
        name_idx = 2  # table names returned in this entry of tuples
        table_names = [tup[name_idx] for tup in cursor.tables()]

        return table_names

    def get_tablesColumns(self, cursor, additionalParams=False):
        """
        get_tablesColumns() - Return a list of tables and their columns
        in the current database
        :return: dict of key:tableName, value: [columns]
        """

        name_idx = 2  # table names returned in this entry of tuples
        if additionalParams:
            tableNames = [tup[name_idx] for tup in cursor.tables(tableType = "TABLE")]
        else :
            tableNames = [tup[name_idx] for tup in cursor.tables()]

        # List of (table/view name, attribute)
        tableColumns = [(tup[2], tup[3]) for tup in cursor.columns()]
        cdDict = defaultdict(list)
        for tbl_attr in tableColumns:
            # append attribute to table entry
            cdDict[tbl_attr[0]].append(tbl_attr[1])

        return cdDict

    def __del__(self):
        self.close()

    def close(self):
        if self.db is not None:
            self.db.close()
            self.db = None
                    

class RowData:
    "rowdata - Interface for row data from an ODBC query"

    # SQL select parser regexps
    # Split select from
    select_re = re.compile(r"select\s+(?P<select>.*)\s+from\s+(?P<tables>.*);?",
                           flags=re.IGNORECASE|re.MULTILINE)
    # Parse individual selection variables with possible as clauses
    var_re = re.compile(
        r"\s*(?P<name>[\*\w`._$]+)(\s+.*?as\s+)(?P<rename>\w+)?",
        flags=re.IGNORECASE | re.MULTILINE)
    # Find wildcards in SQL query, examples:
    #  select * FROM table;
    #  select t.*, s.attrib from ... (some join of t & s)
    wildcard_re = re.compile(r"^\s*([\w]+\.)?\*\s*")

    def __init__(self, stream, cursor, headers=None, firstrow=1):
        """
        RowData - Used for iterating over rows of a SQL cursor
        Pass in a stream name, an iterable cursor, optional headers
           and optionally the row_num on which we should start counting
           which is useful for data sources that have headers
        :param stream: stream name identifies the data source, e.g. table name
        :param cursor: data cursor
        :param headers: column name information, if None will be retrieved
           from cursor.columns()
        :param firstrow: first row of data to process (data start at row 0,
           use 1 to skip a header row)
        """

        self.stream = stream
        self.cursor = cursor
        self.headers = headers
        self.row_num = firstrow - 1
        self.headers = headers
        self.iterated = False

        # Split SQL command into select and from
        m = self.select_re.match(self.stream)
        if m is None:
            raise ValueError(f'ODBC module unable to parse SQL query "{self.stream}"')

        # process attribute columns that are projected by select
        self.wildcard_cols = []  # List of column indices with *
        self.headers = []  # attribute list
        columns = m.group("select").split(',')
        for idx, col in enumerate(columns):
            col = col.strip()  # Remove any leading/trailing whitespace
            # Check to see if we renamed columns using an AS statement
            m = self.var_re.search(col)
            if m is None:
                # No syntax to rename field AS another, check for wildcards
                wcmatch = self.wildcard_re.search(col)
                if wcmatch is not None:
                    self.wildcard_cols.append(idx)
                else:
                    self.headers.append(col)
            else:
                self.headers.append(m.group("rename"))

        if len(self.wildcard_cols) > 1:
            raise ValueError(f"Tethys does not support SQL queries with multiple * fields: {self.stream}")

    def __iter__(self):
        if self.iterated:
            raise RuntimeError("Cannot iterate over a SQL query more than once")
        self.iterated = True
        return self
    
    def row_idx(self):
        # Return current row_num index
        return self.row_num
    
    def __next__(self):
        """
        Retrieve the next line of data.
        Blank rows are skipped over
        :return: dict[field_name] = value
        """
        # Get the next line of data.  We skip over blank rows, so sometimes
        # we need to to grab multiple rows
        nodata_yet = True
        while nodata_yet:
            row = next(self.cursor)
            self.row_num = self.row_num + 1

            if self.row_num == 1:
                self.finalize_headers()

            if isinstance(row, dict):
                row_dict = row
            else:
                # package up with headers
                row_dict = dict(list(zip(self.headers, row)))

            # Skip lines with no data
            nodata_yet = all(v is None or v == "" for v in list(row_dict.values()))

        return row_dict

    def finalize_headers(self, row):
        """
        finalize_headers - If a SQL query contains a wildcard attribute selection, e.g.
        SELECT * FROM mytable;
        we need to determine the attribute names from the result.  We do not have this
        until the first read of the ODBC cursor has been made.

        This function may modify self.headers, replacing wildcard entries with the
        corresponding table attribute (column) names.

        :param row: ODBC row result
        :return: None
        """

        if len(self.wildcard_cols) > 0:
            # The user wrote something along the lines of the following:
            # SELECT * from ...
            # SELECT t.a, t.b, u.* FROM t INNER JOIN u ON ...
            # SELECT t.a, u.*, t.b FROM t INNER JOIN u ON ...
            # Note that we will not handle multiple *'s well as they can be replaced
            # by a single one:
            # SELECT * from t INNER JOIN u ON ...

            # Retrieve the column names from ODBC
            attribs = [a[0] for a in row.cursor_description]

            leftN = self.wildcard_cols[0]
            # Number of non-wildcard columns to the right of the * in the SELECT statement
            to_right = len(self.headers) - self.wildcard_cols[0] - 1
            rightN = len(attribs) - to_right
            # Assemble new header list
            new_headers = self.headers[0:leftN]
            new_headers.extend(attribs[leftN:rightN])
            if to_right > 0:
                new_headers.extend(self.headers[leftN+1:])
            self.headers = new_headers

class RowDataFetchMany(RowData):
    """
    Derived from rowdata, uses the fetchmany attribute of a cursor.
    Much faster than rowdata which takes a severe performance hit
    """


    def __init__(self, stream, cursor, headers=None, firstrow=1):
        """Pass in a stream name, an iterable cursor, optional headers 
           and optionally the row_num on which we should start counting
           which is useful for data sources that have headers

           add flag that says include table name
        """
        super().__init__(stream, cursor, headers, firstrow)
        self.N = 5000  # default count of rows to fetch each iteration
        self.group_idx = 0  # current index into rows
        self.rows = None  # current set of read data


    def setFetchCount(self, N):
            "setFetchCount - define number of rows to get at a time"
            self.N = N

    def __next__(self):
        """
        next() - Return the next element
        :return:
        """

        nodata_yet = True
        while nodata_yet:
            first_block = self.rows is None  # Initial block of data
            if first_block or self.group_idx >= len(self.rows):
                # First read or we reached end of previous block
                self.rows = self.cursor.fetchmany(self.N)
                if len(self.rows) == 0:
                    raise StopIteration  # no more rows
                self.group_idx = 0

            row = self.rows[self.group_idx]
            self.group_idx += 1
            self.row_num += 1

            if first_block:
                self.finalize_headers(row)

            if isinstance(row, dict):
                row_dict = row
            else:
                # package up with headers
                row_dict = dict(list(zip(self.headers, row)))

            # Skip lines with no data
            nodata_yet = all(v is None or v == "" for v in list(row_dict.values()))

        return row_dict

class CSVparser:
    
    data_source = "Table"  # Keyword to indicate a data source
            
    def __init__(self, methods, fname, translator=None, speciesabbr=None,
                 debug=False):

        """Creates a data_parser specialized for ODBC connections.
        methods - an XMLMethods instance
        connector - an connector object that specifies how the 
            connection should be made
        translator - Use the specified translator from collection('SourceMaps')
           to parse a data source consisting of tuples.  Source can
           be anything that ODBC can query.
        speciesabbr - How species names are encoded
        """
        
        # Remember who you are...
        self.source = fname
        self.current_stream = "CSV"
        # data_parser.__init__(self, methods, translator, speciesabbr, debug)
        
    def get_source(self):
        """Return an iterator that provides a dictionary for each row
         with field names as keys"""
        self.fileh = open(self.source, 'r')
        # TODO - fix
        raise ValueError("todo")
        reader = None
        #reader = csv.DictReader(self.fileh)        
        return reader
    
    def close(self):
        self.fileh.close()  
        
    def __del__(self):
        self.close()  
        


        
