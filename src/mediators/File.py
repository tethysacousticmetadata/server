'''
Created on Dec 28, 2014

@author: mroch
'''

# Python standard libary
import os
import tempfile
from threading import Lock
from dbxmlInterface.error import DocumentFailure


# Tethys modules
import REST.util
 
class stream(object):
    "Object for managing a stream"
    def __init__(self, filename=None, fileobj=None):
        "stream object for a file or open file object"
        if filename and fileobj:
            raise ValueError("Cannot specify filename and file object")
        if filename:
            self.fileobj = open(filename, 'r')
        elif fileobj:
            self.fileobj = fileobj
        else:
            raise ValueError("Must specify filename or file object")
        self.lock = Lock()  # Create lock for mutex
        
        
    def reader(self, position = 0):
        "reader - Return a new stream reader at position bytes into file"
        return streamReader(self, position)
    
    def acquire(self, blocking=True):
        return self.lock.acquire(blocking)
    
    def release(self):
        self.lock.release()
        
    def read(self):
        """Wrapper method. creates a reader and returns the stream as a string"""
        reader = self.reader()
        data = reader.read()
        return data
        
    def read_from(self, bytecount=-1, position=None):
        """"
        Read # bytecount at position bytecount into file, returns (data, newposition)
        If bytecount is omitted or negative, the entire remaining file is read.
        If position is omitted or None, the file is read from the
            current position
        """
            
        if (position or position==0) and self.fileobj.tell() != position:
            self.fileobj.seek(position)
        return (self.fileobj.read(bytecount), self.fileobj.tell())
    
    def read_line(self, position=None):
        """
        read_line - Read one line at specified position
        Read from specified position to end of line.
        """
        if (position or position==0) and self.fileobj.tell() != position:
            self.fileobj.seek(position)
        return (self.fileobj.readline(), self.fileobj.tell())
    def close(self):
        self.fileobj.close()     

    
class streamReader(object):
    """
    streamReader - Manages a position on a stream, thus permitting
    multiple readers for the same stream.
    """
    
    def __init__(self, streamhandle, position):
        self.stream = streamhandle
        self.position = position
        
    def read(self, bytecount=-1):
        self.stream.acquire()  # get sole ownership
        (data, self.position) = self.stream.read_from(bytecount, self.position) 
        self.stream.release()  # release ownership
        return data
    
    def readline(self):
        self.stream.acquire()  # get sole ownership
        (data, self.position) = self.stream.read_line(self.position) 
        self.stream.release()  # release ownership
        return data

    def __iter__(self):
        "iterator - not designed for multiple iterators on this object"
        return self

    def __next__(self):
        "return next line of file"
        data = self.readline()
        if data == "":
            raise StopIteration()
        else:
            return data
        
        
def StreamToTempFile(fileH, ext):
    """"
    StreamToTempFile - Copy an open file handle to a temporary file with a designated extension
    Returns the temporary filename
    """
    
    (f, tmpfname) = tempfile.mkstemp(ext)
    REST.util.copy_stream(f, fileH)
    os.close(f)
    return tmpfname

def StreamToFile(fileH, pathtofile):
    """
    StreamToFile - Copy an open file handle to the specified file
    """
    #f = open(pathtofile, "wb")
    ##changed to os.open because os.write,in copy_stream, needs a integer file descriptor.
    ##seemed to not write file properly, but fixed after I added binary flag
    f = os.open(pathtofile, os.O_WRONLY| os.O_CREAT | os.O_BINARY) #write only, create if not exist, binary
    REST.util.copy_stream(f, fileH)
    os.close(f)
    ##returning pathtofile, since this function returns to a dict in FileImport.py
    return pathtofile
    
def RemoveTempFile(fname, docname, xmlsink):
    "RemoveTempFile - Delete a temporary file."
    if fname is not None:
        try:
            os.remove(fname)
        except Exception as e:
            DocumentFailure(xmlsink, docname, "Warning", "Unable to remove server temporary document")

                    
        
 