'''
Created on Sep 30, 2009

@author: mroch
'''

import json
import re
import io
import datetime

from xml.sax import make_parser, parseString
from xml.sax.handler import ContentHandler, EntityResolver

# our modules
import dbxmlInterface.XmlMethods   # Use utility fn for splitting arguments

# 3rd party modules
import maya  # time manipulation
import requests  # HTTP for Humans package

class timespan(object):
    def __init__(self, start, stop, what):
        self.start = start
        self.stop = stop
        self.what = what
    
    def endsBeforeStart(self, other):
        return self.stop < other.start

    def endsAfterStart(self, other):
        return self.stop > other.start

    def endsBeforeEnd(self, other):
        return self.stop > other.stop

    def startsBeforeStart(self, other):
        return self.start < other.start

    def startsAfterStart(self, other):
        return self.start > other.star

    def startsBeforeEnd(self, other):
        return self.start < other.end

    def startsAfterEnd(self, other):
        return self.start > other.end

# XML handling modeled after sax-based XML parser for the BLAST algorithm
# by "The CompBio Dude"
# http://compbiodude.blogspot.com/2008/04/python-blast-sax-example.html
# Accessed Oct 10, 2009

class ephemerisEntityHandler(EntityResolver):
    def resolveEntity(self, publicId, systemId):
        return io.StringIO()

class ephemerisDielHandler(ContentHandler):
    """XML parser for ephemeris data"""

    def startDocument(self):
        self.timespans = []
        self.current = {}
        self.tag = None

    def endDocument(self):
        self.timespans.append((self.current, None))

    def startElement(self, name, attributes):
        if name == "entry" or name == "ephemeris":
            # New entry in table
            self.previous = self.current
            self.current = {}
            self.tag = None
        else:
            self.tag = name

    def characters(self, text):
        # add the string with whitespace removed

        #print "<%s>"%(text)
        #pdb.set_trace()

        if self.tag is not None:
            ntext = text.strip()
            if ntext != "":
                self.current[self.tag] = ntext

    def endElement(self, name):
        if name == "entry":
            self.timespans.append((self.previous, self.current))
        
def parseEphemeris(xmlsource, asString=False):
    """Parse XML stream (or string) of Ephemeris data
    Returns a list of timespan objects
    """

    # Object for handling stream events
    handler = ephemerisDielHandler()  

    # parse it!
    if asString:  
        # module function creates the parser for us and 
        # performs misc. voo-doo to to process the string
        parseString(xmlsource, handler)
    else:
        parser = make_parser()   # make the parser
        parser.parse(xmlsource, handler)

    return handler.timespans  # return results



class ephemeris:
    """mediator to provide ephemeris data from NASA JPL Horizons service"""

    # class variables --------------------
    debug = False

    # string conversions not yet used
    lat_re = re.compile(r"(?P<dir>[wWeE])?(?P<deg>[+-]?\d*(\.\d*)?)")
    long_re = re.compile(r"(?P<dir>[nNsS])?(?P<deg>[+-]?\d*(\.\d*)?)")
    entry_re = re.compile(
        r" *(?P<time>\d\d\d\d\-(?P<month>...)-\d\d \d\d:\d\d) "
        r"(?P<sun>.)(?P<moon>.)(?P<rest>.*)")

    # Start/End of ephemeris tags
    batch_re = re.compile(r"\$\$SOE\n(.*)\n\$\$EOE", re.DOTALL)

    # JPL Horizons service
    server = "ssd.jpl.nasa.gov"
    timeout_s = 10
    
    # SOLAR PRESENCE (OBSERVING SITE)
    # Time tag is followed by a blank, then a solar-presence symbol:
    # Note, when using rise, transit, set (RTS) stepping, Horizons ignores
    # events that occur when the tracked body is beneath the horizon.
    # Consequently, any of the definitions of twilight that rely on the sun
    # being beneath the horizon will not be shown with RTS stepping.
    # Horizons recommends using stepping no more than 5 min apart as that is
    # about the limit of the accuracy due to variables such as atmospheric
    # refraction.

    #   '*'  Daylight (refracted solar upper-limb on or above apparent horizon)
    #   'C'  Civil twilight/dawn
    #   'N'  Nautical twilight/dawn
    #   'A'  Astronomical twilight/dawn
    #   ' '  Night OR geocentric ephemeris
    sun_presence = {
        '*': 'day',
        ' ': 'night'
        }

    # Twilight symbols
    twilight = "CNA"
    twilight_names = {"C": "Civil",
                      "N": "Nautical",
                      "A": "Astronomical"}


    #   LUNAR PRESENCE WITH TARGET RISE/TRANSIT/SET MARKER (OBSERVING SITE)
    #   The solar-presence symbol is immediately followed by another 
    #   marker symbol:

    #   'm'  Refracted upper-limb of Moon on or above apparent horizon
    #   'r'  Rise    (target body on or above cut-off RTS elevation)
    #   't'  Transit (target body at or past local maximum RTS elevation)
    #   's'  Set     (target body on or below cut-off RTS elevation)
    #   ' '  Upper-limb is not refracted
    moon_presence = {
        'm': 'moon',
        'r': 'rise',
        't': 'transit',
        's': 'set',
        ' ': 'no-moon'
        }

    
    # Horizons identified celestial bodies by number
    # Create a dictionary for the ones we are likely to use.
    bodies = {
              "earth": 399,
              "luna": 301,
              "moon": 301,
              "sol": 10,
              "sun": 10
              }

    months = {'Jan': '01', 'Feb': '02', 'Mar': '03', 'Apr': '04',
              'May': '05', 'Jun': '06', 'Jul': '07', 'Aug': '08',
              'Sep': '09', 'Oct': '10', 'Nov': '11', 'Dec': '12'}

    debug = False

    # class methods --------------------
    @staticmethod
    def dms_deg(deg, m, s):
        """dms_deg - convert degrees minutes seconds to degrees"""
        return deg + m/60.0 + s/3600.0
    
    @staticmethod
    def latitudeDMS2Dec(deg, m, s):
        return ephemeris.dms_deg(deg, m, s)
            
    # instance methods --------------------
    def __init__(self, args=None):
        """horizons - Find information for target at longitude/latitude over
        specified time interval.  args consists of / separated name=value pairs:

        Partial list:
        target - desired body, e.g., moon
        latitude & longitude - observer location degrees 
            relative to E (0 - 360) & equator (+ -> N, - -> S)
        start and stop should be ISO 8601 time strings with the T separator
            example for UTC-8:  "2009-10-01T00:00-08:00" 
        interval - Specifies step time, e.g., 5m (min).  If Rise/Transit/Set
            events are of interest, we can use one of the following modes:
            TVH - True visual horizon which accounts for Earth curvature
                and refraction.
            GEO - Geometric horizon that includes refraction
            RAD - Radar horizon; same as GEO without refraction

        Complete list is available at in the JPL Horizons documentation
        https://ssd-api.jpl.nasa.gov/doc/horizons.html

        Example args:
        Show sunrise/sunset at 32.8° N 243.8° E for a few days in October 2009
        Rise and set are computed according the true visual horizon computed
        in 5 m intervals.
        target="sol"/latitude=32.8/longitude=243.8/start="2009-10-01T00:00-08:00"/stop="2009-10-04T00:00-08:00"/interval="5m tvh"

        """

        # List of possible argument names
        names = ['target', 'latitude', 'longitude',
                 'start', 'stop', 'interval']

        # Defaults
        self.altitude = 0  # assume sea level

        argpairs = dbxmlInterface.XmlMethods.namevalues(args)
        for arg in argpairs:
            if len(arg) != 2:
                raise ValueError(
                    "Bad ephemeris argument:  %s, expected name=value.  "%("=".join(arg)))
            if arg[0] not in names:
                raise ValueError("Bad ephemeris argument %s, not in [%s]"%(
                    arg[0], ",".join(names)))
            else:
                if arg[0] == 'target':
                    value = ephemeris.bodies[arg[1]]
                elif arg[0] in ['start', 'stop']:
                    value = maya.parse(arg[1]).datetime(naive=True)
                elif arg[0] in ['latitude', 'longitude']:
                    value = float(arg[1])
                else:
                    value = arg[1]
                setattr(self, arg[0], value)  # self.name = value

        if self.stop < self.start:
            raise ValueError("Interval stop is before interval start")

        # We may need to split the ephemeris query into smaller batches.
        # Each batch will be one entry in self.data
        self.data = list()

        # interval not critical, important thing is that NASA JPL
        # will time out if too long
        max_interval = datetime.timedelta(days=365.25)
        more = True
        current_start = self.start
        current_end = self.stop
        # Process query in batches of max_interval
        while more:
            duration = self.stop - current_start
            if duration > max_interval:
                current_end = min(current_start + max_interval, self.stop)
            else:
                current_end = self.stop
            if self.debug:
                print(f"GET ephemeris {current_start}:{current_end}")
            self.batch(current_start, current_end)
            current_start = current_end
            more = current_start < self.stop

    def batch(self, start, stop):
        """Get ephemeris using web-based protocol"""
      
        cmds = list()
        # cmds.append("batch=1")  # interface changed 2025-02
        cmds.append(f"COMMAND='{self.target}'")
        cmds.append("OBJ_DATA='NO'")
        cmds.append("MAKE_EPHEM='YES'")
        cmds.append("TABLE_TYPE='OBSERVER'")
        cmds.append("CENTER='Coordinate'")
        cmds.append("COORD_TYPE='GEODETIC'")
        cmds.append(f"SITE_COORD='{self.longitude},"
                    f"{self.latitude},{self.altitude}'")
        cmds.append(f"START_TIME='{start}'")
        cmds.append(f"STOP_TIME='{stop}'")
        
        cmds.append(f"STEP_SIZE='%s'"%(self.interval))
            
        items = []
        if self.target == self.bodies['moon']:
            cmds.append("QUANTITIES='1,4,9,10,20,23,24'")
            # We wish to pull the following numerical items from the CSV
            # presence, transition, apparent azimuth and elevation, 
            # apparent magnitude, surface brightness, and illumination
            items = [1, 2, 5, 6, 7, 8, 9]
        else:
            cmds.append("QUANTITIES='1,4,9,20,23,24'")
            # We wish to pull the following numerical items from the CSV
            # presence, transition, apparent azimuth and elevation
            items = [1, 2, 7, 8, 9]
        cmds.append("CSV_FORMAT='YES'")
        join = "&".join(cmds)

        URL = f"https://{self.server}/api/horizons.api?{join}"

        # JPL Horizons switched to using a self-signed certificate, do not verify
        response = requests.get(URL, verify=False)
        response.raise_for_status()   # Generate exception if bad request

        json_data = json.loads(response.text)  # Get results

        # Check results
        err_str = None
        err_tag = "NASA_JPL_Horizons_API"
        try:
            horizons_ver = "<version> " + \
                           f"{json_data['signature']['version']} </version>"
        except KeyError:
            horizons_ver = ""
        if "error" in json_data:
            err_str = json_data['error']
        if not "result" in json_data:
            err_str = "Ephemeris result not returned"
        if err_str is not None:
            raise EnvironmentError(f"<{err_tag}>\n{err_str}\n</{err_tag}>")

        # Find data in result output
        match = self.batch_re.search(json_data["result"])
        
        if not match:
            raise EnvironmentError(
                f"<{err_tag}>\n Ephemeris result from: {URL}"
                "\n did not contain start/end ephemeris markers"
                f"\n</{err_tag}")

        self.raw_data = match.group(1)
        lines = self.raw_data.split("\n")
        problems = False
        data_group = []
        for line in lines:
            m = []
            n = str.split(line, ',')
            for o in n:
                if not o == ' ':
                    o = o.strip()
                m.append(o)
            if m:
                p = str.split(m[0], '-')
                date = m[0].replace(
                    p[1], ephemeris.months[p[1]])
                datum = [maya.parse(date).datetime(naive=True)]
                # Add in other items that do not need to be parsed
                datum.extend([m[idx] for idx in items])
                data_group.append(datum)
            else:
                print("could not parse line")
                print(line)
                problems = True
                problemline = line

        self.data.extend(data_group)

    def xml(self, readable=True):
        """xml - Output ephemeris as XML
        If readable is set to True, rewrite sun/moon presence
        with human comprehensible codes, otherwise use NASA JPL encoding
        """

        strings = []
        entry = None  # put in scope 
        prev = None
        prev_moon = None
        if readable:
            # Distinguish twilight/dawn when the first entry
            # is twilight.
            if self.data[0][1] in self.twilight:
                # Twilight can occur after dusk and before dawn
                # Determine whether the previous state was day or night
                # by searching for the next day/night indicator and flipping it
                found = False
                sidx = 1
                day_night_indicators = list(self.sun_presence.keys())
                while not found and sidx < len(self.data):
                    if self.data[sidx][1] in day_night_indicators:
                        # Flip day/night
                        if self.data[sidx][1] == '*':
                            prev = self.sun_presence[' ']
                        else:
                            prev = self.sun_presence['*']
                        found = True
                    else:
                        sidx = sidx + 1

                if not found:
                    # We never saw a sun/night indicator, should not happen
                    # often.  Default to a value as opposed to guessing based
                    # on local time of day and long/lat
                    prev = 'night'

        # search ahead for the first instance to see if starting in no-moon or moon
        for line in self.data:
            if line[2] == 'r':
                prev_moon = None
                break;
            elif line[2] == 's':
                prev_moon = 'moon'
                break;
        
        for line in self.data:
            if readable:
                attribute = ""
                if line[1] in self.twilight:
                    sun = ephemeris.sun_presence[' ']  # before dawn or after sunset
                    attribute = f' twilight="{self.twilight_names[line[1]]}"'
                else:
                    sun = ephemeris.sun_presence[line[1]]
                prev = sun
                
                # Switch between no-moon and moon if a rise or set transition is found
                if (line[2] == 'r'):
                    prev_moon = 'moon'
                elif (line[2] =='s'):
                    prev_moon = None
                
                # if prev_moon shows that the moon is shown, set the presence to 'm', otherwise
                # keep the same presence
                moon_pres = line[2]
                if (line[2] not in ['r', 's', 't']):
                    if prev_moon:
                        moon_pres = 'm'
                    
                if (self.target == self.bodies['moon']):
                # Reformat with human readable solar/lunar information
                    entry = [line[0].strftime("%Y-%m-%d %H:%M:%S"), 
                             attribute, sun,
                             ephemeris.moon_presence[moon_pres]]
                    entry.extend(line[3:])
                else:
                    entry = [line[0].strftime("%Y-%m-%d %H:%M:%S"), 
                             attribute, sun,
                             ephemeris.moon_presence[moon_pres], line[3], line[4]]
            else:
                if (line[2] == 'r'):
                    prev_moon = 'moon'
                elif (line[2] =='s'):
                    prev_moon = None
                    
                moon_pres = line[2]
                if (line[2] not in ['r', 's', 't']):
                    if prev_moon:
                        line[2] = 'm'
                
                if (self.target == self.bodies['moon']):
                    entry = [line[0].strftime("%Y-%m-%d %H:%M:%S"), 
                             attribute, line[1], moon_pres]
                    entry.extend(line[3:])
                else:
                    entry = [line[0].strftime("%Y-%m-%d %H:%M:%S"), 
                             attribute, line[1], moon_pres, line[3], line[4]]
            # Add XML entry
            if (self.target == self.bodies['moon']):
                strings.append("""
                <entry>
                    <date>%s</date>
                    <sun%s>%s</sun>
                    <moon>%s</moon>
                    <azimuth_app>%s</azimuth_app>
                    <elev_app>%s</elev_app>
                    <app_mag>%s</app_mag>
                    <s_brt>%s</s_brt>
                    <illumination>%s</illumination>                    
                </entry>"""%(tuple(entry)))
            else:
                strings.append("""
                <entry>
                    <date>%s</date>
                    <sun%s>%s</sun>
                    <moon>%s</moon>
                    <azimuth_app>%s</azimuth_app>
                    <elev_app>%s</elev_app>
                </entry>"""%(tuple(entry)))

        # Merge entries and enclose in ephemeris tag
        return "<ephemeris>%s\n</ephemeris>"%("".join(strings))

        
if __name__ == '__main__':
    if False:
        h = ephemeris("sol", 32.8, 243.8,
                     "2010-2-01T00:00-08:00", 
                     "2010-2-04T00:00-08:00", 
                     "5m tvh")
        print(h.xml())
    h = ephemeris("moon", 32.8, 243.8,
                 "2010-2-01T00:00-08:00", 
                 "2010-2-04T00:00-08:00", 
                 "30m")
    print(h.xml())
    pdb.set_trace()
