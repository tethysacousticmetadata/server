"""
sun

Functions for computing solar ephemeris locally without relying on resources
external to the server.

Morris, B. M., et al. (2018). "astroplan: An Open Source Observation Planning
Package in Python." The Astronomical Journal 155(3): 128.
DOI:10.3847/1538-3881/aaa47e.

Note that Astroplan can take into account relative temperature and humidity.
This module does not take this into account and uses the defaults.
"""
# Add-on packages
from astroplan import Observer
import astropy.units as units
from astropy.coordinates import Angle, EarthLocation
from astropy.time import Time, TimeDelta
import maya
from datetime import datetime, timedelta
import multiprocessing
import xml.etree.ElementTree as ET

# Our packages
from XML.sink import Sink
import dbxmlInterface.XmlMethods
import benchmark


class Ephemeris:
    """Solar ephemeris data - provides rise/set times for any Earth-bound
    location and elevation.  Civil, nautical, and astronomical twilight
    times may be found by changing the horizon parameter"""
    # multiprocessing batch size
    seconds_in_six_months = 17093700.0
    timedelta_six_months = TimeDelta(seconds_in_six_months, format='sec')
    seconds_in_one_day = 86400.0
    timedelta_one_day = TimeDelta(seconds_in_one_day, format='sec')

    def __init__(self, args):
        """

        :param args: String indicating location and what should be computed
        Uses a keyword string latitude=X/longitude=X/start=X/stop=X/horizon=X
          latitude,
          longitude, - degrees (WGS84)
          elevation - optional observer height relative to WGS84 ellipsoid
             in meters.  Defaults to 0, mean sea level
          start,
          stop - be ISO 8601 timestamps, e.g. YYYY-MM-DDTHH:MM:SS
            and are assumed in UTC if a timezone specified is not given.
          horizon - optional elevation given in degree:arcminutes
            default -0.5666... horizon used by US Naval Observatory (-34 min)
               which the sun sets/rises taking into account refraction
            other useful values:
               civil twilight: -6
               nautical twilight:  -12
               astronomical twilight:  -18

        """

        # placeholders - will be set in args
        self.longitude = None
        self.latitude = None
        self.start = None
        self.stop = None

        # defaults
        self.horizon = Angle("-0:34d")  # US Naval Observatory
        self.elevation = 0  # mean sea level

        argpairs = dbxmlInterface.XmlMethods.namevalues(args)
        for arg in argpairs:
            if len(arg) != 2:
                raise ValueError(
                    "Bad ephemeris argument:  "
                    f"%{'='.join(arg)}, expected name=value")
            elif arg[0] in ['start', 'stop']:
                value = Time(maya.parse(arg[1]).datetime(naive=True))
            elif arg[0] in ['latitude', 'longitude', 'elevation']:
                value = float(arg[1])
            elif arg[0] == "horizon":
                value = float(arg[1]) * units.deg
            else:
                raise ValueError(f"Unknown ephemeris argument {arg[0]}")
            setattr(self, arg[0], value)  # self.name = value

        if self.longitude is None:
            raise ValueError("Expected longitude specification")
        if self.latitude is None:
            raise ValueError("Expected longitude specification")
        if self.start is None:
            raise ValueError("Expected start specification")
        if self.stop is None:
            raise ValueError("Expected stop specification")

        self.loc = EarthLocation.from_geodetic(
            lon=self.longitude * units.deg,
            lat=self.latitude * units.deg,
            height=self.elevation * units.m,
            ellipsoid='WGS84')
        self.obs = Observer(
            location=self.loc,
            name="observer",
            timezone="UTC",
            pressure=1*units.bar)

        cpu_max = multiprocessing.cpu_count()   # available cpu count
        cpu_usable_fraction = 0.5  # Don't use more than N% of cores
        # Set multiprocessing pool size and create pool
        self.pool_size = round(cpu_max * cpu_usable_fraction)
        self.pool = multiprocessing.Pool(self.pool_size)

    def xml(self):
        """
        :param self:
        :return:  Generate solar rise/set ephemeris XML
        """
        # creating batch params tuple for multiprocessing
        params = []

        stopwatch = benchmark.Timer()

        # Chunk times between start and stop
        current = self.start
        while current <= self.stop:
            end = current + Ephemeris.timedelta_six_months
            until = min(self.stop, end)  # end of interval
            params.append((current, until, self.obs, self.horizon))
            current = end

        if len(params) > 1:
            # Multiple batches, use pool to process
            with self.pool as pool:
                results = pool.map(process_batches, params)
            results = self.check_and_find_missing_entries(results)
        else:
            # No need to process in separate processes
            results = process_batches(params[0])

        # Generate final merged XML string
        final_result = f'<ephemeris>{("".join(results))}\n</ephemeris>'

        report_stopwatch(self.start, self.stop, stopwatch)

        return final_result

    def check_and_find_missing_entries(self, results):
        rise_set_values = ("day", "night")
        rise_set_fns = (self.obs.sun_rise_time, self.obs.sun_set_time)
        last_rise_set = []
        new_results = []
        for result in results:
            ephemeris = ET.fromstring(f'<ephemeris>{result}\n</ephemeris>')
            entries = ephemeris.findall(".//entry")
            if entries:
                first_entry = get_data_from_entry(entries[0])
                last_entry = get_data_from_entry(entries[-1])

                # Define a timedelta representing 12 hours
                twelve_hours = timedelta(hours=12)

                if last_rise_set and last_rise_set[1] == first_entry[1] and (
                        first_entry[0] - last_rise_set[0]) > twelve_hours:
                    index = 1 if last_rise_set[1] == "day" else 0
                    rise_set_time = rise_set_fns[index](
                        Time(last_rise_set[0]), 'next', horizon=self.horizon)
                    if not rise_set_time.mask:
                        sink = Sink()
                        write_entry(rise_set_time, rise_set_values[index], sink)
                        result = sink.getstr() + result
                last_rise_set = last_entry
            new_results.append(result)
        return new_results


def get_data_from_entry(entry):
    date_format = "%Y-%m-%dT%H:%M:%S.%fZ"
    rise_set_date = entry.find("date").text
    rise_set = entry.find("sun").text
    return [datetime.strptime(rise_set_date, date_format), rise_set]


def process_batches(parameters):
    sink = Sink()
    stopwatch = benchmark.Timer()
    # unwrapping tuple into parameters
    start, stop, obs, horizon = parameters
    current = start

    # The Observer class supports computing the next sun rise or set time,
    # but one must specify which is desired.  Consequently, we determine
    # which one is first and alternate between the two.
    rise_set_values = ("day", "night")
    rise_set_fns = (obs.sun_rise_time, obs.sun_set_time)

    rise_set_times = get_next_rise_set_time(
        current, stop, horizon, rise_set_fns)
    if len(rise_set_times) >= 2:
        earliest_idx = 0 if rise_set_times[0] < rise_set_times[1] else 1
        # update the current time
        current = rise_set_times[earliest_idx]
        while current <= stop:
            write_entry(current, rise_set_values[earliest_idx], sink)

            # Prepare next event:  rise->set or set->rise
            next_idx = (earliest_idx + 1) % len(rise_set_fns)
            # Update the rise_set_times to reflect the next time the current
            # event type occurs.  That is, if are processing a sunrise,
            # search for the next sunrise from the sunset time that we have
            # already calculated.  (Similar logic for processing sunset)
            current = rise_set_times[next_idx]  # timestamp next event type
            # Search for next occurrence of current event
            current_rise_set_times = get_next_rise_set_time(
                current, stop, horizon, [rise_set_fns[earliest_idx]])
            if len(current_rise_set_times) > 0:
                rise_set_times[earliest_idx] = current_rise_set_times[0]
                earliest_idx = next_idx
            else:
                break

    sink_str = sink.getstr()

    report_stopwatch(start, stop, stopwatch)
    return sink_str


def get_next_rise_set_time(current, stop, horizon, rise_set_fns):
    rise_set_times = []
    while len(rise_set_times) < len(rise_set_fns) and current <= stop:
        rise_set_times = []
        for fn in rise_set_fns:
            rise_set_time = fn(current, 'next', horizon=horizon)
            if not rise_set_time.mask:
                rise_set_times.append(rise_set_time)
        current += Ephemeris.timedelta_one_day
    return rise_set_times


def write_entry(rise_set_time, rise_set_value, sink):
    sink.xml.startElement("entry")
    sink.xml.newline()
    sink.xml.startElement("date")
    sink.xml.characters(f"{rise_set_time.to_value('isot')}Z")
    sink.xml.endElement("date")
    sink.xml.newline()
    sink.xml.startElement("sun")
    sink.xml.characters(rise_set_value)
    sink.xml.endElement("sun")
    sink.xml.newline()
    sink.xml.endElement("entry")


def report_stopwatch(start, stop, stopwatch):
    """

    :param start:  Astropy start time
    :param stop:  Astropy stop time
    :param stopwatch: benchamarks.timer
    :return:
    """

    # Get stopwatch and convert everything to seconds
    elapsed = stopwatch.elapsed()
    elapsed_s = elapsed.total_seconds() * units.second
    duration_s = (stop - start) * 24 * 3600 * units.second / units.day

    factor = float(duration_s / elapsed_s)
    print(f"Diel {start}-{stop} in {elapsed} ({factor:,.2f} X realtime)")


# Test code
if __name__ == "__main__":
    # Example ephemeris

    # Compare to US Naval Observatory
    # https://aa.usno.navy.mil/calculated/rstt/year?ID=AA&year=2022&task=0&lat=32.75&lon=-117.14&label=San+Diego%2C+CA&tz=0&tz_sign=-1&submit=Get+Data
    default = Ephemeris(
        "latitude=32.75/longitude=-117.14/"
        "start=2022-01-01T00:00:00Z/stop=2022-02-01T00:00Z")

    print(default.xml())

    # Examples for other types of twilight
    #
    # nautical = Ephemeris(
    #     "latitude=32.75/longitude=-117.14/"
    #     "start=2022-01-01T00:00:00Z/stop=2022-02-01T00:00Z/horizon=-12")
    #
    # civil = Ephemeris(
    #     "latitude=32.75/longitude=-117.14/"
    #     "start=2022-01-01T00:00:00Z/stop=2022-02-01T00:00Z/horizon=-12")
    #
    # astronomical = Ephemeris(
    #     "latitude=32.75/longitude=-117.14/"
    #     "start=2022-01-01T00:00:00Z/stop=2022-02-01T00:00Z/horizon=-18")