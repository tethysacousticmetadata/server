'''
Created on Oct 3, 2009

@author: mroch

Sample run given a comma separated value (CSV) file from the HARP deployment table:
$ cmd /c python csv_to_xml.py  --tags_first  --entry Fixed \ 
    --document-name Deployment -f HARP-DataSummary \
    source-docs/Deployment_Information.csv \
    source-docs/Deployment_Information.xml
    
    Generates an XML document <Document>
    Records from CSV file are in <Fixed> tags
    The first line of the CSV file contains field (tag) names.

'''


import pdb
import sys
import optparse
import re
import csv

from xml.sax.saxutils import XMLGenerator
from xml.sax.xmlreader import AttributesNSImpl

# various regular expressions --------------------
# bad tag characters
tag_replacechar_re = re.compile("[ ]")
tag_dropchar_re = re.compile("[\?\(\)\.]")
# double quoted material
quotes_re = re.compile('^"(?P<content>.*)"$')
# longitude/latitude 
longlat_re = re.compile("^(?P<deg>\d+)-(?P<min>\d+(\.\d*)?)\s*(?P<direction>[nswe])\s*$", re.I)
# time in field
timefield_re = re.compile(".*(time|date).*")
# non standard time specification
datetimeUS_re = re.compile("(?P<m>\d+)/(?P<d>\d+)/(?P<y>\d+) (?P<time>\d(\d?):\d\d:\d\d)")
# break out expedition & sites information
expedition_re = re.compile("((?P<Project>\D+[^0-9]*)(?P<Deployment>[0-9]+)-(?P<Site>\D.*))?")
# --------------------

def DataProcLogs_tags(xml, tags):
    # output prefix, will need to customize to file...
    xml.ignorableWhitespace("""
 <Deployment>
    <Expedition>SOCAL33</Expedition>
    <Site>M</Site>
 </Deployment>
 <Detector>
    human
 </Detector>
 <Parameters>
    Simone Baumann-Pickering
 </Parameters>
 <Version> 1.0 </Version>
 <TimeSpan> 
    <begin> 2009-09-01T00:00:00Z </begin>
    <end> 2009-10-01T00:00:00Z </end>
  </TimeSpan>
""")
    return tags


def DataProcLogs_body(xml, tags, values, element):
    "DataProcLogs_body - Human guided Triton detections"

    # return if empty line
    chars = sum([len(v) for v in values])
    if chars == 0:
        return

    # empty attribute object
    attributes = AttributesNSImpl({}, {})

    # Enclosing element for each data row
    xml.startElement(element, attributes)

    for idx in range(len(tags)):
        if tags[idx] in ["Start_time", "End_time"]:
            m = datetimeUS_re.match(values[idx])
            if m:
                values[idx] = "%s-%s-%sT%sZ"%(
                    m.group("y"), m.group("m"), m.group("d"),
                    m.group("time"))

            else:
                print("Unable to match time '%s'"%(values[idx]))

            # Probably a smarter way to handle the namespace issue...
            xml.startElement(tags[idx], attributes)
            xml.startElement("TimeStamp", attributes)
            xml.startElement("when", attributes)
            xml.characters(values[idx])
            xml.endElement("when")
            xml.endElement("TimeStamp")
            xml.endElement(tags[idx])
        else:
            xml.startElement(tags[idx], attributes)
            xml.characters(values[idx])
            xml.endElement(tags[idx])

    xml.endElement(element)

def HARP_DataSummary_tags(xml, tags):
    "HARP deployment from HARP database"
    return tags

def HARP_DataSummary_body(xml, tags, values, element):
    "HARP deployment from HARP database"

    # empty attribute object
    attributes = AttributesNSImpl({}, {})

    # Enclosing element for each data row
    xml.startElement(element, attributes)

    geotime = dict()  # Store place coordinate tag/idx to process as unit

    for idx in range(len(tags)):
        if tags[idx] in ["Data_ID"]:
            # split into expedition and site 
            m = expedition_re.match(values[idx])
            if not m:
                raise ValueError("Could not match Deployment NAME# in %s"%(values[idx]))
            else:
                for tag in ["Region", "Site", "Deployment", "Project"]:
                    xml.startElement(tag, attributes)
                    # HARP database does not currently have project and region
                    if tag == "Region":
                        chars = m.group("Project")
                    else:
                        chars = m.group(tag)
                    if chars is None:
                        chars = "NA"    # missing values/not applicable
                    xml.characters(chars)
                    xml.endElement(tag)
        elif tags[idx] in ["Deployment_Longitude", 
                           "Deployment_Latitude", 
                           "Deployment_Depth_m"]:
            # mark long/lat/depth indices for later processing
            geotime[tags[idx]] = idx
            
        elif tags[idx] in ["Deployment_Date"]:
            geotime[tags[idx]] = idx # mark for later processing
            m = datetimeUS_re.match(values[idx])
            if m:
                values[idx] = "%s-%s-%sT%sZ"%(
                    m.group("y"), m.group("m"), m.group("d"),
                    m.group("time"))
            else:
                print("Warning:  Unable to match time '%s' in record [%s]"%(
                    values[idx],
                    ",".join(['%s="%s"'%(tags[k], values[k])
                              for k in range(len(tags))])))
                values[idx] = "01-01-0000T00:00:00Z"

        else:
            xml.startElement(tags[idx], attributes)
            xml.characters(values[idx])
            xml.endElement(tags[idx])


    # Write out grouped GeoTime element
    xml.startElement("GeoTime", attributes)
    # timestamp
    xml.startElement("TimeStamp", attributes)
    xml.characters(values[geotime["Deployment_Date"]])
    xml.endElement("TimeStamp")
    # longitude
    xml.startElement("longitude", attributes)
    xml.characters(longlat("longitude", values[geotime["Deployment_Longitude"]]))
    xml.endElement("longitude")

    # latitude
    xml.startElement("latitude", attributes)
    xml.characters(longlat("latitude", values[geotime["Deployment_Latitude"]]))
    xml.endElement("latitude")

    # depth = -altitude
    xml.startElement("altitude", attributes)
    xml.characters("-" + values[geotime["Deployment_Depth_m"]])
    xml.endElement("altitude")
    xml.endElement("GeoTime")

    # close off deployment entry
    xml.endElement(element)

def default_body(xml, tags, values, tuple_name, attributes, pretty=True):
    # Write an XML entry head
    xml.startElement(tuple_name, attributes)

    # empty attribute object
    empty_attrib = AttributesNSImpl({}, {})

    if len(tags) != len(values):
        print("tags & values don't match")
        pdb.set_trace()

    # Write components of entry
    for idx in range(len(values)):
        if pretty:
            xml.characters("\n\t")
        xml.startElement(tags[idx], empty_attrib)

        # special handling for DeploymentInformation table
        # modifies longitude/latitude & leaves other values alone
        values[idx] = longlat(tags[idx], values[idx])

        xml.characters(values[idx])
        xml.endElement(tags[idx]) 

    # close up entry
    if pretty:
        xml.characters("\n")
    xml.endElement(tuple_name)
    if pretty:
        xml.characters("\n")

# Specialized processing for certain input files using --filter option
# The tags and values are processed by the callbacks in the dictionary
# value tuple (tag_fn, body_fn), use a value of None for the function
# handle if no callback is needed, e.g. (None, body_fn)
#
# Callback signatures:
# tags = tag_fn(XMLGenerator, tags, attributes)
#  """tag_fn - Given a set of tags and an XMLGenerator stream,
#     return a list of modified tag values and possibly output
#     a prologue to the XML output."""
# 
# body_fn(XMLGenerator, tags, values, element, attributes)
#  """body_fn - Given a set of tags, associated values, attributes,
#     and an XMLGenerator stream, output a specified XML element
#     containing the tags and values. """

filters = {
    "HARP-DataSummary": (HARP_DataSummary_tags, HARP_DataSummary_body),
    "DataProcLogs" : (DataProcLogs_tags, DataProcLogs_body),
    "none" : (None, default_body) 
}

def longlat(tag, value):
    tag = tag.lower()
    # special handling for longitudes and latitudes
    if tag.find('longitude') > -1 or tag.find('latitude') > -1:
        m = longlat_re.match(value)
        if m:
            deg = float(m.group("deg"))
            min = float(m.group("min"))
            degrees = deg + min/60
            
            # All longitude and latitudes are specified with
            # 360 degrees (no negative values)
            if m.group("direction").lower() == 'w':
                degrees = 360 - degrees
            elif m.group("direction").lower() == 's':
                degrees = - degrees
            value = "%f"%(degrees)

    return value
    


def main():

    # Parse command line arguments
    parser = optparse.OptionParser(
        "%prog [options] csv_source xml_dest\n" +
        "\tConvert comma separated value file to xml file")
    parser.add_option("-t", "--tags_first",
                      help="First line contains tag names",
                      action="store_true")
    parser.add_option("-s", "--seperator", 
                     type="string", default=",",
                     help='value seperator (default ",")')
    parser.add_option("-e", "--entry",
                     type="string", default="entry",
                     help="XML tag for each row/entry in the CSV table")
    parser.add_option("-n", "--namespace", default=None,
                     help="XML namespace")
    parser.add_option("-d", "--document-name",
                     type="string", default=None,
                     help="XML document name")
    parser.add_option("-f", "--filter", default="none",
                      help="data filter for modifying data format: [%s]"%(
                        ",".join(list(filters.keys()))) + " (default none)")

    (options, args) = parser.parse_args()

    if len(args) != 2:
        parser.error("Must specify input and output files")

    # get handles to files
    inh = open(args[0])
    reader = csv.reader(inh, delimiter=options.seperator)
    outh = open(args[1], "w")

    # empty attribute object
    empty_attrib = AttributesNSImpl({}, {})

    xml = XMLGenerator(outh)

    xml.startDocument()
    if options.document_name:
            xml.startElement(options.document_name, {})
            #{"xmlns:sio":"http://cetus.ucsd.edu/geospatial_0_1",
            # "xmlns:kml":"http://www.opengis.net/kml/2.2",
            # "xmlns:gx":"http://www.google.com/kml/ext/2.2"})

    (header_fn, body_fn) = filters[options.filter]

    if options.tags_first:
        tags = next(reader)
        # Translate illegal characters in XML tags 
        tags = [tag_replacechar_re.sub("_", tag) for tag in tags]
        tags = [tag_dropchar_re.sub("", tag) for tag in tags]
        # Possibly output prologue & massage tag names
        if header_fn is not None:
            tags = header_fn(xml, tags)
        
    else:
        items = next(reader)
        # Generate numbered tags, one per value, 
        # assume we always have this many for simplicity...
        #       could lead to trouble later
        tags = ["tag_%d"%(n) 
                for n in range(len(items).split(options.seperator))]
        # Possibly output prologue & massage tag names
        if header_fn is not None:
            tags = header_fn(tags, xml)
        # Output first line
        body_fn(xml, tags, items, options.entry)

    
    if body_fn is not None:
        # Read each line & output corresponding XML
        for items in reader:
            body_fn(xml, tags, items, options.entry)

    # close up XML table
    if options.document_name:
        xml.endElementNS((options.namespace, options.document_name), 
                         options.namespace)
    xml.endDocument()
        
main()
    


                                            


        


