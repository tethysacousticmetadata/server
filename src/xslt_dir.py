import os
import lxml.etree as ET
import sys
from XML.transform import strip_xml_encoding_declaration

"""
Stand-alone utility to perform XSLT translation of all .xml files in a
directory (sys.argv[1]).  Sets namespace to be implicit.
"""

dir = sys.argv[1]

# Create an XSLT transformer
xslt = ET.parse("assets/implicit_ns.xsl")
transformer = ET.XSLT(xslt)

# Transform each document
docs = os.listdir(dir)

for d in docs:
    print(d)
    if d.lower().endswith('.xml'):
        with open(os.path.join(dir, d), 'rb') as in_h:
            xml_doc = in_h.read()
            # Create document object model, transform namespace,
            # rebuild the document and save it
            dom = ET.fromstring(xml_doc)
            new_dom = transformer(dom)
            new_xml = ET.tostring(new_dom, pretty_print=True,
                                  encoding="unicode")

        with open(os.path.join(dir, d), 'wt') as out_h:
            out_h.write(new_xml)


