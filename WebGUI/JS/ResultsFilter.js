/**
 * @author Daniel Weston
 * @version 1.0
 * DSanchezWeston@gmail.com
 * With the help of Professors Roger Whitney and Marie Roch
 * 25 Nov 2018
 */

class ResultsFilter{

    /**
     * The results returned from the server for the xQuery
     * @param {Object} HTMLDoc the returned results from the xQuery in the form of a HTML document Object
     * @param {Array[String]} nodeNames An array of strings of the name of the nameNode within HTMLDoc that
     *                          need to be added as a title to the GoogleMap marker
     * @var {Array[Object]} userWantedNodes all formatted to become GoogleMap markers
     */
    constructor(HTMLDoc, nodeNames){
        this.nodeNames = nodeNames;
        this.userWantedNodes = [];
        let test = this.StringToXML(HTMLDoc);
        this.highestLevelResults(test);
    }

    /**
     * @param {HTMLDoc} treeNode HTML Doc of XML results
     */
    highestLevelResults(treeNode){
        let resultsArray = treeNode.children[0].children;
        //alert(resultsArray.length);
        for(let i=0; i<resultsArray.length; i++)
        {
            this.wantedNodeFormater(resultsArray[i]);
        }
    }

    /**
     * Search the innerHTML of the treeNode parameter try to find a longitude and latitude coordinate
     * using regex. If those are found. Then recurse down the node and try to find any nodes that have been
     * highlighted red highlighted.
     * @param {Object} treeNode 
     */
    wantedNodeFormater(treeNode){
        //alert(treeNode.outerHTML);
        //alert(treeNode.innerHTML)
        let long = treeNode.innerHTML.match(/<Longitude>(\d{1,3}\.?\d*)<\/Longitude>/); //find <Longitude>1-3 digit number, a period, zero to infinite digits</Longitude> 
        let lat = treeNode.innerHTML.match(/<Latitude>(-?\d{1,3}\.?\d*)<\/Latitude>/);  //find <Latitude>a - maybe, a 1-3 digit number, a period, zero to infinite digits</Latitude>
        if(long && lat){
            let titleString = treeNode.innerHTML; //this.lookForHighlightedNodes(treeNode);
            this.userWantedNodes.push(
            {
                title: titleString,
                pos:{
                    lat: Number(lat[1]), 
                    lng: Number(long[1]),
                }
            });
        }
    }

    /**
     * Recurse down a tree looking for nodes with names in the @var this.nodeNames and add their string values
     * (outerHTML) to a string that concats other 
     * @param {Object} treeNode 
     * @return {String}
     */
    lookForHighlightedNodes(treeNode) {
        //console.log('hasNodeNames '+treeNode.hasOwnProperty('nodeName')+' '+treeNode.nodeName);
        let titleBuild = "";
        if('nodeName' in treeNode){
            this.nodeNames.forEach(element => { //for all 
                if(treeNode.nodeName.toLowerCase() == element){
                    titleBuild += treeNode.outerHTML;
                }
            })
        }
        for (var childNode in treeNode) {
            if (childNode == 'children') {    //going one step down in the object tree!!
                for(let i = 0; i<treeNode[childNode].length; i++)
                {
                    titleBuild += this.lookForHighlightedNodes(treeNode[childNode][i]);
                }
            }
        }
        return titleBuild;
    }

    /**
     * @return {Array[Object]}
     */
    getUserWantedNodes(){
        return this.userWantedNodes;
    }

    /**
     * Takes an XQuery result as a string and transforms it into an HTMLDoc that has a hierarchy
     * of nodes for each xml level.
     * @param {String} oString
     * @return {HTMLDoc} 
     */
    StringToXML(oString) {
        //code for IE
        if (window.ActiveXObject) { 
            var oXML = new ActiveXObject("Microsoft.XMLDOM"); oXML.loadXML(oString);
            return oXML;
        }
        // code for Chrome, Safari, Firefox, Opera, etc. 
        else {
            return (new DOMParser()).parseFromString(oString, "text/xml");
        }
    }
}