/**
 * @author Daniel Weston
 * @version 1.0
 * DSanchezWeston@gmail.com
 * 28 Aug 2018
 */
"use strict"

/**
 * This is a controller class for unorderded list <ul><\ul> in advancedqueiries.html
 * The idea behind this class is to build the ul with the data within it. When users
 * change the data inside the ul change the data model here.
 */
class ReturnController{

    /**
     * Constructor for the controller the id is significant in that it tries in with 
     * jquery2_ui.js. Everytime a jqueryUI element get dropped on the id it'll attach
     * itself to the ul.
     * @param {AdvancedQueries} parent
     * @param {String} id id attribute of the ul element <ul id = "id"></ul> 
     * @var {Array{ULData}} data the data of the <ul> element
     * @var {UnorderedListView} unorderedListView the unordered list element
     */
    constructor(parent, id){
        this.parent = parent;
        this.id = id;    
        this.data = []; //data (model) of ul. Will update if the data changes inside the ul
        this.unorderedListView;
    }

    /**
     * Adds a row to data. The data contains all the information about the ul interface. So
     * when it come time to process all the users input we just use the data object.
     * @param {Object} node             reference to D3 node taht was selected
     * @param {String} path             path down xQuery schema tree
     * @param {String} operatorValue    = , !=, < ,<=, >, >=, exists, not 
     * @param {String} inputValue       user inputed string
     * @param {String} conjunctionValue AND or OR
     * @var {Array{ULData}} this.data the data of the <ul> element
     */
    addRow(node, path, operatorValue, inputValue, conjunctionValue){

        this.data.push(new ULData(this, this.id, node, path, operatorValue, inputValue, conjunctionValue));
        this.parent.groupSort();
        this.buildView();
    }

    /**
     * Adds a row of data only given the path
     * @param {String} path path down xQuery schema tree
     */
    addDefaultRow(node, path){

        this.data.push(new ULData(this, this.id, node, path, "", "", ""));
        this.parent.groupSort();
        this.buildView();
    }

    /**
     * Method invokes clearing the old <ul> html element and rebuilding a new one with
     * the data array.
     * @var {UnorderedListView} unorderedListView the unordered list element 
     * @var {Array[ULData]} data the data of the <ul> element
     */
    buildView(){
        this.unorderedListView = new UnorderedListView(this, "returnUnorderedList", "returnCondition list-group sortable border border-danger rounded", 0, this.data, true);
        $('[data-trigger="hover"]').popover();
    }

    /**
     * 
     * @param {Integer} index integer of id  
     */
    toggleBackgroundColor(rowIndex){
        this.data[rowIndex].toggleBackgroundColor();
        this.buildView();
    }

    /**
     * Removes a row from the data. Typically after a "X" button was pressed in the UI
     * 
     * @param {Integer} index 
     */
    removeRow(id, index){
        this.data.splice(index,1);
        this.buildView();
    }

    /**
     * Clear all the @var this.data 
     */
    clear(){
        this.data = undefined;
        this.data = [];
        this.buildView();
    }

    /**
     * @var {Array[ULData]} data the data of the <ul> element
     */
    getData(){
        return this.data;
    }

    /**
     * @var {Array{ULData}} data
     * @return {Array[]}
     */
    mapNodes(){
        let mapNodes = [];
        this.data.forEach(element =>{
            if(element.getMap())                        /**@function element.getMap() @see ULData.getMap is the getMap  */
                mapNodes.push(element.getEndOfPath());  //getEndOfPath returns an array
        })
        return mapNodes;
    }

    /**
     * For each index in data build a string representation of it.
     * @var {String} buildString
     * @var {Array[ULData]} data the data of the <ul> element
     * @return {String}
     */
    toString(){
        var buildString = "";
        this.data.forEach(element => {
            buildString += element.toString();
        });
        //console.log(buildString);
        return buildString;
    }
}