

/**
 * Generic AJAX failure for XQueries
 * @param request - An instance of ajax's jqXHR (JQuery XML HTTP Request) class provided to
 *                  AJAX error handlers.
 * @param textStatus:  One of null, timeout, error, abort, parsererror
 * @param textServerError - Text response from server indicating request status, e.g. Not Found
 */
function xqFailureCallback(request, textStatus, textServerError) {
    const CONNECTION = 0  // lost connection
    const NOTFOUND = 404  // bad url/no such resource
    const INTERNALERROR = 500;  // server error

    let msg;
    switch (request.status) {
        case 0:
            msg = "Server did not respond or aborted the connection:  " + this.url;
            break;
        case NOTFOUND:
            msg = "Server reports that the resource was not found:  " + this.url;
            break;
        case INTERNALERROR:
            msg = "Server reports an internal error: " + this.url;
            break;
        default:
            switch (textStatus) {
                case "parseerror":
                    msg = "Unable to parse the server's response: " + this.url;
                    break
                case 'timeout':
                    msg = "Server did not respond within a reasonable time: " + this.url;
                    break
                case 'abort':
                    msg = "Request aborted: " + this.url;
                    break
                default:
                    if (request.responseText != null)
                        msg = "Error from " + this.url + ": " + request.responseText;
                    else
                        msg = "Error from " + this.url + ": " + request.status.toString();
                    break;
            }
    }
    alert (msg);
}

/**
 * Execute an XQuery
 * @param xq - XQuery
 * @param namespaces - 0: strip namespaces, 1: retain namespaces
 * @param successCbk - Callback(response) - Execute callback on results
 * * @param failCbk - Callback(request, textStatus, textServerError) - Error handler callback
 *                  if not specified, defaults to alert
 */
function tethysXQuery(xq, namespaces=1, successCbk, failCbk) {
    let urlValue = window.location.origin+"/XQuery";
    let form = new FormData();  //create a FormData instance that is going to be the body of the HTTP Post
    form.append("XQuery", xq); //add generated query to POST body
    form.append("namespaces", namespaces);

    if (failCbk == null) {
        failCbk = xqFailureCallback;
    }
    let ajaxCall = new AjaxWrapper("post", form, failCbk);
    let xhr = $.ajax(ajaxCall.getSettings()).done(successCbk);
}

/**
 * Retrieve ODBC sources
 * @param successCbk - Success callback
 * @param failCbk - Callback(request, textStatus, textServerError) - Error handler callback
 *                  if not specified, defaults to alert
 */
function getODBCSources(successCbk, failCbk) {
    let urlValue = window.location.origin+"/ODBC";

    if (failCbk == null)
        failCbk = xqFailureCallback;

    let ajaxCall = new AjaxWrapper("get", null, failCbk);
    ajaxCall.setURL(urlValue);
    let xhr = $.ajax(ajaxCall.getSettings()).done(successCbk);
}