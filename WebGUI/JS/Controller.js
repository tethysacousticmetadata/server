/**
 * @author Daniel Weston
 * @version 1.0
 * DSanchezWeston@gmail.com
 * 28 Aug 2018
 */
"use strict"

/**
 * This is a controller class for unorderded list <ul><\ul> in advancedqueiries.html
 * The idea behind this class is to build the ul with the data within it. When users
 * change the data inside the ul change the data model here.
 */
class Controller{

    /**
     * Constructor for the controller the id is significant in that it tries in with 
     * jquery2_ui.js. Everytime a jqueryUI element get dropped on the id it'll attach
     * itself to the ul.
     * @param {String} id id attribute of the ul element <ul id = "id"></ul>
     * @param {Boolean} simple a lazy way to determine if the <li> inside the <ul> will be simple (i.e. just a path name)
     * @var {UnorderedListView} unorderedListView the unordered list element 
     * @var {Array{ULData}} data the data of the <ul> element
     */
    constructor(id, simple){
        this.id = id    
        this.unorderedListView = new UnorderedListView(this, this.id, simple);
        this.data = []; //data (model) of ul. Will update if the data changes inside the ul
    }

    /**
     * Adds a row to data. The data contains all the information about the ul interface. So
     * when it come time to process all the users input we just use the data object.
     * @param {Object} node             reference to D3 node taht was selected
     * @param {String} path             path down xQuery schema tree
     * @param {String} operatorValue    = , !=, < ,<=, >, >=, exists, not 
     * @param {String} inputValue       user inputed string
     * @param {String} conjunctionValue AND or OR
     * @var {Array{ULData}} this.data the data of the <ul> element
     */
    addRow(node, path, operatorValue, inputValue, conjunctionValue){
        this.data.push(new ULData(this.id, node, path, operatorValue, inputValue, conjunctionValue));
        this.buildView();
    }

    /**
     * Adds a row of data only given the path
     * @param {String} path path down xQuery schema tree
     */
    addDefaultRow(node, path){
        this.data.push(new ULData(this.id, node, path, "", "", ""));
        this.buildView();
    }

    /**
     * Method invokes clearing the old <ul> html element and rebuilding a new one with
     * the data array.
     * @var {UnorderedListView} unorderedListView the unordered list element 
     * @var {Array[ULData]} data the data of the <ul> element
     */
    buildView(){
        this.unorderedListView.buildUL(this.data);
        $('[data-trigger="hover"]').popover();   
    }

    /**
     * Removes a row from the data. Typically after a "X" button was pressed in the UI
     * @param {Integer} index 
     */
    removeRow(index){
        this.data.splice(index,1);
        this.buildView();
    }

    /**
     * Users changed the value of the operator selector. Method updates the data object
     * to represent that change.
     * @param {Integer} index 
     * @param {String} value 
     * @var {Array[ULData]} data the data of the <ul> element
     */
    updateOperatorSelect(index, value){
        this.data[index].setOperatorValue(value);
    }

    /**
     * Users changed the value of the input. Method updates the data object
     * to represent that change.
     * @param {Integer} index 
     * @param {String} value 
     */
    updateInputValue(index, value){
        this.data[index].setInputValue(value);
    }

    /**
     * Users changed the value of the conjunction selector. Method updates the data object
     * to represent that change.
     * @param {Integer} index 
     * @param {String} value 
     */
    updateConjunctionSelect(index, value){
        this.data[index].setConjunctionValue(value);
    }

    clear(){
        this.data = undefined;
        this.data = [];
        this.buildView();
    }

    /**
     * @var {Array[ULData]} data the data of the <ul> element
     */
    getData(){
        return this.data;
    }

    /**
     * For each index in data build a string representation of it.
     * @var {String} buildString
     * @var {Array[ULData]} data the data of the <ul> element
     */
    toString(){
        var buildString = "";
        this.data.forEach(element => {
            buildString += element.toString();
        });
        //console.log(buildString);
        return buildString;
    }
}