/**
 * @author Daniel Weston
 * @version 1.0
 * DSanchezWeston@gmail.com
 * 1 Nov 2018
 */

 "use strict"

 class Select{
      /**
      * @param id id of the HTML element
      * @param class class of the HTML element
      * @param maxLength max length of the HTML element
      */
     constructor(idAttribute, classAttribute, maxLength){
        this.select = document.getElementById(idAttribute);
        if(this.select == null){
            this.select = document.createElement("select");
        }
        this.select.setAttribute("id", idAttribute);
        this.select.setAttribute("class", classAttribute);
        this.select.setAttribute("maxlength", maxLength);
     }

     /**
     * Multiple selects elements are created. Created an individual function to fill the options of the select.
     * @param {Element} selectBox   select element thats goinf to have all the options added to it
     * @param {ObjectArray} options key, value pairs to populate the options of the select
     */
    buildSelect(options){
        for(let key in options){
            let opt = document.createElement("option");
            opt.setAttribute("value", options[key]);
            opt.appendChild(document.createTextNode(key));
            this.select.appendChild(opt);
        }
    }

    addOptions(options){
        for(let i=0; i<options.length; i++)
        {
            let opt = document.createElement("option");
            opt.setAttribute("value", options[i].innerHTML);
            opt.appendChild(document.createTextNode(options[i].innerHTML));
            this.select.appendChild(opt);
        }
    }

    getSelect(){
        return this.select;
    }

    get value(){
        return this.select.value;
    }

 }