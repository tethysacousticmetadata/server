/**
 * @author Daniel Weston
 * @version 1.0
 * DSanchezWeston@gmail.com
 * 23 Aug 2018
 */

"use strict"
/**
 * The primary class of the AdvancedQueries.html webpage.
 */
class AdvancedQueries{

    /**
     * @constructor
     * @var {MapWrapper} this.map
     * @var {WhereController} this.whereController
     * @var {ReturnController} this.returnController
     * @var {Select} this.speciesSelect
     * @var {ResultModal} this.modal
     * @var {HTMLElement} this.resultsView  <textarea id="resultTextArea" </textarea>
     * @var {D3Json} this.trees 
     * @var {SnackBar} this.snackBar        <div id="snackBar"></div>
     * @var {D3TreeController} this.d3Controller
     * @var {HTMLElement} this.flatCheckBox <input id="flatCheckBox" type="checkbox">
     * @var {Array[String]} this.unloaded
     * @var {Object} this.keyMap            A key map of all current buttons pressed.
     * @var {HTMLDocument} this.xmlResponse An object that is a tree of all the XML returned from a XQuery submit.
     */
    constructor(){
        this.map = {};  //GoogleMap Wrapper. It get filled after google callback is activated.
        this.whereController = new WhereController(this, "whereView");  // id and do NOT use SimpleRows use Rows
        this.returnController = new ReturnController(this, "returnCondition"); // is and do use SimpleRows
        this.speciesSelect = new Select("speciesSelectAdvanced", "custom-select ml-1", 10);
        this.modal = new ResultModal('advancedModal','closeAdvanced');     //results Modal. Appears after a query with the results.
        this.resultsView = document.getElementById("resultTextArea");
        this.trees = new D3Json();          /**@var {D3JSon} trees */
        this.snackBar = new SnackBar("snackBar", SnackBar.SNACKBAR_DEFAULT_TEXT);
        this.d3Controller = new D3TreeController(d3, this);
        this.flatCheckBox = false; //document.getElementById("flatCheckBox");
        this.matlabCheckBox = document.getElementById("matlabCheckBox");
        this.bboxButton = document.getElementById("bboxButton");
        this.keyMap = {};   //A key map of all current buttons pressed.
        this.unloaded = []; //will be loaded with all the schema to be loaded
        this.xmlResponse = undefined;
        this.noAutocompletelist = [];
        this.convertURL = '';
        this.lastUserQueryForm = null;  // Stores the form associated with last successful user query

        //this.cannedQuery = cannedQueries;
    }

    /**
     * The start of it all. When the webpage loads it creates an instance of Advanced queries and call this function
     * onReady @see $(document).ready
     * this will go thourgh AdvancedQueries.html and add event listeners to all the HTML elements that requier them.
     * After adding the event listeners. It'll call @see this.createTree() to build the @var {D3Json} this.trees object.
     */
    main()
    {
        var _this = this;
        this.getSelectNamesAjax();  //populate select with libraries supported by server
        this.getNoAutocompleteList();


        document.getElementById("clearQueryButton").addEventListener("click", function(){
            _this.clearQuery();
        });

        //document.getElementById('flatCheckBox').addEventListener("click", function(){
        //    _this.groupSort();
        //});
        
        //Set a JQuery event to call schemaSelectChanged when #schemaSelector is changed. Make an AJAX call pulling down the new schema's XML.
        $("#schemaSelector").on('change', function(e){
            _this.schemaSelectChanged(this.value);
        });

        $('#speciesSelectAdvanced').on('change', function(e){
            
            _this.setSpeciesPlaceholder()
        })

        $("#saveAsCombo").on('change', function(e){
            _this.saveAs(this.value);
        });
        //Set an event that builds an XQuery FLWOR and sends it via AJAX
        $('#submitButton').on('click', function() { //When submit is pressed
            _this.submitQueryPressed();
        });


        $('#advancedFileUpload').on('change', function(){ // Load query from file dialog
            var file = document.getElementById('advancedFileUpload').files[0];
            if (file) {
                var reader = new FileReader();
                reader.readAsText(file);
                reader.onload = function(e) {
                // browser completed reading file -
                 _this.readJSON(e.target.result);
                };
            }
        })

        $('#reset').on('click', function() {
            _this.d3Controller.resetZoom();
        });



        $('#undo').on('click', function() {
            _this.d3Controller.zoomUndo();
        });
        
        $('#expandAll').on('click', function() {
            _this.d3Controller.expandAll();
        });
        //when collapse all is pressed all nodes are going to close.
        $('#collapseAll').on('click', function() {
            _this.d3Controller.collapseAll();
        });

        $('#copyButton').on('click', function() {
            document.getElementById('resultTextArea').select();
            document.execCommand('copy');
        });
        this.createTree();   //This function sequentially calls the URL's for each collection. The reason for the sequence is so the tree is always in teh same order.

        // Shut this off as there are no queries defined
        // let qUnit = new AdvancedQueriesTest(this);

        // Update the display of the XQuery/JSON representation of current query
        document.getElementById("view-tab").addEventListener("click",
            function () {_this.queryChanged()})
    }



    clearQuery(){
        this.whereController.clear()
        this.returnController.clear();
        document.getElementById("whereUnorderedList0").innerHTML = '<li class = "whereView list-group-item input-group d-inline-flex mb-1 ui-sortable-handle justify-content-center">Hold left Shift + Click</li>';
        document.getElementById("returnUnorderedList0").innerHTML = '<li class = "returnCondition list-group-item input-group d-inline-flex mb-1 ui-sortable-handle justify-content-center">Hold right Shift + Click</li>'
        this.map.deleteMarkers();
        this.resultsView.innerHTML = '';
        document.getElementById("queryViewTextArea").innerHTML = '';
        this.trees = new D3Json();
        this.createTree();
    }

    /**
     * Save results or query to file
     * @param selected
     */
    saveAs(selected){
        switch (selected) {
            case "MATLAB":
                this.downloadResults("MATLAB", ".mat");
                break;
            case 'R':
                this.downloadResults("R", ".rData");
                break;
            case "XML":
                this.downloadResults("XML", ".xml");
                break;
            case 'JSON':
                this.downloadResults("JSON", ".json");
                break;
            case 'saveQuery':
                this.buildJSON();
                break
        }
    }

    downloadResults(path, ext){
        /*
        uses url from Content-Location to build GET request
        for matlab conversion.
        */
       var url = this.convertURL;
       var link = document.createElement('a');
       link.href = url+ "/" + path;
       var path = link.pathname;
       var filename = path.split("/")[2] + ext
       link.download = filename;
       document.body.appendChild(link);
       link.click();
       document.body.removeChild(link);
       $("#saveAsCombo").val('')
    }


    readJSON(json){
        /*
        Parses json, and loops through wheres and returns building an array of ULData for each.
        Sets model and builds view with new data
        */
        var queryParams = JSON.parse(json);
        if (queryParams.hasOwnProperty('form')){
            // might need to clean out all form elements on simple side here
            $('#simple-tab').tab('show');
            cannedQueries.readQueryJSON(json)
        }
        else {
            var wheres = queryParams.whereData;
            this.savedULWheres = [];
            wheres.forEach(where => {     //all where paths
                var namesFromPath = where.path.split("/");
                var node = this.trees.nameToNode(namesFromPath);
                this.savedULWheres.push(
                    new ULData(this.whereController, "whereView", node, where.path, where.operatorValue, where.inputValue, where.conjunctionValue,true)
                )
            });

            var returns = queryParams.returnData;
            this.savedULReturns = [];
            returns.forEach( r => {     //all where paths
                var namesFromPath = r.path.split("/");
                var node = this.trees.nameToNode(namesFromPath);
                this.savedULReturns.push(
                    new ULData("", "", node, r.path, "", "", "")
                )
            });
            //this.clearQuery()  //this doesn't work here for some reason
            this.returnController.data = this.savedULReturns;
            this.returnController.buildView();
            this.whereController.groupsData = [this.savedULWheres];
            this.whereController.buildView();
            this.queryChanged();
        }
    }

    buildJSON(){
        // builds query json and calls save
        var wheres = this.whereController.getData();
        var returns = this.returnController.getData();
        var JSONWheres = [];
        wheres.forEach(ul=>{
                    JSONWheres.push(
                        {'node': '',
                         'path': ul.getPath(),
                         'operatorValue': ul.getOperatorValue(),
                         'inputValue': ul.getInputValue(),
                         'conjunction': ul.getConjunctionValue()
                        }
                    );
                })
        var JSONReturns = [];
        returns.forEach(ul=>{
                    JSONReturns.push(
                        {'node': '',
                         'path': ul.getPath()
                        }
                    );
                })
        var completeJSON = {'whereData':JSONWheres,'returnData':JSONReturns};
        cannedQueries.saveJSON(JSON.stringify(completeJSON));
    }

    refineQuery(queryType){
       // clear previous query from advanced side
       this.whereController.clear()
       this.returnController.clear();

       this.map.deleteMarkers();
       this.resultsView.innerHTML = '';
       document.getElementById("queryViewTextArea").innerHTML = '';
       // ****************************
       let query = new PreBuiltQueries(cannedQueries.trees);  //this builds the query in case the query isn't submitted before the query is refined
       if (queryType == 'DetectionEffort') {
           $("#schemaSelector").val("Detections").change()
           if ($('#datepicker_start_canned').val() != '' && $('#datepicker_end_canned').val() != '')
           {
                   cannedQueries.setPickerISOValues_Canned()
                   query.detectionEffort(cannedQueries.formData,cannedQueries.returnController, $('#speciesSelectAdvanced').val(),this.bboxButton,this.whereController,
                   cannedQueries.minSliderValue,cannedQueries.maxSliderValue );
           }
           else
               query.detectionEffort(cannedQueries.formData,cannedQueries.returnController, $('#speciesSelectAdvanced').val(),this.bboxButton,this.whereController);
       }
       else if (queryType == 'Detection'){
           $("#schemaSelector").val("Detections").change()
           if ($('#datepicker_start_canned').val() != '' && $('#datepicker_end_canned').val() != '')
           {
                cannedQueries.setPickerISOValues_Canned()
                query.detectionQuery(cannedQueries.formData,cannedQueries.returnController,$('#speciesSelectAdvanced').val(),this.bboxButton,this.whereController,
                    cannedQueries.minSliderValue,cannedQueries.maxSliderValue );
           }
           else
               query.detectionQuery(cannedQueries.formData,cannedQueries.returnController,$('#speciesSelectAdvanced').val(),this.bboxButton,this.whereController);

       }
       else if (queryType == 'Deployment'){
           $("#schemaSelector").val("Deployment").change()
           if ($('#datepicker_start_canned').val() != '' && $('#datepicker_end_canned').val() != '')
           {
               cannedQueries.setPickerISOValues_Canned()
               query.deploymentQuery(cannedQueries.formData,cannedQueries.returnController,$('#speciesSelectAdvanced').val(),this.bboxButton, this.whereController,
                   cannedQueries.minSliderValue,cannedQueries.maxSliderValue);
           }
           else
               query.deploymentQuery(cannedQueries.formData,cannedQueries.returnController,$('#speciesSelectAdvanced').val(),this.bboxButton, this.whereController);
       }

       else if (queryType == 'Localization'){
           $("#schemaSelector").val("Localize").change()
           if ($('#datepicker_start_canned').val() != '' && $('#datepicker_end_canned').val() != '')
           {
               cannedQueries.setPickerISOValues_Canned()
               query.localizationsQuery(cannedQueries.formData,cannedQueries.returnController,$('#speciesSelectAdvanced').val(),this.bboxButton, this.whereController,
                   cannedQueries.minSliderValue,cannedQueries.maxSliderValue);
           }
           else
               query.localizationsQuery(cannedQueries.formData,cannedQueries.returnController,$('#speciesSelectAdvanced').val(),this.bboxButton, this.whereController);
       }


       // query.getWhereData(), query.getReturnData()
       this.returnController.data = query.getReturnData();
       this.returnController.buildView();
       this.whereController.groupsData = [query.getWhereData()];
       this.whereController.buildView();
       //var treeClone = Object.assign({},cannedQueries.trees)  //tried a deep clone of the D3 tree from the simple query side
       this.trees = cannedQueries.trees;
       //this.trees = this.cloneTree(cannedQueries.trees);
       this.queryChanged();
       $('#advanced-tab').tab('show');
        $('#build-tab').tab('show');

    }

    cloneTree(tree){

        var returnTree = new D3Json();
        for (var i = 0; i< tree.children.length; i++){
            returnTree.addChildren(tree.children[i]);
        }

        return returnTree
    }


    getNoAutocompleteList(){
        let urlValue = window.location.origin+"/Schema/noautocompletelist";   //AJAX URL to request xml JSON. the schemaSelector Value is spliced into the string.
        let ajaxCall = new AjaxWrapper("get","", this.ajaxError);   //perform a GET to get XMLJson
        ajaxCall.setURL(urlValue);
        var _this = this;
        $.ajax(ajaxCall.getSettings()).done((response)=>{           //When we get the response for the xml JSON
            _this.whereController.setPathsThatCanNotBeAdded(response.split(" "));
            console.log(response);
        });
    }
    /**
     * This does an AJAX call to get a list of Species Libraries on the server and populates id = "speciesSelectAdvanced"
     */
    setSpeciesPlaceholder(){ // this will probably work for plot species too

         $('#speciesInputDetection').prop('placeholder', $('#speciesSelectAdvanced').val())
         $('#speciesPlots').prop('placeholder', $('#speciesSelectAdvanced').val())
    }

    getSelectNamesAjax(){
        // Select the last prepopulated species
        $('#speciesSelectAdvanced').find('option:last').prop('selected', 'selected');

        let form = new FormData();
        form.append("XQuery", PreBuiltQueries.selectSpeciesAbbreviations());
        var ajaxCall = new AjaxWrapper("post", form, this.ajaxError);
        var _this = this;
        $.ajax(ajaxCall.getSettings()).done(function (response) {   //waiting for the AJAX response
            let xmlResponse = AjaxWrapper.StringToXML(response);    //returns a XMLDocument
            _this.speciesSelect.addOptions(xmlResponse.children[0].children);
            _this.setSpeciesPlaceholder()
            /*
                //xmlResponse (XMLDocument){
                    children (HTMLCollection) = (HTMLCollection)[0: {
                        children:[
                                    0:Name = {
                                        innerHTML:"Species Library"
                                    },
                                    1:Name = {
                                        innerHTML:"Species Library"
                                    }]
                    }, 1:{}, 2:{}, ...]
                }
            */
        });
    }

    /**
     * Google Maps loads asychonously this function gets called from the global call back to add a MapWrapper Object
     * to AdvancedQueries when Google loads.
     */
    setMap(){
        this.map = new MapWrapper(this,"Google-Map-Advanced");
    }

    /**
     * 
     */
    groupSort(){
        if(this.flatCheckBox.checked){
            this.whereController.unGroupGroups();
        }
        else if(this.returnController.getData().length>0 && this.whereController.getData().length>0 )
        {
            var sort = new GroupSort(this.whereController.getData(), this.returnController.getData(), this.trees);
            console.log("ungroup: ", this.whereController.getData());
            this.whereController.setGroups(sort.sortWhere(this.trees));
            this.trees.reset();
        }
        this.whereController.buildView();

    }

    /**
     * This is a list of XML URL's that need to be converted from a JSON representation of a an XML schema into a JSON
     * Object that can be displayed by the D3 API. Everything in the @var {Array[String]} this.unloaded get's loaded as
     * subtrees into @var {D3Json} this.trees
     * @see getSchema
     */
    createTree(){
        this.unloaded = ["Abbreviations", "Localize", "Event", "Ensemble", "Detections", "Deployment", "Calibration"];
        this.getSchema();
    }

    /**
     * Called from @see createTree() it will be called @var {Integer} this.unload.length number of times. Everytime getting a XMLJson from
     * (e.g. http://localhost/Schema/Deployment?format=JSON) converting it to D3Json object. After the Ajax is resolved it gets converted from
     * an XML JSON to a D3 JSON @see XmlToD3. Then it adds the D3Json to @var this.tree as a subtree. One node below root.
     */
    getSchema()
    {
        let schemaName = this.unloaded.pop()
        //let urlValue = "http://"+window.location.host+"/Schema/"+this.unloaded.pop()+"?format=JSON";   //AJAX URL to request xml JSON. the schemaSelector Value is spliced into the string.
        let urlValue = window.location.origin+"/Schema/"+schemaName+"?format=JSON";
        //console.log(urlValue);
        let ajaxCall = new AjaxWrapper("get","", this.ajaxError);   //perform a GET to get XMLJson
        ajaxCall.setURL(urlValue);
        var _this = this;
        $.ajax(ajaxCall.getSettings()).done((response)=>{           //When we get the response for the xml JSON
            let treeData = new XmlToD3();

            treeData.setXmlJson(response);  
            treeData.convert();                                     //convert XmlJson into a D3Json
            let schemaObj = treeData.getD3Json()
            //if (schemaName == 'Deployment'){
                //this.childToRemove = undefined
                //console.log('STOP HERE')
                //schemaObj.children.forEach(function(child){
                //    if (child.name == "DeploymentKey"){
                //      this.childToRemove = child;
                //    }
                //})
                let keyChildName = schemaName+"Key"
                for (var index=0; index < schemaObj.children.length; index++){
                    if (schemaObj.children[index].name == keyChildName){

                        break
                    }
                }
                schemaObj.children.splice(index,1)

                //let keyArray = schemaObj.children.filter(function(child){
                //    return child.name == 'DeploymentKey'
                //})

                //delete keyArray[0]

            //}
            _this.trees.addChildren(schemaObj);          //add D3Json to the tree list
                        
            if(this.unloaded.length>0){ //runs until there are no more collections in unload. Also all done in series so the tree builds in the same order everytime.
                _this.getSchema();      
            }
            else{                       //All collections have been added to the tree. Moving on.
                this.d3Controller.setRoot(this.trees);              //Tree has been completely built and being added as the root of the D3 dendogram.
                                                                    //@description IMPORTANT! even though the next line sets the root again to a sub node
                                                                    // of @var this.trees this is an important step because it adds a bunch of parent, children,
                                                                    // and _children nodes to the tree that'll be useful later

                this.schemaSelectChanged($("#schemaSelector").val());
            }
        });
    }
    
    /**
     * A callback when the ajax has an error. It'll populate the modal with the error message
     * @param {Object} jqXHR 
     * @param {String} textStatus 
     * @param {String} errorThrown 
     */
    ajaxError_deprecated(jqXHR, textStatus, errorThrown){  // yes but modal isn't shown for anything but submitQueryPressed
        advancedQueries.modal.setMessage(advancedQueries.formatXml(textStatus+": "+errorThrown+"\n"+jqXHR.responseText.replace(/[<>]/g, "")));
    }

    errorReport(errString){
        this.snackBar.setText(errString);
        this.snackBar.show();
    }

    /**
     * User changed the select for id = "schemaSelector" the value of the select is passed
     * the tree is searched looking for a direct child with that value name.
     * @param {String} schema 
     */
    schemaSelectChanged(schema){
        
        this.d3Controller.setRoot(this.trees.findCollection(schema));   //Replace the D3 Tree with the new schema selected. All those values are hard coded in AdvancedQueries.html
                                                                        /**@todo but they could be soft coded by adding a getCollectionsNames() function returning an array of strings and
                                                                        populating the select options with that list. */
    }

    /**
     * If there is a value in Return Value(s). Build the query with all the where and return data thats been generated by the user
     * Else inform the user that no return value has been added.
     * @function this.returnController.getData() returns @see {ULData} and @function this.whereController.getData() returns @see {ULData}
     * 
     */



    checkForJoinOnLocalization(whereControllerULs,returnControllerULs){
        let colllectionNamesSansLocalize = ["Abbreviations", "Event", "Ensemble", "Detections", "Deployment", "Calibration"] //"Localize"

        let wheresCollections = [];
        whereControllerULs.forEach(function(whereULDataObj){
            wheresCollections.push(whereULDataObj.path.split('/')[0])
        })
        this.wheresCollectionsSet = new Set(wheresCollections)

        let returnsCollections = [];
        returnControllerULs.forEach(function(returnULDataObj){
            returnsCollections.push(returnULDataObj.path.split('/')[0])
        })
        this.returnsCollectionsSet = new Set(returnsCollections)

        if ( (this.wheresCollectionsSet.has('Localize') && wheresCollections.some(item => colllectionNamesSansLocalize.includes(item))  )
           || (this.returnsCollectionsSet.has('Localize') && returnsCollections.some(item => colllectionNamesSansLocalize.includes(item)) )
           ) {
            return false
             } else{
            return true
        }

    }

    submitQueryPressed(){
        if(Array.isArray(this.returnController.getData()) && this.returnController.getData().length>0)
        {

            if (this.checkForJoinOnLocalization(this.whereController.getData(),this.returnController.getData())) {

                let submit = new SubmitQuery(this.whereController.getData(), this.returnController.getData(), this.speciesSelect.value);    //create a new SubmitClass with the LI inside whereController and returnCotnroller
                let form = new FormData();  //create a FormData instance that is going to be the body of the HTTP Post
                form.append("JSON", submit.buildJSONQuery()); //add generated query to POST body
                form.append("dataType", "save")

                console.log("Advanced JSON query parameters")
                console.log(form)

                this.modal.setLoader();     //The grey circle with a blue arc spinning around it. Indicating to the user the submit is in progress.
                this.modal.show();
                let ajaxCall = new AjaxWrapper("post", form, this.ajaxError);   //Send the build XQuery
                let _this = this;
                var xhr = $.ajax(ajaxCall.getSettings()).done(function (response) {
                    // execute on success

                    let max_MB = 5 // Treat really big things differently
                    let bytes_per_MB = 1024*1024
                    let result_MB = Math.round(response.length / bytes_per_MB * 10) / 10;
                    if (result_MB < max_MB) {
                        _this.resultsView.innerHTML = _this.formatXml(response);    //fill the modal body with the response
                    } else {
                        _this.resultsView.innerHTML = "<div id='tree_div'><p>" +
                    `Results are ${result_MB} megabytes and are not shown as they exceed the ${max_MB} megabyte ` +
                    "browser display limit.  Results may be downloaded with the Save button</p></div>";
                    }
                    _this.modal.hide("advancedModal");
                    let results = new ResultsFilter(response, _this.returnController.mapNodes());    //returns a XMLDocument
                    _this.snackBar.setText(SnackBar.SNACKBAR_DEFAULT_TEXT);

                    // Plot tracks or positions
                    _this.map.clearMap();       //delete all old markers on google map
                    if (! _this.map.constructTracks(response)) {
                        _this.checkForLongLat2(results);    //search for <longitude> and <latitude> XML tags to added to the GoogleMap
                    }

                    _this.queryChanged();     //might not need this if we build the query on change to the view
                    _this.snackBar.show();

                    // Store URL for converting this query and enable conversions
                    _this.convertURL = xhr.getResponseHeader("Content-Location");
                    $("option[value='JSON']").removeAttr("disabled");
                    $("option[value='MATLAB']").removeAttr("disabled");
                    if (cannedQueries.R)
                        $("option[value='R']").removeAttr("disabled");
                    $("option[value='XML']").removeAttr("disabled");

                    //add tethys to the Query box
                    document.getElementById("queryViewTextArea").value += '\n\nXQuery: \n' + response;

                    //  // this prob sends to wrong map
                });
            } else {
                alert("Localizations cannot be currently queried in connection with other collections, only queries on it's own fields.")
            }// end of check for join on Localization
        }
        else{
            this.modal.show();
            this.modal.setMessage(this.formatXml("Please add at least one return condition"));  //THIS DOESN"T WORK FIX
        }


    }

    ajaxError(jqXHR, textStatus, errorThrown){

        advancedQueries.modal.modalBody.innerHTML = "";
        console.log("ERROR")

        console.log("END ERROR")
        let message = jqXHR.responseText
        let newmessage = message.replaceAll("</p>","</p><br>")
        console.log(newmessage)
        advancedQueries.modal.setMessage("<p>"+textStatus+": "+errorThrown+"\n"+newmessage+"</p>");   //encapsulation breaking

        }


    /**
     * After a result is returned. Check the XML String with Regular Expressions if Longitude and Latitude are within the string
     * and then add all those Longitude and Latitude positions to the Google Map.
     * @deprecated replaced by @see checkForLongLat2
     * @param {String} xmlText of String of XML Text
     */
    checkForLongLat(xmlText){
        this.map.deleteMarkers();       //delete all old markers on google maps
        var re = /<Longitude>(\d{1,3}\.?\d*)<\/Longitude><Latitude>(-?\d{1,3}\.?\d*)<\/Latitude>/gmi;   //regular expression to find all Long and Lats
        var found = undefined;
        var locations = [];
        while((found = re.exec(xmlText)) != null){
            locations.push([Number(found[1]), Number(found[2])]);   //Longitude then Lat. Not it's not found[0] because that'd be the entire
                                                                    //<Longitude>DD.DD<Lopngitude><Latitude>DD.DD<Latitude> expression
        }
        if(locations.length>0){
            this.snackBar.setText("Map and Results Updated");
            this.map.addMarkers(locations); //adds all the new locations found
        }
    }

    /**
     * Delete old markers from last query and add new markers from last query to Google Maps.
     * If <longitude> and <latitude> XML tags are found.
     * @param {ResultsFilter} results 
     */
    checkForLongLat2(results){
        if(Array.isArray(results.getUserWantedNodes()) && results.getUserWantedNodes().length>0){   //check for <longitude> and <latitude> XML tags
            this.map.addMarkers(results.getUserWantedNodes()); //adds all the new locations found
            this.snackBar.setText("Map and Results Updated");
        }
        console.log("post reset");
    }

    /**
     * Called when the "Create Query" button is pressed. This is similar to submitQueryPressed. Check  
     */
    queryChanged(){
        if(Array.isArray(advancedQueries.returnController.getData()) && advancedQueries.returnController.getData().length>0)
        {
            let submit = new SubmitQuery(advancedQueries.whereController.getData(), advancedQueries.returnController.getData(), advancedQueries.speciesSelect.value);    //create a new SubmitClass with the LI inside whereController and returnCotnroller
            console.log($("#schemaSelector").val());

            // Build up JSON representation
            let json = submit.buildJSONQuery()
            console.log(advancedQueries.formatQuery(json))
            // String with queries in it
            let queries = "JSON: \n" + advancedQueries.formatQuery(json)

            // Create the XQuery representation
            // POST to the XQuery resource with JSON payload and a plan to return corresponding XQuery
            let form = new FormData()
            form.append("JSON", submit.buildJSONQuery()); //add generated query to POST body
            form.append("plan", "2")
            let ajaxCall = new AjaxWrapper("post", form, this.ajaxError)
            $.ajax(ajaxCall.getSettings()).done(function (response) {
                queries += "\n\nXQuery: \n" + response
                // Update the Query area
                document.getElementById("queryViewTextArea").value = queries
            })

        }
    }

    // XML pretty printer.  Taken from gist by sente on GitHubGist
    // <script src="https://gist.github.com/sente/1083506.js"></script>
    formatQuery(xml) {
        let formatted = '';
        let reg1 = /(\s)(and|or)\s([\$]*)/g;               //finds 3 groups. $1 equals >, $2 equals <, $3 equals / zero to infinite 
        xml = xml.replace(reg1, '$1$2\n$3');   //replace all ></ -> >\r\n</    all xml console 
        let reg2 = /(\w|\))(})\s?(<)/g;               //finds 3 groups. $1 equals >, $2 equals <, $3 equals / zero to infinite 
        xml = xml.replace(reg2, '$1\n$2$3');   //replace all ></ -> >\r\n</    all xml console 
        let reg3 = /(>)({)(\$)/g;               //finds 3 groups. $1 equals >, $2 equals <, $3 equals / zero to infinite 
        xml = xml.replace(reg3, '$1$2\n$3');   //replace all ></ -> >\r\n</    all xml console 
        let reg4 = /(\w)(,)([^\n])/g;               //finds 3 groups. $1 equals >, $2 equals <, $3 equals / zero to infinite 
        xml = xml.replace(reg4, '$1$2\n$3');   //replace all ></ -> >\r\n</    all xml console 
        let pad = 0;
        jQuery.each(xml.split('\n'), function(index, node) {
            let indent = 0;
            node = node.trim();
            //if (node.match( /^for.*/ )) {
            //    indent = 0;}
             if (node.match( /}/ )) {
                if (pad != 0) {
                    pad -= 1;
                }
            } else if (node.match( /{/ )) {
                indent = 1;
            } else {
                indent = 0;
            }

            let padding = '';
            for (let i = 0; i < pad; i++) {
                padding += '    ';
            }

            formatted += padding + node + '\r\n';
            pad += indent;
        });

        return formatted;
    }

    // XML pretty printer.  Taken from gist by sente on GitHubGist
    // <script src="https://gist.github.com/sente/1083506.js"></script>
    formatXml(xml) {
        console.log(xml);
        var formatted = '';
        var reg = /(>)(<)(\/*)/g;               //finds 3 groups. $1 equals >, $2 equals <, $3 equals / zero to infinite 
        xml = xml.replace(reg, '$1\r\n$2$3');   //replace all ></ -> >\r\n</    all xml console 
        var pad = 0;                            /* All xml will be seperated into xml start or end tags <Longitude>25.014</Longitude>*/
        jQuery.each(xml.split('\r\n'), function(index, node) {  //foreach new line
            var indent = 0;
            if (node.match( /.+<\/\w[^>]*>$/ )) {   //Find all xml start and end tags on a line (e.g. <Longitude>25.014</Longitude>, <Vessel>Horizon</Vessel>), but not starts without ends 
                indent = 0;
            } else if (node.match( /^<\/\w/ )) {    //Find all xml end tags and one letter (e.g. '</ResponsibleParty>' the '</R' would be matched)
                if (pad != 0) {
                    pad -= 1;
                }
            } else if (node.match( /^<\w[^>]*[^\/]>.*$/ )) {    //Find all xml start or start and end tags on a line (e.g. <Longitude>25.014</Longitude>, <out>)
                indent = 1;
            } else {
                indent = 0;
            }

            let padding = '';
            for (var i = 0; i < pad; i++) {
                padding += '  ';
            }

            formatted += padding + node + '\r\n';
            pad += indent;
        });
        return formatted;
    }
}

/**
 * Keeps track of which keys are pressed down 'keydown' and clears them on a 'keyup'
 * global variable map is used later to check if "ctrl key" map[17] or "shift key" map[16] is pressed
 * if it is add a <li> item to <ul id = "test2">
 */
onkeydown = onkeyup = function(e){  
    e = e || event; // to deal with IE
    if(e.keyCode == 16 && e.location == 1)      //Shift is keyCode 16 and location 1 is left shift
        advancedQueries.keyMap['ShiftLeft'] = (e.type == 'keydown');
    else if(e.keyCode == 16 && e.location == 2) //Shift is keyCode 16 and location 2 is right shift
        advancedQueries.keyMap['ShiftRight'] = (e.type == 'keydown');
    "Key Pressed: " + String.fromCharCode(e.charCode) + "\n"
    + "charCode: " + e.charCode + "\n"
    + "keyCode: " + e.keyCode + "\n"
    + "SHIFT key pressed: " + e.shiftKey + "\n"
    + "ALT key pressed: " + e.altKey + "\n"
}

/**
 * Keeps track of which keys are pressed down 'keydown' and clears them on a 'keyup'
 * global variable map is used later to check if "ctrl key" map[17] or "shift key" map[16] is pressed
 * if it is add a <li> item to <ul id = "test2">
 */
onmousedown = function(e){  
    e = e || event; // to deal with IE
    advancedQueries.keyMap[e.code] = (e.type == 'keydown'); //if keydown true else false
    console.log("code "+e.code+"\ttype "+e.type); //used for debugging
}

/**
 * Attepmt to stop a bug where the keyup wasn't resetting the shiftdown key. So everytime the mouse moves
 * check if the shift keys are pressed and if not set them to false.
 */
window.onmousemove = function (e) {
    if (!e) e = window.event;
    if (!(e.code == "ShiftLeft" || e.code == "ShiftRight")){
        advancedQueries.keyMap['ShiftLeft'] = false;
        advancedQueries.keyMap['ShiftRight'] = false;
    }
  }

/**
 * First function called when all the javascript finised loading and intitates all
 * the things necessary for the D3tree and adds event listeners to the HTMLElements.
 */
var cannedQueries = new CannedQueries();
var advancedQueries = new AdvancedQueries();
$(document).ready(function() {
    advancedQueries.main();
    cannedQueries.main()
}); //document.ready

/**
 * Callback for Googlemaps when ready to load @see AdvancedQueries.html  
 * <script async defer src="https://maps.googleapis.com/maps/api/js?key=###########################&callback=initMap"></script>
 * on the script the callback=initMap calls this function. This function then creates a MapWrapper inside @var advancedQueries
 */
function initMap(){
    cannedQueries.setMap();
    advancedQueries.setMap();
}