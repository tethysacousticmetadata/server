

const o = {a:0};  //can this not start out with an initial property, like: const o = {}?
const myprop = 'b'
Object.defineProperty(o, myprop, { get: function() { return this.a + 1; } });
// TRY THIS!!!
Object.defineProperty(o, myprop, { get: this.getter_guts });  // when go to write function for defining properties, also setter
console.log(o.b) // Runs the getter, which yields a + 1 (which is 1)
////////
a.alpha = "hi"
environment thingee
b.alpha = "bye"
/////////
var bValue = 38;
var key = 'alpha'
Object.defineProperty(this.frame, property, {
  // Using shorthand method names (ES2015 feature).
  // This is equivalent to:
  get: this.getter_guts,
  set: function(newValue) { bValue = newValue; },

});




const handler = {
  get: function(obj, prop) {  // obj here is the target !!!!
    return //this.frame[prop] or obj[prop] ??
  },
    set: function(obj, prop, value) { obj.prop = value}
};


this.frame = new Proxy({}, this.handler)   /// OR  something = new Proxy(this.frame, this.handler)


// WAIT !! EXTEND Proxy class per, https://stackoverflow.com/questions/37714787/can-i-extend-proxy-with-an-es2015-class


class environment extends Object{ //???

    construtor(parent,name){

        //this.frame = {
        //    get [property](){ // if we set property it is a dynamically added prop to obj, e.g. var value = this.frame.someprop
        //        try {v = this.frame[item] }
        //        catch (e) {
        //            try {
        //                v = this.parent[item]
        //            } catch(e) {
        //                if (!this.parent){
        //                    console.log('KEY ERROR')
        //                }
        //            return undefined
        //            }
        //
        //        }
        //        return v
        //    }
        //}  // WE COULD GO BACK TO THIS !! this.frame = {}  pull the abouve inline function out into separate function on the class
        this.frame = {}
        this.parent = parent
        this.name = name

    }

    getter_guts(item){

        try {v = this.frame[item] }
        catch (e) {
            try {
                v = this.parent[item]
            } catch(e) {
                if (!this.parent){
                    console.log('KEY ERROR')
                }
            return undefined
            }

        }
        return v

    }

    push(){
        return environment(this)  ///THIS MIGHT BE A PROBLEM IF OUTSIDE CLASS
    }

}