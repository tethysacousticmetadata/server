/**
 * @author Daniel Weston
 * @version 1.0
 * DSanchezWeston@gmail.com
 * 3 Oct 2018
 */
 
"use strict"

/**
 * Class used call apon common queries
 */
class PreBuiltQueries{

    /**
     * 
     * @param {D3Json} trees 
     */
    constructor(trees){
        this.returnData = [];
        this.whereData = [];
        this.trees = trees;

        this.selectJSONBlock = []
        this.returnJSONBlock = []
        this.speciesJSONBlock = {"query":{},"return":{}}
    }

    /**
     * Construct a query for detection effort
     * @param {querySpecs}
     * @param {Object} querySpecs 
     */
    detectionEffort(querySpecs,returnController ,species, bboxButton, whereController, minTime, maxTime) {

        this.whereData = [];
        this.returnData = [];
        this.whereController = whereController;

        // Always set the species block, as TSNs may be returned that are children of path
        this.makeSpeciesBlock(species)

        let minLong = querySpecs.deploymentAndDetections.longitude.min;
        let maxLong = querySpecs.deploymentAndDetections.longitude.max;
        let minLat = querySpecs.deploymentAndDetections.latitude.min;
        let maxLat = querySpecs.deploymentAndDetections.latitude.max;
        if (querySpecs.detectionQuery.species != ''){
            this.addWhereRow("Detections/Effort/Kind/SpeciesId", "=",  ''+querySpecs.detectionQuery.species+'', ""); //detections.children[5].children[2].children[0]
            if (querySpecs.detectionQuery.group != '')
                this.addWhereRow("Detections/Effort/Kind/SpeciesId", "=", '' + querySpecs.detectionQuery.group + '', "", null, "Group");
        //@Group/string()
        }
        if (querySpecs.detectionQuery.project != '')
            this.addWhereRow("Deployment/Project","=",''+querySpecs.detectionQuery.project+'',"and"); //deployment.children[0]
        if (querySpecs.detectionQuery.deployment != '')
            this.addWhereRow("Deployment/DeploymentNumber","=",querySpecs.detectionQuery.deployment,"and"); //deployment.children[1]
        if (querySpecs.detectionQuery.site != '')
            this.addWhereRow("Deployment/Site","=",''+querySpecs.detectionQuery.site+'',""); //deployment.children[12]



        if (minLong != '' && maxLong != '' && minLat != '' && maxLat != '' && bboxButton.className == "on"){

            this.addWhereRow("Deployment/DeploymentDetails/Longitude","<", MapWrapper.oneEightyToThreeSixty(maxLong), //deployment.children[9].children[0]
            MapWrapper.oneEightyToThreeSixty(minLong) > MapWrapper.oneEightyToThreeSixty(maxLong)?'or ':'and'); //if(minLong>maxLong get the edge cases else get the inner)
            this.addWhereRow("Deployment/DeploymentDetails/Longitude",">", MapWrapper.oneEightyToThreeSixty(minLong), " and"); //deployment.children[9].children[0]
            this.addWhereRow("Deployment/DeploymentDetails/Latitude",">", minLat, " and"); //deployment.children[9].children[1]
            this.addWhereRow("Deployment/DeploymentDetails/Latitude","<", maxLat, ""); //deployment.children[9].children[1]

        }

        if ($('#datepicker_start_canned').val() != '' && $('#datepicker_end_canned').val() != ''){
            this.addWhereRow("Detections/Effort/Start",">",''+minTime+'',"and"); // contains
            this.addWhereRow("Detections/Effort/End","<",''+maxTime+'',"");  // contains
        }

        this.addReturnRow("Deployment/Id");
        this.addReturnRow("Deployment/Project");
        this.addReturnRow("Deployment/Site");
        this.addReturnRow("Deployment/DeploymentNumber");
        this.addReturnRow("Deployment/DeploymentDetails");
        this.addReturnRow("Detections/Id");
        this.addReturnRow("Detections/Effort/Start");
        this.addReturnRow("Detections/Effort/End");
        this.addReturnRow("Detections/Effort/Kind");
        this.addReturnRow("Detections/Algorithm");

        returnController.data = this.returnData;
    }

    deploymentQuery(querySpecs,returnController ,species, bboxButton, whereController, minTime, maxTime){

        // there are two forms shared by all queries, deploymentAndDetections which contains the bbox inputs,
        // and detectionQuery which contains species,project, site, and deployment
        let minLong = querySpecs.deploymentAndDetections.longitude.min;
        let maxLong = querySpecs.deploymentAndDetections.longitude.max;
        let minLat = querySpecs.deploymentAndDetections.latitude.min;
        let maxLat = querySpecs.deploymentAndDetections.latitude.max;

        this.whereData = [];
        this.returnData = [];
        this.whereController = whereController;

        // Always set the species block, as TSNs may be returned that are children of path
        this.makeSpeciesBlock(species)

        if (querySpecs.detectionQuery.species != '')
            {this.addWhereRow("Detections/Effort/Kind/SpeciesId", "=",''+querySpecs.detectionQuery.species+'',"");
            this.makeSpeciesBlock(species)}
        if (querySpecs.detectionQuery.project != '')
            this.addWhereRow("Deployment/Project","=",''+querySpecs.detectionQuery.project+'',"and"); //deployment.children[0]
        if (querySpecs.detectionQuery.deployment != '')
            this.addWhereRow("Deployment/DeploymentNumber","=",querySpecs.detectionQuery.deployment,"and"); //deployment.children[1]
        if (querySpecs.detectionQuery.site != '')
            this.addWhereRow("Deployment/Site","=",''+querySpecs.detectionQuery.site+'',""); //deployment.children[12]


        if (minLong != '' && maxLong != '' && minLat != '' && maxLat != '' && bboxButton.className == "on"){

            this.addWhereRow("Deployment/DeploymentDetails/Longitude","<", MapWrapper.oneEightyToThreeSixty(maxLong), //deployment.children[9].children[0]
            MapWrapper.oneEightyToThreeSixty(minLong) > MapWrapper.oneEightyToThreeSixty(maxLong)?'or ':'and'); //if(minLong>maxLong get the edge cases else get the inner)
            this.addWhereRow("Deployment/DeploymentDetails/Longitude",">", MapWrapper.oneEightyToThreeSixty(minLong), " and"); //deployment.children[9].children[0]
            this.addWhereRow("Deployment/DeploymentDetails/Latitude",">", minLat, " and"); //deployment.children[9].children[1]
            this.addWhereRow("Deployment/DeploymentDetails/Latitude","<", maxLat, ""); //deployment.children[9].children[1]

        }

        if ($('#datepicker_start_canned').val() != '' && $('#datepicker_end_canned').val() != ''){
            //alert(minTime)
            //this.addWhereRow("Detections/Effort/Start",">",''+minTime+'',"and"); // contains
            //this.addWhereRow("Detections/Effort/End","<",''+maxTime+'',"");  // contains
            this.addWhereRow("Deployment/DeploymentDetails/TimeStamp",">",''+minTime+'',"and");
            this.addWhereRow("Deployment/DeploymentDetails/TimeStamp","<",''+maxTime+'',"");
        }
        this.addReturnRow("Deployment/Id");
        this.addReturnRow("Deployment/Project");
        this.addReturnRow("Deployment/DeploymentNumber");
        this.addReturnRow("Deployment/Site");
        this.addReturnRow("Deployment/SamplingDetails/Channel");
        this.addReturnRow("Deployment/DeploymentDetails");


        returnController.data = this.returnData;
    }

    detectionQuery(querySpecs,returnController ,species, bboxButton,whereController, minTime, maxTime){

        let minLong = querySpecs.deploymentAndDetections.longitude.min;
        let maxLong = querySpecs.deploymentAndDetections.longitude.max;
        let minLat = querySpecs.deploymentAndDetections.latitude.min;
        let maxLat = querySpecs.deploymentAndDetections.latitude.max;


        this.whereData = [];
        this.returnData = [];
        this.whereController = whereController;

        // Always set the species block, as TSNs may be returned that are children of path
        this.makeSpeciesBlock(species)

        //alert(querySpecs.detectionQuery.species)
        if (querySpecs.detectionQuery.species != '')
             {this.addWhereRow("Detections/Effort/Kind/SpeciesId", "=",''+querySpecs.detectionQuery.species+'',"and"); //detections.children[5].children[2].children[0]
                 this.makeSpeciesBlock(species)}
        if (querySpecs.detectionQuery.project != '')
            this.addWhereRow("Deployment/Project","=",''+querySpecs.detectionQuery.project+'',"and"); //detections.children[1].children[1]
        if (querySpecs.detectionQuery.deployment != '')
            this.addWhereRow("Deployment/DeploymentNumber","=",querySpecs.detectionQuery.deployment,"and"); //detections.children[1].children[2]  // LOOOOK HERE
        if (querySpecs.detectionQuery.site != '')
            this.addWhereRow("Deployment/Site","=",''+querySpecs.detectionQuery.site+'',""); //detections.children[1].children[3]
        if (minLong != '' && maxLong != '' && minLat != '' && maxLat != '' && bboxButton.className == "on"){

            this.addWhereRow("Deployment/DeploymentDetails/Longitude","<", MapWrapper.oneEightyToThreeSixty(maxLong), //deployment.children[9].children[0]
            MapWrapper.oneEightyToThreeSixty(minLong) > MapWrapper.oneEightyToThreeSixty(maxLong)?'or ':'and'); //if(minLong>maxLong get the edge cases else get the inner)
            this.addWhereRow("Deployment/DeploymentDetails/Longitude",">", MapWrapper.oneEightyToThreeSixty(minLong), " and");
            this.addWhereRow("Deployment/DeploymentDetails/Latitude",">", minLat, " and");
            this.addWhereRow("Deployment/DeploymentDetails/Latitude","<", maxLat, "");

        }

        if ($('#datepicker_start_canned').val() != '' && $('#datepicker_end_canned').val() != ''){
            this.addWhereRow("Detections/Effort/Start",">",''+minTime+'',"and"); // contains
            this.addWhereRow("Detections/Effort/End","<",''+maxTime+'',"");  // contains

            //this.addWhereRow("Detections/Effort/Start","<",''+minTime+'',"and");  // overlap 1,2
            //this.addWhereRow("Detections/Effort/End",">",''+minTime+'',"or");     // overlap 1,2

            //this.addWhereRow("Detections/Effort/Start","<",''+maxTime+'',"and");  // overlap 3
            //this.addWhereRow("Detections/Effort/Start",">",''+minTime+'',"and");     // overlap 3

            //this.addWhereRow("Detections/OnEffort/Detection/Start",">",''+minTime+'',"and");  // detection
            //this.addWhereRow("Detections/OnEffort/Detection/Start","<",''+maxTime+'',"");     // detection

         }


        this.addReturnRow("Detections/OnEffort/Detection");
        this.addReturnRow("Deployment/DeploymentDetails");
        if (querySpecs.detectionQuery.species != '')
            this.addWhereRow("Detections/OnEffort/Detection/SpeciesId","=",''+querySpecs.detectionQuery.species+'',""); //detections.children[6].children[0].children[7]
        returnController.data = this.returnData;
    }


    localizationsQuery(querySpecs, returnController , species, bboxButton, whereController ,minTime, maxTime ){
        //detectionQuery(querySpecs,returnController ,species, bboxButton,whereController, minTime, maxTime)
        this.whereData = [];
        this.returnData = [];
        this.whereController = whereController;

        // Always set the species block, as TSNs may be returned that are children of path
        this.makeSpeciesBlock(species)

        let minLong = querySpecs.deploymentAndDetections.longitude.min;
        let maxLong = querySpecs.deploymentAndDetections.longitude.max;
        let minLat = querySpecs.deploymentAndDetections.latitude.min;
        let maxLat = querySpecs.deploymentAndDetections.latitude.max;

        if (querySpecs.detectionQuery.species != '')
             {this.addWhereRow("Localize/Localizations/Localization/SpeciesId", "=",''+querySpecs.detectionQuery.species+'',"and"); //detections.children[5].children[2].children[0]
              //this.addWhereRow("Detections/OnEffort/Detection/SpeciesID", "=",''+querySpecs.detectionQuery.species+'',"and")
                 this.makeSpeciesBlock(species)}

        if ($('#datepicker_start_canned').val() != '' && $('#datepicker_end_canned').val() != ''){
            this.addWhereRow("Localize/Localizations/Localization/TimeStamp",">",''+minTime+'',"and"); //localize.children[8].children[0].children[1]
            this.addWhereRow("Localize/Localizations/Localization/TimeStamp","<",''+maxTime+'',"");
         }

        if (minLong != '' && maxLong != '' && minLat != '' && maxLat != '' && bboxButton.className == "on"){
            let bounds = "Localize/Localizations/Localization/Track/WGS84/CoordinateBounds/"
            this.addWhereRow(bounds+ "SouthEast/Longitude",  ">", MapWrapper.oneEightyToThreeSixty(maxLong)?'or ':'and');
            this.addWhereRow(bounds+ "NorthWest/Longitude",">=",MapWrapper.oneEightyToThreeSixty(minLong), "and");
            this.addWhereRow(bounds+ "NorthWest/Latitude",">=",MapWrapper.oneEightyToThreeSixty(minLat), "and");
            this.addWhereRow(bounds+ "SouthEast/Latitude","<=",MapWrapper.oneEightyToThreeSixty(maxLat), "");
        }

        this.addReturnRow("Localize/Id")
        this.addReturnRow("Localize/DataSource")
        this.addReturnRow("Localize/Localizations/Localization");  //localize.children[8].children[0]

        returnController.data = this.returnData;
    }


    /**
     * @return {ULData}
     */
    getWhereData(){
        return this.whereData;
    }

    /**
     * @return {ULData}
     */
    getReturnData(){
        return this.returnData;
    }
    
    /**
     * Builds a ULData object and adds it to @var this.whereData
     * @param {Object} node 
     * @param {String} path 
     * @param {String} operatorValue 
     * @param {String} inputValue 
     * @param {String} conjunctionValue 
     */
    addWhereRow_xquery(path, operatorValue, inputValue, conjunctionValue){
        let node = this.trees.nameToNode(path.split("/"));
        console.log('split path '+path.split("/"))
        if (arguments[4])
        {
            //alert("poop "+arguments[4])
            let excludeFromLoop = arguments[4];
            if (arguments[5])
                this.whereData.push(new ULData(this.whereController, "whereView_Canned", node, path, operatorValue, inputValue, conjunctionValue,false, excludeFromLoop, arguments[5]));
            else
                this.whereData.push(new ULData(this.whereController, "whereView_Canned", node, path, operatorValue, inputValue, conjunctionValue, false, excludeFromLoop));

        }
        else {

            if (arguments[5])
                this.whereData.push(new ULData(this.whereController, "whereView_Canned", node, path, operatorValue, inputValue, conjunctionValue,false, null, arguments[5]));
            else
                this.whereData.push(new ULData(this.whereController, "whereView_Canned", node, path, operatorValue, inputValue, conjunctionValue, false));
        }


    }

    makeSpeciesBlock(speciesLibrary){

        if (speciesLibrary != "latin") {
            var fromFunc = "lib:abbrev2tsn"
            var toFunc = "lib:tsn2abbrev"
            var operand = ["%s",speciesLibrary]
        } else {
            var fromFunc = "lib:completename2tsn"
            var toFunc = "lib:tsn2completename"
            var operand = ["%s"]

        }
        this.speciesJSONBlock.query["op"] = fromFunc
        this.speciesJSONBlock.query["optype"] = "function"
        this.speciesJSONBlock.query["operands"] = operand

        this.speciesJSONBlock.return["op"] = toFunc
        this.speciesJSONBlock.return["optype"] = "function"
        this.speciesJSONBlock.return["operands"] = operand

    }

    addWhereRow(path, operatorValue, inputValue, conjunctionValue){
        let node = this.trees.nameToNode(path.split("/"));
        console.log('split path '+path.split("/"))
        if (arguments[4])
        {
            //alert("poop "+arguments[4])
            let excludeFromLoop = arguments[4];
            if (arguments[5]){
                this.whereData.push(new ULData(this.whereController, "whereView_Canned", node, path, operatorValue, inputValue, conjunctionValue,false, excludeFromLoop, arguments[5]));
                let selectObj= {"op":operatorValue,"operands":[path,inputValue],"optype":"infix"}
                this.selectJSONBlock.push(selectObj)
            } else {
                this.whereData.push(new ULData(this.whereController, "whereView_Canned", node, path, operatorValue, inputValue, conjunctionValue, false, excludeFromLoop));
                let selectObj= {"op":operatorValue,"operands":[path,inputValue],"optype":"infix"}
                this.selectJSONBlock.push(selectObj)
            }

        }
        else {

            if (arguments[5]) {
                this.whereData.push(new ULData(this.whereController, "whereView_Canned", node, path, operatorValue, inputValue, conjunctionValue, false, null, arguments[5]));
                let selectObj = {"op": operatorValue, "operands": [path, inputValue], "optype": "infix"}
                this.selectJSONBlock.push(selectObj)
            }
            else
            {
                this.whereData.push(new ULData(this.whereController, "whereView_Canned", node, path, operatorValue, inputValue, conjunctionValue, false));
                let selectObj = {"op": operatorValue, "operands": [path, inputValue], "optype": "infix"}
                this.selectJSONBlock.push(selectObj)
            }
        }


    }

    /** this.trees.nameToNode("Detections/Effort/Kind/SpeciesId".split("/")),
     * 
     * @param {String} node 
     * @param {String} path 
     */
    addReturnRow(path){
        let node = this.trees.nameToNode(path.split("/"));
        this.returnData.push(new ULData("","", node, path, "", "", "",false));
        this.returnJSONBlock.push(path)
    }

    /**
     * Used in @see AdvancedQueries.getSelectNamesAjax() to fill the select with all the species libraries
     * @return {String}
     */
    static selectSpeciesAbbreviations(){
        return SubmitQuery.DEFAULT_SDSU_NAMESPACE+SubmitQuery.DEFAULT_START
        +"\nfor $abbr_name in collection('SpeciesAbbreviations')/Abbreviations/Name\n"
        +'order by $abbr_name return $abbr_name\n'
        + SubmitQuery.DEFAULT_FOOTER;
    }

    /**
     * Return a CSV table descring Group declarations within the species abbreviations
     * @return {string} - string of lines of the form:  abbrev map name, encoding name, group
     */
    static selectSpeciesGroup() {
        return SubmitQuery.DEFAULT_SDSU_NAMESPACE +
            `
            for $Abbrev in collection("SpeciesAbbreviations")/Abbreviations,
                $map in $Abbrev/Map[completename/@Group]
            return
               fn:concat(
                 $Abbrev/Name/text(),
                 ",",
                 $map/coding/text(),
                 ",",
                 $map/completename/@Group
                 )
             `;

    }

    
}
