/**
 * @author Daniel Weston
 * @version 1.0
 * DSanchezWeston@gmail.com
 * With the help of Professors Roger Whitney and Marie Roch
 * 23 Aug 2018
 */
"use strict";

//const fileLoad = require("../JSONVersionOfSchemas/deployment/Deployment.json");

//var imp = require("./Test/ReadJSON.js");
/**
 * class that takes a xml JSON object and converts it into a D3 JSON object.
 */
class XmlToD3{
    constructor(){
        this.xmlJson = {};
        this.d3Json = new D3Json();
        this.nodesThatAreChoice = []
        this.nodesThatAreAttributes = []
    }

    setXmlJson(xmlJson){
        this.xmlJson = xmlJson;
    }

    /**
     * Traverses d3Json and removes all Object keys named count and flworNodes.
     * This resets the tree again to be used to build another query withou losing
     * refernce to the old query parameters
     */
    resetD3Json(){
        this.d3Json.reset();
    }
    /**
     * The non recursive parts buildD3Json.
     */
    convert(){
        this.d3Json = new D3Json();
        //this.buildD3Json(this.xmlJson, this.d3Json, false, "0", "0");  // sequence
        this.idCounter = 0
        this.buildD3Json(this.xmlJson, this.d3Json, false, '0', '');
        //this.d3Json.finalize();
        this.d3Json = this.d3Json['children'][0];
        this.d3Json['root'] = true;
    }

    /**
     * @deprecated bug ahs been fixed
     * Comment this out once the bug fix for Longitude and Latitude are fixed for inserting into the XMLJson
     */
    quickFix(){
        if(this.d3Json['name'] == "Deployment")
        {
            this.d3Json.children[9].children.push({'name': 'Longitude'});
            this.d3Json.children[9].children.push({'name': 'Latitude'});
            this.d3Json.children[10].children.push({'name': 'Longitude'});
            this.d3Json.children[10].children.push({'name': 'Latitude'});
        }
    }

    /**
     * Recursive function that buils a D3Json
     * @param {Object} xmlNode  initially the JSON of the XML Schema
     * @param {Object} d3PassedNode initially an empty object. Eventually an object that'll be able to be displayed in the D3 API dendogram
     * @param {Boolean} maxOccursFlag
     * @param minOccursFlag
     */

    getSourceMapType(type){
        let lcType = type.toLowerCase()
        let mappedType
        if (lcType.includes('date'))
            mappedType = "DateTime"
        if (lcType.includes('string'))
            mappedType = "String"
        if (lcType.includes('integer'))
            mappedType = "Integer"
        if (lcType.includes('int'))
            mappedType = "Integer"
        if (lcType.includes('token'))
            mappedType = "String"  //WHAT IS GOING TO BE THE DEFAULT???
        return mappedType
    }


    buildD3Json(xmlNode, d3PassedNode, maxOccursFlag, minOccursFlag, nodeId) {
        var d3Node = d3PassedNode;      //assign the passed D3 object tree as a node
        if (typeof (xmlNode) == "string")   //if the xmlNode is a string we're at the bottom of a tree path and need to return
            return;

        if ('attribute' in xmlNode) {
            xmlNode['attribute']['@minOccurs'] = '0'
            xmlNode['attribute']['@attribute'] = '1'
        }

        if("extension" in xmlNode){
            if(xmlNode["extension"]['attribute'] != undefined){

                if(xmlNode["extension"]['attribute'].length == undefined)//single object
                {
                     this.nodesThatAreAttributes.push(xmlNode["extension"]['attribute'])
                }else{
                    [xmlNode["extension"]['attribute']].flat().forEach(att => {
                        this.nodesThatAreAttributes.push(att)
                    })
                }
            }
        }

        if('choice' in xmlNode){
            this.grabAllChoiceNodesUnderElement(xmlNode['choice']);
        }

        for(var xmlChild in xmlNode){   // for all keys inside an Object of key, values
            if(xmlNode[xmlChild] !== null) {    //has keys
                if(xmlChild == "any"){
                    d3Node['anyParameter'] = "1"
                }
                if(typeof(xmlNode[xmlChild])=="string"){    //child value is a string
                    //alert(xmlNode.valueOf())
                    if(xmlChild == "@name" ){    //condition to add to d3 JSON. Modify different parameters

                        nodeId += nodeId != '' ? '__' + xmlNode[xmlChild] : xmlNode[xmlChild]
                        d3Node = this.buildNode(xmlNode[xmlChild]); //A significant node was found and we need to grow the D3 tree
                        d3Node.id = nodeId

                        if(xmlNode[xmlChild].toLowerCase().includes('longitude')
                            || xmlNode[xmlChild].toLowerCase().includes('latitude')){
                            d3Node['type'] = 'latlong'
                        }
                        else if ("@type" in xmlNode){
                            //alert(xmlNode["@type"])
                            d3Node['type'] = this.getSourceMapType(xmlNode["@type"])
                        }

                        if (!("@maxOccurs" in xmlNode)){

                            d3Node['maxOccurs'] = "1"

                        } else {
                            d3Node['maxOccurs'] = xmlNode['@maxOccurs']
                        }

                        if("annotation" in xmlNode)
                        {
                            let annotation = xmlNode['annotation']
                            let description = []

                            if(annotation.length == undefined) //single object
                            {
                                description.push(annotation['documentation'])
                            }
                            else
                            {
                                annotation.forEach(ann => {
                                    description.push(ann['documentation'])
                                })
                            }

                            d3Node['description'] = description
                        }

                        //get choice values that match the node
                        let hasChoice = this.nodesThatAreChoice.filter(e => e[0] == xmlNode)
                        if(hasChoice.length > 0)
                        {
                            //if labeled main vs child
                            if(hasChoice[0][1] == 'main'){
                                d3Node['choice'] = true
                            }else{
                                d3Node['childChoice'] = true
                            }

                            d3Node['choiceFilled'] = false
                        }

                        if ("@attribute" in xmlNode) {
                            d3Node['attribute'] = xmlNode['@attribute']
                        }
                        else if(this.isNodeInAttributes(xmlNode))
                        {
                            d3Node['attribute'] = "1"
                        }

                        //if minOccurs is not in node and its not an attribute default to 1
                        //if minOccurs is not in node but it is an attribtue, default to 0
                        if (!("@minOccurs" in xmlNode)
                            & (!('attribute' in d3Node) ||  (d3Node['attribute'] == "0")))
                        {
                            d3Node['minOccurs'] = "1"
                        }
                        else if (!("@minOccurs" in xmlNode)){
                            d3Node['minOccurs'] = "0"
                        }
                        else {
                            d3Node['minOccurs'] = xmlNode['@minOccurs']
                        }

                        if(d3Node['id_num'] == undefined)
                            d3Node['id_num'] = this.idCounter
                            this.idCounter += 1

                        if((this.d3Json.children[0] != undefined && this.d3Json.children[0].name == 'Deployment') //only check if multi record if deployment
                            && this.isMultiRecordNode(d3Node)){ // if the node is deployment/sensor/audio or deployment/samplingdetails/channel,
                            d3Node['multiRecord'] = true  // we want to allow for multiple records to be created
                        }

                        maxOccursFlag = false;
                        minOccursFlag = '0'
                        if(d3PassedNode['children'] == null)        //if object doesn't have a 'children' key add one and the value of an empty array
                            d3PassedNode['children'] = [];
                        d3PassedNode['children'].push(d3Node);      //add new node under last significant node
                    }
                }

                this.buildD3Json(xmlNode[xmlChild], d3Node, maxOccursFlag, minOccursFlag, nodeId);  //recurse into child, d3Node changed if next child is a
            }
        }

        nodeId = ''
    }

    grabAllChoiceNodesUnderElement(elements, isChild = false){
        Object.entries(elements).forEach(choice => {
            this.nodesThatAreChoice.push([choice[1], isChild ? "child" : "main"]) //set top choice nodes to main, all children are choiceChild to signal to potentially
                                                                                    //change required status of other nodes
            if(choice[1]['sequence'] != undefined){
               this.grabAllChoiceNodesUnderElement(choice[1]['sequence']['element'], true);
            }
        })
    }

    isMultiRecordNode(node) {  //deployment/SamplingDetails/channel parent and all children
           return this.multiRecordNodeIds().includes(node.id)  //deployment/sensor/audio parent and all children
    }

    isNodeInAttributes(node){
        let isAnAttribute = false
        this.nodesThatAreAttributes.forEach(att => { //is node in attribute list
            if(att['@name'] == node['@name']
                && att['@type'] == node['@type']
                && att['annotation']['documentation'][0] == node['annotation']['documentation'][0]){
                isAnAttribute = true
            }
        })

        return isAnAttribute;
    }

    multiRecordNodeIds(){
        return [
            "Deployment__Sensors__Audio",
            "Deployment__Sensors__Audio__Number",
            "Deployment__Sensors__Audio__SensorId",
            "Deployment__Sensors__Audio__Geometry",
            "Deployment__Sensors__Audio__Name",
            "Deployment__Sensors__Audio__Description",
            "Deployment__Sensors__Audio__HydrophoneId",
            "Deployment__Sensors__Audio__PreampId",
            "Deployment__Sensors__Audio__Geometry__x_m",
            "Deployment__Sensors__Audio__Geometry__y_m",
            "Deployment__Sensors__Audio__Geometry__z_m",
            "Deployment__SamplingDetails__Channel__ChannelNumber",
            "Deployment__SamplingDetails__Channel__SensorNumber",
            "Deployment__SamplingDetails__Channel__Start",
            "Deployment__SamplingDetails__Channel__End",
            "Deployment__SamplingDetails__Channel__Sampling",
            "Deployment__SamplingDetails__Channel__Sampling__Regimen",
            "Deployment__SamplingDetails__Channel__Sampling__Regimen__TimeStamp",
            "Deployment__SamplingDetails__Channel__Sampling__Regimen__SampleRate_kHz",
            "Deployment__SamplingDetails__Channel__Sampling__Regimen__SampleBits",
            "Deployment__SamplingDetails__Channel__Gain",
            "Deployment__SamplingDetails__Channel__Gain__Regimen",
            "Deployment__SamplingDetails__Channel__Gain__Regimen__TimeStamp",
            "Deployment__SamplingDetails__Channel__Gain__Regimen__Gain_dB",
            "Deployment__SamplingDetails__Channel__Gain__Regimen__Gain_rel",
            "Deployment__SamplingDetails__Channel__DutyCycle",
            "Deployment__SamplingDetails__Channel__DutyCycle__Regimen",
            "Deployment__SamplingDetails__Channel__DutyCycle__Regimen__TimeStamp",
            "Deployment__SamplingDetails__Channel__DutyCycle__Regimen__RecordingDuration_s",
            "Deployment__SamplingDetails__Channel__DutyCycle__Regimen__RecordingInterval_s"
        ]
    }

    /**
     * Builds an object that gets added to the D3 tree
     * @param {String} nodeName become the value of the 'name' key
     */
    buildNode(nodeName){
        var node = {};
        node['name'] = nodeName;
        return node;
    }

    /**
     * toRoot recursively climbs from node to teh node's parent and returns the path to the root.
     * @param {Object} node 
     */
    toRoot(node){
        var path= "";
        if(node.parent != null) // Not root node
        {
            // find parent of root add parents name before yours \Deployment\DeploymentNumber\...
            return toRoot(node.parent)+"\/"+node.name;
        }
        return node.name;   //The root Node
    }

    /**
     * Incomplete haven't finished
     * @param {*} nodeTree 
     * @param {*} input 
     */
    searchRecursive(nodeTree, input)
    {
        var pathExists = false;
        var inputArr = input.split("\\");   //split the path into parts (i.e. \Deployment\Id -> ["", Deployment, Id]
        inputArr.shift();                   //remove first element. initially is a "" afterwards it's the last used cell
        if(inputArr.length == 1 && nodeTree.hasOwnProperty(inputArr[0])){   //last value in inputArr is also in tree
            console.log("proc");
            return true;
        }
        console.log(inputArr);
        console.log(nodeTree[inputArr[0]]);
        if (nodeTree[inputArr[0]] !== null && typeof(nodeTree[inputArr[0]]) == "object" ) {//child () exists and child is an object
            pathExists = this.search(nodeTree[inputArr[0]], inputArr.join("\\"));    //child node becomes the root node in next recursion and make the array a string again
            if(pathExists)                                                      //this was necessary to immediately exit all recursions with the true boolean value
                return pathExists;
        }
    }

    /**
     * Incomplete
     * @param {*} nodeTree 
     * @param {*} path 
     */
    searchNonRecursive(nodeTree, path)
    {
        var inputArr = path.split("\/");   //split the path into parts (i.e. \deployments\deploymentID -> ["", deployment, deploymentID]
        var node = nodeTree;                //root node
        
        if(inputArr[0] == "")               //remove "" from array if input string was lead with \
            inputArr.shift();
        console.log("name:"+ node['name']+ "\t"+inputArr[0]+"\t"+ (node['name'] == inputArr[0]));
        console.log("children:"+node.hasOwnProperty('children'));
        if(node['name'] == inputArr[0] && node.hasOwnProperty('children')){
            node = node.children;
        }
        for(var i = 1; i<inputArr.length; i++)  //for every item in the array
        {
            for(var index in node)
            {
                if(node[index]['name'] == inputArr[i])
                    if(i+1 == inputArr.length)
                        return node[index];
                    else if(node[index].hasOwnProperty('children')){
                        node = node[index].children;
                        break;
                    }
                    else if(node[index].hasOwnProperty('_children')){
                        node = node[index]._children;
                        break;
                    }
                    
            }
        }
        return;
    }

    /**all the now recursion functionality before the recursion */
    search(path){
        return this.searchNonRecursive(this.d3Json, path);
    }

    /**
     * Incomplete
     * Checks the node for and all node to the root 
     * @param {Object} node 
     * @param {String} attribute 
     */
    checkForAttribute(node, attribute)
    {
        var attributeFoundNodes = [];
        if(node.parent != null) // Not root node
        {
            var temp = this.checkForAttribute(node.parent, attribute);
            temp.forEach(element => {
                attributeFoundNodes.unshift(element);
            })
        }
        if(node[attribute])
            attributeFoundNodes.unshift(node);
        return attributeFoundNodes;
    }

    nodeOfUnbounded(nodeTree, input){
        if(this.checkAttribute( input, "@maxOccurs"))
            return;
    }

    /**
     * @return {D3Json}
     */
    getD3Json(){
        return this.d3Json;
    }

    /**
     * @return {XMLJson}
     */
    getXmlJson(){
        return this.xmlJson;
    }

    /**
     * show the entire D3Json Object (Tree)
     */
    showD3Json(){
        this.down(this.d3Json, "");
    }

    /**
     * show the entire XmlJson Object (Tree)
     */
    showXmlJson(){
        this.down(this.xmlJson, "");
    }

    /**
     * Prints out all the leafs and their paths to the root node
     * @param {Object} treeNode 
     * @param {String} path 
     */
    down(treeNode, path) {
        for (var childNode in treeNode) {
            if (treeNode[childNode] !== null && typeof(treeNode[childNode])=="object") {    //going one step down in the object tree!!
                this.down(treeNode[childNode], path+"\\"+childNode);
            }
            else    //No more child nodes. Tree bedrock. Prints only tree ends (final children, children without children)
            {
                console.log(path+"\\"+childNode+": "+treeNode[childNode]);
            }
        }
    }

    /**
     * @deprecated
     * Traverses down a tree and deletes all 'count' and 'flworNodes' keys
     * @param {Object} treeNode 
     */
    downReset(treeNode) {
        for (var childNode in treeNode) {
            if (treeNode[childNode] !== null && childNode !== 'parent' && childNode !== 'flworNodes' && typeof(treeNode[childNode])=="object" ) {    //going one step down in the object tree!!
                this.downReset(treeNode[childNode]);
            }
            delete treeNode.flworNodes;
            delete treeNode.whereCount;
            delete treeNode.returnCount;
            delete treeNode.forVar;
        }
    }
}
