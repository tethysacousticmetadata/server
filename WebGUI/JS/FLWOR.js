/**
 * @author Daniel Weston
 * @version 1.0
 * DSanchezWeston@gmail.com
 * 23 Aug 2016
 * This whole file of classes was me trying to imitate this documentation
 * https://www.w3.org/TR/2017/REC-xquery-31-20170321/#id-flwor-expressions
 * I tried to build classes for ExprSingle and everything that could be implemented inside it.
 * A lot of the structure is here, but the code to determine the which is which needs to be  written. 
 */
"use strict";

class ExprSingle{
    constructor(expr){
        this.expr = expr;
    }

    setExpr(expr){
        this.expr = expr;
    }
    
    getExpr(){
        return this.expr;
    }

    toString(){
        return this.expr.toString();
    }
}

/**
 * https://www.w3.org/TR/2017/REC-xquery-31-20170321/#prod-xquery31-FLWORExpr
 * This is a simple class to represent an xQuery FLWOR. xQuery FLWORS have lots of expressions for
 * the websites purposes there are intitalClause, intermeditateClauses, and a returnClause.
 */
class FLWOR{
    constructor(header, footer){
        this.intialClause;
        this.intermediateClause = [];   //can have multiple
        this.returnClause;
        this.header = header;
        this.footer = footer;
    }

    setInitialClause(initialClause){
        this.initialClause = initialClause;
    }

    getInitialClause(){
        return this.initialClause;
    }

    addIntermediateClause(intermediateClause){ 
        this.intermediateClause.push(intermediateClause);
    }

    getIntermediateClause(){
        return this.intermediateClause;
    }

    setReturnClause(returnClause){
        this.returnClause = returnClause;
    }

    getReturnClause(){
        return this.returnClause;
    }

    validate(){
        if(!this.initialClause){
            console.log("FLWOR Expression requires an initial clause!")
            return false;
        } 
        if(!this.returnClause){
            console.log("FLWOR Expression requires a return clause!")
            return false;
        }
        return true;
    }

    toString(){
        if(this.validate())
        {
            let buildString = this.header+"\n";
            if(this.initialClause)
                buildString += this.initialClause.toString() + "\n";
            if(this.intermediateClause.length>0){
                let tempStr = ""
                this.intermediateClause.forEach(interClause => {
                    if(interClause)
                        tempStr += interClause.exprSingle+" and\n";
                });
                let finalWhereClause = new WhereClause(tempStr.substring(0, tempStr.length - " and\n".length));
                buildString += finalWhereClause.toString() + "\n";
            }
            if(this.returnClause)
                buildString +=  this.returnClause.toString() + "\n";
            buildString += this.footer;
            return buildString;
        }
    }
}

/**
 * https://www.w3.org/TR/2017/REC-xquery-31-20170321/#doc-xquery31-ForClause
 * ForClause	   ::=   	"for" ForBinding ("," ForBinding)*
 */
class ForClause{
    constructor(){
        this.forBindings = [];
    }

    addForBinding(forBinding){
        this.forBindings.push(forBinding);
    }

    toString(){
        var buildString = "for ";
        this.forBindings.forEach(forBinding => {
            buildString += forBinding.toString()+" ,\n";
            });
            buildString = buildString.substring(0, buildString.length - " ,\n".length);
        return buildString;
    }
}

/**
 * https://www.w3.org/TR/2017/REC-xquery-31-20170321/#prod-xquery31-ForBinding
 * ForBinding	   ::=   	"$" VarName TypeDeclaration? AllowingEmpty? PositionalVar? "in" ExprSingle
 */
class ForBinding{
    constructor(varName, exprSingle){
        this.varName = varName;
        this.exprSingle = exprSingle;
    }

    setVarName(varName){
        this.varName = varName;
    }

    getVarName(){
        return "$"+this.varName;
    }
    
    setAllowingEmpty(allowingEmpty){
        this.allowingEmpty = allowingEmpty;
    }
    
    setExprSingle(exprSingle){
        this.exprSingle = exprSingle;
    }

    toString(){
        return this.getVarName() + " in " + this.exprSingle.toString();
    }
}

/**
 * https://www.w3.org/TR/2017/REC-xquery-31-20170321/#doc-xquery31-LetClause
 * LetClause	   ::=   	"let" LetBinding ("," LetBinding)*
 */
class LetClause{
    constructor(){
        this.letBindings = [];
    }

    addLetBinding(letBinding){
        this.letBindings.push(letBinding);
    }

    toString(){
        var buildString = "let ";
        this.letBindings.forEach(forBinding => {
            buildString += forBinding.toString()+" , ";
        });
        buildString = buildString.substring(0, buildString.length - 2);
        return buildString;
    }
}

/**
 * https://www.w3.org/TR/2017/REC-xquery-31-20170321/#prod-xquery31-LetBinding
 * LetBinding	   ::=   	"$" VarName TypeDeclaration? ":=" ExprSingle
 */
class LetBinding{
    constructor(varName, exprSingle){
        this.varName = varName;
        this.typeDeclaration;
        this.exprSingle = exprSingle;
    }

    setVarName(varName){
        this.varName = varName; 
    }

    getVarName(){
        return this.varName; 
    }

    setTypeDeclaration(typeDeclaration){
        this.typeDeclaration = typeDeclaration; 
    }

    setExprSingle(exprSingle){
        this.exprSingle = exprSingle; 
    }

    toString(){
        var buildStr = "$" + this.varName + " ";
        if(this.typeDeclaration)
            buildStr += this.typeDeclaration + " ";
        if(this.exprSingle)
            buildStr += ":= " + this.exprSingle.toString() + " ";
        return buildStr;
    }
}

/**
 * https://www.w3.org/TR/2017/REC-xquery-31-20170321/#prod-xquery31-ReturnClause
 * ReturnClause	   ::=   	"return" ExprSingle
 * 
 */
class ReturnClause{
    constructor(exprSingle){
        this.exprSingle = exprSingle;
    }

    /**
     * If a special regex is found add some extra syntax otherwise just print the expression
     * The where value equivalent of the special case is implemented in @see ULData.isSpecialData()
     */
    toString(){
        return "return "+this.exprSingle;
    }

}

/**
 * https://www.w3.org/TR/2017/REC-xquery-31-20170321/#doc-xquery31-IntermediateClause
 * IntermediateClause	   ::=   	InitialClause | WhereClause | GroupByClause | OrderByClause | CountClause
 */
class IntermediateClause{
    constructor(){
        this.clause;
    }

    setInitialClause(clause){
        this.clause = clause;
    }

}

/**
 * https://www.w3.org/TR/2017/REC-xquery-31-20170321/#prod-xquery31-WhereClause
 * WhereClause	   ::=   	"where" ExprSingle
 */
class WhereClause{
    constructor(exprSingle){
        if(exprSingle)
            this.exprSingle = exprSingle;
    }

    getExprSingle(){
        return this.exprSingle;
    }

    toString(){
        return "where " + this.exprSingle.toString();
    }
}

