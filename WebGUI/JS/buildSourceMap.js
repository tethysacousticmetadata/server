 /**
 * buildSourceMap.js
 * These functions support a web-based graphical user interface for designing the XML specification
 *
 * SourceMaps are XML specifications that map between user data and Tethys schemata to
 * permit users to map foreign data into Tethys. We support multiple foreign data sources, in
 * that one map data from two databases although we do not support queries that are dependent on
 * one another (e.g., you cannot do a cross database join).
 *
 * We denote foreign data sources with a source name, table, and attribute, separated by a character that
 * cannot be a legal character in any of the three above-named fields.  See class tablePaths:separator
 * for specification of the character (":" at the time of this writing).
 *
 * Many of functions deal with nodes in the D3 tree.
 * At some point, we may wish to separate out the control of the D3 tree from the data it represents.
 * Some of the important fields related to the SourceMap
 *
 * Some of the important node decorators that are used:
 * field - List of attributes from user source data being mapped
 * tableName - List of "table" like entities (table, sheet, CSV file) associated with the fields
 * customField - Used to combine multiple fields and/or provide additional text before or after a field
 * whereClause - A restriction on a value of an attribute.  Note that this may affect values in other areas
 *   as the restriction is placed at the SQL query
 * customIsCalc - boolean indicating the field is the result of a SQL calculated alias, e.g.
 *   SELECT t.min * 60 AS sec FROM t... ;
 *   In this case: the node's customAlias field would contain sec
 *   and customField [t.min] * 60.  (Alias names are generated automatically and are not meaningful)
 *
 * The following fields are generated when analyzing a D3 tree to generate an XML specification
 * They are all related to SQL queries.  They are only attached to nodes where queries are generated.
 * In general, queries are generated at the root of the XML schema tree or at nodes that can appear multiple
 * times.
 *
 * sqlTables - List of tables associated with a query.  Queries always appear at the root or in nodes
 *   that can be repeated.
 * sqlAttr - Attributes that will be transformed via computation into another name
 * sqlWhere - Conditions affecting SELECT statements
 *
 */

/**
 * Determine the path separator string.
 * @returns {string}
 */
function getPathSep() {
    // Assume only Windows is likely to have \ and everyone else uses /
    return sep = (window.navigator.platform.startsWith("Win")) ? "\\" : "/";
}

class tablePaths {
    // character used to separate fields
    static separator = ":"

    /**
     * Check is string has a path separator
     * @param path
     * @param separator - Defaults to the tablePaths.separator, but can be overriden
     * @return {boolean} - True path variable contains a multi-element path
     */
    static is_path(path, separator=this.separator) {
        return path.indexOf(separator) >= 0
    }

    /**
     * Combine an array of names into a path
     * @param fields
     * @return {string}
     */
    static combine(...fields) {
        return fields.join(this.separator)
    }

    /**
     * Split a path into its components.
     * @param path
     * @param separator - Defaults to tablePaths.separator but can be overriden
     * @return {*}
     */
    static split(path, separator = this.separator) {
        return path.split(separator)
    }

    /**
     * Return a string with the table and attribute.
     * By default, the table and attribute are separated by the default separator, but this
     * can be overridden.
     *
     * Examples (assuming tablePaths.separator == ':':
     *
     * get_tbl_attr('mysrc:mytbl:myattr')
     * returns:  mytbl|myattr
     *
     * get_tbl_attr('mysrc:mytbl:myattr', '.')
     * returns:  mytbl.myattr
     *
     * get_tbl_attr('spreadsheet:some table:attrib2', '.', '`')
     * returns:  `some table`.`attrib2`
     *
     * @param path - path to parse
     * @param delim - separator used in reconstruction, e.g. use '.' if building for SQL
     * @param quote - If provided, each field is quoted with this character.  For example, use backtick in SQL.
     *                If two characters are provided, they book-end each field, e.g. "[]" -> [field]
     * @returns {*}
     */
    static get_tbl_attr(path, delim=this.separator, quote=null) {
        return this.mutate(path, delim, quote, -2)
    }

    /**
     * Return source name
     * Caveat:  Assume source is first element of the path
     * @param path
     * @returns {*}
     */
    static get_src(path) {
        let path_els = path.split(this.separator)
        return path_els[0]
    }

    /**
     * Get source and table
     * @param path
     * @returns {*}
     */
    static get_src_tbl(path) {
        let path_els = path.split(this.separator);
        return path_els.slice(0, 2)
    }

    static get_attr(path) {
        return path.split(this.separator).pop()
    }

    /**
     * Return array [first element, rest of path]
     * @param path
     * @returns {*[]}
     */
    static get_head_tail(path) {
        let path_els = path.split(this.separator);
        return [path_els[0], path_els.slice(1).join(this.separator)]
    }

    /**
     * Return a dictionary with potential keys:  source, table, attr
     * providing the source name, table name, and attribute name.
     * If only two of the three components are present, it ambiguous
     * whether the path contains  source & table or table & attrib.
     * In this case, the optional argument as lets us know which
     * case should be prioritized.
     * @param path - relational DBMS path
     * @param as - Only used when < 3 path elements:
     *    srctbl - path contains source and table (default)
     *    tblattr - path contains table and attribute
     */
    static get_names(path, as="srctbl") {
        let path_els = path.split(this.separator);
        let els = {}
        if (path_els.length < 3) {
            if (as == "srctbl") {
                els.source = path_els[0];
                els.table = path_els[1];
            } else {
                els.table = path_els[0];
                els.attr = path_els[1];
            }
        } else {
            els.source = path_els[0];
            els.table = path_els[1];
            els.attr = path_els[2];
        }
        return els
    }

    /**
     * Change a path to a different format.  Allows mutating delimeter
     * between path elements and providing quote elements between
     * each element.
     * Example:  .mutate("a:b", '!', '"') --> "a"!"b"
     * @param path - Path that is separated by class variable separator
     * @param delim - new delimiter
     * @param quote - quote character(s), if two characters, first is placed before and second after
     * @param start - first element (obeys slice rules)
     * @param end - last element (obeys slice rules)
     */
    static mutate(path, delim=this.separator, quote=null, start=null, end=null) {
        let path_els = path.split(this.separator);
        if (quote != null) {
            if (quote.length == 1) {
                quote = quote + quote  // front/back quote will be the same character
            }
            path_els = path_els.map(val => quote[0] + val + quote[1])
        }

        if (start == null)
            start = 0
        if (end == null)
            end = path_els.length
        return path_els.slice(start,end).join(delim)
    }
}

/**
 * Given a set of nodes that are being loaded/saved, identify the sources
 * that were used and return the list of source descriptors
 * @param nodes
 * @returns {*[]}
 */
function findEmployedSources(nodes) {
    // Identify the sources used in the paths
    let pathsN = nodes.length;
    let source_ids = new Set()
    for (let idx = 0; idx < pathsN; idx++) {
        // Specified a table?
        if (nodes[idx].table != null) {
            let tables = nodes[idx].table
            for (let tidx = 0; tidx < tables.length; tidx++) {
                source_ids.add(tablePaths.get_src(tables[tidx]))
            }
        }
    }

    // Filter existing sources for those used
    let sources = window.allSourceNames;
    let used = []
    for (let sidx = 0; sidx < sources.length; sidx++)
        if (source_ids.has(sources[sidx].sourceName))
            used.push(sources[sidx])

    return used
}

/**
 * Save import map specification to server.
 * Differs from publishing in that this specification can be reloaded and edited
 * whereas a published map loses the information saved here
 * @param checkValidation
 * @returns {null}
 */
function saveConfiguration(checkValidation)
     {
         //check if the config name is already taken
         // if checkValidation is false, then they want to override the config
         let configName = $("#configName").val()
         if(checkValidation){
             let repeatName = false
             let savedConfigNames = document.querySelectorAll('.configNameSelect')
             savedConfigNames.forEach(ele => {
                if($(ele).val().toLowerCase() == configName.toLowerCase()){
                    repeatName = true
                }
             })

             if(repeatName){
                 showConfigRepeatName(configName);
                 return null;
             }
         }

         // Create configuration object
         let configInfo = extractJsonInfoFromDropPath(dropPaths)
         let config =  {
             'schemaName': $('#schemaSelector').val(),
             'dropPaths': configInfo.node_list,
             'configName': configName,
             // todo:  Do we want to save relations that people have set up?  perhaps
             // todo:  but if there are changes in the database... 'relations': window.Relations,
             'sourceNames': findEmployedSources(configInfo.node_list),
             'produce': $("#DataSourcesGenerate input:radio:checked").get(0).id
         }

         let form = new FormData();  // Data to be passed to Tethys

         // Marshall to pretty-printed Javascript object notation (JSON) string
         let json_str = JSON.stringify(config, null, '  ')
        form.append("jsonString", json_str)

         //ajax call into the server to save the config
        var ajaxCall = new AjaxWrapper("saveImportConfig", form, showajaxError);

        $.ajax(ajaxCall.getSettings()).done(function (response) {
            if (response.hasOwnProperty('errors')) {
                reportOdbcErrors(response['errors'])
                return;
            }

            alert('Configuration ' + configName + ' Successfully Saved to the Server')

             //remove and reset the config dropdown list
             $('.configNameSelect').remove()
             getAllConfigs();

        }).fail(function(error){
            alert(error['statusText']);
        })
    }

/**
 * Retrieve draft import configuration (JSON) from server
 * @param configName
 */
function getConfiguration(configName) {

    if (configName == "")
        return

        let form = new FormData()
        form.append('configName', configName)

        //ajax call to grab import config to load in
        // let ajaxCall = new AjaxWrapper("getimportconfig", null, showajaxError);   //perform a GET to get XMLJson
        let settings = {
             url: "/ImportConfig",
             data: {"configName": configName },
             async: false
        }

        $.ajax(settings).done((response)=> {
            ingestConfigData(response)
        })
}

/**
 * Transform a string into a valid XML element name.
 * Valid names must start with an alphabetic character or _
 * Groups of illegal characters are replaced by an underscore.
 * When the name does not start with a character, we prepend a_
 *
 * Note that while colon is a valid in an element name, it is used to represent a namespace
 * and we want to map user data into the Tethys namespace.  As a consequence, we translate :
 * to underscore as well.
 *
 * @param s
 * @return {string}
 *
 * Example:  toValidXml("3 @#little:pigs") --> a_3_little_pigs
 */
function toValidXml(s) {
    let maxlen = 1024;  // max len for an element name defined by XML standard

    // replace groups of bad characters with a single underscore
    let elem_name = s.replace(/[^\p{Letter}\p{Number}\-_\.]+/gu, "_");
    // Make sure thst element starts with a letter
    if (/^\p{Letter}/u.test(elem_name) == false) {
        // User string would result in illegal XML element name, prepend
        elem_name = "_" + elem_name;
    }
    if (elem_name.length > maxlen)
        elem_name = elem_name.slice(0, maxlen);
    return elem_name
}

class Connection {

    constructor(node) {
        this.fieldName = node.field;
        this.tableName = node.tableName;
        this.sourceName = node.source;
        this.defaultValue = node.defaultValue;
        this.customField = node.customField;
        this.customAlias = node.customAlias;
        this.customIsCalc = node.customIsCalc;

        this.draggedNode = node;
        this.maxOccursPath = []

    }  //end of contructor


    addFieldAndTableToNode(node)
    {
        return node;
    }


    delay(seconds){
        return new Promise(resolve => {
            setTimeout(resolve, seconds * 1000);
        });
    }

    /**
     * Recursively analyze a node and its parents when a user maps a Tethys
     * field to a table and attribute from their example data source.
     * @param node
     */
    toRootMaxOccurs(node, selectionPlaced=false) {
        if (node != undefined && node.name != 'Collections') {
            // We have a valid node

            node['id'] = node.id

            // Determine if we are at the root level or if there is a sequence associated with this node
            let isRootNode = node.hasOwnProperty('parent') == false
                || node.parent.name == "Collections"
            let isSequence = (node.hasOwnProperty('maxOccurs') && node.maxOccurs == 'unbounded');

            // Node has children and all of them are hidden?
            let allChildrenHidden = (node._children != null) && (node.children == null);
            if (allChildrenHidden) {
                // Simulate a click on the node to expand the hidden children
                d3Controller.click(node)
                // Unclear why the delay was added here.
                // Perhaps it was added to provide time for other updates?
                this.delay(1)
            }

            // Build a path to the ancestor where the select will go
            // Last element will be the element where the select is placed unless
            // it is modified later by analysis the bindings that we are creating.
            this.maxOccursPath.push(node)

            // Recurse to complete the path to the root node and find the correct
            // place to put the SQL query if we have not already done so.
            this.toRootMaxOccurs(node.parent, selectionPlaced);
        } else {
            return // base case of recursion
        }


    };
}
/////////////////////////////////////////////////////////

class LRUCache {
  constructor(capacity) {  // yes put how do I determine capacity
    this.cache = new Map();
    this.capacity = capacity;
  }

  get(key) {
    if (!this.cache.has(key)) return -1;

    let val = this.cache.get(key);

    this.cache.delete(key);
    this.cache.set(key, val);

    return val;
  }

  // Implementing Put method
  put(key, value, deleteKey) {
    this.cache.delete(key);
    let lsr_value;
    if (this.cache.size === this.capacity) {
      // two different deletes depending on if 3rd arg is sent
        if (deleteKey){
            lsr_value = self.get(deleteKey);
            self.cache.delete(deleteKey)
        } else {
            lsr_value = this.cache.values().next().value;
            this.cache.delete(this.cache.keys().next().value);
        }
      this.cache.set(key, value);
    } else {
      this.cache.set(key, value);
    }
    return lsr_value
  }

  getLeastRecent(index) {  // 0 is least
    return Array.from(this.cache)[index];
  }

  getMostRecent() {
    return Array.from(this.cache)[this.cache.size - 1];
  }

}







/////////////////////////////////////////////////////////
class SourceMapTreeController extends D3TreeController {

    //constructor(d3, parentController){
    //    super(d3, parentController)
    //    alert('in new constructor')
    //}

    update(source) {
        //super.update(source)
         //Whenever the tree changes update those changes
        let _this = this; //keeps scope inside anonymous functions

        let nodes = this.tree.nodes(this.d3.root).reverse(),
            links = this.tree.links(nodes);

        // Normalize for fixed-depth.
        nodes.forEach(function(d) {
            d.y = d.depth * D3TreeController.NODE_TO_NODE_DEPTH;
        });

        // Update the nodesvg¦
        var node = this.svg.selectAll("g.node")
            .data(nodes, function(d) {
                return d.id || (d.id = ++_this.i);
            })

        // Enter any new nodes at the parent's previous position.
        // Create a group "g" element and set contents
        var nodeEnter = node.enter().append("g")
            .attr("class", "node")
            .attr("transform", function(d) {
                return "translate(" + source.x0 + "," + source.y0 + ")";    //top to bottom
                //return "translate(" + source.y0 + "," + source.x0 + ")";  //uncomment this if you want the dendogram to go left to right
            }).on("click", function(node) {
                  _this.click(node);
            }) // adding a comment
            .on("dblclick", function(node){
                _this.__clickedNode = node;
                return false;
            })
            .on("mouseover", function(d) {
                var g = d3.select(this); // The D3 selection
                let node = g[0][0]['__data__'];  // Access the schema node
                //Add field description when hovering over
                let jfieldDescriptionName = $("#fieldDescriptionName");
                let jfieldDescription = $("#fieldDescription");
                jfieldDescriptionName.show()
                // Construct long-form Tethys schema element to table attributes
                jfieldDescriptionName.text(buildFieldMappingText(node, true) + " - ");
                jfieldDescription.show()

                let description = []
                if(node['description'] != undefined) {
                    if(node['description'].length == 1){
                        description.push(node['description'][0]["$"])
                    } else{
                        node['description'].forEach(desc => {
                            description.push(desc["$"])
                        })
                    }
                } else
                    description.push("No description. ")

                jfieldDescription.text(description.join(" "))
            })
            .on("mouseout", function() {
                // Remove the info text on mouse out.
                //$('#fieldDescriptionBox').hide()
                $('#fieldDescriptionName').text()
                $('#fieldDescription').text()
                //d3.select(this).select('text.info').remove()
            });

        nodeEnter.append("circle")
            .attr("r", 1e-5)
            .style("fill", function(d) {
                return d._children ? "lightsteelblue" : "#fff";//light stell blue if has children. Empty if it doesn't.
            });

        nodeEnter.append("text")    //the position of node text
            .attr("y", function(d) {//if children place above else place below
                return d.children || d._children ? -12 : 17;
            })
            .attr("dx", 0)
            .attr("text-anchor", function(d) {//if children place anchor at start else end
                return "middle";
                //return d.children || d._children ? "end" : "start";
            })
            .text(function(d) {
                return d.name;
            })
            .style("fill-opacity", 1e-6);

        // Transition nodes to their new position.
        var nodeUpdate = node.transition()
            .duration(this.duration)
            .attr("transform", function(d) {
                return "translate(" + d.x + "," + d.y + ")";    //top- down
                //return "translate(" + d.y + "," + d.x + ")";  //uncomment this if you want the dendogram to go left to right
            });

        nodeUpdate.select("circle")
            .attr("r", 7)   //change circle size
            .style("fill", function(d) {
                 if (d.hasOwnProperty('choiceFilled') && d['choiceFilled'] == true){
                    return "#fff"; //if choice is filled in, the other choice is optional
                }
                else
                if((d.field != undefined && d.field.length > 0)
                    || d.defaultValue != undefined){
                    return "#4f9440"
                }
                else if (d.class === "found") {
                    return "#ff4136"; //red
                } else if (d._children) { // ._children is hidden children
                    if (d.minOccurs == '1' && d._children[0].attribute)
                        return "#C35671"
                    else if (d.minOccurs == '1')
                        return "#F3B1C1"
                    return "lightsteelblue";
                } else {  // this branch is for fully expanded or terminal leaf node
                    if (d.minOccurs == '1' && (!d.hasOwnProperty('children')))
                        return "#C35671"
                    if (d.minOccurs == '1' && d.children[0].attribute)  // && d.children && d.children[0].attribute
                        return "#C35671"
                    //if (d.minOccurs == '1' && d.children[0].name.toLowerCase() == 'group')
                        //return "#C35671"
                    return "#fff";      //white
                }
            })
            .style("stroke", function(d) {  //circle outline
                if (d.class === "found") {
                    return "#ff4136"; //red
                }
            })
            .style("stroke-dasharray", function(d) {
                if (d.anyParameter == "1") {
                    return ("5", "3");
                } else
                    return ("50", "0");  // no dash
            });

        nodeUpdate.select("text")
            .style("fill-opacity", 1);

        // Transition exiting nodes to the parent's new position.
        var nodeExit = node.exit().transition()
            .duration(this.duration)
            .attr("transform", function(d) {
                return "translate(" + source.x + "," + source.y + ")";  //top- down
                //return "translate(" + source.y + "," + source.x + ")";  //uncomment this if you want the dendogram to go left to right
            })
            .remove();

        nodeExit.select("circle")
            .attr("r", 1e-6);

        nodeExit.select("text")
            .style("fill-opacity", 1e-6);

        // Update the linksâ€¦
        var link = this.svg.selectAll("path.link")
            .data(links, function(d) {
                return d.target.id;
            });

        // Enter any new links at the parent's previous position.
        link.enter().insert("path", "g")
            .attr("class", "link")
            .attr("d", function(d) {
                var o = {
                    x: source.x0,
                    y: source.y0
                };
                return _this.diagonal({
                    source: o,
                    target: o
                });
            });

        // Transition links to their new position.
        link.transition()
            .duration(this.duration)
            .attr("d", this.diagonal)
            .style("stroke", function(d) {
                if (d.target.class === "found") {
                    return "#ff4136";
                }
            });

        // Transition exiting nodes to the parent's new position.
        link.exit().transition()
            .duration(this.duration)
            .attr("d", function(d) {
                var o = {
                    x: source.x,
                    y: source.y
                };
                return _this.diagonal({
                    source: o,
                    target: o
                });
            })
            .remove();

        nodes.forEach(function(d) {
            d.x0 = d.x;
            d.y0 = d.y;
        });

        /**
         * This is a bizarre neccesity
         * This JQuery function allows an element to be created at a node.
         * @var _this.__clickedNode this variable was assigned up in the nodeEnter.on.("click", ...)
         * It had to be done this way. Putting the code inside the on click
         * would lose the e event and out here we wouldn't have context to the node, so
         * it's been bridged by @var _this.__clickedNode .
         */
        //$(".node").on("dblclick.zoom", null)
        $(".node").on("dblclick", function (e) {
            let fun = ()=>{
                let node = _this.__clickedNode;
                console.log("node")
                console.log(node)
                _this.__clickedNode = undefined;
                //_this.toCollection(node, "\/")
                var inner = $("<div id="+_this.toRoot(node, "_")+" class = 'input-group-text'>"+node.name+"</div>", {class:"inner"}).draggable({
                cursor: "grab",                 //up above toRoot(node, "_") is used instead of "\/" to not cause an error in JQuery.
                    connectToSortable: ".sortable",
                    //connectWith: ".myClass.sorting.ui-state-default",
                    //helper: "clone",
                    revert: "invalid",
                    scroll: "true"
                }).appendTo("body");
                //inner.data("node", node);
                console.log(node)
                inner[0]['node'] = node;  //is this where item is set on ui obj in Jquery_Ui.js?
                inner.css("position", "absolute");
                e.type = "mousedown.draggable";
                e.target = inner[0];
                inner.css("left", e.pageX);
                inner.css("top", e.pageY);
                //$("#"+_this.toRoot(node, "_")).on("mouseup", function(e){ alert('in drop')//up above toRoot(node, "_") is used instead of "\/" to not cause an error in JQuery.
                //    this.outerHTML = "";    //delete element on mouseup.
                //});

                //if your drag does not land on a droppable element, remove it
                $(':not(.dropFieldTrigger)').on('mouseup', function(){
                    $("#"+_this.toRoot(node, "_")).remove()
                })

                $('.dropFieldTrigger').on("mouseup", function(e){ //alert('in drop')//up above toRoot(node, "_") is used instead of "\/" to not cause an error in JQuery.
                    //this.outerHTML = "";    //delete element on mouseup.
                    $("#"+_this.toRoot(node, "_")).remove()
                });

                inner.trigger(e);
            }
            if(!_this.__clickedNode){
                setTimeout(fun,10);
            }else{
                fun();
            }
            return false;
        });
    }

}


/**
 * annotateSqlNodes -
 * During the drag and drop functionality, the sql select fields and tables move to the closest sequence parent
 * to minimize hits to the database/data retrieval, we want to merge sql calls that share the same tables
 * this function goes through the drops and if they have a tableNeeded, it searches below nodes for those same
 * tables to bring up the table and select fields
 *
 * @param root - Root node of schema (stored in D3 tree)
 * @param paths - Array of arrays.  Each sub-array is a path from a node that was bound to user
 *      source data to the root node
*/
function annotateSqlNodes(root, paths) {
    let result = {}
    let err_list = []
    /*
     * The D3 tree may contain information related to queries from previous invocations.
     * As the relationship may have changed, clear out any fields related to this before
     * we start analysis.
     * todo: reengineer so that we are not so reliant on the D3 tree
     */

    /* dropPaths is an array where each element is an array of nodes.
     * The first entry is the node to which we are binding data and contains
     * information related to the table and attribute from which data will come.
     *
     * Subsequent entries are the parent nodes to the root of the XML document.
     * Parent entries that occur more than once will be decorated with a sqlTablesNeeded
     */

    // Determine which tables we will need to pull data from
    const tables = {}
    paths.forEach(drop => {
        /* First item on each droppath list will have a list of tablenames involved in
         * constructing a value unless we map the field to a constant in which case
         * no table was used.
         */

        // Determine all tables used at this node
        let table_set;
        if (drop[0].tableName != null)
            table_set = new Set(drop[0].tableName);
        else
            table_set = new Set();

        // Where clauses may reference additional tables
        let clauses = drop[0].whereClause
        if (clauses != null) {
            for (let i=0; i < clauses.length; i++) {
                if (clauses[i].lhs.table != null)
                    table_set.add(clauses[i].lhs.table);
                if (clauses[i].rhs.table != null) {
                    table_set.add(clauses[i].rhs.table);
                }
            }
        }
        // Associate the drop path which each of the tables the first node references
        table_set.forEach(table => {
                if (! tables[table]) {
                    tables[table] = []  // initialize list
                }
                tables[table].push(drop)  // associate dropPath list with this table
            })
    })

    // Sets to note which nodes carry SQL queries & queries that are across multiple tables
    let sqlNodes = new Set()  // nodes that will carry SQL queries
    let multitable = new Set()  // nodes with SQL queries FROM multiple tables
    /*
     * For each table, determine the least upper bound of the tables within the drop paths and ensure
     * that we place exactly one table needed entry at an appropriate level.
     *
     * CAVEAT:  If someone wants to run two separate queries against the same table data in different
     * parts of the XML tree, we will merge these.  We could do something smarter with user-guided
     * intervention, but that is beyond our scope for now which addresses the needs of most users.
     */
    for (const table in tables) {
        // Get list of paths associated with this table and find the length of the shortest one
        let pathsThisTable = tables[table]
        let shortestPath = Math.min(...pathsThisTable.map((p) => p.length))

        // Find the shallowest node that allows multiple instances across all paths
        // that use this table
        let minDepth = 0
        let curDepth = 0
        let done = false
        while (! done) {
            // Verify path to shallowest node identical on all paths
            let identical = true
            let offset = - (curDepth + 1)
            // Setup slice range (awkward as selecting last item different than previous ones)
            let rng = (offset == -1) ? [-1] : [offset, offset+1]
            // Grab a node at the current depth from any path (first one will do)
            let node = pathsThisTable[0].slice(...rng)[0]
            // Ensure that the node is identical to nodes on other paths at the same depth
            for (let path_idx = 0; path_idx < pathsThisTable.length; path_idx++) {
                let path_set = pathsThisTable[path_idx]
                let target = path_set.slice(...rng)[0]  // Access current depth
                identical = target === node
                if (! identical)
                    break
            }
            if (identical) {
                // all paths are consistent  If multiple elements are possible,
                // this might be the correct depth, but we'll need to see if
                // there's anything deeper
                if (node.maxOccurs != null && node.maxOccurs == "unbounded")
                    minDepth = curDepth
                // need to search deeper?
                curDepth = curDepth + 1
                done = curDepth >= shortestPath - 1
            } else {
                done = true
            }
        }

        /*
         * minDepth has the depth at which we should place our select clause
         * Any path at minDepth (counting from back) will contain the node that
         * should be targeted for the SQL SELECT clause.
         * Update this target node to include a list of tables and attributes used in the SQL query
         */
        let offset = -(minDepth+1)
        let rng = (offset == -1) ? [-1] : [offset, offset+1]
        let target = pathsThisTable[0].slice(...rng)[0]

        // Note that this table is used
        if (target.sqlTables == null)
            target.sqlTables = new Set()  // first time we need table at this node
        target.sqlTables.add(table)
        if (target.sqlTables.size > 1)
            multitable.add(target)  // Note more than one table associated with node

        sqlNodes.add(target)  // Note we modified this target node

        // Analyze each drop path to determine what needs to be added to the select
        if (target.sqlAttr == null)
            target.sqlAttr = new Set()
        for (let paths_idx = 0; paths_idx < pathsThisTable.length; paths_idx++) {
            let node = pathsThisTable[paths_idx][0]   // First node contains XML element binding to attribute
            // Add fields associated with the current table to selection set
            node.field.forEach(f => {
                if (f.includes(table)) {
                    // Field is associated with the current table, add to selections
                    if (typeof(f) != 'string')
                        f = f[0]  // unclear why we need this, set a breakpoint and try to find...
                    f = tablePaths.get_tbl_attr(f)  // Remove source name
                    target.sqlAttr.add(f)
                }
            })

            /*
             * Process where clauses and derived (calculated) values
             *
             * todo: If a computation/where involves more than one table, we need to do this at the
             * innermost SELECT node and use Tethys's nested query mechanism.  We are not dealing with
             * this now as most computations are likely to be from a single table.
             */

            // When computation is required, we update the aliasField of the node used to construct
            // the SQL query so that we know to alias some fields to a new name
            if (node.customIsCalc != null && node.customIsCalc == true) {
                // Bind attribute to a numeric expression using SQL as
                if (target.sqlAttrAs == null)
                    target.sqlAttrAs = new Set()
                target.sqlAttrAs.add(node.customField + " as " + node.customAlias)
            }
            // Process where clause if it exists
            if (node.whereClause != null && node.whereClause.length > 0) {
                if (target.sqlWhere == null)
                    target.sqlWhere = []
                target.sqlWhere.push(...node.whereClause)
                // Gets fully qualified names
                let attrs = getWhereAttributes(node.whereClause)
                attrs.forEach(attr => {
                    let t = tablePaths.get_names(attr)
                    // Transform to correct format and add
                    target.sqlTables.add(tablePaths.combine(t.source, t.table))
                    target.sqlAttr.add(tablePaths.combine(t.table, t.attr))
                })
            }
        }

        let tbl_attr = Array.from(target.sqlAttr).join(", ")
        console.log(`Query added node ${target.id}:  Table ${table} Selection ${tbl_attr}`)
    }

    // Audit list of nodes with SQL queries to make sure we are not trying to query from
    // multiple sources which we do not support
    sqlNodes = Array.from(sqlNodes)
    for (let idx=0; idx < sqlNodes.length; idx++) {
        // Check for more than one source
        let tables = Array.from(sqlNodes[idx].sqlTables)
        let sources = new Set(tables.map(t => tablePaths.get_src(t)))
        if (sources.size > 1) {
            srcNames = Array.from(sources)
            err_list.push(`Tethys destination element ${sqlNodes[idx].name} cannot query from multiple sources simultaneously: ${srcNames}`)
        }
    }

    showDecoratedTree(root, ["sqlTables", "sqlAttr", "sqlWhere"])

    result.sqlNodes = sqlNodes
    result.multitable = multitable
    if (err_list.length > 0)
        result.error = err_list.join(". ")
    else
        result.error = null
    return result
}

/**
 * Construct a set of attributes used in a set of where clauses
 * @param whereClauses  list of comparison dicts
 * @return {*[]}
 */
function getWhereAttributes(whereClauses) {
    let attr = new Set()
    for (let idx=0; idx < whereClauses.length; idx++) {
        let clause = whereClauses[idx];
        if (clause.lhs.operandType == "attribute")
            attr.add(clause.lhs.attr)
        if (clause.rhs.operandType == "attribute")
            attr.add(clause.rhs.attr)
    }
    return attr
}
// ------------------------- Join processing callbacks  -------------------------
function initializeJoinDialog(event) {
    linkTables($(event.currentTarget));
}

/**
 * Invoked each time we have a node that requires manual selection of key/foreign key
 * relations for where clause
 * @param datastore - interface for resolving joins
 */
function linkTables(datastore) {
    // We can create SQL queries involving joins at multiple elements
    // of the XML document that we are producing.  Retrieve two parallel
    // lists, one with information about the element nodes at which we
    // are performing the joins, the second with criteria for joining
    // the tables.  We assume that users are doing an inner join and
    // that the user is providing information to link the two tables.
    // In many cases, this will result in a 1:1 relationship, but need
    // not do so.
    let nodes = datastore.data("joinNodes");  // elements with joins
    let linkages = datastore.data("linkages");  // information about joins
    // current element
    let idx = datastore.data("linkage_idx");

    while (idx < linkages.length && linkages[idx].resolved()) {
        idx = idx + 1;
    }
    if (idx >= linkages.length) {
        finishJoinDialog(datastore);
    }

    let node = nodes[idx]

    // Select one or more tables to attempt to match
    let target_set = linkages[idx].smallest_set();
    let match_to = linkages[idx].other_tables(target_set)

    // populate general information about the current where clause we are
    // attempting to resolve
    let target_tables = Array.from(target_set);
    $('.sqlQueryAtField').html(node.name);
    $('.tablesNeedingToBeConnected').text(linkages[idx].toString())
    //$('.joinT1Table').html('<strong>' + target_tables[0] + '</strong>')

    $('.matchingArea').hide()
    $('.finalConnectionBox').hide()

    // Path to node where join will take place
    let path = node.id.replace("__", "/")
    $('.relationPathId').text(path)

    // Remove any existing associations between fields
    $('.relationshipUl').find(".joinCriterion").remove();

    // There are two possible lists of table that we can match from/to, populate these
    joinDialogBuildTableSelect($("#joinT1Table"), "joinT1Entry", target_tables)
    joinDialogBuildTableSelect($("#joinT2Table"), "joinT2Entry", match_to)
    // and show the attributes for the currently selected table
    let t1 = target_tables[0]
    let t2 = match_to[0]
    $('.joinT1Attribs').html(createFieldButtonForRelationship(t1, true))
    $('.joinT2Attribs').html(createFieldButtonForRelationship(t2, true))
    // Ensure no attributes currently selected
    let commonAttribs = joinDialogDefaultTableAttrib(t1, t2)
    let defaultMatch = commonAttribs.length > 0
    if (defaultMatch) {
        // Select and populate a default match if available.
        // We do not use Id as it is commonly used as a row number and is unlikely to be a good match
        let attrib = null
        let didx = 0
        while (attrib == null && didx < commonAttribs.length) {
            if (commonAttribs[didx].toLowerCase() != "id")
                attrib = commonAttribs[didx];
            else
                didx = didx + 1;
        }
        if (attrib != null) {
            // Find the matching buttons and select them
            let data = tablePaths.combine(tablePaths.split(t1)[1], attrib)
            $(".joinT1Attribs").find(`[data-field='${data}']`).click()

            data = tablePaths.combine(tablePaths.split(t2)[1], attrib)
            $(".joinT2Attribs").find(`[data-field='${data}']`).click()

            $(".addRelationConnectBtn").toggle(true)  // Enable adding relation
        } else
            defaultMatch = false
    }
    if (! defaultMatch) {
        // Nothing in common, ensure selected attribute data null
        $(".joinT1").data('field', null)
        $(".joinT2").data('field', null)
        $(".addRelationConnectBtn").toggle(false)  // Button not valid until we select pair
    }

    // Store currently selected table
    $(".joinT1").data('table', target_tables[0]) // Table identifier
    $(".joinT2").data('table', match_to[0]) // Table identifier

    $(".matchingArea").show()  // make areea visible

    // Hide buttons that are not yet valid
    $(".alignTables").toggle(false)
}

/**
 * Given two tablenames, see if they have any common attributes.
 * If so, select the attribute buttons for one of the common attributes
 * @param t1
 * @param t2
 * @return Array of intersection of t1 and t2 attributes
 */
function joinDialogDefaultTableAttrib(t1, t2) {
    // Retrieve attributes associated with tables
    t1 = tablePaths.get_names(t1)
    t2 = tablePaths.get_names(t2)
    let t1attribs = window.sourceData[t1.source].tables[t1.table].attributes;
    let t2attribs = new Set(window.sourceData[t2.source].tables[t2.table].attributes);
    // Determine common attributes
    let intersection = t1attribs.filter(val => t2attribs.has(val))
    return intersection
}

/**
 * Wrap up the join modal (table alignment)
 * @param modal_elem
 */
function finishJoinDialog(modal_elem) {
    modal_elem.modal('hide');
    // todo - Think about linkage tables and how we pass these to createMap2
    // could decorate nodes, dict with node ids to table...
    createMap2();  // generate and publish the import map
}

/**
 * We use dropdown lists to select tables within the join dialog.
 * @param selector - dropdown selector object
 * @param className - class associated with list of tables
 * @param tables - list of table options
 */
function joinDialogBuildTableSelect(selector, className, tables) {
    // Remove current items in selector
    selector.find('option').remove().end()
    // Add tables to selector
    for (let idx=0; idx < tables.length; idx++) {
        // Construct an element to represent the table
        let el = document.createElement('option')
        el.className = className
        el.value = tables[idx]
        el.text = tables[idx]
        selector.append(el)  // Add to list of selections
    }
}

/**
 * When specifying join criteria, we need to populate table names and fields for each of the tables
 * being joined.
 * @param tableName - Table being targeted
 * @param matchAreaTableNm - Selector for populating table name in textual description
 * @param fieldCardTableNm - Selector for populating table name in card representing table
 * @param dataTarget - Card representing table, will have 'table' data property set to tableName
 * @param attrTarget - Selector for where table attributes will be displayed
 */
function join_table_attrib_choices(tableName, dataTarget, attrTarget)
{
    $(dataTarget).data('table', tableName)
    $(attrTarget).html(createFieldButtonForRelationship(tableName))
}

/**
 * Add buttons for attributes of table
 * @param table - name of table including source
 * @returns {string} - HTML listing table attributes as buttons, each
 *                     with attribute data-main indicating table association.
 *
 */
function createFieldButtonForRelationship(table){
    let tbl = tablePaths.get_names(table)

    let tableFields = window.sourceData[tbl.source].tables[tbl.table].attributes
    let fieldsHtml = '<div class="row tableFields">'
    let btn_class = "btn btn-outline-primary btn-sm mb-2 smallButtonFont relationFieldBtn"
    tableFields.forEach(field => {
        let tableField = tablePaths.combine(tbl.table, field)
        fieldsHtml += `<div class="col-lg-6 col-sm-4 text-left">
                       <button class="${btn_class}" data-table="${table}" data-field="${tableField}">
                       ${field}
                       </button></div>`
        })

    fieldsHtml += '</div>'

    return fieldsHtml;
}

// --------------------- where clause management ------------------

/*
 * Tried to do this as an HTML5 template, but it was not playing nicely with selectpicker
 * Ended up having to use hard-coded ids, anything in {} will be replaced with a number
 * when this HTML is injected.
 *
 * SQL comparison generator
 * User specifies table.attr or value for left and right hand operands and an operator
 * to compare them.
 */
whereComponent = `

      <div class="row comparison" id="whereClause{}">
        <div class="WhereLHS col-md">
          <!-- left operand -->
          <nav>
            <div class="nav-tabs nav" role="tablist">
              <button class="nav-link active tblAttrTab" data-toggle="tab" data-target="#lhsTblAttr{}" type="button" role="tab" aria-controls="field-control" aria-selected="true">
                Field
              </button>
              <button class="nav-link valueTab" data-toggle="tab" data-target="#lhsVal{}" type="button" role="tab" aria-controls="value-control" aria-selected="false">
                Value
              </button>
            </div>
          </nav>
          <!-- Operand to a where clause -->
          <div class="tab-content">
            <!-- Table/Attribute specifiers -->
            <div class="tab-pane show active whereTblAttr" role="tabpanel" id="lhsTblAttr{}" aria-label="select table attribute">
              <select class="selectpicker table" data-title="table"  data-live-search="true"
                 onchange="whereClauseSetAttributes(this)">
              </select>
              <select class="selectpicker attr" data-title="field" data-live-search="true"
                 onchange="whereClauseAttributeChange(this)">
              </select>
            </div>
            <!-- Value operand -->
            <div class="tab-pane whereVal" role="tabpanel" id="lhsVal{}" aria-label="select value">
              <input class="form-control valueToCompare" type="text" />
            </div>
          </div>
        </div>

        <div class="col-">
          <!-- operator -->
          <select class="select operator" id="clauseOp{}" data-title="op">
            <option class="selected" data-fix="infix">=</option>
            <option data-fix="infix">&gt;</option>
            <option data-fix="infix">&lt;</option>
            <option data-fix="infix">&ge;</option>
            <option data-fix="infix">&le;</option>
            <option data-fix="infix">&ne;</option>
            <option data-fix="prefix">contains</option>
          </select>
        </div>

        <div class="WhereRHS col-md">
          <!-- right operand -->
          <nav>
            <div class="nav-tabs nav" role="tablist">
              <button class="nav-link tblAttrTab" data-toggle="tab" data-target="#rhsTblAttr{}" type="button" role="tab" aria-controls="field-control" aria-selected="true">
                Field
              </button>
              <button class="nav-link valueTab active" data-toggle="tab" data-target="#rhsVal{}" type="button" role="tab" aria-controls="value-control" aria-selected="false">
                Value
              </button>
            </div>
          </nav>
          <!-- Operand to a where clause -->
          <div class="tab-content">
            <!-- Table/Field operand -->
            <div class="tab-pane whereTblAttr" id="rhsTblAttr{}" role="tabpanel" aria-label="select table attribute">
              <select class="selectpicker table" data-title="table" data-live-search="true"
                 onchange="whereClauseSetAttributes(this)">
              </select>
              <select class="selectpicker attr" data-title="field" data-live-search="true"
                 onchange="whereClauseAttributeChange(this)">
              </select>
            </div>
            <!-- Value operand -->
            <div class="tab-pane show active whereVal" id="rhsVal{}" role="tabpanel" aria-label="select value">
              <input class="form-control valueToCompare" type="text" />
            </div>
          </div>
        </div>
        
        <div class="col-">
          <a class="whereClauseDelete" onclick='this.closest(".comparison").remove()' aria-label="delete clause">
             <i class="fa fa-fw fa-trash mr-2"></i>
          </a>
        </div>
      </div>
`

/**
 * Initialize the where clause dialog
 * @param event - GUI event, currentTarget points to the modal
 */
function whereClauseManager(event) {
    let mgrRoot = $(event.currentTarget)
    // We will need to distinguish clause ids, set up a counter
    mgrRoot.data("counter", new Counter());

    let comparisons = mgrRoot.find(".comparisons")
    comparisons.find(".row").remove();  // Remove existing dialog comparisons

    // Access the node that is related to this
    let node_id = $(event.relatedTarget).data("node-id")
    let node = getNodeFromD3(node_id)
    mgrRoot.find(".TethysElem").html(node.name)  // Show node name in dialog

    // Store the node so that it may be accessed by other callbacks
    mgrRoot.data("node", node)

    if (node.whereClause != null) {
        whereClausePopulateExisting(node, mgrRoot)
    } else {
        whereClauseAdd(mgrRoot)  // No current constraints, add an empty row
    }
}

/**
 * Add a new where clause to the where clause manager.
 * @param mgrRoot - JQuery handle to root of clause dialog manager ("#whereClauseManager")
 */
function whereClauseAdd(mgrRoot) {
    let node = mgrRoot.data("node")  // Access Tethys element node representation
    // Generate html with unique ids
    let newid = mgrRoot.data("counter").next();
    let whereN = whereComponent.replace(/\{\}/g, newid.toString())

    // add it to the list of comparisons, then retrieve JQuery wrapped DOM for new component
    let comparisons = mgrRoot.find(".comparisons")
    comparisons.append(whereN)
    let comp = comparisons.find("#whereClause"+newid.toString())

    // populate tables, attributes will be populated as selected
    let tables = comp.find("select.table")
    whereClauseSetTables(tables.eq(0), node.tableName)
    whereClauseSetTables(tables.eq(1), node.tableName)

    // selectpicker needs to be initialized when created programmatically
    comp.find("select.selectpicker").selectpicker()

    return comp
}

/**
 * Populate comparisons from an existing whereClause associated with a node.
 * @param node - Tethys node, used to retrieve information
 *    about how to populate the where clauses
 * @param mgrRoot - JQuery wrapped #WhereClauseManager, the root of the dialog
 */
function whereClausePopulateExisting(node, mgrRoot) {
  let comparisons = mgrRoot.find(".comparisons");
  let clauses = node.whereClause;
  if (clauses != null) {
      for (let c=0; c < clauses.length; c++) {
          // Create and populate each clause.
          // This will also add the table lists, attributes will have to be populated separately
          comp = whereClauseAdd(mgrRoot);

          let clause = clauses[c]  // process current clause

          // Set left operand, operator, right operand
          let lhs = comp.find(".WhereLHS");
          whereClausePopulateExistingSide(clause.lhs, lhs)

          let op = comp.find(".operator");
          op.val(clause.op);

          let rhs = comp.find(".WhereRHS");
          whereClausePopulateExistingSide(clause.rhs, rhs);
      }
  }
}

function whereClausePopulateExistingSide(clause_side, comparison_side) {
    if (clause_side.operandType == "literal") {
        // User provided a value
        let literal = comparison_side.find(".valueToCompare");
        literal.val(clause_side.val);
        literal.attr("Type", clause_side.type);
    } else if (clause_side.operandType == "attribute") {
        // Use specified table and attribute
        // Retrieve the table select and set appropriate selected value.
        let tbl = comparison_side.find('.table')
        tbl.find(".selectpicker").selectpicker('val', clause_side.table)  // Set active table

        // Retrieve he attribute select.  Populate and set selected value
        let attr = comparison_side.find('.attr')
        whereClauseSetAttributes(tbl);
        // Set properties as selected
        attr.find(".selectpicker").selectpicker('val', clause_side.attr)
    }
}
/**
 * Save the data in the current set of conditions to the node representing
 * the Tethys element associated with these where clauses
 * @param obj - Dummy argument
 */
function whereClauseCommit(obj) {
    let mgr = $("#whereClauseManager");
    let comparisons = mgr.find(".comparison");
    let clauses = [];
    let tables = new Set()  // Track tables used in clause
    for (let i=0; i < comparisons.length; i++) {
        // Retrieve data associated with each clause
        let clause = {};
        clause.lhs = whereGet(comparisons.eq(i).find(".WhereLHS"));
        clause.op = comparisons.eq(i).find(".operator").find("option:selected").val();
        clause.rhs = whereGet(comparisons.eq(i).find(".WhereRHS"));

        // Verify this clause has been populated before adding it to the clause list
        let populated = clause.lhs != null && clause.op != null && clause.rhs != null
        if (populated) {
            // Keep track of tables used in this clause, needed for determining where to place SQL queries
            if (clause.lhs.table != null)
                tables.add(clause.lhs.table)
            if (clause.rhs.table != null)
                tables.add(clause.rhs.table)
            // Add to clause list
            clauses.push(clause)
        }
    }

    // Update the node with the new information
    let node = mgr.data("node");
    node.whereClause = clauses
    node.whereTables = Array.from(tables)
    // update the field mapping to ensure correct filtered node indicator
    let label = buildFieldMappingText(node)
    $("#mappingTableBody").find("#"+node.id).find(".mappingText").text(label)
}

/**
 * Extract information about an operand for a where clause
 * @param operand - JQuery instance at the root of the left- or right- hand side
 * @return {{}}  Dictionary with keys:
 *   operandType - attribute or literal
 *
 *   if attribute:
 *   attr - source:table:attribute
 *   table - source:table
 *
 *   if literal:
 *   val - String in input box
 *   type - HTML input Type attribute used to constrain input
 */
function whereGet(operand) {
    let pane = operand.find(".tab-pane.active") // Retrieve active pane
    // Use the class to figure out whether this is a value or table/attr specification
    let classes = pane.attr("class");
    let tblattr = classes.match(/TblAttr/i) != null
    let opdict = {}
    // Information about operand
    if (tblattr) {
        // User specified table/attribute
        // We only need the attribute as it is a complete path that specifies
        // the source, table, and attribute
        opdict.attr = pane.find(".attr").find("option:selected").val();
        if (opdict.attr == null || opdict.attr == "") {
            opdict = null;  // attribute not populated
        } else {
            opdict.operandType = "attribute"
            opdict.table = tablePaths.combine(...tablePaths.get_src_tbl(opdict.attr))
        }
    } else {
        // User specified a literal (constant value)
        opdict.operandType = "literal";
        let jval = pane.find("input");
        let val = jval.val();  // Current value of input box
        if (val == null || val == "") {
            opdict = null;  // no value
        } else {
            opdict.type = jval.attr("Type");  // What type is associated with this entry?
            opdict.val = val // Text string in input box
        }
    }
    return opdict
}
/**
 * Class for generating indices
 */
class Counter {
    /**
     * Create a new counter that returns a succesive integer each time next is called
     * @param start - first index to be returned, defaults to 0
     */
    constructor(start=0) {
        this.count = start-1;  // We preincrement, so we decrement here
    }

    /**
     * Get next index
     * @return {number}
     */
    next() {
        this.count++;
        return this.count;
    }
}

/**
 * Populate the where clause table dropdowns.
 * Tables that are involved in the current node are listed first followed
 * by any other tables that are related to sources associated with the
 * node.
 * @param tbl_menus
 * @param active_tables
 */
function whereClauseSetTables(tbl_menus, active_tables) {
    // Build list of tables with the ones being used first

    // Add in active tables
    active_tables.forEach(tbl => {
        tbl_menus.append(`<option>${tbl}</option>`)
        })

    // Add in any other tables associated with the sources
    let node_sources;
    let node_tables = new Set(active_tables)
    if (active_tables.length == 0) {
        // No active tables were passed in, use all active sources
        node_sources = Array.from(Object.keys(window.sourceData));
    } else {
        // Only use sources from active tables
        node_sources = new Set(active_tables.map(el => tablePaths.get_src(el)));
    }

    tbl_menus.append('<option data-divider="true"></option>')  // div line
    let scounter = 0;
    node_sources.forEach(source => {
        // Get table names qualified with the source
        let tables = window.sourceData[source].tablesPathed
        // Add in those tables that we have not already listed
        for (let i = 0; i < tables.length; i++) {
            if (!node_tables.has(tables[i]))
                tbl_menus.append(`<option value="${tables[i]}">${tables[i]}</option>`)
        }
        // Add divider between each set of tables grouped by source
        let moreSources = scounter < node_sources.size - 1;
        if (moreSources) {
            $(tbl_menus).append('<option divider="true"></option>');
            scounter++;
        }
    })

    tbl_menus.selectpicker();  // init selectpicker w/ options
}

/**
 * Populate attributes for current table
 * @param tbl - table element that changed
 */
function whereClauseSetAttributes(tbl) {
    // attributes selector is a child of one of the siblings of the parent
    let jtbl = $(tbl);
    let names = tablePaths.get_names(jtbl.find("option:selected").val());


    // Retrieve attribute associated with table
    let attrPathed = window.sourceData[names.source].tables[names.table].attributesPathed
    let attr = window.sourceData[names.source].tables[names.table].attributes

    // Retrieve the picker, remove existing options and repopulate
    let jattr = jtbl.parent().siblings().find(".attr");
    jattr.find('option').remove();
    for (let i=0; i < attrPathed.length; i++) {
        jattr.append(`<option value="${attrPathed[i]}">${attr[i]}</option>`)
    }

    jattr.selectpicker('refresh');  // Needed after changing options
}

/**
 * When an attribute is selected on one side of the operation, we change the value
 * type associated with the other side to match the type of the attribute
 * @param attr
 */
function whereClauseAttributeChange(attr) {
    let jattr = $(attr);
    let attr_str = jattr.val();

    // Determine attribute type
    let parts = tablePaths.get_names(attr_str);
    let type = window.sourceData[parts.source].tables[parts.table].attributeTypes[parts.attr];
    // Map ODBC type to HTML type
    typemap = {
        "datetime": "datetime-local",
        "date": "date",
        "time": "time",
        "int": "number",
        "float": "number",
        "str": "text"
    }
    let htmltype = "text"
    if (type in typemap)
        htmltype = typemap[type]

    // Find value on other side
    let root = jattr.closest(".comparison");  // Find which comparison we are in
    let onLHS = jattr.closest(".WhereLHS").length > 0
    let other = null;
    if (onLHS) {
        other = root.find(".WhereRHS").find(".valueToCompare");
    } else {
        other = root.find(".WhereLHS").find(".valueToCompare");
    }
    other.attr("Type", htmltype)

    console.log('attribute selected')
}


// ------------------------ conditionManager functionality --------------------
 /**
  * conditionManager - Manage conditional inclusion of source map directives.
  * May be called from one of two scenarios.  In either scenario,
  * the event.currentTarget points to the root of the manager dialog.
  *
  * 1.  When a Tethys element is dropped onto a condition target, drag and drop routines
  *     will set data-node of the condition manager to contain the node.
  * 2.  When the condition menu is invoked on an existing binding, event.relatedTarget contains
  *     data-node-id, the name of the node.
  * @param event - event trigger
  */
 function conditionManager(event) {
    let mgrRoot = $(event.currentTarget)

    // This dialog is very similar in structure to the whereClauseManager
    // but it does not support multiple comparisions and has different operations

    let comparisons = mgrRoot.find(".comparisons")
    comparisons.find(".comparison").remove();  // Remove existing comparisons

    // Access the node that is related to this
    let node = mgrRoot.data("node");
    if (node == null) {
        // context menu node name populated in no
        node = getNodeFromD3($(event.relatedTarget).data("node-id"))
    }

    mgrRoot.find(".TethysElem").html(node.name)  // Show node name in dialog

    // Store the node so that it may be accessed by other callbacks
    mgrRoot.data("node", node)

    conditionClauseAdd(mgrRoot)  // No current constraints, add an empty row

}

/**
 * Add a condition clause to the conditionManager
 * @param mgrRoot - JQuery handle to root of clause dialog manager
 */
function conditionClauseAdd(mgrRoot) {
    let node = mgrRoot.data("node")  // Access Tethys element node representation
    // Customize the id.  We assume there will only be one clause, see whereClauseManager
    // for an example of how to manage multiple clauses.
    let newid = "Cond";
    let whereN = whereComponent.replace(/\{\}/g, newid)

    // add it to the list of comparisons, then retrieve JQuery wrapped DOM for new component
    let comparisons = mgrRoot.find(".comparisons")
    comparisons.append(whereN)
    // Ids are all named whereClause as we are repurposing these
    let comp = comparisons.find("#whereClause"+newid)

    // Find tables used by this node or its children
    // populate tables, attributes will be populated as selected
    let tables = comp.find("select.table")

    let activeTableSet = new Set();
    findReferencedTables(node, activeTableSet);
    let active_tables = Array.from(activeTableSet)
    whereClauseSetTables(tables.eq(0), active_tables)

    // Update operators
    let op = comp.find(".operator")
    op.find("option").remove()
    op.append('<option value="not_empty" selected>has a value</option>')

    // As we reuse the functionality of the where clauses, some portions of the interface
    // need to be modified.

    // not_empty is univariate, hide the RHS.  Do this in CSS so that they layout does not change.
    // Sett css to visbile to restore
    comp.find(".WhereRHS").css("visibility", "hidden")

    // selectpicker needs to be initialized when created programmatically
    comp.find("select.selectpicker").selectpicker()

    let lhs = comp.find(".WhereLHS");
    if (node.conditionClause != null) {
        // populate the operator and table
        op.selectpicker("val", node.conditionClause[0].op)
        lhs.find(".table.selectpicker").selectpicker("val", node.conditionClause[0].lhs.table);
        // populate the attributes associated with the selected table (note we reuse the
        // whereClauseSetAttributes logic as it is essentially the same.
        whereClauseSetAttributes(lhs.find(".table.selectpicker"))
        // Select the correct attribute.
        lhs.find(".attr.selectpicker").selectpicker("val", node.conditionClause[0].lhs.attr);
    } else {
        // user should not be able to select a value until they pick a table
        lhs.find(".valueTab").prop("hidden", true)
    }
    return
}

/**
 * Build a set of all of the tables used by a node and its children
 * @param node  Root node of recursive search
 * @param tblSet  Set which will be mutated to add in any new tables that are discovered
 */
function findReferencedTables(node, tblSet) {
    // Note all tables used by this node
    if (node.tableName != null) {
        for (let idx = 0; idx < node.tableName.length; idx++)
            tblSet.add(node.tableName[idx])
    }
    // Node all tables used by children of this node
    // Note that we do not explore tables used in conditions
    if (node.children != null) {
        for (let idx = 0; idx < node.children.length; idx++)
            findReferencedTables(node.children[idx], tblSet)
    }
}

function conditionCommit(obj) {
    let mgr = $(obj.dataset.target)
    // Conditions currently only support one comparison, so we should
    // not expect to find more than one
    let comparison_list = mgr.find(".comparison");

    let node = mgr.data("node");
    if (comparison_list.length > 0) {
        let comparison = comparison_list.eq(0);

        let clause = {};
        clause.lhs = whereGet(comparison.find(".WhereLHS"));
        if (clause.lhs == null) {
            delete node.conditionClause;  // no selection
        } else {
            clause.op = comparison.find(".operator").find("option:selected").val();
            node.conditionClause = [clause];
        }
        nodeDrop(node);  // Add to field list and update list of active dropped nodes
    } else {
        // User has deleted the clause and is committing.  Removes clause
        delete node.conditionClause;
        let status = nodeInformation(node);
        if (! status.display) {
            // Condition removed/canceled, see if it can be removed from drop list
            removeDroppedNode(node.id)
        }
    }
    let label = buildFieldMappingText(node);  // Update field mapping list
    updateFieldMap(label, [node])

}
// -------------------------  -------------------------
/**
 * Generate the XML source map
 * Creates XML structure along with embedded queries for processing data as specified
 * in the current schema map bindings.
 * @returns {null}
 */
function createMap() {
    // Set up message for error (will override class if there is not a problem)
    let pubMsg = $("#publicationMsg")
    pubMsg.addClass("alert-danger").removeClass("alert-info");
    let pubMsgText = pubMsg.find(".publicationMsgText")

    // Look for required fields that are not yet mapped.
    let mappingList = document.getElementsByClassName('mappingText')
    if (mappingList.length) {
        let errors = []
        for (let i = 0; i < mappingList.length; i++) {
            // Every entry on the list must have a value or a condition
            let node = getNodeFromD3($(mappingList[i]).parent().attr('id'))
            if (node.field == null && node.defaultValue == null && node.conditionClause == null) {
                errors.push(mappingList[i].textContent)
            }
        }
        // populate error alert with required fields to fill out
        if (errors.length > 0) {
            pubMsgText.text('These required fields have not yet been assigned: ' + errors.join(', '));
            pubMsg.show();
            return;
        }
    }

    // If sources have not been loaded, we won't be able to see any join constraints
    let addsourceElement = document.getElementsByClassName('addSource')
    if (addsourceElement.length) {
        let errors = []
        for (let i = 0; i < addsourceElement.length; i++) {
            errors.push($(addsourceElement[i]).data('source'))
        }
        // populate error alert with required sources to fill out
        if (errors.length > 0) {
            pubMsgText.text('All data sources must be populated: ' + errors.join(', ') +
                '. This allow us to potentially infer relationships between tables and data types');
            pubMsg.show();
            return;
        }
    }

    $('#createMapError').hide()

    // Annotate the tree with information for SQL queries
    let root = window.currentD3
    clear_sqltables(root)  // Remove any information related to SQL query, we will compute these

    let annotationInfo = annotateSqlNodes(root, dropPaths)
    if (annotationInfo.error != null) {
        pubMsgText.text('Uploading Source Map Error: ' + annotationInfo.error)
        pubMsg.show();
        return
    }

    // Process information related to joins
    if (annotationInfo.multitable.size > 0) {
        let multitable_nodes = Array.from(annotationInfo.multitable)
        annotationInfo.linkages = [];
        let first_unresolved_idx = null;

        // Attempt to resolve join constraints based on what we know
        for (let idx=0; idx < multitable_nodes.length; idx++) {
            // Create a linkage table for the node that will require a merge
            let linkages = new TableLinkages(Array.from(multitable_nodes[idx].sqlTables))
            // Verify query will only query tables from one source
            // Other sources can be referenced, e.g. in a where clause, but they cannot be selected
            let sources = Array.from(multitable_nodes[idx].sqlTables.values()).map(
                    p => tablePaths.get_head_tail(p)[0])
            sources = [... new Set(sources)]
            if (sources.length > 1) {
                alert(`Tethys field ${multitable_nodes[idx].name} queries more than one data source: ${sources}.  Only one data source may be queried at a time`)
            }
            // Attempt to add known foreign key constraints that we know about from the ODBC client
            // We only expect to see these with real databases.  These constraints will be ignored if the tables
            // involved were not used in the query.
            let constraints = window.sourceData[sources[0]].constraints
            for (let cidx = 0; cidx < constraints.length; cidx++) {
                // Only pays attention to constraints related to tables in the selection
                let added = linkages.add_constraint(...constraints[cidx])
                console.log(` ${multitable_nodes[idx].id}: constraint ${constraints[cidx]} - ${added ? "added" : "ignored"}`)
            }
            if (! linkages.resolved()) {
                first_unresolved_idx = idx;
            }
            annotationInfo.linkages.push(linkages)
        }
        if (first_unresolved_idx != null) {
            // Provide list of Tethys elements where joins will be created and what we know about them
            let jqJoinModal = $("#JoinModal");
            jqJoinModal.data("joinNodes", multitable_nodes);
            jqJoinModal.data("linkages", annotationInfo.linkages);
            jqJoinModal.data("linkage_idx", first_unresolved_idx);  // Which element to process first
            // Make the modal visible, this will also trigger initialization code that will
            // populate the modal to process the Tethys elements
            $("#JoinModal").modal('show');
        }
    } else {
        createMap2()
    }
}

/**
 * Retrieve the LinkageTable associated with a specific node as specified by it's id
  * @param node_id - string providing a unique representation of the Tethys field
 * @returns {null} - LinkageTable containing information about fields that can be used in a where clause
 */
function getLinkages(node_id) {

    let jqJoinModal = $("#JoinModal");
    let linkages = jqJoinModal.data("linkages")
    let nodes = jqJoinModal.data("joinNodes")

    // Linkages and nodes are parallel arrays, find node_id in nodes.
    let linkage = null  // default if we don't find the node
    if (nodes != null) {
        let idx = 0; // find the linkage table associated with node_id
        while (idx < nodes.length && nodes[idx].id != node_id)
            idx++;
        if (idx < nodes.length)
            linkage = linkages[idx]
    }
    return linkage
}

function createMap2() {

    // Construct the document object model (DOM) of the XML document
    let root = window.currentD3
    let mapName = $('#configName').val()
    let sources = window.allSourceNames
    let xml = buildSourceMap(root, mapName, sources)

    if (xml == "")
        return;  // Unable to generate XML, an error message will have been generated.

    // todo:  Remove dependence on globals...
    labelIndex = 0
    labelFieldDict = {}

    let overwrite = $('#overwriteSourceMap').is(':checked')

    // Import specification used in the POST for adding the source map
    let specification = `<?xml version="1.0" encoding="utf-8"?>
        <import>
          <overwrite>${overwrite}</overwrite>
          <sources>
            <source>
              <type>file</type>
              <file>${$('#configName').val() + '.xml'}</file>
            </source>
          </sources>
        </import>`

    const blob = new Blob([xml], { type : 'application/xml;charset=utf-8' } );
    let xmlFile = new File([blob], $('#configName').val() + '.xml', { type : 'application/xml;charset=utf-8' })
    console.log(xmlFile)

    let form = new FormData()
    form.append("specification", specification)
    let configName = $("#configName").val();
    form.append(configName + '.xml',xmlFile)

    let urlValue = window.location.origin+"/SourceMaps/import";
    let ajaxCall = new AjaxWrapper("post",form, null, this.ajaxError);   //perform a GET to get XMLJson this.ajaxError
    ajaxCall.setURL(urlValue);

    let pubMsg = $("#publicationMsg");
    let pubMsgText = pubMsg.find(".publicationMsgText");
    $.ajax(ajaxCall.getSettings()).done((response) => {
        pubMsg.removeClass("alert-danger").addClass("alert-info");
        pubMsgText.text(`Source/Import map ${configName} published`);
        pubMsg.show();
    }).fail(function(data) {
        let description = data['responseText']
        if (description.startsWith("<?xml version")) {
            // Escape xml text & mark preformatted
            let escaped = pubMsgText.text(description).html();
            description = "\n<pre>\n" + escaped + "\n</pre>";
        }
        pubMsgText.html("Publication error:  " + description)
        pubMsg.show();
    });
}


/**
 * showDecoratedTree
 * Conduct a preorder visit of a decorated node tree
 * For each node, we show the node id and the values of the properties (if present)
 * @param node - node of interest
 * @param propList - List of properties.  Each one is displayed and shown if present in the table
 * @param propPresentOnly - If true, only prints nodes that have one of the specified properties
 */
function showDecoratedTree(node, propList, propPresentOnly=true) {
    console.log("Decorated tree -------------------")
    showDecoratedTree_helper(node, propList, propPresentOnly, propPresentOnly)
    console.log("Decorated tree (end) ------------")
}

/**
 * showDecoratedTree_helper - Recursive helper function for showDecoratedTree
 * @param node - node of interest
 * @param propList - List of properties.  Each one is displayed and shown if present in the table
 * @param propPresentOnly - If true, only prints nodes that have one of the specified properties
 */
function showDecoratedTree_helper(node, propList, propPresentOnly) {

    // Find property values from propList and output node
    let props = []
    propList.forEach(p => {
        if (node[p] != null) {
            values = node[p]  // Array or Set
            if (values instanceof Set)
                values = Array.from(values) // Set->Array
            else if (typeof values == 'string' || values instanceof String)
                values = [values]  // singleton array
            props.push(p + ":" + values.join(", "))
        }
    })

    if (propPresentOnly == false || props.length > 0)
        console.log(node.id + "\n  " + props.join("\n  "))

    // Recurse on children
    let children = getChildren(node)
    for (let c_idx=0; c_idx < children.length; c_idx++) {
        showDecoratedTree_helper(children[c_idx], propList)
    }
}

/**
 * clear_sqltables - Recursively remove all properties associated with
 * SQL SELECT queries for the specified node and its descendants
 * @param node
 */
function clear_sqltables(node) {

    /* Remove all properties associated with a SELECT SQL query:
     * sqlTables - list of tables in query
     * sqlWhere - list of where clauses
     * selectedFields - the attributes of the tables
     * aliasFields - fields that are aliased with an AS
     */
    delete node.sqlTables
    delete node.sqlWhere
    delete node.sqlAttr
    delete node.sqlAttrAs
    // Recursively clear rest of tree
    let children = getChildren(node)
    for (let idx=0; idx < children.length; idx++) {
        clear_sqltables(children[idx])
    }
}

/**
 *
 * @param node
 * @param docname
 * @param sources - List of pairs, [source name, type hint]
 *    source name - Name used to refer to a source such as a database, a workbook, a CSV file...
 *    type hint - Hint about the type of source
 */
function buildSourceMap(node, docname, sources) {
    // Create the XML document and get the root element
    let doc = xmlbuilder2.create("Mapping")

    // Start building the document
    let root = doc.root()
    root.ele("Name").txt(docname)  // SourceMap name

    // Document attributes
    let doc_attr = root.ele("DocumentAttributes")
    names = ["xmlns", "xmlns:xsi", "xsi:schemaLocation"]
    values = [
        "http://tethys.sdsu.edu/schema/1.0",
        "http://www.w3.org/2001/XMLSchema-instance",
        "http://tethys.sdsu.edu/schema/1.0 tethys.xsd"
        ]
    for (let idx=0; idx < names.length; idx++) {
        let an_attr = doc_attr.ele("Attribute")
        an_attr.ele("Name").txt(names[idx])
        an_attr.ele("Value").txt(values[idx])
    }

    // Add the list of sources
    let docSources = root.ele("DocumentSources")
    sources.forEach(s => {
        let src = docSources.ele("Source")
        src.ele("Name").txt(s.sourceName)
        src.ele("OpenDatabaseConnectivity").txt(s.driverName)
    })

    // Start the list of translation map directives.
    // We will append to ths depending upon the bindings the user has created between
    // their data and Tethys data
    let directives = root.ele("Directives")
    let result = createDirectives( directives, node)

    let xml;
    if (result) {
        xml = doc.end({format: 'xml', prettyPrint: true})
        console.log("Produced map:")
        console.log(xml)
    } else
        xml = "";
    return xml
}

/**
 * Retrieve a list of child nodes
 * @param node   Target node
 * @returns {*}  List of children
 */
function getChildren(node) {
    // All children in one of two lists depending on their display state
    let children = node.children || node._children
    if (children == null)
        children = []  // empty list to avoid special case
    return children
}

/**
 * The <Directives> element contains directions on how to translate the user's source data to Tethys XML
 * @param directives  XMLBuilder element to which we add children
 * @param node  Top-level node (root of document that will be generated when data are ingested)
 * @returns {boolean}
 */
function createDirectives(directives, node) {

    // Check if user wants to create a single document or multiple ones as this
    // affects how we nest the Table directives.
    let multipleDocs = $("#DataSourcesGenerate input:radio:checked").get(0).id == 'multidoc'

    let docRoot = null // Parent for children of Directives and Table
    if (! multipleDocs) {
        // Single document:  Add data under root element
        // for most schemata, the query at the root node only produces a single row
        docRoot = directives.ele(node.name)
        docRoot = tableQuery(node, docRoot)
    } else {
        if (node.sqlTables == null) {
            $("#multipleDocumentsInvalid").show()
            return false;
        }
        // Generate multiple documents
        // The Table statement is placed above the root element
        // Rows of the query produce one document each
        docRoot = tableQuery(node, directives)
        docRoot = docRoot.ele(node.name)
    }
    // process children of the root node
    // This may generate additional queries
    childDirectives(node, docRoot)
    return true;
}

/**
 *
 * @param node - current schema tree node, we will process its children
 * @param xml  - any processed children are added to this node of the xml tree
 */
function childDirectives(node, xml) {
    let children = getChildren(node)

    for (let child_idx=0; child_idx < children.length; child_idx++) {
        let c = children[child_idx]
        if (c.conditionClause != null) {
            let clauses = c.conditionClause;
            // Currently only support a single clause
            let clause = c.conditionClause[0]
            if (clause.op == "not_empty") {
                let attr = tablePaths.get_tbl_attr(clause.lhs.attr, '.', '`');
                // Embed element c inside the condition
                xml = xml.ele("Condition");
                let predicate = xml.ele("Predicate");
                predicate.ele("Operand").txt(`[${attr}]`);
                predicate.ele("Operation",
                    att={"op": "empty", "retain": "iffalse"})
                process_child(c, xml);
            } else {
                process_child(c, xml_element)
            }
        } else
            process_child(c, xml)
}

function process_child(c, xml_current) {
    // inject table query if needed.  xml === xml_current if no child needed
    xml_current = tableQuery(c, xml)
    let xml_child = null
    if (bindingPresent(c) && c.attribute == null) {
        // populate the entry
        let entry = createMapping("Entry",xml_current, c)

        // Check to see if there are any children that are attributes as indicated
        // by the presence of the attribute decorator on the node.  These must be added
        // as children of the current entry
        // <Entry>
        //    ...
        //    <Attribute> ...
        let c_children = getChildren(c)
        for (let idx=0; idx < c_children.length; idx++) {
            subnode = c_children[idx]
            if (bindingPresent(subnode) && subnode.attribute != null) {
                createMapping("Attribute",entry, subnode)
            }
        }
    } else if (subtreeBindingPresent(c)) {
        xml_child = xml_current.ele(c.name)  // Need to generate this node
    }

    if (xml_child != null) {
        // We created a child element, process anything else that is needed
        childDirectives(c, xml_child)
    }
}

}

/**_
 * nodeNeeded - Determine whether a node is needed in the SourceMap
 * Nodes are only populated if they have children that are bound to a user data field or default
 * or if they must occur at least once and do not have any children.
 * @param node
 * @returns {boolean}
 */
function nodeNeeded(node) {
    let children = getChildren(node)
    let needed = (children.length == 0 && node.minOccurs == "1")
    if (! needed) {
        // check if a child is filled in
        for (let i=0; i < children.length; i++) {
            needed = subtreeBindingPresent(c)
            if (needed)
                break  // no need to look further
        }
    }
    return needed
}

/**
 * bindingPresent - Check to see if a node has been bound to a value
 * This will either be related to an external data source field or a default value
 * @param node
 * @returns {boolean}
 */
function bindingPresent(node) {
    // Have any fields or default values been associated with this node?
    return (node.field != null && node.field.length > 0) ||
        (node.defaultValue != null)
}

/**
 * getSource - Return the source field information for a node.  Returns null if no source present
 * When bindings to a source field are present, one of the following will have occurred
 * 1.  A calculation has been associated with an alias, return the alias name
 * 2.  Multiple source fields have been combined, return a string that the SourceMap interpreter
 *     will be able to use to combine the fields.
 * 3.  a single source field has been specified, return the source field
 * @param node
 * @returns {*}
 */
function getSource(node) {
    let value = null
    if (node.customAlias != null) {
        value = '[' + node.customAlias + ']' // Should only be present for calculations on multiple fields
    } else if (node.customField != null) {
        // custom field contains pre [field] post, possibly repeated multiple times
        value = prePostBackquote(node.customField)
    } else if (node.field != null) {
        let s = tablePaths.get_tbl_attr(node.field[0], '.', '`')
        value = '[' + s + ']'
    }
    return value
}


/**
 * createMapping - An <Entry> or <Attribute> node has been created,
 * add appropriate children to specify Source, Kind, Dest, and
 * Default
 * @param nodename - "Entry" or "Attribute"
 * @param xml - xmlbuilder node parent of new node
 * @param node - node containing entry information
 */
function createMapping(nodename, xml, node) {
    // populate Source, Kind, Dest, Default as appropriate
    if (node.anyParameter == "1") {
        // Special case: #any
        let outputs = node.field;
        if (outputs.length > 0) {
            let custom = xml.ele(node.name);
            for (let idx = 0; idx < outputs.length; idx++) {
                let field = outputs[idx];
                let entry = custom.ele(nodename);
                entry.ele("Source").txt(tablePaths.get_tbl_attr(field, '.', '`'))
                // Map the user field to a valid XML element
                let destname = toValidXml(tablePaths.get_attr(field));
                entry.ele("Dest").txt(destname);
            }
        }
    } else {
        let entry = xml.ele(nodename);
        let source = getSource(node)
        if (source != null) {
            entry.ele("Source").txt(source)
        }
        if (node.type != null)
            entry.ele("Kind").txt(node.type)
        entry.ele("Dest").txt(node.name)
        if (node.defaultValue != null)
            entry.ele("Default").txt(node.defaultValue)
    }
}

/**
 * subtreeBindingPresent - See if node or any of its children have been bound to a table attribute
 * or default value.
 * @param node  Root of search
 * @returns {boolean}  True - bindings have been made, otherwise False
 */
function subtreeBindingPresent(node) {
    if (node == null)
        return false
    else {
        // bindings are present if fields have been selected or a default has been provided
        let present = bindingPresent(node)
        if (! present) {
            // Check children for bindings
            let children = getChildren(node)
            for (let i=0; i < children.length; i++) {
                present = subtreeBindingPresent(children[i])
                if (present)
                    break  // no need to look further
            }
        }
        return present
    }
}

/**
 * Add a SQL query to the sourcemap if needed
 * @param node - This node is examined to see if it has a table associated with it.
 *    If so, a query is formed.
 * @param xml - The XML node that will be the parent of the TABLE query node
 * @returns {*}  The XML node if there is no query, otherwise the new SQL query XML node.
 */
function tableQuery(node, xml) {
    let tbl = xml  // Return xml when no change has been made
    if (node.sqlTables != null && node.sqlTables.size > 0) {

        let select = []
        select.push("SELECT")

        // set of attributes we will need to query.  These are stored in source:table:attr paths
        let attr_set = new Set(node.sqlAttr)
        // List of attributes that have been transformed to SQL syntax.  We usually backtick tables
        // and attributes to prevent issues with certain characters in table names, e.g. $, .
        let attr_sql = []

        // Translate the projected attributes to SQL syntax
        if (node.sqlAttr != null) {
            // Remove source references and add backquotes
            let sel = [...node.sqlAttr].map(
                attr => tablePaths.get_tbl_attr(attr, '.', '`'))
            attr_sql.push(...sel)
        }
        // Add in projections that are renamed using AS syntax
        if (node.sqlAttrAs != null) {
            /*
             * each aliasField is of the format:
             *  prefix-text field postfix-text AS varname
             * The fields need to be modified, removing the source name and back quoting
             * the field names.  We use a regular expression to pull these out and
             * reconstruct the field name with the appropriate modifications
             */
            aliases = Array.from(node.sqlAttrAs)
            for (let idx=0; idx < aliases.length; idx++) {
                calcQ = prePostBackquote(aliases[idx], bracketRm=true)
                attr_sql.push(...calcQ)
            }
        }

        // We process the constraint where clause before proceeding as it may introduce additional
        // attributes and tables that will need to included in the attribute projection and table
        // selection.
        let constraintClause = []
        if (node.sqlWhere != null) {
            let ops2SQL = {
                "=": "=",
                ">": ">",
                "≥":">=",
                "≤": "<=",
                "≠": "!=",
                "contains": "contains"
            }
            let constraints = [];
            for (let c=0; c < node.sqlWhere.length; c++) {
                let lhs = getOperand2SQL(node.sqlWhere[c].lhs)
                if (lhs.attr != null && ! attr_set.has(lhs.attr)) {
                    // We hav enot see this attribute before, note it.
                    attr_set.add(lhs.attr);
                    attr_sql.push(tablePaths.get_tbl_attr(lhs.attr, '.', '`'));
                }
                let op = ops2SQL[node.sqlWhere[c].op]
                let rhs = getOperand2SQL(node.sqlWhere[c].rhs)
                if (rhs.attr != null && ! attr_set.has(rhs.attr)) {
                    // We hav enot see this attribute before, note it.
                    attr_set.add(rhs.attr);
                    attr_sql.push(tablePaths.get_tbl_attr(rhs.attr, '.', '`'));
                }
                if (op == "contains") {
                    // prefix operator
                    constraints.push(`${op}(${lhs.sql}, ${rhs.sql})`)
                } else {
                    // infix operator
                    constraints.push(`${lhs.sql} ${op} ${rhs.sql}`)
                }
            }

            constraintClause.push("WHERE")
            constraintClause.push(constraints.join(" AND "))
        }

        select.push(attr_sql.join(", "))
        select.push("FROM")

        // Process tables involved in this query.
        // This will be a series of inner joins.  Joins need to be made in an order
        // that each new table can be joined to one of the previous ones
        // Add FROM and WHERE clauses
        // Tables must have source reference removed and are backquoted as well
        let tables = Array.from(node.sqlTables)
        let sources = new Set(tables.map(t => tablePaths.get_src(t)))

        let linkage = getLinkages(node.id)
        if (linkage != null && linkage.hasConstraints()) {
            let ordering = linkage.getJoinOrder();  // appropriate order to join tables
            let current = ordering.tables[0]
            // Add in table name, stripping off the source name
            select.push(tablePaths.mutate(current, '.', '`', 1, 2))
            for (let t = 1; t < ordering.tables.length; t++) {
                select.push("JOIN")  // join defaults to inner join
                current = ordering.tables[t]
                select.push(tablePaths.mutate(current, '.', '`', 1, 2))

                select.push("ON")
                let clause = []
                let on = ordering.on[t - 1]  // tables used for ON clause
                let constraints = linkage.getConstraints(...on)  // constraints associated with these tables
                // add the constraints
                for (let c = 0; c < constraints.length; c++) {
                    clause.push(
                        tablePaths.get_tbl_attr(constraints[c][0], '.', '`') + " = " +
                        tablePaths.get_tbl_attr(constraints[c][1], '.', '`'))
                }
                select.push(clause.join(" AND "))
            }
        } else {
            // Simple select without inner join, probably only one table
            let src_tables = tables.map(t => tablePaths.mutate(t, '.', '`', 1, 2))
            if (src_tables.length > 1) {
                // Multiple tables here will produce a cross product join which is not what folks will want.
                console.log(`Note: Tethys element ${node.name} has a cross product join: ${src_tables}`)
            }
            select.push(src_tables.join(", "))
        }

        select.push(...constraintClause);  // Add in constraints
        let sqlQuery = select.join(" ") + ";"

        // Add the table element
        tbl = xml.ele("Table")
        tbl.att("query", sqlQuery)  // SQL query
        // Which database/document source?  Only really needed when multiple sources are used
        // but it does not hurt.  We are guaranteed that only one value is here as we cannot
        // query from multiple databases simultaneously.  Trying to do so would have generated
        // an error in annotateSqlNodes
        tbl.att("source", sources.values().next().value)
        // Label for this query (can be used to refer disambiguate nesting collisions)
        // We are not currently generating labels so this should not produce anything
        tbl.att("label", node.label)
    }
    return tbl
}

/**
 * Given an object specifying an operand, return a dictionary describing the operand
 * @param operand
 * @return dict fields
 *      sql - How this value should be represented in sql.  For tables and attributes
 *         we backquote tables and fields so that special characters can be handled.
 *      attr - Full path to attribute including source:  source:table:attr
 */
function getOperand2SQL(operand) {
    let op = {};
    switch (operand.operandType) {
        case 'literal':
            switch (operand.type) {
                case "text":
                case "str":
                case "data":
                case "datetime":
                case "time":
                    // Escape quotes inside text and enclose in double quotes
                    op.sql = '"' + operand.val.replace(/\"/g, '\\"') + '"'
                    break
                case "number":
                    op.sql = operand.val;
                    break
                default:
                    op.sql = operand.val;
                    console.log(`Unknown where literal type: ${operand.type} with value ${operand.value}`)
            }
            break

        case 'attribute':
            // Strip off source name, back tick the fields and change separator to SQL's .
            op.sql = tablePaths.get_tbl_attr(operand.attr, '.', '`')
            op.attr = operand.attr;
            break
    }

    return op
}

/**
 * prePostBackquote
 * When we have multiple entries in a custom field, e.g.:
 * [drifter:deployDetails$:Project]-[drifter:deployDetails$:DeploymentID]-[drifter:deployDetails$:Site]
 * we need to:
 *     1. remove the source (first field, drifter),
 *     2. add backquotes to the field names (e.g. protect spaces & other things SQL might not like)
 *     3. and possibly remove the square brackets (done for queries, but not when using values)
 *
 * @param s string to be parsed
 * @param bracketRm false | true - remove brackets
 * @returns {*[]} modified string
 */
function prePostBackquote(s, bracketRm=false) {
    const re = /(?<prefix>[^\[]*)\[(?<pattern>.*?)\](?<postfix>[^\[]*)/g
    let revised = []
    for (let m of s.matchAll(re)) {
        let pieces = []
        if (m.groups.prefix)
            pieces.push(m.groups.prefix)
        // split source, table:attr
        let srcRef = tablePaths.get_head_tail(m.groups.pattern)
        // table:attr -> `table`.`attr`  (SQL syntax)
        let tbl_attr = tablePaths.get_tbl_attr(srcRef[1], '.', '`');

        if (bracketRm)
            pieces.push(tbl_attr)
        else
            pieces.push('[' + tbl_attr +']')
        if (m.groups.postfix)
            pieces.push(m.groups.postfix)
        revised.push(pieces.join(""))  // reassemble and save
    }
    return revised
}


/**
 * TableLinkages
 * Class for managing relationships between tables
 * Given a set of tables, this class can be used to track relationships between foreign keys and
 * the tables to which they refer. It maintains sets of tables that can be accessed from one
 * another based on their key relationships and supports dynamic addition/removal of attributes
 * for when these are specified by users as opposed to a database administrator.
 */
class TableLinkages {
    constructor(items) {
        /**
         * Given a list of table names, initialize each table as being independent
         * of other tables and initialize an empty list of constraints that enable
         * joins between tables
         * @type {*[]}
         */
        this.items = items;  // Keep item list around
        this.sets = [];
        this.items.forEach(item => {
            this.sets.push(new Set([item]))
        })
        this.history = []  // operations on these sets
    }

    /**
     * Given a pair of table names, and a list of qualified table attributes,
     * merge the sets associated with the two tables and add eq_constraints to
     * the list of constraints
     * @param a -  table a
     * @param b - table b
     * @param eq_constraints - List of lists that can be used to construct the
     *    join criteria.
     *
     * Example:  tl.merge("a_tbl", "b_tbl", [["a_tbl.prikey1", "b_tbl.forkey1"],
     *                    ["a_tbl.prikey2", "b_tbl.forkey2"]]
     * would create a join with SELECT [...] FROM a_tbl, b_tbl WHERE
     *     a_tbl.prikey1 = b_tbl.forkey1 and a_tbl.prikey2 = b_tbl.forkey2
     *
     * Returns True if merged, False if the tables are already in the same set or
     * if a table does not exist.
     */

    merge(a, b, eq_constraints) {

        // Find the sets
        let set_a = this.identify_set(a);
        let set_b = this.identify_set(b);

        // both sets identified and not the same?
        let merge_sets = set_a != -1 && set_b != -1 && set_a != set_b;

        if (merge_sets) {
            // Verify constraints are valid
            let val_constraints = Array.isArray(eq_constraints)
            if (val_constraints) {
                for (let idx=0; idx < eq_constraints.length; idx++) {
                    val_constraints = (val_constraints && Array.isArray(eq_constraints[idx])
                        && eq_constraints[idx].length == 2);
                    if (! val_constraints)
                        break
                }
            }
            if (! val_constraints)
                alert("Internal error.  merge contains constraint elements that are not lists of length 2")

            // Store constraints and state should we need to revert
            let record = {};
            record.a = a;
            record.b = b;
            record.constraints = eq_constraints;
            this.history.push(record)
            
            // Two different sets, merge into set_a and delete set_b
            this.sets[set_a] = new Set([...this.sets[set_a], ...this.sets[set_b]]);
            this.sets.splice(set_b, 1);
        }

        return merge_sets;
    }

    /**
     * remove - Remove constraints associating two tables a and b.
     * @param a
     * @param b
     */
    remove(a, b) {

        let removed = false;

        // See if we can find a history record for this pair of tables
        let idx = self.find_table_history_record(a, b);
        if (idx != null) {
            removed = true
            // Reconstruct without the record that we don't want
            let linkages = new TableLinkages(this.items);
            for (let ridx=0; ridx < this.history.length; ridx++) {
                if (ridx != idx)
                    linkages.merge(
                        this.history[ridx].a,
                        this.history[ridx].b,
                        this.history[ridx].constraints)
            }
            // Update with new history and sets
            this.history = linkages.history;
            this.sets = linkages.sets;
        }
        return removed
    }

    /**
     * Given table.constraint constraint specifications, add the constraint
     * and update the table partitioning.  If any of constraints are related
     * to tables that are not in the set of tables associated with this
     * instance, no constraint is added.
     * @param c1  constraint specification
     * @param c2  constraint specification
     * @return {boolean}  constraint added?
     */
    add_constraint(c1, c2) {
        let c1parts = tablePaths.get_names(c1);
        let c2parts = tablePaths.get_names(c2);
        let t1 = tablePaths.combine(c1parts.source, c1parts.table)
        let t2 = tablePaths.combine(c2parts.source, c2parts.table)

        let added;

        if (this.items.indexOf(t1) == -1 || this.items.indexOf(t2) == -1) {
            added = false
        } else {
            // Attempt a merge, will return false if already in same set
            let merged = this.merge(t1, t2, [[c1, c2]])
            if (!merged) {
                // tables already in the same set, add/update history
                let hidx = this.find_table_history_record(t1, t2);
                if (hidx == null) {
                    // Need a new history record
                    this.history.push({'a': t1, 'b': t2, 'constraints': [[c1, c2]]})
                } else {
                    // Add to existing constraints
                    this.history[hidx].constraints.push([c1, c2])
                }
            }
        }
        return added
    }

    remove_constraint(c1, c2) {
        let c1parts = tablePaths.get_names(c1);
        let c2parts = tablePaths.get_names(c2);
        let t1 = tablePaths.combine(c1parts.source, c1parts.table)
        let t2 = tablePaths.combine(c2parts.source, c2parts.table)

        let hidx = this.find_table_history_record(t1, t2)
        if (hidx != null) {
            let constraints = this.history[hidx]
            // See if constraint in list
            // todo:  let filtered = constraints.filter(val => )
        } else
            console.log(`TableLinkages.remove_constraint("{c1}","{c2}") - no such constraint`)
    }

    /**
     *
     * @param t1
     * @param t2
     * @returns {number}
     */
    find_table_history_record(t1, t2) {
        let hidx = 0
        while (hidx < this.history.length &&
            ! (this.history[hidx].a == t1 && this.history[hidx].b == t2) &&
            ! (this.history[hidx].b == t1 && this.history[hidx].a == t2)
            ) {
            hidx = hidx + 1;
        }
        if (hidx >= this.history.length)
            hidx = null
        return hidx;
    }
    /**
     * identify_set - Given a table name, identify the index of the set to which it
     * currently belongs.
     * @param tbl - table name
     * @returns {number} - index of set, -1 if not found
     */
    identify_set(tbl) {
        let set_idx = -1  // Assume not found

        /* search sets list for set containing a */
        for (let idx=0; idx < this.sets.length; idx++) {
            if (this.sets[idx].has(tbl)) {
                set_idx = idx  // found it!
                break
            }
        }
        return(set_idx);
    }

    /**
     * Have all of the constraints been resolved for a join?
     * @returns {boolean}
     */
    resolved() {
        return(this.sets.length == 1);
    }

    /**
     * Return contents of smallest cardinality set
     * @returns {Set}
     */
    smallest_set() {
        let smallest = Infinity
        let sidx = 0;
        for (let idx=0; idx < this.sets.length; idx++) {
            let cardinality = this.sets[idx].size
            if (cardinality < smallest) {
                smallest = cardinality;
                sidx = idx;
            }
        }
        return this.sets[sidx];
    }

    other_tables(s) {
        // Generate a list of sets other than s
        let other_sets = this.sets.filter((item) => ! this.set_equality(s, item));

        // Transform to arrays and flatten into a single list
        let others = []
        for (let idx=0; idx < other_sets.length; idx++) {
            others.push(Array.from(other_sets[idx]))
        }
        others = others.flat()

        return others
    }

    /**
     * Check for set equality between a pair of sets
     * @param a
     * @param b
     * @returns {false|this is *[]}
     */
    set_equality(a, b) {
        // Must be same length and every item in a is also in b
        return a.length == b.length && [...a].every((item) => b.has(item))
    }

    /**
     * Show string representation of table associations
     * @returns {string}
     */
    toString() {
        let values = []
        for (let idx=0; idx < this.sets.length; idx++) {
            let multiple = this.sets[idx].size > 1;  // set cardinality > 1?
            let items = Array.from(this.sets[idx])
            if (multiple)
                values.push("(" + items.join(", ") + ")")
            else
                values.push(...items)
        }
        return values.join(", ");
    }

    toWhere() {
        // Generate SQL where clause arguments from all constraints
        let comparisons = []
        for (let hidx=0; hidx < this.history.length; hidx++) {
            for (let cidx=0; cidx < this.history[hidx].constraints.length; cidx++) {
                comparisons.push(
                    tablePaths.get_tbl_attr(this.history[hidx].constraints[cidx][0], '.', '`')
                    + ' = ' +
                    tablePaths.get_tbl_attr(this.history[hidx].constraints[cidx][1], '.', '`'))
            }
        }
        let comparison_str = comparisons.join(' and ');
        return comparison_str
    }

    /**
     * Return true if this TableLinkage has constraints present.
     * @returns {boolean}
     */
    hasConstraints() {
        return this.history.length > 0
    }

    /**
     * Return list of constraint pairs for tables t1 & t2 (order independent)
     * @param t1
     * @param t2
     * @returns {*[]}  [(t1_c1, t2_c1), (t1_c2, t2_c2), (t2_c3, t1_c3), ...]
     */
    getConstraints(t1, t2) {
        let clist = []  // List of constraints
        for (let hidx=0; hidx < this.history.length; hidx++) {
            if ((this.history[hidx].a == t1 && this.history[hidx].b == t2) ||
                (this.history[hidx].a == t2 && this.history[hidx].b == t1)) {
                clist.push(...this.history[hidx].constraints)
            }
        }
        return clist
    }

    /**
     * Based upon the current set of constraints, determine an ordering for
     * joining the tables in this TableLinkage
     * @return {*[]} dictionary with fields:
     *    tables - table order in SQL join
     *    on - List of pairs of tables.  When adding tables[t] to the set of joins,
     *       the table constraints from on[t-1] will indicate the pair of tables that
     *       should be used to find keys for the join's ON clause
     */
    getJoinOrder() {
        // Create undirected edges between tables to build a spanning tree
        // We assume that the resulting graph will be fully connected
        let edges = [];
        for (let hidx=0; hidx < this.history.length; hidx++)
            edges.push([this.history[hidx].a, this.history[hidx].b])

        let graph = new UndirectedGraph(this.items, edges)
        let spanningTree = graph.minSpanningTree()

        // Order the edge such that each one builds on tables that have already been seen
        let ordered = graph.adjacentOrdering(spanningTree)

        let vertices = ordered[0]  // pick a pair of vertices
        for (let oidx=1; oidx < ordered.length; oidx++) {
            // each edge should introduce a vertex that was not there before
            let edge = ordered[oidx]
            if (vertices.indexOf(edge[0]) != -1) {
                vertices.push(edge[1])
            } else {
                vertices.push(edge[0])
            }
        }

        let ordering = {}
        ordering.tables = graph.vertIdx2Sym(vertices)  // Convert indices to names
        ordering.on = graph.edgeIdx2Sym(ordered)
        return ordering
    }
}

class UndirectedGraph {
    /**
     *
     * @param vertices  List of vertex names
     * @param edges   List of lists between vertices
     *
     * Example new UndirectedGraph(["a", "b", "c"], ["a", "c"], ["a", "b"]}
     */
    constructor(vertices, edges) {
        this.vertices = vertices;
        this.verticesN = this.vertices.length;

        // Map between node names and node idx
        this.vert2idx = {}
        for (let idx=0; idx < this.verticesN; idx++)
            this.vert2idx[this.vertices[idx]] = idx;

        // Construct the edge list based on the vertex indices
        this.edgesNamed = edges;
        this.edges = []
        for (let idx=0; idx < this.edgesNamed.length; idx++) {
            // Convert named edges to refer to vertices
            let namedEdge = this.edgesNamed[idx]
            let numericEdge = [this.vert2idx[namedEdge[0]], this.vert2idx[namedEdge[1]]]
            this.edges.push(numericEdge)
        }
        this.edgesN = this.edges.length;


    }

    /**
     * Order the edges in a graph such that each edge after the first incrementally introduces
     * a connection to a vertex that has already been visited
     * @param edges  List of edges between vertex indices.  If null, uses the instance edges of the
     *    graph.  Only needs to be provided for a computed subgraph edge list, e.g., a minimum
     *    spanning tree
     * @return {*} List of numeric edges.
     *    List of edges between vertex indices, use edgeIdx2Sym if name list desired
     */
    adjacentOrdering(edges=null) {
        let visited = new Set()
        let order = []

        if (edges == null)
            edges = this.edges

        // Pick an edge, if the graph is fully connected it should not matter which one
        let edge = edges[0]
        visited.add(edge[0])
        visited.add(edge[1])
        order.push(edge)

        while (visited.size < this.verticesN) {
            // Find the first edge where one node has been visited and other has not
            for(let eidx=1; eidx < edges.length; eidx++) {
                edge = edges[eidx]
                let v0 = visited.has(edge[0])
                let v1 = visited.has(edge[1])
                let add = (v0 == true && v1 == false) || (v1 == false && v1 == true)  // xor
                if (add) {
                    // Add the unvisited edge
                    if (v0 == true) {
                        visited.add(edge[1])
                    } else {
                        visited.add(edge[0])
                    }
                    order.push(edge)
                }
            }
        }
        return order
    }

    /**
     * Find the edge subset that results in a minimum spanning tree.
     * @return {*[]} List of numeric edges, use edgeIdx2Sym if name list desired
     */
    minSpanningTree() {
        // based on Kruskal's algorithm

        /**
         * Find operation in union-find, returns index of set to which
         * the vertex belongs
         * @param parents
         * @param vertex
         * @returns {*}
         */
        function getSetIdx(parents, vertex) {
            let current = vertex;
            while (parents[current] != 0) {
                current = parents[current]
            }
            return current;
        }

        function unionSet(s0, s1, parent, rank) {
            if (rank[s0] < rank[s1])
                parent[s0] = s1
            else if (rank[s0] > rank[s1])
                parent[s1] = s0
            else {
                // tie breaker, increase rank of one of them...
                parent[s1] = s0
                rank[s0] = rank[s0]+1
            }
        }

        // union-find algorithm for sets, each vertex is its own parent
        // We won't bother to use Javascript Set since it's functionality is limited
        let parent = new Array(this.verticesN)
        for (let idx=0; idx < this.verticesN; idx++)
            parent[idx] = 0

        // Set rank, all start at zero
        let rank = new Array(this.verticesN).fill(0)

        let minCost = 0;  // Minimum cost so far
        // Process all edges
        let mst_edges = []
        for (let e=0; e < this.edgesN; e++) {
            // Find the set membership for each vertex in the edge
            let s0 = getSetIdx(parent, this.edges[e][0]);
            let s1 = getSetIdx(parent, this.edges[e][1]);

            if (s0 != s1) {
                // Use edge to bridge tree

                // Get edge weight or default to 1 if not present
                let weight = this.edges[e].length > 2 ? this.edges[e][2] : 1
                minCost += weight

                unionSet(s0, s1, parent, rank)
                mst_edges.push(this.edges[e])
            }
        }

        return mst_edges
    } // end minSpanningTree

    /**
     * Convert a numeric list of edges between vertices
     * to list the vertex names
     * @param edges
     * @return {*[]}
     */
    edgeIdx2Sym(edges) {
        let named_edges = []
        for (let e=0; e < edges.length; e++) {
            let edge = edges[e]
            named_edges.push([this.vertices[edge[0]], this.vertices[edge[1]]])
        }
        return named_edges
    }

    /**
     * Convert vertices indices to a list of names
     * @param indices - vertex indices
     * @return {*[]} - vertex names
     */
    vertIdx2Sym(indices) {
        let named_vertices = []
        for (let v=0; v < indices.length; v++)
            named_vertices.push(this.vertices[indices[v]])
        return named_vertices
    }
}
