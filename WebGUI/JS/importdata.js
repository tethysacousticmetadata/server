


// Root elements and the collections to which they belong
let root2collection = {
    "Abbreviations": "SpeciesAbbreviations",
    "Calibration": "Calibrations",
    "Deployment": "Deployments",
    "Detections": "Detections",
    "Ensemble": "Ensemble",
    "Event": "Events",
    "Localize": "Localizations",
    "Mapping": "SourceMaps"
}

/**
 * Add custom file filtering rules to a DropZone object.
 * @param dzoneObj
 */
function dropzoneFileFilter(dzoneObj) {
    dzoneObj.on("addedfile", function(newFile) {
        let n = this.files.length;  // number of files in list
        if (n > 1) {
            // Duplicate only possible when there are > 1 files

            // newFile will be at this.files[n-1], check prior
            for (let idx= 0; idx < n-1; idx++) {
                // Check for duplicate filename (could also check size
                // and lastModified.toString()) if desired but we are
                // trying to prevent duplicate filenames here
                if (this.files[idx].name === newFile.name)
                    this.removeFile(newFile);
            }
        }

    })

}
function getSourceMaps() {

    // Query SourceMaps to retrieve names, root elements, and DocumentSources
    let xq = `
    declare namespace tmpns = "http://tmp.tethys.sdsu.edu";

    declare function tmpns:SourceMapRootDirective($elem as node()) as xs:string {

    let $name := $elem/name()
    return
    if (not($name))
    then
       "unknown"
    else 
       if ($name = "Directives" or $name = "Table" or $name = "Sheet")
       then
          if ($elem/*[1])
         then 
           tmpns:SourceMapRootDirective($elem/*[1])
         else 
           "unknown"
       else
          $name
    };

    
    <Result> {
    for $map in collection("SourceMaps")/Mapping
    return
    <map> 
       {$map/Name},
       <Root>{tmpns:SourceMapRootDirective($map/Directives)}</Root>
       {$map/DocumentSources}
    </map>
    } </Result>`;

    // Execute query
    tethysXQuery(xq, 0, (result) => {
        // XQuery executed successfully - parse & populate web page

        // XML string to DOM tree
        const parser = new DOMParser();
        const doc = parser.parseFromString(result, "application/xml");

        let maps = doc.evaluate(
            "Result/map", doc, null, XPathResult.any, null);

        // Update options for selecting import map
        let jCollections = $("#importmap");
        jCollections.find("option").remove();  // Remove existing options

        // No import map
        jCollections.append('<option value="xml">None (data are Tethys-compliant XML)</option>')
        jCollections.find('option[value="xml"]').data("sources", [{"source": "XML", "odbc": "XML"}]);

        // Other import maps
        let map = maps.iterateNext()
        while (map) {
            // Retrieve collection name and populate option
            let name = doc.evaluate("Name/text()", map, null, XPathResult.STRING_TYPE).stringValue;
            jCollections.append(`<option value="${name}">${name}</option>`)

            // Retrieve newly added option
            let option = jCollections.find(`option[value="${name}"]`)

            // Get root element and then find set the collection associated with it
            let root = doc.evaluate("Root/text()", map, null, XPathResult.STRING_TYPE).stringValue;
            let collection = root2collection[root];
            option.data("collection", collection)

            // Collect source hint information (used to populate sources templates)
            let srcNames = doc.evaluate("DocumentSources/Source", map, null);
            let src = srcNames.iterateNext();
            let sources = [];
            if (src != null) {
                let srcName = src.getElementsByTagName("Name")[0].textContent;
                let odbc = src.getElementsByTagName("OpenDatabaseConnectivity")[0].textContent;
                sources.push({"source": srcName, "odbc": odbc})
            }
            option.data("sources", sources)
            map = maps.iterateNext()
        }

        jCollections.selectpicker("refresh")
        mapSelect("#importmap");  // Update inputs for current map
    })

}

/**
 * Populate the species abbreviations drop down
 */
function getSpeciesAbbreviations() {
    // Query for names of SpeciesAbbreviations maps
    let xq = `
      declare default element namespace "http://tethys.sdsu.edu/schema/1.0";
      collection("SpeciesAbbreviations")/Abbreviations/Name/text()    `

    tethysXQuery(xq, 1, (result) => {
        let names = result.split("\n");
        names.sort();
        let jqAbbr = $("#speciesmap");
        if (jqAbbr.length > 0) {
            // Add standard options TSN & Latin
            jqAbbr.append('<option value="TSN">ITIS taxonomic serial no</option>')
            jqAbbr.append('<option value="Latin">Latin names</option>')
            // Add database-specific species maps
            for (let idx=0; idx < names.length; idx++) {
                jqAbbr.append(`<option value=${names[idx]}>${names[idx]}</option>`)
            }
            jqAbbr.selectpicker('refresh')
        }
    })
}

/**
 * Given a selection for import map, attempt to determine to which collection to which we be adding.
 * Set the collection name if possible.
 * @param importMap
 */
function mapSelect(importMap, allow_duplicates) {
    let jqImportMap = $(importMap);
    let selected = jqImportMap.find("option:selected")
    let collection = selected.data("collection");

    // Set species coding as disabled for XML, enabled for everything else
    let jqSpeciesMap = $("#speciesmap");
    let status_current = jqSpeciesMap.prop("disabled");
    let status_desired = selected.val() == "xml"
    if (status_current != status_desired) {
        jqSpeciesMap.prop("disabled", status_desired);
        jqSpeciesMap.selectpicker("refresh")
    }

    // Select the collection associated with the current import map (if available)
    if (collection && collection != "undefined") {
        $("#collection").selectpicker("val", collection);
    }

    // Add in any sources that we know we will need for this import map
    // (old sources are not deleted)
    let sourcediv = $("#dataSourceList");
    let sources = selected.data("sources");
    if (sources && sources.length) {
        // User provided type of ODBC data in the source map
        for (let idx = 0; idx < sources.length; idx++) {
            let source = sources[idx];
            addSource(sourcediv, source, allow_duplicates)
        }
    } else {
        // No sourcemap was given or it did not provide a source hint
        if (selected.val() == "xml") {
            // User has an XML source
            addSource(sourcediv, {"OpenDatabaseConnectivity": "XML"}, true)
        } else {
            // default to a common type of source map
            addSource(sourcediv, {"OpenDatabaseConnectivity": "Excel"})
        }
    }
}


/**
 * Given a FileEntry, create a Promise that will resolve to
 * a set of FileEntry objects.  When FileEntry is a file, it
 * returns the FileEntry.  When FileEntry is a directory, it
 * creates a recursive list of all file children of FileEntry.
 * @param entry - FileEntry object
 * @return {Promise<any>}
 */
function traverseFSEntry(entry) {
    // based on post by Tom J Newell
    // https://stackoverflow.com/questions/18815197/javascript-file-dropping-and-reading-directories-asynchronous-recursion

    let promise;
    if (entry.isDirectory) {
        // Set up to recursively read directory entries
        const reader = entry.createReader();

        promise = new Promise((resolve, reject) => {
            const promises = [];

            function traverseFSDir(reader) {
                reader.readEntries(
                    (entries) => {
                        // entries contains most recent read
                        if (! entries.length) {
                            // No more reads, resolve the promise chain
                            resolve(Promise.all(promises));
                        } else {
                            promises.push(
                                Promise.all(entries.map(
                                    (e) => {
                                        // Recurse on FSEntry to handle new item
                                        return traverseFSEntry(e);
                                    })))
                            // Continue reading the current directory as there may be more
                            traverseFSDir(reader);
                        }
                    }
                )
            }
            traverseFSDir(reader);
        });
    } else {
        promise = new Promise( (resolve, reject) => {
            resolve(entry);
        });
    }

    return promise;
}

/**
 * compress a buffer using specified encoding system
 * @param buffer - buffer array to compress
 * @param encoding - encoding system, e.g., "gzip"
 */
function compress(buffer, encoding) {
    // Set up compression pipeline
    const cs = new CompressionStream(encoding);
    const writer = cs.writable.getWriter();
    // Compress the data
    writer.write(buffer);
    writer.close()
    // Return the data
    return new Response(cs.readable).arrayBuffer()
}

class FilePackage {
    constructor() {
        this.files = {};
        this.archive = {};
    };

    /**
     * Add a list of files that will be uploaded
     *  files - array of File instances to add
     */
    add_files(files) {
        this.#add_files_to(files, this.files);
    }

    /**
     * Add a list of files that will be compressed to a zip file
     * and sent as an attachment archive
     * @param files
     */
    add_archive_files(files) {
        this.#add_files_to(files, this.archive);
    };

    /**
     * Private method to add list of files to a specific dict
     * @param files
     * @param dictionary
     * @private
     */
    #add_files_to(files, dictionary) {
        for (let idx=0; idx < files.length; idx++) {
            let f = files[idx];
            dictionary[f.name] = f;
        }
    }

    /**
     * Private method to retrieve the keys of a dictionary
     * @param dictionary
     * @return {string[]}
     * @private
     */
    #get_keys(dictionary) {
        return Object.keys(dictionary)
    }

    /**
     * Return list of filenames
     * @return {string[]}
     */
    get_filenames() {
        return this.#get_keys(this.files)
    };

    /**
     * Return file manager entry for file list entry based on name
     * @param name
     * @return {*}
     */
    get_file(name) {
        return this.#get_by_key(name, this.files)
    }

    /**
     * Return file manager entry for archive list entry based on name
     * @param name
     * @return {*}
     */
    get_archive(name) {
        return this.#get_by_key(name, this.archive)
    }

    /**
     * Return file manager entry based on name for given dictionary
     * @param name
     * @param dictionary
     * @return {*}
     */
    #get_by_key(name, dictionary) {
        return dictionary[name]
    }


    /**
     * Return list of names destined for an attachment archive
     * @return {string[]}
     */
    get_archive_filenames() {
        return this.#get_keys(this.archive)
    };
}

function upload(jqDataImport) {
    let importMap = jqDataImport.find("#importmap").selectpicker("val");
    let collection = jqDataImport.find("#collection").selectpicker("val");
    let speciesMap = jqDataImport.find("#speciesmap").selectpicker("val");

    let sources = jqDataImport.find(".dataTarget");
    if (sources.length == 0) {
        alert("Must specify at least one source before uploading.");
        return
    }
    let xml = xmlbuilder2.create("import");
    let root = xml.root();

    root.ele("overwrite").txt(
        jqDataImport.find("#overwrite").is(":checked") ? "true" : "false");

    if (!(importMap.match(/xml/i))) {
        root.ele("source_map").txt(importMap);
        root.ele("species_map").txt(speciesMap);
    }

    let filepkg = new FilePackage();  // file payload manager

    // Visual feedback to user that something is happening (not really, POST will not provide feedback)
    $(".uploading-false").hide()
    $(".uploading-true").show();

    populateServerResponse($("#importResult"), "",
        "Preparing Tethys submission...", false);
    // Set up each source section, and add associated files to filepkg
    let sourcesXml = root.ele("sources");
    for (let sidx = 0; sidx < sources.length; sidx++) {
        let source = sources.eq(sidx);
        let added = addSourceXml(source, filepkg, sourcesXml);
        if (! added) {
            let sourceName = source.find(".sourceName").val()
            populateServerResponse($("#importResult"),
                "Source named \"" + sourceName + "\" does not have data associated with it.",
                "Cannot submit");
            return
        }
    }

    // Declare and populate POST form data
    let form = new FormData();

    // Add specifications and source files
    let filenames = filepkg.get_filenames()
    if (filenames.length > 0) {
        // Use first filename without extension as docname
        let docname = filenames[0];
        docname = docname.substring(0, docname.lastIndexOf("."));
        root.ele("docname").txt(docname);

        // Add specific files to form
        // todo:  compress xml, will need to adjust filename as well & decompress on server
        for (let idx = 0; idx < filenames.length; idx++) {
            form.append(filenames[idx],
                filepkg.get_file(filenames[idx]),
                filenames[idx]);
        }
    }
    let specificationXml = xml.end({format: 'xml', prettyPrint: true});
    form.append('specification', specificationXml);
    console.log(specificationXml)

    // Add any supplemental information to a zip archive.
    let attachments = $("#attachments")[0].dropzone.getQueuedFiles();
    let archFilesN = attachments.length;
    let archive_barrier = null;
    if (archFilesN > 0) {
        // Select name for archive based on root name of first file
        let packageName = filenames[0]
        packageName = packageName.substring(0, packageName.lastIndexOf('.'));
        let archiveName = [packageName, 'attach.zip'].join('-');
        // Add entries for zip file
        let archive = new JSZip();
        for (let idx = 0; idx < archFilesN; idx++) {
            let a = attachments[idx];
            archive.file(a.name, a)
        }
        archive_barrier = new Promise((resolve, reject) => {
            archive.generateAsync({type: "blob"}).then((content) => {
                let archiveFile = new File(
                    [content], archiveName, {type: 'application/x-zip'})
                form.append("Attachment", archiveFile);
                resolve();
            })
        })
    } else {
        archive_barrier = Promise.resolve();  // will resolve right away.
    }
    // Wait until archive is built
    archive_barrier.then( (resolve) => {
        populateServerResponse($("#importResult"), "", "Submitting to Tethys", false)
        let ajaxCall = new AjaxWrapper("post", form, importFailure);
        let url = window.location.origin + "/" + collection + "/import";
        ajaxCall.setURL(url);
        $.ajax(ajaxCall.getSettings())
            .done(function (response) {
                populateServerResponse($("#importResult"), response, "Import Result");
            }).fail(function (response) {
                if (response.responseText == null) {
                    populateServerResponse($("#importResult"),
                        "Send to server failed for unspecified reason." +
                        "\nOne known cause for this is changing the file contents after specifying the file to upload." +
                        "\nThis can be addressed by removing the file and adding it back in.",
                        "Failed to send to server")
                } else {
                    populateServerResponse($("#importResult"),
                        response.responseText,
                        "Tethys server was unable to complete request")
                }
                console.log(response);
                console.log("ajax call failed")
            })
        return true
    })
}


/**
 * Server responses come in two varieties:
 * XML - This will be displayed in a <pre> (preformatted) block
 * HTML - Will be displayed as sent, must start with <!DOCTYPE html
 * @param jqElement
 * @param message - response message
 * @param title - optional title, only used for XML documents as HTML likely to have a title
 * @param stopProgressIndicator - if True, stops the progress indicator
 */
function populateServerResponse(jqElement, message, title=null, stopProgressIndicator=true) {

    if (stopProgressIndicator) {
        // Stop progress spinner
        $(".uploading-true").hide();
        $(".uploading-false").show();
    }

    let html;
    if (message != null) {
        if (message.startsWith("<!DOCTYPE html")) {
            html = message;
        } else {
            let escaped = jqElement.text(message).html();  // Escape xml text
            html = "";
            if (title)
                html = `<h2>${title}</h2>\n`
            html = html + "\n<pre>\n" + escaped + "\n</pre>";
        }
    } else
        html = "No response from server";
    jqElement.html(html)
}

function importFailure(reqObj, err, err2) {
    populateServerResponse($("#importResult"), reqObj.responseText, "Import contains one or more failures");
    console.log(reqObj.responseText)
}

/**
 * addSourceXml - Package data for submission to server.
 * @param source - Each group of files is assigned a name, in many cases this may
 *    be left blank.
 * @param filePackage
 * @param xml
 */
function addSourceXml(source, filePackage, xml) {
    let sourceXml = xml.ele("source");
    let sourceName = source.find(".sourceName").val()
    if (sourceName == "")
        sourceName = "default-source";

    let options = getODBC(source);

    // Is the user specifying one or more files/directories?
    let jqFile = source.find(".filePath");
    let fileP = ! jqFile.is(":hidden");
    let promise = null
    if (fileP) {
        // Populate the file package and import specification for the files to be sent
        let dropZone = source.find(".dropzone");
        if (dropZone.length == 1) {  // dropzone widget exists
            dropZone = dropZone[0].dropzone;  // Access nested Dropzone instance
            let inputs = dropZone.getQueuedFiles();

            if (inputs.length == 0)
                return false;

            filePackage.add_files(inputs)

            // Write out the XML description for this source
            sourceXml.ele("type").txt(options.type);
            sourceXml.ele("name").txt(options.odbc.sourceName);
            for (let idx=0; idx < inputs.length; idx++) {
                sourceXml.ele("file").txt(inputs[idx].name);
            }

            // Override dbFileName based on the selected file
            options.odbc.dbFileName = inputs[0].name;
            addXmlOdbc(options, sourceXml)
        }
    } else {
        // ODBC resource connection
        sourceXml.ele("type").txt("resource");
        sourceXml.ele("name").txt(sourceName);
        addXmlOdbc(options, sourceXml)
    }

    return true;
}

/**
 * Populate the ODBC selection types
 */
function importInitialize() {

    getSourceMaps();  // Populate import maps
    getSpeciesAbbreviations(); // Populate species abbreviations

    $(document).on('change', '.file-input', function() {
        let jqInput = $(this)
        let files = jqInput[0].files
        let filesN = files.length;
        let textbox = jqInput.parent().find(".file-message")
        let maxShow = 7  // maximum # files to list

        // Show the first maxShow items and a message if there are more
        let basenames = []
        let pathsep;
        let listN = Math.min(maxShow, filesN);
        for (let idx=0; idx < listN; idx++) {
            // Extract the basename of the file.  We don't know what the path separator is
            // or if there will even be one, but it is either / or \.  If we don't find /,
            // assume \.
            let filename = files[idx].name;
            if (filename.lastIndexOf('/') == -1)
                pathsep = '\\';
            else
                pathsep = '/';
            let basename = filename.split(pathsep).pop()
            basenames.push(basename)
        }
        let msg = basenames.join(", ")

        let remaining = filesN - maxShow;
        if (remaining > 0) {
            msg = msg + ` and ${remaining} more files`
        }
        textbox.text(msg);
    });

    //mapSelect('#importmap', true);  // initialize a data source

    // Disable drag & drop outside of target drop zone
    //$(document).bind('drop dragover', function (e) {
    //        e.preventDefault();
    //});

}
