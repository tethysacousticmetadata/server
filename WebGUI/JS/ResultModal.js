/**
 * @author Daniel Weston
 * @version 1.0
 * DSanchezWeston@gmail.com
 * 28 Aug 2018
 */
"use strict";

/**
 * Used to be a Modal built to display the results of the xQuery. Now used to
 * display a loader after submit query is pressed to visually give the user an indication
 * that the server is still working.
 */
class ResultModal{
    constructor(modalID,closeID){
        this.modalID = modalID;
        this.closeID = closeID
        this.modalElement = document.getElementById(''+modalID+'');
        //this.modalHeader = document.getElementById("modalHeader");
        this.modalBody = document.getElementById(''+modalID+'Body');
        this.modalTextArea;
        this.span;
        this.setSpan(closeID,modalID);  // attaches event handler to close span
        this.errorSpan = document.getElementById("modal-error");
        //this.setBody('<div class="loader d-inline-flex justify-content-center"></div>');
        
    }

    /**
     * Adds hide modal to the X in the modal
     */
    setSpan(closeID,modalID){
        //this.span = document.getElementById(''+closeID+'');   //close is an "X in the top right corner"
        //alert(modalID)
        this.span = $('#'+closeID+'')[0];
        function closeButtonPress(modal) {  //closure for modal hide
            return function() {
                //modal.hide();
                console.log('setSpan Modal Hide');
                $('#'+modalID+'').modal('hide');

            }
        }
        this.span.addEventListener("click", 
            closeButtonPress(this));

    }

    /**
     * <span id="close" class="close">&times;</span>
     */
    setHeader(){

        modalHeader.innerHTML = '<span id="close" class="close">&times;</span>';// + this.endOfHeader;
    }

    /**
     * A query was submitted via AJAX to the server and a loader is written in the modal body
     * to indicate to the user that the server is working. It's the blude spinning object.
     */
    setLoader(){
        //this.modalHeader.innerHTML = "";  // this was clearing out the close span
        //<div class="loader d-inline-flex justify-content-center
        this.setBody('<div class="loader justify-content-center"></div>');
    }

    /**
     * @param {String} html is the text to be inserted into the body of the modal
     */
    setMessage(html){

        //this.setHeader();
        //this.setSpan(this.closeID,this.modalID);
        this.setBody(html);
    }

    /**
     * @param {String} html is the text to be inserted into the body of the modal
     */
    setCannedQueryResults(html){
    //this.modalBody.textContent = html;
    this.modalBody.innerHTML = '<pre lang="xml">'+html+'</pre>';
    }


    setBody(html){
        this.modalBody.innerHTML = html;
    }

    /**
     * @deprecated
     * Used to contian some HTML Buttons. Might find some use for it later.
     */
    setFooter(){
        
    }

    /**
     * Show the modal on the webpage
     */
    show(){
        console.log("show modal");
        $('#advancedModal').modal('show');
    }

    /**
     * Hide the modal on the webpage
     */
    hide(modalID){
        
        function sleep(ms) {
            return new Promise(resolve => {setTimeout(resolve, ms);});
          }
          async function stall() {  //Stall the modal from hiding. Clears a bug where the modal would open and close too fast
            await sleep(1000);      //milliseconds
            $('#'+modalID+'').modal('hide');
          }
          stall();
    }
}