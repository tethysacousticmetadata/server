/**
 * @author Daniel Weston
 * @version 1.0
 * DSanchezWeston@gmail.com
 * With the help of Professors Roger Whitney and Marie Roch. San Diego State University.
 * 23 Aug 2018
 */
"use strict";

/**
 * a class that builds a xQuery to be submitted to a Tethys server. 
 * The query can have multiple nested queries.
 */
class SubmitQuery{
    
    /**
     * 
     * @param {Array[ULData]} whereData 
     * @param {Array[ULData]} returnData 
     * @var {Array[String]} collections //(e.g. ["Detections", "Deployment"]) 
     * @var {FLWOR} flwor The complete flwor. It can have nested flwors inside, but at the end of buildQuery
     *                      It'll be a complete FLWOR
     */
    constructor(whereData, returnData, speciesLibrary, recordName){
        this.collections_orderBy = ['Deployment', 'Detections', 'Localize', 'Detections', 'Calibrations','Abbreviations'];
        this.whereData =  whereData;    //{Array[ULData]}
        this.returnData = this.sort_collections(returnData)  //{Array[ULData]}
        //var justsetthis = this.sort_collections(returnData)
        //alert(this.sort_collections(returnData)); //+this.sort_collections(returnData)
        this.speciesLibrary = speciesLibrary;
        this.collections = [];          //(e.g.)
        this.returnCollections = [];
        this.determineReturnCollections();  //if multiple collections found query will be a join
        this.determineWhereCollections()  //tried this to see if we could base joins on where clauses
        this.whereSet = [];
        if (recordName){
            this.recordName = recordName;
        } else {
            this.recordName = "out"
        }

        this.flwor;
        this.collectionMap = {
            "Calibration": "collection('Calibrations')/Calibration",
            "Detections": "collection('Detections')/Detections ",
            "Deployment": "collection('Deployments')/Deployment",
	    "Ensemble": "collection('Ensemble')/ty:Ensemble",
	    "Event": "collection('Events')/Events",
	    "Itis": "collection('ITIS')/ITIS",
	    "Localize": "collection('Localizations')/Localize",
	    "Abbreviations": "collection('SpeciesAbbreviations')/Abbreviations"
        };

	// Set default namespaces, prefixes, etc.
	this._ty_namespace = SubmitQuery.DEFAULT_SDSU_NAMESPACE;
	this._lib_namespace = SubmitQuery.DEFAULT_LIB_NAMESPACE;
	// FLWR expression will be inserted between result start and end
	this._result_start = SubmitQuery.DEFAULT_START  
	this._result_end = SubmitQuery.DEFAULT_FOOTER
    }


    buildJSONQuery(){
        //alert('is it here')
        // loop through where data and get properties from each UL object
        let selectArray = []
        let constructSpsBlock = true
        this.whereData.forEach(function(ul){
            let operator = ul.operatorValue;
            let value = ul.inputValue;
            let path = ul.path;
            if (path.includes("Detections") || path.includes("Localizations")) {
                // Might return something with a SpeciesId
                constructSpsBlock = true;
            }
           selectArray.push({"op":operator,"operands":[path,value],"optype":"binary"})

        })

        let returnArray = []
        this.returnData.forEach(function(ul){
            // Anything with Detections requires the species block as we do not know
            // what children the returned element might have
            if (ul.path.includes("Detections"))
                constructSpsBlock = true;
            returnArray.push(ul.path)
        })

        // example species obj, needs to add to outer object
        //"species":{"query":{"op":"lib:abbrev2tsn","optype":"function","operands":["%s","SIO.SWAL.v1"]},
        //          "return":{"op":"lib:tsn2completename","optype":"function","operands":["%s"]}}

        if (constructSpsBlock){

            if ($('#speciesSelectAdvanced').val() != "latin") {
                    var fromFunc = "lib:abbrev2tsn"
                    var toFunc = "lib:tsn2abbrev"
                    var operand = ["%s",$('#speciesSelectAdvanced').val()]
                } else {
                    var fromFunc = "lib:completename2tsn"
                    var toFunc = "lib:tsn2completename"
                    var operand = ["%s"]
                }
            var spsBlock = {}
            spsBlock["query"] = {"op":fromFunc,"optype":"function","operands":operand}
            spsBlock["return"] = {"op":toFunc,"optype":"function","operands":operand}

        }
        if (constructSpsBlock)
        { var JSONObj = {"species":spsBlock}}
        else
        {var JSONObj = {};}
        JSONObj["return"] = returnArray;
        JSONObj["select"] = selectArray;
        JSONObj["enclose"] = 1;  // Wrap things that appear multiple times within their enclosing element
        JSONObj["namespaces"] = 0;  // Strip namespaces from returned data
        console.log(JSON.stringify(JSONObj))
        return JSON.stringify(JSONObj)

    }

    // namespace getters and setters
    set ty_namespace(ns) {
	this._ty_namespace = ns;
    }

    get ty_namespace() {
	return this._ty_namespace;
    }

    set lib_namespace(ns) {
	this._lib_namespace = ns;
    }

    get lib_namespace() {
	return this._lib_namespace;
    }

    set result_start(startxml) {
	this._result_start = startxml;
    }

    get result_start() {
	return this._result_start;
    }

    set result_end(endxml) {
	this._result_end = endxml;
    }

    get result_end() {
        return this._result_end;
    }

    // getters that build on above properties
    get header() {
	// Add in namespaces
	let str = this.ty_namespace;
	str += this.lib_namespace;
	// Add in start for FLWR
	str += this.result_start

	return str;
    }

    get footer() {
	return this.result_end;
    }
    
    sort_collections(items){
     var result = [];
        this.collections_orderBy.forEach(function(key) {
        var found = false;
        items = items.filter(function(item) {
        if(item.path_collection == key) {
            result.push(item);
            found = true;
            return false;
        } else
            return true;
        })
    })

    return result;
    }

    static get DEFAULT_SDSU_NAMESPACE() {
        return 'declare default element namespace "http://tethys.sdsu.edu/schema/1.0";\n';
    }

    static get DEFAULT_LIB_NAMESPACE() {
        return 'import module namespace lib="http://tethys.sdsu.edu/XQueryFns" at "Tethys.xq";\n';
    }

    static get DEFAULT_START() {
        return '<Result xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">{';
    }

    get DEFAULT_HEADER() {
        let str = SubmitQuery.DEFAULT_SDSU_NAMESPACE;
        //if($('#speciesSelect').val())
        str += SubmitQuery.DEFAULT_LIB_NAMESPACE;
        return str+=SubmitQuery.DEFAULT_START;
    }

    static get DEFAULT_FOOTER() {
        return '}\n</Result>';
    }

    /**
     * The entry point into this class. This should be the only function called outside of the class
     * @see AdvancedQueries.queryChanged() , @see AdvancedQueries.submitQueryPressed()
     * @param {D3Json} theTree The entire collections tree. Where the top node is called Collections.
     * @param {Boolean} isFlat parameter used to indicate the query built should be a single flower 
     */
     /**
     *                                          /\
     *                                         /  \ BuildInitialFlwor() The join outer FLWOR
     *                                        /____\
     *                                       /   |  \
     *         BuildFlwor() Deployment      /    |   \  BuildFlwor() Detections
     *                                     /     |    \
     *                                    /______|_____\
     */
    buildQuery(theTree, isFlat, isCanned){


        let query = "";
        if(isFlat && this.collections.length>1)
	    //******* Flat and Multiple collection       /\
	    //                                          /  \ BuildFlatJoinedQuery() one FLWOR
	    //                                         /____\
            query = this.buildFlatJoinedQuery(theTree); 
        else if(isFlat){
	    //******* Flat and Single collection       /\
	    //                                        /  \   BuildFlatQuery()    one FLWOR
	    //                                       /____\
            query = this.buildFlatQuery(theTree.findCollection(this.collections[0]));
        }                                                                       
        else if(!isFlat && this.collections.length>1){
	    //nested and multiple collections
            //********************************************* FOR EACH COllECTION BUILD A FLWOR FOR THAT COLLECTION ******************************************/
            let collectionQueriesTemp = [];
            this.returnCollections.forEach(collection =>{        //forEach (e.g. ["Deployments, Detections"])
                collectionQueriesTemp.push(this.buildNonInitialQuery(theTree.findCollection(collection))); //Builds the inner FLWORs for each collection
                theTree.reset();                        /**@see D3Json.reset() removes all added keys, value pairs to build FLWORS*/
            });    
            //********************************************** ALL INNER FLWORS ARE BUILT ********************************************************************/
            //*********************** FOR EACH WHERE NOT INSIDE THE INNER FLOWERS FIND WHAT COLLECTION IT BELONGS TO AND FORMAT IT ACCORDINGLY *************/
            this.whereSet = []; //prevent repeats by clearing first
            this.whereData.forEach(data=>{  //reading all original where data so they can all be added to the outer flowr
                this.whereSet.push(data);   //cloning
            }) 
            let upperWheres = [];
            this.collections.forEach(collection =>{
		//                                          /\
		//                                         /  \ BuildInitialJoinFlwor() The join outer FLWOR
		//                                        /____\
		//                                       /   |  \
		//         BuildFlwor() e.g.Deployment  /    |   \  BuildFlwor() e.g. Detections inner (nested) FLOWER
		//                                     /     |    \
		//                                    /______|_____\
                let collectionTree = theTree.findCollection(collection);                                
                let collectionWheres = [];                                                              
                this.whereSet.forEach(whereItem => {                                                    
                    if(this.getCollection(collectionTree) ==  this.getCollection(whereItem.getNode()))  
                        collectionWheres.push(whereItem);                                               
                });                                                                                     
                collectionTree['forVar'] = 1;
                upperWheres.push(this.buildWhere(collectionTree, collectionWheres, isCanned));  // START HERE
            });
            
            theTree.reset();
            //********************************************************** BUILD THE OUTER FLWOR **************************************************************/
            if (Array.isArray(collectionQueriesTemp)){  // collectionQueriesTemp can be an array of arrays, if so flatten
                if (Array.isArray(collectionQueriesTemp[0])){
                var collectionQueries = this.flatten(collectionQueriesTemp);
                }
                else {var collectionQueries = collectionQueriesTemp;}
            }
            else {var collectionQueries = collectionQueriesTemp;}

            query = this.initialJoinFlwor(theTree, collectionQueries, upperWheres);
        }
        else if(!isFlat){       //LOOK HERE                        //nested and single                 /\
            let singleCollection = theTree.findCollection(this.collections[0]); //         /  \ initialFlwor                        outer FLWOR
            query = this.buildNonInitialQuery(singleCollection);                //        /____\
            console.log("DEPLOYMENT "+query)
            //reload whereset so that whole whereset gets sent to initialFlwor
            this.whereSet = []; //prevent repeats by clearing first
            this.whereData.forEach(data => {  //reading all original where data so they can all be added to the outer flowr
                if (data.excludeFromLoop != "outer")
                    this.whereSet.push(data);   //cloning
            })

            console.log("QUERY BEFORE INITIAL FLOWER")
            console.log(query) // this contains the 'inner' for loop in the return statement, it is an array of strings, one for each return statement

            query = this.initialFlwor(singleCollection , query, this.whereSet, isCanned); //       /      \
                                                                                //      /        \  buildNonInitialQuery()          inner (nested) FLWOR
                                                                                //     /__________\
        }
        theTree.reset();
        console.log(query);
        return query;
    }

    /**
     * This function marks all nodes in the return and where statements and their parents to the root node. Then marks
     * all the nodes with maxOccurs = "unbounded".
     * @param {XmlToD3} XmlAndD3Json instance with both the xml and d3 JSON Object trees stored within it.
     * @var {Array[ULData]} returnSet
     * @var {Array[ULData]} whereSet
     * @return {String} a complete FLWOR XQuery
     */
    buildNonInitialQuery(theCollectionTree){
        var returnSet = [];                     //clone of returnData as oppose to reference
        this.returnData.forEach(element => {    //all return paths
            returnSet.push(element.getNode());
        }); //all nodes are found
        // #################################################################  RETURN START  ###################################
        for(var index in returnSet){    //adding returnCount keys to D3JSON and incrementing everytime a leaf to root path intersects
            this.toCollectionCounting("returnCount", returnSet[index]);
        }

        for(var index in returnSet){    //building nested queries
            this.safeAddFlworNode(returnSet[index], returnSet[index]); //mark nodes with maxOccurs key and unbounded value. Eventually create a flwor for it.
            this.toCollectionSetupQueries(returnSet[index]);
        }

        // ###################################  RETURN END  ################   WHERE START  ###################################
        this.whereSet = [];                      //clone of whereData as oppose to reference
        this.whereData.forEach(element => {     //all where paths
            this.whereSet.push(element);   //cloning
        }); //all nodes are found
        for(var index in this.whereSet)    //adding count to D3JSON
        {

            this.toCollectionCounting("whereCount", this.whereSet[index].getNode());
        }
        theCollectionTree['forVar'] = 1;
        // ####################################  WHERE END  #####################################################################
        //START Building Queries
        console.log('collectionTree');
        console.log(theCollectionTree);
        return this.down( theCollectionTree, theCollectionTree);             //Builds all nested FLWOR statements
    }

    determineReturnCollections(){

        this.determineCollections(this.returnData);
        this.collections.forEach(element => {
                this.returnCollections.push(element);
            });

    }

    determineWhereCollections(){
        this.determineCollections(this.whereData);
    }

    /**
     * Finds the collection name of all nodes in the nodes array (e.g. the node DeploymentDetails is a node under the Deployment
     * Collections. Effort is under the Detections collections etc.) When the collection is found check if it's all ready been
     * been added from a different node. If not add it to @var {Array[String]} this.collections array
     * @param {Array[ULData]} nodes
     */


    determineCollections(nodes){
        //this.collections = [];  //This was cleaning out the collections array so that both Where and Return's couldn't be added together
        nodes.forEach((element)=>{
            if(!this.collections.includes(this.getCollection(element.getNode())))   //If string isn't in collections array
                this.collections.push(this.getCollection(element.getNode()));       //add string
        })
        console.log('collections from SubmitQuery '+this.collections);
    }

    /**
    * A down tree recursive function. Depth before breath
    * @param {Object} node the current node in the traversal
    * @param {Object} lastFlworNode the next node above @var node that a FLWOR statement will be created at
    * @return {Array or String} The return can be a string or a flwor, string of a leaf (bottom) node, or an
    * array of Strings that gets flattened after it's returns
    */
    down(node, lastFlworNode) {
        var childrensFlwor = [];
        var passDownLastFlworNode = lastFlworNode;
        //********************************************* PRE RECURSION *********************************************
        if(node.hasOwnProperty('flworNodes'))   //If node has a flworNodes key increment the forVar value. A flower will be created at this node
        {
            node['forVar'] = lastFlworNode['forVar']+1; //increment
            passDownLastFlworNode = node;       //lastFlworNode becomes this node
        }
        //********************************************* RECURSION *********************************************
        for (var childNode in node.children) {  //for each children key
            if (node.children[childNode] !== null && typeof(node.children[childNode])=="object" && node.children[childNode].hasOwnProperty('returnCount')) {    //going one step down in the object tree!!
                childrensFlwor.push(this.down(node.children[childNode], passDownLastFlworNode));    //Recurse!
                childrensFlwor = this.flatten(childrensFlwor);  //If an array was return it'll be an array within in an array so we flattend the 2D array to 1D
            }
        }
        for (var childNode in node._children) {  //for each _children key this is a workaround if somehow the nodes are hidden after being selected. This is the exact same code as the 6 lines of code above it. It could be wrapped in a function. Lazy Programming!
            if (node._children[childNode] !== null && typeof(node._children[childNode])=="object" && node._children[childNode].hasOwnProperty('returnCount')) {    //going one step down in the object tree!!
                childrensFlwor.push(this.down(node._children[childNode], passDownLastFlworNode));    //Recurse!
                childrensFlwor = this.flatten(childrensFlwor);  //If an array was return it'll be an array within in an array so we flattend the 2D array to 1D
            }
        }
        //********************************************* POST RECURSION *********************************************
        if(node.hasOwnProperty('flworNodes'))   
        {
            var tempWhere = [];
            for(var whereIndex = this.whereSet.length-1; whereIndex >= 0; whereIndex--){    //for each where condition with a node under the current unbounded node 
                if(this.replacePath(this.whereSet[whereIndex].getNode(), node) != "")       //check if any where condition nodes are underneath this node
                {
                    //console.log("after: "+ this.replacePath(this.whereSet[whereIndex].getNode(), node));    //used for debugging
                    tempWhere.push(this.whereSet[whereIndex]);  //add where condition 
                    this.whereSet.splice(whereIndex,1);         //remove the where condition from the object array
                }
            }
            return this.buildFlwor(node, lastFlworNode, childrensFlwor, tempWhere); //build a nested flwor
        }
        else if(childrensFlwor.length == 0)//(hasOwnProperty('count'))   //No more child nodes. Tree bedrock. Prints only tree ends (final children, children without children)
        {
            return "$"+this.replacePath(node, lastFlworNode);
        }
        return childrensFlwor;
    }

    /**
     * Flattern an array within in an array into a 1D array
     * @param {Array or String} arr 
     */
    flatten(arr){   
        return [].concat(...arr);
    }

    /**
     * Recusivly traverse a tree bottom to top and check if it has a 'maxOccurs' key.
     * If it does check if the value of the key is 'unbounded' and add a new key 'flworNodes' and val
     * @param {Object} node node of a D3Json Tree
     */
    toRootSetupQueries(node){
        if(node == undefined)
            console.log("ppp");
        if(node.parent == null) // If parent is null. It's root so return.
            return;
        if(node.parent.hasOwnProperty('maxOccurs'))
        {
            this.safeAddFlworNode(node.parent, node);   //adding elements for a FLWOR build
        }
        this.toRootSetupQueries(node.parent);        
    }

    /**
     * Recusivly traverse a tree bottom to top and check if it has a 'maxOccurs' key.
     * If it does check if the value of the key is 'unbounded' and add a new key 'flworNodes' and val
     * @param {Object} node node of a D3Json Tree
     */
    toCollectionSetupQueries(node){
        if(node == undefined)
            console.log("ppp");
        if(node.parent == null || node.parent.name == "Collections") // If parent is null. It's root so return.
            return;
        if(node.parent.hasOwnProperty('maxOccurs'))
        {
            this.safeAddFlworNode(node.parent, node);   //adding elements for a FLWOR build
        }
        this.toCollectionSetupQueries(node.parent);
    }

    /**
     * Add a flworNodes key at the node if it has a maxOccurs key and an unbounded value
     * @param {Object} node node of a D3Json Tree
     * @param {Object} flworNode node of a D3Json Tree
     */
    safeAddFlworNode(node, flworNode){
        if(node['maxOccurs'] == "unbounded")
            {
                if(!node.hasOwnProperty('flworNodes'))  //if node doesn't has a flworNodes key add one
                    node['flworNodes'] = [];
                node['flworNodes'].push(flworNode);
            }
    }

    /**
     * Traveling from a node to a root and adding a key to that node and incrementing it if it all ready has it
     * the count is used to determine if to nodes share a node on the path to the root. The root will always equal the
     * number of nodes below it.
     * @param {String} key would be "whereCount" or "returnCount"
     * @param {Object} node node of a D3Json Tree
     */
    toRootCounting(key, node){  //DEPRECATED
        if(!node.hasOwnProperty(key))   //if node doesn't have key add one
            node[key] = 0;
        node[key]++; 
        if(node.parent != null) // Not root node
        {
            this.toRootCounting(key, node.parent);
        }
    }

    /**
     * Traveling from a ndoe to a root and adding a key to that node and incrementing it if it all ready has it
     * the count is used to determine if to nodes share a node on the path to the root. The root will always equal the
     * number of nodes below it.
     * @param {String} key would be "whereCount" or "returnCount"
     * @param {Object} node node of a D3Json Tree
     */
    toCollectionCounting(key, node){
        if(!node.hasOwnProperty(key))   //if node doesn't have key add one
            node[key] = 0;
        node[key]++; 
        if(node.parent != null && node.parent.name !== undefined && node.parent.name != "Collections") // Not root node
        {
            this.toCollectionCounting(key, node.parent);
        }
    }

     /**
     * A function to recursively travel up a tree.
     * @param {Object} node 
     */
    toCollection(node){
        if(node.parent != null){ // Not root node
            let parentsNamesAndNodeName = this.toCollection(node.parent)+"\/"+node.name;
            if(parentsNamesAndNodeName.startsWith("\/"))
                parentsNamesAndNodeName = parentsNamesAndNodeName.substring(1);
            return parentsNamesAndNodeName; //find parent of root add parents name before yours \Deployments\Id\...
        }
        return ""; //The Root Node which is the Collection node
    }

    /**
     * 
     * @param {Object} node node of a D3Json Tree
     *
     * Jeff:  We will probably need to do some work in here to support @any
     */
    getCollectionRootNode(node){  // fixed collections array for canned queries
        if (node.parent != null && node.parent.name != "Collections"){
            let returned = this.getCollectionRootNode(node.parent);
            return (typeof returned == "string")?returned:node.name;
        }
        return node.name;
    }


    getCollection(node){
        if(node.parent != null){ // Not root node
            let returned = this.getCollection(node.parent);
            // find parent of root add parents name before yours \Deployments\Id\...
            return (typeof returned == "string")?returned:node.name;
        }
        return 1;   //The Root Node which is the Collection node
    }


    /**
     * A function to recursively travel up a tree.
     * @param {Object} node 
     */
    toRoot(node){
        if(node.parent != null){ // Not root node
            return this.toRoot(node.parent)+"\/"+node.name; //find parent of root add parents name before yours \Deployments\Id\...
        }
        return node.name;   //The Root Node
    }

    /**
     * A function to recursively travel up a tree.
     * @param {Object} node 
     */
    getRoot(node){
        if(node.parent != null){ // Not root node
            return this.getRoot(node.parent) //find parent of root add parents name before yours \Deployments\Id\...
        }
        return node.name;   //The Root Node
    }

    /**
     * 
     * @param {Object} node 
     * @param {Object} lastFlworNode 
     * @return {String} 
     */
    replacePath(node, lastFlworNode){
        var nodePath = this.toCollection(node);                   //returns full path name
        var lastFlworPath = this.toCollection(lastFlworNode);     //returns full path name
        //console.log("before node: "+nodePath+"\tlastFlower:"+ lastFlworPath);   //used for debugging
        if(nodePath.includes(lastFlworPath)){               //if lastFlworPath is inside nodePath
            var regex = new RegExp(lastFlworPath);          //create a Regular Expression that's the lastFlworPath
            var newPath = this.getCollection(node)+nodePath.replace(regex, lastFlworNode['forVar'].toString());  //Deployment/SamplingDetails changes to $x2/SamplingDetails
            return newPath;
        }
        return "";
    }

    /**
     * Create a XQuery Flwor expression
     * Build an initial clause with a for clause
     * add all the where conditions inside the @var whereArray
     * add all the return conditions inside the @var returnArray
     * 
     * @param {Object} node the current node where a FLWOR needs to be built
     * @param {Object} lastFlworNode the next node above @var node that a FLWOR statement will be created at
     * @param {Array[ULData]} returnArray All the return conditions that'll be added to the return clause
     * @param {Array[ULData]} whereArray All the where conditons that'll be added to the where clause
     * @var {FLOWR} flower Object that's being built inside this function
     * @var {String} this.varPrefix fixed string for an arbitrary variable name
     * @return {String} this.flowr.toString is a string of the xQuery that was built
     */
    buildFlwor(node, lastFlworNode, returnArray, whereArray){
        var flower = new FLWOR("","");
        flower.setInitialClause(this.buildFor(node, lastFlworNode));
        let whereClause = this.buildWhere(node, whereArray);
        if(whereClause)
            flower.addIntermediateClause(whereClause);
        flower.setReturnClause(this.buildReturn(node, returnArray));
        console.log("FLOWER")
        console.log(flower.toString());                 //used for debugging
        return flower.toString();
    }

    buildFor(node, lastFlworNode){
        var forClause = new ForClause("", "");          //Build Initial Clause (For Clause)
        forClause.addForBinding(new ForBinding(this.getCollection(node)+node['forVar'], "$"+this.replacePath(node, lastFlworNode))); //for $x(n+1) in $x(n)/rest/of/path
        return forClause;
    }

    /**
     * A variation of @function buildFlwor. This is the last FLWOR build and will have all other FLWORs created
     * nested with inside of it.
     * @param {Object} node the current node where a FLWOR needs to be built
     * @param {Array[ULData]} returnArray All the return conditions that'll be added to the return clause
     * @var {Array[ULData]} this.whereArray All the where conditons that'll be added to the where clause
     * @var {FLOWR} flower Object that's being built inside this function
     * @var {String} this.varPrefix fixed string for an arbitrary variable name
     * @return {String} complete string od the XQuery
     */
    initialFlwor(node, returnArray, whereArray, isCanned){
        var flower = new FLWOR(this.DEFAULT_HEADER, SubmitQuery.DEFAULT_FOOTER); //header and footer of flwor
        flower.setInitialClause(this.buildInitialFor());
        let whereClause = this.buildWhere(node, whereArray, isCanned); // HERE!!
        //alert(whereClause)
        if(whereClause)
            flower.addIntermediateClause(whereClause);
            //alert(flower.toString())
        flower.setReturnClause(this.buildReturn(node, returnArray));
        return flower.toString();
    }

    initialJoinFlwor(node, returnArray, whereArray){
        var flower = new FLWOR(this.header, this.footer); //header and footer of flwor
        node['forVar'] = 1;
        flower.setInitialClause(this.buildInitialFor());
        whereArray.forEach(element=>{
            flower.addIntermediateClause(element);
        });
        flower.addIntermediateClause(this.getWhereJoins());
        flower.setReturnClause(this.buildReturn(node, returnArray));
        return flower.toString();
    }

    finishFlowr(returnArray, whereArray){
        
        //TODO where join statements
        flower.setReturnClause(new ReturnClause(returnArray));
        return flower.toString();
    }

    buildInitialFor()
    {
        var forClause = new ForClause("", "");
        this.collections.forEach((collection)=>{
            forClause.addForBinding(new ForBinding(collection+"1", this.collectionMap[collection]));
        });
        return forClause;
    }

    buildWhere(node, whereArray, isCanned){
        var whereTemp = [];   //Clone of the Array Indexes instead od refrences
        var Long_or = undefined;
        console.log("how many times does this get called")
        if(whereArray){
            whereArray.forEach(element => {
                whereTemp.push(element);
            });
        }
        var whereExpression = "";   //Intermediate Clause
        whereTemp.forEach(element => {

            if (element.getNode().name == 'Longitude' && element.getConjunction() == "or ") {
                Long_or = true;
                //alert('setting Long_or')
                whereExpression += "( "+ element.toString(this.replacePath(element.getNode(), node), null, this.speciesLibrary, isCanned);
            } else if (!(element.getNode().name == 'Longitude' && element.getConjunction() == " and" && Long_or)) {

                whereExpression += element.toString(this.replacePath(element.getNode(), node), null, this.speciesLibrary, isCanned);
            }

            if (element.getNode().name == 'Longitude' && element.getConjunction() == " and" && Long_or){
                var tmpString = element.toString(this.replacePath(element.getNode(), node), null, this.speciesLibrary, isCanned)
                var position = tmpString.indexOf('and')
                whereExpression += tmpString.slice(0,position) + " ) "+ tmpString.slice(position)
            }

            //else
               //whereExpression += element.toString(this.replacePath(element.getNode(), node), null, this.speciesLibrary);

            //whereExpression += element.toString(replaced, null, this.speciesLibrary); //+ " and "; //add it to the expression
            //whereExpression += element.toString(this.replacePath(element.getNode(), node), null, this.speciesLibrary); //+ " and "; //add it to the expression

        });
        if(whereExpression != ""){
            //console.log("WHERE EXP "+whereExpression)
            whereExpression = whereExpression.endsWith("and ")?whereExpression.substring(0, whereExpression.length - "and ".length):whereExpression;    //chops the "and " at the end
            whereExpression = whereExpression.endsWith("or ")?whereExpression.substring(0, whereExpression.length - "or ".length):whereExpression;
            //whereExpression = "("+whereExpression+")"//remove the "or " at the end
            return new WhereClause(whereExpression);
        }
        return null;
    }

    /**
     * 
     * @param {Object} node 
     * @param {Array[ULData]} returnArray 
     */
    buildReturn(node, returnArray){
        var returnExpression = "";
        //this.recordName = this.recordName+Math.random().toString()
        if(returnArray.length>1 && Array.isArray(returnArray))    //multiple return 
        {                                   //for $x2 in $x1/Sample return <out>{$x2/returnArray[0], $x2/returnArray[1], ...}</out>
            returnExpression = "<"+this.recordName+">{";
            returnArray.forEach(element => {
                returnExpression += this.checkSpecies(element.toString())+ ", ";   
            });
            returnExpression = returnExpression.endsWith(", ")?returnExpression.substring(0, returnExpression.length - ", ".length):returnExpression;    //chops the ", " at the end
            returnExpression += "}</"+this.recordName+">";
        }
        else if(returnArray.length == 0){  //Add the current forVal (i.e. for $x2 in $x1/Sample return $x2)
            returnExpression += "$"+this.getCollection(node)+node['forVar'];
        }
        else{   //for $x2 in $x1/Sample return $x2/returnArray. Array has only one item in it.
            returnExpression += this.checkSpecies(returnArray.toString());   
        }
        return new ReturnClause(returnExpression);
    }

    /**
     * Check return strings for $.../SpeciesId and if a species library is selected. The string needs to be
     * rewritten as lib:tsn2completename($.../SpeciesId) or lib:tsn2abbrev($.../SpeciesId). @see SubmitQuery.regularExpression
     * for the regular expression that needs to be matched.
     * @param {String} str 
     */
    checkSpecies(str){
        let regularExpression = SubmitQuery.regularExpression; //find a $ and a little alphabet,numbers, and / inbetween /SpeciesId
        let temp = str;
        if(regularExpression.test(str) && this.speciesLibrary){  //THIS IS ENCAPSULATION BREAKING BUT IT'S A WORK AROUND TO USE TSN LIBS
            if(this.speciesLibrary == "latin")
                temp = str.replace(regularExpression, 'lib:tsn2completename('+str+')'); 
            else{
                temp = str.replace(regularExpression, 'lib:tsn2abbrev( '+str+' , "'+this.speciesLibrary+'")'); 
            }
        }
        return temp;
    }

    static get regularExpression(){
        return /\$[\w\/]+?\/SpeciesId$/i;
    }

    static get regularExpressionForSpeciesID(){
        return /[\w\/]+?\/SpeciesId$/i;
    }

    static get regularExpressionForEffort(){
        return /[\w\/]+?\/Kind\/SpeciesId$/i;
    }

    /**
     * Create a query to receive a list for autocomplete of values.
     * `import schema namespace ty="http://tethys.sdsu.edu/schema/1.0" at "tethys.xsd";
        <Result xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
        {
        for $var in distinct-values(collection("Deployments")/Deployment/Site)
        order by $var
          return $var
        }
        </Result>`
        
     */
    autocompleteQuery(path){
        let regularExpression_SpeciesID = SubmitQuery.regularExpressionForSpeciesID;
        let regularExpression_SpeciesID_Effort = SubmitQuery.regularExpressionForEffort;
        console.log($('#speciesSelectAdvanced').val());
        if( regularExpression_SpeciesID.test(path) && $('#speciesSelectAdvanced').val()){  //  both SpeciesID's from Effort and OnEffort will meet this condition
            console.log('in here '+$('#speciesSelectAdvanced').val())
            if (regularExpression_SpeciesID_Effort.test(path)) {
                if ($('#speciesSelectAdvanced').val() == "latin") {
                    return 'declare default element namespace "http://tethys.sdsu.edu/schema/1.0";\n' +
                        'import module namespace lib="http://tethys.sdsu.edu/XQueryFns" at "Tethys.xq";\n' +
                        "let $tsns := distinct-values(collection('Detections')/Detections/Effort/Kind/SpeciesId)\n" +
                        "return for $rank in collection('ITIS_ranks')/ranks/rank[tsn=$tsns]\n" +
                        "order by $rank/completename\n" +
                        "   return $rank/completename"
                } else {
                    var query = `
                    declare default element namespace "http://tethys.sdsu.edu/schema/1.0";
                    let $map := collection("SpeciesAbbreviations")/Abbreviations[Name="SIO.SWAL.v1"]/Map
                    for $tsn in distinct-values(collection("Detections")/Detections/Effort/Kind/SpeciesId)
                    return (
                       $map[tsn = $tsn]/coding, 
                       for $grp in distinct-values(collection("Detections")/Detections/Effort/Kind/SpeciesId[. = $tsn]/@Group)
                       return
                          $map[tsn = $tsn and tsn/@Group = $grp]/coding
                       )
                    `
                    return query

                    /*
                    return 'import schema namespace ty="http://tethys.sdsu.edu/schema/1.0" at "tethys.xsd"; \n' +
                        'import module namespace lib="http://tethys.sdsu.edu/XQueryFns" at "Tethys.xq"; \n' +
                        'let $var := element Kind {\n' +
                        '  for $v in distinct-values(collection(\'Detections\')/ty:Detections/Effort/Kind/SpeciesId) \n' +
                        '  return element SpeciesId {$v}\n' +
                        '}\n' +
                        'let $abbr := lib:SpeciesIDtsn2abbrev($var, "' + $('#speciesSelectAdvanced').val() + '")\n' +
                        'for $id in $abbr/SpeciesId \n' +
                        'order by $id\n' +
                        'return $id'
                        */

                }
            } else {
                if ($('#speciesSelectAdvanced').val() == "latin") {
                    return 'declare default element namespace "http://tethys.sdsu.edu/schema/1.0"; ' +
                        'import module namespace lib="http://tethys.sdsu.edu/XQueryFns" at "Tethys.xq"; ' +
                        "let $var := for $v in distinct-values(collection('Detections')/Detections/OnEffort/Detection/SpeciesId) " +
                        "return <tsn>{$v}</tsn> " +
                        "for $tsn in $var " +
                        "return lib:tsn2completename($tsn)"
                } else {
                    /*return 'declare default element namespace "http://tethys.sdsu.edu/schema/1.0"; '+
                    'import module namespace lib="http://tethys.sdsu.edu/XQueryFns" at "Tethys.xq"; '+
                    "let $var := for $v in distinct-values(collection('Detections')/Detections/Effort/Kind/SpeciesId) "+
                    "return <tsn>{$v}</tsn> "+
                    "for $tsn in $var "+
                    'return lib:tsn2abbrev($tsn, "'+$('#speciesSelectAdvanced').val()+'")' */

                    return 'declare default element namespace "http://tethys.sdsu.edu/schema/1.0"; \n' +
                        'import module namespace lib="http://tethys.sdsu.edu/XQueryFns" at "Tethys.xq"; \n' +
                        'let $var := element Kind {\n' +
                        '  for $v in distinct-values(collection(\'Detections\')/Detections/OnEffort/Detection/SpeciesId) \n' +
                        '  return element SpeciesId {$v}\n' +
                        '}\n' +
                        'let $abbr := lib:SpeciesIDtsn2abbrev($var, "' + $('#speciesSelectAdvanced').val() + '")\n' +
                        'for $id in $abbr/SpeciesId \n' +
                        'order by $id\n' +
                        'return $id'

                }
            }
        } else {
            this.determineWhereCollections();
            return SubmitQuery.DEFAULT_SDSU_NAMESPACE+

            'for $var in distinct-values('+this.collectionMap[this.collections[0]]+'\/'+this.whereData[0].getPathWithoutRoot()+')'+
                '\norder by $var return $var';
          }
    }

    /**
     * 
     * @param {D3Json} theTree 
     */
    buildFlatJoinedQuery(theTree){
        let collectionQueries = [];
        let upperWheres = [];
        this.collections.forEach(collection =>{
            let collectionWheres = []; 
            let collectionTree = theTree.findCollection(collection);
            collectionTree['forVar'] = 1;
            this.returnData.forEach(element => {
                if(this.getCollection(collectionTree) ==  this.getCollection(element.getNode()))
                    collectionQueries.push("$"+this.replacePath(element.getNode(), collectionTree));
            });
            this.whereData.forEach(whereItem => {
                if(this.getCollection(collectionTree) ==  this.getCollection(whereItem.getNode()))
                    collectionWheres.push(whereItem);
            });
            upperWheres.push(this.buildWhere(collectionTree, collectionWheres));
        });
        var temp = this.initialJoinFlwor(theTree, collectionQueries, upperWheres) //Builds final FLWOR statement. 
        return temp;
    }

    /**
     * 
     * @param {D3Json} theTree 
     */
    buildFlatQuery(theTree){
        theTree['forVar'] = 1;
        var returnSet = [];                     //clone of returnData as oppose to reference
        this.returnData.forEach(element => {    //all return paths
            returnSet.push("$"+this.replacePath(element.getNode(), theTree));  //format the return arguments 
        }); //all nodes are found
        var whereSet = [];                      //clone of whereData as oppose to reference
        this.whereData.forEach(element => {     //all where paths
            whereSet.push(element);             //cloning
        }); //all nodes are found 
        var temp = this.initialFlwor(theTree, returnSet, whereSet);         //Builds final FLWOR statement.        
        return temp;
    }

    get DEPLOYMENT_AND_CALIBRATIONS_WHERE(){
        return ["$Deployment1/Instrument/ID = $Calibration1/Project"];
    }

    get DEPLOYMENT_AND_DETECTIONS_WHERE(){
        return ["$Deployment1/Id = $Detections1/DataSource/DeploymentId"];
    }

    get DEPLOYMENT_AND_ENSEMBLE_WHERE(){
        return ["$Deployment1/Id = $Ensemble1/Unit/DeploymentId"];
    }

    get DEPLOYMENT_AND_LOCALIZE_WHERE(){
        return ["$Deployment1/Id = $Localize1/DataSource/DeploymentId"];
    }

    get DETECTIONS_AND_ENSEMBLE_WHERE(){
        return ["$Detections1/Datasource/EnsembleId = $Ensemble1/Id"];
    }

    get DETECTIONS_AND_ITIS_WHERE(){
        return ["$Detections1/Effort/Kind/SpeciesId = $ITIS1/Kingdom/Species/tsn"];
    }

    get ITIS_AND_SPECIES_WHERE(){
        return ["$ITIS1/Kingdom/tsn = $Abbreviations1/Map/tsn",
                "$ITIS1/Kingdom/completename = $Abbreviations1/Map/completename"];
    }

    getWhereJoins(){
        let whereClauses = [];
        if(this.collections.includes("Deployment") && this.collections.includes("Calibration"))
            whereClauses.push(this.DEPLOYMENT_AND_CALIBRATIONS_WHERE);
        if(this.collections.includes("Deployment") && this.collections.includes("Detections"))
            whereClauses.push(this.DEPLOYMENT_AND_DETECTIONS_WHERE);
        if(this.collections.includes("Deployment") && this.collections.includes("Ensemble"))
            whereClauses.push(this.DEPLOYMENT_AND_ENSEMBLE_WHERE);
        if(this.collections.includes("Deployment") && this.collections.includes("Localize"))
            whereClauses.push(this.DEPLOYMENT_AND_LOCALIZE_WHERE);
        if(this.collections.includes("Detections") && this.collections.includes("Ensemble"))
            whereClauses.push(this.DETECTIONS_AND_ENSEMBLE_WHERE);
        if(this.collections.includes("Detections") && this.collections.includes("Itis"))
            whereClauses.push(this.DETECTIONS_AND_ITIS_WHERE);
        if(this.collections.includes("Itis") && this.collections.includes("Species"))
            whereClauses.push(this.ITIS_AND_SPECIES_WHERE);
        whereClauses = this.flatten(whereClauses);
        return new WhereClause(whereClauses.join(" and "));
    }




}


