/**
 * @author Daniel Weston
 * @version 1.0
 * DSanchezWeston@gmail.com
 * 28 Aug 2018
 */
"use strict"

/**
 * The view portion of a ULData object. Row Class is going to build a listitem <li> with a path, operatorSelect, value input, conjunction select, and a remove button
 * All rows should be part of a an UnorderedList
 * 
 */

class SimpleRow{

    /**
     * 
     * @param {UnorderedListView} parent parent element an unordered list <ul> that contains this element <li>
     * @param {String} path string of XQuery path Deployment\Project
     * @param {Integer} index index of list item. Used when an event happens and the information needs to be passed up to the controller
     */
    constructor(parent, ulData, index){
        this.parent = parent;
        this.ulData = ulData;
        this.index = index;

        this.listItem;
        this.pathSpan;
        this.removeButton;

        this.listItem = document.createElement("li");
        this.listItem.setAttribute("id" , this.ulData.getId() + index);
        this.listItem.setAttribute("class" , this.ulData.getId() +" list-group-item input-group d-inline-flex mb-1 justify-content-end");
        this.buildRow();    //added here to not double call in Subclasses with this method.
    }

    /**
     * This will call all the functions to build each piece of the complete <li></li>
     * the complete structure should look like:
     * <li><span>Deployment\Project</span><button>X</button></li>
     * @param {String} name 
     * @param {Integer} index 
     * @param {Boolean} disable
     */
    buildRow(){
        this.listItem.innerHTML = "";   //clears element
        this.buildPath();               //builds <span id = "this.columnId['path']+index">path<span>
        this.buildRemoveButton();       //builds <button id="removeButton0" class="removeButton btn btn-sm btn-light btn-outline-dark" value="0">X</button>
        this.listItem.appendChild(this.pathDiv);
        this.listItem.appendChild(this.removeDiv);

    }

    /**
     * 
     * builds <span id = "this.columnId['path']+index class= "input-group-text" data-trigger = "hover" data-content =this.path>path<span>
     * @param {String} path text in the span
     * @param {Integer} index used for a unique id
     */
    buildPath(){
        let _this = this;
        this.pathDiv = document.createElement("div");
        this.pathDiv.setAttribute("class", "input-group-prepend");
        let pathSpan = document.createElement("span");
        pathSpan.setAttribute("id", 'path'+this.index);
        pathSpan.setAttribute("class", "input-group-text ");
        pathSpan.setAttribute("data-trigger", "hover");
        pathSpan.setAttribute("data-content", this.ulData.getPath());
        pathSpan.setAttribute("data-placement","left");
        if(!this.ulData.getMap()) //No map add made the background red. isMap true by default
            pathSpan.setAttribute("class", "input-group-text bg-danger text-dark");
        pathSpan.addEventListener("click", function(){
            _this.parent.toggleBackgroundColor(_this.index);
        });
        pathSpan.appendChild(document.createTextNode(this.shortenPath(this.ulData.getPath())));
        this.pathDiv.appendChild(pathSpan);
    }

    /**
     * Creates the Remove button of the list item
     * <button id="removeButton0" class="removeButton btn btn-sm btn-light btn-outline-dark" value="0">X</button>
     * @param {Integer} index index of list item. Used when an event happens and the information needs to be passed up to the controller
     */
    buildRemoveButton(){
        let _this = this;
        this.removeDiv = document.createElement("div");
        this.removeDiv.setAttribute("class", "input-group-append");
        let removeButton = document.createElement("button");
        removeButton.setAttribute("id", 'removeButton'+this.index);
        removeButton.setAttribute("class","btn btn-sm btn-dark");
        removeButton.setAttribute("value", this.index);
        removeButton.appendChild(document.createTextNode("X"));
        removeButton.addEventListener("click", function(){
                _this.parent.removeRow(_this.index);
        });
        this.removeDiv.appendChild(removeButton);
    }


    regularExpressionForPath(){
        return /[\w\/]+?\/SpeciesId$/i;
    }

    isSpeciesID() {
        var path = this.ulData.getPath();
        var regex = this.regularExpressionForPath();
        if(regex.test(path)) {
           return true
        } else {
            return false
        }
    }
    /*
    Determines if row's path contains SpeciesId, if so checks to see if species library has been
    chosen
    */
    determineTypeOfRequest(){
        // called from click event on search button
        if (this.isSpeciesID()){
            if ($('#speciesSelectAdvanced').val()){
                this.ulData.getAutoArr(this); // asynchronous call, callback calls addAutoCompleteForModal
            } else {
                alert("Please select species library");
            }
        } else {
            this.addAutoCompleteForModal();
            //$('#GetValues').modal('show');
        }
    }

    addAutoCompleteForModal() {
        let _this = this;
        var parent = this.parent;
        var index = this.index;
        var input = this.valueInput;
        var autocompleteData= this.ulData.getAutocompleteArr();
        var combo = new STComboBox();
        combo.Init("GetValueTextInput2");
        var data = [];
        for(var i=0; i < autocompleteData.length; i++) {
            data.push({id: i, text: autocompleteData[i]});
        }
        combo.populateList(this,data);


        $("#GetValueTextInput2-ddi").on("change", function(){
                //updates input value in row, may not need this, look at onSelect below
                var selectedValue = combo.getInput().val();
                input.value = selectedValue;
                _this.parent.updateInputValue(_this.index, selectedValue);
                //alert('on select');
        });
        $('#GetValues').modal('show');
    }

    onSelect(selectedValue){  // called from stcombobox.js for a click on each item
        var input = this.valueInput;
        input.value = selectedValue;
        this.parent.updateInputValue(this.index, selectedValue);
    }

    buildMagGlassButton(){
        let _this = this;
        this.findDiv = document.createElement("div");
        this.findDiv.setAttribute("class", "input-group-append");
        let findButton = document.createElement("button");
        findButton.setAttribute("id", 'findButton'+_this.index);
        findButton.setAttribute("value", _this.index);
        findButton.setAttribute("data-toggle", "tooltip")
        findButton.setAttribute("title", "Search for values")
        let span = document.createElement("span");
        span.setAttribute("class","fa fa-search");
        findButton.appendChild(span);
        //findButton.appendChild(document.createTextNode("Get Values"));
        findButton.addEventListener("click", function(){
                this.magnifyingModal = new ResultModal('GetValues','closeGetValues');
                document.getElementById("pathSpan").innerHTML = _this.shortenPath(_this.ulData.getPath());
                this.magnifyingModal.setBody("<span id = 'GetValueTextInput2'></span>");
                _this.determineTypeOfRequest();
                $("#closeGetValues").on("click",
                        ()=>{
                            $('#GetValues').modal('hide');
                            });
        });
        this.findDiv.appendChild(findButton);
    }




    shortenPath(path)
    {
        var pathArr = path.split("\/");
        return (pathArr.length>2)?(pathArr[pathArr.length-2]+"\/"+pathArr[pathArr.length-1]):path;
        //return path; //debug use only. Turned on when I want to see the whole path name.
    }

    getListItem()
    {
        return this.listItem;
    }

    getRemoveButton()
    {
        return this.removeButton;
    }
}