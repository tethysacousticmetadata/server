/**
 * @author Daniel Weston
 * @version 1.0
 * DSanchezWeston@gmail.com
 * 28 Aug 2018
 */
"use strict";

/**
 * This is a class to act as an object for the id = "whereView" <div> in advancedqueiries.html.
 * This class should house multiple <ul> elements and be grouped accordingly to the query.
 * The View of Model, View, Controller (MVC)
 * @see ULData  are the Model
 * @see UnorderedListView are the views withing this view.
 * @see WhereController is the Controller
 */
class WhereView{
    
    /**
     * Attaches to html element with same id <ul id = "idName"><\ul>
     * Saves parent information to pass up function calls later when inputs are entered.
     * 
     * @param {WhereController} controller   Parent Controller Object
     * @param {String} idName           idName string (i.e whereCondition, returnCondition)
     */
    constructor(controller, id){
        this.controller = controller;
        this.id = id;
        this.unorderedListViews = [];
        this.groupDivView = document.getElementById(this.id);
        if(this.groupDivView == null){
            this.groupDivView = document.createElement("div");
            this.groupDivView.setAttribute("id", "whereView");  //<div id = "whereView">
            document.getElementById("whereTop").appendChild(this.groupDivView);
        }
    }

    /**
     * Clears the <ul> element and rebuilds it with the data object given
     * foreach data object inside of it crate a <li></li> element. If this.simple is true
     * make a SimpleRow else make a Row. Simple rows only have a path. Rows have path, select,
     * input, select and button 
     * 
     * @param {Array[Array[ULData]]} data 
     */
    buildView(groupsData){
        this.groupDivView.innerHTML = "";                 //clear the <div>
        this.unorderedListViews = [];
        for(let j = 0 ; j<groupsData.length; j++){        
            this.unorderedListViews.push(new UnorderedListView(this, "whereUnorderedList", "whereCondition list-group sortable border border-danger rounded", j, groupsData[j]));
        }
        if(groupsData.length == 0)
            this.unorderedListViews.push(new UnorderedListView(this, "whereUnorderedList", "whereCondition list-group sortable border border-danger rounded", 0, []));
        $('[data-trigger="hover"]').popover(); 
        this.assignAutoComplete();
    }

    assignAutoComplete(groupsData){
        //if(groupsData.length === this.unorderedListViews.length){ //Sanity check
            for(let i = 0; i<this.unorderedListViews.length; i++)
            {
                this.unorderedListViews[i].activateAutoComplete();
            }
        //}
    }

    /**
     * @return {HTMLElement} this.groupDivView
     */
    getGroupDivView(){
        return this.groupDivView;
    }

    /**
     * Event call being passed up to parent to modify its data object
     * @param {Integer} index integer of id  
     */
    toggleBackgroundColor(index){
        this.controller.toggleBackgroundColor(index);
    }

    /**
     * Event call being passed up to parent to modify its data object
     * @param {Integer} index 
     */
    removeRow(groupIndex, rowIndex){
        //console.log("id: "+groupIndex+"\trowIndex: "+rowIndex);
        this.controller.removeRow(groupIndex, rowIndex);
    }

    /**
     * Event call being passed up to parent to modify its data object
     * @param {Integer} index   index of row in data
     * @param {String} value    new value of operatorSelect
     */
    updateOperatorSelect(groupIndex, rowIndex, value){
        this.controller.updateOperatorSelect(groupIndex, rowIndex, value);
    }

    /**
     * Event call being passed up to parent to modify its data object
     * @param {Integer} index   index of row in data
     * @param {String} value    new value of InputValue
     */
    updateInputValue(groupIndex, rowIndex, value){
        this.controller.updateInputValue(groupIndex, rowIndex, value);
    }

    /**
     * Event call being passed up to parent to modify its data object
     * @param {Integer} index   index of row in data
     * @param {String} value    new value of conjunctionSelect
     */
    updateConjunctionSelect(groupIndex, rowIndex, value){
        this.controller.updateConjunctionSelect(groupIndex, rowIndex, value);
    }


}