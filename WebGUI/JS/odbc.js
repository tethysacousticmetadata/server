var odbcTemplate = `
<div class="dataTarget border border-secondary px-md-2 py-md-2">
    <div class="row mb-3">
        <div class="col-lg-6 mt-2">

            <div class="form-group">
                <label>Database or File Type</label>
                <i class="fa fa-info-circle fa-md" aria-hidden="true"
                   data-toggle="collapse" href=".helpODBCDriver">
                </i>
                <select class="selectpicker odbctype form-control" data-title="Source Type"
                    data-live-search="true" onchange="selectODBC(this)">
                </select>
            </div>
        </div>
        <div class="col-lg-3 col-sm-6 mt-2">
            <div class="form-group">
                <label>Source Name</label>
                <i class="fa fa-info-circle fa-md" aria-hidden="true"
                   data-toggle="collapse" href=".helpSource"></i>
                <input class="form-control sourceName" type="text">
            </div>
        </div>
        
        <div class="col-1 mt-5 float-right">
          <a class="sourceDelete" onclick='this.closest(".dataTarget").remove()' aria-label="delete data source">
             <i class="fa fa-fw fa-trash mr-2"></i>
          </a>
        </div>

    </div>
    <div class="row">
        <div class="filePath col-lg-12 dbFields">
           <form class="dropzone" action="/invalid-import">
               <div class="dz-message" data-dz-message>
                   <span>
                       File(s) needed to create the document.
                       <br>
                       Drag and drop files or click to choose.
                   </span>
               </div>
           </form>
        </div>
        <div class="mySqlDbFields sqlServerDbFields dbFields serverDb col-lg-4 col-sm-6">
            <div class="form-group">
                <label>Server/Host Name</label>
                <i class="fa fa-info-circle fa-md" aria-hidden="true"
                   onclick="showHideMoreInfo('helpServer')"></i>
                <input class="form-control postgres dbServerValue" type="text">
            </div>
        </div>
        <div class="mySqlDbFields col-lg-4 col-sm-6 dbFields">
            <div class="form-group">
                <label>Port</label>
                <i class="fa fa-info-circle fa-md" aria-hidden="true"
                   onclick="showHideMoreInfo('helpPort')"></i>
                <input class="form-control dbMySqlPortValue" type="text">
            </div>
        </div>
        <div class="mySqlDbFields sqlServerDbFields dbFields serverDb  col-lg-4 col-sm-6">
            <div class="form-group">
                <label>Database Name</label>
                <i class="fa fa-info-circle fa-md" aria-hidden="true"
                   onclick="showHideMoreInfo('helpDatabaseName')"></i>
                <input class="form-control mysql dbDatabaseName" type="text">
            </div>
        </div>
        <div class="mySqlDbFields sqlServerDbFields col-sm-6 dbFields">
            <div class="form-group">
                <label>Username</label>
                <i class="fa fa-info-circle fa-md" aria-hidden="true"
                   onclick="showHideMoreInfo('helpUsername')"></i>
                <input class="form-control mysql dbUsernameValue" type="text">
                <div class="slqServerAuthentication">
                    Authentication:
                    <div class="form-check mt-3">
                        <label class="form-check-label">
                             Windows
                            <input class="form-check-input windowsAuth" type="radio" name="trustedConnection"
                               value="yes">
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            SQL Server
                            <input class="form-check-input sqlAuth" type="radio" name="trustedConnection"
                               value="no" checked>
                        </label>
                    </div>
                </div>
            </div>
        </div>
        <div class="mySqlDbFields sqlServerDbFields col-sm-6 dbFields">
            <div class="form-group">
                <label>
                    Password
                    <i class="fa fa-info-circle fa-md" aria-hidden="true"
                   onclick="showHideMoreInfo('helpPassword')"></i>

                    <div class="input-group passwordGroup">
                        <input class="form-control dbPasswordValue" type="password" data-toggle="password">
                        <div class="input-group-append">
                            <span class="input-group-text"><i class="fa fa-eye-slash passwordToggleBtn"></i></span>
                        </div>
                    </div>
                </label>
            </div>
        </div>
    </div>
</div>
`

function showHideDbFields(dataTarget) {
    // Determine type of ODBC source
    let odbcType = dataTarget.find(".odbctype").selectpicker("val").toLowerCase();

    // Hide fields, then expose the ones we need
    dataTarget.find('.dbFields').hide();
    dataTarget.find('.slqServerAuthentication').hide();

    let allServerDb = dataTarget.find('.serverDb');

    // Remove column classes as might not be used, add back in for used ones later
    allServerDb.removeClass(['col-lg-6', 'col-lg-4'])

    if(odbcType.includes('microsoft access') ||
        odbcType.includes('microsoft excel') ||
        odbcType.includes('xml')) {
        // File products
        dataTarget.find('.filePath').show();
        // Adjust maximum number of files that can be uploaded based on type
        let dzOptions = dataTarget.find(".dropzone")[0].dropzone.options;
        if (odbcType.includes("text driver"))
            dzOptions.maxFiles = null;
        else
            dzOptions.maxFiles = 1;
    }
    else if(odbcType.includes('sql server')) {
        // Microsoft SQL Server
        dataTarget.find('.sqlServerDbFields').show();
        dataTarget.find('.slqServerAuthentication').show();
        allServerDb.addClass('col-lg-6');
    }
    else if(odbcType.includes('mysql odbc')) {
        // Oracle MySQL  https://www.mysql.com/
        dataTarget.find('.mySqlDbFields').show();
        allServerDb.addClass('col-lg-4');
        dataTarget.find('.dbMySqlPortValue').val('3306');
    }
    else if(odbcType.includes('postgresql')) {
        // PostgreSQL (Postgres)  https://www.postgresql.org/
        dataTarget.find('.mySqlDbFields').show();
        allServerDb.addClass('col-lg-4');
        dataTarget.find('.dbMySqlPortValue').val('5432');
    }
}

/**
 * Update ODBC fields for current ODBC type
 * @param elem
 */
function selectODBC(elem) {
    let jqOdbc = $(elem)
    let driver = jqOdbc.val();
    let dataTarget = jqOdbc.parents(".dataTarget");
    showHideDbFields(dataTarget);
    }

/**
 * Given a JavaQuery handle to an ODBC data target, populate an object with information
 * related to using the ODBC source.
 * @param jqDataTarget - ODBC data target JavaQuery handle
 * @return {{}} - object with ODBC fields
 */
function getODBC(jqDataTarget) {
    let options = {};

    options.odbc = {};

    options.odbc.driverName = jqDataTarget.find(".odbctype.selectpicker").val(); // connection type
    // case-independent version to determine connection-specific fields
    let odbcType = options.odbc.driverName.toLowerCase();

    options.odbc.sourceName = jqDataTarget.find(".sourceName").val();
    if (options.odbc.sourceName == "")
        options.odbc.sourceName = "default-source";

    // Populate other fields based on connection type
    if(odbcType.includes('microsoft access') ||
        odbcType.includes('microsoft excel') ||
        odbcType.includes('xml')) {
        // File products
        options.type = "file";
        // This may not always be useful as people may include other files that we do
        // not want to be the target of the ODBC.  It will likely need to be overwritten
        // after further processing.
        options.odbc.dbFileName = jqDataTarget.find('.filePath').val();
    } else {
        // Network resource products
        options.type = "resource";
        options.odbc.server = jqDataTarget.find(".dbServerValue").val();
        options.odbc.port = jqDataTarget.find(".dbMySqlPortValue").val();
        options.odbc.dbName = jqDataTarget.find(".dbDatabaseName").val();
        options.odbc.username = jqDataTarget.find(".dbUsernameValue").val();
        options.odbc.password = jqDataTarget.find(".dbPasswordValue").val();
        if (odbcType.includes("sql server")) {
            options.odbc.trustedConnection = jqDataTarget.find(".windowsAuth").val();
        }

    }
    return options;
}

/**
 * Populate the odbc section of an import source specification.
 * Adds appropriate fields to the ODBC section of the a <source> element, e.g.
 * <source>
 *     ... other elements ...
 *     <odbc>
 *         <driverName>MySQL Driver</driver>
 *         <server>tethys.domain.gov</server>
 *         <port>3389</port>
 *         <dbName>DataLoggers</dbName>
 *         <username>JoeCool</username>
 *         <password>CharlieBrown</password>
 *     </odbc>
 * </source>
 * @param options - Object containing open database connection fields.  Each attribute
 *   is converted into one of the <odbc> fields, with the value being the field contents
 * @param sourceXml - xmlbuilder2 handle to <source> element
 */
function addXmlOdbc(options, sourceXml) {
    let odbc = sourceXml.ele("odbc");
    let keys = Object.keys(options.odbc);
    for (let kidx=0; kidx < keys.length; kidx++) {
        odbc.ele(keys[kidx]).txt(options.odbc[keys[kidx]]);
    }
}

/**
 * Determine server ODBC sources and populate the ODBC dropdown menu.
 * @param jqODBC - selectpicker to populate
 * @param driver - if not null, selection value
 */
function populateODBC(jqODBC, driver) {
    // Query Tethys server for the ODBC sources and then populate with callback
    getODBCSources(odbc => {
        //let jqODBC = $(".odbctype.selectpicker");

        // remove existing options
        jqODBC.find("option:not('.bs-title-option')").remove()
        let sources = odbc.split("\n");
        sources.push("Tethys-compliant XML");
        for (let s of sources) {
            jqODBC.append(`<option value="${s}">${s}</option>`)
        }
        jqODBC.selectpicker("refresh")
        if (driver != null) {
            let selected = null;
            let jqDrivers = jqODBC.find("option");
            let driverLC = driver.toLowerCase();
            // Find an entry that contains the specified driver (case-insensitive)
            let matches = jqDrivers.filter(function(item) {
               let value = $(this).attr("value");
               let result = false;
               if (value)
                   result = value.toLowerCase().indexOf(driverLC) !== -1;
               return result;
            });
            if (matches.length > 0) {
                if (matches.length == 1)
                    selected = matches.attr("value");
                else {
                    // Pick one with unicode if available, otherwise first one
                    // Note that JQuery filter does not support the case insensitive flag
                    // so we match [uU]nicode with nicode and hope the rest is lower case
                    let uni = matches.filter('[value*="nicode"]');
                    if (uni.length > 0)
                        selected = uni.attr("value");
                    else
                        selected = matches.attr("value")
                }
            }
            if (selected)
                jqODBC.selectpicker("val", selected);
        }
        showHideDbFields(jqODBC.parents(".dataTarget"))
    });
}

/**
 * Add a file/ODBC data source form.  If the source name already exists, we do not add it.
 * @param sourcediv - Parent of source list (JQuery object)
 * @param source - Dictionary, might have keys:
 *     OpenDatabaseConnectivity - ODBC type.  Examples:  Microsoft Excel, MySQL
 *     source - Source name, the source name is only relevant if the SQL queries
 *       within the sourcemap refer to the source label.  This allows us to query
 *       from multiple sources submitted at the same time, in most cases this is not
 *       relevant as there is only a single source.  When there are multiple sources,
 *       only one is specified here as addSource adds one new source.
 * @param allow_duplicates - Add a source even if it duplicates an existing one.  Default is false
 *      We cannot have two sources with the same source name, but we might want to allow
 *      a user to temporarily declare a copy before submitting the data to the server.
 *      (Hence, the default is false as this is usually not a good idea.)
 */
function addSource(sourcediv, source, allow_duplicates) {

    let newsource = true;
    if (source.source) {
        // source name has been specified, make sure that it is not already here
        let exists = sourcediv.find(".sourceName").filter(
            function () {
                return $(this).val() == source.source
            })
        newsource = exists.length == 0;
    }

    if (allow_duplicates == null)
        allow_duplicates = false;

    if (newsource || allow_duplicates) {
        // Add data source dialog
        sourcediv.append(odbcTemplate)
        // Find newly added element
        let dataTarget = sourcediv.find(".dataTarget:last-child")
        // Initialize any callbacks
        addPasswordToggleFunctionality(dataTarget)
        dataTarget.find(".dropzone").dropzone({
            "autoProcessQueue": false,
            "addRemoveLinks": true,
            "url": "/invalid_url",
            "maxfilesexceeded": function(file) {
                this.removeAllFiles();
                this.addFile(file);
            },
            "dictRemoveFile": "×"
        });
        // Add file acceptance filter
        let dz = dataTarget.find(".dropzone")[0].dropzone;
        dropzoneFileFilter(dz);

        // Populate what we know about the data target
        if (source.source) {
            dataTarget.find(".sourceName").val(source.source)
        }

        let odbc = dataTarget.find(".odbctype.selectpicker");
        odbc.selectpicker('refresh');  // initialize
        // The ODBC type can be specified in one of two ways
        // Either by source.odbc or source.OpenDatabaseConnectivity
        odbctype = null
        if (source.odbc)
            odbctype = source.odbc;
        if (source.OpenDatabaseConnectivity)
            // Source Map specification, takes precedence if both present
            odbctype = source.OpenDatabaseConnectivity;

        if (odbctype) {
            populateODBC(odbc, odbctype)
        }
    }
}

/**
 * Add callbacks to enable password visibility toggle.
 * @param form - Current ODBC group
 */
function addPasswordToggleFunctionality(form) {

    let passwordGroups = form.find(".passwordGroup");
    if (passwordGroups.length > 0) {
        for (let idx=0; idx < passwordGroups.length; idx++) {
            let group = passwordGroups.eq(idx);
            let btn = group.find(".input-group-text");

            btn.on('click', (event) => {
                // Find the set of controls associated with the password
                let group = $(event.currentTarget).closest(".passwordGroup");
                let icon = group.find(".fa");
                let form = group.find(".dbPasswordValue");

                // Swap hide/show & visibility indicator
                let typePassword = "password";
                let typeText = "text";
                let visible = "fa-eye";
                let obscured = "fa-eye-slash";
                if (form.attr("type") == typePassword) {
                    form.attr("type", typeText);
                    icon.removeClass(obscured).addClass(visible)
                } else {
                    form.attr("type", typePassword);
                    icon.removeClass(visible).addClass(obscured)
                }
            });
        }
    }

}
