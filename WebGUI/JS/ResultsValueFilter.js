/**
 * @author Daniel Weston
 * @version 1.0
 * DSanchezWeston@gmail.com
 * With the help of Professors Roger Whitney and Marie Roch
 * 25 Nov 2018
 */


/**
 * class that takes a xml JSON object and converts it into a D3 JSON object.
 */
class ResultsValueFilter{

    /**
     * The results returned from the server for the xQuery
     * @param {Object} HTMLDoc the returned results from the xQuery in the form of a HTML document Object
     * @param {Array[String]} nodeNames An array of strings of the name of the nameNode within HTMLDoc that
     *                          need to be added as a title to the GoogleMap marker
     * @var {Array[Object]} userWantedNodes all formatted to become GoogleMap markers
     */
    constructor(responseString){
        this.distinctValues = [];
        this.highestLevelResults(responseString);
        //this.highestLevelResults(this.StringToXML(responseString));
    }

    /**
     * @param {HTMLDoc} treeNode HTML Doc of XML results
     * @var treeNode{
     *      ... : ...
     *      childNodes: [
     *                      0:{
     *                          ... : ...
     *                          childNodes: [
     *                                          0:{
     *                                              ... : ...
     *                                              data: "ARTIC Aleut Antarc BM BRINS ..."
     *                                              ... : ...    
     *                                          }
     *                                      ]
     *                          ... : ...
     *                      }
     *                  ]
     *      ... : ...
     *          }
     */
    highestLevelResults(treeNode){
        this.distinctValues = treeNode.split("\n");
        /*if(treeNode.childNodes[0].childNodes[0])    //in any results were returned
        {
            let resultsText = treeNode.childNodes[0].childNodes[0].data;
            console.log(resultsText);
            if(resultsText)
                this.distinctValues = resultsText.split(" ");
        }*/
    }

    /**
     * @return {Array[String]} this.distinctValues
     */
    getDistinctValues(){
        return this.distinctValues;
    }

    /**
     * Takes an XQuery result as a string and transforms it into an HTMLDoc that has a hierarchy
     * of nodes for each xml level.
     * @param {String} oString
     * @return {HTMLDoc} 
     */
    StringToXML(oString) {
        //code for IE
        if (window.ActiveXObject) { 
            var oXML = new ActiveXObject("Microsoft.XMLDOM"); oXML.loadXML(oString);
            return oXML;
        }
        // code for Chrome, Safari, Firefox, Opera, etc. 
        else {
            return (new DOMParser()).parseFromString(oString, "text/xml");
        }
    }
}