/**
 * @author Daniel Weston
 * @version 1.0
 * DSanchezWeston@gmail.com
 * 28 Aug 2018
 */

'use strict';

/**
 * Currently as is this is not being used. This was a class idea for teh D3 zoom function, but winded up being uneccesary.
 * Check the link below for more information.
 * https://github.com/d3/d3-3.x-api-reference/blob/master/Zoom-Behavior.md
 */
class ZoomController{
    constructor(){
        this.margin = 40;
        this.point = {
            x: 0,
            y: 0,
        }
        this.oldTranslate = {
            x: 0,
            y: 0
        }
        this.zoomScale = 1;
        this.oldScale = 0;
    }

    static get minZoom(){
        return 0.5;
    }

    static get maxZoom(){
        return 3;
    }

    validateScale(newZoomScale){
        //console.log("new"+newZoomScale)
        var zMove = (newZoomScale - this.oldScale)/1000;
        this.oldScale = newZoomScale;
        this.zoomScale += zMove;
        if(this.zoomScale<=this.min){
            this.zoomScale = this.min;
        }
        else if(this.zoomScale>=this.max){
            this.zoomScale = this.max;
        }
        return this.zoomScale;
    }

    /**
     * 
     * @param {Array} translate is an Array of length 2, with a x coordinate at [0] and a y at [1]
     * @param {Object} treeBounds has 4 keys xMin, xMax, yMix, and yMax
     * @param {*} elementWidth width of svg element
     * @param {*} elementHeight height of svg element
     */
    validateTranslate(translate, treeBounds, elementWidth, elementHeight)
    {
        var xMove = translate[0] - this.oldTranslate['x'];
        var yMove = translate[1] - this.oldTranslate['y'];
        this.oldTranslate['x'] = translate[0];
        this.oldTranslate['y'] = translate [1];
        this.point['x'] += xMove;
        this.point['y'] += yMove; 
                                          //would go well beyond the bounds, but if they wanted to move in the 
        //console.log("point.x: "+this.point['x']+"\tpoint.y: "+this.point['y']+"\twidth: "+elementWidth+"\theight: "+elementHeight);
        var treeWidth = (treeBounds['xMax'] - treeBounds['xMin']);
        var treeHeight = (treeBounds['yMax'] - treeBounds['yMin']);
        if(this.point['x'] <  0.5 *treeWidth - this.margin){    //x coor ~800 - half of tree width < 0 - margin => x coor < half of tree width - margin
            this.point['x'] = 0.5 *treeWidth - this.margin;
        }
        if(this.point['x'] +0.5*treeWidth >  elementWidth + this.margin){ //x coor ~800 + half of tree width < tree width + margin => x coor < half of tree width + margin
            this.point['x'] = elementWidth  + this.margin - 0.5*treeWidth;
        }
        if(this.point['y']< 0 - this.margin){    //y
            this.point['y'] = -this.margin;
        }
        if(this.point['y'] + treeHeight> elementHeight + this.margin){
            this.point['y'] = elementHeight + this.margin - treeHeight;
        }
        return [this.point['x'], this.point['y']];
    }
}