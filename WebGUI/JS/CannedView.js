 /** 
 * @author Daniel Weston
 * @version 1.0
 * DSanchezWeston@gmail.com
 * Created on: 5th October 2018
 */

"use strict"

/**
 * @classdesc the V of MVC for cannedQueries.html
 * a class built to control all the HTMLElements od the cannedqueries.html web page
 */
class CannedView{

    constructor(){
       this.speciesInput = document.getElementById('speciesInput');
       this.minLong = document.getElementById('minLong');
       this.maxLong = document.getElementById('maxLong');
       this.minLat = document.getElementById('minLat');
       this.maxLat = document.getElementById('maxLat');
       this.speciesSelect = document.getElementById('speciesSelect');
    }

    /**
     * Update the LongLat input boxes on CannedQueries.html
     * @param {Object} data HTML input elements
     */
    updateLongLat(data){

        this.minLong.value = data.deploymentAndDetections.longitude['min'].toFixed(2);
        this.maxLong.value = data.deploymentAndDetections.longitude['max'].toFixed(2);
        this.minLat.value = data.deploymentAndDetections.latitude['min'].toFixed(2);
        this.maxLat.value = data.deploymentAndDetections.latitude['max'].toFixed(2);
    }

    addOptions(options){
        for(let i=0; i<options.length; i++)
        {
            let opt = document.createElement("option");
            opt.setAttribute("value", options[i].innerHTML);
            opt.appendChild(document.createTextNode(options[i].innerHTML));
            this.speciesSelect.appendChild(opt);
        }
    }



}