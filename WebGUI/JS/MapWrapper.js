/**
 * @author Daniel Weston
 * @version 1.0
 * DSanchezWeston@gmail.com
 * Following code was taken and modified from
 * https://developers.google.com/maps/documentation/javascript/examples/marker-remove
 */
"use strict"

class MapWrapper{

    constructor(controller,mapElementID){
        var myLatLng = {lat: 20, lng: -20};    //Start at latitutde 0 and longitude 0
        this.mapOptions = {
           // restriction: {
           //     latLngBounds: {north: 10, south: -10, west: -18, east: 18},
           //     strictBounds: false
           // },
            mapId: mapElementID,
            zoom: 1,
            minZoom: 2,
            center: myLatLng,               
            mapTypeId: 'satellite'          //Start in satellite view
        }
        this.map = new google.maps.Map(document.getElementById(mapElementID), this.mapOptions);
        this.markers = [];                  //markers that'll be added if coordiantes given in addMarkers()
        this.toDelete = [];
        this.controller = controller;
        this.square = new google.maps.Polygon({
            paths: [
                {lat: 0, lng: 0},
                {lat: 0, lng: 0},
                {lat: 0, lng: 0},
                {lat: 0, lng: 0}
              ],
            strokeColor: '#FF0000', //parameters of the red long lat box
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: '#FF0000',
            fillOpacity: 0.05
          });

        this.map.addListener('bounds_changed', ()=>{this.reportBounds()});

        // Set up a document object model parser
        if (window.ActiveXObject)
            this.oXML = new ActiveXObject("Microsoft.XMLDOM");
        else {
            this.oXML = new DOMParser();
        }
    }

    paddedBounds(npad, spad, epad, wpad) {
        var SW = this.map.getBounds().getSouthWest();
        var NE = this.map.getBounds().getNorthEast();
        var topRight = this.map.getProjection().fromLatLngToPoint(NE);
        var bottomLeft = this.map.getProjection().fromLatLngToPoint(SW);
        var scale = Math.pow(2, this.map.getZoom());

        var SWtopoint = this.map.getProjection().fromLatLngToPoint(SW);
        var SWpoint = new google.maps.Point(((SWtopoint.x - bottomLeft.x) * scale) + wpad, ((SWtopoint.y - topRight.y) * scale) - spad);
        var SWworld = new google.maps.Point(SWpoint.x / scale + bottomLeft.x, SWpoint.y / scale + topRight.y);
        var pt1 = this.map.getProjection().fromPointToLatLng(SWworld);

        var NEtopoint = this.map.getProjection().fromLatLngToPoint(NE);
        var NEpoint = new google.maps.Point(((NEtopoint.x - bottomLeft.x) * scale) - epad, ((NEtopoint.y - topRight.y) * scale) + npad);
        var NEworld = new google.maps.Point(NEpoint.x / scale + bottomLeft.x, NEpoint.y / scale + topRight.y);
        var pt2 = this.map.getProjection().fromPointToLatLng(NEworld);

        return new google.maps.LatLngBounds(pt1, pt2);
    }

    reportBounds(){

        if ($('#bboxButton').attr("class") == 'on'){
            var querySpecs = this.controller.formData
            if ($('#bbox_lock').prop('checked')) {
                    var newRect =  new google.maps.LatLngBounds({lat: querySpecs.deploymentAndDetections.latitude.min, lng: querySpecs.deploymentAndDetections.longitude.min},
                                                               {lat: querySpecs.deploymentAndDetections.latitude.max, lng: querySpecs.deploymentAndDetections.longitude.max})
                } else {
                    var newRect = this.paddedBounds(140, 140, 300, 300)
                }
                this.square.setBounds(newRect);
                this.controller.mapBoxChangedPassingUpLongLat(this.square.getBounds().getNorthEast(), this.square.getBounds().getSouthWest());

        }
    }

    deletePolylines(){
        for (var i=0;i<this.toDelete.length;i++){
            this.toDelete[i].setMap(null);
        }
        this.toDelete = [];
    }

    constructMapObjs(lats,longs){
        if (lats.length == longs.length){
            var trackCoords = [];
            for (var i=0;i<lats.length;i++){
                trackCoords.push({lat: parseFloat(lats[i]), lng: parseFloat(longs[i])})
            }
            var track = new google.maps.Polyline({

                path: trackCoords,
                geodesic: true,
                strokeColor:  '#'+(Math.random()*0xFFFFFF<<0).toString(16), //'#FF0000',
                strokeOpacity: 1.0,
                strokeWeight: 2
            });
            this.toDelete.push(track)
            track.setMap(this.map);

            var infoWindow = new google.maps.InfoWindow();
                google.maps.event.addListener(track, 'mouseover', function(e) {
                infoWindow.setPosition(e.latLng);
                infoWindow.setContent("location: " + e.latLng);
                infoWindow.open(this.map);
            });

            google.maps.event.addListener(track, 'mouseout', function() {
            infoWindow.close();
            });
        }

    }

    /**
     * Adds markers to @var this.map
     * @param {Array[Array]} locations An array of two length arrays holding the Longitude and Latitude in that order
     * @see addMarker which adds each Longlat individually
     * @todo data validation of locations array. Logitude's range -180 to 180 and Latitude's range -90 to 90.
     */
    addMarkers(preFormattedMarkers){

        for (let i = 0; i < preFormattedMarkers.length; i++) {
            this.addMarker(preFormattedMarkers[i]);
        }
        var mcOptions = {
            map: this.map,
            markers: this.markers,
            gridSize: 50,
            maxZoom: 15,
            minimumClusterSize: 10,
            imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m'
        };

        this.markerCluster = new markerClusterer.MarkerClusterer(mcOptions);
    }

    /***
     updating text associated with a cluster
     // Marker clusterer
const yourCluster = new MarkerClusterer({
    map: yourMap,
    markers: yourMarkers, // yourMarkers.slice(0, 3) if you want to have multiple clusters each with a different custom icon on the same map (e.g., set a clusterer for the first 3 markers)
    renderer: {
        render: ({ markers, _position: position }) => {
            return new google.maps.Marker({
                position: {
                    lat: position.lat(),
                    lng: position.lng(),
                },
                label: {
                    text: String(markers.length),
                    color: "white",
                },
                icon: "/path/to/your/cluster-icon-1.png",
            });
        },
    },
});
     */
    addMarkersLocations(preFormattedMarkers){
        for (let i = 0; i < preFormattedMarkers.length; i++) {
            this.addMarkerLocations(preFormattedMarkers[i]);
        }
        let mcOptions = {
            gridSize: 50,
            maxZoom: 15,
            imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m'
         };
        let markerCluster = new markerClusterer.MarkerClusterer(this.map, this.markers,
            mcOptions);
    }



    /**
     * Adding a single marker to the Google Map @var this.map
     * @param {Array} location tuple number array Longitude is [0] and Laitude is [1]
     * @todo data validation of locations array. Logitude's range -180 to 180 and Latitude's range -90 to 90.
     */
    addMarker(preFormattedMarker){
        let _this = this;
        // lat/long object, mapping longitude from [0, 360) to (-180,180]
        let lng = preFormattedMarker.pos.lng;
        if (preFormattedMarker.pos.lng > 180)
            lng = lng - 360;
        let latlng =  new google.maps.LatLng(preFormattedMarker.pos.lat, lng);
        this.markers.push(new google.maps.marker.AdvancedMarkerElement({
            map: this.map,
            title: _this.formatXml2(preFormattedMarker.title),
            position: latlng //function takes Latitude first then Longitude
            }));  //add to the rest of markers

           /* this.markers.push(new google.maps.Marker({
            title: _this.formatXml2(preFormattedMarker.title),
            position: new google.maps.LatLng(preFormattedMarker.pos.lat, preFormattedMarker.pos.lng), //function takes Latitude first then Longitude
            map: this.map}));  //add to the rest of markers */

    }

    addMarkerLocations(preFormattedMarker){
        //alert('is it adding individual markers? ' + preFormattedMarker[0]);
        let _this = this;
        this.markers.push(new google.maps.Marker({
            //title: _this.formatXml2(preFormattedMarker.title),
            position: new google.maps.LatLng(preFormattedMarker[1], preFormattedMarker[0]) //function takes Latitude first then Longitude
            }));  //add to the rest of markers

           /* this.markers.push(new google.maps.Marker({
            title: _this.formatXml2(preFormattedMarker.title),
            position: new google.maps.LatLng(preFormattedMarker.pos.lat, preFormattedMarker.pos.lng), //function takes Latitude first then Longitude
            map: this.map}));  //add to the rest of markers */

    }

    /**
     * show all markers on @var this.map. @see setMapOnAll
     */
    showMarkers() {
        this.setMapOnAll(this.map);
    }

    /** 
     * called from @see showMarkers and @see clearMarkers adds markers to @param map give and clears markers if the
     * value of @param map is null 
     */
    setMapOnAll(map){
        for (var i = 0; i < this.markers.length; i++) {
            this.markers[i].setMap(map);
        }
        //this.square.setMap(map);
    }
    
    /**
     * By sending a null it clears all markers.
     */
    clearMarkers(){

        this.setMapOnAll(null);
    }

    clearMap() {
        /**
         * Remove any polylines and markers on the map
         */
        this.deletePolylines();
        this.deleteMarkers()
    }
    /**
     * @see clearMarkers
     */
    deleteMarkers() {
        this.clearMarkers();
        if (this.markerCluster)
            this.markerCluster.clearMarkers();
        this.markers = [];
    }

    removeSquare(){
    this.square.setMap(null);
    }


    drawSquare(querySpecs){
        if ($('#bbox_lock').prop('checked')) {
            var box = {
                north: querySpecs.deploymentAndDetections.latitude.max,
                south: querySpecs.deploymentAndDetections.latitude.min,
                east: MapWrapper.threeSixtyToOneEighty(querySpecs.deploymentAndDetections.longitude.max),
                west: MapWrapper.threeSixtyToOneEighty(querySpecs.deploymentAndDetections.longitude.min),
            };
        } else {
            // get bounds of current map instead of prelim form based coordinates ??
            var newRect = this.paddedBounds(140, 140, 300, 300)
            var NE = newRect.getNorthEast()
            var SW = newRect.getSouthWest()
            var box = {
                north: NE.lat(),
                south: SW.lat(),
                east: NE.lng(),
                west: SW.lng(),
            };
        }

          this.square = new google.maps.Rectangle({
            bounds: box,
            strokeColor: '#FF0000',
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: '#FF0',
            fillOpacity: 0.1,
            editable: true,
            draggable: true
          });
          this.square.setMap(this.map);
          this.controller.mapBoxChangedPassingUpLongLat(this.square.getBounds().getNorthEast(), this.square.getBounds().getSouthWest())
          this.square.addListener('bounds_changed', ()=>{this.showNewRect()});
    }

    /**
     * This function is used to convert logitude from 0 to 360 range to -180 to 180 range.
     * Tethyes uses 360 and GoogleMaps uses -180.
     * @see PreBuiltQueries.SpeciesLocation()
     * @param {Number} input 
     */
    static threeSixtyToOneEighty(input){
        console.log("lat: "+((input+180)%360)-180);
        return ((input+180)%360)-180;
    }

    /**
     * This function is used to convert logitude from -180 to 180 range to 0 to 360 range.
     * Tethyes uses 360 and GoogleMaps uses -180.
     * @see PreBuiltQueries.SpeciesLocation()
     * @param {Number} input 
     */
    static oneEightyToThreeSixty(input){
        console.log("lat "+input+": "+MapWrapper.mod(Number(input),360));
        return MapWrapper.mod(Number(input),360);
    }

    /**
     * Event called when the red box on the Google map gets adjusted.
     * https://developers.google.com/maps/documentation/javascript/examples/rectangle-event
     */
    showNewRect(){
        this.controller.mapBoxChangedPassingUpLongLat(this.square.getBounds().getNorthEast(), this.square.getBounds().getSouthWest())
    }

    /**
     * Used to fix the modulo function. JS has a bug and this is a workaround.
     * https://stackoverflow.com/questions/4467539/javascript-modulo-gives-a-negative-result-for-negative-numbers
     * @param {Number} n 
     * @param {Number} m 
     */
    static mod(n,m){
       return ((n % m) + m) % m;
    }

    formatXml2(xml) {
        var formatted = '';
        var reg = /(>)(<)(\/*)/g;
        xml = xml.replace(reg, '$1\r\n$2$3');
        var pad = 0;
        jQuery.each(xml.split('\r\n'), function(index, node) {
            var indent = 0;
            if (node.match( /.+<\/\w[^>]*>$/ )) {
                indent = 0;
            } else if (node.match( /^<\/\w/ )) {
                if (pad != 0) {
                    pad -= 1;
                }
            } else if (node.match( /^<\w[^>]*[^\/]>.*$/ )) {
                indent = 1;
            } else {
                indent = 0;
            }

            var padding = '';
            for (var i = 0; i < pad; i++) {
                padding += '  ';
            }

            formatted += padding + node + '\r\n';
            pad += indent;
        });
        return formatted;
    }

    /**
     * Given XML results with WGS84 tracks, plot the tracks on the current map
     * Expects Result/Record/Localize/Localization/Track/WGS84/Coordinates
     * with children Longitude & Latitude
     * It is possible to generate results in the advanced map interface that do not
     * conform to this.
     * @param xmlText - localizations in an XML string
     * @returns boolean - True if we plotted any Tracks
     */
    constructTracks(xmlText){
        let found_tracks = false;  // None so far...

        // Create document object model of XML and find WGS84 tracks
        let dom = this.oXML.parseFromString(xmlText, 'text/xml');

        // Look for paths where Tracks could be, will be one of these
        // We don't try to catch everything, if someone returns beneath the Localization level
        // we will not catch it.
        for (const prefix of [
            "./Result/Localize",
            "./Result/Record/Localize",
            "./Result/Localize/Localizations",
            "./Result/Localize/Localizations",
            "./Result/",
            ])
        {
            let path = prefix + "/Localization/Track/WGS84/Coordinates";

            let iter = dom.evaluate(path,
                dom, // Root node within dom to search
            null, // resolver
                XPathResult.ORDERED_NODE_ITERATOR_TYPE,
                null);  // XPathResult object to use (create if null)

            let coord = null;
            let more = true;
            while (more) {
                try {
                    coord = iter.iterateNext()  // get next coord or null
                    more = coord != null;  // last Next returns null, see if more to find

                } catch (e) {
                    console.error(e);
                    more = false;
                }
                // Should not need to check for valid data as invalid data should not
                // have been admitted to the database
                if (coord != null) {
                    found_tracks = true;  // Note that we found 1+ tracks

                    let long = coord.getElementsByTagName('Longitude');
                    let longs_str = long[0].textContent;
                    let longs =  longs_str.split(/\s+/)

                    let lat = coord.getElementsByTagName('Latitude');
                    let lats_str = lat[0].textContent;
                    let lats =  lats_str.split(/\s+/)

                    this.constructMapObjs(lats,longs);
                }
            }
            if (found_tracks) {
                break;  // No need to check other paths
            }
        }
        return found_tracks;
    }
}