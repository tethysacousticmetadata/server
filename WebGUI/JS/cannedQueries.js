/**
 * @author Daniel Weston
 * @version 1.0
 * DSanchezWeston@gmail.com
 * 3 Oct 2018
 */

"use strict"
/**
 * @classdesc the C in MVC of cannedQueries.html. Class is the top of the hierarchy that controls the map
 * and red box on the map.
 */

class CannedQueries{

    constructor(){
       this.map = null;  /* initialized with map wrapper once map is constructed */
       this.bodyMain = document.getElementById("main");
       this.formData = new CannedData();
       this.view = new CannedView();
       this.modal = new ResultModal('simpleModal','closeSimple');
       this.plotModal = new ResultModal('plotModal','closePlot');
       this.resultsView = document.getElementById("resultTextAreaSimple");
       this.matlabCheckBox = document.getElementById("matlabCheckBox");
       this.bboxButton = document.getElementById("bboxButton");
       this.trees = new D3Json();
       this.currentSchema;
       this.unloaded = [];
       this.returnController = new ReturnController(this, "returnCondition");
       this.convertURL = '';
       this.GroupLookup = {};
       // When non-null contains FormData for the successful query constructed with this class
       this.last_query_completed = null;
       // When non-null contains FormData for latest query attempt.
       this.last_query_attempt = null;
    }

    static get SIDEBAR_OPEN_DEFAULT_WIDTH() {
        return "300px";
        //return "100%";
    }

    static get SIDEBAR_CLOSED_DEFAULT_WIDTH() {
        return "50px";
        //return "5%";
    }

    build_conditions(cond_list) {
        /**
         * Given a list of XPATH conditions, build up a string [cond_list[0] and cond_list[1] and ...]
         * Returns "" if cond_list is empty
         */
        var s = "";
        if (! (cond_list === undefined) && cond_list.length > 0) {
            s = "[" + cond_list.join(" and ") + "]"
        }
        return s
    }

    /**
     * The start of it all. When the webpage loads it creates and instance of Advanced queries and call this function
     * onReady
     */
    main()
    {


        var _this = this;

        $('#refineQueryCombo').on('change', function(){
            var valueSelected = this.value;
            if (valueSelected != '')
                advancedQueries.refineQuery(valueSelected);
                $("#refineQueryCombo").val('')
        })

        $("#saveAsComboSimple").on('change', function(e){
            _this.saveAs(this.value);
        });

        //$('#saveQueryBtn').on('click', function() {
        //    _this.saveQueryJSON();
        //});
        //.....................................................................
        $('#GetSpeciesValues').on('click', function(){
            var btnValue = this.value;
            _this.openValuesModal(btnValue,'simple')
        });
        $('#GetProjectValues').on('click', function(){
            var btnValue = this.value;
            _this.openValuesModal(btnValue,'simple')
        });
        $('#GetSiteValues').on('click', function(){
            var btnValue = this.value;
            _this.openValuesModal(btnValue,'simple')
        });
        $('#GetDeploymentValues').on('click', function(){
            var btnValue = this.value;
            _this.openValuesModal(btnValue,'simple')
        });
        //.....................................................................
        // Search buttons for plots tab
        $('#GetSpeciesPlots').on('click', function(){
            var btnValue = this.value;
            _this.openValuesModal(btnValue, 'plots')
        });
        $('#GetProjectPlots').on('click', function(){
            var btnValue = this.value;
            _this.openValuesModal(btnValue, 'plots')
        });
        $('#GetSitePlots').on('click', function(){
            var btnValue = this.value;
            _this.openValuesModal(btnValue, 'plots')
        });
        $('#GetDeploymentPlots').on('click', function(){
            var btnValue = this.value;
            _this.openValuesModal(btnValue, 'plots')
        });
        $('#GetCallPlots').on('click', function(){
            var btnValue = this.value;
            _this.openValuesModal(btnValue, 'plots')
        });
        $('#GetGranularityPlots').on('click', function(){
            var btnValue = this.value;
            _this.openValuesModal(btnValue, 'plots')
        });
        //.....................................................................
        // used to load query file and submit
        $('#fileUpload').on('change', function(){
        var file = document.getElementById('fileUpload').files[0];
        if (file) {
            var reader = new FileReader();
            reader.readAsText(file);
            reader.onload = function(e) {
            // browser completed reading file - send to submit query for query files
            //alert(e.target.result);
            //_this.savedQueryPressed(e.target.result);
            _this.readQueryJSON(e.target.result);
            };
        }
        })

        //.....................................................................
        $('#DetectEffortBtn').on('click', function() { //When submit is pressed
            _this.submitQueryPressed(this.value);
        });
        $('#DetectionBtn').on('click', function() { //When submit is pressed
            _this.submitQueryPressed(this.value);
        });
        $('#DeploymentBtn').on('click', function() { //When submit is pressed
            _this.submitQueryPressed(this.value);
        });
        $('#LocalizationBtn').on('click', function() { //When submit is pressed
            _this.submitQueryPressed(this.value);
        });
        $('#PlotsBtn').on('click', function() { //When submit is pressed
            _this.submitQueryPressed(this.value);
        });


        // ..................................................
         $('#bboxButton').on('click', ()=>{ //When bbox is pressed
             this.toggleBBOX();
        });
        // ..................................................
        $('#simple-tab').on('shown.bs.tab', function (e) { _this.reloadTree(); })
        // ..................................................

        $('input#minLong').on('blur', function(){
            _this.formData.setMinLongitude(Number(this.value));
        })
        $('input#maxLong').on('blur', function(){
            _this.formData.setMaxLongitude(Number(this.value));
        })
        $('input#minLat').on('blur', function(){
            _this.formData.setMinLatitude(Number(this.value));
        })
        $('input#maxLat').on('blur', function(){
            _this.formData.setMaxLatitude(Number(this.value));
        })
        //.................. weekly plot form events..........................
        $('input#speciesPlots').on('blur', function(){
            _this.formData.setWeeklyPlotSpecies(this.value);
        })

        $('input#CallPlots').on('blur', function(){
            _this.formData.setWeeklyPlotCall(this.value);
        })

        $('input#ProjectPlots').on('blur', function(){
            _this.formData.setWeeklyPlotProject(this.value);
        })

        $('input#DeploymentPlots').on('blur', function(){
            _this.formData.setWeeklyPlotDeployment(this.value);
        })
        $('input#SitePlots').on('blur', function(){
            _this.formData.setWeeklyPlotSite(this.value);
        })
        $('input#GranularityPlots').on('blur', function(){
            _this.formData.setWeeklyPlotGranularity(this.value);
        })
        //.................. Detection Query form events..........................

        $('input#speciesInputDetection').on('blur', function(){
            _this.formData.setSpeciesDetections(this.value);
        })
        //$('input#speciesInputDetection').autocomplete({
        //        source: ['dfasf','Mui','km'],
        //    });

        $('input#SiteInput').on('blur', function(){
            _this.formData.setSite(this.value);
        })

        $('input#ProjectInput').on('blur', function(){
            _this.formData.setProject(this.value);
        })

        $('input#DeploymentInput').on('blur', function(){
            _this.formData.setDeployment(this.value);
        })
        // END of Detection Query form events
        this.checkR()
        this.getGroups()
        //this.getSelectNamesAjax();
        this.unloaded = ["Abbreviations", "Localize","Event","Ensemble","Detections","Deployment","Calibration"];
        this.getSchema();   //This function sequentially calls the URL's for each collection. The reason for the sequence is so the tree is always in teh same order.

        //...........................Plots Date/Time ............................................
        $(function() {
               $("#datepicker_start").datepicker({ dateFormat: "yy-mm-dd",changeYear: true }).val()
               // look here! new Date($('#datepicker_start').val()).toISOString()
       });

       $(function() {
               $("#timepicker_start").timepicker({ timeFormat: "H:i:s", step:60})
               // look here! new Date($('#datepicker_start').val()).toISOString()
       });

       $(function() {
               $("#datepicker_end").datepicker({ dateFormat: "yy-mm-dd",changeYear: true }).val()
               // look here! new Date($('#datepicker_end').val()).toISOString()
       });

       $(function() {
               $("#timepicker_end").timepicker({ timeFormat: "H:i:s", step:60})
               // look here! new Date($('#datepicker_start').val()).toISOString()
       });

        //...........................Canned Date/Time ............................................
        $(function() {
               $("#datepicker_start_canned").datepicker({ dateFormat: "yy-mm-dd",changeYear: true }).val()
               // look here! new Date($('#datepicker_start').val()).toISOString()
       });

       $(function() {
               $("#timepicker_start_canned").timepicker({ timeFormat: "H:i:s", step:60})
               // look here! new Date($('#datepicker_start').val()).toISOString()
       });

       $(function() {
               $("#datepicker_end_canned").datepicker({ dateFormat: "yy-mm-dd",changeYear: true }).val()
               // look here! new Date($('#datepicker_end').val()).toISOString()
       });

       $(function() {
               $("#timepicker_end_canned").timepicker({ timeFormat: "H:i:s", step:60})
               // look here! new Date($('#datepicker_start').val()).toISOString()
       });
        // ...................................................................................

        $('#ClearQuery').on('click', function() {
            _this.clearSimpleQuery();
        });

    } // end of main

    clearSimpleFormData(){
        this.formData.setSpeciesDetections('');
        this.formData.setSpeciesGroup('')
        this.formData.setDeployment('')
        this.formData.setProject('')
        this.formData.setSite('')

    }
    clearSimpleQuery(){
        //clear model i.e. form
        this.clearSimpleFormData();
        // clear bbox
        this.toggleBBOX_FromAJAX()
        this.map.deleteMarkers();
        // clear dates and times
        $("#datepicker_start_canned").val('')
        $("#datepicker_end_canned").val('')
        $("#timepicker_start_canned").val('')
        $("#timepicker_end_canned").val('')

        $("#ProjectInput").val('')
        $("#SiteInput").val('')
        $("#DeploymentInput").val('')
        $("#speciesInputDetection").val('')

        this.resultsView.innerHTML = ''
        this.closeResults()


    }

    /**
     * Save results or query to file
     * @param selected
     */
    saveAs(selected){
        switch (selected) {
            case "MATLAB":
                this.downloadResults("Matlab", ".mat");
                break;
            case 'R':
                this.downloadResults("R", ".rData");
                break;
            case "XML":
                this.downloadResults("XML", ".xml");
                break;
            case 'JSON':
                this.downloadResults("JSON", ".json");
                break;
            case 'saveQuery':
                this.saveQueryJSON();
                break
        }
    }

    onSelect(selectedValue){  // called from stcombobox.js for a click on each item
        //alert(this.btnValue);
        //alert(selectedValue)
        if (this.type == 'simple') {
            if (this.btnValue == 'species')
            {
                $("#speciesInputDetection").val(selectedValue);
                $("#speciesInputDetection").blur();
                }
            else if (this.btnValue == 'site')
                {$("#SiteInput").val(selectedValue);
                 $("#SiteInput").blur();
                 }
            else if (this.btnValue == 'project')
                {
                $("#ProjectInput").val(selectedValue);
                $("#ProjectInput").blur();
                }
            else if (this.btnValue == 'deployment')
                {
                $("#DeploymentInput").val(selectedValue);
                $("#DeploymentInput").blur();
                }
        }
        else if (this.type == 'plots') {
            if (this.btnValue == 'species')
               { $("#speciesPlots").val(selectedValue);
                this.formData.setWeeklyPlotSpecies(selectedValue);}
            else if (this.btnValue == 'site')
               { $("#SitePlots").val(selectedValue);
                this.formData.setWeeklyPlotSite(selectedValue);}
            else if (this.btnValue == 'project')
               { $("#ProjectPlots").val(selectedValue);
                this.formData.setWeeklyPlotProject(selectedValue); }
            else if (this.btnValue == 'deployment')
               { $("#DeploymentPlots").val(selectedValue);
                this.formData.setWeeklyPlotDeployment(selectedValue); }
            else if (this.btnValue == 'call')
               { $("#CallPlots").val(selectedValue);
                this.formData.setWeeklyPlotCall(selectedValue); }
            else if (this.btnValue == 'granularity')
               { $("#GranularityPlots").val(selectedValue);
                this.formData.setWeeklyPlotGranularity(selectedValue); }
        }
    }

    openValuesModal(value, type){
        /*
        called on search button click, builds XQuery and shows modal. Calls getAutoArray with query string
        value - indicator of which button was clicked
        type - simple:  simple query interface
               plots: plot interface
        */

        var openModal = true;
        // What kind of species encoding? Latin or abbrev
        var selectedLibrary = $('#speciesSelectAdvanced').val();

        if (selectedLibrary == 'latin') {
                var spsLibraryStr_vector = 'lib:SpeciesIDtsn2name'
                var sps_tsnTo = 'lib:tsn2completename';
                var sps_ToTsn = 'lib:completename2tsn';
                var libraryArg = '';
        } else {
               var spsLibraryStr_vector =  'lib:SpeciesIDtsn2abbrev';
               var sps_tsnTo = 'lib:tsn2abbrev';
               var sps_ToTsn = 'lib:abbrev2tsn';
               var libraryArg = ',"'+selectedLibrary+'"'
               }

        // Retrieve the current entry values
        var values = {}
        if (type == 'simple') {
            values['site'] = $('#SiteInput').val();
            values['deployment'] = $('#DeploymentInput').val();
            values['project'] = $('#ProjectInput').val();
            values['call'] = $('#CallPlots').val();
            values['granularity'] = $('#GranularityPlots').val();
            values['species'] = $('#speciesInputDetection').val();

        } else if (type == 'plots'){
           values['species'] = $('#speciesPlots').val();
           values['call'] = $('#CallPlots').val();
           values['site'] = $('#SitePlots').val();
           values['deployment'] = $('#DeploymentPlots').val();
           values['project'] = $('#ProjectPlots').val();
           values['granularity'] = $('#GranularityPlots').val();
        }
        // Paths dictionary maps the values names to a collection and path relative to the root
        // This lets us construct queries that restrict the results to specific conditions
        // Each entry contains: [CollectionName, PathRelativeToRootElement, ShouldValueBeQuoted (bool)]
        var paths = {
            'site':["Deployments", "Site", true],
            'deployment':["Deployments", 'DeploymentNumber', false],
            'project':["Deployments", "Project", true],
            'call':["Detections", "Effort/Call", true],
            'granularity':["Detections", "Effort/Kind/Granularity", true],
            'species':["Detections", "Effort/Kind/SpeciesId", true]
        }

        // Build the query to retrieve what we are interested in.
        var criteria = {}
        delete values[value];  // Remove the value we are trying to find
        // Build up other conditions
        var conditions = {};
        for (const key of Object.keys(values)) {
            var keyval = values[key];
            if (keyval != '') {
                var path_info = paths[key];  // Collection, Path, Quote?
                var rhs;  // right hand side of operator
                if (path_info[2])
                    // Split out comma separated values, quote and reassemble into a list
                    // e.g., boo, boo   , bear => ("boo", "boo", "bear")
                    rhs = '(' + keyval.split(",").map(s => '"' + s.trim() + '"').join(", ") + ')';
                else
                    rhs = '(' + keyval + ')';
                if (! (path_info[0] in conditions)) {
                    conditions[path_info[0]] = []  // first time, create a list
                }
                // add the condition to the list for this collection
                conditions[path_info[0]].push(path_info[1] + " = " + rhs);
            }
        }

        var xquery = ['declare default element namespace "http://tethys.sdsu.edu/schema/1.0";'];
        switch (value) {
            case 'species':
                if (! ("Detections" in conditions))
                    conditions["Detections"]= [];
                // Restrict to specific ids
                if ("Deployments" in conditions) {
                    // Find Ids associated with deployments and add to restrictions associated
                    // with the detections
                    xquery.push('let $depids := collection("Deployments")/Deployment' +
                        this.build_conditions(conditions["Deployments"]) + '/Id');
                    conditions["Detections"].unshift("DataSource/DeploymentId = $depids");
                }
                // Retrieve TSNs
                xquery.push('let $tsns := distinct-values(collection("Detections")/Detections' +
                    this.build_conditions(conditions["Detections"]) + '/' + paths[value][1] +')'
                )
                if (selectedLibrary == 'latin') {
                    xquery.push('for $tsn in $tsns')
                    xquery.push('  return collection("ITIS_ranks")/ranks/rank[tsn = $tsn cast as xs:double]/completename')
                } else {
                    // using a species abbreviation
                    xquery.push('let $map := collection("SpeciesAbbreviations")/Abbreviations[Name="' +
                        selectedLibrary + '"]/Map');
                    xquery.push('for $tsn in $tsns')
                    xquery.push('  return $map[tsn = $tsn]/coding')
                }
                break;

            case 'site':
            case 'deployment':
            case 'project':
                // Deployment restrictions
                xquery.push('distinct-values(collection("Deployments")/Deployment' +
                    this.build_conditions(conditions["Deployments"]) + '/' + paths[value][1] + ')');
                break;

            case 'call':
                console.log("UNUSED");
                break;
        }
        var distinctValuesXQuery = xquery.join("\n");
        if (openModal && type == 'simple'){
            this.getAutoArray(distinctValuesXQuery, value, type)
            this.valuesModal = new ResultModal('GetValuesSimple','closeGetValuesSimple');
            document.getElementById("pathSpanSimple").innerHTML = value;
            this.valuesModal.setBody("<span id = 'putComboHere'></span>");
            $('#GetValuesSimple').modal('show');

            $("#closeGetValuesSimple").on("click",
                ()=>{
                    $('#GetValuesSimple').modal('hide');
                    //lets see if we can fire off something here that populates the field
                    this.onSelect(this.combo.getInput().val()); //comboObject.getInput().val()
                });
        }

        if (openModal && type == 'plots'){
            this.getAutoArray(distinctValuesXQuery, value, type)
            this.valuesModal = new ResultModal('GetValuesPlots','closeGetValuesPlots');
            document.getElementById("pathSpanPlots").innerHTML = value;
            this.valuesModal.setBody("<span id = 'putComboHerePlots'></span>");
            $('#GetValuesPlots').modal('show');
            $("#closeGetValuesPlots").on("click",
                ()=>{
                    $('#GetValuesPlots').modal('hide');
                    this.onSelect(this.combo.getInput().val());
                });
        }
    }
    getAutoArray(query, btnValue, type){
        // queries database and gets unique values for autocomplete
        console.log(query)
        let form = new FormData();  //create a FormData instance that is going to be the body of the HTTP Post
        form.append("XQuery", query); //add generated query to POST body
        let autocompleteAjaxRequest = new AjaxWrapper("post", form, this.autoArrEmpty);
        let _this = this;
        $.ajax(autocompleteAjaxRequest.getSettings()).done((response)=>{
            console.log("DISTINCT");
            console.log(response);
            var newResponse = response.replace(/<[^>]*>/g,"");
            _this.autocompleteArr = newResponse.split("\n");
            this.addAutoCompleteForModal(btnValue, type);
        });
    }

    autoArrEmpty(jqXHR, textStatus, errorThrown){
        console.log(textStatus+": "+errorThrown+"\n"+jqXHR.responseText);
        this.autocompleteArr = ['bad request'];
    }

    handleEnter_simple(e){
        if(e.which === 13){
            //alert('dfgafgafa')
            if (this.combo.presence){
            this.onSelect($('#putComboHere-ddi').val());
            }
            $('#GetValuesSimple').modal('hide');
        }
    }
    handleEnter_plots(e) {
        if(e.which === 13){
             if (this.combo.presence) {
                 this.onSelect($('#putComboHerePlots-ddi').val());
             }
             $('#GetValuesPlots').modal('hide');
        }
    }


    addAutoCompleteForModal(btnValue, type) {

        var autocompleteData = this.autocompleteArr;
        this.combo = new STComboBox();
        if (type == 'simple')
            this.combo.Init("putComboHere");
        else if (type == 'plots')
            this.combo.Init("putComboHerePlots");
        var data = [];
        for (var i = 0; i < autocompleteData.length; i++) {
            data.push({id: i, text: autocompleteData[i]});
        }

        this.combo.populateList(this, data);
        if (type == 'simple') {
            //alert('does it ever get ere')
            $('#putComboHere-ddi').on('keypress',
            this.handleEnter_simple.bind(this))

         } else if (type == 'plots'){
            $('#putComboHerePlots-ddi').on('keypress',
            this.handleEnter_plots.bind(this))
        }
        this.btnValue = btnValue;
        this.type = type;
    }

    toggleBBOX_FromAJAX() {
        if (this.bboxButton.className == "on") {
            this.bboxButton.className = "off";
            this.map.removeSquare();

            $('input#minLong').val('');
            $('input#maxLong').val('');
            $('input#minLat').val('');
            $('input#maxLat').val('');

            $('input#minLong')[0].disabled = true;
            $('input#maxLong')[0].disabled = true;
            $('input#minLat')[0].disabled = true;
            $('input#maxLat')[0].disabled = true;

        }
    }


    toggleBBOX(){
        if(this.bboxButton.className == "on") {
            this.bboxButton.className="off";
            this.map.removeSquare();

            $('input#minLong').val('');
            $('input#maxLong').val('');
            $('input#minLat').val('');
            $('input#maxLat').val('');

            $('input#minLong')[0].disabled = true;
            $('input#maxLong')[0].disabled = true;
            $('input#minLat')[0].disabled = true;
            $('input#maxLat')[0].disabled = true;

        } else {
        //el.className="on";
            this.bboxButton.className = "on"
            this.map.drawSquare(this.formData);

            $('input#minLong')[0].disabled = false;
            $('input#maxLong')[0].disabled = false;
            $('input#minLat')[0].disabled = false;
            $('input#maxLat')[0].disabled = false;
        }

    }

    reloadTree(){
    this.trees = new D3Json();
    this.unloaded = [ "Abbreviations", "Localize","Event","Ensemble","Detections","Deployment","Calibration"];
    this.getSchema();

    }


    getSchema()
    {
        let urlValue = window.location.origin+"/Schema/"+this.unloaded.pop()+"?format=JSON";   //AJAX URL to request xml JSON. the schemaSelector Value is spliced into the string.
        let ajaxCall = new AjaxWrapper("get","", this.ajaxError);
        ajaxCall.setURL(urlValue);
        var _this = this;
        $.ajax(ajaxCall.getSettings()).done((response)=>{ //When we get the response for the xml JSON=
            let treeData = new XmlToD3();
            treeData.setXmlJson(response);
            treeData.convert();
            _this.trees.addChildren(treeData.getD3Json());

            if(this.unloaded.length>0){ //runs until there are no more collections to load
                _this.getSchema();
            }
            else{
                _this.trees.createParents();
               // the following is the way advanced queries works
               //this.d3Controller.setRoot(this.trees);
               //this.schemaSelectChanged($("#schemaSelector").val());
            }
        });
    }


    /**
     * Populates <select id = "speciesSelect" </select>
     */
    getSelectNamesAjax(){
        alert("DOS THIS")
        let form = new FormData();
        form.append("XQuery", PreBuiltQueries.selectSpeciesAbbreviations());
        var ajaxCall = new AjaxWrapper("post", form, this.ajaxError);
        var _this = this;
        $.ajax(ajaxCall.getSettings()).done(function (response) { //waiting for the AJAX response
            let xmlResponse = AjaxWrapper.StringToXML(response);
            _this.view.addOptions(xmlResponse.children[0].children);
        });
    }

    /**
     * Construct the GroupLookup dict
     * Dict is keyed by species abbreviation map name
     * and has a subdict that maps the abbreviation coding to the group name
     */
    getGroups(){

        let form = new FormData();
        // Retrieve CSV text.  Each line is of the form:
        //   MapName,coding,group
        form.append("XQuery", PreBuiltQueries.selectSpeciesGroup());
        form.append("responseType", "text/plain")
        var ajaxCall = new AjaxWrapper("post", form, this.ajaxError);
        var _this = this;  // for accessing in inner fn
        $.ajax(ajaxCall.getSettings()).done(function (response) {
            let lines = response.split("\n");
            for (let i = 0; i < lines.length; i++) {
                let values = lines[i].split(",");
                let mapid = values[0];
                let encoding = values[1];
                let group = values[2];
                if (! (mapid in _this.GroupLookup))
                    _this.GroupLookup[mapid] = {}; // first time this species abbr map
                // Associate the Group with this encoding
                _this.GroupLookup[mapid][encoding] = group;
            }
        })
    }

    checkR(){

        var ajaxCall = new AjaxWrapper("get", '', this.ajaxError);
        ajaxCall.setURL("/Tethys/R")
        var _this = this;
        $.ajax(ajaxCall.getSettings()).done(function (response) { //waiting for the AJAX response
            //alert(response)
            if (response == 'false'){
                _this.R = false;
            } else {
                _this.R = true;
            }
            })


    }

    readQueryJSON(json){
        /*
        called from load query dialog event on change. parses json from file.
        determines if query is simple or advanced. populates model (form) and view for simple query
        */
        var queryparams = JSON.parse(json);
        if (queryparams.hasOwnProperty('form')){
            var queryType = Object.keys(queryparams)[0];
            this.formData.deploymentAndDetections.longitude.min = queryparams.formbbox.longitude.min;
            this.formData.deploymentAndDetections.longitude.max = queryparams.formbbox.longitude.max;
            this.formData.deploymentAndDetections.latitude.min = queryparams.formbbox.latitude.min;
            this.formData.deploymentAndDetections.latitude.max = queryparams.formbbox.latitude.max;

            this.formData.detectionQuery.species = queryparams.form.species[0];
            this.formData.detectionQuery.site = queryparams.form.site;
            this.formData.detectionQuery.project = queryparams.form.project;
            this.formData.detectionQuery.deployment = queryparams.form.deployment;

            document.getElementById('minLong').value = queryparams.formbbox.longitude.min;
            document.getElementById('maxLong').value = queryparams.formbbox.longitude.max;
            document.getElementById('maxLat').value = queryparams.formbbox.latitude.max;
            document.getElementById('minLat').value = queryparams.formbbox.latitude.min;

            document.getElementById('speciesInputDetection').value = queryparams.form.species[0];
            document.getElementById('SiteInput').value = queryparams.form.site;
            document.getElementById('ProjectInput').value = queryparams.form.project;
            document.getElementById('DeploymentInput').value = queryparams.form.deployment;
            document.getElementById('fileUpload').value = '';
        } else {
            advancedQueries.readJSON(json)
            $('#advanced-tab').tab('show');
            $('#build-tab').tab('show');
        }
    }

    saveQueryJSON(){
        /*
        called from save query combo, uses query object to populate where and return clauses,
        uses two forms from CannedData.js to get user entered values in simple form
        */
        var JSONContainer = {'formbbox':this.formData.deploymentAndDetections, 'form':this.formData.detectionQuery};
        var queryJSON = JSON.stringify(JSONContainer);
        this.saveJSON(queryJSON);
    }

    getClauses(query){
        /*
        called from saveQueryJSON. Gets where and return clauses from query object
        */
        var whereData = query.getWhereData();
        var returnData = query.getReturnData();
        var preJSONClauses = {'whereData':[],'returnData':[]};
        for(var i=0; i < whereData.length; i++) {
            preJSONClauses.whereData.push({'path': whereData[i].getPath(),
                                           'inputValue': whereData[i].getInputValue(),
                                           'operatorValue': whereData[i].getOperatorValue(),
                                           'conjunction': whereData[i].getConjunctionValue(),
                                           'node': ''
                                          });
        }
        for(var i=0; i < returnData.length; i++) {
            preJSONClauses.returnData.push({'path': returnData[i].getPath(), 'node': ''});
        }
        return preJSONClauses
    }



    constructNonJoinQuery(forloop,mainQuery){

        forloop = "<depinfo>{"+forloop+"}</depinfo></ty:Result>";
        var fullQuery = mainQuery.replace("</ty:Result>",forloop);
        return fullQuery
    }

    checkPlotForm(){

        if ($('#speciesSelectAdvanced').val() == ''){
            alert('Please choose a species library');
            return false;
        }
        if (!$('#datepicker_start').val() || !$('#datepicker_end').val()){
            if (!$('#speciesPlots').val() || !$('#ProjectPlots').val() || !$('#SitePlots').val() || !$('#CallPlots').val() || !$('#GranularityPlots').val() || !$('#DeploymentPlots').val())
            {
                alert("Missing plot values");
                return false;
            }
        } else{
            if (!$('#speciesPlots').val() || !$('#ProjectPlots').val() || !$('#SitePlots').val() || !$('#CallPlots').val() || !$('#GranularityPlots').val())
            {
                alert("Missing plot values");
                return false;
            }

        }
        return true;
    }

    setPickerISOValues_Plots(){
        /*
         * Parses timestamps in QueryByTime fields and constructs an ISO 8601 string
         * ISO8601 is stored in object variables (min|max)SliderValue_Plots
         */

        if ($('#datepicker_start').val() != '' && $('#datepicker_end').val() != '')
        {
            $('#datepicker_start').blur();
            $('#datepicker_end').blur();
            $('#timepicker_start').blur();
            $('#timepicker_end').blur();

            // start date/time
            var beginDate = new Date($('#datepicker_start').val()+'T00:00:00');
            if ($('#timepicker_start').val() != ''){
                if ($('#timepicker_start').val().indexOf(":") != -1 ){
                    var beginTimeArray = $('#timepicker_start').val().split(":")

                    var beginHours = parseInt(beginTimeArray[0]);
                    var beginMinutes = parseInt(beginTimeArray[1]);
                    var beginSeconds = parseInt(beginTimeArray[2]);

                    beginDate.setUTCHours(beginHours)
                    beginDate.setMinutes(beginMinutes)
                    beginDate.setSeconds(beginSeconds)

                }
            }else {
                    beginDate.setUTCHours(0)
                    beginDate.setMinutes(0)
                    beginDate.setSeconds(0)
            }
            try{
                this.minSliderValue_Plots = beginDate.toISOString();
               }
            catch(err){
                alert(err.message+" in begin date, must be yyyy-mm-dd")
                return false;
               }

            // end date/time
            var endDate = new Date($('#datepicker_end').val()+'T00:00:00');
            if ($('#timepicker_end').val() != '') {
                if ($('#timepicker_end').val().indexOf(":") != -1 ){
                    var endTimeArray = $('#timepicker_end').val().split(":")

                    var endHours = parseInt(endTimeArray[0]);
                    var endMinutes = parseInt(endTimeArray[1]);
                    var endSeconds = parseInt(endTimeArray[2]);

                    endDate.setUTCHours(endHours);
                    endDate.setMinutes(endMinutes);
                    endDate.setSeconds(endSeconds);
                }
            } else {
                    endDate.setUTCHours(0)
                    endDate.setMinutes(0)
                    endDate.setSeconds(0)
            }
            try{
                this.maxSliderValue_Plots = endDate.toISOString();
               }
            catch(err){
                alert(err.message+" in end date, must be yyyy-mm-dd")
                return false;
                }
        return true;
        }
        else {
            this.minSliderValue_Plots = '';
            this.maxSliderValue_Plots = '';
            return true;
        }
    }


    setPickerISOValues_Canned(){
        /*
         * Parses timestamps in QueryByTime fields and constructs an ISO 8601 string
         * ISO8601 is stored in object variables (min|max)SliderValue
         */

        if ($('#datepicker_start_canned').val() != '' && $('#datepicker_end_canned').val() != '')
        {
            $('#datepicker_start_canned').blur();
            $('#datepicker_end_canned').blur();
            $('#timepicker_start_canned').blur();
            $('#timepicker_end_canned').blur();

            // start date/time
            var beginDate = new Date($('#datepicker_start_canned').val()+'T00:00:00');
            if ($('#timepicker_start_canned').val() != ''){
                if ($('#timepicker_start_canned').val().indexOf(":") != -1 ){
                    var beginTimeArray = $('#timepicker_start_canned').val().split(":")

                    var beginHours = parseInt(beginTimeArray[0]);
                    var beginMinutes = parseInt(beginTimeArray[1]);
                    var beginSeconds = parseInt(beginTimeArray[2]);

                    beginDate.setUTCHours(beginHours)
                    beginDate.setMinutes(beginMinutes)
                    beginDate.setSeconds(beginSeconds)

                }
            } else {
                    beginDate.setUTCHours(0)
                    beginDate.setMinutes(0)
                    beginDate.setSeconds(0)
            }
            try{
                this.minSliderValue = beginDate.toISOString();
               }
            catch(err){
                alert(err.message+" in begin date, must be yyyy-mm-dd")
                return false;
               }

            // end date/time
            var endDate = new Date($('#datepicker_end_canned').val()+'T00:00:00');
            if ($('#timepicker_end_canned').val() != '') {
                if ($('#timepicker_end_canned').val().indexOf(":") != -1 ){
                    var endTimeArray = $('#timepicker_end_canned').val().split(":")

                    var endHours = parseInt(endTimeArray[0]);
                    var endMinutes = parseInt(endTimeArray[1]);
                    var endSeconds = parseInt(endTimeArray[2]);

                    endDate.setUTCHours(endHours);
                    endDate.setMinutes(endMinutes);
                    endDate.setSeconds(endSeconds);
                }
            } else {
                    endDate.setUTCHours(0)
                    endDate.setMinutes(0)
                    endDate.setSeconds(0)
            }
            try{
                this.maxSliderValue = endDate.toISOString();
               }
            catch(err){
                alert(err.message+" in end date, must be yyyy-mm-dd")
                return false;
                }
        return true;
        }
        else {
            this.minSliderValue = '';
            this.maxSliderValue = '';
            return true;
        }
    }

    tryJSONPrint(xml){
        var json = parser.parse(xml)

        var defaultOptions = {
        attributeNamePrefix : "@_",
        attrNodeName: "@", //default is false
        textNodeName : "#text",
        ignoreAttributes : true,
        cdataTagName: "__cdata", //default is false
        cdataPositionChar: "\\c",
        format: true,
        indentBy: "  ",
        supressEmptyNode: false,
             tagValueProcessor: a=> a,
           attrValueProcessor: a=> a

        };
        var Parser = new parser.j2xParser(defaultOptions)
        var myxml = Parser.parse(json)
        return myxml //json
    }

    xmlToUL(sourceXml,xmlDoc, NoRecords)
        {
            if (NoRecords > 100)
                var classStr = '';
            else
                var classStr = 'class="jstree-open"';
            var xsltDoc = new DOMParser().parseFromString([
                // describes how we want to modify the XML - UL
                '<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">' ,
                '<xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes"/>' ,
                '<xsl:strip-space elements="*"/>' ,
                '<xsl:template match="/">' ,
                '    <ul>',
                '        <xsl:apply-templates/>' ,
                '    </ul>' ,
                '</xsl:template>' ,
                '<xsl:template match="*">' ,
                '    <li '+classStr+' id="{generate-id(.)}">' ,
                '        <xsl:value-of select="name()" />' ,
                '        <xsl:if test="text()">' ,
                '            <xsl:text>: </xsl:text>' ,
                '            <xsl:apply-templates select="text()" />' ,
                '        </xsl:if>' ,
                '        <xsl:if test="*">' ,
                '            <ul>' ,
                '                <xsl:apply-templates/>' ,
                '            </ul>' ,
                '        </xsl:if>' ,
                '    </li>' ,
                '</xsl:template>' ,
                '</xsl:stylesheet>',
            ].join('\n'), 'application/xml');

            var xsltProcessor = new XSLTProcessor();
            xsltProcessor.importStylesheet(xsltDoc);
            var resultDoc = xsltProcessor.transformToDocument(xmlDoc);
            var resultXml = new XMLSerializer().serializeToString(resultDoc);


            return resultXml;
        };



    prettifyXml(sourceXml)
        {
            console.log('starting parser')
            var xmlDoc = new DOMParser().parseFromString(sourceXml, 'text/xml');  //need to check browser here
            console.log('done parsing')
            var xsltDoc = new DOMParser().parseFromString([
                // describes how we want to modify the XML - indent everything
                '<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform">',
                '  <xsl:strip-space elements="*"/>',
                '  <xsl:template match="para[content-style][not(text())]">', // change to just text() to strip space in text nodes
                '    <xsl:value-of select="normalize-space(.)"/>',
                '  </xsl:template>',
                '  <xsl:template match="node()|@*">',
                '    <xsl:copy><xsl:apply-templates select="node()|@*"/></xsl:copy>',
                '  </xsl:template>',
                '  <xsl:output indent="yes"/>',
                '</xsl:stylesheet>',
            ].join('\n'), 'application/xml');

            var xsltProcessor = new XSLTProcessor();
            xsltProcessor.importStylesheet(xsltDoc);
            console.log('starting transform')
            var resultDoc = xsltProcessor.transformToDocument(xmlDoc);
            console.log('done transform, serializing')
            var resultXml = new XMLSerializer().serializeToString(resultDoc);
            console.log('done serializing')
            return resultXml;
        };

    submitQueryPressed(btnValue){

        if (btnValue == "Plots"){
            this.queryType = "Plots"
        } else {
            this.queryType = "Simple"
            this.closeResults()
        }
        this.trees.reset();
        let query = new PreBuiltQueries(this.trees);
        if (btnValue == "DetectionEffort") {
            if (!this.setPickerISOValues_Canned())
                return;
            if (this.formData.detectionQuery.species != ''){
                if ($('#speciesSelectAdvanced').val() in this.GroupLookup){
                    if (this.formData.detectionQuery.species in this.GroupLookup[$('#speciesSelectAdvanced').val()])
                        this.formData.setSpeciesGroup(this.GroupLookup[$('#speciesSelectAdvanced').val()][this.formData.detectionQuery.species])
                    else
                        this.formData.setSpeciesGroup('')
                }
            }
            query.detectionEffort(this.formData,this.returnController, $('#speciesSelectAdvanced').val(), this.bboxButton,advancedQueries.whereController,this.minSliderValue,this.maxSliderValue);
        }

        else if (btnValue == "Detection") {
            if (!this.setPickerISOValues_Canned())
                return;
            query.detectionQuery(this.formData,this.returnController,$('#speciesSelectAdvanced').val(), this.bboxButton,advancedQueries.whereController,this.minSliderValue,this.maxSliderValue);
        }
        else if (btnValue == "Deployment"){
            if (!this.setPickerISOValues_Canned())
                return;
            query.deploymentQuery(this.formData,this.returnController,$('#speciesSelectAdvanced').val(), this.bboxButton,advancedQueries.whereController,this.minSliderValue,this.maxSliderValue);
        }
        else if (btnValue == "Localizations"){
           if (!this.setPickerISOValues_Canned())
                return;
           query.localizationsQuery(this.formData, this.returnController, $('#speciesSelectAdvanced').val(), this.bboxButton, advancedQueries.whereController,this.minSliderValue,this.maxSliderValue);
           // querySpecs, returnController , species, bboxButton, whereController ,minTime, maxTime
        }

        // Create a new form data that is saved in the object so that we can access it on success
        this.last_query_attempt = new FormData();


        // create a new SubmitClass with the list where and return list items
        let submit = new SubmitQuery(query.getWhereData(), query.getReturnData(),
				     $('#speciesSelectAdvanced').val(), btnValue+"Record");

        var cannedQueryText = JSON.stringify({
            "enclose":1,
            "namespaces":0,  // default strip namespace
            "species":query.speciesJSONBlock,
            "select": query.selectJSONBlock,
            "return":query.returnJSONBlock})

        console.log(cannedQueryText)

        this.last_query_attempt.append("JSON",cannedQueryText)  // Query parameters
        // Now that we cache query results, we should redesign this section
        this.last_query_attempt.append("dataType","save")  // saves text for future retrieval

        this.modal.setLoader();     //The grey circle with a blue arc spinning around it. Indicating to the user the submit is in progress.
        $('#simpleModal').modal('show');

        let ajaxCall = new AjaxWrapper("post", this.last_query_attempt, this.ajaxError);
        let _this = this;
        var ajax_params = ajaxCall.getSettings();
        let xhr = $.ajax(ajax_params).done( (response, textStatus, jqXHR) => { //waiting for the AJAX response
            // Save successful query so that we can repeat it if needed (e.g., save results as type)
            _this.last_query_completed = _this.last_query_attempt;

            let max_MB = 5 // Treat really big things differently
            let bytes_per_MB = 1024*1024
            let result_MB = Math.round(response.length / bytes_per_MB * 10) / 10;
            if (result_MB < max_MB) {
                // Small enough to set up a tree
                var responseDOM = new DOMParser().parseFromString(response, 'text/xml')
                var NoRecords = responseDOM.children[0].children.length
                if (responseDOM.children[0].children.length > 0){
                    //var h0 = performance.now()
                    _this.resultFormating(_this.xmlToUL(response, responseDOM, NoRecords))
                    //var h1 = performance.now()
                    //console.log("all of resultFormating "+ (h1 - h0)/1000)
                    //var g0 = performance.now()
                    var gug1 = performance.now()
                    console.log("opening results")
                    _this.openResults()
                    var gug2 = performance.now()
                    console.log("done opening results "+ (gug2 - gug1)/1000)
                    if (NoRecords > 100)
                        $("#tree_div").jstree().open_node($('.jstree-node'))// opens up Results li
                } else {
                    $('#plugins4_q')[0].style.display = 'none'
                    _this.resultsView.innerHTML = "<div id='tree_div'><p>No Results Meet Your Query Parameters</p></div>";
                    $("#openButDiv")[0].style.color = "black"
                    //$('#simpleModal').modal('hide');  // ibet it trying set markers on empty sets
                    _this.openResults()
                }

            } else {
                $('#plugins4_q')[0].style.display = 'none'
                _this.resultsView.innerHTML = "<div id='tree_div'><p>" +
                    `Results are ${result_MB} megabytes and are not shown as they exceed the ${max_MB} megabyte ` +
                    "browser display limit.  Results may be downloaded with the Save button</p></div>";
                $("#openButDiv")[0].style.color = "black"
                _this.openResults()
            }
            _this.map.clearMap();
            if (! _this.map.constructTracks(response))
                _this.checkForLongLat(response);
            _this.toggleBBOX_FromAJAX()  // this turns off existing bbox in prep for new query

            // URL for converting this query response to another format
            _this.convertURL = xhr.getResponseHeader("Content-Location");
            // Remove disabled options as the query succeeded
            $("option[value='MATLAB']").removeAttr("disabled");
            if (_this.R)
                $("option[value='R']").removeAttr("disabled");
            $("option[value='JSON']").removeAttr("disabled");
            $("option[value='XML']").removeAttr("disabled");

            _this.modal.hide('simpleModal')
            console.log("simple query successfully processed")

        });
    }

    openResults(){
      if ($('#resultsViewSimple').is('.collapse:not(.show)') || $('#resultsViewSimple').hasClass('collapsing')) {
          //alert('in collapsing')
          if ($('#resultsViewSimple').is('.collapse:not(.show)'))
            $('#openButDiv').trigger("click")
          else {
              $('#resultsViewSimple').on('hidden.bs.collapse', function(){$('#openButDiv').trigger("click")})
          }
      }
    }

    closeResults(){

     if (!$('#resultsViewSimple').is('.collapse:not(.show)')) {
         //alert('closing')
         $('#openButDiv').trigger("click")
         //$('#resultsViewSimple').collapse();

      }
    }

    makeQueryResult(query){
        var wnd = window.open("Failed Query", "_blank");
        wnd.document.write(query);
    }

    ajaxError(jqXHR, textStatus, errorThrown){

        //cannedQueries.resultsView.innerHTML = textStatus+": "+errorThrown+"\n"+jqXHR.responseText;   //encapsulation breaking
        //alert(textStatus)

        let responseText;
        let query = "";
        let inner;

        if (jqXHR.readyState == 0) {
            // message was not sent
            responseText = "Server down?  Unable to communicate with Tethys server.";
        } else {
            // See if we can split the query and error message
            try {
                responseText = jqXHR.responseText.split("--")[0];
                query = jqXHR.responseText.split("-- Query --")[1];
            } catch {
                responseText = jqXHR.responseText;
            }
        }

        cannedQueries.modal.hide('simpleModal')
        // cannedQueries.modal.hide('plotModal')

        //cannedQueries.xxxx = 1  // this needs to be a bit different
        if (cannedQueries.queryType == "Simple") {

            $('#plugins4_q')[0].style.display = 'none'
            if (query != '' && query != undefined) {
                query = query.replaceAll('"',"'")
                query = query.replace(/(\r\n|\n|\r)/gm, "<br>");
                inner = '<div id="tree_div"><p>'+textStatus+': '+errorThrown+'<br>'+responseText+'<br>' +
                    '<a  href="javascript:void(0);" role="button" onClick="cannedQueries.makeQueryResult(&#34'+query+'&#34)" >View The Query</a></p></div>'; //href="https://www.w3schools.com"


            } else{
                inner = "<div id='tree_div'><p>"+textStatus+": "+errorThrown+"\n"+responseText+"</p></div>";
            }

            document.getElementById('resultTextAreaSimple').innerHTML = inner
            cannedQueries.openResults()
            //cannedQueries.blinker("Click To See Error")
            $("#openButDiv")[0].style.color ="red"
            cannedQueries.map.deleteMarkers();
        }
    }

    saveJSON(json){
        // saves query json
        var link = document.createElement('a');
        link.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(json));
        link.setAttribute('download', 'jsonQuery.json');
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);

    }

    getNewSaveButton(){
        var newButton = `<svg version="1.1"  xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	    viewBox="0 0 49 49" style="enable-background:new 0 0 49 49;" xml:space="preserve" height="1em" width="1em">
        <g>
        	<path d="M39.914,0H37.5h-28h-9v49h7h33h8V8.586L39.914,0z M35.5,2v14h-24V2H35.5z M9.5,47V28h29v19H9.5z M46.5,47h-6V26h-33v21h-5
        		V2h7v16h28V2h1.586L46.5,9.414V47z"/>
        	<path d="M13.5,33h7c0.553,0,1-0.447,1-1s-0.447-1-1-1h-7c-0.553,0-1,0.447-1,1S12.947,33,13.5,33z"/>
        	<path d="M23.5,35h-10c-0.553,0-1,0.447-1,1s0.447,1,1,1h10c0.553,0,1-0.447,1-1S24.053,35,23.5,35z"/>
        	<path d="M25.79,35.29c-0.181,0.189-0.29,0.45-0.29,0.71s0.109,0.52,0.29,0.71C25.979,36.89,26.229,37,26.5,37
        		c0.26,0,0.52-0.11,0.71-0.29c0.18-0.19,0.29-0.45,0.29-0.71s-0.11-0.521-0.29-0.71C26.84,34.92,26.16,34.92,25.79,35.29z"/>
        	<path d="M33.5,4h-6v10h6V4z M31.5,12h-2V6h2V12z"/>
        </g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g>
        </svg>`;
        //<div>Icons made by <a href="https://www.flaticon.com/authors/smashicons" title="Smashicons">Smashicons</a> from <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a></div>
    return newButton;
    }



    getPlot(url){

        url = url+"/weeklydetection";
        $( "#PlotsContainer" ).load( url, function(response, status, xhr) {
            $('#plotModal').modal('hide')
            if ( status == "error" ) {
                var msg = "Sorry but there was an error: ";
                $( "#PlotsContainer" ).html( msg + xhr.status + " " + xhr.statusText );
             }
             // attempt to change toolbar
             var btnDiv = document.createElement('div');
             btnDiv.setAttribute("class","modebar-group");
             //btnDiv.innerHTML = '<a rel="tooltip" onClick="cannedQueries.DownloadPlotAsSVG();" class="modebar-btn" data-title="Download plot as a SVG" data-toggle="false" data-gravity="n"><svg viewBox="0 0 1000 1000" class="icon" height="1em" width="1em"><path d="m500 450c-83 0-150-67-150-150 0-83 67-150 150-150 83 0 150 67 150 150 0 83-67 150-150 150z m400 150h-120c-16 0-34 13-39 29l-31 93c-6 15-23 28-40 28h-340c-16 0-34-13-39-28l-31-94c-6-15-23-28-40-28h-120c-55 0-100-45-100-100v-450c0-55 45-100 100-100h800c55 0 100 45 100 100v450c0 55-45 100-100 100z m-400-550c-138 0-250 112-250 250 0 138 112 250 250 250 138 0 250-112 250-250 0-138-112-250-250-250z m365 380c-19 0-35 16-35 35 0 19 16 35 35 35 19 0 35-16 35-35 0-19-16-35-35-35z" transform="matrix(1 0 0 -1 0 850)"></path></svg></a>'
             btnDiv.innerHTML = '<a rel="tooltip" onClick="cannedQueries.DownloadPlotAsSVG();" class="modebar-btn" data-title="Download plot as a SVG" data-toggle="false" data-gravity="n">'+cannedQueries.getNewSaveButton()+'</a>';
             var el = $('.modebar.modebar--hover.ease-bg')[0];
             if (el)
                el.prepend(btnDiv);
            });
    }




    DownloadPlotAsSVG() {

        var openingSVGtag = '<svg class="main-svg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="800" height="500" style="background: white;">';
        var closingSVGtag = '</svg>';
        var firstLayer = $('.main-svg')[0].innerHTML.replace("<br>"," ");
        var secondLayer = $('.main-svg')[1].innerHTML.replace("<br>"," ");
        var svgString = openingSVGtag + firstLayer + secondLayer + closingSVGtag;
        var SVG_Anchor = document.createElement('a');
        SVG_Anchor.setAttribute('href', 'data:text/plain,' + encodeURIComponent(svgString));
        SVG_Anchor.setAttribute('download','plotSVG.svg');
        SVG_Anchor.click()

    }

    getPlot_deprecated(url){
        let form = new FormData();
        url = url+"/weeklydetection";
        var ajaxCall = new AjaxWrapper("get", form, this.ajaxError);
        ajaxCall.setURL(url)
        var _this = this;
        var xhr = $.ajax(ajaxCall.getSettings()).done(function (response) {
            console.log(response);
            document.getElementById("PlotsContainer").innerHTML = response;

        });
    }

    downloadResults(path, ext){
                /*
        uses url from Content-Location to build GET request
        for matlab conversion.
        */
       var url = this.convertURL;
       var link = document.createElement('a');
       link.href = url+ "/" + path;
       var path = link.pathname;
       var filename = path.split("/")[2] + ext
       link.download = filename;
       document.body.appendChild(link);
       link.click();
       document.body.removeChild(link);
       $("#saveAsComboSimple").val('')
        /**
         * Construct a download link and click it for the desired download type
         */
        /*
        let response_type = "application/" + path.toLowerCase();

        // Copy all keys except responseType from last successful query
        let form = new FormData();
        for (const pair of this.last_query_completed.entries()) {
            if (pair[0] != "responseType") {
                form.append(pair[0], pair[1]);
            }
        }
        // Add in how we want the data returned
        form.append("responseType", response_type);

        let ajaxCall = new AjaxWrapper("post", form, this.ajaxError);
        var ajax_params = ajaxCall.getSettings();
        this.download_name = "query_data" + ext
        let xhr = $.ajax(ajax_params).done( (response, result, jqXHR) => { //waiting for the AJAX response
            let responseType = jqXHR.getResponseHeader("responseType")
            console.log(CryptoJS.MD5(response))
            let blob = new Blob([response], {type: responseType});

            // Create a temporary link target in the document
            let url = window.URL.createObjectURL(blob);
            let anchor = document.createElement("a");
            anchor.href = url;
            anchor.download = this.download_name;
            document.body.appendChild(anchor);
            anchor.click();  // Start download

            $("#saveAsComboSimple").val('')  // deselect save as option

            // Cleanup
            let timeout_ms = 300;
            setTimeout(function() {
                window.URL.revokeObjectURL(url);
            }, timeout_ms)
        })*/

        /**
        let url = this.convertURL;  // Conversion resource for last query
        let link = document.createElement('a');
        link.href = url+ "/" + path;
        let components = url.split("/");
        link.download = components[components.length - 1] + ext;  // default filename

        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);


         **/

    }

    setMap(){
        this.map = new MapWrapper(this,"Google-Map-Simple");
    }

    formatXml_experiment(nodes){
        var mystring = "";
        for (var i = 0; i < nodes.length; i++){
            mystring += nodes[i].innerHTML
        }
    return mystring;
    }

    formatXml(xml) {
        //console.log(xml);
        var formatted = '';
        var reg = /(>)(<)(\/*)/g;               //finds 3 groups. $1 equals >, $2 equals <, $3 equals / zero to infinite
        xml = xml.replace(reg, '$1\r\n$2$3');   //replace all ></ -> >\r\n</    all xml console
        var pad = 0;                            /* All xml will be seperated into xml start or end tags <Longitude>25.014</Longitude>*/
        var lines = xml.split('\r\n');
        //alert(lines.length);
        for (var b=0, len = lines.length; b < len; b++){  //foreach new line
            var indent = 0;
            var node = lines[b];
            if (node.match( /.+<\/\w[^>]*>$/ )) {   //Find all xml start and end tags on a line (e.g. <Longitude>25.014</Longitude>, <Vessel>Horizon</Vessel>), but not starts without ends
                indent = 0;
            } else if (node.match( /^<\/\w/ )) {    //Find all xml end tags and one letter (e.g. '</ResponsibleParty>' the '</R' would be matched)
                if (pad != 0) {
                    pad -= 1;
                }
            } else if (node.match( /^<\w[^>]*[^\/]>.*$/ )) {    //Find all xml start or start and end tags on a line (e.g. <Longitude>25.014</Longitude>, <out>)
                indent = 1;
            } else {
                indent = 0;
            }

            let padding = '';
            for (var i = 0; i < pad; i++) {
                padding += '  ';
            }

            formatted += padding + node + '\r\n';
            pad += indent;
        };
        return formatted;
    }

    formatXml_old(xml) {
        //console.log(xml);
        var formatted = '';
        var reg = /(>)(<)(\/*)/g;               //finds 3 groups. $1 equals >, $2 equals <, $3 equals / zero to infinite
        xml = xml.replace(reg, '$1\r\n$2$3');   //replace all ></ -> >\r\n</    all xml console
        var pad = 0;                            /* All xml will be seperated into xml start or end tags <Longitude>25.014</Longitude>*/
        jQuery.each(xml.split('\r\n'), function(index, node) {  //foreach new line
            var indent = 0;
            if (node.match( /.+<\/\w[^>]*>$/ )) {   //Find all xml start and end tags on a line (e.g. <Longitude>25.014</Longitude>, <Vessel>Horizon</Vessel>), but not starts without ends
                indent = 0;
            } else if (node.match( /^<\/\w/ )) {    //Find all xml end tags and one letter (e.g. '</ResponsibleParty>' the '</R' would be matched)
                if (pad != 0) {
                    pad -= 1;
                }
            } else if (node.match( /^<\w[^>]*[^\/]>.*$/ )) {    //Find all xml start or start and end tags on a line (e.g. <Longitude>25.014</Longitude>, <out>)
                indent = 1;
            } else {
                indent = 0;
            }

            let padding = '';
            for (var i = 0; i < pad; i++) {
                padding += '  ';
            }

            formatted += padding + node + '\r\n';
            pad += indent;
        });
        return formatted;
    }

    checkForLongLat(xmlText){
        let re = /<Longitude>(\d{1,3}\.?\d*)<\/Longitude><Latitude>(\d{1,3}\.?\d*)<\/Latitude>/gmi;   //regular expression to find all Long and Lats
        let found = undefined;
        let locations = [];
        while((found = re.exec(xmlText)) != null){
            locations.push([Number(found[1]), Number(found[2])]);   //Longitude then Lat. Not it's not found[0] because that'd be the entire
            //if(Number(found[1])>160)
                //console.log("Long: "+Number(found[1])+"\tLat:"+Number(found[2]));                                                        //<Longitude>DD.DD<Lopngitude><Latitude>DD.DD<Latitude> expression
        }
        console.log("locations len")
        console.log(locations.length)
        let results = new ResultsFilter(xmlText, this.returnController.mapNodes() ) //['deploymentdetails','site','speciesID','recoverydetails']);
        console.log("done building results filter");
        // this.map.deleteMarkers();       //delete all old markers on google maps

        var mytime1 = performance.now()
        this.map.addMarkers(results.getUserWantedNodes());
        var mytime2 = performance.now()
        console.log("add markers time: "+ (mytime2 - mytime1)/1000)
        //this.googleMap.addMarkersLocations(locations); //adds all the new locations found
        //this.googleMap.drawSquare(this.formData);
    }

    /**
     * The GoogleMap box has been adjusted passing up the new Dimensions
     * @param {Object} northEast
     * @param {Object} southWest
     */

    mapBoxChangedPassingUpLongLat_2(northEast, southWest){
        this.formData.setLongitudeLatitude_2(northEast, southWest);
        this.view.updateLongLat(this.formData);
    }
    mapBoxChangedPassingUpLongLat(northEast, southWest){
        this.formData.setLongitudeLatitude(northEast, southWest);
        this.view.updateLongLat(this.formData);
    }

    turnOffBlinker(intervalID){
        //alert(intervalID)
        clearInterval(intervalID)

    }

    resultFormating(response) {
        var _this = this;
        document.getElementById('resultTextAreaSimple').innerHTML = "<div id='tree_div'>"+response+"</div>";
        //console.log("append")
        //$("#resultTextAreaSimple").append("<div id='tree_div'>"+response+"</div>")
        //console.log("end append")
        var t0 = performance.now()
        $('#tree_div').jstree({'plugins':['search'],'core':{'worker':false}

        }).on('changed.jstree', function (e, data) {console.log('clicking tree')})
        //alert(data.selected);
        var t1 = performance.now()
        console.log("time for just jstree instance "+ (t1-t0)/1000)
        $('#plugins4_q')[0].style.display = 'block';

        var to = false;
        $('#plugins4_q').keyup(function () {
        if(to) { clearTimeout(to); }
        to = setTimeout(function () {
        var v = $('#plugins4_q').val();
        $('#tree_div').jstree(true).search(v);
        }, 250);
        });
    }
}



/*$(document).ready(function() {
    // 6 create an instance when the DOM is ready
    $('#resultTextAreaSimple').jstree();
    // 7 bind to events triggered on the tree
    $('#resultTextAreaSimple').on("changed.jstree", function (e, data) {
      console.log(data.selected);
    });
}); */

/*var cannedQueries = new CannedQueries();
$(document).ready(function() {
    cannedQueries.main();
}); //document.ready */

//function initMap(){
//    alert('setting map in Simple');
//    cannedQueries.setMap();
//}

