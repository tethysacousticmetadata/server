
// relational (unary) operators (prefix)
const relop = ['<', '>', '<=', '>=', '=', '!=', 'not'];

// binary operators (infix)
const dashop = ['-', '–'];
const logicop = ['and', 'or'];

// tokens for manipulating priority
const priopen = '(';
const priclose = ')';

// token index for tracking the current token being parsed.
let tokenIndex = 0;
      
// tokens from lexer tokenization of the expression
let tokens = [];   

// variable name used for building query string
let varname = '';

/**
 * A LL(1) recursive descent parser for validating and 
 * translating simple expressions 
 * 
  Assumes that the expression is related to a single variable

  The grammar rules for validating and evaluating the expression are specified 
  as follows in the Extended Backus-Naur Form (EBNF):
      1. <exp> ::= <term> { <logicop> <term> } 
         curly brackets here mean 0 or n repetition
      2. <logicop> ::= and | or
         | is the alternate symbol
      3. <term> ::= <range_term> | <relop_term> | (<exp>)
      4. <range_term> ::= number – number
      5. <relop_term> ::= <relop> number
      6. <relop> ::= > | < | >= | <= | = | != | not

  Notes:
  1) Operator precedence - 
     The combination of the above rules implies the dash and relop operators 
     have precedence over the logical operators, when the parentheses are not there.  
  2) Number could be an integer or a real number. 
      TODO: (Check the regular expression to make it work for a real number)
  3) These rules and their corresponding recursive descent parsing can be easily modified 
      or augmented to accommodate new expression formats.

  See below for a detailed explanation of the above grammar rules for simple expressions in 
  the Extended Backus-Naur Form (EBNF).
  a. The categories of the tokens (from lexer) are:
      i. Logical operators (binary), can be one of: 
          • and, or
              a. and is an AND operator
              b. or is an OR operator
      ii. The dash operator (binary), can be:
          • – 
              a. – is the dash symbol
      iii Relop operators (relational operators, unary), can be one of:
          • >, <, >=, <=, <>, =, !=, not
      iv. This int token can be any valid combination of digits:
          • int

  b. The expression (language construct) patterns are specified as follows: 
    i. term {op term} – can be just a term or any length as long as 
        pairs of op and term are added. 
    ii. A parenthesis pair may be used to group any term op term combination. 
        For example, both of the following combinations are valid:
        • term op (term op term) op term
        • term op term op (term op term)
          •  An op can be one of logical operators (specified above): 
            a. and
            b. or
    iii. A term is specified as any of the following forms:
      • int – int
        a.  int is a token that represents an integer (specified above)
        b.  This indicates a range between two integers, with an infix – (the dash symbol)
          c.  Examples: 1 – 100, 9 – 18
      • relop int
        a.  This also indicates a range of integer numbers by using relational operators. 
        b.  Examples: > 10, <=10, != 10, >=10, not 10…
      • An expression specified in b. i.
        a.  to build the expression recursively to form 
          a  series of terms with possible nested expressions in parentheses. 
        b.  The combinations of rules 1, 2, 3 above would be able to specify and implement 
            recursive nested expression pattern in b. i.
            i. e.g., ((1 - 100) or (> 100 and < 150)) and (!= 110)
    iv. Examples of the expression pattern specified above:
      • 7 - 17
      • > 90
      • (7 - 17) or > 90
      • 1 - 100 and not 50
      • > 50 or = 20  jeff: same as x = 20 or x > 50
      • (1 - 100 and != 50) or > 200
      • ((1 - 100) or > 150) and < 300
      • (5 - 100) and (not 50) and (>= 30 or (6 - 12))

  Note: again, as mentioned above, the dash and relop operators have precedence over the 
  logical operators 

  The following recursive descent parsing algorithm is a LL(1) parser, it implements one parsing 
  procedure for each one of the above non-terminals (grammar rules), starting from the top of the 
  parse tree in <exp>, then drilling into lower hierachical levels.

  These procedures work together to handle all combinations of the grammar rules, and they 
  automatically handle the nested compositions of terms with multi-level priority brackets. 
  This maybe better than the recursive approach.

  The query string is directly built along the parsing, could be easily changed to build the parse tree 
 * @param {String} expr - string to be parsed
 * @param {String} varname - variable name to be used in generating X-Query
 * @returns {String} - X-Query string
 * Returns a query string built along the parsing, could be easily changed to build the parse tree
 */
const recDescent = (expr, vname) => {
  //reset token index

  if (expr.match(/.*[,].*/)){
    newstring = '(='+expr.replace(/[,]/g, ' or =')+')'
    //expr = '(' + newstring + ')';
    expr = newstring;
  }
  //alert(expr)
  tokenIndex = 0;

  varname = vname;  

  // execute parsing

  let endString = parse(expr)
  //alert(endString)
  let matchArray = endString.match(/(?<=(?<==)\s)[a-zA-Z]*/g) // for quoting characters
  if (Array.isArray(matchArray)) {
    if (matchArray.length > 0) {
      replacedReturn = endString.replace(/(?<=(?<==)\s)[a-zA-Z]*/g, function (x) {
        if (!isNumber(x) && x.length) {
          return '"' + x + '"'
        } else {
          return x
        }
      })
    } else {
      replacedReturn = endString
    }
  } // end of isArray
  else {
    replacedReturn = endString;
  }
  return replacedReturn

};

/** 
 * @returns {String} Query string built from parsing
 **/
const parse = (expr) => {
  lex(expr);
  return exp().parsedStruc;
};

/** 
 * lexer - tokenize the expression into a list of tokens
 **/ 
const lex = (expr) => {
  const regex = /[-\(\)=]|[!<>]=|\w+|[^ +]\W+/g;
  tokens = expr.match(regex).map(s => s.trim()).filter(s => s.length);
  //tokens = expr.match(regex).map(s => s.trim().toLowerCase()).filter(s => s.length);
  console.log("TOKENS")
  console.log(tokens)
};

/**
 * recursive descent parsing procedures corresponding to the grammar rules 
 * <exp> parsing procedure corresponding to rule 1 (see above)
 * @returns {parseRes} Part of the query string built from this procedure
 * */ 
const exp = () => {

  let found = false;
  let parsedstr = '';

  // expecting a term first
  let pt1 = term();
      
  if (pt1.valid) {
    found = true;
    parsedstr += pt1.parsedStruc;

    let l = logicop_p();

    while (l.valid && found) {
      // build query expression 
      parsedstr += " " + l.parsedStruc;

      // advance to next token
      tokenIndex += 1;
      // expecting another term after an logical operator
      let pt2 = term();

      if (!pt2.valid) {
        found = false;
        throw `Expecting the start of a range term (a number),
                or a relational term (an unary operator), 
                or a left parenthesis, ` + parsingContext();
      } else {
        parsedstr += ' ' + pt2.parsedStruc;
        l = logicop_p();
      }
    }
  }

  return parseRes(found, parsedstr);
};

/**
 * <logicop> parsing procedure corresponding to rule 2 (see above)
 * @returns {parseRes} Part of the query string built from this procedure
 * */ 
const logicop_p = () => {
  let found = false;
  let parsedstr = '';

  let nextToken = getNextToken();

  if (logicop.includes(nextToken)) {
      found = true;
      parsedstr = nextToken;
  }

  return parseRes(found, parsedstr);
};

/**
 * <term> parsing procedure corresponding to rule 3 (see above)
 * @returns {parseRes} Part of the query string built from this procedure
 */
const term = () => {

  let found = false;
  let parsedstr = ''

  // a term can be either a range_term, a relop_term, 
  // or an exp enclosed in parentheses (exp) 
  let r = range_term();
  
  if (r.valid) {
    found = true;
    parsedstr = r.parsedStruc;
    // advance to next token
    tokenIndex += 1;
  } else {
    let rl = relop_term(); 
    if (rl.valid) {
      found = true;
      parsedstr = rl.parsedStruc;
      // advance to next token
      tokenIndex += 1; 
    } else {
      // expecting an (exp) here
      let nextToken = getNextToken();
      if (nextToken === priopen) {
        parsedstr += priopen;
        // advance to next token
        tokenIndex += 1;
        // an expression is expected here
        let e = exp();
        //alert('e.valid '+e.valid)
        if (e.valid) {

          parsedstr += e.parsedStruc;

          nextToken = getNextToken();
          if (nextToken === priclose) {
            found = true;
            parsedstr += priclose;
            // advance to next token
            tokenIndex += 1;
          } else 
            throw `Expecting a right parenthesis, ` + parsingContext();
        }
      } else {
        if (nextToken !== null && nextToken !== '') 
          throw `Expecting the start of a range term (a number), 
                or a relational term (an unary operator), 
                or a left parenthesis, ` + parsingContext();
      }
    }
  }

  return parseRes(found, parsedstr);
};

/**
 * <range_term> parsing procedure corresponding to rule 4 (see above)
 * Important note: here the translated range term does NOT have parentheses around it
 * You may consider to automatically enclose the translated term with parentheses 
 * to ensure precedence when preceded or followed by a logical and / or operator.  
 * @returns {parseRes} Part of the query string built from this procedure
 */
const range_term = () => {
  
  let found = false;
  let parsedstr = '';

  let num1 = getNextToken();
  let num2 = '';
  if (isNumber(num1)) {
    // advance to next token
    tokenIndex += 1;

    let nextToken = getNextToken();
    if (dashop.includes(nextToken)) {
        // advance to next token
        tokenIndex += 1;

        num2 = getNextToken();
        if (isNumber(num2)) 
            found = true;
        else
            throw `Expecting a number for the range here, ` + parsingContext();
    } else 
        throw `Expecting a dash operator for the range here, ` + parsingContext();
  }
  
  if (found)
      // sorry for hardcoded string here.
      /*Important note: here the translated range term does NOT have parentheses around it
       * You may consider to automatically enclose the translated term with parentheses 
       * to ensure precedence when preceded or followed by a logical and / or operator.*/
      // parsedstr = priopen + varname + ' >= ' +  num1 + 
      //' and ' + varname + ' <= ' + num2 + priclose;
      parsedstr = varname + ' >= ' +  num1 + 
                  ' and ' + varname + ' <= ' + num2; 

  return parseRes(found, parsedstr);
};

/**
 * <relop_term> parsing procedure corresponding to rule 5 (see above)
 * @returns {parseRes} Part of the query string built from this procedure
 */
const relop_term = () => {
  
  let found = false;
  let parsedstr = '';

  let r = relop_p();
  let num = '';
  if (r.valid) {
    // advance to next token
    tokenIndex += 1;

    num = getNextToken();
    //if (isNumber(num))   // COMMENTED THIS OUT
      found = true;
    //else
      //throw `Expecting a number for the relational term here, ` + parsingContext();
  }
  
  if (found)
      // sorry for hardcoded string here.
      parsedstr = varname + ' ' +  r.parsedStruc + ' ' + num; 

  return parseRes(found, parsedstr);
};

/**
 * <relop_p> parsing procedure corresponding to rule 6 (see above)
 * @returns {parseRes} Part of the query string built from this procedure
 */
const relop_p = () => {
  
  let found = false;
  let parsedstr = '';

  let nextToken = getNextToken();
  if (relop.includes(nextToken)) {
    found = true;
    parsedstr = nextToken;

    if (parsedstr === 'not') 
      parsedstr = '!=';
  }
  
  return parseRes(found, parsedstr);
};

/**
 * @param {String} str 
 * @returns {boolean} check if a string is a valid number 
 */
const isNumber = str => {
  if (typeof str != "string") return false; // we only process strings!  
  return !isNaN(str) && // use type coercion to parse the _entirety_ of the string (`parseFloat` alone does not do this)...
         !isNaN(parseFloat(str)); // ...and ensure strings of whitespace fail
};

/**
 * @param {boolean} valid 
 * @param {String} parsedstr 
 * @returns Returns a parsed result object containing whether parsing is valid, 
 * and the parsed structure in string
 */
const parseRes = (valid, parsedstr) => ({ valid: valid, parsedStruc: parsedstr });

/** 
 * @returns {String} get the next token
 * */ 
const getNextToken = () => {
  if (tokenIndex >= 0 && tokenIndex < tokens.length) 
      return tokens[tokenIndex];
  else
      return '';
};

/** 
 * @returns {String} get the parsed portion of the expression
 * */ 
const getParsedExpr = () => {
  if (tokenIndex >= 0 && tokenIndex <= tokens.length)
      return tokens.slice(0, tokenIndex).join(" ");
  else
      return '';
};

/** 
 * @returns {String} return the parsing context where the parsing is happening
 * */ 
const parsingContext = () => {
return `Token being parsed: ` + getNextToken() + 
       ` portion of expression parsed so far: ` + getParsedExpr();
};

// Here are some example of running the parsing algorithm to 
// generate a translated query string based on the passed-in expression:
function unit_test() {
  /* Not really a unit test as we are not returning pass/fail for anything */

  // Positive testing cases:

  let exprt = '7 - 17';
  console.log(recDescent(exprt, 'v'));

  exprt = '> 90';
  console.log(recDescent(exprt, 'v'));

  exprt = '(7 - 17) or > 90';
  console.log(recDescent(exprt, 'v'));

  exprt = '1 - 100 and not 50';
  console.log(recDescent(exprt, 'v'));

  exprt = '> 50 or = 20';
  console.log(recDescent(exprt, 'v'));

  exprt = '(1 - 100 and != 50) or > 200';
  console.log(recDescent(exprt, 'v'));

  exprt = '((1 - 100) or > 150) and < 300';
  console.log(recDescent(exprt, 'v'));

  exprt = '(5 - 100) and (not 50) and (>= 30 or (6 - 12))';
  console.log(recDescent(exprt, 'v'));

  exprt = '((1 - 100) or (> 100 and < 150)) and (!= 110)';
  console.log(recDescent(exprt, 'v'));

  // Negative testing cases: (some other error cases need to be tested)
  try {
    exprt = '7 - ';
    console.log(recDescent(exprt, 'v'));
  } catch (err) {
    console.log(err);
  };

  try {
    exprt = '> ';
    console.log(recDescent(exprt, 'v'));
  } catch (err) {
    console.log(err);
  };

  try {
    exprt = '(7 - 17';
    console.log(recDescent(exprt, 'v'));
  } catch (err) {
    console.log(err);
  };

  try {
    exprt = '(7 - 17 or > ';
    console.log(recDescent(exprt, 'v'));
  } catch (err) {
    console.log(err);
  };

  try {
    exprt = '(7 - 17) and ';
    console.log(recDescent(exprt, 'v'));
  } catch (err) {
    console.log(err);
  };

}