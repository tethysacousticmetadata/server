class Environment{

    constructor(parent,name){
        this.name = name
        this.parent = parent
        //this.frame = {}
        this.frame = []
    }

    push(element) {
        // push element into frame
        this.items.push(element);
    }

    get_root(){
        return this.frame[this.items.length - 1]
    }

    get_parent(){
        return this.parent
    }

    depth(){
        var d = 0
        current = this.get_parent()
        while (current != None)
        {
            d = d + 1
            current = current.get_parent()
        }
        return d

    }

}

