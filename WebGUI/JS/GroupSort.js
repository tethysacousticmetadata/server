/**
 * @author Daniel Weston
 * @version 1.0
 * DSanchezWeston@gmail.com
 * With the help of Professors Roger Whitney and Marie Roch. San Diego State University.
 * 23 Aug 2018
 */
"use strict";

/**
 * a class that builds a xQuery to be submitted to a Tethys server. 
 * The query can have multiple nested queries.
 */
class GroupSort{
    
    constructor(whereData, returnData){
        this.whereData =  whereData;
        this.returnData = returnData;
        this.whereSet = [];
        this.groups = [];
    }

    /**
     * With
     * @param {Object} d3Json instance with both the xml and d3 JSON trees stored within it.
     * @return {String} a complete FLWOR XQuery
     */
    sortWhere(d3Json){
        //find all nodes in return and where conditions
        
        var returnSet = [];
        this.returnData.forEach(element => {    //all return paths
            returnSet.push(element.getNode());
        }); //all nodes are found
        // #################################################################  RETURN START  ###################################
        for(var index in returnSet){    //adding returnCount keys to D3JSON and incrementing everytime a leaf to root path intersects
            this.toRootCounting("returnCount", returnSet[index]);
        }

        for(var index in returnSet){    //building nested queries
            this.safeAddFlworNode(returnSet[index], returnSet[index]); //mark nodes with maxOccurs key and unbounded value. Eventually create a flwor for it.
            this.toRootSetupQueries(returnSet[index]);
        }
        
        // ###################################  RETURN END  ################   WHERE START  ###################################
        this.whereSet = this.whereData;
        for(var index in this.whereSet)    //adding count to D3JSON
        {
            this.toRootCounting("whereCount", this.whereSet[index].getNode());
        }
        d3Json['forVar'] = 1;
        
        //START sorting
        this.down( d3Json, d3Json);
        if(this.whereSet.length)
            this.groups.push(this.whereSet);
        console.log("groups: " + this.groups);
        
        return this.groups;
    }

    /**
    * A down tree recursive function 
    * @param {Object} node the current node in the traversal
    * @param {Object} lastFlworNode the next node above @var node that a FLWOR statement will be created at
    * @return {Array or String} The return can be a string of a flwor, string of a leaf (bottom) node, or an
    * array of Strings that gets flattened after it's returns
    */
    down(node, lastFlworNode) {
        var childrensFlwor = [];
        var passDownLastFlworNode = lastFlworNode;
        if(node.hasOwnProperty('flworNodes'))   //pre recursion going down. If node has a flworNodes key increment the forFar key value. A flower will be created at this node
        {
            node['forVar'] = lastFlworNode['forVar']+1; //
            passDownLastFlworNode = node;       
        }
        for (var childNode in node.children) {  //for each children key
            if (node.children[childNode] !== null && typeof(node.children[childNode])=="object" && node.children[childNode].hasOwnProperty('returnCount')) {    //going one step down in the object tree!!
                this.down(node.children[childNode], passDownLastFlworNode);
                //childrensFlwor.push(this.down(node.children[childNode], passDownLastFlworNode));    //Add node
                //childrensFlwor = this.flatten(childrensFlwor);  //If an array was return it'll be an array within in an array so we flattend the 2D array to 1D
            }
        }
        for (var childNode in node._children) {  //for each _children key this is a workaround if somehow the nodes are hidden after being selected. This is the exact same code as the 6 lines of code above it. It could be wrapped in a function. Lazy Programming!
            if (node._children[childNode] !== null && typeof(node._children[childNode])=="object" && node._children[childNode].hasOwnProperty('returnCount')) {    //going one step down in the object tree!!
                this.down(node._children[childNode], passDownLastFlworNode);
            }
        }
        if(node.hasOwnProperty('flworNodes'))   //post recursion coming back up.
        {
            var tempWhere = [];
            for(var whereIndex = this.whereSet.length-1; whereIndex >= 0; whereIndex--){    //for each where condition with a node under the current unbounded node 
                if(this.replacePath(this.whereSet[whereIndex].getNode(), node) != "")       //check if any where condition nodes are underneath this node
                {
                    console.log("after: "+ this.replacePath(this.whereSet[whereIndex].getNode(), node));    //used for debugging
                    childrensFlwor.push(this.whereSet[whereIndex]);  //add where condition 
                    this.whereSet.splice(whereIndex,1);         //remove the where condition from the object array
                }
            }
            
            //return childrensFlwor;
        }
        else if(childrensFlwor.length == 0)//(hasOwnProperty('count'))   //No more child nodes. Tree bedrock. Prints only tree ends (final children, children without children)
        {
            //return "$"+this.replacePath(node, lastFlworNode);
        }
        if(childrensFlwor.length>0)
            this.groups.push(childrensFlwor);
    }

    /**
     * Flattern an array within in an array into a 1D array
     * @param {Array or String} arr 
     */
    flatten(arr){   
        return [].concat(...arr);
    }

    /**
     * recusivly traverse a tree and check if it has a 'maxOccurs' key.
     * If it does check if the value of the key is 'unbounded' and add a new key
     * @param {Object} node 
     */
    toRootSetupQueries(node){
        if(node == undefined)
            console.log("ppp");
        if(node.parent == null) // Not root node
            return;
        if(node.parent.hasOwnProperty('maxOccurs'))
        {
            this.safeAddFlworNode(node.parent, node);   //adding elements for a FLWOR build
        }
        this.toRootSetupQueries(node.parent);        
    }

    /**
     * Add a flworNodes key at the node if it has a maxOccurs key and an unbounded value
     * @param {Object} node 
     * @param {Object} flworNode 
     */
    safeAddFlworNode(node, flworNode){
        if(node['maxOccurs'] == "unbounded")
            {
                if(!node.hasOwnProperty('flworNodes'))  //if node doesn't has a flworNodes key add one
                    node['flworNodes'] = [];
                node['flworNodes'].push(flworNode);
            }
    }

    /**
     * Traveling from a ndoe to a root and adding a key to that node and incrementing it if it all ready has it
     * the count is used to determine if to nodes share a node on the path to the root. The root will always equal the
     * number of nodes below it.
     * @param {String} key 
     * @param {Object} node 
     */
    toRootCounting(key,node){
        var path= "";
        if(!node.hasOwnProperty(key))   //if node doesn't have key add one
            node[key] = 0;
        node[key]++; 
        if(node.parent != null) // Not root node
        {
            this.toRootCounting(key, node.parent);
        }
    }

    /**
     * A function to recursively travel down a tree.
     * @param {Object} node 
     */
    toRoot(node){
        var path= "";
        if(node.parent != null) // Not root node
        {
            // find parent of root add parents name before yours \Deployments\DeploymentNumber\...
            return this.toRoot(node.parent)+"\/"+node.name;
        }
        return node.name;   //The Root Node
    }

    /**
     * 
     * @param {Object} node 
     * @param {Object} lastFlworNode 
     */
    replacePath(node, lastFlworNode){
        var nodePath = this.toRoot(node);                   //returns full path name
        var lastFlworPath = this.toRoot(lastFlworNode);     //returns full path name
        //console.log("before node: "+nodePath+"\tlastFlower:"+ lastFlworPath);   //used for debugging
        if(nodePath.includes(lastFlworPath)){               //if lastFlworPath is inside nodePath
            var regex = new RegExp(lastFlworPath);          //create a Regular Expression that's the lastFlworPath
            var newPath = nodePath.replace(regex, lastFlworNode['forVar'].toString());  //Deployment/SamplingDetails changes to $x2/SamplingDetails
            return newPath;
        }
        return "";
    }
}


