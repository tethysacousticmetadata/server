Object/**
 * @author Daniel Weston
 * @version 1.0
 * DSanchezWeston@gmail.com
 * 28 Aug 2018
 */
"use strict"

/**
 * This is a controller class for unorderded lists <ul><\ul> in advancedqueiries.html
 * The idea behind this class is to build the ul with the data within it. When users
 * change the data inside the ul change the data model here.
 * @see AdvancedQueiries
 * The Controller of Model, View, Controller (MVC)
 * @see ULData  are the Model(Data)
 * @see WhereView is the view
 * @see WhereController is the Controller
 */
class WhereController{

    /**
     * @param {AdvancedQueries} parent
     * @param {String} id "WhereView"
     * @var {Array[Array[ULData]]}groupsData holds all the information of the whereViews elements
     * @var {WhereView} whereView the view object of the multiple <ul>'s. All <ul>'s get created with the @var groupData
     */
    constructor(parent, id){
        this.parent = parent;
        this.id = id;
        this.pathsThatCanNotBeAdded = [];
        this.groupsData = [];
        this.whereView = new WhereView(this, this.id);
    }

    addGroup(groupData){
        this.groupsData.push(groupData);
        this.buildView();
    }

    /**
     * Called after an algorithm finds all the nested nodes and seperates the nested nodes into their
     * corresponding groups. @see AdvancedQueries.groupSort
     * @param {Array[Array[ULData]]} datasForGroups 
     */
    setGroups(datasForGroups)
    {
        this.groupsData = datasForGroups;
    }


    setPathsThatCanNotBeAdded(list){
        this.pathsThatCanNotBeAdded = list; 
    }
    /**
     * Flattens the data and therefor the view so the users can interact with
     * the conjunctions inbetween @see AdvancedQueries.groupSort
     */
    unGroupGroups()
    {
        let temp = this.getData();
        this.groupsData = [];
        this.groupsData[0] = temp;
    }

    /**
     * Update @var groupsData with a new ULData element. This function is called when the user adds a 
     * new condition to the wehere conditions. Either after alt clicking of drag and dropping a div onto
     * teh where condition. @see jquery_ui2.setWhereListeners and @see D3TreeController.click
     * @param {Object} node Object of a d3 node in the D3 tree structure
     * @param {String} path String of the Object node path from its root element ("Deployment/DeploymentNumber")
     */
    addUngrouped(node, path){
        let addAutocomplete = true;
        let _this = this;
        this.pathsThatCanNotBeAdded.forEach(badPath =>{
            let regEx  = new RegExp('^'+badPath+'[\/a-z]*', 'i');
            if(regEx.test(path)){
                addAutocomplete = false;
                //_this.parent.errorReport("Node can NOT be added to Where Condition");
            }
        });
        this.groupsData.push([new ULData(this, this.id, node, path, "", "", "", addAutocomplete)]);
        this.parent.groupSort();
    }

    /**
     * Builds multiple <ul> with <li> them corresponding to the ULData stored in @var groupsData
     * The builds also include call back function to activate @callback removeRow, @callback updateOperatorSelect,
     * @callback updateInputValue, @callback updateOperatorSelect
     * @var {whereView} whereView the unordered list element 
     * @var {Array[Array[ULData]]} groupsData the data of the <ul> element
     */
    buildView(){
        this.whereView.buildView(this.groupsData);
    }

    /**
     * Removes a group from the data if there are no more elements in the group.
     * @param {Integer} index 
     */
    removeGroup(index){
        this.groupsData.splice(index,1);    //remove index from the array
    }

    /**
     * This functions should do nothing. It gets invoked when someone clicks on the
     * path div. In the Return controller it should change the background of the view, but
     * this isn't a behavior wanted in the whereView so we just provide a function to 
     * catch the call and end it by doing nothing.
     * @param {Integer} rowIndex 
     */
    toggleBackgroundColor(rowIndex){    
    }

    /**
     * Removes a row from the data. Typically after a "X" button was pressed in the UI
     * @param {Integer} index 
     */
    removeRow(groupIndex, rowIndex){
        this.groupsData[groupIndex].splice(rowIndex,1);
        if(this.groupsData[groupIndex].length == 0)
            this.removeGroup(groupIndex);
        this.buildView();
    }

    /**
     * Users changed the value of the operator selector. Method updates the data object
     * to represent that change.
     * @param {Integer} index 
     * @param {String} value 
     * @var {Array[ULData]} datas the data of the <ul> element
     */
    updateOperatorSelect(groupIndex, rowIndex, value){
        if(this.groupsData.length>0){
            this.groupsData[groupIndex][rowIndex].setOperatorValue(value);
            //this.buildView();
        }
    }

    /**
     * Users changed the value of the input. Method updates the data object
     * to represent that change.
     * @param {Integer} index 
     * @param {String} value 
     */
    updateInputValue(groupIndex, rowIndex, value){
        if(this.groupsData.length>0){
            this.groupsData[groupIndex][rowIndex].setInputValue(value);
            //this.buildView();
        }
    }

    /**
     * Users changed the value of the conjunction selector. Method updates the data object
     * to represent that change.
     * @param {Integer} index 
     * @param {String} value 
     */
    updateConjunctionSelect(groupIndex, rowIndex, value){
        if(this.groupsData.length>0){
            this.groupsData[groupIndex][rowIndex].setConjunctionValue(value);
            //this.buildView();
        }
    }

    /**
     * Not currently used. But clears all the data and the views
     */
    clear(){
        this.groupsData = undefined;
        this.groupsData = [];
        this.buildView();
    }

    /**
     * @var {Array[ULData]} data the data of the <ul> element
     */
    getData(){
        return this.flatten(this.groupsData);
    }

    /**
     * For each index in data build a string representation of it.
     * @var {String} buildString
     * @var {Array[ULData]} data the data of the <ul> element
     */
    toString(){
        var buildString = "";
        this.datas.forEach(element => {
            buildString += element.toString();
        });
        return buildString;
    }

    assignAutoComplete(){
        this.whereView.assignAutoComplete(this.groupsData);
    }

    /**
     * Flattern an array within in an array into a 1D array
     * @param {Array or String} arr 
     */
    flatten(arr){   
        return [].concat(...arr);
    }
}