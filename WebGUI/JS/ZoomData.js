/**
 * @author Daniel Weston
 * @version 1.0
 * DSanchezWeston@gmail.com
 * 28 Aug 2018
 */

'use strict';

/**
 * Currently as is this is not being used. This was a class idea for teh D3 zoom function, but winded up being uneccesary.
 * Check the link below for more information.
 * https://github.com/d3/d3-3.x-api-reference/blob/master/Zoom-Behavior.md
 */
class ZoomData{
    constructor(x, y, scale){
        this.current = {
            x: x,
            y: y,
            zoomScale: scale,
        };
        this.last = {
            x: x,
            y: y,
            zoomScale: scale,
        };
    }

    newPoint(point, scale){
        this.last.x = this.current.x;
        this.last.y = this.current.y;
        this.last.zoomScale = this.current.zoomScale;
        this.current.x = point[0];
        this.current.y = point[1];
        this.current.zoomScale = scale;
    }

    static get minZoom(){
        return 0.5;
    }

    static get maxZoom(){
        return 3;
    }

    get x(){
        return this.last.x;
    }

    get y(){
        return this.last.y;
    }

    get point(){
        return [this.x, this.y];
    }

    get zoomScale(){
        return this.last.zoomScale;
    }

}