/**
 * @author Daniel Weston
 * @version 1.0
 * DSanchezWeston@gmail.com
 * 28 Aug 2018
 */
"use strict"

/**
 * The view portion of a ULData object. Row Class is going to build a listitem <li> with a path, operatorSelect, value input, conjunction select, and a remove button
 * All rows should be part of a an UnorderedList
 * 
 */

class ConjunctionRow{

    /**
     * 
     * @param {UnorderedListView} parent parent element an unordered list <ul> that contains this element <li>
     * @param {String } id  id attribute of list item <li id = "whereCondition0">
     * @param {Integer} index index of list item. Used when an event happens and the information needs to be passed up to the controller
     */
    constructor(parent, ulData, index){
        this.parent = parent;
        this.id = ulData.getId();
        this.index = index;
        if(ulData.getConjunctionValue())
            this.conjunctionValue = ulData.getConjunctionValue();
        else
            this.conjunctionValue = "and";
        
        this.listItem = document.createElement("li");
        this.listItem.setAttribute("id" , "conjunctionRow" + index);
        this.listItem.setAttribute("class" , "d-flex justify-content-center mb-1");
        this.buildConjunction();
    }

    /**
     * Builds the select part of the list item <select id = , class = , >
     * <option></option></select>
     * Attaches an event handler to update the data in the controller everytime the value changes
     * @param {Integer} index 
     * @param {Boolean} disable 
     */
    buildConjunction(){
        if(this.disable) //last row doesn't need a conjunction
            return;
        this.conjunctionSelect = document.createElement("select");
        this.conjunctionSelect.setAttribute("id", 'conjunction'+this.index);
        this.conjunctionSelect.setAttribute("class","conjunction custom-select");
        
        var options = {
            "and": "and",
            "or": "or"
        }
        this.conjunctionSelect = this.buildSelect(this.conjunctionSelect, options);
        this.conjunctionSelect.value = this.conjunctionValue;

        /**
         * Function closure to add the correct function on the EventListener
         * @param {UnorderedListView} parent 
         * @param {Integer} index 
         */
        function updateConjunctionSelect(parent, index) {
            return function() {
                parent.updateConjunctionSelect(index, this.value);
            }
        }
        this.conjunctionSelect.addEventListener("change", 
            updateConjunctionSelect(this.parent, this.index));
        this.listItem.appendChild(this.conjunctionSelect);
    }

    /**
     * Multiple selects elements are created. An individual function to fill the options of the select.
     * @param {Element} selectBox   select element thats goinf to have all the options added to it
     * @param {ObjectArray} options key, value pairs to populate the options of the select
     */
    buildSelect(selectBox, options){
        for(var key in options)
        {
            var opt = document.createElement("option");
            opt.setAttribute("value", options[key]);
            opt.appendChild(document.createTextNode(key));
            selectBox.appendChild(opt);
        }
        return selectBox;
    }

    /**
     * @return {HTMLElement}
     */
    getListItem()
    {
        return this.listItem;
    }
}