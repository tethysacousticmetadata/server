/**
 * @author Daniel Weston
 * @version 1.0
 * DSanchezWeston@gmail.com
 * 5 Oct 2018
 */

 'use strict'

 /**
  * Created this wrapper to reduce copying code everytime an AJAX call is made
  */
 class AjaxWrapper{

    /**
     * 
     * @param {String} httpMethod Either "get" or "post"
     * @param {FormData} form the XQuery that'll be sent to Tethys
     * @param {Function} errorCallback if an error happens callback to this functions
     */
    constructor(httpMethod, form, errorCallback){
        if (httpMethod.toLowerCase() == "post") {    //create a post
            this.settings = {   //these are all the parameters needed for a jQuery $.ajax()
                "async": AjaxWrapper.DEFAULT_ASYNC,
                "crossDomain": AjaxWrapper.DEFAULT_CROSS_DOMAIN,
                "url": AjaxWrapper.DEFAULT_URL,
                "method": AjaxWrapper.DEFAULT_METHOD,
                "headers": AjaxWrapper.DEFAULT_HEADER, //can probably be deleted
                "processData": AjaxWrapper.DEFAULT_PROCESS_DATA,
                "contentType": AjaxWrapper.DEFAULT_CONTENT_TYPE,
                "mimeType": AjaxWrapper.DEFAULT_MIME_TYPE,
                "data": form,    //adding the query that we setup above
                "error": errorCallback,
            }
        }
        else if (httpMethod.toLowerCase() == "get") {
            this.settings = {   //these are all the parameters needed for a jQuery $.ajax()
                "async": AjaxWrapper.DEFAULT_ASYNC,
                "crossDomain": AjaxWrapper.DEFAULT_CROSS_DOMAIN,
                "url": AjaxWrapper.DEFAULT_URL,
                "method": "GET",
                "headers": AjaxWrapper.DEFAULT_HEADER, //can probably be deleted
                "error": errorCallback,
            }
        }
        else if (httpMethod.toLowerCase() == "import") {
            this.settings = {   //these are all the parameters needed for a jQuery $.ajax()
                "async": AjaxWrapper.DEFAULT_ASYNC,
                "crossDomain": AjaxWrapper.DEFAULT_CROSS_DOMAIN,
                "url": "specify_collection/import",  // Must be set by caller
                "method": AjaxWrapper.DEFAULT_METHOD,
                "headers": AjaxWrapper.DEFAULT_HEADER, //can probably be deleted
                "processData": AjaxWrapper.DEFAULT_PROCESS_DATA,
                "contentType": AjaxWrapper.DEFAULT_CONTENT_TYPE,
                "mimeType": AjaxWrapper.DEFAULT_MIME_TYPE,
                "data": form,    //adding the query that we setup above
                "error": errorCallback,
            }
        }
        else if(httpMethod.toLowerCase() == "postconnectionstring"){
            this.settings = {   //these are all the parameters needed for a jQuery $.ajax()
                "async": AjaxWrapper.DEFAULT_ASYNC,
                "crossDomain": AjaxWrapper.DEFAULT_CROSS_DOMAIN,
                "url": "/dbJSON",
                "method": AjaxWrapper.DEFAULT_METHOD,
                "headers": AjaxWrapper.DEFAULT_HEADER, //can probably be deleted
                "processData": AjaxWrapper.DEFAULT_PROCESS_DATA,
                "contentType": AjaxWrapper.DEFAULT_CONTENT_TYPE,
                "mimeType": AjaxWrapper.DEFAULT_MIME_TYPE,
                "data": form,    //adding the query that we setup above
                "error": errorCallback,
            }
        }                                           //create a get
        else if(httpMethod.toLowerCase() == "saveimportconfig"){
            this.settings = {   //these are all the parameters needed for a jQuery $.ajax()
                "async": false,
                "crossDomain": AjaxWrapper.DEFAULT_CROSS_DOMAIN,
                "url": "/ImportConfig",
                "method": "POST",
                "headers": AjaxWrapper.DEFAULT_HEADER, //can probably be deleted
                "processData": AjaxWrapper.DEFAULT_PROCESS_DATA,
                "contentType": AjaxWrapper.DEFAULT_CONTENT_TYPE,
                "mimeType": AjaxWrapper.DEFAULT_MIME_TYPE,
                "data": form,
                "error": errorCallback,
            }
        }                                         //create a get
        else if(httpMethod.toLowerCase() == "deleteimportconfig"){
            this.settings = {   //these are all the parameters needed for a jQuery $.ajax()
                "async": false,
                "crossDomain": AjaxWrapper.DEFAULT_CROSS_DOMAIN,
                "url": "/ImportConfig",
                "method": "POST",
                "headers": AjaxWrapper.DEFAULT_HEADER, //can probably be deleted
                "processData": AjaxWrapper.DEFAULT_PROCESS_DATA,
                "contentType": AjaxWrapper.DEFAULT_CONTENT_TYPE,
                "mimeType": AjaxWrapper.DEFAULT_MIME_TYPE,
                "data": form,
                "error": errorCallback,
            }
        }
        else if(httpMethod.toLowerCase() == "getimportconfig")
        {
            this.settings = {   //these are all the parameters needed for a jQuery $.ajax()
                "async": AjaxWrapper.DEFAULT_ASYNC,
                "crossDomain": AjaxWrapper.DEFAULT_CROSS_DOMAIN,
                "url": '/ImportConfig',
                "method": "GET",
                "data": form,
                "headers": AjaxWrapper.DEFAULT_HEADER, //can probably be deleted
                "error": errorCallback,
            } 
        }
    }

    static get DEFAULT_PORT() {
        return "9779";
    }

    static get DEVELOPER_PORT() {
        return "9780";
    }

    static get DEFAULT_ASYNC() {
        return true;
    }

    static get DEFAULT_CROSS_DOMAIN() {
        return false;
    }

    static get DEFAULT_URL() {
        return "/XQuery";
    }

    static get DEFAULT_METHOD() {
        return "POST";
    }

    static get DEFAULT_HEADER() {
        return {
            "Cache-Control": "no-cache",
            "Postman-Token": "84d21639-f3df-4d07-923e-25fa5b8e29d4" //can probably be deleted
        };
    }

    static get DEFAULT_PROCESS_DATA() {
        return false;
    }

    static get DEFAULT_CONTENT_TYPE() {
        return false;
    }

    static get DEFAULT_MIME_TYPE() {
        return "multipart/form-data";
    }

    setURL(url){
        this.settings['url'] = url;
    }

    setSettings(settings){
        this.settings = settings;
    }

    getSettings(){
        return this.settings;
    }

    /**
     * Turns a string of XML into a XMLDocument
     * @param {String} oString 
     * @return {XMLDocument}
     */
    static StringToXML(oString) {
        //code for IE
        if (window.ActiveXObject) { 
            var oXML = new ActiveXObject("Microsoft.XMLDOM"); oXML.loadXML(oString);
            return oXML;
        }
        // code for Chrome, Safari, Firefox, Opera, etc. 
        else {
            return (new DOMParser()).parseFromString(oString, "text/xml");
        }
    }
 }