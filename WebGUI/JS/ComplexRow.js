/**
 * @author Daniel Weston
 * @version 1.0
 * DSanchezWeston@gmail.com
 * 28 Aug 2018
 */
"use strict"

/**
 * Row Class is going to build a listitem <li> with a path, operatorSelect, value input, conjunction select, and a remove button
 * All rows should be part of a an UnorderedList adn each interactive element should jave an eventlistener.
 * 
 */

class ComplexRow extends SimpleRow{
    /**
     * All the information that's going to be put inside the <li>
     * @param {UnorderedListView} parent    parent Object used to send up event form event listeneres
     * @param {String} operatorValue        = , !=, < ,<=, >, >=, exists, not
     * @param {String} inputValue           string inside the <input> element
     * @param {String} conjunctionValue     AND or OR
     * @param {Integer} index               index added to id to make unique id
     */
    constructor(parent, ulData, index){
        super(parent, ulData, index);       //parent, id, path, index, isMap all set 
                                            //addColumnId() also called in super
        this.listItem.classList.remove("justify-content-end");
        this.operatorSelect;    //<select id = "operatorSelector#" class = "complex custom-select">
        this.valueInput;        //<input id="value#" class = "ui-automcomple-input>"
        this.conjunctionSelect; //<select id = "conjunction0" class = "conjunction custom-select">
        this.buildRow();        //build the entire <li>...</li> HTML Element
    }

    /**
     * This will call all the functions to build each piece of the complete <li></li>
     * the complete structure should look like:
     * <li class = "whereView#" class= "whereView list-group-item input-group d-inline-flex">
     *      <div class = "input-group-prepend"> //Makes the left side be rounded
     *          <span></span>
     *      </div>
     *      <select></select>
     *      <input></input>
     *      <select></select>
     *      <div class = "input-group-append">  //Makes the right side be rounded
     *          <button id = "removeButton#" class = "btn btn-sm btn-dark"></button>
     *      </div>
     * </li>
     */
    buildRow(){
        this.listItem.innerHTML = "";
        //this.listItem.setAttribute("class", "list-group-item");
        this.buildPath();
        this.buildOperator();
        if(this.ulData.getOperatorValue() != "exists")
            this.buildValue();
        this.buildRemoveButton();
        this.buildMagGlassButton();
        this.listItem.appendChild(this.pathDiv);
        this.listItem.appendChild(this.operatorSelect);
        this.listItem.appendChild(this.findDiv);
        if(this.ulData.getOperatorValue() != "exists")
            this.listItem.appendChild(this.valueInput);
        this.listItem.appendChild(this.removeDiv);

    }

    /**
     * Builds the second child element inside the list item
     * Structure looks like <select id = this.colulmnId['operator']+index class = "operator btn btn-info btn-outline-primary btn-sm custom-select"
     * <option value = options[key]>key<option>
     * </select>
     * Attaches an event handler to update the data in the controller everytime the value changes
     */
    buildOperator(){
        let _this = this;
        this.operatorSelect = document.createElement("select");
        this.operatorSelect.setAttribute("id", 'operatorSelect'+this.index);
        this.operatorSelect.setAttribute("class","complex custom-select");
        this.operatorSelect.setAttribute("maxlength", "10");
        let options = {
            "=": "=",
            "!=": "!=",
            "≥":">=",
            ">":">",
            "≤":"<=",
            "<":"<",
            "not":"not",
            "exists":"exists"
        }
        this.operatorSelect = this.buildSelect(this.operatorSelect, options);
        this.operatorSelect.value = this.ulData.getOperatorValue();
        this.operatorSelect.addEventListener("change", function(){
            _this.parent.updateOperatorSelect(_this.index, this.value);
        });
    }

    /**
     * Multiple selects elements are created. Created an individual function to fill the options of the select.
     * @param {Element} selectBox   select element thats goinf to have all the options added to it
     * @param {ObjectArray} options key, value pairs to populate the options of the select
     */
    buildSelect(selectBox, options){
        for(var key in options)
        {
            var opt = document.createElement("option");
            opt.setAttribute("value", options[key]);
            opt.appendChild(document.createTextNode(key));
            selectBox.appendChild(opt);
        }
        return selectBox;
    }

    /**
     * Builds <input id = , type = , class = , name= , size = , maxlength = , value = ></input>
     * Attaches an event handler to update the data in the controller everytime the value is blurred
     * @param {String} value 
     * @param {Integer} index 
     */
    buildValue(){
        var _this = this;
        this.valueInput = document.createElement("INPUT");
        this.valueInput.setAttribute("type", "text");
        this.valueInput.setAttribute("id", 'value'+this.index);
        //this.valueInput.setAttribute("class", "form-control");
        this.valueInput.setAttribute("class", "ui-autocomplete-input");
        //this.valueInput.setAttribute("name", "value");
        if(this.valueInput != "")
            this.valueInput.setAttribute("value", this.ulData.getInputValue());
            this.valueInput.addEventListener("blur", function(){
                _this.parent.updateInputValue(_this.index, _this.valueInput.value);
        });
        //$( function() {
            
         // } );
        
    }

    addAutoCompleteListener(){
        let _this = this;
        this.autocompleteData= this.ulData.getAutocompleteArr();
        if(this.autocompleteData){
            $( this.valueInput ).autocomplete({
                source: _this.autocompleteData,
            });
        }
    }
    
}
