
navbar = `

    <nav id="mainNavBar" class="navbar navbar-expand navbar-light">
    <span class="navbar-brand" >
    NavBarTitle
    </span>

    <div class="dropdown float-right ml-auto mr-1">
        <button class="btn btn-outline-* dropdown-toggle" type="button" id="navMenuButton"
                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <img src="../images/Tethys.png" alt="Tethys Antioch mosaic" height="35"></img>
        </button>
        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navMenuButton">
            <li>
                <a class="dropdown-item"  href="query.html">Query</a>
            </li>
            <li>
                <a class="dropdown-item" href="import.html">Import data</a>
           </li>
            <li>
                <a class="dropdown-item" href="importmap.html" title="Develop mappings between your data and Tethys fields.">Plan data import</a>
            </li>
            <li>
               <a class="dropdown-item" 
               href=javascript:void(window.open(window.location.protocol+"//"+window.location.host+"/Documentation"))>
               Documentation
               </a>
            </li>
            <li>
               <a class="dropdown-item" href=javascript:void(window.open("https://tethys.sdsu.edu/tutorials/"))>
               Tethys Tutorial Videos
               </a>
            </li>
            <li>
                <div class="dropdown-submenu dropdown-submenu-right"
                     title="Retrieve XML from specific documents in one of the collections below">
                    Document listings
                    <li> <a class="dropdown-item"
                            href=javascript:void(window.open(window.location.protocol+"//"+window.location.host+"/Calibrations"))>Calibrations</a>
                    </li>
                    <li> <a class="dropdown-item"
                            href=javascript:void(window.open(window.location.protocol+"//"+window.location.host+"/Deployments"))>Deployments</a>
                    </li>
                    <li> <a class="dropdown-item"
                            href=javascript:void(window.open(window.location.protocol+"//"+window.location.host+"/Detections"))>Detections</a>
                    </li>
                    <li> <a class="dropdown-item"
                            href=javascript:void(window.open(window.location.protocol+"//"+window.location.host+"/Ensembles"))>Ensembles</a>
                    </li>
                    <li> <a class="dropdown-item"
                            href=javascript:void(window.open(window.location.protocol+"//"+window.location.host+"/Localizations"))>Localizations</a>
                    </li>
                    <li> <a class="dropdown-item"
                            href=javascript:void(window.open(window.location.protocol+"//"+window.location.host+"/SourceMaps"))>SourceMaps</a>
                    </li>
                    <li> <a class="dropdown-item"
                            href=javascript:void(window.open(window.location.protocol+"//"+window.location.host+"/SpeciesAbbreviations"))>SpeciesAbbreviations</a>
                    </li>
                </div>
            </li>
        </div>
    </div>
</nav>  <!--   //  Main Navbar-->
`
function addNavBar(title) {
    let titledNavBar = navbar.replace("NavBarTitle",title)
    document.write(titledNavBar)
}
