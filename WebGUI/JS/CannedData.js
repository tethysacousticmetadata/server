/**
 * @author Daniel Weston
 * @version 1.0
 * DSanchezWeston@gmail.com
 * 3 Oct 2018
 */

"use strict"

/**
 * @classdesc This is the model of MVC for cannedQueries data
 * @todo a lot. More data and potentially making this a super class
 * for multiple prebuild canned queries.
 */
class CannedData{

    constructor(){
       
        this.deploymentAndDetections = {

            "longitude": {
                "min" : -179,
                "max" : -1,
            },

            "latitude": {
                "min": -50,
                "max": 50,
            },
        };

        this.weeklyDetectionPlot = {
        "species": [''],
        "site": '',
        "project": '',
        "deployment": '',
        "call": '',
        "granularity": ''
        };


        this.detectionQuery = {
        "species": [''],
        "site": '',
        "project": '',
        "deployment": '',
            "group": ''
        };



    }
    //******************************************
    // setters for weekly detection plots form
    setWeeklyPlotSpecies(species){
        this.weeklyDetectionPlot.species = species.split(",");
    }
    setWeeklyPlotSite(site){
        this.weeklyDetectionPlot.site = site;
    }

    setWeeklyPlotProject(project){
        this.weeklyDetectionPlot.project = project;
    }

    setWeeklyPlotDeployment(deployment){
        this.weeklyDetectionPlot.deployment = deployment;
    }
    setWeeklyPlotCall(call){
        this.weeklyDetectionPlot.call = call;
    }
    setWeeklyPlotGranularity(granularity){
        this.weeklyDetectionPlot.granularity = granularity;
    }


    //******************************************
    // All Simple Queries
    setSpeciesDetections(species){
        this.detectionQuery.species = species.split(",");
    }

    setSpeciesGroup(group){
        this.detectionQuery.group = group;
    }

    setSite(site){
        this.detectionQuery.site = site;
    }

    setProject(project){
        this.detectionQuery.project = project;
    }

    setDeployment(deployment){
        this.detectionQuery.deployment = deployment;
    }

    //******************************************

    /**
     * 
     * @param {Point} northEast top right corner of long lat box
     * @param {Point} southWest bottom left corner of long lat box
     */
    setLongitudeLatitude(northEast, southWest){
        this.deploymentAndDetections.longitude['min'] = southWest.lng();
        this.deploymentAndDetections.longitude['max'] = northEast.lng();
        this.deploymentAndDetections.latitude['min'] = southWest.lat();
        this.deploymentAndDetections.latitude['max'] = northEast.lat();
    }

    setLongitudeLatitude_2(northEast, southWest){
        this.deploymentAndDetections.longitude['min'] = southWest.lng()-99;
        this.deploymentAndDetections.longitude['max'] = northEast.lng()-99;
        this.deploymentAndDetections.latitude['min'] = southWest.lat()-99;
        this.deploymentAndDetections.latitude['max'] = northEast.lat()-99;
    }
    
    setLongitude(longitude){
        this.deploymentAndDetections.longitude = longitude;
    }

    getLongitude(){
        return this.deploymentAndDetections.longitude;
    }

    setLatitude(latitude){
        this.deploymentAndDetections.latitude = latitude;
    }

    setMinLatitude(min){
        this.deploymentAndDetections.latitude['min'] = min;
    }

    setMaxLatitude(max){
        this.deploymentAndDetections.latitude['max'] = max;
    }

    getLatitude(){
        return this.deploymentAndDetections.latitude;
    }

    setMinLongitude(min){
        this.deploymentAndDetections.longitude['min'] = min;
    }

    setMaxLongitude(max){
        this.deploymentAndDetections.longitude['max'] = max;
    }

    

}

