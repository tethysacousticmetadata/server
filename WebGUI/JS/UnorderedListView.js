/**
 * @author Daniel Weston
 * @version 1.0
 * DSanchezWeston@gmail.com
 * 28 Aug 2018
 */
"use strict";

/**
 * This is a class to act as an object for the <ul> in advancedqueiries.html.
 * The idea behind this class is to build everything inside the <ul id = "this.id"><li></li></ul>
 * and to communicate with its controller when user events enter data. 
 * @see WhereView
 * @see ReturnView
 */
class UnorderedListView{
    
    /**
     * Attaches to html element with same id <ul id = "@param id"><\ul>
     * Saves parent information to pass up function calls later when inputs are entered.
     * 
     * @param {Controller} controller   Parent Controller Object
     * @param {String} id               id string (i.e whereCondition, returnCondition)
     * @param {String} classStr
     * @param {Number} index            index in a list of <ul> elements inside the parent
     * @param {Array[ULData]} data      One array of ULData that fills out the li views created             
     * @param {Boolean} isSimple        A bad way of determining if the <li> being built should be simple or complex
     */
    constructor(controller, id, classStr, index, data, isSimple){
        this.controller = controller;
        this.id = id;
        this.classStr = classStr;
        this.index = index;
        this.unorderedListView = document.getElementById(this.id+this.index);   //tries to find the element
        if(this.unorderedListView == null){ //if not found creates the element
            this.unorderedListView = document.createElement("ul");
            this.unorderedListView.setAttribute("id", this.id+this.index);
            this.unorderedListView.setAttribute("class", this.classStr);
            document.getElementById(this.controller.id).appendChild(this.unorderedListView);
        }
        this.rows = []; /**@var {Array[ComplexRow or SimpleRow]} this.rows */
        if(isSimple)    //return condition
            this.buildSimpleUnorderedList(data);
        else            //where condition
            this.buildUnorderedList(data);
        setWhereListeners();    //encapsulation breaking in @see jquery2.js
    }

    /**
     * Foreach ULData element create a @see ComplexRow and @see ConjunctionRow until the last row
     * then only build a ComplexRow. Once all the <li> rows are build append them to 
     * @var this.unorderedListView
     * @param {Array[ULData]} data
     */
    buildUnorderedList(data){
        this.unorderedListView.innerHTML = "";                 //clear <ul>
        this.rows = [];                          //temproarly hold the <li> created
        for(let i = 0; i < data.length; i++) {  //foreach data row make a corresponding <li> element 
            var disable = false;        
            if(i == data.length-1)              //last row conjunctionSelect is disabled
                disable = true;
            this.rows.push(new ComplexRow(this, data[i], i));
            if(!disable)                        //Don't create a conjunction Row for the last <li>
                this.rows.push(new ConjunctionRow(this, data[i], i, disable));
        }
        for(let j = 0; j < this.rows.length; j++)    //add all <li> to <ul>
        {
            this.unorderedListView.appendChild(this.rows[j].getListItem());
        }
    }

    /**
     * Foreach ULData element create a @see SimpleRow. Once all the <li> rows are build append them to 
     * @var this.unorderedListView
     * @param {Array[ULData]} data
     */
    buildSimpleUnorderedList(data){
        this.unorderedListView.innerHTML = "";                 //clear <ul>
        var rows = [];                          
        for(var i = 0; i < data.length; i++) {  //foreach data row make a corresponding <li> element
            rows.push(new SimpleRow(this, data[i], i));
        }
        for(var i = 0; i < rows.length; i++)    //add all <li> to <ul>
        {
            this.unorderedListView.appendChild(rows[i].getListItem());
        }
    }

    /**
     * @var this.unorderedListView getter
     */
    getUnorderedListView(){
        return this.unorderedListView;
    }

     /**
     * Event call being passed up to parent to modify its data object (@see ULData.mapNode)
     * @param {Integer} index integer of id  
     */
    toggleBackgroundColor(index){
        this.controller.toggleBackgroundColor(index);
    }

    /**
     * Event call being passed up to parent to remove @see ULData and the corresponding view
     * @param {Integer} index   row index of <li> element and corresponding ULData
     * @var this.index          is the group index on <ul>
     */
    removeRow(index){
        //alert('is it getting to removeRow in UnorderedListView');
        this.controller.removeRow(this.index, index);
    }

    /**
     * Event call being passed up to parent to modify its operator data @see ULData.operatorValue 
     * @param {Integer} index   row index of <li> element and corresponding ULData
     * @param {String} value    new value of operatorSelect
     * @var this.index          is the group index on <ul>
     */
    updateOperatorSelect(index, value){
        this.controller.updateOperatorSelect(this.index, index, value);
    }

    /**
     * Event call being passed up to parent to modify its input data @see ULData.inputValue
     * @param {Integer} index   row index of <li> element and corresponding ULData
     * @param {String} value    new value of InputValue
     * @var this.index          is the group index on <ul>
     */
    updateInputValue(index, value){
        this.controller.updateInputValue(this.index, index, value);
    }

    /**
     * Event call being passed up to parent to modify its conjunction data @see ULData.conjunctionValue
     * @param {Integer} index   row index of <li> element and corresponding ULData
     * @param {String} value    new value of conjunctionSelect
     * @var this.index          is the group index on <ul>
     */
    updateConjunctionSelect(index, value){
        this.controller.updateConjunctionSelect(this.index, index, value);
    }

    activateAutoComplete(){
        this.rows.forEach (row=> {
            if(row instanceof ComplexRow)
                row.addAutoCompleteListener();
        });
    }


}