/**
 * @author Daniel Weston
 * @version 1.0
 * DSanchezWeston@gmail.com
 * 23 Aug 2018
 * Object Oriented controller for D3 (Data-Driven Documents)
 * With the assistance of Roger Whitney and Scott Lindeneau
 * https://d3js.org/
 * https://github.com/d3/d3/wiki
 */

 /**
  * @todo a lot of constants in here can be made static gets
  */
class D3TreeController{

    /**
     * Constructor for Object Oriented controller for D3 Tree structure
     * @constructor
     * @param {Object} d3 global object created by D3 JS import. No need to attach it to Object, but I'll pretend Encapsulation exists
     * @param {AdvancedQueries} parentController incase we'd want the node clicks to be passed up to parent
     */
    constructor(d3, parentController){
        var _this = this;
        this.parentController = parentController;
        this.d3 = d3;
        this.initialHeight = 100;
        this.savePoint = false;
        /**************************************/
        //alert($('#treeStructureBox').height())
        /********************************** Link to D3 v3 api of what's happening here https://github.com/d3/d3-3.x-api-reference/blob/master/Zoom-Behavior.md ***************************************/
        //this.lastZoom = new ZoomData($('svg').width()/2, this.initialHeight, 1); //this wasn't being used
        this.zoom = this.d3.behavior.zoom();
        this.svg = this.d3.select("#treeStructureBox").append("svg")
            .attr("width", "100%")
            .attr("height", "100%")
            .attr("id","treeSVG")

        //.attr("preserveAspectRatio", "xMinYMin meet") //this makes the font smaller
        //.attr("viewBox", "0 0 1600 600")  //this makes the font smaller
        .attr("class", "border border-primary rounded treeHeight")
        .call(this.zoom.on("zoom", function() { //zoom effect part and panning
            if(this.savePoint){
                this.beginDrag(_this.d3.event.translate, _this.d3.event.scale);
                this.savePoint = false;
            }
            _this.svg.attr("transform", "translate(" + _this.d3.event.translate + ")" + " scale(" + _this.d3.event.scale + ")");
            
        }))
            //.on("dblclick.zoom", null)  //need to think about whether we want this turned off, it was afterall a quick fix to a weird problem


        .append("g")  //root node x and y coordinates on svg
        .on("dblclick.zoom", null)
        //$('svg').click((e)=>{this.savePoint = true});
        //$('svg').mouseup((e)=>{this.savePoint = true});
        this.svg.attr("transform", "translate(780,"+this.initialHeight+")" + " scale(1)");
        this.zoom.translate([780 ,this.initialHeight]);
        //this.resetZoom();
        //*******************************************************************************************************************************************************************************************/

        this.i = 0;
        this.duration = 500;
        this.select2_data;
        this.diameter = 960;
        this.div = d3.select("body")
        .append("div") // declare the tooltip div
        .attr("class", "tooltip")
        .style("opacity", 0);

        this.tree = this.d3.layout.tree()
        //.size([height, width])
        .nodeSize([50, 0])   //distance inbetween nodes
        .separation(function separation(a, b) {
            return a.parent == b.parent ? 2 : 1;
        });

        this.diagonal = this.d3.svg.diagonal()
            .projection(function(d) { return [d.x, d.y]; });
            
    }


    /**
     * @static
     * Edit to change the distance from parent to children nodes. d.depth is an integer representing depth inside the hierarcy.
     */
    static get NODE_TO_NODE_DEPTH(){
        return 80;
    }

    /**
     * resets the zoom
     */
    resetZoom(){
        //alert($('svg').width());
        this.svg.attr("transform", "translate("+$('svg').width()/2+","+this.initialHeight+")" + " scale(1)");
        this.zoom.scale([1]);  
        this.zoom.translate([$('svg').width()/2 ,this.initialHeight]);
        this.zoom.scaleExtent([ZoomData.minZoom, ZoomData.maxZoom]);
    }

    /**
     * Not working yet
     * @todo implement a way to undo the last graph action. 
     */
    zoomUndo(){
        this.svg.attr("transform", "translate("+this.lastZoom.x+","+this.lastZoom.y+")" + " scale("+ this.lastZoom.zoomScale +")"); 
    }

    getSvg(){
        return this.svg;
    }

    /**
     * Called when a new xml is parsed and successfully converted to a D3 JSON. @see AdvancedQueries.schemaSelectChanged
     * @param {Object} d3Json Object of a XML Parsed JSON Object converted for D3 Object to display
     */
    setRoot(d3Json)
    {
        this.d3.root = d3Json;
        this.intializeD3();
    }

    //basically a way to get the path to an object
    searchTree(obj, search, path, paths) {
        if (obj.name.indexOf(search) != -1) { //if search is found return, add the object to the path and return it
            path.push(obj);
            paths.push(path.slice(0)); //clone array
        }
        if (obj.children || obj._children) { //if children are collapsed d3 object will have them instantiated as _children
            var children = (obj.children) ? obj.children : obj._children;
            for (var i = 0; i < children.length; i++) {
                path.push(obj); // we assume this path is the right one			  
                searchTree(children[i], search, path, paths);
                path.pop();
            }
        }
    }

    /**
     * 
     * @param {Object} node 
     * @param {Object} leaves 
     * @param {*} index 
     * @todo figure out what this function does
     * @return {Array[Integer, Object]} 
     */
    extract_select2_data(node, leaves, index) {
        if (node.children) {
            for (var i = 0; i < node.children.length; i++) {
                index = this.extract_select2_data(node.children[i], leaves, index)[0];
            }
        } else {
            leaves.push({
                id: ++index,
                text: node.name
            });
        }
        return [index, leaves];
    }

    /**
     * Recursively collapse children underneath the node
     * @param {Object} d a D3 node
     */
    collapse(d) {
        if (d.children) {
            d._children = d.children;
            d._children.forEach(element=>{this.collapse(element)});
            d.children = null;
        }
    }

    /**
     * Recusively expand all the nodes below d
     * @param {Object} d 
     */
    expand(d) {
        var children = (d.children) ? d.children : d._children;
        if (d._children) {
            d.children = d._children;
            d._children = null;
        }
        if (children)
            children.forEach(element =>{this.expand(element)});
    }

    expandOne(d) {
        this.expand(d);
    }

    /**
     * Expand all nodes from the top to the bottom
     */
    expandAll() {
        this.expand(this.d3.root);
        this.update(this.d3.root);
    }


    expandAllWithoutUpdate() {
        this.expand(this.d3.root);
    }

    /**
     * Collapse all nodes, but don't update the D3 Tree on the webpage (Dendogram)
     * with the collapse
     */
    collapseAllWithoutUpdate()
    {
        var _this = this;
        this.d3.root.children.forEach(element =>{_this.collapse(element)});
        this.collapse(this.d3.root);
    }

    /**
     * Collapse all nodes in the tree.
     */
    collapseAll() {
        this.collapseAllWithoutUpdate();
        this.update(this.d3.root);
    }

    /**
     * @deprecated
     * Unused
     * @param {} paths 
     */
    openPaths(paths) {
        for (var i = 0; i < paths.length; i++) {
            if (paths[i].id !== "1") { //i.e. not root
                paths[i].class = 'found';
                if (paths[i]._children) { //if children are hidden: open them, otherwise: don't do anything
                    paths[i].children = paths[i]._children;
                    paths[i]._children = null;
                }
                update(paths[i]);
            }
        }
    }

    /**
     * @see update this is a function called on an event handler attached to all nodes everytime the graph is updated.
     * This function toggles a node by expanding or closing it if 'AltLeft' or 'AltRight' isn't pressed. Otherwise
     * It adds a <li> to the where condition for 'AltLeft' and a <li> to the return valuefor 'AltRight'
     * @param {Object} node 
     */
    click(node) {
        var _this = this;
        if(!this.parentController.keyMap['ShiftLeft'] && !this.parentController.keyMap['ShiftRight'])//Not CTRL and NOT SHIFT held down
        {
            function sleep(ms) {
                return new Promise(resolve => setTimeout(resolve, ms));
              }
              async function stall() {  //stall the node from opening for the double click
                await sleep(1200);
                _this.toggle(node);
                _this.update(node);
              }
            stall();
        }
        // Some controllers have special behaviors for clicking with the shift button held down.
        if(this.parentController.keyMap['ShiftLeft'])
        {
            if (this.parentController.whereController != null)
                this.parentController.whereController.addUngrouped(node, this.toCollection(node, "\/"));
            //this.parentController.groupSort();

        }
        else if(this.parentController.keyMap['ShiftRight']) //exclusive or ctrl, shift key is held down
        {
            if (this.parentController.returnController != null)
                this.parentController.returnController.addDefaultRow(node, this.toCollection(node, "\/"));
            //this.parentController.groupSort();
        }
    }

    changeColor(nodeId, colorHex){
        var _this = this;
        var nodes = _this.tree.nodes(this.d3.root).reverse();
        var node = _this.svg.selectAll("g.node")
            .data(nodes, function(d) {
                return d.id == nodeId;
        })
        //
        // console.log(node)
        // console.log(this.d3.zoomIdentity())
        //
        // let identity = this.d3.zoomIdentity.translate(100, 100).scale(3)
        //
        // _this.svg.transition()
        //     .duration(200)
        //     .transform(identity)

        node.select('circle').style('fill', colorHex)
        node.select('circle').style('border', '#000000')
    }

    /**
     * A function to recursively travel up a tree.
     * @param {Object} node 
     */
    toCollection(node){
        if(node.parent != null){ // Not root node
            let parentsNamesAndNodeName = this.toCollection(node.parent)+"\/"+node.name;
            if(parentsNamesAndNodeName.startsWith("\/"))
                parentsNamesAndNodeName = parentsNamesAndNodeName.substring(1);
            return parentsNamesAndNodeName; //find parent of root add parents name before yours \Deployments\Id...
        }
        return ""; //The Root Node
    }

    /**
     * Recursive function that travels from a leaf to the root node and returns the path in a 
     * node.name+delimiter+node+delimeiter+...+node.name string
     * @param {Object} node starting node
     * @param {String} delimiter delimiter inbetween each node name
     * @return {String} a string with all its parent nodes and itself seperated by delimiters
     */
    toRoot(node, delimiter){
        if(node.parent != null) // Not root node
        {
            //find parent of root add parents name before yours \Deployments\Id\...
            return this.toRoot(node.parent, delimiter)+delimiter+node.name;
        }
        return node.name;   //The Root Node
    }

    //attach search box listener
    /*$("#search").on("select2-selecting", function(e) {
        var paths = [];
        searchTree(root, e.object.text, [], paths);
        if (paths.length > 0) {
            paths.forEach(function(p) {
                openPaths(p)
            });
            //openPaths(paths);
        } else {
            alert(e.object.text + " not found!");
        }
    })*/

    /**
     * Called after @function setRoot() to 
     */
    intializeD3()  //gets called after xmlJSON is received to start the D3 JSON tree
    {
        this.d3.root.x0 = 800;   /**intial coordinates. Don't be fooled though to move the graph around @see d3.event.translate */
        this.d3.root.x = 800;    /**intial coordinates. Don't be fooled though to move the graph around @see d3.event.translate */
        this.d3.root.y0 = 0;
        this.d3.root.y = 0;     //intial coordinates
        this.expandAll();   //expand all the nodes this is to stop a bug from happening where non expanded nodes won't
                            //have parent keys created
        this.collapseAllWithoutUpdate();    
        this.toggle(this.d3.root);  //expand the first node (root)
        //if(this.d3.root.name == "Deployment"){  //This If block is used for debugging instead createing the return nodes
        //    this.toggle(this.d3.root.children[6]);  // and where conditions. I instead unlock the path to them and create parent keys.
        //    this.toggle(this.d3.root.children[6].children[0]); //As of right now hard code for Deployment/SamplingDetails/Channel
        //}
        //this.select2_data = this.extract_select2_data(this.d3.root,[],0)[1];
        this.update(this.d3.root);
        this.d3.root.x0 = 800;    //intial coordinates
        this.d3.root.x = 800;    //intial coordinates
        this.d3.root.y0 = 0;
        this.d3.root.y = 0;    //intial coordinates
        //init search box, //this.select2_data,
        /** @todo  implement a search box that finds node based on their names*/
        /*$("#search").select2({
            data: this.select2_data,
            containerCssClass: "search"
        }); */
    }

    /**
     * collapses an expanded node and expand a collapsed node within @var this.d3.root
     * @param {Object} node
     */
    toggle(node){
        if (node.children) {
            node._children = node.children;
            node.children = null;
        } else {
            node.children = node._children;
            node._children = null;
        }
    }

    /**
     * A function that updates the D3 tree as nodes open and close. It assign event handles to each node
     * and redraws the tree as it changes from expanding and collapsing.
     * @param {Object} source highest node 
     */
    update(source) { //Whenever the tree changes update those changes 
        var _this = this; //keeps scope inside anonymous fucntions
        var nodes = this.tree.nodes(this.d3.root).reverse(),
            links = this.tree.links(nodes);

        // Normalize for fixed-depth.
        nodes.forEach(function(d) {
            d.y = d.depth * D3TreeController.NODE_TO_NODE_DEPTH;
        });

        // Update the nodesvg¦
        var node = this.svg.selectAll("g.node")
            .data(nodes, function(d) {
                return d.id || (d.id = ++_this.i);
            })

        //console.log("node",node);
        // Enter any new nodes at the parent's previous position.          
        var nodeEnter = node.enter().append("g")
            .attr("class", "node")
            .attr("transform", function(d) {
                return "translate(" + source.x0 + "," + source.y0 + ")";    //top to bottom
                //return "translate(" + source.y0 + "," + source.x0 + ")";  //uncomment this if you want the dendogram to go left to right
            }).on("click", function(node) {
                  _this.click(node);
            }) // adding a comment
            .on("dblclick", function(node){
                _this.__clickedNode = node;
                return false;
            })
            .on("mouseover", function(d) {
                var g = d3.select(this); // The node
                // The class is used to remove the additional text later
                var info = g.append('text')
                    .classed('info', true)
                    .attr('y', 20)
                    .attr('x', 20)
                    .text(d.comment);

                // Provide Schema comments on entering field
                $('#fieldDescriptionName').show()
                $('#fieldDescription').show()
                $('#fieldDescriptionName').text(g[0][0]['__data__']['name'] + " - ")

                if(g[0][0]['__data__']['description'] != undefined)
                {
                    let description = []
                    if(g[0][0]['__data__']['description'].length == 1){
                        description.push(g[0][0]['__data__']['description'][0]["$"])
                    }else{
                        g[0][0]['__data__']['description'].forEach(desc => {
                            description.push(desc["$"])
                        })
                    }
                    $('#fieldDescription').text(description.join(' '))
                }else{
                    $('#fieldDescription').text("N/A")
                }
            })
            .on("mouseout", function() {
                // Remove the info text on mouse out.
                d3.select(this).select('text.info').remove()
            });

        nodeEnter.append("circle")
            .attr("r", 1e-5)
            .style("fill", function(d) {
                return d._children ? "lightsteelblue" : "#fff";//light stell blue if has children. Empty if it doesn't.
            });

        nodeEnter.append("text")    //the postion of node text
            .attr("y", function(d) {//if children place above else place below
                return d.children || d._children ? -12 : 17;
            })
            .attr("dx", 0)
            .attr("text-anchor", function(d) {//if children place anchor at start else end
                return "middle";
                //return d.children || d._children ? "end" : "start";
            })
            .text(function(d) {
                return d.name;
            })
            .style("fill-opacity", 1e-6);

        // Transition nodes to their new position.
        var nodeUpdate = node.transition()
            .duration(this.duration)
            .attr("transform", function(d) {
                return "translate(" + d.x + "," + d.y + ")";    //top- down
                //return "translate(" + d.y + "," + d.x + ")";  //uncomment this if you want the dendogram to go left to right
            });

        nodeUpdate.select("circle")
            .attr("r", 7)   //change circle size
            .style("fill", function(d) {
                if (d.class === "found") {
                    return "#ff4136"; //red
                } else if (d._children) { // ._children is hidden children
                    if (d.minOccurs == '1' && d._children[0].attribute)
                        return "#C35671"
                    return "lightsteelblue";
                } else {  // this branch is for fully expanded or terminal leaf node
                    if (d.minOccurs == '1' && (!d.hasOwnProperty('children')))
                        return "#C35671"
                    if (d.minOccurs == '1' && d.children[0].attribute)  // && d.children && d.children[0].attribute
                        return "#C35671"
                    //if (d.minOccurs == '1' && d.children[0].name.toLowerCase() == 'group')
                        //return "#C35671"
                    return "#fff";      //white
                }
            })
            .style("stroke", function(d) {  //circle outline
                if (d.class === "found") {
                    return "#ff4136"; //red
                }
            });

        nodeUpdate.select("text")
            .style("fill-opacity", 1);

        // Transition exiting nodes to the parent's new position.
        var nodeExit = node.exit().transition()
            .duration(this.duration)
            .attr("transform", function(d) {
                return "translate(" + source.x + "," + source.y + ")";  //top- down
                //return "translate(" + source.y + "," + source.x + ")";  //uncomment this if you want the dendogram to go left to right
            })
            .remove();

        nodeExit.select("circle")
            .attr("r", 1e-6);

        nodeExit.select("text")
            .style("fill-opacity", 1e-6);

        // Update the linksâ€¦
        var link = this.svg.selectAll("path.link")
            .data(links, function(d) {
                return d.target.id;
            });

        // Enter any new links at the parent's previous position.
        link.enter().insert("path", "g")
            .attr("class", "link")
            .attr("d", function(d) {
                var o = {
                    x: source.x0,
                    y: source.y0
                };
                return _this.diagonal({
                    source: o,
                    target: o
                });
            });

        // Transition links to their new position.
        link.transition()
            .duration(this.duration)
            .attr("d", this.diagonal)
            .style("stroke", function(d) {
                if (d.target.class === "found") {
                    return "#ff4136";
                }
            });

        // Transition exiting nodes to the parent's new position.
        link.exit().transition()
            .duration(this.duration)
            .attr("d", function(d) {
                var o = {
                    x: source.x,
                    y: source.y
                };
                return _this.diagonal({
                    source: o,
                    target: o
                });
            })
            .remove();

        nodes.forEach(function(d) {
            d.x0 = d.x;
            d.y0 = d.y;
        });

        /**
         * This is a bizarre neccesity
         * This JQuery function allows an element to be created at a node.
         * @var _this.__clickedNode this variable was assigned up in the nodeEnter.on.("click", ...)
         * It had to be done this way. Putting the code inside the on click
         * would lose the e event and out here we wouldn't have context to the node, so
         * it's been bridged by @var _this.__clickedNode .
         */
        //$(".node").on("dblclick.zoom", null)

        $(".node").on("dblclick", function (e) {
            let fun = ()=>{
                let node = _this.__clickedNode;
                _this.__clickedNode = undefined;
                var inner = $("<div id="+_this.toRoot(node, "_")+" class = 'input-group-text'>"+_this.toCollection(node, "\/")+"</div>", {class:"inner"}).draggable({
                cursor: "grab",                 //up above toRoot(node, "_") is used instead of "\/" to not cause an error in JQuery.
                    connectToSortable: ".sortable",
                    //connectWith: ".myClass.sorting.ui-state-default",
                    //helper: "clone",
                    revert: "invalid",
                    scroll: "true",
                }).appendTo("body");
                //inner.data("node", node);
                inner[0]['node'] = node;  //is this where item is set on ui obj in Jquery_Ui.js?
                console.log('HERE IN D3TreeController')
                console.log(node)
                console.log(e)
                console.log("Inner")
                console.log(inner)
                inner.css("position", "absolute");
                e.type = "mousedown.draggable";
                e.target = inner[0];
                inner.css("left", e.pageX);
                inner.css("top", e.pageY);
                $("#"+_this.toRoot(node, "_")).on("mouseup", function(e){//up above toRoot(node, "_") is used instead of "\/" to not cause an error in JQuery.
                    this.outerHTML = "";    //delete element on mouseup.
                });
                inner.trigger(e);
            }
            if(!_this.__clickedNode){
                setTimeout(fun,10);
            }else{
                fun();
            }
            return false;
        });
    }
}


class D3ImportJoinTables {

     constructor(d3, parentController){
        var _this = this;
        this.parentController = parentController;
        this.d3 = d3;
        this.initialHeight = 100;
        this.savePoint = false;
        /**************************************/
        /********************************** Link to D3 v3 api of what's happening here https://github.com/d3/d3-3.x-api-reference/blob/master/Zoom-Behavior.md ***************************************/
        //this.lastZoom = new ZoomData($('svg').width()/2, this.initialHeight, 1); //this wasn't being used
        this.zoom = this.d3.behavior.zoom();
        console.log("PPPPPPPPP")
        console.log(this.d3.select("#joinDropDownDivLeft"))
        this.svg = this.d3.select("#joinDropDownDivLeft").append("svg")
        .attr("width", "100%")  //greylined window width //1600px
        .attr("height", "100%") //greylined window height
        .attr("id","treeSVG")

        //.attr("preserveAspectRatio", "xMinYMin meet") //this makes the font smaller
        //.attr("viewBox", "0 0 1600 600")  //this makes the font smaller
        .attr("class", "border border-primary rounded")
        .call(this.zoom.on("zoom", function() { //zoom effect part and panning
            if(this.savePoint){
                this.beginDrag(_this.d3.event.translate, _this.d3.event.scale);
                this.savePoint = false;
            }
            _this.svg.attr("transform", "translate(" + _this.d3.event.translate + ")" + " scale(" + _this.d3.event.scale + ")");

        }))
            //.on("dblclick.zoom", null)  //need to think about whether we want this turned off, it was afterall a quick fix to a weird problem


        .append("g")  //root node x and y coordinates on svg
        .on("dblclick.zoom", null)
        //$('svg').click((e)=>{this.savePoint = true});
        //$('svg').mouseup((e)=>{this.savePoint = true});
        this.svg.attr("transform", "translate(780,"+this.initialHeight+")" + " scale(1)");
        this.zoom.translate([780 ,this.initialHeight]);
        //this.resetZoom();
        //*******************************************************************************************************************************************************************************************/

        this.i = 0;
        this.duration = 500;
        this.select2_data;
        this.diameter = 960;
        this.div = d3.select("body")
        .append("div") // declare the tooltip div
        .attr("class", "tooltip")
        .style("opacity", 0);

        this.tree = this.d3.layout.tree()
        //.size([height, width])
        .nodeSize([50, 0])   //distance inbetween nodes
        .separation(function separation(a, b) {
            return a.parent == b.parent ? 2 : 1;
        });

        this.diagonal = this.d3.svg.diagonal()
            .projection(function(d) { return [d.x, d.y]; });

    }
}