/**
 * @author Daniel Weston
 * @version 1.0
 * DSanchezWeston@gmail.com
 * With the help of Professors Roger Whitney and Marie Roch
 * 15 Oct 2018
 */
"use strict";

/**
 * class that takes a xml JSON object and converts it into a D3 JSON object.
 */
class D3Json{

    /**
     * @var {Array[D3Json]} this.children each is a D3Json Tree for each schema
     */
    constructor(){
        this.children = [];         
        this.name = "Collections";  //name of root node
    }

    /**
     * Unused, but sets an object as the only child of the Object
     * @param {Object} children 
     */
    setChildren(children){
        this.children = children;
    }

    /**
     * Adds direct children to the object. This should be called mostly in @see AdvancedQueries.getSchema()
     * in the $.ajax().done()
     * @param {Object} child D3Json object
     */
    addChildren(child){ 
        this.children.push(child);
    }

    /**
     * Starting point for a recursive flush of the D3Json object
     * @see downReset()
     */
    reset(){
        this.downReset(this);
    }

    /**
     * Traverses down a tree and deletes all 'count', 'flworNodes', 'whereCount', 'returnCount', and 'forVar' keys
     * This will be called after a query was generated and erase all keys that were added.
     * @param {Object} treeNode 
     */
    downReset(treeNode) {
        for (var childNode in treeNode) {
            if (treeNode[childNode] !== null && childNode !== 'parent' && childNode !== 'flworNodes' && typeof(treeNode[childNode])=="object" ) {    //going one step down in the object tree!!
                this.downReset(treeNode[childNode]);
            }
            delete treeNode.flworNodes;
            delete treeNode.whereCount;
            delete treeNode.returnCount;
            delete treeNode.forVar;
        }
    }

    /**
     * Used to find a sub tree child.
     * @param {String} schema 
     * @return {Object} a sub Object of this Object
     */
    findCollection(schema){
        for(var i = 0; i<this.children.length; i++){    //searching only children which are one level deep
            if(this.children[i]['name'] == schema){     //children have a name value equal to @var schema?
                return this.children[i];
            }
        }
    }

    /**
     * Quick check 
     * @param {String} schema 
     * @returns {Boolean} a collection was found
     */
    inCollection(schema){
        this.children.forEach((element)=>{
            if(element['name'] == schema){
                return true;
            }
        });
        return false;
    }

    /**
     * Travel down the D3Json every node given in the position array and return that node
     * @param {Array[Numbers]} positionArray 
     * @return {Object}
     */
    toNode(positionArray){
        return this.trasverseTree(positionArray, "node");
    }

    /**
     * Travel down the D3Json every node given in the position array and return that path
     * @param {Array[String]} nodeNamesArray The left most Number in the array is the topmost node in tree.
     * @return {Object} 
     */
    nameToNode(nodeNamesArray){
        return this.trasverseTreeWithStrings(nodeNamesArray, "node");
    }

    /**
     * Travel down the D3Json every node given in the position array and return that path
     * @param {Array[Numbers]} positionArray The left most Number in the array is the topmost node in tree.
     * @return {String} 
     */
    path(positionArray){
        return this.trasverseTree(positionArray, "path");
    }

    /**
     * Travel down the D3Json every node given in the position array and return that path
     * @param {Array[String]} positionArray The left most Number in the array is the topmost node in tree.
     * @return {Object} 
     */
    nameToPath(nodeNamesArray){
        return this.trasverseTreeWithStrings(nodeNamesArray, "path");
    }

    /**
     * Give an array of Numbers to travel down the tree to sub children. The second parameter
     * is used to determine if the node or path is wanted as the returned value
     * @param {Array[Numbers]} positionArray The left most Number in the array is the topmost node in tree.
     * @param {String} wantReturned "path" or "node" @see toNode and @see path
     * @return {Object, String, Boolean} node Object, path String, or false
     */
    trasverseTreeWithStrings(nodeNamesArray, wantReturned){
        let node = this;
        let path = "";
        for(let i = 0; i<nodeNamesArray.length; i++)
        {
            if("children" in node && node.children !== null){ 
                for(let j = 0; j<node.children.length; j++)
                {
                    if(node.children[j].hasOwnProperty('name') && node.children[j].name === nodeNamesArray[i]){
                        node = node.children[j];
                        path += node.name+"\/";
                        break;
                    }
                }
            }
            else if("_children" in node && node._children !== null){ 
                for(let j = 0; j<node._children.length; j++)
                {
                    if(node._children[j].hasOwnProperty('name') && node._children[j].name === nodeNamesArray[i]){
                        node = node._children[j];
                        path += node.name+"\/";
                        break;
                    }
                }
                
            }
            else
                return false;
        }
        if(wantReturned == "node")
            return node;
        if(wantReturned == "path")
            return path; 
        else
            return null;
    }

    /**
     * Give an array of Numbers to travel down the tree to sub children. The second parameter
     * is used to determine if the node or path is wanted as the returned value
     * @param {Array[Numbers]} positionArray The left most Number in the array is the topmost node in tree.
     * @param {String} wantReturned "path" or "node" @see toNode and @see path
     * @return {Object, String, Boolean} node Object, path String, or false
     */
    trasverseTree(positionArray, wantReturned){
        let node = this;
        let path = "";
        for(let i = 0; i<positionArray.length; i++)
        {
            if("children" in node && node.children !== null){ 
                if(node.children[positionArray[i]] !== void 0){
                    node = node.children[positionArray[i]];
                    path += node.name+"\/";
                }
            }
            else if("_children" in node && node._children !== null){
                if(node._children[positionArray[i]] !== void 0){
                    node = node._children[positionArray[i]];
                    path += node.name+"\/";
                }
            }
            else
                return false;
        }
        if(wantReturned == "node")
            return node;
        if(wantReturned == "path")
            return path; 
        else
            return null;
    }

    /**
     * gets called from getSchema in cannedQueries
     */
    createParents() {
        this.children.forEach((element)=>{
            this.down(element, this);
            //delete element.parent;
        });
    }

    /** 
     * this IS being used!
     */
    down(treeNode, parent){
        for (var childNode in treeNode) {
            if(treeNode[childNode] !== null && typeof(treeNode[childNode]) =="object"){
                if(Array.isArray(treeNode[childNode])){
                    treeNode[childNode].forEach((element)=>{
                        this.down(element, treeNode);    
                    });
                }
            }
        } //alert('dfggfg')

        treeNode.parent = parent;
        //console.log('treeNode Attri '+ treeNode['attribute'] + ' '+treeNode['name'])
        if (treeNode['attribute'])
            treeNode.parent['attribute_parent']
    }
}