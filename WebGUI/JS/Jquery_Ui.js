/**
 * @author Daniel Weston
 * @version 1.0
 * DSanchezWeston@gmail.com
 */

'use strict';
$(document).ready(function() {
    setWhereListeners();
    setReturnListeners();
    setDraggableListeners();
});

/**
 * Whenever a ui-draggable <div> gets dropped on a .whereCondition.sortable class add a new
 * ULData to the where controller and create a UL with a LI.
 */
function setWhereListeners(){
    $( ".whereCondition").sortable({
        cursor: "grabbing",
        revert: true,
        receive: function( event, ui ) {
            advancedQueries.whereController.addUngrouped(ui.item[0].node, ui.item[0].innerText);
            ui.item[0].remove();
            //advancedQueries.groupSort();
        },
        start: function(event, ui) {
            //old = ui.item.index();
            //console.log("old: "+ old);
        },
        stop: function(event, ui) {
            //whereUL.insert(old , ui.item.index());
        },
      });
}

/**
 * Whenever a ui-draggable <div> gets dropped on a .returnCondition.sortable class add a new
 * ULData to the where controller and create a UL with a LI.
 */
function setReturnListeners(){
    $( ".returnCondition" ).sortable({
        cursor: "grabbing",
        revert: true,
        receive: function( event, ui ) {
            advancedQueries.returnController.addDefaultRow(ui.item[0].node, ui.item[0].innerText);
            ui.item[0].remove();
        },
      });
}

/**
 * Whenever a ui-draggable <div> gets dropped on a .whereCondition.sortable class add a new
 * ULData to the where controller and create a UL with a LI.
 */
function setDraggableListeners(){
    $( "#draggable" ).draggable({
        cursor: "grab",
        connectToSortable: ".sortable",
        //helper: "clone",
        revert: "invalid",
        scroll: "true"
    });
}

