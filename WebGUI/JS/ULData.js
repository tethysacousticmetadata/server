/**
 * @author Daniel Weston
 * @version 1.0
 * DSanchezWeston@gmail.com
 * An object to hold one list items worth of data
 */

"use strict";
/**
 * The Model of Model, View, Controller (MVC)
 * The V would be @see whereView.UnorderedListView or @see UnorderedListView 
 * The C would be @see WhereController or @see ReturnController
 */

class ULData{

    /**
     * @param {Obhect} parent WhereController or ReturnController
     * @param {String} id "<li id = "whereCondition"></li>"
     * @param {Object} node
     * @param {String} path XML schema path "Deployment/DeploymentNumber"
     * @param {String} operatorValue "<" ">" "<=" ">=" "not" "exists" "="
     * @param {String} inputValue String values like project names or start times "CINMS", "05-22-2015"
     * @param {String} conjunctionValue "and" "or" "---"
     * @param {Boolean} addAutocomplete whether to add autocomplete to the whereview input when it's built 
     * @var {Array[String]} this.autocompleteArr will hold all the options for the inputValue
     * @var {Boolean} mapNode user wants this result in the GoogleMap title @see MapMarker.addMarker
     */
    constructor(parent, id, node, path, operatorValue, inputValue, conjunctionValue, addAutocomplete){
        this.path_collection = path.split('/')[0];
        this.parent = parent;
        this.id = id;
        this.node = node;
        this.path = path;
        this.operatorValue = operatorValue?operatorValue:"=";           //defaules to "=" if no value is given
        this.inputValue = inputValue;    
        this.conjunctionValue = conjunctionValue?conjunctionValue:"and";   //defaules to "=" if no value is given
        this.addAutocomplete = addAutocomplete
        this.autocompleteArr =[];
        this.mapNode = true;
        if (arguments[8])
            this.excludeFromLoop = arguments[8];
        else
            this.excludeFromLoop = false;
        if (arguments[9])
            this.attribute = arguments[9]
        else
            this.attribute = false;
        if(this.id === "whereView" && this.addAutocomplete)//Run for whereView not for returnView    
            this.getAutoArr('');
    }

    getConjunction()
    {
        return this.conjunctionValue
    }

    getAutoArr(fromRow){
        let submit = new SubmitQuery([this], [],"");    //create a new SubmitClass with the LI inside whereController and returnCotnroller
        let form = new FormData();  //create a FormData instance that is going to be the body of the HTTP Post
        form.append("XQuery", submit.autocompleteQuery(this.path)); //add generated query to POST body
        console.log(submit.autocompleteQuery(this.path));
        let autocompleteAjaxRequest = new AjaxWrapper("post", form, this.autoArrEmpty);
        let _this = this;
        $.ajax(autocompleteAjaxRequest.getSettings()).done((response)=>{   //When we get the response for the xml JSON
            //let results = new ResultsValueFilter(response);
            //this.autocompleteArr = results.getDistinctValues();
            console.log(response);
            var newResponse = response.replace(/<[^>]*>/g,"");
            this.autocompleteArr = newResponse.split("\n");
            _this.parent.assignAutoComplete();
            if (fromRow){
                fromRow.addAutoCompleteForModal();  //just for speciesID
            }
        });
    }

    autoArrEmpty(jqXHR, textStatus, errorThrown){
        console.log(textStatus+": "+errorThrown+"\n"+jqXHR.responseText);
    }

    static getLibrary(){
        return {}
    }

    /**
     * @return {Array[String]} this.autocompleteArr
     */
    getAutocompleteArr(){
        return this.autocompleteArr;
    }

    /**
     * Setter to change id data
     * @param {String} id data of id element of li object <li id = "whereCondition"></li>
     */
    setId(id){
        this.id = id;
    }

    /**
     * Setter to change node data
     * @param {Object} node D3 tree node
     */
    setNode(node){
        this.node = node;
    }

    /**
     * Setter to change path data
     * @param {String} path XML schema path "Deployment/Id"
     */
    setPath(path){
        this.path = path;
    }

    /**
     * Setter to change operator data
     * @param {String} operatorValue "<" ">" "<=" ">=" "not" "exists" "="
     */
    setOperatorValue(operatorValue){
        this.operatorValue = operatorValue;
    }

    /**
     * Setter to change input data
     * @param {String} inputValue String values liek project names or start times "CINMS", "05-22-2015"
     */
    setInputValue(inputValue){
        this.inputValue = inputValue;
    }

    /**
     * Setter to change conjunctionValue data
     * @param {String} conjunctionValue 
     */
    setConjunctionValue(conjunctionValue){
        this.conjunctionValue = conjunctionValue;
    }

    /**
     * Toggle boolean value. If true then false. If false then true.
     */
    toggleBackgroundColor(){
        this.mapNode = true^this.mapNode;
    }

    /**
     * @return {String} id list item id attribute
     */
    getId(){
        return this.id;
    }

    /**
     * @return {Object} d3 tree node
     */
    getNode(){
        return this.node;
    }

    /**
     * @return {String} path XML Schema path 
     */
    getPath(){
        return this.path;
    }

    getPathWithoutRoot(){
        let match = this.path.replace(/^[a-zA-Z_]+\//,"");
        return match;
    }

    /**
     * Returns the end of the String. Which is equivalent to the leaf node of the xPath
     * (e.g. /Deployments/SamplingData/Channel whould return Channel)
     * @return {String} the first match from the regex. The regex doesn't use globals so there should only be one match
     */
    getEndOfPath(){
        let match = this.path.match(/[a-zA-Z_]+$/);  //look for one or more letters including "_" ([a-zA-Z]+) at the end of the string ($)
        return match[0].toLowerCase();
    }

    /**
     * @return {String} operatorValue "<" ">" "<=" ">=" "not" "exists" "="
     */
    getOperatorValue(){
        return this.operatorValue;
    }

    /**
     * @return {String} inputValue String values liek project names or start times "CINMS", "05-22-2015"
     */
    getInputValue(){
        return this.inputValue;
    }

    /**
     * @return {String} conjunctionValue "and" "or" "---"
     */
    getConjunctionValue(){
        return this.conjunctionValue;
    }

    /**
     * @return {Boolean}
     */
    getMap(){
        return this.mapNode;
    }

    /**
     * I modified this function to have a optional path string sent to it. This is a workaround
     * to not permanently change nodes path names that propigate to the original node. This will
     * turn all ULData into a string 
     * @param {String} optionalPath 
     * @param {String} variableNumber identifies the variable pointing to the path where this element will be found
     *     The convention is to use the collection name followed by a number, this variableNumber is the value
     *     to be appended.  Example: $Detections2 would have variableNumber set to 2
     * @param {String} speciesSelectValue this param get passed all the way from AdvancedQueries. AdvancedQueries -> SubmitQuery -> whereExpression.toString()
     */
    toString(optionalPath, variableNumber, speciesSelectValue){
        if(speciesSelectValue)
            console.log("bp");
        var buildString = "";
        var path = undefined;
        if(optionalPath)    //use optionalPath instead of this.path
            path = optionalPath;
        else if(this.path)  //No OptionalPath passed in toString
            path = this.path;
        if(path)
            buildString += "$"+path+(variableNumber?variableNumber:""); //if variableNumber add it
        if(this.operatorValue){
            if(this.operatorValue == "exists")
                return "exists($"+ path + ") " + this.conjunctionValue + " ";
            else if(this.operatorValue == "not")
                return " not($"+path + ") " + this.conjunctionValue + " ";    
            else
                buildString += " "+this.operatorValue;
        }
        if(this.inputValue){
            if(this.isSpecialData(speciesSelectValue)) { //A $Detections#/.../SpeciesId was detected and we need to add lib syntax
                if (speciesSelectValue == "Vernacular")
                    buildString += ' lib:vernacular2tsn(' + this.quoteStringN(this.inputValue) + ', "English")';
                else if (speciesSelectValue == "latin")
                    buildString += ' lib:completename2tsn(' + this.quoteStringN(this.inputValue) + ')';
                else {
                    if (this.attribute) {
                        //alert(buildString)
                        buildString = buildString.split("=")[0].trim() + "/@" + this.attribute + "/string() = "+this.quoteStringN(this.inputValue);

                    } else {
                        buildString += ' lib:abbrev2tsn(' + this.quoteStringN(this.inputValue) + ', "' + speciesSelectValue + '")';
                    }
                }
            } else
            {console.log("INPUT VALUE IN toString")
                console.log(this.inputValue)
                buildString += " "+this.quoteString(this.inputValue); }
        }
        if(this.conjunctionValue)
            buildString += " "+this.conjunctionValue+" ";
        console.log("BUILD STRING")
        console.log(buildString)
        console.log(this.conjunctionValue)
        return buildString;
    }

    /**
     * Written by Roger Whitney
     * Method used to test if value is a string or number. Inside XQuery where clause strings need quotations, but numbers do not
     * where \Deployments\Project = "CINMS" and \Deployment\DeploymentId = 2
     * @param {String} value 
     */
    isString(value) {
        return (typeof value === "string") || (value instanceof String);
    }
    
    /**
     * Written by Roger Whitney
     * Add quotation marks to all strings value, but not to nubmers.
     * @param {Strings} aString 
     */
    quoteString(aString) {
        const quote = "\"";
        if (this.isString(aString) && aString != "" && !aString.startsWith("$") && isNaN(aString)) { //a string and note equal to "" and is not a number
            return quote + aString + quote;
        } else {
            return aString;
        }
    }

    quoteStringN(aString) {
        const quote = "\"";
        if (this.isString(aString) && aString != "" && !aString.startsWith("$")) { //a string and note equal to "" and is not a number
            return quote + aString + quote;
        } else {
            return aString;
        }
    }

    /**
     * Detect if the path name meets the regular express /^\$Detections\d*\/.*\/SpeciesId$/i
     * If it does return true
     * The return value equivalent of this is implemented in @see FLWOR.ReturnCLause.toString()
     */
    isSpecialData(speciesSelectValue){
        let regularExpression = /\/SpeciesId\/?$/i;
        console.log(regularExpression.test(this.path))
        return regularExpression.test(this.path) && speciesSelectValue;  //@todo this assumes an advancedQueries exists globally. Bad Coding!
            
    }

    species_special_data(){
        let regExp = /\/SpeciesId$/i;
        return regExp.exec(xmlText);
    }
}

