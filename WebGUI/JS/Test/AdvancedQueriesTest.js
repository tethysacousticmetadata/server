/**
 * @author Daniel Weston
 * @version 2.0
 * DSanchezWeston@gmail.com
 * 26 Oct 2018
 */

"use strict"

/**
 * Entire class is unit tests using Qunit to test the advancedQueries html
 * to disable the tests change @see debugging to false
 * https://api.qunitjs.com
 */
class AdvancedQueriesTest{

    /**
     * Automatically starts Qunit test if debugging is true
     * @param {Object} advancedQueries the instance created in @see AdvancedQueries
     */
    constructor(advancedQueries){
        this.advancedQueries = advancedQueries;
        if(this.debugging)
            this.runTests();
    }

    /**
     * change the variables to true/false to enable/disable running their unit test
     */
    get debugging(){
        return false;
    }

     /**
     * change the variables to true/false to enable/disable running their unit test
     */
    static get flworTest(){
        return {
            forBinding: true,
            forClause: true,
            whereClause: true,
            returnClause: true,
            flowerCheck: true,
        }
    }

    /**
     * change the variables to true/false to enable/disable running their unit test
     */
    static get buildQuery(){
        return {
            nestedSingle: true,
            flatSingle: true,
            nestedJoined: true,
            flatJoined: true
        }
    }

    /**
     * change the variables to true/false to enable/disable running their unit test
     */
    static get regexTest(){
        return {

        }
    }

    /**
     * change the variables to true/false to enable/disable running their unit test
     */
    static get speciesId(){
        return {
            one: true,
            two: true,
            three: true,
            four: true,
            five: true,
            six: true,
        }
    }

    /**
     * change the variables to true/false to enable/disable running their unit test
     */
    static get joinCombinations(){
        return {
            calibrationsAndDeployment: true,
            deploymentAndDetections: true,
            deploymentAndEnsemble: true,
            deploymentAndLocalize: true,
            detectionsAndEnsemble: true,
            detectionsAndItis: true,
            itisAndSpecies: true
        }
    }

    /**
     * @return {Array} ["Calibration","ID"]
     */
    static get CALIBRATION_ID(){
        return ["Calibration","ID"];
    }

    /**
     * @return {Array} ["Detections"]
     */
    static get DETECTIONS(){
        return ["Detections"];
    }
    
    /**
     * @return {Array} ["Detections","Effort"]
     */
    static get DETECTIONS_EFFORT(){
        return ["Detections", "Effort"];
    }
    
    /**
     * @return {Array} ["Detections","Effort","Kind"]
     */
    static get DETECTIONS_EFFORT_KIND(){
        return ["Detections", "Effort", "Kind"];
    }
    
    /**
     * @return {Array} ["Detections","Effort","Kind","SpeciesID"]
     */
    static get DETECTIONS_EFFORT_KIND_SPECIESID(){
        return ["Detections","Effort","Kind","SpeciesID"];
    }

    /**
     * @return {Array} ["Detections","Effort","Kind","SpeciesID","Group"]
     */
    static get DETECTIONS_EFFORT_KIND_SPECIESID_GROUP(){
        return ["Detections","Effort","Kind","SpeciesID","Group"];
    }

    /**
     * @return {Array} ["Detections", "Id"]
     */
    static get DETECTIONS_ID(){
        return ["Detections", "Id"];
    }
    
    /**
     * @return {Array} ["Deployment","DeploymentDetails"]
     */
    static get DEPLOYMENT_DEPLOYMENTDETAILS(){
        return ["Deployment","DeploymentDetails"];
    }

    /**
     * @return {Array} ["Deployment", "DeploymentDetails", "Longitude"]
     */
    static get DEPLOYMENT_DEPLOYMENTDETAILS_LONGITUDE(){
        return ["Deployment", "DeploymentDetails", "Longitude"];
    }
    
    /**
     * @return {Array} ["Deployment", "DeploymentDetails", "Latitude"]
     */
    static get DEPLOYMENT_DEPLOYMENTDETAILS_LATITUDE(){
        return ["Deployment", "DeploymentDetails", "Latitude"];
    }
    
    /**
     * @return {Array} ["Deployment","Project"]
     */
    static get DEPLOYMENT_PROJECT(){
        return ["Deployment","Project"]; //[1,0]
    }

    /**
     * @return {Array} ["Deployment", "SamplingDetails", "Channel", "Start"]
     */
    static get DEPLOYMENT_SAMPLINGDETAILS_CHANNEL_START(){
        return ["Deployment", "SamplingDetails", "Channel", "Start"];
    }
    
    /**
     * @return {Array} ["Deployment", "Site"]
     */
    static get DEPLOYMENT_SITE(){
        return ["Deployment", "Site"];
    }

    /**
     * @return {Array} ["Ensemble", "Name"]
     */
    static get ENSEMBLE_NAME(){
        return ["Ensemble", "Name"];    //[3,0]
    }

    /**
     * @return {Array} ["Localize", "Description"]
     */
    static get LOCALIZE_DESCRIPTION(){
        return ["Localize", "Description"];  //[5,0]
    }


    testTree(){
        QUnit.test("Qunit works", function( assert ){
            assert.equal(2, 1);
        });
    }

    /**
     * Entry point into unit tests
     */
    runTests(){
            TestSetup.createTestDivs(); //creates the HTML elements on webpage to display QUnit results     
            this.checkFLWOR();          //Unit tests for FLWOR.js
            this.checkAdvancedQueries();//Unit tests for SnackBar.js and SubmitQuery.js        
            this.checkXMLToD3();
    }

    /**
     * Check SnackBar.js and SubmitQuery.js
     */
    checkAdvancedQueries(){
        console.log("checkAdvancedQueries TESTS START");
        let _this = this;
        QUnit.module( "Advanced Queries" );
        QUnit.test("snackBar Text", function( assert ){
            assert.equal(typeof _this.advancedQueries.snackBar, 'object', 'type is object');
            assert.equal(_this.advancedQueries.snackBar instanceof SnackBar , true, 'instance of Snackbar');
            assert.equal(_this.advancedQueries.snackBar.getText(), SnackBar.SNACKBAR_DEFAULT_TEXT, _this.advancedQueries.snackBar.getText()+` =? ${SnackBar.SNACKBAR_DEFAULT_TEXT}`);
        });
        this.checkRegex();
        QUnit.test( "asynchronous test: async focus", function( assert ) {
            var done = assert.async();
            setTimeout(function() {
              assert.equal( _this.advancedQueries.trees.children.length, 8, `${_this.advancedQueries.trees.children.length} =? 8` );
              done();
            },2000);
        });
        function sleep(ms) {
            return new Promise(resolve => setTimeout(resolve, ms));
        }
        async function stall() {  //stall the node from opening for the double click
        await sleep(2000);
            _this.checkBuildQueryFourOptions();
        }
        stall();
        //_this.checkBuildQueryFourOptions();
        console.log("checkAdvancedQueries TESTS END");
    }

    /**
     * @todo function would test all the functions of converting a xmlJSon into a D3Json
     * @see XmlToD3.js
     */
    checkRegex(){
        QUnit.module( "Regex Test" );
        QUnit.test("return regex", function( assert ){
            let reg = SubmitQuery.regularExpression;
            assert.notEqual(reg.test('where $Detections1/Effort/Kind/SpeciesID = "Gg"'), true, 'Return Clause regex: where $Detections1/Effort/Kind/SpeciesID = "Gg" '+reg.test('where $Detections1/Effort/Kind/SpeciesID = "Gg"'));
            assert.equal(reg.test('return $Detections1/Effort/Kind/SpeciesID'), true, 'Return Clause regex: return $Detections1/Effort/Kind/SpeciesID '+reg.test('where $Detections1/Effort/Kind/SpeciesID = "Gg"'));
            assert.notEqual(reg.test('where $Detections2/Kind/SpeciesID = "Gg"'), true, 'Return Clause regex: where $Detections2/Kind/SpeciesID = "Gg" '+reg.test('where $Detections1/Effort/Kind/SpeciesID = "Gg"'));
            assert.equal(reg.test('return $Detections2/Kind/SpeciesID'), true, 'Return Clause regex: return $Detections2/Kind/SpeciesID '+reg.test('where $Detections1/Effort/Kind/SpeciesID = "Gg"'));
            assert.notEqual(reg.test('where $Detections3/SpeciesID = "Gg"'), true, 'Return Clause regex: where $Detections3/SpeciesID = "Gg" '+reg.test('where $Detections1/Effort/Kind/SpeciesID = "Gg"'));
            assert.equal(reg.test('return $Detections3/SpeciesID'), true, 'Return Clause regex: return $Detections3/SpeciesID '+reg.test('where $Detections1/Effort/Kind/SpeciesID = "Gg"'));
            assert.notEqual(reg.test('where $Detections4 = "Gg"'), true, 'Return Clause regex: where $Detections4 = "Gg" '+reg.test('where $Detections1/Effort/Kind/SpeciesID = "Gg"'));
            assert.equal(reg.test('return $Detections4'), false, 'Return Clause regex: return $Detections4 '+reg.test('where $Detections1/Effort/Kind/SpeciesID = "Gg"'));
            assert.equal(reg.test('where $Detections1/Effort/Kind/SpeciesID/Group = "Gg"'), false, 'where $Detections1/Effort/Kind/SpeciesID/Group = "Gg" '+reg.test('where $Detections1/Effort/Kind/SpeciesID = "Gg"'));
            assert.equal(reg.test('return $Detections1/Effort/Kind/SpeciesID/Group'), false, 'Return Clause regex: return $Detections1/Effort/Kind/SpeciesID/Group '+reg.test('where $Detections1/Effort/Kind/SpeciesID = "Gg"'));
            //assert.equal(reg.test('<out>{$Deployment1/DeploymentDetails, $Detections1/Effort/Kind/SpeciesID}</out>'), true, 'Return Clause regex: <out>{$Deployment1/DeploymentDetails, $Detections1/Effort/Kind/SpeciesID}</out> '+reg.test('<out>{$Deployment1/DeploymentDetails, $Detections1/Effort/Kind/SpeciesID}</out>'));
        });
    }

    /**
     * @todo function would test all the functions of converting a xmlJSon into a D3Json
     * @see XmlToD3.js
     */
    checkXMLToD3(){

    }

    checkComplexRow(){
        QUnit.module( "Complex Row" );
        let cR = new ComplexRow("fire", "fire/root/maw", "=", "inputValue", "And", 0);
        QUnit.test("Tree Children Load", function( assert ){
            assert.equal( advancedQuery.trees.children.length, 8, `${advancedQuery.trees.children.length} =? 8` );
        });

    }

    /**
     * FLWOR is an initial, intermediate, and return clause. Unit tests tests all parts of the FLWOR 
     */
    checkFLWOR(){
        let flower = new FLWOR("header", "footer");
        let forBinding = new ForBinding("varName", "exprSingle"); 
        let forClause = new ForClause();
        let whereClause = new WhereClause("AbCdEfGh IjKlMnOpQr");
        let returnClause = new ReturnClause("returnClause");

        if(AdvancedQueriesTest.flworTest.forBinding){
            console.log("FLWOR TESTS START");
            QUnit.module( "FLWOR" );
            QUnit.test("ForBinding Check", function( assert ){
                assert.equal(typeof forBinding, 'object', 'forBinding is an object');
                assert.equal(forBinding instanceof ForBinding , true, 'instance of ForBinding');
                assert.equal(typeof(forBinding.toString()), 'string', 'toString is String');
                assert.equal(forBinding.toString(), "$varName in exprSingle", forBinding.toString()+` =? $varName in exprSingle` );
            });
        }

        if(AdvancedQueriesTest.flworTest.forClause){
            
            forClause.addForBinding(forBinding);
            QUnit.test("ForClause Check", function( assert ){
                assert.equal(typeof forClause, 'object', 'forClause is an object');
                assert.equal(forClause instanceof ForClause , true, 'instance of ForClause');
                assert.equal(typeof(forClause.toString()), 'string', 'toString is String');
                assert.equal( forClause.toString(), "for $varName in exprSingle", forClause.toString()+` =? for $varName in exprSingle` );
            });
        }

        if(AdvancedQueriesTest.flworTest.whereClause){
            flower.setInitialClause(forBinding);
            QUnit.test("WhereClause Check", function( assert ){
                assert.equal(typeof whereClause, 'object', 'whereClause is an object');
                assert.equal(whereClause instanceof WhereClause , true, 'instance of WhereClause');
                assert.equal(typeof(whereClause.toString()), 'string', 'toString is String');
                assert.equal( whereClause.toString(), "where AbCdEfGh IjKlMnOpQr", whereClause.toString()+` =? where AbCdEfGh IjKlMnOpQr` );
            });
        }

        if(AdvancedQueriesTest.flworTest.returnClause){
            flower.addIntermediateClause(whereClause);
            QUnit.test("ReturnClause Check", function( assert ){
                assert.equal(typeof returnClause, 'object', 'returnClause is an object');
                assert.equal(returnClause instanceof ReturnClause , true, 'instance of ReturnClause');
                assert.equal(typeof(returnClause.toString()), 'string', 'toString is String');
                assert.equal( returnClause.toString(), "return returnClause", returnClause.toString()+` =? return returnClause` );
            });
        }

        if(AdvancedQueriesTest.flworTest.flowerCheck){
            flower.setReturnClause(returnClause);
            QUnit.test("FLWOR Check", function( assert ){
                assert.equal(typeof flower, 'object', 'flower is an object');
                assert.equal(flower instanceof FLWOR , true, 'instance of flower');
                assert.equal(typeof(flower.toString()), 'string', 'toString is String');
                assert.equal( flower.toString(), 'header\n$varName in exprSingle\nwhere AbCdEfGh IjKlMnOpQr\nreturn returnClause\nfooter', flower.toString()+'\n=?\n'
                + 'header\n$varName in exprSingle\nwhere AbCdEfGh IjKlMnOpQr\nreturn returnClause\nfooter' );
            });
        }
        console.log("FLWOR TESTS END");
    }

    /**
     * This module tests the four types of queries can made. non-joined and flat, non-joined and nested, joined and flat, and joined and nested
     * .replace(/\s/g,'') removes all whitespaces
     */
    checkBuildQueryFourOptions(){
        let _this = this;
        let detectionsEffortKindSpeciesID = ["Detections","Effort","Kind","SpeciesID"];
        QUnit.module( "Submit Query 4 options" );
        if(AdvancedQueriesTest.buildQuery.nestedSingle){        
            QUnit.test("Build Nested Single Submit Query #1", function( assert ){
                let whereData = _this.generateWhere();
                let returnData = _this.generateReturn();
                _this.advancedQueries.speciesSelect.select.value = "";
                let submit = new SubmitQuery(whereData, returnData, _this.advancedQueries.speciesSelect.select.value);
                assert.equal( submit.buildQuery(_this.advancedQueries.trees, false).replace(/\s/g,''), //replace(/\s/g, ' ') removes all white spaces
                `import schema namespace ty="http://tethys.sdsu.edu/schema/1.0" at "tethys.xsd";
                <ty:Result xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
                {
                    for $Deployment1 in collection('Deployments')/ty:Deployment
                    where $Deployment1/DeploymentDetails/Longitude < 50  and 
                    $Deployment1/DeploymentDetails/Longitude > -50  and 
                    $Deployment1/DeploymentDetails/Latitude > -40  and 
                    $Deployment1/DeploymentDetails/Latitude < 40
                    return 
                    <out>{
                        for $Deployment2 in $Deployment1/SamplingDetails/Channel
                        return $Deployment2/Start,
                        $Deployment1/DeploymentDetails,
                        $Deployment1/Site
                    }</out>
                }
                </ty:Result>`.replace(/\s/g,''), submit.buildQuery(_this.advancedQueries.trees, false) );//replace(/\s/g, ' ') removes all white spaces
            });
        }
        if(AdvancedQueriesTest.buildQuery.flatSingle){
            QUnit.test("Build Flat Single Submit Query #2", function( assert ){
                let whereData = _this.generateWhere();
                let returnData = _this.generateReturn();
                _this.advancedQueries.speciesSelect.select.value = "";
                let submit = new SubmitQuery(whereData, returnData, _this.advancedQueries.speciesSelect.select.value);
                assert.equal( submit.buildQuery(_this.advancedQueries.trees, true).replace(/\s/g,''), //replace(/\s/g, ' ') removes all white spaces
                `import schema namespace ty="http://tethys.sdsu.edu/schema/1.0" at "tethys.xsd";
                <ty:Result xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
                {
                    for $Deployment1 in collection('Deployments')/ty:Deployment
                    where $Deployment1/DeploymentDetails/Longitude < 50  and
                    $Deployment1/DeploymentDetails/Longitude > -50  and
                    $Deployment1/DeploymentDetails/Latitude > -40  and
                    $Deployment1/DeploymentDetails/Latitude < 40
                    return <out>{
                        $Deployment1/SamplingDetails/Channel/Start, 
                        $Deployment1/DeploymentDetails, 
                        $Deployment1/Site
                    }</out>
                }
                </ty:Result>`.replace(/\s/g,'') );//replace(/\s/g, ' ') removes all white spaces
            });
        }
        if(AdvancedQueriesTest.buildQuery.nestedJoined){
            QUnit.test("Build Nested Joined Submit Query #3", function( assert ){
                let whereData = _this.generateWhere();
                let returnData = _this.generateReturn();
                returnData.push(new ULData(null,"", _this.advancedQueries.trees.nameToNode(AdvancedQueriesTest.DETECTIONS_EFFORT_KIND_SPECIESID), _this.advancedQueries.trees.path(detectionsEffortKindSpeciesID)));
                _this.advancedQueries.speciesSelect.select.value = "";
                let submit = new SubmitQuery(whereData, returnData, _this.advancedQueries.speciesSelect.select.value);
                assert.equal( submit.buildQuery(_this.advancedQueries.trees, false).replace(/\s/g,''), //replace(/\s/g, ' ') removes all white spaces
                `import schema namespace ty="http://tethys.sdsu.edu/schema/1.0" at "tethys.xsd";
                <ty:Result xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">
                {
                    for $Deployment1 in collection('Deployments')/ty:Deployment , $Detections1 in collection('Detections')/ty:Detections 
                        where $Deployment1/DeploymentDetails/Longitude< 50  and
                        $Deployment1/DeploymentDetails/Longitude > -50  and
                        $Deployment1/DeploymentDetails/Latitude > -40  and
                        $Deployment1/DeploymentDetails/Latitude < 40 and
                        $Deployment1/Project = $Detections1/DataSource/Project and
                        $Deployment1/Site = $Detections1/DataSource/Site and 
                        $Deployment1/DeploymentID = $Detections1/DataSource/Deployment
                        return <out>{
                            for $Deployment2 in $Deployment1/SamplingDetails/Channel
                            return $Deployment2/Start,
                                $Deployment1/DeploymentDetails, $Deployment1/Site, 
                                for $Detections2 in $Detections1/Effort/Kind
                                return $Detections2/SpeciesID
                        }</out>
                    }
                </ty:Result>`.replace(/\s/g,''), submit.buildQuery(_this.advancedQueries.trees, false));//replace(/\s/g, ' ') removes all white spaces
            });
        }
        if(AdvancedQueriesTest.buildQuery.flatJoined)
        {
            QUnit.test("Build Flat Joined Submit Query #4", function( assert ){
                let whereData = _this.generateWhere();
                let returnData = _this.generateReturn();
                returnData.push(new ULData(null,"", _this.advancedQueries.trees.nameToNode(AdvancedQueriesTest.DETECTIONS_EFFORT_KIND_SPECIESID), _this.advancedQueries.trees.nameToPath(AdvancedQueriesTest.DETECTIONS_EFFORT_KIND_SPECIESID)));
                _this.advancedQueries.speciesSelect.select.value = "";
                let submit = new SubmitQuery(whereData, returnData, _this.advancedQueries.speciesSelect.select.value);
                assert.equal( submit.buildQuery( _this.advancedQueries.trees, true).replace(/\s/g,''), //replace(/\s/g, ' ') removes all white spaces
                `import schema namespace ty=\"http://tethys.sdsu.edu/schema/1.0\" at \"tethys.xsd\";
                <ty:Result xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">
                {
                    for $Deployment1 in collection('Deployments')/ty:Deployment ,
                    $Detections1 in collection('Detections')/ty:Detections
                    where $Deployment1/DeploymentDetails/Longitude < 50 and
                    $Deployment1/DeploymentDetails/Longitude > -50  and
                    $Deployment1/DeploymentDetails/Latitude > -40  and 
                    $Deployment1/DeploymentDetails/Latitude < 40 and
                    $Deployment1/Project = $Detections1/DataSource/Project and 
                    $Deployment1/Site = $Detections1/DataSource/Site and 
                    $Deployment1/DeploymentID = $Detections1/DataSource/Deployment
                    return <out>{
                        $Deployment1/SamplingDetails/Channel/Start, 
                        $Deployment1/DeploymentDetails, 
                        $Deployment1/Site, 
                        $Detections1/Effort/Kind/SpeciesID
                    }</out>
                }
                </ty:Result>`.replace(/\s/g,''), submit.buildQuery( _this.advancedQueries.trees, true));    //replace(/\s/g, ' ') removes all white spaces
            });
        }
        this.checkBuildQuerySpeciesId();    //This is here because we need the trees to be assembled 
        
    }

    /**
     * Check if the "lib:abbrev2tsn() and lib:tsn2abbrev( , "SIO.SWAL.v1")" text is being added when speciesSelect is used.
     */
    checkBuildQuerySpeciesId(){
        let _this = this;
        QUnit.module( "Submit Query SpeciesID" );
        if(AdvancedQueriesTest.speciesId.one){
            QUnit.test("Build Detections SpeciesID Query #1 Nested", function( assert ){
                let whereData = [];
                let returnData = [];
                _this.advancedQueries.speciesSelect.select.value = "SIO.SWAL.v1";
                whereData.push(new ULData(null,"", _this.advancedQueries.trees.nameToNode(AdvancedQueriesTest.DETECTIONS_EFFORT_KIND_SPECIESID), _this.advancedQueries.trees.nameToPath(AdvancedQueriesTest.DETECTIONS_EFFORT_KIND_SPECIESID),"=", "Gg", ""));
                returnData.push(new ULData(null,"", _this.advancedQueries.trees.nameToNode(AdvancedQueriesTest.DETECTIONS), _this.advancedQueries.trees.nameToPath(AdvancedQueriesTest.DETECTIONS)));
                let submit = new SubmitQuery(whereData, returnData, _this.advancedQueries.speciesSelect.select.value);
                assert.equal( submit.buildQuery(_this.advancedQueries.trees, false).replace(/\s/g,''), 
                `import schema namespace ty=\"http://tethys.sdsu.edu/schema/1.0\" at \"tethys.xsd\";\
                import module namespace lib=\"http://tethys.sdsu.edu/XQueryFns\" at \"Tethys.xq\";\
                <ty:Result xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">
                {
                    for $Detections1 in collection('Detections')/ty:Detections
                    where $Detections1/Effort/Kind/SpeciesID = lib:abbrev2tsn(\"Gg\", \"SIO.SWAL.v1\")
                    return $Detections1
                }
                </ty:Result>`.replace(/\s/g,''), submit.buildQuery(_this.advancedQueries.trees, false));
            });
        }
        
        if(AdvancedQueriesTest.speciesId.two){
            QUnit.test("Build Detections SpeciesID Query #2 Nested", function( assert ){
                let whereData = [];
                let returnData = [];
                _this.advancedQueries.speciesSelect.select.value = "SIO.SWAL.v1";
                whereData.push(new ULData(null,"", _this.advancedQueries.trees.nameToNode(AdvancedQueriesTest.DETECTIONS_EFFORT_KIND_SPECIESID), _this.advancedQueries.trees.nameToPath(AdvancedQueriesTest.DETECTIONS_EFFORT_KIND_SPECIESID),"=", "Gg", ""));
                returnData.push(new ULData(null,"", _this.advancedQueries.trees.nameToNode(AdvancedQueriesTest.DETECTIONS_EFFORT), _this.advancedQueries.trees.nameToPath(AdvancedQueriesTest.DETECTIONS_EFFORT)));
                let submit = new SubmitQuery(whereData, returnData, _this.advancedQueries.speciesSelect.select.value);
                assert.equal( submit.buildQuery(_this.advancedQueries.trees, false).replace(/\s/g,''), 
                `import schema namespace ty=\"http://tethys.sdsu.edu/schema/1.0\" at \"tethys.xsd\";\
                import module namespace lib=\"http://tethys.sdsu.edu/XQueryFns\" at \"Tethys.xq\";\
                <ty:Result xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">
                {
                    for $Detections1 in collection('Detections')/ty:Detections
                    where $Detections1/Effort/Kind/SpeciesID = lib:abbrev2tsn(\"Gg\", \"SIO.SWAL.v1\")
                    return $Detections1/Effort
                }
                </ty:Result>`.replace(/\s/g,''), submit.buildQuery(_this.advancedQueries.trees, false));
            });
        }
        
        if(AdvancedQueriesTest.speciesId.three){
            QUnit.test("Build Detections SpeciesID Query #3 Nested", function( assert ){
                let whereData = [];
                let returnData = [];
                _this.advancedQueries.speciesSelect.select.value = "SIO.SWAL.v1";
                whereData.push(new ULData(null,"", _this.advancedQueries.trees.nameToNode(AdvancedQueriesTest.DETECTIONS_EFFORT_KIND_SPECIESID), _this.advancedQueries.trees.nameToPath(AdvancedQueriesTest.DETECTIONS_EFFORT_KIND_SPECIESID),"=", "Gg", ""));
                returnData.push(new ULData(null,"", _this.advancedQueries.trees.nameToNode(AdvancedQueriesTest.DETECTIONS_EFFORT_KIND), _this.advancedQueries.trees.nameToPath(AdvancedQueriesTest.DETECTIONS_EFFORT_KIND)));
                let submit = new SubmitQuery(whereData, returnData, _this.advancedQueries.speciesSelect.select.value);
                assert.equal( submit.buildQuery(_this.advancedQueries.trees, true).replace(/\s/g,''), 
                `import schema namespace ty=\"http://tethys.sdsu.edu/schema/1.0\" at \"tethys.xsd\";\
                import module namespace lib=\"http://tethys.sdsu.edu/XQueryFns\" at \"Tethys.xq\";\
                <ty:Result xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">
                {
                    for $Detections1 in collection('Detections')/ty:Detections
                    where $Detections1/Effort/Kind/SpeciesID = lib:abbrev2tsn(\"Gg\", \"SIO.SWAL.v1\")
                    return $Detections1/Effort/Kind
                }
                </ty:Result>`.replace(/\s/g,''), submit.buildQuery(_this.advancedQueries.trees, true));
            });
        }
        if(AdvancedQueriesTest.speciesId.four){
            QUnit.test("Build Detections SpeciesID Query #4 Nested", function( assert ){
                let whereData = [];
                let returnData = [];
                _this.advancedQueries.speciesSelect.select.value = "SIO.SWAL.v1";
                whereData.push(new ULData(null,"", _this.advancedQueries.trees.nameToNode(AdvancedQueriesTest.DETECTIONS_EFFORT_KIND_SPECIESID), _this.advancedQueries.trees.nameToPath(AdvancedQueriesTest.DETECTIONS_EFFORT_KIND_SPECIESID),"=", "Gg", ""));
                returnData.push(new ULData(null,"", _this.advancedQueries.trees.nameToNode(AdvancedQueriesTest.DETECTIONS_EFFORT_KIND_SPECIESID), _this.advancedQueries.trees.nameToPath(AdvancedQueriesTest.DETECTIONS_EFFORT_KIND_SPECIESID)));
                let submit = new SubmitQuery(whereData, returnData, _this.advancedQueries.speciesSelect.select.value);
                assert.equal( submit.buildQuery(_this.advancedQueries.trees, true).replace(/\s/g,''),    
                `import schema namespace ty=\"http://tethys.sdsu.edu/schema/1.0\" at \"tethys.xsd\";
                import module namespace lib=\"http://tethys.sdsu.edu/XQueryFns\" at \"Tethys.xq\";
                <ty:Result xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">
                {
                    for $Detections1 in collection('Detections')/ty:Detections
                    where $Detections1/Effort/Kind/SpeciesID = lib:abbrev2tsn(\"Gg\", \"SIO.SWAL.v1\")
                    return lib:tsn2abbrev($Detections1/Effort/Kind/SpeciesID, \"SIO.SWAL.v1\")
                }
                </ty:Result>`.replace(/\s/g,''), submit.buildQuery(_this.advancedQueries.trees, true));
            });
        }
        
        if(AdvancedQueriesTest.speciesId.five){
            QUnit.test("Build Detections SpeciesID Query #5 Nested", function( assert ){
                let whereData = [];
                let returnData = [];
                _this.advancedQueries.speciesSelect.select.value = "SIO.SWAL.v1";
                whereData.push(new ULData(null,"", _this.advancedQueries.trees.nameToNode(AdvancedQueriesTest.DETECTIONS_EFFORT_KIND_SPECIESID), _this.advancedQueries.trees.nameToPath(AdvancedQueriesTest.DETECTIONS_EFFORT_KIND_SPECIESID),"=", "Gg", ""));
                returnData.push(new ULData(null,"", _this.advancedQueries.trees.nameToNode(AdvancedQueriesTest.DETECTIONS_EFFORT_KIND_SPECIESID_GROUP), _this.advancedQueries.trees.nameToPath(AdvancedQueriesTest.DETECTIONS_EFFORT_KIND_SPECIESID_GROUP)));
                let submit = new SubmitQuery(whereData, returnData, _this.advancedQueries.speciesSelect.select.value);
                assert.equal( submit.buildQuery(_this.advancedQueries.trees, false).replace(/\s/g,''), 
                `   importschemanamespacety=\"http://tethys.sdsu.edu/schema/1.0\"at\"tethys.xsd\";
                    importmodulenamespacelib=\"http://tethys.sdsu.edu/XQueryFns\"at\"Tethys.xq\";
                    <ty:Resultxmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">
                    {
                        for $Detections1incollection('Detections')/ty:Detections
                        return
                            for $Detections2in$Detections1/Effort/Kind
                                where $Detections2/SpeciesID=lib:abbrev2tsn(\"Gg\",\"SIO.SWAL.v1\")
                                return $Detections2/SpeciesID/Group
                    }
                    </ty:Result>`.replace(/\s/g,''), submit.buildQuery(_this.advancedQueries.trees, false));
            });
        }
        
        if(AdvancedQueriesTest.speciesId.six){
            QUnit.test("Build Detections SpeciesID Query #6 Flat Joined", function( assert ){
                let whereData = [];
                let returnData = [];
                _this.advancedQueries.speciesSelect.select.value = "SIO.SWAL.v1";
                whereData.push(new ULData(null,"", _this.advancedQueries.trees.nameToNode(AdvancedQueriesTest.DETECTIONS_EFFORT_KIND_SPECIESID), _this.advancedQueries.trees.nameToPath(AdvancedQueriesTest.DETECTIONS_EFFORT_KIND_SPECIESID),"=", "Gg", ""));
                returnData.push(new ULData(null,"", _this.advancedQueries.trees.nameToNode(AdvancedQueriesTest.DEPLOYMENT_DEPLOYMENTDETAILS), _this.advancedQueries.trees.nameToPath(AdvancedQueriesTest.DEPLOYMENT_DEPLOYMENTDETAILS)));
                returnData.push(new ULData(null,"", _this.advancedQueries.trees.nameToNode(AdvancedQueriesTest.DETECTIONS_EFFORT_KIND_SPECIESID), _this.advancedQueries.trees.nameToPath(AdvancedQueriesTest.DETECTIONS_EFFORT_KIND_SPECIESID)));
                let submit = new SubmitQuery(whereData, returnData, _this.advancedQueries.speciesSelect.select.value);
                assert.equal( submit.buildQuery(_this.advancedQueries.trees, true).replace(/\s/g,''), 
                    `import schema namespace ty="http://tethys.sdsu.edu/schema/1.0" at "tethys.xsd";
                    import module namespace lib="http://tethys.sdsu.edu/XQueryFns" at "Tethys.xq";
                    <ty:Result xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
                    {
                        for $Deployment1 in collection('Deployments')/ty:Deployment ,
                            $Detections1 in collection('Detections')/ty:Detections 
                            where $Detections1/Effort/Kind/SpeciesID = lib:abbrev2tsn("Gg", "SIO.SWAL.v1")  and
                            $Deployment1/Project = $Detections1/DataSource/Project and $Deployment1/Site = $Detections1/DataSource/Site and $Deployment1/DeploymentID = $Detections1/DataSource/Deployment
                            return 
                            <out>
                            {
                                $Deployment1/DeploymentDetails, 
                                lib:tsn2abbrev($Detections1/Effort/Kind/SpeciesID, "SIO.SWAL.v1")
                            }
                            </out>
                    }
                    </ty:Result>`.replace(/\s/g,''), submit.buildQuery(_this.advancedQueries.trees, true));
            });
        }

        if(AdvancedQueriesTest.speciesId.six){
            QUnit.test("Build Detections SpeciesID Query #7 Nested Joined", function( assert ){
                let whereData = [];
                let returnData = [];
                _this.advancedQueries.speciesSelect.select.value = "SIO.SWAL.v1";
                whereData.push(new ULData(null,"", _this.advancedQueries.trees.nameToNode(AdvancedQueriesTest.DETECTIONS_EFFORT_KIND_SPECIESID), _this.advancedQueries.trees.nameToPath(AdvancedQueriesTest.DETECTIONS_EFFORT_KIND_SPECIESID),"=", "Gg", ""));
                returnData.push(new ULData(null,"", _this.advancedQueries.trees.nameToNode(AdvancedQueriesTest.DEPLOYMENT_DEPLOYMENTDETAILS), _this.advancedQueries.trees.nameToPath(AdvancedQueriesTest.DEPLOYMENT_DEPLOYMENTDETAILS)));
                returnData.push(new ULData(null,"", _this.advancedQueries.trees.nameToNode(AdvancedQueriesTest.DETECTIONS_EFFORT_KIND_SPECIESID), _this.advancedQueries.trees.nameToPath(AdvancedQueriesTest.DETECTIONS_EFFORT_KIND_SPECIESID)));
                let submit = new SubmitQuery(whereData, returnData, _this.advancedQueries.speciesSelect.select.value);
                assert.equal( submit.buildQuery(_this.advancedQueries.trees, false).replace(/\s/g,''), 
                    `importschemanamespacety=\"http://tethys.sdsu.edu/schema/1.0\"at\"tethys.xsd\";
                    importmodulenamespacelib=\"http://tethys.sdsu.edu/XQueryFns\"at\"Tethys.xq\";
                    <ty:Resultxmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">
                    {
                        for $Deployment1 in collection('Deployments')/ty:Deployment,
                            $Detections1 in collection('Detections')/ty:Detections
                            where $Detections1/Effort/Kind/SpeciesID = lib:abbrev2tsn("Gg", "SIO.SWAL.v1") and 
                            $Deployment1/Project = $Detections1/DataSource/Project and 
                            $Deployment1/Site = $Detections1/DataSource/Site and 
                            $Deployment1/DeploymentID = $Detections1/DataSource/Deployment
                            return
                            <out>{
                                $Deployment1/DeploymentDetails,
                                for $Detections2 in $Detections1/Effort/Kind 
                                where $Detections2/SpeciesID=lib:abbrev2tsn("Gg","SIO.SWAL.v1")
                                return lib:tsn2abbrev($Detections2/SpeciesID,"SIO.SWAL.v1")
                            }</out>
                    }
                    </ty:Result>`.replace(/\s/g,''), submit.buildQuery(_this.advancedQueries.trees, false));
            });
        }
        this.checkJoinCombinations();
    }

    /**
     * Many combinations of joined queries
     */
    checkJoinCombinations(){
        let _this = this;
        QUnit.module( "Join Combinations" );
        if(AdvancedQueriesTest.joinCombinations.calibrationsAndDeployment){
            QUnit.test("Build Joined Calibrations and Deployment Query", function( assert ){
                let whereData = [];
                let returnData = [];
                returnData.push(new ULData(null,"", _this.advancedQueries.trees.nameToNode(AdvancedQueriesTest.CALIBRATION_ID), _this.advancedQueries.trees.nameToPath(AdvancedQueriesTest.CALIBRATION_ID)));
                returnData.push(new ULData(null,"", _this.advancedQueries.trees.nameToNode(AdvancedQueriesTest.DEPLOYMENT_DEPLOYMENTDETAILS), _this.advancedQueries.trees.nameToPath(AdvancedQueriesTest.DEPLOYMENT_DEPLOYMENTDETAILS)));
                let submit = new SubmitQuery(whereData, returnData, _this.advancedQueries.speciesSelect.select.value);
                assert.equal( submit.buildQuery(_this.advancedQueries.trees, true).replace(/\s/g,''), 
                    `import schema namespace ty=\"http://tethys.sdsu.edu/schema/1.0\"at\"tethys.xsd\";
                    import module namespace lib=\"http://tethys.sdsu.edu/XQueryFns\"at\"Tethys.xq\";
                    <ty:Resultxmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">
                    {
                        for$Calibration1 in collection('Calibrations')/ty:Calibration,
                        $Deployment1incollection('Deployments')/ty:Deployment
                        where $Deployment1/Instrument/ID=$Calibration1/Project 
                        return
                        <out>{
                            $Calibration1/ID,
                            $Deployment1/DeploymentDetails
                        }</out>
                    }</ty:Result>`.replace(/\s/g,''), submit.buildQuery(_this.advancedQueries.trees, true));
            });
        }
        
        if(AdvancedQueriesTest.joinCombinations.deploymentAndDetections){
            QUnit.test("Build Joined Deployment and Detections Query", function( assert ){
                let whereData = [];
                let returnData = [];
                returnData.push(new ULData(null,"", _this.advancedQueries.trees.nameToNode(AdvancedQueriesTest.DEPLOYMENT_PROJECT), _this.advancedQueries.trees.nameToPath(AdvancedQueriesTest.DEPLOYMENT_PROJECT)));
                returnData.push(new ULData(null,"", _this.advancedQueries.trees.nameToNode(AdvancedQueriesTest.DETECTIONS_EFFORT), _this.advancedQueries.trees.nameToPath(AdvancedQueriesTest.DETECTIONS_EFFORT)));
                _this.advancedQueries.speciesSelect.select.value = "SIO.SWAL.v1";
                let submit = new SubmitQuery(whereData, returnData, _this.advancedQueries.speciesSelect.select.value);
                assert.equal( submit.buildQuery(_this.advancedQueries.trees, true).replace(/\s/g,''), 
                    `import schemanamespacety=\"http://tethys.sdsu.edu/schema/1.0\"at\"tethys.xsd\";
                    import module namespace lib=\"http://tethys.sdsu.edu/XQueryFns\"at\"Tethys.xq\";
                    <ty:Resultxmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">
                    {
                        for $Deployment1 in collection('Deployments')/ty:Deployment,
                        $Detections1 in collection('Detections')/ty:Detections
                        where $Deployment1/Project=$Detections1/DataSource/Project and
                        $Deployment1/Site=$Detections1/DataSource/Site and
                        $Deployment1/DeploymentID=$Detections1/DataSource/Deploymentreturn
                        <out>{
                            $Deployment1/Project,
                            $Detections1/Effort}
                        </out>
                    }</ty:Result>`.replace(/\s/g,''), submit.buildQuery(_this.advancedQueries.trees, true));
            });
        }
        if(AdvancedQueriesTest.joinCombinations.deploymentAndEnsemble){
            QUnit.test("Build Joined Deployment and Ensemble Query", function( assert ){
                let whereData = [];
                let returnData = [];
                returnData.push(new ULData(null,"", _this.advancedQueries.trees.nameToNode(AdvancedQueriesTest.DEPLOYMENT_PROJECT), _this.advancedQueries.trees.nameToPath(AdvancedQueriesTest.DEPLOYMENT_PROJECT)));
                returnData.push(new ULData(null,"", _this.advancedQueries.trees.nameToNode(AdvancedQueriesTest.ENSEMBLE_NAME), _this.advancedQueries.trees.nameToPath(AdvancedQueriesTest.ENSEMBLE_NAME)));
                _this.advancedQueries.speciesSelect.select.value = "SIO.SWAL.v1";
                let submit = new SubmitQuery(whereData, returnData, _this.advancedQueries.speciesSelect.select.value);
                assert.equal( submit.buildQuery(_this.advancedQueries.trees, true).replace(/\s/g,''), 
                    `import schemanamespacety=\"http://tethys.sdsu.edu/schema/1.0\"at\"tethys.xsd\";
                    import modulename spacelib=\"http://tethys.sdsu.edu/XQueryFns\"at\"Tethys.xq\";
                    <ty:Resultxmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">
                    {
                        for$Deployment1incollection('Deployments')/ty:Deployment,
                        $Ensemble1incollection('Ensemble')/ty:Ensemble
                        where $Deployment1/Project=$Ensemble1/Unit/Project and
                        $Deployment1/DeploymentID=$Ensemble1/Unit/Deployment
                        return
                        <out>{
                            $Deployment1/Project,
                            $Ensemble1/Name
                        }</out>
                    }</ty:Result>`.replace(/\s/g,''), submit.buildQuery(_this.advancedQueries.trees, true));
            });
        }
        if(AdvancedQueriesTest.joinCombinations.deploymentAndLocalize){
            QUnit.test("Build Joined Deployment and Localize Query", function( assert ){
                let whereData = [];
                let returnData = [];
                returnData.push(new ULData(null,"", _this.advancedQueries.trees.nameToNode(AdvancedQueriesTest.DEPLOYMENT_PROJECT), _this.advancedQueries.trees.nameToPath(AdvancedQueriesTest.DEPLOYMENT_PROJECT)));
                returnData.push(new ULData(null,"", _this.advancedQueries.trees.nameToNode(AdvancedQueriesTest.LOCALIZE_DESCRIPTION), _this.advancedQueries.trees.nameToPath(AdvancedQueriesTest.LOCALIZE_DESCRIPTION)));
                _this.advancedQueries.speciesSelect.select.value = "SIO.SWAL.v1";
                let submit = new SubmitQuery(whereData, returnData, _this.advancedQueries.speciesSelect.select.value);
                assert.equal( submit.buildQuery(_this.advancedQueries.trees, true).replace(/\s/g,''), 
                    `import schema namespace ty=\"http://tethys.sdsu.edu/schema/1.0\"at\"tethys.xsd\";
                    import module namespace lib=\"http://tethys.sdsu.edu/XQueryFns\"at\"Tethys.xq\";
                    <ty:Resultxmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">
                    {
                        for $Deployment1 in collection('Deployments')/ty:Deployment,
                        $Localize1incollection('Localize')/ty:Localize
                        where $Deployment1/DeploymentID=$Localize1/DataSource/Deployment
                        return
                        <out>{
                            $Deployment1/Project,
                            $Localize1/Description
                        }</out>
                    }</ty:Result>`.replace(/\s/g,''), submit.buildQuery(_this.advancedQueries.trees, true));
            });
        }
        if(AdvancedQueriesTest.joinCombinations.detectionsAndEnsemble){
            QUnit.test("Build Joined Detections and Ensemble Query", function( assert ){
                let whereData = [];
                let returnData = [];
                returnData.push(new ULData(null,"", _this.advancedQueries.trees.nameToNode(AdvancedQueriesTest.DETECTIONS_ID), _this.advancedQueries.trees.nameToPath(AdvancedQueriesTest.DETECTIONS_ID)));
                returnData.push(new ULData(null,"", _this.advancedQueries.trees.nameToNode(AdvancedQueriesTest.ENSEMBLE_NAME), _this.advancedQueries.trees.nameToPath(AdvancedQueriesTest.ENSEMBLE_NAME)));
                _this.advancedQueries.speciesSelect.select.value = "SIO.SWAL.v1";
                let submit = new SubmitQuery(whereData, returnData, _this.advancedQueries.speciesSelect.select.value);
                assert.equal( submit.buildQuery(_this.advancedQueries.trees, true).replace(/\s/g,''), 
                    `importschemanamespacety=\"http://tethys.sdsu.edu/schema/1.0\"at\"tethys.xsd\";
                    importmodulenamespacelib=\"http://tethys.sdsu.edu/XQueryFns\"at\"Tethys.xq\";
                    <ty:Resultxmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">
                    {
                        for$Detections1incollection('Detections')/ty:Detections,
                        $Ensemble1incollection('Ensemble')/ty:Ensemble
                        where $Detections1/Datasource/EnsembleName=$Ensemble1/Name
                        return
                        <out>{
                            $Detections1/Id,
                            $Ensemble1/Name
                        }</out>
                    }</ty:Result>`.replace(/\s/g,''), submit.buildQuery(_this.advancedQueries.trees, true));
            });
        }
    }

    /**
     * @var {Array[ULData]} this.whereData
     * ULData(id, node, path, operatorValue, inputValue, conjunctionValue)
     * @return {Array[ULData]}
     */
    generateWhere(){
        let whereData = [];
        whereData.push(new ULData(null,"", this.advancedQueries.trees.nameToNode(AdvancedQueriesTest.DEPLOYMENT_DEPLOYMENTDETAILS_LONGITUDE), this.advancedQueries.trees.nameToPath(AdvancedQueriesTest.DEPLOYMENT_DEPLOYMENTDETAILS_LONGITUDE),"<", 50, " and"));
        whereData.push(new ULData(null,"", this.advancedQueries.trees.nameToNode(AdvancedQueriesTest.DEPLOYMENT_DEPLOYMENTDETAILS_LONGITUDE), this.advancedQueries.trees.nameToPath(AdvancedQueriesTest.DEPLOYMENT_DEPLOYMENTDETAILS_LONGITUDE),">", -50, " and"));
        whereData.push(new ULData(null,"", this.advancedQueries.trees.nameToNode(AdvancedQueriesTest.DEPLOYMENT_DEPLOYMENTDETAILS_LATITUDE), this.advancedQueries.trees.nameToPath(AdvancedQueriesTest.DEPLOYMENT_DEPLOYMENTDETAILS_LATITUDE),">", -40, " and"));
        whereData.push(new ULData(null,"", this.advancedQueries.trees.nameToNode(AdvancedQueriesTest.DEPLOYMENT_DEPLOYMENTDETAILS_LATITUDE), this.advancedQueries.trees.nameToPath(AdvancedQueriesTest.DEPLOYMENT_DEPLOYMENTDETAILS_LATITUDE),"<", 40, ""));
        return whereData;
    }

    /**
     * @var {Array[ULData]} this.whereData
     * ULData(id, node, path, operatorValue, inputValue, conjunctionValue)
     * @return {Array[ULData]}
     */
    generateReturn(){
        let returnData = [];
        returnData.push(new ULData(null,"", this.advancedQueries.trees.nameToNode(AdvancedQueriesTest.DEPLOYMENT_SAMPLINGDETAILS_CHANNEL_START), this.advancedQueries.trees.nameToPath(AdvancedQueriesTest.DEPLOYMENT_SAMPLINGDETAILS_CHANNEL_START)));
        returnData.push(new ULData(null,"", this.advancedQueries.trees.nameToNode(AdvancedQueriesTest.DEPLOYMENT_DEPLOYMENTDETAILS), this.advancedQueries.trees.nameToPath(AdvancedQueriesTest.DEPLOYMENT_DEPLOYMENTDETAILS)));
        returnData.push(new ULData(null,"", this.advancedQueries.trees.nameToNode(AdvancedQueriesTest.DEPLOYMENT_SITE),this.advancedQueries.trees.nameToPath(AdvancedQueriesTest.DEPLOYMENT_SITE)));
        return returnData;    
    }

}


