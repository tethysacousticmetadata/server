/**
 * @author Daniel Weston
 * @version 1.0
 * DSanchezWeston@gmail.com
 * 29 Oct 2018
 */

 class TestSetup{
     constructor(){

     }

     static createTestDivs(){
        let body = document.getElementById("body");
        let qunitFixture = document.createElement("div");
        qunitFixture.setAttribute("id", "qunit-fixture");
        let qunit = document.createElement("div");
        qunit.setAttribute("id", "qunit");
        body.prepend(qunitFixture);
        body.prepend(qunit);
     }
 }