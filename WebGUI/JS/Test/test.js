
QUnit.test( "hello test", function( assert ) {
    assert.ok( 2 == "2", " 2 == 2, Passed!" );
  });

QUnit.test("threeSixtyToOneEighty", function (assert){
    let answer = [0,10,20,30,40,50,60,70,80,90,100,110,120,130,140,150,160,170,-180,-170,-160,-150,-140,-130,-120,-110,-100,-90,-80,-70,-60,-50,-40,-30,-20,-10,0];
    for(var i = 0; i<370; i+=10){
      assert.equal(MapWrapper.threeSixtyToOneEighty(i), answer[i/10], `Passed ${i} -> ${MapWrapper.threeSixtyToOneEighty(i)} = ${answer[i/10]}`);
    }
});

QUnit.test("oneEightyToThreeSixty", function (assert){
  let answer = [180,190,200,210,220,230,240,250,260,270,280,290,300,310,320,330,340,350,0,10,20,30,40,50,60,70,80,90,100,110,120,130,140,150,160,170];
  for(var i = -180; i<170; i+=10){
    assert.equal(MapWrapper.oneEightyToThreeSixty(i), answer[i/10+18], `Passed ${i} -> ${MapWrapper.oneEightyToThreeSixty(i)} = ${answer[i/10+18]}`);
  }
});

var unloaded = [ "Abbreviations", "Localize","Event","Ensemble","Detections","Deployment","Calibration"];
QUnit.test("Tree Structure", function (assert){
  let urlValue = "http://"+window.location.host+"/Schema/"+unloaded.pop()+"?format=JSON";   //AJAX URL to request xml JSON. the schemaSelector Value is spliced into the string.
  let ajaxCall = new AjaxWrapper("get","", this.ajaxError);
  ajaxCall.setURL(urlValue);
  var _this = this;
  $.ajax(ajaxCall.getSettings()).done(response=>{ //When we get the response for the xml JSON=
      let treeData = new XmlToD3();
      treeData.setXmlJson(response);
      treeData.convert();
      _this.trees.addChildren(treeData.getD3Json());
                  
      if(this.unloaded.length>0){ //runs until there are no more collections to load
          _this.getSchema();  
      }
      else{
        assert.equal(_this.trees.length, 8, `Passed ${_this.trees.length} -> 8 = ${_this.trees.length}`);
      }
  });
});

