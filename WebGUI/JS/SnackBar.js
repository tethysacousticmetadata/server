/**
 * @author Daniel Weston
 * @version 1.0
 * DSanchezWeston@gmail.com
 * 29 Oct 2018
 */

 "use strict"
//https://www.w3schools.com/howto/howto_js_snackbar.asp
 class SnackBar{
    
    /**
     * 
     * @param {String} id HTML ID 
     * @param {String} text Initial Test 
     */
    constructor(id, text){
        this.snackBar = document.getElementById(id);
        this.setText(text);
    }

    /**
     * Set text of snack bar
     * @param {String} text 
     */
    setText(text){
        this.snackBar.innerHTML = text;
    }

    /**
     * Get text of snack bar
     */
    getText(){
        return this.snackBar.innerHTML;
    }

    /**
     * Show the snack bar
     */
    show(){
        this.snackBar.className = "show";   // Add the "show" class to DIV
        setTimeout(function(){ this.snackBar.className = snackBar.className.replace("show", ""); }, 3000); // After 3 seconds, remove the show class from DIV
    }

    /**
     * Default text of the snack bar
     */
    static get SNACKBAR_DEFAULT_TEXT(){
        return "Results Updated";
    }

    /**
     * Default query text of the snack bar
     */
    static get SNACKBAR_CREATE_QUERY_TEXT(){
        return "Query Updated";
    }

 }